import React, { Component } from 'react';
import Navigator from './src/navigation/Route';
import {Provider} from 'react-redux'
import { Store, PersistStore } from './src/Store';
import { PersistGate } from 'redux-persist/integration/react';
import {SafeAreaView, ToastAndroid, Alert} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'


export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <Provider store={Store}>
        <PersistGate loading={null} persistor={PersistStore}>
          <SafeAreaView style={{flex: 1,}}>
            <Navigator />
          </SafeAreaView>
        </PersistGate>
      </Provider>
    )
  }
}
