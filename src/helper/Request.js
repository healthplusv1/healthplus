import axios from "axios";
import { Store } from '../Store'

export const GET = (url, params = {}, config = { headers: {} }) => {
  let state = Store.getState()
  if (state.auth.token != null) {
    config.headers.Authorization = 'Bearer' + state.auth.token
  }
  return axios.get(url, params, config);
}

export const POST = (url, params = {}, config = { headers: {} }) => {
  let state = Store.getState()
  if (state.auth.token != null) {
    config.headers.Authorization = 'Bearer' + state.auth.token
  }
  return axios.post(url, params, config);
}

export const UPDATE = (url, params = {}) => {
  return axios.put(url, params);
}

export const DELETE = (url) => {
  return axios.delete(url, params);
}

