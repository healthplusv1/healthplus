import {AsyncStorage} from 'react-native';
import { createStore, applyMiddleware} from 'redux';
import {persistStore, persistReducer} from 'redux-persist';

//import reducer
import routereducer from './RootReducer';
import autoMergeLevel1 from 'redux-persist/lib/stateReconciler/autoMergeLevel1';
import thunk from 'redux-thunk';

const persistConfig ={
    key:'root',
    storage:AsyncStorage,
    stateReconciler:autoMergeLevel1,
    whitelist:['auth']
}
const PersistReducer = persistReducer(persistConfig, routereducer)

const Store = createStore(
    PersistReducer,
    {},
    applyMiddleware(thunk)
)

let PersistStore = persistStore(Store)

export {PersistStore, Store}
