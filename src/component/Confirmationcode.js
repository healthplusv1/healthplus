import React, { useState } from 'react';
import { SafeAreaView, Text, StyleSheet } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage'

import {
    CodeField,
    Cursor,
    useBlurOnFulfill,
    useClearByFocusCell,
} from 'react-native-confirmation-code-field';
import { xs } from '../utils/Responsive';
import { blue, light } from '../utils/Color';

const styles = StyleSheet.create({
    root: { padding: xs(10), },
    codeFiledRoot: { justifyContent: 'space-between', alignItems: 'center' },
    cell: {
        width: xs(50),
        height: xs(50),
        lineHeight: xs(50),
        fontSize: xs(30),
        borderColor: blue,
        textAlign: 'center',
        marginRight: xs(5),
        backgroundColor: blue,
        color: light,
        borderRadius: xs(10)
    },
    focusCell: {
        borderColor: blue,
    },
});

const CELL_COUNT = 6;


const Confirmationcode = () => {
    const [value, setValue] = useState('');
    const ref = useBlurOnFulfill({ value, cellCount: CELL_COUNT });
    const [props, getCellOnLayoutHandler] = useClearByFocusCell({
        value,
        setValue,
    });

    return (
        <SafeAreaView style={styles.root}>
            <CodeField
                ref={ref}
                {...props}
                value={value}
                onEndEditing={() => AsyncStorage.setItem('@kode', value)}
                onChangeText={setValue}
                cellCount={CELL_COUNT}
                rootStyle={styles.codeFiledRoot}
                keyboardType="number-pad"
                renderCell={({ index, symbol, isFocused }) => (
                    <Text
                        key={index}
                        style={[styles.cell, isFocused && styles.focusCell]}
                        onLayout={getCellOnLayoutHandler(index)}>
                        {symbol || (isFocused ? <Cursor /> : null)}
                    </Text>
                )}
            />
        </SafeAreaView>
    );
};

export default Confirmationcode;