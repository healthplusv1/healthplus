import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput } from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen'

class InputText extends Component {
  render() {
    const readonly = this.props.readonly || false
        var editable = true
        if (readonly) {
            editable = false
        }
        if (this.props.disabled) {
            editable = false
        }
    return (
      <View style={{marginBottom: hp(1)}}>
        <Text style={styles.label}>{this.props.label}</Text>
        <TextInput
            style={[styles.inputUser, { color: this.props.disabled ? "#B9B9B9" : "#000" }]}
            placeholder={this.props.placeholder ? this.props.placeholder : this.props.label}
            placeholderTextColor={"#B9B9B9"}
            underlineColorAndroid="#f1f1f1"
            onChangeText={this.props.onChange}
            value={this.props.value}
            editable={editable}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  inputUser: {
      fontSize: wp(4),
      marginTop: hp(-1),
      color: "#000",
  },
  label: {
      color: '#000',
      fontSize: wp(3)
  }
});


export {InputText}
export default (InputText)