import { combineReducers } from 'redux'
import AuthReducer from './module/AuthReducer'

const RootReducer = combineReducers({
    auth: AuthReducer
})

export default RootReducer