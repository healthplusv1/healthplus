export const setAuth = ({user, token}) => {
    return{
        type:'SET_AUTH',
        user,
        token
    }
}