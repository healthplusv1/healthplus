import { createStackNavigator} from 'react-navigation-stack'
import RewardPage from './screen/Reward'
import VoucherPage from './screen/Voucher'
import PulsaPage from './screen/Pulsa'
import PulPage from './screen/Pul'
import TokenKlaimPage from './screen/TokenKlaim'
import StarbucksPage from './screen/Starbucks'
import TokenGamePage from './screen/TokenGame'
import MyRewardsPage from '../reward/screen/MyRewards'

export const Rewardstack = createStackNavigator({
    Reward: {
        screen: RewardPage,
        navigationOptions:{
            header: null,
        }
    },
    Voucher: {
        screen: VoucherPage,
        navigationOptions:{
            header: null,
        }
    },
    Pulsa: {
        screen: PulsaPage,
        navigationOptions:{
            header: null,
        }
    },
    Pul: {
        screen: PulPage,
        navigationOptions:{
            header: null,
        }
    },
    TokenKlaim: {
        screen: TokenKlaimPage,
        navigationOptions:{
            header: null,
        }
    },
    Starbucks: {
        screen: StarbucksPage,
        navigationOptions:{
            header: null,
        }
    },
    TokenGame: {
        screen: TokenGamePage,
        navigationOptions:{
            header: null,
        }
    },
    MyRewards: {
        screen: MyRewardsPage,
        navigationOptions:{
            header: null,
        }
    },
});