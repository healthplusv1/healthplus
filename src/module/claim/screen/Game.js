import React from 'react'
import { Text, View, ImageBackground, Image, ScrollView, TouchableOpacity } from 'react-native'
import { b, p, f, c } from '../utils/StyleHelper';
import { vs, xs } from '../utils/Responsive';
import { blue, light, grey, softBlue } from '../utils/Color';

export default class Game extends React.Component {

	constructor(props) {
		super(props);
		this.state = {

		}
	}

	render() {
		return (
			<>
			<View style={{ width: '100%', height: xs(100), backgroundColor: 'orange' }}>
				<View style={[ p.row, b.ml4, { marginTop: vs(35) }]}>
					<TouchableOpacity onPress={() => this.props.navigation.navigate("Home")}>
						<Image source={require('../asset/back.png')} style={{ width: xs(25), height: xs(25) }} />
					</TouchableOpacity>
					<Text style={[ c.light, f.bold, f._20, { marginLeft: vs(55) }]}> Pusat Permainan </Text>
				</View>
			</View>
			<ScrollView>
				<View style={{ height: xs(30) }} />
				<View style={[ p.center, { width: '100%' }]}>
					<View style={{ width: xs(300) }}>
						<TouchableOpacity onPress={() => this.props.navigation.navigate("DailyHoroskope", {url: 'https://d2387gc122wjn2.cloudfront.net/en/daily-horoscope-healthplus/index.html'})} style={[ b.mt4 ]}>
							<View style={[ b.rounded, { width: '100%', backgroundColor: '#E298FF' }]}>
								<View style={[ p.row ]}>
									<Image source={require('../asset/daily_horoskope.png')} style={[ b.roundedHigh, b.ml2, b.mb2, { width: xs(100), height: xs(80), marginTop: vs(-20) }]} />
									<View style={[ b.ml3, { marginTop: vs(10), width: xs(125) }]}>
										<Text style={[ c.light, f.bold ]}>
											Daily Horoskope
										</Text>
										<View style={[ b.roundedLow, b.mt1, { backgroundColor: '#D16DF8', width: xs(60) }]}>
											<Text style={[ c.light, p.textCenter ]}> Family </Text>
										</View>
									</View>
									<View style={[ b.rounded, b.ml1, { marginTop: vs(20), height: xs(25), backgroundColor: '#D16DF8' }]}>
										<Image source={require('../asset/chevron.png')} style={{ width: xs(25), height: xs(25) }} />
									</View>
								</View>
							</View>
						</TouchableOpacity>

						<TouchableOpacity onPress={() => this.props.navigation.navigate("DailyHoroskope", {url: 'https://d2387gc122wjn2.cloudfront.net/en/megacity-hop-healthplus/index.html'})} style={{ marginTop: vs(35) }}>
							<View style={[ b.rounded, { width: '100%', backgroundColor: '#52A3FF' }]}>
								<View style={[ p.row ]}>
									<Image source={require('../asset/mega_city.png')} style={[ b.roundedHigh, b.ml2, b.mb2, { width: xs(100), height: xs(80), marginTop: vs(-20) }]} />
									<View style={[ b.ml3, { marginTop: vs(10), width: xs(125) }]}>
										<Text style={[ c.light, f.bold ]}>
											Megacity Hop
										</Text>
										<View style={[ b.roundedLow, b.mt1, { backgroundColor: '#2289FF', width: xs(60) }]}>
											<Text style={[ c.light, p.textCenter ]}> Puzzle </Text>
										</View>
									</View>
									<View style={[ b.rounded, b.ml1, { marginTop: vs(20), backgroundColor: '#2289FF', height: xs(25) }]}>
										<Image source={require('../asset/chevron.png')} style={{ width: xs(25), height: xs(25) }} />
									</View>
								</View>
							</View>
						</TouchableOpacity>

						<TouchableOpacity onPress={() => this.props.navigation.navigate("DailyHoroskope", {url: 'https://d2387gc122wjn2.cloudfront.net/en/construct-a-bridge-healthplus/index.html'})} style={{ marginTop: vs(35) }}>
							<View style={[ b.rounded, { width: '100%', backgroundColor: '#FF1516' }]}>
								<View style={[ p.row ]}>
									<Image source={require('../asset/Bridge.png')} style={[ b.roundedHigh, b.ml2, b.mb2, { width: xs(100), height: xs(80), marginTop: vs(-20) }]} />
									<View style={[ b.ml3, { marginTop: vs(10), width: xs(125) }]}>
										<Text style={[ c.light, f.bold ]}>
											Construct A Bridge
										</Text>
										<View style={[ b.roundedLow, b.mt1, { backgroundColor: '#D80001', width: xs(60) }]}>
											<Text style={[ c.light, p.textCenter ]}> Puzzle </Text>
										</View>
									</View>
									<View style={[ b.rounded, b.ml1, { marginTop: vs(20), backgroundColor: '#D80001', height: xs(25) }]}>
										<Image source={require('../asset/chevron.png')} style={{ width: xs(25), height: xs(25) }} />
									</View>
								</View>
							</View>
						</TouchableOpacity>

						<TouchableOpacity onPress={() => this.props.navigation.navigate("DailyHoroskope", {url: 'https://d2387gc122wjn2.cloudfront.net/en/nom-nom-yum-healthplus/index.html'})} style={{ marginTop: vs(35) }}>
							<View style={[ b.rounded, { width: '100%', backgroundColor: '#FABB38' }]}>
								<View style={[ p.row ]}>
									<Image source={require('../asset/Nom-nom.png')} style={[ b.roundedHigh, b.ml2, b.mb2, { width: xs(100), height: xs(80), marginTop: vs(-20) }]} />
									<View style={[ b.ml3, { marginTop: vs(10), width: xs(125) }]}>
										<Text style={[ c.light, f.bold ]}>
											NomNom Yum
										</Text>
										<View style={[ b.roundedLow, b.mt1, { backgroundColor: '#E99D00', width: xs(60) }]}>
											<Text style={[ c.light, p.textCenter ]}> Arcade </Text>
										</View>
									</View>
									<View style={[ b.rounded, b.ml1, { marginTop: vs(20), backgroundColor: '#E99D00', height: xs(25) }]}>
										<Image source={require('../asset/chevron.png')} style={{ width: xs(25), height: xs(25) }} />
									</View>
								</View>
							</View>
						</TouchableOpacity>

						<TouchableOpacity onPress={() => this.props.navigation.navigate("DailyHoroskope", {url: 'https://d2387gc122wjn2.cloudfront.net/en/happy-slushie-healthplus/index.html'})} style={{ marginTop: vs(35) }}>
							<View style={[ b.rounded, { width: '100%', backgroundColor: '#FF9085' }]}>
								<View style={[ p.row ]}>
									<Image source={require('../asset/Happy_Slushie.png')} style={[ b.roundedHigh, b.ml2, b.mb2, { width: xs(100), height: xs(80), marginTop: vs(-20) }]} />
									<View style={[ b.ml3, { marginTop: vs(10), width: xs(125) }]}>
										<Text style={[ c.light, f.bold ]}>
											Happy Slushie
										</Text>
										<View style={[ b.roundedLow, b.mt1, { backgroundColor: '#F97265', width: xs(60) }]}>
											<Text style={[ c.light, p.textCenter ]}> Arcade </Text>
										</View>
									</View>
									<View style={[ b.rounded, b.ml1, { marginTop: vs(20), backgroundColor: '#F97265', height: xs(25) }]}>
										<Image source={require('../asset/chevron.png')} style={{ width: xs(25), height: xs(25) }} />
									</View>
								</View>
							</View>
						</TouchableOpacity>

						<TouchableOpacity onPress={() => this.props.navigation.navigate("DailyHoroskope", {url: 'https://d2387gc122wjn2.cloudfront.net/en/monkey-bubble-shooter-healthplus/index.html'})} style={{ marginTop: vs(35) }}>
							<View style={[ b.rounded, { width: '100%', backgroundColor: '#007E70' }]}>
								<View style={[ p.row ]}>
									<Image source={require('../asset/monkey.png')} style={[ b.roundedHigh, b.ml2, b.mb2, { width: xs(100), height: xs(80), marginTop: vs(-20) }]} />
									<View style={[ b.ml3, { marginTop: vs(10), width: xs(125) }]}>
										<Text style={[ c.light, f.bold ]}>
											Monkey Bubble
										</Text>
										<View style={[ b.roundedLow, b.mt1, { backgroundColor: '#014B43', width: xs(60) }]}>
											<Text style={[ c.light, p.textCenter ]}> Arcade </Text>
										</View>
									</View>
									<View style={[ b.rounded, b.ml1, { marginTop: vs(20), backgroundColor: '#014B43', height: xs(25) }]}>
										<Image source={require('../asset/chevron.png')} style={{ width: xs(25), height: xs(25) }} />
									</View>
								</View>
							</View>
						</TouchableOpacity>

						<TouchableOpacity onPress={() => this.props.navigation.navigate("DailyHoroskope", {url: 'https://d2387gc122wjn2.cloudfront.net/en/giant-hamster-run-healthplus/index.html'})} style={{ marginTop: vs(35) }}>
							<View style={[ b.rounded, { width: '100%', backgroundColor: '#A2A7B0' }]}>
								<View style={[ p.row ]}>
									<Image source={require('../asset/hamster.png')} style={[ b.roundedHigh, b.ml2, b.mb2, { width: xs(100), height: xs(80), marginTop: vs(-20) }]} />
									<View style={[ b.ml3, { marginTop: vs(10), width: xs(125) }]}>
										<Text style={[ c.light, f.bold ]}>
											Giant Hamster Run
										</Text>
										<View style={[ b.roundedLow, b.mt1, { backgroundColor: '#8E949F', width: xs(60) }]}>
											<Text style={[ c.light, p.textCenter ]}> Action </Text>
										</View>
									</View>
									<View style={[ b.rounded, b.ml1, { marginTop: vs(20), backgroundColor: '#8E949F', height: xs(25) }]}>
										<Image source={require('../asset/chevron.png')} style={{ width: xs(25), height: xs(25) }} />
									</View>
								</View>
							</View>
						</TouchableOpacity>

						<TouchableOpacity onPress={() => this.props.navigation.navigate("DailyHoroskope", {url: 'https://d2387gc122wjn2.cloudfront.net/en/chroma-challenge-healthplus/index.html'})} style={{ marginTop: vs(35) }}>
							<View style={[ b.rounded, { width: '100%', backgroundColor: '#607C85' }]}>
								<View style={[ p.row ]}>
									<Image source={require('../asset/chroma.png')} style={[ b.roundedHigh, b.ml2, b.mb2, { width: xs(100), height: xs(80), marginTop: vs(-20) }]} />
									<View style={[ b.ml3, { marginTop: vs(10), width: xs(125) }]}>
										<Text style={[ c.light, f.bold ]}>
											Chroma
										</Text>
										<View style={[ b.roundedLow, b.mt1, { backgroundColor: '#2F515C', width: xs(60) }]}>
											<Text style={[ c.light, p.textCenter ]}> Action </Text>
										</View>
									</View>
									<View style={[ b.rounded, b.ml1, { marginTop: vs(20), backgroundColor: '#2F515C', height: xs(25) }]}>
										<Image source={require('../asset/chevron.png')} style={{ width: xs(25), height: xs(25) }} />
									</View>
								</View>
							</View>
						</TouchableOpacity>

						<TouchableOpacity onPress={() => this.props.navigation.navigate("DailyHoroskope", {url: 'https://d2387gc122wjn2.cloudfront.net/en/koala-sling-healthplus/index.html'})} style={{ marginTop: vs(35) }}>
							<View style={[ b.rounded, { width: '100%', backgroundColor: '#CEE360' }]}>
								<View style={[ p.row ]}>
									<Image source={require('../asset/koala_sling.jpeg')} style={[ b.roundedHigh, b.ml2, b.mb2, { width: xs(100), height: xs(80), marginTop: vs(-20) }]} />
									<View style={[ b.ml3, { marginTop: vs(10), width: xs(125) }]}>
										<Text style={[ c.light, f.bold ]}>
											Koala Sling
										</Text>
										<View style={[ b.roundedLow, b.mt1, { backgroundColor: '#86B13B', width: xs(60) }]}>
											<Text style={[ c.light, p.textCenter ]}> Action </Text>
										</View>
									</View>
									<View style={[ b.rounded, b.ml1, { marginTop: vs(20), backgroundColor: '#86B13B', height: xs(25) }]}>
										<Image source={require('../asset/chevron.png')} style={{ width: xs(25), height: xs(25) }} />
									</View>
								</View>
							</View>
						</TouchableOpacity>

						<TouchableOpacity onPress={() => this.props.navigation.navigate("DailyHoroskope", {url: 'https://d2387gc122wjn2.cloudfront.net/en/pool-game-healthplus/index.html'})} style={{ marginTop: vs(35) }}>
							<View style={[ b.rounded, { width: '100%', backgroundColor: '#7E3C16' }]}>
								<View style={[ p.row ]}>
									<Image source={require('../asset/ball_pool.png')} style={[ b.roundedHigh, b.ml2, b.mb2, { width: xs(100), height: xs(80), marginTop: vs(-20) }]} />
									<View style={[ b.ml3, { marginTop: vs(10), width: xs(125) }]}>
										<Text style={[ c.light, f.bold ]}>
											9 Ball Pool
										</Text>
										<View style={[ b.roundedLow, b.mt1, { backgroundColor: '#2A2113', width: xs(60) }]}>
											<Text style={[ c.light, p.textCenter ]}> Arcade </Text>
										</View>
									</View>
									<View style={[ b.rounded, b.ml1, { marginTop: vs(20), backgroundColor: '#2A2113', height: xs(25) }]}>
										<Image source={require('../asset/chevron.png')} style={{ width: xs(25), height: xs(25) }} />
									</View>
								</View>
							</View>
						</TouchableOpacity>

					</View>
				</View>
				<View style={{ height: xs(30) }} />
			</ScrollView>
			</>
		)
	}
}