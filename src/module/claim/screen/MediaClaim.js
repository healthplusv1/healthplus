import React from 'react'
import { StyleSheet, ActivityIndicator, Text, View, ImageBackground, Image, TouchableOpacity, Alert, ScrollView, I18nManager } from 'react-native'
import { b, p, f, c } from '../utils/StyleHelper';
import { vs, xs } from '../utils/Responsive';
import { blue, light, grey, softBlue } from '../utils/Color';
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { API_URL } from 'react-native-dotenv'

export default class MediaClaim extends React.Component {
	constructor(props) {
	    super(props);
	    this.state ={
	    	nama_media: null,
			deskripsi: null,
			url_ikon: null,
	    	loading: true,
	    	data: null,
	    	nama_file: ''
	    }
	}

	componentDidMount(){
		axios.get(API_URL+'/main/get_media_detail', {params:{
			id_media: this.props.navigation.getParam('id_media')
		}})
		.then(result => {
			// alert(JSON.stringify(result))
			if (result.data.status === "voucher_not_found") {
			this.setState({loading: false}) 
				Alert.alert(
				'Voucher Tidak Ditemukan',
				'Voucher Baru Saja Dihapus',
				[
					{text: 'Kembali', onPress: () => {
						this.props.navigation.navigate('Movie')
					}}
				]);
			}else{
				this.setState({
					loading: false,
					nama_media: result.data.data.nama_media,
					deskripsi: result.data.data.deskripsi,
					url_ikon: result.data.data.url_ikon,
					url_gambar: result.data.data.url_gambar,
					data: result.data.data,
				})
			}
			// alert(JSON.stringify(this.state))
		})
		.catch(e => {
			// Alert.alert('', e)
		})
	}

	claimElearning(){
		Alert.alert(
	      'Klaim E-Learning',
	      'Apakah Anda Yakin?',
	      [
	        {text: 'Batal', onPress: () => {}, style: 'cancel'},
	        {text: 'Klaim', onPress: () => {
				var d = new Date();
				var c = d.getMonth()+1;
				// alert(d.getFullYear()+'-'+c+'-'+d.getDate())
				axios.get(API_URL+'/main/claim_elearning', {params:{
					no_hp: this.state.no_hp,
					id_voucher: this.props.navigation.getParam('id'),
					tanggal_klaim: d.getFullYear()+'-'+c+'-'+d.getDate(),
					harga: this.state.poin
				}})
				.then(respon => {
					// alert(JSON.stringify(respon))
					if (respon.data.status === 'not_enough_point') {
						Alert.alert('','Point Tidak Cukup')
					}else if (respon.data.status === 'true'){
						this.props.navigation.navigate("MyRewards")
					}
				})
				.catch(e => {
					// Alert.alert('',JSON.stringify(e))
				})
	        }}, 
	      ]
	    );
		// this.props.navigation.navigate('MyRewards')
	}

	render() {
		if (this.state.loading) {
			return (
            <View style={[styles.container_loading, styles.horizontal_loading]}>
              <ActivityIndicator size="large" color="#0000ff" />
            </View>
            )
		}else{
			return (
				<ScrollView>
					<View style={[ b.container ]}>
						<ImageBackground source={{ uri: API_URL+'/media_files/'+this.state.url_gambar}} style={{ width: '100%', height: xs(200) }}>
							<TouchableOpacity onPress={() => this.props.navigation.navigate('Movie')} style={[b.ml4, b.mt4, { width: xs(50), height: xs(50) }]}>
								<Image source={require('../asset/back.png')} style={{ width: xs(25), height: xs(25) }}/>
							</TouchableOpacity>
						</ImageBackground>
					</View>
					<View style={[ p.center, b.container ]}>
						<View style={[ p.center, { width: xs(300) }]}>
							<Text style={[ b.mt4, f._20, f.bold, p.textCenter ]}> {this.state.nama_media} </Text>
						</View>
					</View>
					<View style={[  p.center, b.mt2 ]}>
			          <View style={{ borderBottomWidth: 2, borderBottomColor: '#C0C0C0', width: xs(300) }} />
			        </View>
			        <View style={[ b.container, b.mt2, p.center ]}>
			        	<View style={[ b.mt2, { width: xs(310) }]}>
				        	<Text style={[ f.bold, b.mt4, f._18 ]}> Deskripsi </Text>
				        	<Text style={[ b.mt2, b.ml1, f._14 ]}>
				        		{this.state.deskripsi}
				        	</Text>
				        	<View style={[ b.mt4, b.ml1 ]}>
					        	<Image source={{ uri: API_URL+'/media_files/'+this.state.url_ikon }} style={{ width: xs(100), height: xs(100) }} />
				        	</View>
			        	</View>
			        </View>
			        <View style={[  p.center, { marginTop: vs(30) }]}>
			          <View style={{ borderBottomWidth: 2, borderBottomColor: '#C0C0C0', width: xs(300) }} />
			        </View>
			        <View style={[ b.container, p.center, { marginTop: vs(40), paddingBottom: vs(50) }]}>
						<TouchableOpacity onPress={() => this.props.navigation.navigate('MediaMovie', {nama_file: this.state.data.nama_file})} style={[ p.center, { backgroundColor: '#DE00DE', borderRadius: xs(50) }]}>
							<Text style={[ c.light, f.bold, f._18, b.pl3, b.pr3, b.pt3, b.pb3 ]}> Tonton Sekarang </Text>
						</TouchableOpacity>
					</View>
				</ScrollView>
			)
		}
	}
}const styles = StyleSheet.create({
  // loading centered screenn
  container_loading: {
    flex: 1,
    justifyContent: "center"
  },
  horizontal_loading: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },
});