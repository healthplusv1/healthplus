import React from 'react'
import { Text, View, TouchableOpacity, Image, ImageBackground, ScrollView, Alert, StyleSheet, ActivityIndicator} from 'react-native'
import { b, p, f, c } from '../utils/StyleHelper';
import { vs, xs } from '../utils/Responsive';
import { blue, light, grey, softBlue } from '../utils/Color';
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { API_URL } from 'react-native-dotenv'

export default class DramaClaim extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			data: null,
			loading: true,
			nama_file: '',
		}
	}

	// async claimFilm() {
	// 	const no_hp = await AsyncStorage.getItem('@no_hp')
	// 	axios.get(API_URL+'/main/claim_film', {params:{
	// 		no_hp: no_hp,
	// 		id_film: this.props.navigation.getParam('id_film')
	// 	}})
	// 	.then(respon => {
	// 		this.props.navigation.navigate('FilmDrama')
	// 		// alert(JSON.stringify(respon))
	// 	})
	// 	.catch(e => {
	// 		// Alert.alert('',JSON.stringify(e))
	// 	})
	// 	// this.props.navigation.navigate('MyRewards')
	// }

	componentDidMount(){
		axios.get(API_URL+'/main/get_detail_drama/', {params:{
			id_film: this.props.navigation.getParam('id_film')
		}})
		.then(resp => {
			if (resp.data.status) {
				// alert(JSON.stringify(resp))
				this.setState({
					data: resp.data.data,
					loading: false
				})
				// alert(JSON.stringify(this.state.data[0].judul))
			}else{
				Alert.alert(
				'Film Tidak Ditemukan',
				'Film Baru Saja Dihapus',
				[
					{text: 'Kembali', onPress: () => {
						this.props.navigation.navigate('Movie')
					}}
				]);
			}
		})
		.catch(erro =>{
			this.setState({loading: false})
			console.warn(erro)
		})
	}

	render() {
		if (this.state.loading) {
			return (
            <View style={[styles.container_loading, styles.horizontal_loading]}>
              <ActivityIndicator size="large" color="#0000ff" />
            </View>
            )
		}else{
			return (
				<ScrollView>
					<View>
						<ImageBackground source={{ uri: API_URL+'/film_files/'+this.state.data[0].url_gambar }} style={{ width: '100%', height: xs(200) }}>
							<TouchableOpacity onPress={() => this.props.navigation.navigate('Movie')} style={[b.ml4, b.mt4, { width: xs(50), height: xs(50) }]}>
								<Image source={require('../asset/back.png')} style={{ width: xs(25), height: xs(25) }}/>
							</TouchableOpacity>
						</ImageBackground>
					</View>
					<View style={[ p.center, b.mt4, { width: '100%' }]}>
						<Text style={[ p.textCenter, f.bold, f._20, { width: xs(300) }]}>
							{this.state.data[0].judul}
						</Text>
						<Text>
							{this.state.data[0].tahun} | Movie-{this.state.data[0].usia} | {this.state.data[0].genres}
						</Text>
					</View>
					<View style={[ p.center, b.mt4, { width: '100%' }]}>
						<View style={{ borderBottomWidth: 2, borderBottomColor: '#C0C0C0', width: xs(300) }} />
					</View>
					<View style={[ p.center, b.mt4, { width: '100%' }]}>
						<View style={{ width: xs(300) }}>
							<View style={[ p.row ]}>
								<Image source={require('../asset/airbnb-star-selected.png')} style={{ width: xs(50), height: xs(50) }} />
								<View style={[ b.ml2 ]}>
									<View style={[ p.row ]}>
										<Text style={{ fontSize: 22 }}>
											{this.state.data[0].rating}
										</Text>
										<Text style={{ marginTop: vs(7) }}>
											/10
										</Text>
									</View>
									<Text>
										{this.state.data[0].review}
									</Text>
								</View>
							</View>
							<Text style={[ b.mt4, f.bold, f._18 ]}>
								Deskripsi
							</Text>
							<Text style={[ b.mt4, f._14, { textAlign: 'justify' }]}>
								{this.state.data[0].sinopsis}
							</Text>
							<Text style={[ b.mt2, f.italic ]}>
								Pemeran: {this.state.data[0].artis}
							</Text>
						</View>
					</View>
					<View style={[ p.center, b.mt4, { width: '100%', height: xs(20) }]}>
						<View style={{ borderBottomWidth: 2, borderBottomColor: '#C0C0C0', width: xs(300) }} />
					</View>
					<View style={[ b.container, p.center, { marginTop: vs(40), paddingBottom: vs(50) }]}>
						<TouchableOpacity onPress={() => this.props.navigation.navigate('FilmDrama', {nama_file: this.state.data[0].nama_file})} style={[ p.center, { backgroundColor: '#DE00DE', borderRadius: xs(50) }]}>
							<Text style={[ c.light, f.bold, f._18, b.pl3, b.pr3, b.pt3, b.pb3 ]}> Tonton Sekarang </Text>
						</TouchableOpacity>
					</View>
				</ScrollView>
			)
		}
	}
}
const styles = StyleSheet.create({
  // loading centered screenn
  container_loading: {
    flex: 1,
    justifyContent: "center"
  },
  horizontal_loading: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },
});