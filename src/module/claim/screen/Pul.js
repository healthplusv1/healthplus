import React from 'react'
import { Text, Alert, View, TouchableOpacity, ImageBackground, Image, ScrollView, StyleSheet, ActivityIndicator, I18nManager } from 'react-native'
import { b, p, f, c } from '../utils/StyleHelper';
import { vs, xs } from '../utils/Responsive';
import { blue, light, grey, softBlue } from '../utils/Color';
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import analytics from '@react-native-firebase/analytics'
import { API_URL } from 'react-native-dotenv'

export default class Starbucks extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
		  nama_voucher: null,
		  diskon: null,
		  deskripsi: null,
		  harga: null,
		  gambar: null,
		  id_login: null,
		  loading: true
		};
		// https://m.ayobandung.com/images-bandung/post/articles/2020/01/13/76173/logo_google.jpg
	}

	async componentDidMount(){
		await analytics().logEvent('view_reward_pulse', {
			item: 'pulse_id'
		})
		await analytics().setUserProperty('user_sees_detail_pulse', 'null')

		const id_login = await AsyncStorage.getItem('@id_login')
		this.setState({
			id_login: id_login
		})
		// alert(this.props.navigation.getParam('id_voucher'))
		axios.get(API_URL+'/main/get_voucher_detail', {params:{
			id_voucher: this.props.navigation.getParam('id_voucher')
		}})
		.then(result => {
			if (result.data.status === "voucher_not_found") {
				Alert.alert(
				'Voucher Tidak Ditemukan',
				'Voucher Baru Saja Dihapus',
				[
					{text: 'Kembali', onPress: () => {
						this.props.navigation.navigate('Pulsa')
					}}
				]);
			}else{
				this.setState({
	    			nama_voucher: result.data.data.nama_voucher,
					diskon: result.data.data.diskon,
					deskripsi: result.data.data.deskripsi,
					gambar: result.data.data.url_gambar,
					harga: result.data.data.harga,
					loading: false
	    		})
			}
		})
	}

	async claimVoucher(){
		await analytics().logEvent('claim_pulse', {
			item: 'pulse_id'
		})
		await analytics().setUserProperty('user_claim_pulse', 'null')

		// alert(JSON.stringify(this.state))
		Alert.alert(
	      'Klaim Voucher',
	      'Apakah Anda Yakin?',
	      [
	        {text: 'Batal', onPress: () => {}, style: 'cancel'},
	        {text: 'Klaim', onPress: () => {
				var d = new Date();
				var c = d.getMonth()+1;
				// alert(d.getFullYear()+'-'+c+'-'+d.getDate())
				axios.get(API_URL+'/main/claim_voucher', {params:{
					id_login: this.state.id_login,
					id_voucher: this.props.navigation.getParam('id_voucher'),
					tanggal_klaim: d.getFullYear()+'-'+c+'-'+d.getDate(),
					harga: this.state.harga
				}})
				.then(async respon => {
					await analytics().logEvent('claimed_pulse', {
						item: 'pulse_id'
					})
					await analytics().setUserProperty('user_claimed_pulse', 'null')
					
					// alert(JSON.stringify(respon))
					if (respon.data.status === 'not_enough_point') {
						Alert.alert('', 'Point Tidak Cukup')
					}else if (respon.data.status === 'true'){
						this.props.navigation.navigate("MyRewards")
					}
				})
				.catch(e => {
					// alert(e)
				})
	        }}, 
	      ]
	    );
		// this.props.navigation.navigate('MyRewards')
	}

	render() {
		return (
			<>
				{this.state.loading ?
					<View style={[styles.container_loading, styles.horizontal_loading]}>
						<ActivityIndicator size="large" color="#0000ff" />
					</View>
					:
					null
				}
				<ScrollView showsVerticalScrollIndicator={false}  style={{ backgroundColor: '#181B22' }}>
					<View style={[ b.container ]}>
						<ImageBackground source={{ uri: API_URL+'/src/images/'+this.state.gambar }} style={{ width: '100%', height: xs(200) }}>
							<TouchableOpacity onPress={() => this.props.navigation.navigate('Pulsa')} style={[b.ml4, b.mt4, { width: xs(50), height: xs(50) }]}>
								<Image source={require('../asset/back.png')} style={{ width: xs(25), height: xs(25) }}/>
							</TouchableOpacity>
						</ImageBackground>
					</View>
					<View style={[ p.center ]}>
						<View style={[ p.center, { width: xs(300) }]}>
							<Text style={[ b.mt4, f._20, p.textCenter, c.light ]}> {this.state.nama_voucher} - {this.state.diskon} </Text>
						</View>
					</View>
					<View style={[  p.center, b.mt2 ]}>
					<View style={{ borderBottomWidth: 2, borderBottomColor: '#C0C0C0', width: xs(300) }} />
					</View>
					<View style={[ b.container, b.mt4, p.center ]}>
						<View style={[ b.mt4, { width: xs(350) }]}>
							<Text style={[ f.bold, b.mt4, b.ml4, f._18, c.light ]}> Deskripsi </Text>
							<Text style={[ b.mt2, f._14, c.light, { marginLeft: vs(23) }]}>
								{this.state.deskripsi}
							</Text>
						</View>
					</View>
					<View style={[  p.center, { marginTop: vs(50) }]}>
					<View style={{ borderBottomWidth: 2, borderBottomColor: '#C0C0C0', width: xs(300) }} />
					</View>
					<View style={[ b.container, p.center, b.mt2 ]}>
						<View style={[ p.row, { width: xs(300) }]}>
							<Text style={[ c.blue, f._20 ]}>
								Poin yang dibutuhkan :
							</Text>
							<View style={[ b.roundedLow, p.center, b.ml1, { backgroundColor: '#A19ECB' }]}>
								<Text style={[ c.light, f.bold ]}> {this.state.harga} </Text>
							</View>
						</View>
					</View>
					<View style={[ b.container, p.center, { marginTop: vs(40) }]}>
						<TouchableOpacity onPress={() => this.claimVoucher()} style={[ b.roundedHigh, p.center, { backgroundColor: '#A19ECB', width: xs(150), height: xs(40) }]}>
							<Text style={[ c.light, f.bold ]}> Tebus </Text>
						</TouchableOpacity>
					</View>
					<View style={{ height: xs(30) }}></View>
				</ScrollView>
			</>
		)
	}
}
const styles = StyleSheet.create({
  // loading centered screenn
  container_loading: {
    flex: 1,
    position:'absolute',
    zIndex:1,
    width: '100%',
    height: '100%',
    justifyContent: "center",
    backgroundColor: 'rgba(0,0,0,0.7)'
  },
  horizontal_loading: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },
});