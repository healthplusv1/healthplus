import React from 'react'
import { Text, View, Image, TouchableOpacity, ScrollView, StyleSheet, FlatList, ActivityIndicator, ImageBackground } from 'react-native'
import { b, p, f, c } from '../utils/StyleHelper';
import { vs, xs } from '../utils/Responsive';
import { blue, light, grey, softBlue } from '../utils/Color';
import { Container, Tab, Tabs, StyleProvider } from 'native-base';
import axios from 'axios'
import { API_URL } from 'react-native-dotenv'

export default class Movie extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			refresh: false,
			loadinga: true,
			loadingb: true,
			loadingc: true,
			data: null,
			datamedia: null,
			datadrama: null,
		}
	}

	componentDidMount(){
		axios.get(API_URL+'/main/get_all_film')
		.then(resp => {
			if (resp.data.status) {
				this.setState({
					data: resp.data.data,
					loadinga: false
				})
				// alert(JSON.stringify(this.state.data))
			}else{
				this.setState({loadinga: false})
			}
		})
		.catch(erro => {
			this.setState({loadinga: false})
			console.warn(erro)
		})

		axios.get(API_URL+'/main/get_all_drama')
		.then(resp => {
			if (resp.data.status) {
				this.setState({
					datadrama: resp.data.data,
					loadingb: false
				})
				// alert(JSON.stringify(this.state.data))
			}else{
				this.setState({loadingb: false})
			}
		})
		.catch(erro => {
			this.setState({loadingb: false})
			console.warn(erro)
		})

		axios.get(API_URL+'/main/get_all_media')
		.then(resp => {
			if (resp.data.status) {
				this.setState({
					datamedia: resp.data.data,
					loadingc: false
				})
				// alert(JSON.stringify(this.state.data))
			}else{
				this.setState({loadingc: false})
			}
		})
		.catch(erro => {
			this.setState({loadingc: false})
			console.warn(erro)
		})
	}

	// reload(){
	// 	this.state.data
	// 	alert(JSON.stringify(this.state.data))
	// 	this.setState({
	// 		refresh: false
	// 	})
	// }

	render() {
		if (this.state.loadinga || this.state.loadingb || this.state.loadingc) {
	      return (
	        <>
            <View style={[styles.container_loading, styles.horizontal_loading]}>
              <ActivityIndicator size="large" color="#0000ff" />
            </View>
            </>
            )
        }else{

			return (
				<>
					<View style={{ width: '100%', height: xs(100), backgroundColor: '#DE00DE' }}>
						<View style={[ p.row, b.ml4, { marginTop: vs(35) }]}>
							<TouchableOpacity onPress={() => this.props.navigation.navigate("Home")}>
								<Image source={require('../asset/back.png')} style={{ width: xs(25), height: xs(25) }} />
							</TouchableOpacity>
							<Text style={[ c.light, f.bold, f._20, { marginLeft: vs(95) }]}> Media </Text>
						</View>
					</View>
					<Container>
		                <Tabs tabBarUnderlineStyle={{borderBottomWidth: 2, borderBottomColor: '#DE00DE'}}>
		                    <Tab heading='Film' tabStyle={{backgroundColor: 'white'}} textStyle={{color: 'grey'}} 
						      activeTabStyle={{backgroundColor: '#FFFFFF'}}
						      activeTextStyle={{color: '#000000'}}>
						      <View style={[ p.center, b.mt4 ]}>
							      <FlatList
							      numColumns={3}
							      data={this.state.data}
							      keyExtractor={item => item.id_film}
							      renderItem={({item}) => (
							      	<>
							      	<View style={{margin: 5}}>
							      	<TouchableOpacity onPress={() => this.props.navigation.navigate("FilmClaim", {kode: item.kode, id_film: item.id_film})}>
										<Image source={{ uri: API_URL+'/film_files/'+item.url_ikon }} style={{ width: xs(100), height: xs(150) }} />
									</TouchableOpacity>
									</View>
									</>
							      )}
							      />
						      </View>
		                    </Tab>

		                    <Tab heading='Drama' tabStyle={{backgroundColor: 'white'}} textStyle={{color: 'grey'}} 
						      activeTabStyle={{backgroundColor: '#FFFFFF'}}
						      activeTextStyle={{color: '#000000'}}>
						      <View style={[ p.center, b.mt4 ]}>
							      <FlatList
							      numColumns={3}
							      data={this.state.datadrama}
							      keyExtractor={item => item.id_film}
							      renderItem={({item}) => (
							      	<>
							      	<View style={{margin: 5}}>
							      	<TouchableOpacity onPress={() => this.props.navigation.navigate("DramaClaim", {id_film: item.id_film})}>
										<Image source={{ uri: API_URL+'/film_files/'+item.url_ikon }} style={{ width: xs(100), height: xs(150) }} />
									</TouchableOpacity>
									</View>
									</>
							      )}
							      />
						      </View>
		                    </Tab>

		                    <Tab heading='Media' tabStyle={{backgroundColor: 'white'}} textStyle={{color: 'grey'}} 
						      activeTabStyle={{backgroundColor: '#FFFFFF'}}
						      activeTextStyle={{color: '#000000'}}>
						      <View style={[ b.mt4 ]}>
		                        <FlatList
									data={this.state.datamedia}
							        keyExtractor={item => item.id_media}
							        renderItem={({item}) => (
										<View style={[ p.center ]}>
											<TouchableOpacity onPress={() => this.props.navigation.navigate('MediaClaim', {id_media: item.id_media})} style={{ width: xs(350), height: xs(110) }}>
												<ImageBackground source={require('../asset/group_490.png')} style={{ width: xs(350), height: xs(110) }}>
													<View style={[ p.row ]}>
														<View style={[ b.ml3, b.mt1, p.center, { width: xs(160), height: xs(95) }]}>
															<Image source={{uri: API_URL+'/media_files/'+item.url_gambar}} style={{ width: xs(150), height: xs(95) }} />
														</View>
														<View style={[ b.mt1, b.ml1, p.center, { width: xs(160), height: xs(95) }]}>
															<View style={{ width: '100%' }}>
																<Text style={[ f.bold, f._16, b.ml1 ]}>
																	{item.nama_media}
																</Text>
																<Text style={{ marginLeft: vs(2) }}> {item.kategori} </Text>
															</View>
														</View>
													</View>
												</ImageBackground>
											</TouchableOpacity>
										</View>
							        )}
								/>
						      </View>
		                    </Tab>
		                </Tabs>
		            </Container>
				</>
			)
        }
	}
}

const styles = StyleSheet.create({
    container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'green',
    },
    title: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    },
    // loading centered screenn
	container_loading: {
	flex: 1,
	justifyContent: "center"
	},
	horizontal_loading: {
	flexDirection: "row",
	justifyContent: "space-around",
	padding: 10
	}
});