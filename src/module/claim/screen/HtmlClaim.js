import React from 'react'
import { StyleSheet, ActivityIndicator, Text, View, ImageBackground, Image, TouchableOpacity, Alert, ScrollView, I18nManager } from 'react-native'
import { b, p, f, c } from '../utils/StyleHelper';
import { vs, xs } from '../utils/Responsive';
import { blue, light, grey, softBlue } from '../utils/Color';
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { API_URL } from 'react-native-dotenv'

export default class HtmlClaim extends React.Component {
	constructor(props) {
	    super(props);
	    this.state ={
	    	nama_elearning: null,
			tipe: null,
			deskripsi: null,
			url_ikon: null,
			poin: null,
	    	loading: true,
	    	data: null,
	    	nama_file: ''
	    }
	}

	async componentDidMount(){
		const nama = await AsyncStorage.getItem('@nama')
		this.setState({
			nama: nama
		})

		axios.get(API_URL+'/main/get_elearning_detail', {params:{
			id_elearning: this.props.navigation.getParam('id_elearning')
		}})
		.then(result => {
			// alert(JSON.stringify(result))
			if (result.data.status === "voucher_not_found") {
			this.setState({loading: false}) 
				Alert.alert(
				'Voucher Tidak Ditemukan',
				'Voucher Baru Saja Dihapus',
				[
					{text: 'Kembali', onPress: () => {
						this.props.navigation.navigate('Elearning')
					}}
				]);
			}else{
				this.setState({
					loading: false,
					nama_elearning: result.data.data.nama_elearning,
					tipe: result.data.data.tipe,
					deskripsi: result.data.data.deskripsi,
					url_ikon: result.data.data.url_ikon,
					url_gambar: result.data.data.url_gambar,
					poin: result.data.data.poin,
					data: result.data.data
				})
			}
			// alert(JSON.stringify(this.state))
		})
		.catch(e => {
			// Alert.alert('', e)
		})
	}

	render() {
		if (this.state.loading) {
			return (
            <View style={[styles.container_loading, styles.horizontal_loading]}>
              <ActivityIndicator size="large" color="#0000ff" />
            </View>
            )
		}else{
			return (
				<ScrollView>
					<View style={[ b.container ]}>
						<ImageBackground source={this.state.tipe === 'pdf' ? { uri: API_URL+'/elearning_files/'+this.state.url_gambar} : { uri: API_URL+'/elearning_files/video/'+this.state.url_gambar}} style={{ width: '100%', height: xs(200) }}>
							<TouchableOpacity onPress={() => this.props.navigation.navigate('Elearning')} style={[b.ml4, b.mt4, { width: xs(50), height: xs(50) }]}>
								<Image source={require('../asset/back.png')} style={{ width: xs(25), height: xs(25) }}/>
							</TouchableOpacity>
						</ImageBackground>
					</View>
					<View style={[ p.center, b.container ]}>
						<View style={[ p.center, { width: xs(300) }]}>
							<Text style={[ b.mt4, f._20, f.bold, p.textCenter ]}> {this.state.nama_elearning} </Text>
						</View>
					</View>
					<View style={[  p.center, b.mt2 ]}>
			          <View style={{ borderBottomWidth: 2, borderBottomColor: '#C0C0C0', width: xs(300) }} />
			        </View>
			        <View style={[ b.container, b.mt2, p.center ]}>
			        	<View style={[ b.mt2, { width: xs(310) }]}>
			        		<View style={[ p.row ]}>
					        	<Text style={[ f.bold, b.mt4, f._18 ]}> Deskripsi </Text>
					        	<View style={[ p.center, { marginLeft: vs(150) }]}>
					        		<Text style={[ f.bold, f._14, { textTransform: 'capitalize' }]}> {this.state.tipe} </Text>
					        		<Image source={this.state.tipe === 'pdf' ? require('../asset/Path-2749.png') : require('../asset/Path-2751.png')} style={[ b.ml1, { width: xs(30), height: xs(30) }]} />
					        	</View>
			        		</View>
				        	<Text style={[ b.mt2, b.ml1, f._14 ]}>
				        		{this.state.deskripsi}
				        	</Text>
				        	<View style={[ b.mt4, b.ml1 ]}>
					        	<Image source={this.state.tipe === 'pdf' ? { uri: API_URL+'/elearning_files/'+this.state.url_ikon } : { uri: API_URL+'/elearning_files/video/'+this.state.url_ikon }} style={{ width: xs(100), height: xs(100) }} />
				        	</View>
			        	</View>
			        </View>
			        <View style={[  p.center, { marginTop: vs(30) }]}>
			          <View style={{ borderBottomWidth: 2, borderBottomColor: '#C0C0C0', width: xs(300) }} />
			        </View>
			        <View style={[ b.container, p.center, { marginTop: vs(40), paddingBottom: vs(50) }]}>
						<TouchableOpacity onPress={() => this.state.tipe === 'pdf' ? this.props.navigation.navigate('Html', {nama_file: this.state.data.nama_file}) : this.props.navigation.navigate('Adobe', {nama_file: this.state.data.nama_file})} style={[ p.center, { backgroundColor: 'red', borderRadius: xs(50) }]}>
							<Text style={[ c.light, f.bold, f._18, b.pl3, b.pr3, b.pt3, b.pb3 ]}> Pelajari Sekarang </Text>
						</TouchableOpacity>
					</View>
				</ScrollView>
			)
		}
	}
}
const styles = StyleSheet.create({
  // loading centered screenn
  container_loading: {
    flex: 1,
    justifyContent: "center"
  },
  horizontal_loading: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },
});