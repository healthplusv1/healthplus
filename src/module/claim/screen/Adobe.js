import React from 'react'
import { Alert, Text, View, Platform, StyleSheet } from 'react-native'
import Video from 'react-native-video';
import MediaControls, { PLAYER_STATES } from 'react-native-media-controls';
import Orientation from 'react-native-orientation-locker'
import KeepAwake from 'react-native-keep-awake';
import { API_URL } from 'react-native-dotenv'

export default class Adobe extends React.Component {

  videoPlayer;
constructor(props) {
    super(props);
    this.state = {
      currentTime: 0,
      duration: 0,
      isFullScreen: false,
      isLoading: true,
      paused: false,
      isBuffer: false,
      playerState: PLAYER_STATES.PLAYING,
      screenType:'content'
};
  }

  componentDidMount(){
    this.onBuffer()
    KeepAwake.activate();
  }
  onSeek = seek => {
    this.videoPlayer.seek(seek);
  };
  onPaused = playerState => {
    this.setState({
      paused: !this.state.paused,
      playerState,
    });
  };
  onReplay = () => {
    this.videoPlayer.seek(0);
    this.setState({ playerState: PLAYER_STATES.PLAYING });
  };
  onBuffer = buff => {
    console.log(this.props.navigation.getParam('nama_file'))
    console.log(buff)
    if (buff === undefined) {
    }else if (buff.isBuffering === true){
        this.setState({ isLoading: true })
    }else if (buff.isBuffering === false){
        this.setState({ isLoading: false })
    }
  };
  onProgress = data => {
    const { isLoading, playerState } = this.state;
    // Video Player will continue progress even if the video already ended
    if (!isLoading && playerState !== PLAYER_STATES.ENDED) {
      this.setState({ currentTime: data.currentTime });
    }
  };
  onLoad = data => this.setState({ duration: data.duration, isLoading: false });
  onLoadStart = data => this.setState({ isLoading: true });
  onEnd = () => this.setState({ playerState: PLAYER_STATES.ENDED });
  onError = () => alert('Oh! ', error);
  exitFullScreen = () => {
    alert("Exit full screen");
  };
  enterFullScreen = () => {};
  onFullScreen = () => {
    if(this.state.isFullScreen == false){
      Orientation.lockToLandscape();
      this.setState({isFullScreen: true})
    }
    else if (this.state.isFullScreen == true){
      Orientation.unlockAllOrientations();
      this.setState({isFullScreen: false})
    }
  };
  renderToolbar = () => (
    <View >
    </View>
  );
  onSeeking = currentTime => this.setState({ currentTime });
        
  render() {
    return (
      <View style={styles.container}>
        <Video
          bufferConfig={{
        minBufferMs: 15000,
        maxBufferMs: 50000,
        bufferForPlaybackMs: 2500,
        bufferForPlaybackAfterRebufferMs: 5000
      }}
          onEnd={this.onEnd}
          onLoad={this.onLoad}
          onLoadStart={this.onLoadStart}
          onProgress={this.onProgress}
          paused={this.state.paused}
          onBuffer={this.onBuffer}
          ref={videoPlayer => (this.videoPlayer = videoPlayer)}
          resizeMode={this.state.isFullScreen}
          onFullScreen={this.state.isFullScreen}
          source={{ uri: API_URL+'/elearning_files/video/'+this.props.navigation.getParam('nama_file') }} 
          style={styles.mediaPlayer}
          volume={10}
          playInBackground={false}
        />
        <MediaControls
          duration={this.state.duration}
          isLoading={this.state.isLoading}
          isBuffering={this.state.isBuffering}
          mainColor="#333"
          onFullScreen={this.onFullScreen}
          onPaused={this.onPaused}
          onReplay={this.onReplay}
          onSeek={this.onSeek}
          onSeeking={this.onSeeking}
          playerState={this.state.playerState}
          progress={this.state.currentTime}
          toolbar={this.renderToolbar()}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  toolbar: {
    marginTop: 30,
    backgroundColor: 'white',
    padding: 10,
    borderRadius: 5,
  },
  mediaPlayer: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    backgroundColor: 'black',
  },
});