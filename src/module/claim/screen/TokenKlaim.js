import React from 'react'
import { Text, View, TouchableOpacity, ImageBackground, Image, ScrollView, Alert, StyleSheet, ActivityIndicator } from 'react-native'
import { b, p, f, c } from '../utils/StyleHelper';
import { vs, xs } from '../utils/Responsive';
import { blue, light, grey, softBlue } from '../utils/Color';
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import analytics from '@react-native-firebase/analytics'
import { API_URL } from 'react-native-dotenv'

export default class TokenKlaim extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
		  nama_voucher: 'Starbucks Coffee',
		  diskon: 'Disc. 10%',
		  deskripsi: "Whether you'are writing your first line of codes or transforming your career, start with us and learn awesome skills that allows you to stand out and shine. With the changing era into information technology, equip yourself with the latest skill-sets to cater to the demand of fast changing industries.",
		  harga: '950',
		  gambar: null,
		  url_ikon2: null,
		  id_login: null,
		  loading: true
		};
		// https://m.ayobandung.com/images-bandung/post/articles/2020/01/13/76173/logo_google.jpg
	}

	async componentDidMount(){
		await analytics().logEvent('view_reward_token_game', {
			item: 'token_game_id'
		})
		await analytics().setUserProperty('user_sees_detail_token_game', 'null')

		const id_login = await AsyncStorage.getItem('@id_login')
		this.setState({
			id_login: id_login
		})
		// alert(this.props.navigation.getParam('id_voucher'));
		axios.get(API_URL+'/main/get_game_token_detail', {params:{
			id_voucher: this.props.navigation.getParam('id_voucher')
		}})
		.then(result => {
			// alert(JSON.stringify(result))
			if (result.data.status === "voucher_not_found") {
				Alert.alert(
				'Voucher Tidak Ditemukan',
				'Voucher Baru Saja Dihapus',
				[
					{text: 'Kembali', onPress: () => {
						this.props.navigation.navigate('Voucher')
					}}
				]);
			}else{
				this.setState({
					nama_voucher: result.data.data.nama_voucher,
					diskon: result.data.data.diskon,
					deskripsi: result.data.data.deskripsi,
					gambar: result.data.data.url_gambar,
					harga: result.data.data.harga,
					url_ikon2: result.data.data.url_ikon2,
					loading: false
				})
				// alert(JSON.stringify(this.state))
			}
		})
		.catch(e => {
			// Alert.alert('', e)
		})
	}

	async claimVoucher(){
		await analytics().logEvent('claim_token_game', {
			item: 'token_game_id'
		})
		await analytics().setUserProperty('user_claim_token_game', 'null')

		Alert.alert(
	      'Klaim Voucher',
	      'Apakah Anda Yakin?',
	      [
	        {text: 'Batal', onPress: () => {}, style: 'cancel'},
	        {text: 'Klaim', onPress: () => {
				var d = new Date();
				var c = d.getMonth()+1;
				// alert(d.getFullYear()+'-'+c+'-'+d.getDate())
				axios.get(API_URL+'/main/claim_game_token', {params:{
					id_login: this.state.id_login,
					id_voucher: this.props.navigation.getParam('id_voucher'),
					tanggal_klaim: d.getFullYear()+'-'+c+'-'+d.getDate(),
					harga: this.state.harga
				}})
				.then(async respon => {
					await analytics().logEvent('claimed_token_game', {
						item: 'token_game_id'
					})
					await analytics().setUserProperty('user_claimed_token_game', 'null')
					
					// alert(JSON.stringify(respon))
					if (respon.data.status === 'not_enough_point') {
						Alert.alert('','Point Tidak Cukup')
					}else if (respon.data.status === 'true'){
						this.props.navigation.navigate("MyRewards")
					}
				})
				.catch(e => {
					// Alert.alert('',JSON.stringify(e))
				})
	        }}, 
	      ]
	    );
		// this.props.navigation.navigate('MyRewards')
	}

	render() {
		return (
			<>
				{this.state.loading ?
					<View style={[styles.container_loading, styles.horizontal_loading]}>
						<ActivityIndicator size="large" color="#0000ff" />
					</View>
					:
					null
				}
				<ScrollView showsVerticalScrollIndicator={false}  style={{ backgroundColor: '#181B22' }}>
					<View style={[ b.container ]}>
						<ImageBackground source={{ uri: API_URL+'/src/images/'+this.state.gambar }} style={{ width: '100%', height: xs(200) }}>
							<TouchableOpacity onPress={() => this.props.navigation.navigate('TokenGame')} style={[b.ml4, b.mt4, { width: xs(50), height: xs(50) }]}>
								<Image source={require('../asset/back.png')} style={{ width: xs(25), height: xs(25) }}/>
							</TouchableOpacity>
						</ImageBackground>
					</View>
					<View style={[ p.center, b.container ]}>
						<View style={[ p.center, { width: xs(300) }]}>
							<Text style={[ b.mt4, f._20, p.textCenter, c.light ]}> {this.state.nama_voucher} - {this.state.diskon} </Text>
						</View>
					</View>
					<View style={[  p.center, b.mt2 ]}>
					<View style={{ borderBottomWidth: 2, borderBottomColor: '#C0C0C0', width: xs(300) }} />
					</View>
					<View style={[ b.container, b.mt4, p.center ]}>
						<View style={[ b.mt4, { width: xs(310) }]}>
							<Text style={[ f.bold, b.mt4, f._18, c.light ]}> Deskripsi </Text>
							<Text style={[ b.mt2, b.ml1, f._14, c.light ]}>
								{this.state.deskripsi}
							</Text>
							<View style={[ b.mt4, b.ml1 ]}>
								<Image source={{ uri: API_URL+'/src/images/'+this.state.url_ikon2 }} style={{ width: xs(100), height: xs(100) }} />
							</View>
						</View>
					</View>
					<View style={[  p.center, { marginTop: vs(30) }]}>
					<View style={{ borderBottomWidth: 2, borderBottomColor: '#C0C0C0', width: xs(300) }} />
					</View>
					<View style={[ b.container, p.center, b.mt2 ]}>
						<View style={[ p.row, { width: xs(300) }]}>
							<Text style={[ c.blue, f._20 ]}>
								Poin yang dibutuhkan :
							</Text>
							<View style={[ b.roundedLow, p.center, b.ml1, { backgroundColor: '#2DCC70' }]}>
								<Text style={[ c.light, f.bold ]}> {this.state.harga} </Text>
							</View>
						</View>
					</View>
					<View style={[ b.container, p.center, { marginTop: vs(40), paddingBottom: vs(50) }]}>
						<TouchableOpacity onPress={() => this.claimVoucher()} style={[ b.roundedHigh, p.center, { backgroundColor: '#2DCC70', width: xs(150), height: xs(40) }]}>
							<Text style={[ c.light, f.bold, f._18 ]}> Klaim </Text>
						</TouchableOpacity>
					</View>
				</ScrollView>
			</>
		)
	}
}
const styles = StyleSheet.create({
  // loading centered screenn
  container_loading: {
    flex: 1,
    position:'absolute',
    zIndex:1,
    width: '100%',
    height: '100%',
    justifyContent: "center",
    backgroundColor: 'rgba(0,0,0,0.7)'
  },
  horizontal_loading: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },
});