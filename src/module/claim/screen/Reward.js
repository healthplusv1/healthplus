import React from 'react'
import { Text, View, TouchableOpacity, ImageBackground, Image, Alert, ScrollView, StyleSheet, ActivityIndicator, RefreshControl } from 'react-native'
import { b, p, f, c } from '../utils/StyleHelper';
import { vs, xs } from '../utils/Responsive';
import { blue, light, grey, softBlue } from '../utils/Color';
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import Svg from '../utils/Svg';
import LinearGradient from 'react-native-linear-gradient'
import analytics from '@react-native-firebase/analytics'
import { API_URL } from 'react-native-dotenv'

export default class Reward extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			id_login: '',
			point: '....',
			loading: false,
		};
	}

	async componentDidMount(){
		await analytics().logEvent('view_reward', {
			item: 'voucher_id'
		})
		await analytics().setUserProperty('user_sees_reward', 'null')
		
	    const id_login = await AsyncStorage.getItem('@id_login')
	    if (id_login !== null) {
	      this.setState({
	        id_login: id_login,
	      })
	    }
		this.getLatestPoint()
	}

	async getLatestPoint(){
		this.setState({
			point: '....'
		})
		axios.get(API_URL+'/main/get_user_latest_point', {params:{
		id_login: this.state.id_login
		}})
		.then(result => {
		this.setState({
			point: result.data.data.point,
			loading: false
		})
		})
		.catch(e => {
		if (e.message === 'Network Error') {
			Alert.alert('Tidak Ada Koneksi Internet','Mohon Periksa Koneksi Internet Anda Lalu Restart Aplikasi')
			// alert('Network Error: Please Reboot Your Internet Connection')
		}else{
			// Alert.alert('', JSON.stringify(e.message))
		}
		})
	}

	onRefresh() {
		this.setState({ loading: true })
		this.componentDidMount()
	}

	render() {
		return (
			<>
				{this.state.loading ?
					<View style={[styles.container_loading, styles.horizontal_loading]}>
						<ActivityIndicator size="large" color="#0000ff" />
					</View>
					:
					null
				}
				<LinearGradient
					start={{x: 0.25, y: 0.0}} end={{x: 0.5, y: 2.5}}
					locations={[0,0.4,0]}
					colors={[ '#655353', '#000000', '#000000' ]}
					style={[ p.center, { height: xs(80), width: '100%' }]}
				>
					<View style={[ p.row, { width: '100%' }]}>
						<View style={[ b.ml3, { width: xs(210) }]}>
							<Text style={[ c.light, f._20, { fontFamily: 'lineto-circular-pro-bold' }]}>Hadiah</Text>
						</View>
						<View style={[ b.ml2 ]}>
							<TouchableOpacity onPress={() => this.props.navigation.navigate('MyRewards')} style={[ b.roundedHigh, { backgroundColor: '#555555' }]}>
								<View style={[ p.row, p.center, b.ml1, b.mr2, b.mt1, b.mb1 ]}>
									<Svg icon="rewards" size={24} />
									<Text style={[ c.light ]}>Hadiah saya</Text>
								</View>
							</TouchableOpacity>
						</View>
					</View>
				</LinearGradient>
				<ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#000' }} refreshControl={<RefreshControl refreshing={this.state.loading} onRefresh={() => this.onRefresh()} />}>
					<View style={[ p.center, b.mt4 ]}>
						<ImageBackground source={require('../asset/Group-227.png')} style={{ width: xs(350), height: xs(160) }}>
							<View style={[ p.row ]}>
								<View style={[ b.mt4, b.ml4, { width: xs(200) }]}>
									<Text style={[ c.light ]}> Total Poin Saya </Text>
								</View>
								<View style={[p.alignEnd, b.mt4, b.ml4, { width: xs(85) }]}>
									<TouchableOpacity onPress={() => this.getLatestPoint()}>
										<Image source={require('../asset/reward-refresh-btn.png')} style={{ width: xs(15), height: xs(15) }} />
									</TouchableOpacity>
								</View>
							</View>
							<View style={[ p.row ]}>
								<Text style={[ c.light, { fontSize: 70, marginLeft: vs(10), fontFamily: 'lineto-circular-pro-bold' }]}> {this.state.point} </Text>
								<Text style={[ c.light, f._20, { marginTop: vs(55), fontFamily: 'lineto-circular-pro-bold' }]}> Poin </Text>
							</View>
						</ImageBackground>
					</View>
					<View style={[ p.row ]}>
						<View style={[ b.ml2, { width: xs(110), marginTop: vs(23) }]}>
							<View style={[ b.mt2, { borderBottomWidth: 1, borderBottomColor: '#C0C0C0' }]} />
						</View>
						<View style={[ b.ml2, p.center, b.mt4, { width: xs(100) }]}>
							<Text style={[ f._20, c.light, { fontFamily: 'lineto-circular-pro-bold' }]}> Kategori </Text>
						</View>
						<View style={[ b.ml2, { width: xs(110), marginTop: vs(23) }]}>
							<View style={[ b.mt2, { borderBottomWidth: 1, borderBottomColor: '#C0C0C0' }]} />
						</View>
					</View>
					<View style={[ b.container, p.center, b.mt3, { height: xs(70) }]}>
						<TouchableOpacity onPress={() => this.props.navigation.navigate('Voucher')} style={[ b.rounded, p.justifyCenter, { backgroundColor: '#2DCC70', borderWidth: 1, width: xs(300), height: xs(60) }]}>
							<View style={[ p.row ]}>
								<Svg icon="voucher" size={50} style={[ b.ml4 ]} />
								<Text style={[ c.light, b.ml4, b.mt2, f._18, { fontFamily: 'lineto-circular-pro-bold' }]}> Voucher </Text>
							</View>
						</TouchableOpacity>
					</View>
					<View style={[ b.container, p.center, { height: xs(70) }]}>
						<TouchableOpacity onPress={() => this.props.navigation.navigate('Pulsa')} style={[ b.rounded, p.justifyCenter, { backgroundColor: '#A19ECB', borderWidth: 1, width: xs(300), height: xs(60) }]}>
							<View style={[ p.row ]}>
								<Svg icon="pulsa" size={50} style={[ b.ml4 ]} />
								<Text style={[ c.light, b.ml4, b.mt2, f._18, { fontFamily: 'lineto-circular-pro-bold' }]}> Pulsa </Text>
							</View>
						</TouchableOpacity>
					</View>
					<View style={[ b.container, p.center, { height: xs(70) }]}>
						<TouchableOpacity onPress={() => this.props.navigation.navigate('TokenGame')} style={[ b.rounded, p.justifyCenter, { backgroundColor: '#78D1C2', borderWidth: 1, width: xs(300), height: xs(60) }]}>
							<View style={[ p.row ]}>
								<Svg icon="token" size={50} style={[ b.ml4 ]} />
								<Text style={[ c.light, b.ml4, f._18, { marginTop: vs(12), fontFamily: 'lineto-circular-pro-bold' }]}> Token Game </Text>
							</View>
						</TouchableOpacity>
					</View>
					<View style={{ height: xs(50) }} />
				</ScrollView>
			</>
		)
	}
}
const styles = StyleSheet.create({
  // loading centered screenn
  container_loading: {
    flex: 1,
    position:'absolute',
    zIndex:1,
    width: '100%',
    height: '100%',
    justifyContent: "center",
    backgroundColor: 'rgba(0,0,0,0.7)'
  },
  horizontal_loading: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  }
});