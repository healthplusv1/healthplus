import React from 'react'
import { Text, View, Alert, TouchableOpacity, ImageBackground, Image, ScrollView, FlatList, StyleSheet, ActivityIndicator } from 'react-native'
import { b, p, f, c } from '../utils/StyleHelper';
import { vs, xs } from '../utils/Responsive';
import { blue, light, grey, softBlue } from '../utils/Color';
import axios from 'axios'
import analytics from '@react-native-firebase/analytics'
import { API_URL } from 'react-native-dotenv'

export default class Voucher extends React.Component {
	constructor(props) {
        super(props);
        this.state = {
            data: null,
            loading: true
        };
    }

    async componentDidMount(){
		await analytics().logEvent('view_reward_category', {
			item: 'TOKEN_GAME_SCREEN'
		})
		await analytics().setUserProperty('user_sees_all_token_game', 'null')
		
    	axios.get(API_URL+'/main/get_all_token_game')
    	.then(res => {
    		// alert(JSON.stringify(res))
    		this.setState({
    			data: res.data.data,
    			loading: false,
    		})
    	})
    	.catch(e => {
    		// Alert.alert('',e)
    	})
    }

	render() {
		return (
			<>
				{this.state.loading ?
					<View style={[styles.container_loading, styles.horizontal_loading]}>
						<ActivityIndicator size="large" color="#0000ff" />
					</View>
					:
					null
				}
				<View style={[ p.center, { width: '100%', height: xs(80), backgroundColor: '#78D1C2' }]}>
					<View style={[ p.row, { width: '100%' }]}>
						<View style={[ p.center, { width: xs(50), height: xs(50) }]}>
							<TouchableOpacity onPress={() => this.props.navigation.navigate('Reward')}>
								<Image source={require('../asset/back.png')} style={{ width: xs(25), height: xs(25) }}/>
							</TouchableOpacity>
						</View>
						<View style={[ p.center, b.ml4, { width: xs(220) }]}>
							<Text style={[ c.light, f.bold, f._20 ]}> Token Game </Text>
						</View>
					</View>
				</View>
				<ScrollView showsVerticalScrollIndicator={false}  style={{ backgroundColor: '#181B22' }}>
					<View style={[ b.mt4 ]}>
						<FlatList
						data={this.state.data}
						keyExtractor={item => item.id_voucher}
						renderItem={ ({item}) => (
							<View style={[ p.center ]}>
								<TouchableOpacity onPress={() => this.props.navigation.navigate('TokenKlaim', {id_voucher: item.id_voucher})} style={{ width: xs(350), height: xs(110) }}>
									<ImageBackground source={require('../asset/Group-480.png')} style={{ width: xs(350), height: xs(110) }}>
										<View style={[ p.row ]}>
											<Image source={{ uri: API_URL+'/src/images/'+item.url_ikon }} style={[ b.ml4, b.mt4, { width: xs(60), height: xs(60) }]} />
											<View style={[ b.ml4, b.mt1, p.center, { width: xs(150), height: xs(95) }]}>
												<View style={{ width: '100%' }}>
													<Text style={[ f.bold, f._16, b.ml1 ]}>
														{item.nama_voucher}
													</Text>
													<Text style={{ marginLeft: vs(2) }}> {item.diskon} </Text>
												</View>
											</View>
											<View style={[ p.center, b.mt2, { width: xs(60), height: xs(80), marginLeft: vs(10) }]}>
												<Text style={[ f.bold, p.center, c.blue, f._20 ]}> {item.harga} </Text>
												<Text style={[ f.bold, c.blue, f._18 ]}> Poin </Text>
											</View>
										</View>
									</ImageBackground>
								</TouchableOpacity>
							</View>
						)}
						/>
					</View>
					<View style={{ height: xs(20) }}></View>	
				</ScrollView>
			</>
		)
	}
}
const styles = StyleSheet.create({
  // loading centered screenn
  container_loading: {
    flex: 1,
    position:'absolute',
    zIndex:1,
    width: '100%',
    height: '100%',
    justifyContent: "center",
    backgroundColor: 'rgba(0,0,0,0.7)'
  },
  horizontal_loading: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },
});