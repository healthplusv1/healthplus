import { Platform } from 'react-native';
import type { UrlPropsType } from 'react-native-view-pdf';

import base64Data from './base64.json';

export type Resource = {
  resource: string,
  resourceType: 'url' | 'base64' | 'file',
  urlProps?: UrlPropsType,
};

export default resources;