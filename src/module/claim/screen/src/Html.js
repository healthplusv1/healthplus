import React, { Fragment  } from 'react';
import {
  Alert,
  SafeAreaView,
  StatusBar,
  Text,
  View,
} from 'react-native';
import PDFView from 'react-native-view-pdf';
import Spinner from 'react-native-loading-spinner-overlay';
import DropdownAlert from 'react-native-dropdownalert';
import { API_URL } from 'react-native-dotenv'

import styles from './style';
// import resources from './resources';
import type { Resource } from './resources';
import Button from './Button';
import pkg from '../../../../../package.json';
import base64Data from './base64.json';

type StateType = {
  activeButton?: 'assets' | 'file' | 'url' | 'post' | 'base64' | 'errorData' | 'errorProtocol',
  resource?: Resource,
  spinner: boolean,
  canReload?: boolean,
  multiple?: boolean,
};

const HorisontalLine = () => (<View style={styles.horizontalLine}/>);

type PdfContentType = {
  resource?: Resource,
  onRef?: Function,
  onLoad? : Function,
  onError?: Function,
  onPageChanged?: Function,
  onScrolled?: Function,
};

const PdfContent = (props: PdfContentType) => {
  if (props.resource) {
    return (
      <PDFView
        fadeInDuration={250.0}
        style={styles.pdfView}
        ref={props.onRef}
        {...props.resource}
        onLoad={props.onLoad}
        onError={props.onError}
        onScrolled={props.onScrolled}
        onPageChanged={props.onPageChanged}
      />
    );
  }

  return (
    <>
    </>
  );
};


export default class Html extends React.Component {

  _dropdownRef: ?DropdownAlert;

  _pdfRef: ?PDFView;

  _renderStarted: number;


  constructor(props: *) {
    super(props);
    this.state = { resource: undefined, spinner: false };
    this._renderStarted = 0;
  }

  componentDidMount(){
    this.setUrl()
  }

  setUrl = () => {
    const resources: {[key: string]: Resource } = {
      fileAssets: {
        resource: Platform.OS === 'ios' ? 'test-pdf.pdf' : 'assets-pdf.pdf',
        resourceType: 'file',
      },
      file: {
        resource: `${Platform.OS === 'ios' ? '' : '/sdcard/Download/'}downloadedDocument.pdf`,
        resourceType: 'file',
      },
      url: {
        resource: API_URL+'/elearning_files/'+this.props.navigation.getParam('nama_file'),
        resourceType: 'url',
      },
      urlPost: {
        // Run "node demo/utils/server.js" to start the local server. Put correct IP
        resource: 'http://_SERVER_:8080',
        resourceType: 'url',
        urlProps: {
          method: 'POST',
          headers: {
            'Accept-Language': 'en-us,en;q=0.5',
            'Accept-encoding': 'application/pdf',
            'Referer': 'http://localhost/',
          },
          body: 'Request PDF body',
        },
      },
      base64: {
        resource: base64Data.document,
        resourceType: 'base64',
      },
      invalidData: {
        resource: '*invalid base 64*',
        resourceType: 'base64',
      },
      invalidProtocol: {
        resource: 'file:/test-pdf.pdf',
        resourceType: 'url',
      },
    };
    // alert(JSON.stringify(resources.url))
    this.setState({
      activeButton: 'url',
      resource: resources.url,
      spinner: true,
    });
  }


  setUrlPost = () => {
    this.setState({
      activeButton: 'post',
      resource: resources.urlPost,
      spinner: true,
    });
  }


  setBase64 = () => {
    this.setState({
      activeButton: 'base64',
      resource: resources.base64,
      spinner: true,
    });
  }


  setFile = () => {
    this.setState({
      activeButton: 'file',
      resource: resources.file,
      spinner: true,
    });
  }


  setFileAssets = () => {
    this.setState({
      activeButton: 'assets',
      resource: resources.fileAssets,
      spinner: true,
    });
  }


  dataWithError = () => {
    this.setState({
      activeButton: 'errorData',
      resource: resources.invalidData,
      spinner: true,
    });
  }


  protocolWithError = () => {
    this.setState({
      activeButton: 'errorProtocol',
      resource: resources.invalidProtocol,
      spinner: true,
    });
  }


  resetData = () => {
    this.setState({
      activeButton: undefined,
      resource: undefined,
      canReload: false,
    });
  }


  multiplePDFs = () => {
    const { state } = this;

    if (state.multiple) {
      this.setState({ multiple: false });
      return;
    }

    Alert.alert(
      'Duplicate PDF',
      'The PDF will be duplicated',
      [
        { text: 'Cancel', onPress: () => {}, style: 'cancel' },
        { text: 'OK', onPress: () => {
          this.setState({ multiple: true });
        }},
      ],
      { cancelable: false }
    );
  }


  handleLoad = () => {
    this.setState({ spinner: false, canReload: false });
    if (this._dropdownRef) {
      this._dropdownRef.alertWithType(
        'success',
        'Document loaded',
        `Loading time: ${((new Date()).getTime() - this._renderStarted)}`,
      );
    }
  }


  handleError = (error: Error) => {
    this.setState({ spinner: false, canReload: false });
    if (this._dropdownRef) {
      this._dropdownRef.alertWithType(
        'error',
        'Document loading failed',
        'File Not Found'
      );
    }
  }


  handlePageChanged = (page: number, pageCount: number) => {
    console.log(`page ${page + 1} out of ${pageCount}`);
  }

  handleOnScrolled = (offset: number) => {
    console.log(`offset is: ${offset}`);
  }


  reloadPDF = async () => {
    const pdfRef = this._pdfRef;

    if (!pdfRef) {
      return;
    }

    this.setState({ spinner: true });
    try {
      await pdfRef.reload();
    } catch (err) {
      this.setState({ spinner: false });
      if (this._dropdownRef) {
        this._dropdownRef.alertWithType(
          'error',
          'Document reload failed'
        );
      }
    }
  }


  onRef = (ref: ?PDFView) => {
    this._pdfRef = ref;
  }


  render() {
    const { state } = this;
    const { activeButton } = state;
    this._renderStarted = (new Date()).getTime();

    return (
      <Fragment>
        <StatusBar barStyle="dark-content" />
        <SafeAreaView style={styles.container}>
          <PdfContent
            resource={state.resource}
            onLoad={this.handleLoad}
            onError={this.handleError}
            onRef={this.onRef}
            onPageChanged={this.handlePageChanged}
            onScrolled={this.handleOnScrolled}
          />
          
          {state.multiple && state.resource && <HorisontalLine />}
          {state.multiple && state.resource && <PdfContent resource={state.resource} />}
          
          {state.canReload && (
            <View style={styles.floatButtons}>
              <Button
                onPress={this.reloadPDF}
                title="Reload PDF"
                style={styles.reloadButton}
              />
            </View>
          )}
          <Spinner
            visible={this.state.spinner}
            textContent="Loading..."
            textStyle={styles.spinnerTextStyle}
          />
          <DropdownAlert ref={(ref) => {
            this._dropdownRef = ref;
          }}
          />
        </SafeAreaView>
      </Fragment>
    );
  }
}