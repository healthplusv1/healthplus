import React from 'react'
import { ActivityIndicator, StyleSheet, FlatList, Text, View, TouchableOpacity, ImageBackground, Image, ScrollView, I18nManager } from 'react-native'
import { b, p, f, c } from '../utils/StyleHelper';
import { vs, xs } from '../utils/Responsive';
import { blue, light, grey, softBlue } from '../utils/Color';
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { API_URL } from 'react-native-dotenv'


export default class Elearning extends React.Component {

	constructor(props) {
	    super(props);
	    this.state ={
	    	data: null,
	    	loading: true,
	    }
	}

	componentDidMount() {
		axios.get(API_URL+'/main/get_all_e_learning')
    	.then(res => {
    		// alert(JSON.stringify(res))
    		this.setState({
    			data: res.data.data,
    			loading: false,
    		})
    	})
    	.catch(e => {
    		Alert.alert('',e)
    	})
	  }

	render() {
		if (this.state.loading) {
			return (
            <View style={[styles.container_loading, styles.horizontal_loading]}>
              <ActivityIndicator size="large" color="#0000ff" />
            </View>
            )
        }else{
			return (
				<ScrollView>
					<View style={{ height: xs(100), width: '100%', backgroundColor: 'red' }}>
						<View style={[ p.row, b.ml4, { marginTop: vs(35) }]}>
							<TouchableOpacity onPress={() => this.props.navigation.navigate('Home')} style={{ width: xs(50), height: xs(50) }}>
								<Image source={require('../asset/back.png')} style={{ width: xs(25), height: xs(25) }}/>
							</TouchableOpacity>
							<Text style={[ c.light, f.bold, f._20, { marginLeft: vs(50) }]}> E-Learning </Text>
						</View>
					</View>
					<View style={[ b.mt4 ]}>
						<FlatList
						data={this.state.data}
				        keyExtractor={item => item.id_elearning}
				        renderItem={({item}) => (
							<View style={[ p.center ]}>
								<TouchableOpacity onPress={() => this.props.navigation.navigate('HtmlClaim', {id_elearning: item.id_elearning})} style={{ width: xs(350), height: xs(110) }}>
									<ImageBackground source={require('../asset/group_490.png')} style={{ width: xs(350), height: xs(110) }}>
										<View style={[ p.row ]}>
											<View style={[ b.ml3, b.mt1, p.center, { width: xs(160), height: xs(95) }]}>
												<Image source={ item.tipe === 'pdf' ? {uri: API_URL+'/elearning_files/'+item.url_gambar} : {uri: API_URL+'/elearning_files/video/'+item.url_gambar}} style={{ width: xs(150), height: xs(95) }} />
											</View>
											<View style={[ b.mt1, b.ml1, p.center, { width: xs(160), height: xs(95) }]}>
												<View style={{ width: '100%' }}>
													<Text style={[ f.bold, f._16, b.ml1 ]}>
														{item.nama_elearning}
													</Text>
													<Text style={{ marginLeft: vs(2) }}> {item.kategori} </Text>
												</View>
											</View>
										</View>
									</ImageBackground>
								</TouchableOpacity>
							</View>
				        	)}
						/>
					</View>
					<View style={{ height: xs(30) }}></View>
				</ScrollView>
			)
        }
	}
}
const styles = StyleSheet.create({
  // loading centered screenn
  container_loading: {
    flex: 1,
    justifyContent: "center"
  },
  horizontal_loading: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },
});

// onPress={() => item.tipe === 'pdf' ? this.props.navigation.navigate('Html', {nama_file: item.nama_file}) : this.props.navigation.navigate('Adobe', {nama_file: item.nama_file})}