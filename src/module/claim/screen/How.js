import React from 'react'
import { Text, View, ScrollView, Image, TouchableOpacity } from 'react-native'
import { b, p, f, c } from '../utils/StyleHelper'
import { vs, xs } from '../utils/Responsive'
import FastImage from 'react-native-fast-image'

export default class How extends React.Component {
	render() {
		return (
			<ScrollView showsVerticalScrollIndicator={false}>
				<View style={[ p.center, { width: '100%' }]}>
					<FastImage source={require('../asset/Infographic_rev.jpg')} style={{ width: '100%', height: xs(1400) }}>
						<TouchableOpacity onPress={() => this.props.navigation.pop()}>
							<Image source={require('../asset/back.png')} style={[ b.mt4, b.ml4, { marginTop: vs(30), width: xs(30), height: xs(30) }]} />
						</TouchableOpacity>
					</FastImage>
				</View>
			</ScrollView>
		)
	}
}