import React, { memo } from 'react';
import { ms } from '../utils/Responsive';
import Learn from '../asset/icons/learn.svg'
import Voucher from '../asset/icons/voucher.svg'
import Pulsa from '../asset/icons/pulsa.svg'
import Movie from '../asset/icons/movie.svg'
import Token from '../asset/icons/token.svg'
import Tebal_Rewards from '../asset/icons/tebal_rewards.svg'
import Rewards from '../asset/icons/rewards.svg'


const Svg = ({ icon, size = 30, fill, opacity, ...props }) => {
  const icons = {
    learn: Learn,
    voucher: Voucher,
    pulsa: Pulsa,
    movie: Movie,
    token: Token,
    tebal_rewards: Tebal_Rewards,
    rewards: Rewards,
  };

  if (icons[icon] === undefined)
    return (
      <></>
    );

  const Icon = icons[icon]
  return (
    <Icon
      width={ms(size)}
      fill={fill || null}
      height={ms(size)}
      fillOpacity={opacity || 1}
      {...props}
    />
  );
}
export default memo(Svg)
