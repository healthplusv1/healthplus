import React, { memo } from 'react';
import { ms } from '../utils/Responsive';
import Search from '../asset/icons/search.svg'


const Svg = ({ icon, size = 30, fill, opacity, ...props }) => {
  const icons = {
    search: Search,
  };

  if (icons[icon] === undefined)
    return (
      <></>
    );

  const Icon = icons[icon]
  return (
    <Icon
      width={ms(size)}
      fill={fill || null}
      height={ms(size)}
      fillOpacity={opacity || 1}
      {...props}
    />
  );
}
export default memo(Svg)
