import React from 'react'
import { Text, View, TouchableOpacity, Image, ImageBackground, ScrollView, Alert, FlatList, ActivityIndicator, StyleSheet, ToastAndroid } from 'react-native'
import { b, p, f, c } from '../utils/StyleHelper'
import { vs, xs } from '../utils/Responsive'
import { blue, light, grey, softBlue } from '../utils/Color'
import Svg from '../utils/Svg'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'
import {NavigationEvents} from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons'
import FastImage from 'react-native-fast-image'
import LinearGradient from 'react-native-linear-gradient'
import ActionButton from 'react-native-action-button';
import analytics from '@react-native-firebase/analytics'
import { API_URL, API_LOCAL } from 'react-native-dotenv'

export default class League extends React.Component {

	constructor(props){
		super(props);
		this.state ={
			group: null,
			group1: null,
			group2: null,
			loading: true
		}
	}

	async componentDidMount() {
		await analytics().logEvent('view_league', {
			item: 'LEAGUE_SCREEN'
		})
		await analytics().setUserProperty('user_sees_all_league', 'null')
		
		const id_login = await AsyncStorage.getItem('@id_login')
		axios.get(API_URL+'/main/get_admin_group', {params:{
			id_login: id_login
		}})
		.then(result => {
			this.setState({
				loading: false,
				group: result.data.data
			})
			// alert(JSON.stringify(result))
		})
		.catch(e => {
			console.log('get_admin_group => '+e)
		})

		axios.get(API_URL+'/main/get_all_anggota_group', {params:{
			id_login: id_login
		}})
		.then(result => {
			this.setState({
				loading: false,
				group1: result.data.data
			})
			// alert(JSON.stringify(result.data.data))
		})
		.catch(e => {
			console.log('get_anggota_group => '+e)
		})

		axios.get(API_URL+'/main/get_admin_group', {params:{
			id_login: id_login
		}})
		.then(result => {
			this.setState({
				loading: false,
				group2: result.data.data
			})
			// alert(JSON.stringify(result))
		})
		.catch(e => {
			console.log('get_admin_group => '+e)
		})
	}

	async gabungGroup(id_group) {
		const id_login = await AsyncStorage.getItem('@id_login');

		axios.get(API_URL+'/main/request_join_anggota_league', {params:{
			id_login: id_login,
			id_league: id_group
		}})
		.then(response => {
			// alert(JSON.stringify(response))
			if(response.data.status === 'true') {
				Alert.alert('', 'Permintaan untuk Bergabung ke League Terkirim')
				
				axios.get(API_URL+'/main/plus_point', {params:{
					id_login: id_login,
					point: 2,
				}})
				.then(() => {
					console.info('success added 2 point')
					// Alert.alert('',JSON.stringify(result.data))
				})
			}
			this.setState({
				status_join: response.data.status,
			})
			this.componentDidMount()
		})
	}

	render() {
		return (
			<>
				{this.state.loading ?
					<View style={[styles.container_loading, styles.horizontal_loading]}>
						<ActivityIndicator size="large" color="#0000ff" />
					</View>
					:
					null
				}
				<NavigationEvents onDidFocus={() => this.componentDidMount()} />
				<ActionButton buttonColor={blue} style={{ zIndex: 1 }} fixNativeFeedbackRadius={true}>
					<ActionButton.Item buttonColor='#9b59b6' title="Buat Liga" onPress={() => this.props.navigation.navigate('BuatGrup')}>
						<Icon name="md-create" size={20} color={light} />
					</ActionButton.Item>
				</ActionButton>
				<LinearGradient 
					start={{x: 0.25, y: 0.0}} end={{x: 0.5, y: 3.5}}
					locations={[0,0.4,0]}
					colors={[ '#655353', '#000000', '#000000' ]}
					style={[ p.center, { width: '100%', height: xs(80), backgroundColor: '#000000' }]}
				>
					<View style={[ p.row, { width: '100%' }]}>
						<View style={[ b.ml4, { width: xs(200) }]}>
							<Text style={[ c.light, f._20, { fontFamily: 'lineto-circular-pro-bold' }]}>Liga</Text>
						</View>
						{/* <View style={[ b.ml4, p.alignEnd, { width: xs(100) }]}>
							<TouchableOpacity>
								<Svg icon="search" size={25} />
							</TouchableOpacity>
						</View> */}
					</View>
				</LinearGradient>
				<ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#000' }}>
					<View style={{ width: '100%' }}>
						<View style={{ width: '100%' }}>
							<View style={[ b.mt2, { width: '100%', backgroundColor: '#3A3A3A' }]}>
								<Text style={[ c.light, b.ml4, b.mt3 ]}>Liga Saya</Text>
								<View style={[ b.pl2, b.pr2 ]}>
									{this.state.group2 === undefined ?
										<View style={[ p.center, b.mt1, b.mb2, { width: '100%' }]}>
											<View style={[ b.rounded, p.center, { width: xs(320), backgroundColor: '#000' }]}>
												<Text style={[ b.mt1, b.mb1, c.light ]}>Anda belum Mengikuti Liga apapun</Text>
											</View>
										</View>
										:
										<ScrollView showsHorizontalScrollIndicator={false} horizontal={true} style={[ b.mt3, b.mb3 ]}>
											<FlatList
												extraData={this.state}
												horizontal={true}
												data={this.state.group2}
												keyExtractor={item => item.id_group}
												renderItem={({item}) => (
													<TouchableOpacity onPress={() => this.props.navigation.push('Grup_Chall', {id_group: item.id_group})} style={[ b.ml2 ]}>
														<View style={[ p.center, { width: xs(100) }]}>
															<FastImage 
																source={(item.url_gambar === null ? (require('../../home/asset/icon_group.png')) : {
																	uri: API_URL+'/src/icon_group/'+item.url_gambar
																})}
																style={[ b.rounded, { width: xs(80), height: xs(80) }]}
															/>
															<Text style={[ p.textCenter, b.mt1, f._12, c.light ]} numberOfLines={2}>{item.nama_group}</Text>
														</View>
													</TouchableOpacity>
												)}
											/>
										</ScrollView>
									}
								</View>
							</View>
							<View style={{ width: '100%' }}>
								<Text style={[ c.light, b.ml4, b.mt4, b.mb4, f._18, { fontFamily: 'lineto-circular-pro-bold' }]}>Semua Liga</Text>
								<View style={{ borderBottomWidth: 1, borderBottomColor: '#3A3A3A' }} />
							</View>
							{this.state.group1 === undefined ?
								<View style={[ p.center, b.mt1, b.mb2, { width: '100%' }]}>
									<View style={[ b.rounded, p.center, { width: xs(320), backgroundColor: '#555555' }]}>
										<Text style={[ b.mt1, b.mb1, c.light ]}>Tidak ada Liga yang Terserdia</Text>
									</View>
								</View>
								:
								<FlatList
								extraData={this.state}
								data={this.state.group1}
								keyExtractor={item => item.id_group}
								renderItem={({item}) => (
									<>
										<TouchableOpacity onPress={() => this.props.navigation.push('Grup_Chall', {id_group: item.id_group})} style={[ b.ml4, b.mt2 ]}>
											<View style={[ p.row, { width: '100%' }]}>
												<View style={{ width: xs(60) }}>
													<FastImage source={( item.url_gambar === null ? (require('../asset/icon_group.png')) : {uri: API_URL+'/src/icon_group/'+item.url_gambar} )} style={[ b.rounded, { width: xs(50), height: xs(50) }]} />
												</View>
												<View style={[ b.ml4, { width: xs(230) }]}>
													<View style={{ width: xs(225) }}>
														<Text style={[ c.light, { marginTop: vs(-4) }]} numberOfLines={1}>{item.nama_group}</Text>
													</View>
													<View style={[ c.light, p.row ]}>
														<Text style={[ c.light ]}>{item.jml_anggota} Pelari</Text>
														<View style={[ b.bordered, b.rounded, b.ml1, b.mr1, { backgroundColor: '#DADADA', width: xs(7), height: xs(7), marginTop: vs(5) }]} />
														<View style={{ width: xs(160) }}>
															<Text style={[ c.light ]} numberOfLines={1}>{item.lokasi}</Text>
														</View>
													</View>
													<TouchableOpacity onPress={() => this.gabungGroup(item.id_group)} style={[ p.center, b.rounded, b.mt1, { borderWidth: 1, borderColor: '#2C84FF', width: xs(100) }]}>
														<Text style={[ b.mt1, b.mb1, { color: '#2C84FF' }]}>Gabung</Text>
													</TouchableOpacity>
												</View>
											</View>
										</TouchableOpacity>
										<View style={[ b.mt2, { width: '100%', borderBottomWidth: 1, borderBottomColor: '#3A3A3A' }]} />
									</>
								)}
								/>
							}
						</View>
					</View>
				</ScrollView>
			</>
		)
	}
}

const styles = StyleSheet.create({
  // loading centered screenn
  container_loading: {
    flex: 1,
    position:'absolute',
    zIndex:1,
    width: '100%',
    height: '100%',
    justifyContent: "center",
    backgroundColor: 'rgba(0,0,0,0.7)'
  },
  horizontal_loading: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },
});