import { createStackNavigator} from 'react-navigation-stack'
import LeaguePage from './screen/League'
import Grup_ChallPage from './screen/Grup_Chall'
import Kompetisi_LeaguePage from './screen/Kompetisi_League'
import RiwayatKompetisi_LeaguePage from './screen/RiwayatKompetisi_League'
import LihatSemuaPostinganPage from '../home/screen/Tracking/LihatSemuaPostingan'
import LihatKomePage from '../home/screen/Tracking/LihatKome'

export const Leaguestack = createStackNavigator({
    League: {
        screen: LeaguePage,
        navigationOptions:{
            header: null,
        }
    },

    Grup_Chall: {
        screen: Grup_ChallPage,
        navigationOptions:{
            header: null,
        }
    },

    Kompetisi_League: {
        screen: Kompetisi_LeaguePage,
        navigationOptions:{
            header: null,
        }
    },

    RiwayatKompetisi_League: {
        screen: RiwayatKompetisi_LeaguePage,
        navigationOptions:{
            header: null,
        }
    },

    LihatSemuaPostingan: {
        screen: LihatSemuaPostinganPage,
        navigationOptions:{
            header: null,
        }
    },

    LihatKome: {
        screen: LihatKomePage,
        navigationOptions:{
            header: null,
        }
    },
});