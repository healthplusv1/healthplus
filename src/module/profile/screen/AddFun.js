import React from 'react'
import { Text, View, BackHandler, TouchableOpacity, TextInput, StyleSheet, PixelRatio, ScrollView, Image, Alert, ActivityIndicator } from 'react-native'
import { b, p, f, c } from '../utils/StyleHelper'
import { vs, xs } from '../utils/Responsive'
import { blue, light, grey, softBlue } from '../utils/Color'
import Svg from '../utils/Svg'
import ImagePicker from 'react-native-image-picker';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage'
import { API_URL } from 'react-native-dotenv'

export default class AddFun extends React.Component {

	constructor(props){
		super(props);
		this.state ={
			judul: null,
			url_gambar: '',
            uri: '',
            fileName: null,
            file_image: null,
            isLoading: false
		}
	}

	launchCamera = () => {
	    let options = {
	      storageOptions: {
	        skipBackup: true,
	        path: 'images',
	      },
	    };
	    ImagePicker.launchCamera(options, (response) => {
	      console.log('Response = ', response);

	      if (response.didCancel) {
	        console.log('User cancelled image picker');
	      } else if (response.error) {
	        console.log('ImagePicker Error: ', response.error);
	      } else if (response.customButton) {
	        console.log('User tapped custom button: ', response.customButton);
	        alert(response.customButton);
	      } else {
	        console.log(response.fileName);
	        this.setState({
	          uri: {uri: response.uri},
	          url_gambar: response.uri,
	          fileName: response.fileName,
	          file_image: response
	        });
	      }
	    });

	}

  launchImageLibrary = () => {
    let options = {
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.launchImageLibrary(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        alert(response.customButton);
      } else {
        console.log(response);
        // alert(JSON.stringify(response));
        this.setState({
          uri: {uri: response.uri},
          url_gambar: response.uri,
          fileName: response.fileName,
          file_image: response
        });
      }
    });

	}

	renderFileUri() {
	    if (this.state.url_gambar) {
	      return <Image
	        source={{ uri: this.state.url_gambar }}
	        style={styles.images}
	      />
	    } else {
	      return <Image
	        style={styles.images}
	      />
	    }
	}

	createFormDataPhoto = (photo, id) => {
	    const data = new FormData();
	    data.append('file_image', {
	      name: (photo.fileName == null)? 'myphoto': photo.fileName,
	      type: photo.type,
	      uri: photo.uri
	    });

	    data.append("id_user", id);
	    data.append("judul", this.state.judul);
	    data.append("status", 'deactivated');
	    return data;
	  };

	async upload_funstation(){
		const id = await AsyncStorage.getItem('@id')

		// axios.get(API_URL+'/main/insert_funstation', {params:{
		// 	id_user: id,
		// 	judul: this.state.judul,
		// 	url_gambar: this.state.url_gambar,
		// 	status: 'deactivated',
		// }}).then(data => {
		// 	alert(JSON.stringify(data))
		// 	// if (data.data.status == 'true') {
		// 	// 	alert('Success')
		// 	// 	// this.props.navigation.navigate('Fun')
		// 	// } else {
		// 	// 	alert('Failed')
		// 	// }
		// }).catch(erro => {
		// 	// alert(JSON.stringify(erro))
		// 	alert('Error')
		// })

		if (this.state.judul === '' && this.state.file_image === '') {
			Alert.alert('Masukkan Judul dan Gambar untuk memposting!')
		} else {
			this.setState({
				isLoading: true
			})
		}

	    fetch(API_URL+'/main/insert_funstation', {
	      method: 'POST',
	      headers: {
	        'Content-Type': 'multipart/form-data'
	      },
	      body: this.createFormDataPhoto(this.state.file_image, id)
	    })
	      .then(response => response.json())
	      .then(response => {
	        // console.log(response);
	      this.setState({
	      	isLoading: false
	      })
	        Alert.alert(
	        	"Pemberitahuan",
	        	"Postingan Anda telah dikirimkan, kami akan memeriksanya.",
	        	[
	        		{ text: 'OK', onPress: () => this.props.navigation.navigate('Fun') }
	        	]
	        )
	      })
	      .catch(error => {
	        console.log('Upload Error', error);
	        // alert(JSON.stringify(error))
	      });
	}

	render() {
		if(this.state.isLoading){
	      return(
	        <View style={{ left: 0, right: 0, top: 0, bottom: 0, position: 'absolute', alignItems: 'center', justifyContent: 'center', backgroundColor: 'transparent' }}>
	          <ActivityIndicator size="large" color="#0000ff"/>
	        </View>
	      )
	    }
		return (
			<ScrollView>
				<View style={{ height: xs(80), width: '100%', backgroundColor: '#F6B529' }}>
					<View style={[ p.row, b.ml4, { marginTop: vs(23) }]}>
						<TouchableOpacity onPress={() => this.props.navigation.navigate('Fun')}>
							<Svg icon="cross" size={30} />
						</TouchableOpacity>
						<Text style={[ c.light, f.bold, f._20, { marginLeft: vs(90) }]}> Upload </Text>
					</View>
				</View>
				<View style={[ p.center, { width: '100%' }]}>
					<View style={[ b.mt4, b.rounded, b.shadow, { width: xs(325), height: xs(250), borderWidth: 2, borderColor: '#DADADA' }]}>
						<TextInput
							placeholder={"Post title..."}
							multiline={true}
							style={[ b.ml2, { width: xs(300) }]}
							onChangeText={text => {
								let judul = this.state.judul
								judul = text
								this.setState({ judul });
							}}
							value={this.state.judul}
						/>
						<View style={[ p.center, { width: '100%' }]}>
							{this.renderFileUri()}
						</View>
					</View>

					<View style={[ b.mt4, b.rounded, b.shadow, { width: xs(325), height: xs(150), borderWidth: 2, borderColor: '#DADADA' }]}>
						<TouchableOpacity onPress={this.launchCamera}>
							<View style={[ p.row, { marginLeft: vs(60) }]}>
								<Svg icon="cam" size={30} style={[ b.mt4 ]} />
								<Text style={[ c.grey, f.bold, f._18, b.ml4, { marginTop: vs(25) }]}> Gunakan Kamera </Text>
							</View>
						</TouchableOpacity>
						<View style={[ p.center, b.mt4 ]}>
				            <View style={{ borderBottomWidth: 2, borderBottomColor: '#C0C0C0', width: xs(300) }} />
				        </View>
				        <TouchableOpacity onPress={this.launchImageLibrary}>
							<View style={[ p.row, { marginLeft: vs(60) }]}>
								<Svg icon="img" size={30} style={[ b.mt4 ]} />
								<Text style={[ c.grey, f.bold, f._18, b.ml4, { marginTop: vs(25) }]}> Pilih dari Gallery </Text>
							</View>
						</TouchableOpacity>
					</View>
				</View>
				<View style={[ b.container, p.center, { marginTop: vs(40), paddingBottom: vs(50) }]}>
					<TouchableOpacity onPress={() => this.upload_funstation()} style={[ b.roundedHigh, p.center, { backgroundColor: '#F6B529', height: xs(40), width: xs(150) }]}>
						<Text style={[ c.light, f.bold, f._18 ]}> Post </Text>
					</TouchableOpacity>
				</View>
			</ScrollView>
		)
	}
}

const styles = StyleSheet.create({
  images: {
    width: xs(315),
    height: xs(195),
    marginHorizontal: 3,
  },
});