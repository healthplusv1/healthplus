import React from 'react'
import { Text, View, TouchableOpacity, Image, Alert } from 'react-native'
import { b, p, f, c } from '../utils/StyleHelper';
import { vs, xs } from '../utils/Responsive';
import { blue, light, grey, softBlue } from '../utils/Color';
import Share from 'react-native-share';
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios';
import { API_URL } from 'react-native-dotenv'


const url = 'https://healthplus.co/';
const title = '';
const message = 'Please check this out.';
const icon = 'data:<data_type>/<file_extension>;base64,<base64_data>';
const options = Platform.select({
  ios: {
    activityItemSources: [
      { // For sharing url with custom title.
        placeholderItem: { type: 'url', content: url },
        item: {
          default: { type: 'url', content: url },
        },
        subject: {
          default: title,
        },
        linkMetadata: { originalUrl: url, url, title },
      },
      { // For sharing text.
        placeholderItem: { type: 'text', content: message },
        item: {
          default: { type: 'text', content: message },
          message: null, // Specify no text to share via Messages app.
        },
        linkMetadata: { // For showing app icon on share preview.
           title: message
        },
      },
      { // For using custom icon instead of default text icon at share preview when sharing with message.
        placeholderItem: {
          type: 'url',
          content: icon
        },
        item: {
          default: {
            type: 'text',
            content: `${message} ${url}`
          },
        },
        linkMetadata: {
           title: message,
           icon: icon
        }
      },
    ],
  },
  default: {
    title,
    subject: title,
    message: `${message} ${url}`,
  },
});

export default class Station extends React.Component {

	constructor(props){
		super(props);
		this.state = {
            up: '....',
			liked: false,
			id_login: '',
			id_fun_station: ''
		}
	}

	componentDidMount(){
		axios.get(API_URL+'/main/get_funstation_latest_up', {params:{
  			id_fun_station: this.props.id
  		}})
  		.then(result => {
	      // alert(JSON.stringify(result))
	      this.setState({
	        up: result.data.data.up
	      })
	    })
	}

	async _increaseValue(){
		const id_login = await AsyncStorage.getItem('@id_login');

		console.log(this.state.liked)
	    if (this.state.liked == false) {
	      this.setState({
	      	liked: !this.state.liked,
	      	// up: this.state.up +1
	      })
	      axios.get(API_URL+'/main/insert_like', {params:{
	      	id_login: id_login,
	      	id_fun_station: this.props.id
	      }})
	      .then(resp => {
	      	if (resp.data.status === 'true') {
	      		// Alert.alert("Berhasil")
	      		this.setState({
			        up: '....'
			    })
	      		axios.get(API_URL+'/main/get_funstation_latest_up', {params:{
	      			id_fun_station: this.props.id
	      		}})
	      		.then(result => {
			      // alert(JSON.stringify(result))
			      this.setState({
			        up: result.data.data.up
			      })
			    })
			    .catch(e => {
			      if (e.message === 'Network Error') {
			        Alert.alert('Tidak Ada Koneksi Internet','Mohon Periksa Koneksi Internet Anda Lalu Restart Aplikasi')
			        // Alert.alert('Network Error','Please Check Your Internet Connection then Restart The App')
			      }else{
			        // Alert.alert('', JSON.stringify(e.message))
			      }
			    })
	      	} else {
	      		Alert.alert("Gagal")
	      	}
	      })
	      .catch(err => {
	      	Alert.alert("Error")
	      })
	    }
	    else {
	      this.setState({
	      	liked: !this.state.liked,
	      	// up: this.state.up -1
	      })
	      axios.get(API_URL+'/main/delete_like', {params:{
	      	id_login: id_login,
	      	id_fun_station: this.props.id
	      }})
	      .then(resp => {
	      	if (resp.data.status === 'true') {
	      		// Alert.alert("Berhasil")
	      		this.setState({
			        up: '....'
			    })
	      		axios.get(API_URL+'/main/get_funstation_latest_up', {params:{
	      			id_fun_station: this.props.id
	      		}})
	      		.then(result => {
			      // alert(JSON.stringify(result))
			      this.setState({
			        up: result.data.data.up
			      })
			    })
			    .catch(e => {
			      if (e.message === 'Network Error') {
			        Alert.alert('Tidak Ada Koneksi Internet','Mohon Periksa Koneksi Internet Anda Lalu Restart Aplikasi')
			        // Alert.alert('Network Error','Please Check Your Internet Connection then Restart The App')
			      }else{
			        // Alert.alert('', JSON.stringify(e.message))
			      }
			    })
	      	} else {
	      		Alert.alert("Gagal")
	      	}
	      })
	      .catch(err => {
	      	Alert.alert("Error")
	      })
	    }
  	}

	render() {
		return (
			<View style={[ b.container ], {marginBottom: 0, backgroundColor: 'whitesmoke'}}>
                <View style={[ b.mt1, p.center ]}>
                  <View style={[ b.bordered, { width: '100%', height: xs(50), backgroundColor: '#FFFFFF' }]}>
                    <Text style={[ b.mt2, b.ml3, f._18 ]}> {this.props.judul} </Text>
                  </View>
                  <Image source={{ uri: API_URL+'/src/funstation/'+this.props.url_gambar}} style={{ flex: 1, aspectRatio: 1, width: '100%', resizeMode: 'contain' }} />
                  <View style={[ b.bordered, { width: '100%', height: xs(50), backgroundColor: '#FFFFFF' }]}>
                    <View style={[ p.row ]}>
                       <View style={[ b.ml2, b.mt1, p.row, { width: xs(200) }]}>
                         <Image source={this.props.url_gambar_user === null ? { uri: API_URL+'/src/avatar/default.png' } : { uri: API_URL+'/src/avatar/'+this.props.url_gambar_user } } style={{ width: xs(40), height: xs(40) }} />
                         <Text style={[ c.grey, b.ml1, { marginTop: vs(12) }]}> {this.props.nama} </Text>
                       </View>
                       <View style={[ p.row ]}>
                        <TouchableOpacity activeOpacity={.5} onPress={() => {this._increaseValue()}}>
                          <View style={[ p.row ]}>
                            {this.state.liked ? <Image source={require('../asset/liked.png')} style={[ b.mt3, { width: xs(20), height: xs(23) }]} />
                            : <Image source={require('../asset/like.png')} style={[ b.mt3, { width: xs(20), height: xs(23) }]} />}
                            <Text style={[ c.grey, b.mt3, b.ml1 ]}> {this.state.up} </Text>
                          </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => Share.open(options)}>
                          <View style={[ p.row ]}>
                            <Image source={require('../asset/share.png')} style={[ b.mt3, b.ml3, { width: xs(20), height: xs(23) }]} />
                            <Text style={[ c.grey, b.mt3 ]}> SHARE </Text>
                          </View>
                        </TouchableOpacity>
                       </View>
                    </View>
                  </View>
                </View>
            </View>
		)
	}
}