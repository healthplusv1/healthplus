import React, { Component } from 'react'
import { Text, View, ScrollView, TouchableOpacity, TouchableHighlight, Animated } from 'react-native'
import { blue, light } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { b, f, c, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'

export default class SmartWatch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fadeValue: new Animated.Value(0),
            fadeValue2: new Animated.Value(0),
            fadeValue3: new Animated.Value(0),
            fadeValue4: new Animated.Value(0),
        }
    }

    componentDidMount = () => {
        Animated.loop(
            Animated.sequence([
                Animated.timing(this.state.fadeValue, {
                    toValue: 1,
                    duration: 300
                }),
                Animated.timing(this.state.fadeValue2, {
                    toValue: 1,
                    duration: 300
                }),
                Animated.timing(this.state.fadeValue3, {
                    toValue: 1,
                    duration: 300
                }),
                Animated.timing(this.state.fadeValue4, {
                    toValue: 1,
                    duration: 300
                })
            ]),
            {
                iterations: 1000000
            }
        ).start()
    }

    render() {
        return (
            <View style={{ width: '100%', height: '100%', backgroundColor: '#121212' }}>
                <View style={{ width: '100%', alignItems: 'center' }}>
                    <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('4%'), marginBottom: wp('4%') }}>
                        <View style={{ width: wp('10%') }}>
                            <TouchableOpacity onPress={() => this.props.navigation.pop()}>
                                <FastImage source={require('../asset/left.png')} style={{ width: wp('8%'), height: wp('8%') }} />
                            </TouchableOpacity>
                        </View>
                        <View style={{ width: wp('70%'), alignItems: 'center' }}>
                            <Text style={[ c.light, f._18, { fontFamily: 'lineto-circular-pro-bold' }]}>Menambahkan Smartwatch</Text>
                        </View>
                        <View style={{ width: wp('10%') }}></View>
                    </View>
                </View>
                <ScrollView style={{ backgroundColor: '#181B22' }}>
                    <View style={{ width: '100%', alignItems: 'center' }}>
                        <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold', marginTop: wp('4%') }]}>Mencari smartwatch...</Text>
                        <View style={{ width: wp('90%'), height: wp('90%'), marginTop: wp('10%'), alignItems: 'center',  justifyContent: 'center' }}>
                            <FastImage source={require('../asset/hubungkanperangkat_icon.png')} style={{ width: xs(35), height: xs(45) }} />
                            <Animated.View style={{ width: wp('30%'), height: wp('30%'), borderWidth: 2, borderColor: light, borderRadius: wp('30%'), opacity: this.state.fadeValue, position: 'absolute' }} />
                            <Animated.View style={{ width: wp('50%'), height: wp('50%'), borderWidth: 2, borderColor: light, borderRadius: wp('50%'), opacity: this.state.fadeValue2, position: 'absolute' }} />
                            <Animated.View style={{ width: wp('70%'), height: wp('70%'), borderWidth: 2, borderColor: light, borderRadius: wp('50%'), opacity: this.state.fadeValue3, position: 'absolute' }} />
                            <Animated.View style={{ width: wp('90%'), height: wp('90%'), borderWidth: 2, borderColor: light, borderRadius: wp('50%'), opacity: this.state.fadeValue4, position: 'absolute' }} />
                        </View>
                        <TouchableHighlight onPress={() => console.log('Pressed')} underlayColor='#0033FF' style={{ width: wp('90%'), height: hp('8%'), backgroundColor: blue, alignItems: 'center', justifyContent: 'center', marginTop: wp('10%'), borderRadius: wp('2%') }}>
                            <Text style={[ c.light, f._18, { fontFamily: 'lineto-circular-pro-bold' }]}>Pindai Perangkat</Text>
                        </TouchableHighlight>
                    </View>
                </ScrollView>
            </View>
        )
    }
}
