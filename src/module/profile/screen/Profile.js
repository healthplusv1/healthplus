import React, { Component } from 'react'
import { Text, View, TouchableOpacity, FlatList, StyleSheet, TouchableHighlight, Animated, Image, ScrollView, TextInput, ImageBackground, RefreshControl } from 'react-native'
import { blue, light, softGrey } from '../utils/Color'
import { f, c, b, p } from '../utils/StyleHelper'
import { xs, vs } from '../utils/Responsive'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import Icon from 'react-native-vector-icons/Ionicons'
import Dialog, { DialogContent } from 'react-native-popup-dialog'
import DateTimePicker from '@react-native-community/datetimepicker'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { API_URL } from 'react-native-dotenv'

export default class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            edit: false,
            nama: 'loading...',
            url_avatar: null,
            nmr_telp: '+6287882973962',
            email: 'rizki@healthplus.co',
            count: '',
            hidden: false,
            hidden_touch: true,
            touch: '',
            visible: false,
            jenis_kelamin: 'Laki-laki',
            date: new Date(),
            timeActive: false,
            age_now: '',
            month:[
				{bulan: '01'},
				{bulan: '02'},
				{bulan: '03'},
				{bulan: '04'},
				{bulan: '05'},
				{bulan: '06'},
				{bulan: '07'},
				{bulan: '08'},
				{bulan: '09'},
				{bulan: '10'},
				{bulan: '11'},
				{bulan: '12'}
			],
            status: [
				{jenis_status: 'Belum Bayar', image: require('../asset/belumbayar_icon.png')},
				{jenis_status: 'Menuju Apotek', image: require('../asset/menujuapotek_icon.png')},
				{jenis_status: 'Dikirim', image: require('../asset/dikirim_icon.png')},
				{jenis_status: 'Beri Penilaian', image: require('../asset/beripenilaian_icon.png')},
			],
            all_challenges: null,
            clicked: 0,
            pending: undefined,
            to_the_apotek: undefined,
            delivering: undefined,
            finished: undefined
        }
    }

    async componentDidMount() {
        // this.setState({ loading: true })

        const id_login = await AsyncStorage.getItem('@id_login')

        axios.get(API_URL+'/main/get_user_by_id_login', {params:{
            id_login: id_login
        }})
        .then(result => {
            // console.log(result.data.data[0].nama)
            this.setState({
                url_avatar: result.data.data[0].url_gambar,
                nama: result.data.data[0].nama,
            })
        })
        .catch(e => {
            if (e.message === 'Network Error') {
                Alert.alert('Tidak Ada Koneksi Internet','Mohon Periksa Koneksi Internet Anda Lalu Restart Aplikasi')
                // Alert.alert('Network Error','Please Check Your Internet Connection then Restart The App')
            }else{
                // Alert.alert('', JSON.stringify(e.message))
            }
        })

        axios.get(API_URL+'/main/shopping/order/pending_payment_order', {params: {
            id_login: '118205008143923829902'
        }})
        .then(res => {
            // console.log(res.data.data)
            this.setState({
                loading: false,
                pending: res.data.data
            })
        })
        .catch(e => {
            console.log('Pending History => '+e)
        })

        axios.get(API_URL+'/main/shopping/order/to_the_apotek_order', {params: {
            id_login: '118205008143923829902'
        }})
        .then(res => {
            // console.log(res.data.data.length)
            this.setState({
                loading: false,
                to_the_apotek: res.data.data
            })
        })
        .catch(e => {
            console.log('Pending History => '+e)
        })

        axios.get(API_URL+'/main/shopping/order/delivering_order', {params: {
            id_login: '118205008143923829902'
        }})
        .then(res => {
            // console.log(res.data.data)
            this.setState({
                loading: false,
                delivering: res.data.data
            })
        })
        .catch(e => {
            console.log('Pending History => '+e)
        })

        axios.get(API_URL+'/main/shopping/order/finished_order', {params: {
            id_login: '118205008143923829902'
        }})
        .then(res => {
            // console.log(res.data.data)
            this.setState({
                loading: false,
                finished: res.data.data
            })
        })
        .catch(e => {
            console.log('Pending History => '+e)
        })

        axios.get(API_URL+'/main/shopping/cart', {params: {
			id_login: '118205008143923829902'
		}})
		.then(res => {
			this.setState({
				loading: false,
				count: res.data.data.length,
			})
		})
		.catch(e => {
			console.log(e)
		})

        this.calculate_age(this.state.date)
    }

    calculate_age = (dob1) => {
        var today = new Date();
        var birthDate = new Date(dob1);
        var age_now = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) 
        {
            age_now--;
        }
        console.log(age_now);
        this.setState({ age_now: age_now })
        return age_now;
    }

    editBtn() {
        if (this.state.edit === false) {
            this.setState({ edit: true })
        } else {
            this.setState({ edit: false })
        }
    }

    dateFix(date) {
        date = date.split('-')
        if (date[2].length == 1) {
            date[2] = '0'+date[2]
        }
        date = date.join('-')

        return date
    }

    async swicth(jenis_status, index) {
		await this.setState({
            clicked: index,
            loading: false,
            all_challenges: jenis_status
        })
        // console.log(this.state.clicked)
	}
    
    render() {
        return (
            <View style={{ width: '100%', height: '100%', backgroundColor: '#181B22' }}>
                <Dialog
                    visible={this.state.visible}
                    onTouchOutside={() => this.setState({ visible:false })}
                >
                    <DialogContent>
                        <View style={{ width: wp('70%'), marginTop: wp('4%'), marginBottom: wp('-2%') }}>
                            <TouchableOpacity onPress={() => this.setState({ visible: false, jenis_kelamin: 'Laki-laki' })} style={{ width: '100%', height: hp('5%'), alignItems: 'center', justifyContent: 'center' }}>
                                <Text>Laki-laki</Text>
                            </TouchableOpacity>
                            <View style={{ width: '100%', borderBottomWidth: 1, borderBottomColor: '#CCC' }} />
                            <TouchableOpacity onPress={() => this.setState({ visible: false, jenis_kelamin: 'Perempuan' })} style={{ width: '100%', height: hp('5%'), alignItems: 'center', justifyContent: 'center' }}>
                                <Text>Perempuan</Text>
                            </TouchableOpacity>
                        </View>
                    </DialogContent>
                </Dialog>
                <View style={{ width: '100%', backgroundColor: '#121212', alignItems: 'center' }}>
                    <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('5%'), marginBottom: wp('5%') }}>
                        <View style={{ width: wp('45%') }}>
                            <FastImage source={require('../asset/healthPlus.png')} style={{ width: xs(160), height: xs(38) }} />
                        </View>
                        <View style={{ width: wp('45%'), flexDirection: 'row-reverse' }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Drawer')}>
                                <Image source={require('../asset/menu.png')} style={{ width: wp('7%'), height: wp('7%'), tintColor: light }} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Notifikasi')} style={{ marginRight: wp('2%') }}>
                                <Icon name="ios-notifications" size={25} color={light} />
                                {this.state.notif === 'true' ?
                                    <View style={{ width: xs(10), height: xs(10), backgroundColor: 'orangered', borderRadius: xs(5),
                                    position: 'absolute', marginLeft: vs(6), marginTop: vs(1) }} />
                                    :
                                    null
                                }
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.props.navigation.push('Cart')} style={{ marginRight: wp('2%') }}>
                                {this.state.count !== 0 ?
                                <View style={{ width: wp('3.5%'), height: wp('3.5%'), backgroundColor: 'orange', position: 'absolute', borderRadius: wp('3.5%'), zIndex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={[ c.light, f._8 ]}>{this.state.count}</Text>
                                </View>
                                :
                                null
                                }
                                <Image source={require('../asset/addCart.png')} style={{ width: wp('7%'), height: wp('7%'), tintColor: light }} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.props.navigation.push('FrequentlyOrder')} style={{ marginRight: wp('2%') }}>
                                <Image source={require('../asset/Frequentlyorder_icon.png')} style={{ width: wp('8%'), height: wp('7%'), tintColor: light }} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <ScrollView style={{ backgroundColor: '#121212' }} refreshControl={<RefreshControl onRefresh={() => this.componentDidMount()} refreshing={this.state.loading} />}>
                    <View style={{ width: '100%', backgroundColor: '#121212', alignItems: 'center' }}>
                        <View style={{ width: wp('90%'), marginTop: wp('4%'), marginBottom: wp('5%'), flexDirection: 'row', alignItems: 'center' }}>
                            <View style={{ width: wp('20%'), height: wp('20%'), backgroundColor: softGrey, borderRadius: wp('15%'), alignItems: 'center', justifyContent: 'center' }}>
                                <Image source={this.state.url_avatar !== null ? require('../asset/user.png') : { uri: API_URL+'/src/images/'+this.state.url_avatar }} style={{ width: wp('15%'), height: wp('15%'), tintColor: '#CCC' }} />
                            </View>
                            <ImageBackground source={require('../asset/group_1035.png')} style={[ b.ml3, { width: xs(250), height: xs(85) }]} imageStyle={[ b.roundedLow ]}>
                                <View style={ { width: '100%', height: xs(80), alignItems: 'center' }}>
                                    <View style={[ b.mt1, { width: xs(220), flexDirection: 'row', alignItems: 'center' }]}>
                                        <View style={{ width: xs(190) }}>
                                        {this.state.edit === true ?
                                            <TextInput
                                                placeholder='Masukkan Nama Anda'
                                                placeholderTextColor={light}
                                                style={{ width: xs(160), height: xs(35), borderWidth: 1, borderColor: light, borderRadius: xs(5), color: light }}
                                                value={this.state.nama}
                                                onChangeText={(text) => this.setState({ nama: text })}
                                            />
                                            :
                                            <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>{this.state.nama}</Text>
                                        }
                                        </View>
                                        <View style={{ width: xs(30), alignItems: 'flex-end' }}>
                                        <TouchableOpacity onPress={() => this.editBtn()}>
                                            <Image source={this.state.edit === false ? require('../asset/edit.png') : require('../asset/checkmark.png')} style={{ width: xs(20), height: xs(20), tintColor: light }} />
                                        </TouchableOpacity>
                                        </View>
                                    </View>
                                    <View style={[ b.mt1, { width: '100%', height: xs(55), backgroundColor: blue }]}>
                                        <View style={{ width: xs(95), height: '100%', justifyContent: 'center' }}>
                                            <View style={[ b.ml2, { flexDirection: 'row', alignItems: 'center' }]}>
                                                <FastImage source={require('../asset/point_icon.png')} style={{ width: xs(20), height: xs(20) }} />
                                                <Text style={[ c.light, b.ml1, { fontFamily: 'lineto-circular-pro-bold' }]}>100</Text>
                                            </View>
                                            <Text style={[ f._12, c.light, b.ml2, { marginTop: vs(2) }]}>Total Poin Saya</Text>
                                        </View>
                                    </View>
                                </View>
                            </ImageBackground>
                        </View>
                    </View>

                    {this.state.hidden !== true && (
                        <View style={{ width: '100%', marginTop: wp('4%'), alignItems: 'center' }}>
                            <View style={{ width: wp('90%'), flexDirection: 'row' }}>
                                <TouchableOpacity onPress={() => this.setState({ hidden: true, hidden_touch: false, touch: 'riwayat' })} style={{ width: wp('20%'), alignItems: 'center', marginRight: wp('4%') }}>
                                    <View style={{ width: wp('15%'), height: wp('15%'), backgroundColor: blue, borderRadius: wp('2%'), alignItems: 'center', justifyContent: 'center' }}>
                                        <Image source={require('../asset/order_icon.png')} style={{ width: xs(32), height: xs(30) }} />
                                    </View>
                                    <Text style={[ c.light, f._12, { textAlign: 'center' }]}>Riwayat Pesanan</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.setState({ hidden: true, hidden_touch: false, touch: 'akun' })} style={{ width: wp('20%'), alignItems: 'center', marginRight: wp('4%') }}>
                                    <View style={{ width: wp('15%'), height: wp('15%'), backgroundColor: blue, borderRadius: wp('2%'), alignItems: 'center', justifyContent: 'center' }}>
                                        <Image source={require('../asset/pengaturanakun_icon.png')} style={{ width: xs(25), height: xs(31) }} />
                                    </View>
                                    <Text style={[ c.light, f._12, { textAlign: 'center' }]}>Pengaturan Akun</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.props.navigation.push('SmartWatch')} style={{ width: wp('20%'), alignItems: 'center' }}>
                                    <View style={{ width: wp('15%'), height: wp('15%'), backgroundColor: blue, borderRadius: wp('2%'), alignItems: 'center', justifyContent: 'center' }}>
                                        <Image source={require('../asset/hubungkanperangkat_icon.png')} style={{ width: xs(25), height: xs(34) }} />
                                    </View>
                                    <Text style={[ c.light, f._12, { textAlign: 'center' }]}>Hubungkan Perangkat</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    )}

                    {this.state.hidden === true && this.state.hidden_touch === false && this.state.touch === 'riwayat' && (
                        <View style={{ width: '100%', backgroundColor: '#181B22' }}>
                            <View style={{ width: '100%', alignItems: 'center' }}>
                                <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('4%'), marginBottom: wp('2%') }}>
                                    <TouchableOpacity onPress={() => this.setState({ hidden: false, hidden_touch: true })}>
                                        <Image source={require('../asset/left.png')} style={{ width: wp('8%'), height: wp('8%') }} />
                                    </TouchableOpacity>
                                    <Text style={[ c.light, f._16, { marginLeft: wp('2%') }]}>Riwayat Pesanan</Text>
                                </View>
                            </View>
                            <View style={{ width: '100%', height: wp('22%'), marginTop: wp('2%'), borderTopWidth: 2, borderTopColor: light, alignItems: 'center', backgroundColor: '#181B22' }}>
                                <FlatList
                                    data={this.state.status}
                                    horizontal={true}
                                    keyExtractor={item => item.jenis_status}
                                    renderItem={({item, index}) => (
                                        (this.state.clicked === index ?
                                            <View style={{ width: wp('22.5%'), alignItems: 'center', justifyContent: 'center', backgroundColor: blue }}>
                                                <TouchableOpacity onPress={() => this.swicth(item.jenis_status, index)} style={{ alignItems: 'center' }}>
                                                    <Image source={item.image} style={{ width: wp('7%'), height: wp('7%'), tintColor: light }} />
                                                    <Text style={[ b.ml2, b.mr2, b.mt1, b.mb1, c.light, f._12, p.textCenter ]}>{item.jenis_status}</Text>
                                                </TouchableOpacity>
                                            </View>
                                            :
                                            <View style={{ width: wp('22.5%'), alignItems: 'center', justifyContent: 'center' }}>
                                                <TouchableOpacity onPress={() => this.swicth(item.jenis_status, index)} style={{ alignItems: 'center' }}>
                                                    <Image source={item.image} style={{ width: wp('7%'), height: wp('7%'), tintColor: light }} />
                                                    <Text style={[ b.ml2, b.mr2, b.mt1, b.mb1, c.light, f._12, p.textCenter ]}>{item.jenis_status}</Text>
                                                </TouchableOpacity>
                                            </View>
                                        )
                                    )}
                                />
                            </View>
                            <ScrollView style={{ backgroundColor: '#121212' }} refreshControl={<RefreshControl onRefresh={() => this.componentDidMount()} refreshing={this.state.loading} />}>
                                {this.state.clicked === 0 && (
                                    (this.state.pending !== undefined ?
                                        <FlatList
                                            data={this.state.pending}
                                            keyExtractor={item => item.id}
                                            renderItem={({ item }) => (
                                                <View style={{ width: '100%', alignItems: 'center' }}>
                                                    <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
                                                        <View style={{ width: wp('45%'), flexDirection: 'row', alignItems: 'center' }}>
                                                            <View style={[ p.center, { width: wp('10%'), height: wp('10%'), borderRadius: wp('5%'), backgroundColor: light }]}>
                                                                <Image source={require('../asset/logo_apotek.png')} style={{ width: wp('6%'), height: wp('6%') }} />
                                                            </View>
                                                            <Text style={[ c.light, f._16, { marginLeft: wp('2%') }]}>Apotek Sehati</Text>
                                                        </View>
                                                        <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                                                            <Text style={[ c.light, f._16 ]}>Belum dibayar</Text>
                                                        </View>
                                                    </View>
                                                    <FlatList
                                                        data={item.items}
                                                        renderItem={({ item }) => (
                                                            <>
                                                                <View style={{ width: wp('90%'), flexDirection: 'row', marginTop: wp('4%') }}>
                                                                    <View style={{ width: wp('25%') }}>
                                                                        <View style={{ width: '100%', height: wp('25%'), backgroundColor: light, borderRadius: wp('2%'), alignItems: 'center', justifyContent: 'center' }}>
                                                                            <Image source={require('../asset/logo_combi_small.png')} style={{ width: wp('5%'), height: wp('5%'), position: 'absolute', left: wp('1%'), top: wp('1%'), zIndex: 1 }} />
                                                                            <Image source={{ uri: API_URL+'/src/images/'+item.gambar }} style={{ width: wp('16%'), height: wp('16%') }} />
                                                                        </View>
                                                                    </View>
                                                                    <View style={{ width: wp('45%') }}>
                                                                        <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold', marginLeft: wp('2%') }]}>{item.nama}</Text>
                                                                    </View>
                                                                    <View style={{ width: wp('20%'), alignItems: 'flex-end' }}>
                                                                        <Text style={[ c.light, { marginTop: wp('15%') }]}>x{item.jumlah}</Text>
                                                                        <Text style={[ c.light ]}>{this.format(item.harga * item.jumlah)}</Text>
                                                                    </View>
                                                                </View>
                                                                <View style={{ width: '100%', borderBottomWidth: 1, borderBottomColor: light, marginTop: wp('3%') }} />
                                                            </>
                                                        )}
                                                    />

                                                    <View style={{ width: wp('90%'), marginTop: wp('2%'), marginBottom: wp('2%'), flexDirection: 'row', alignItems: 'center' }}>
                                                        <View style={{ width: wp('40%') }}>
                                                            <Text style={[ c.light ]}>{item.items.length} produk</Text>
                                                        </View>
                                                        <View style={{ width: wp('50%'), alignItems: 'flex-end' }}>
                                                            <Text style={[ c.light ]}>Total Pesanan: {this.format(item.total)}</Text>
                                                        </View>
                                                    </View>
                                                    <View style={{ width: wp('90%'), borderBottomWidth: 1, borderBottomColor: light }} />

                                                    <View style={{ width: wp('90%'), alignItems: 'flex-end', marginTop: wp('2%') }}>
                                                        <TouchableOpacity onPress={() => this.nextScreen(item)} style={{ width: wp('30%'), height: wp('10%'), backgroundColor: blue, alignItems: 'center', justifyContent: 'center', borderRadius: wp('1%') }}>
                                                            <Text style={[ c.light ]}>Bayar Sekarang</Text>
                                                        </TouchableOpacity>
                                                    </View>

                                                    <View style={{ width: '100%', borderBottomWidth: 1, borderBottomColor: light, marginTop: wp('2%'), marginBottom: wp('2%') }} />
                                                </View>
                                            )}
                                        />
                                        :
                                        <View style={{ width: '100%', alignItems: 'center' }}>
                                            <View style={{ width: wp('90%'), marginTop: wp('4%'), marginBottom: wp('4%') }}>
                                                <Text style={[ c.light, { textAlign: 'center' }]}>Anda belum memiliki history pesanan</Text>
                                            </View>
                                        </View>
                                    )
                                )}

                                {this.state.all_challenges === 'Menuju Apotek' && (
                                    (this.state.to_the_apotek.length !== 0 ?
                                        <FlatList
                                            data={this.state.to_the_apotek}
                                            keyExtractor={item => item.id}
                                            renderItem={({ item }) => (
                                                <View style={{ width: '100%', alignItems: 'center' }}>
                                                    <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
                                                        <View style={{ width: wp('45%'), flexDirection: 'row', alignItems: 'center' }}>
                                                            <View style={[ p.center, { width: wp('10%'), height: wp('10%'), borderRadius: wp('5%'), backgroundColor: light }]}>
                                                                <Image source={require('../asset/logo_apotek.png')} style={{ width: wp('6%'), height: wp('6%') }} />
                                                            </View>
                                                            <Text style={[ c.light, f._16, { marginLeft: wp('2%') }]}>Apotek Sehati</Text>
                                                        </View>
                                                        <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                                                            <Text style={[ c.light, f._16 ]}>Menuju Apotek</Text>
                                                        </View>
                                                    </View>
                                                    <TouchableOpacity onPress={() => this.props.navigation.push('DetailPayment')}>
                                                        <FlatList
                                                            data={item.items}
                                                            renderItem={({ item }) => (
                                                                <>
                                                                    <View style={{ width: wp('90%'), flexDirection: 'row', marginTop: wp('4%') }}>
                                                                        <View style={{ width: wp('25%') }}>
                                                                            <View style={{ width: '100%', height: wp('25%'), backgroundColor: light, borderRadius: wp('2%'), alignItems: 'center', justifyContent: 'center' }}>
                                                                                <Image source={require('../asset/logo_combi_small.png')} style={{ width: wp('5%'), height: wp('5%'), position: 'absolute', left: wp('1%'), top: wp('1%'), zIndex: 1 }} />
                                                                                <Image source={{ uri: API_URL+'/src/images/'+item.gambar }} style={{ width: wp('16%'), height: wp('16%') }} />
                                                                            </View>
                                                                        </View>
                                                                        <View style={{ width: wp('45%') }}>
                                                                            <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold', marginLeft: wp('2%') }]}>{item.nama}</Text>
                                                                        </View>
                                                                        <View style={{ width: wp('20%'), alignItems: 'flex-end' }}>
                                                                            <Text style={[ c.light, { marginTop: wp('15%') }]}>x{item.jumlah}</Text>
                                                                            <Text style={[ c.light ]}>{this.format(item.harga * item.jumlah)}</Text>
                                                                        </View>
                                                                    </View>
                                                                    <View style={{ width: '100%', borderBottomWidth: 1, borderBottomColor: light, marginTop: wp('3%') }} />
                                                                </>
                                                            )}
                                                        />
                                                    </TouchableOpacity>

                                                    <View style={{ width: wp('90%'), marginTop: wp('2%'), marginBottom: wp('2%'), flexDirection: 'row', alignItems: 'center' }}>
                                                        <View style={{ width: wp('40%') }}>
                                                            <Text style={[ c.light ]}>{item.items.length} produk</Text>
                                                        </View>
                                                        <View style={{ width: wp('50%'), alignItems: 'flex-end' }}>
                                                            <Text style={[ c.light ]}>Total Pesanan: {this.format(item.total)}</Text>
                                                        </View>
                                                    </View>
                                                    <View style={{ width: wp('90%'), borderBottomWidth: 1, borderBottomColor: light }} />
                                                </View>
                                            )}
                                        />
                                        :
                                        <View style={{ width: '100%', alignItems: 'center' }}>
                                            <View style={{ width: wp('90%') }}>
                                                <Text style={[ c.light, { textAlign: 'center' }]}>Anda belum memiliki pesanan</Text>
                                            </View>
                                        </View>
                                    )
                                )}

                                {this.state.all_challenges === 'Dikirim' && (
                                    (this.state.delivering.length !== 0 ?
                                        <FlatList
                                            data={this.state.delivering}
                                            keyExtractor={item => item.id}
                                            renderItem={({ item }) => (
                                                <View style={{ width: '100%', alignItems: 'center' }}>
                                                    <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
                                                        <View style={{ width: wp('45%'), flexDirection: 'row', alignItems: 'center' }}>
                                                            <View style={[ p.center, { width: wp('10%'), height: wp('10%'), borderRadius: wp('5%'), backgroundColor: light }]}>
                                                                <Image source={require('../asset/logo_apotek.png')} style={{ width: wp('6%'), height: wp('6%') }} />
                                                            </View>
                                                            <Text style={[ c.light, f._16, { marginLeft: wp('2%') }]}>Apotek Sehati</Text>
                                                        </View>
                                                        <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                                                            <Text style={[ c.light, f._16 ]}>Sedang dikirim</Text>
                                                        </View>
                                                    </View>
                                                    <TouchableOpacity onPress={() => this.props.navigation.push('DetailPayment')}>
                                                        <FlatList
                                                            data={item.items}
                                                            renderItem={({ item }) => (
                                                                <>
                                                                    <View style={{ width: wp('90%'), flexDirection: 'row', marginTop: wp('4%') }}>
                                                                        <View style={{ width: wp('25%') }}>
                                                                            <View style={{ width: '100%', height: wp('25%'), backgroundColor: light, borderRadius: wp('2%'), alignItems: 'center', justifyContent: 'center' }}>
                                                                                <Image source={require('../asset/logo_combi_small.png')} style={{ width: wp('5%'), height: wp('5%'), position: 'absolute', left: wp('1%'), top: wp('1%'), zIndex: 1 }} />
                                                                                <Image source={{ uri: API_URL+'/src/images/'+item.gambar }} style={{ width: wp('16%'), height: wp('16%') }} />
                                                                            </View>
                                                                        </View>
                                                                        <View style={{ width: wp('45%') }}>
                                                                            <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold', marginLeft: wp('2%') }]}>{item.nama}</Text>
                                                                        </View>
                                                                        <View style={{ width: wp('20%'), alignItems: 'flex-end' }}>
                                                                            <Text style={[ c.light, { marginTop: wp('15%') }]}>x{item.jumlah}</Text>
                                                                            <Text style={[ c.light ]}>{this.format(item.harga * item.jumlah)}</Text>
                                                                        </View>
                                                                    </View>
                                                                    <View style={{ width: '100%', borderBottomWidth: 1, borderBottomColor: light, marginTop: wp('3%') }} />
                                                                </>
                                                            )}
                                                        />
                                                    </TouchableOpacity>

                                                    <View style={{ width: wp('90%'), marginTop: wp('2%'), marginBottom: wp('2%'), flexDirection: 'row', alignItems: 'center' }}>
                                                        <View style={{ width: wp('40%') }}>
                                                            <Text style={[ c.light ]}>{item.items.length} produk</Text>
                                                        </View>
                                                        <View style={{ width: wp('50%'), alignItems: 'flex-end' }}>
                                                            <Text style={[ c.light ]}>Total Pesanan: {this.format(item.total)}</Text>
                                                        </View>
                                                    </View>
                                                    <View style={{ width: wp('90%'), borderBottomWidth: 1, borderBottomColor: light }} />
                                                </View>
                                            )}
                                        />
                                        :
                                        <View style={{ width: '100%', alignItems: 'center' }}>
                                            <View style={{ width: wp('90%') }}>
                                                <Text style={[ c.light, { textAlign: 'center' }]}>Anda belum memiliki pesanan</Text>
                                            </View>
                                        </View>
                                    )
                                )}

                                {this.state.all_challenges === 'Beri Penilaian' && (
                                    (this.state.finished.length !== 0 ?
                                        <FlatList
                                            data={this.state.pesanan}
                                            keyExtractor={item => item.key}
                                            renderItem={({item}) => (
                                                <View style={{ width: '100%', alignItems: 'center', marginTop: wp('1%') }}>
                                                    <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
                                                        <View style={{ width: wp('45%'), flexDirection: 'row', alignItems: 'center' }}>
                                                            <View style={[ p.center, { width: wp('10%'), height: wp('10%'), borderRadius: wp('5%'), backgroundColor: light }]}>
                                                                <Image source={require('../asset/logo_apotek.png')} style={{ width: wp('6%'), height: wp('6%') }} />
                                                            </View>
                                                            <Text style={[ c.light, f._16, { marginLeft: wp('2%') }]}>Apotek Sehati</Text>
                                                        </View>
                                                        <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                                                            {item.status === 'selesai' && (
                                                                <Text style={[ c.light, f._16 ]}>Selesai</Text>
                                                            )}
                                                            {item.status === 'dinilai' && (
                                                                <Text style={[ c.light, f._16 ]}>Telah dinilai</Text>
                                                            )}
                                                        </View>
                                                    </View>
                                                    <TouchableOpacity onPress={() => this.props.navigation.push('DetailPayment')} style={{ width: wp('90%'), flexDirection: 'row', marginTop: wp('4%') }}>
                                                        <View style={{ width: wp('25%') }}>
                                                            <View style={{ width: '100%', height: wp('25%'), backgroundColor: light, borderRadius: wp('2%'), alignItems: 'center', justifyContent: 'center' }}>
                                                                <Image source={require('../asset/logo_combi_small.png')} style={{ width: wp('5%'), height: wp('5%'), position: 'absolute', left: wp('1%'), top: wp('1%'), zIndex: 1 }} />
                                                                <Image source={item.image} style={{ width: wp('16%'), height: wp('16%') }} />
                                                            </View>
                                                        </View>
                                                        <View style={{ width: wp('45%') }}>
                                                            <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold', marginLeft: wp('2%') }]}>{item.name}</Text>
                                                        </View>
                                                        <View style={{ width: wp('20%'), alignItems: 'flex-end' }}>
                                                            <Text style={[ c.light, { marginTop: wp('15%') }]}>x{item.lots}</Text>
                                                            <Text style={[ c.light ]}>{this.format(item.price)}</Text>
                                                        </View>
                                                    </TouchableOpacity>
                                                    <View style={{ width: '100%', borderBottomWidth: 1, borderBottomColor: light, marginTop: wp('2%') }} />

                                                    <View style={{ width: wp('90%'), marginTop: wp('2%'), marginBottom: wp('2%'), flexDirection: 'row', alignItems: 'center' }}>
                                                        <View style={{ width: wp('40%') }}>
                                                            <Text style={[ c.light ]}>1 produk</Text>
                                                        </View>
                                                        <View style={{ width: wp('50%'), alignItems: 'flex-end' }}>
                                                            <Text style={[ c.light ]}>Total Pesanan: {this.format(item.price)}</Text>
                                                        </View>
                                                    </View>
                                                    <View style={{ width: wp('90%'), borderBottomWidth: 1, borderBottomColor: light }} />

                                                    <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('2%'), marginBottom: wp('2%') }}>
                                                        <View style={{ width: wp('45%') }}>
                                                            {item.status === 'selesai' && (
                                                                <TouchableOpacity onPress={() => this.props.navigation.push('AddReview')}>
                                                                    <Text style={[ c.blue ]}>Berikan penilaian</Text>
                                                                </TouchableOpacity>
                                                            )}
                                                            {item.status === 'dinilai' && (
                                                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                                    <Image source={require('../asset/star.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: blue }} />
                                                                    <Image source={require('../asset/star.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: blue, marginLeft: wp('1%') }} />
                                                                    <Image source={require('../asset/star.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: blue, marginLeft: wp('1%') }} />
                                                                    <Image source={require('../asset/star.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: blue, marginLeft: wp('1%') }} />
                                                                    <Image source={require('../asset/star.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: blue, marginLeft: wp('1%') }} />
                                                                </View>
                                                            )}
                                                        </View>
                                                        <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                                                            <TouchableOpacity onPress={() => this.props.navigation.push('Cart')} style={{ width: wp('30%'), height: wp('10%'), backgroundColor: blue, alignItems: 'center', justifyContent: 'center', borderRadius: wp('1%') }}>
                                                                <Text style={[ c.light ]}>Beli Lagi</Text>
                                                            </TouchableOpacity>
                                                        </View>
                                                    </View>

                                                    <View style={{ width: '100%', height: wp('3%'), backgroundColor: '#181B22' }}></View>
                                                </View>
                                            )}
                                        />
                                        :
                                        <View style={{ width: '100%', alignItems: 'center' }}>
                                            <View style={{ width: wp('90%') }}>
                                                <Text style={[ c.light, { textAlign: 'center' }]}>Anda belum memiliki pesanan</Text>
                                            </View>
                                        </View>
                                    )
                                )}
                            </ScrollView>
                        </View>
                    )}

                    {this.state.hidden === true && this.state.hidden_touch === false && this.state.touch === 'akun' && (
                        <View style={{ width: '100%', alignItems: 'center', backgroundColor: '#181B22' }}>
                            {this.state.timeActive === true && (
                                <DateTimePicker
                                    mode='date'
                                    value={new Date()}
                                    maximumDate={new Date().setDate(new Date().getDate())}
                                    onChange={data => {
                                        // console.log(this.dateFix(new Date(data.nativeEvent.timestamp).getFullYear()+'-'+this.state.month[new Date(data.nativeEvent.timestamp).getMonth()].bulan+'-'+new Date(data.nativeEvent.timestamp).getDate()))
                                        if (data.type === 'dismissed') {
                                            this.setState({
                                                timeActive: false
                                            })
                                        } else {
                                            this.setState({
                                                date: this.dateFix(new Date(data.nativeEvent.timestamp).getFullYear()+'-'+this.state.month[new Date(data.nativeEvent.timestamp).getMonth()].bulan+'-'+new Date(data.nativeEvent.timestamp).getDate()),
                                                timeActive: false
                                            })
                                            this.componentDidMount()
                                        }
                                    }}
                                />
                            )}
                            <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('2%'), marginBottom: wp('2%') }}>
                                <TouchableOpacity onPress={() => this.setState({ hidden: false, hidden_touch: true })}>
                                    <Image source={require('../asset/left.png')} style={{ width: wp('8%'), height: wp('8%') }} />
                                </TouchableOpacity>
                                <Text style={[ c.light, f._16, { marginLeft: wp('2%') }]}>Pengaturan Akun</Text>
                            </View>
                            <View style={{ width: '100%', height: hp('15%'), flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                                <TouchableOpacity onPress={() => this.setState({ timeActive: true })} style={{ alignItems: 'center' }}>
                                    <FastImage source={require('../asset/calendar_icon.png')} style={{ width: wp('7%'), height: wp('7%') }} />
                                    <Text style={[ c.light, { marginTop: wp('1%') }]}>{this.state.age_now} Tahun</Text>
                                </TouchableOpacity>
                                <View style={{ width: wp('10%'), borderBottomWidth: 1, borderBottomColor: light, transform: [{ rotate: '90deg' }], marginLeft: wp('12%'), marginRight: wp('12%') }} />
                                <TouchableOpacity onPress={() => this.setState({ visible: true })} style={{ alignItems: 'center' }}>
                                    <FastImage source={require('../asset/gender_icon.png')} style={{ width: wp('7%'), height: wp('7%') }} />
                                    <Text style={[ c.light, { marginTop: wp('1%') }]}>{this.state.jenis_kelamin}</Text>
                                </TouchableOpacity>
                            </View>

                            <View style={{ width: '100%', alignItems: 'center', backgroundColor: '#121212' }}>
                                <View style={{ width: wp('90%'), marginTop: wp('4%'), marginBottom: wp('4%') }}>
                                    <Text style={[ c.blue ]}>Informasi Kontak</Text>
                                </View>
                            </View>

                            <View style={{ width: '100%', alignItems: 'center' }}>
                                <View style={{ width: wp('90%'), marginTop: wp('4%'), marginBottom: wp('6%') }}>
                                    <Text style={[ c.light ]}>Nomor Telepon</Text>
                                    <View style={{ width: wp('90%'), borderBottomWidth: 1, borderBottomColor: light }}>
                                        {this.state.edit === true ?
                                            <TextInput
                                                placeholder='Masukkan nomor telp Anda'
                                                placeholderTextColor={light}
                                                style={{ width: '100%', borderBottomWidth: 1, borderBottomColor: light, color: light }}
                                                value={this.state.nmr_telp}
                                                onChangeText={(text) => this.setState({ nmr_telp: text })}
                                            />
                                            :
                                            <Text style={[ c.light ]}>+6287882973962</Text>
                                        }
                                    </View>

                                    <Text style={[ c.light, { marginTop: wp('4%') }]}>Email</Text>
                                    <View style={{ width: wp('90%'), borderBottomWidth: 1, borderBottomColor: light }}>
                                        {this.state.edit === true ?
                                            <TextInput
                                                placeholder='Masukkan email Anda'
                                                placeholderTextColor={light}
                                                style={{ width: '100%', borderBottomWidth: 1, borderBottomColor: light, color: light }}
                                                value={this.state.email}
                                                onChangeText={(text) => this.setState({ email: text })}
                                            />
                                            :
                                            <Text style={[ c.light ]}>rizki@healthplus.co</Text>
                                        }
                                    </View>
                                </View>
                            </View>
                        </View>
                    )}

                </ScrollView>
            </View>
        )
    }
}