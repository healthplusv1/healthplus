import React from 'react';
import { Alert, FlatList, ActivityIndicator, StyleSheet, Text, View, Dimensions, TouchableOpacity, ScrollView, Image, ImageBackground, PixelRatio } from 'react-native';
import { TabView, SceneMap } from 'react-native-tab-view';
import { b, p, f, c } from '../utils/StyleHelper';
import { vs, xs } from '../utils/Responsive';
import { blue, light, grey, softBlue } from '../utils/Color';
import ImagePicker from 'react-native-image-picker';
import axios from 'axios'
import Station from './Station.js'
import { API_URL } from 'react-native-dotenv'


export default class Fun extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      ImageSource: null,
      updated: false,
      refreshing: false,
      loading: true
    }
  }

  componentDidMount(){
    axios.get(API_URL+'/main/get_all_funstation')
    .then(resp => {
      // alert(JSON.stringify(resp.data.data))
      this.setState({
        data: resp.data.data,
        refreshing: false,
        loading: false
      })
    })
    .catch(erro => {

    })
  }

  render() {
    if (this.state.loading) {
      return (
      <View style={[styles.container_loading, styles.horizontal_loading]}>
        <ActivityIndicator size="large" color="#0000ff" />
      </View>
      )
    }
    return (
      <View>
        <TouchableOpacity onPress={() => this.props.navigation.navigate('AddFun')} style={{ position: 'absolute', bottom: vs(20), right: vs(20), zIndex: 1 }}>
          <Image source={require('../asset/add.png')} style={{ width: xs(50), height: xs(50) }} />
        </TouchableOpacity>
        <View>
          <FlatList
            data={this.state.data}
            keyExtractor={item => item.id_fun_station}
            refreshing={this.state.refreshing}
            onRefresh={() => {
              this.setState({refreshing: true})
              this.componentDidMount()
            }}
            renderItem={({item}) => (
              <Station
                judul={item.judul}
                url_gambar={item.url_gambar}
                url_gambar_user={item.url_gambar_user}
                nama={item.nama}
                // up={item.up}
                id={item.id_fun_station}
              />
            )}
          />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  // loading centered screenn
  container_loading: {
    flex: 1,
    justifyContent: "center"
  },
  horizontal_loading: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },

  container: {
      // flex: 1,
      // justifyContent: 'center',
      // alignItems: 'center',
      // backgroundColor: '#FFF8E1'
    height:'100%' ,
    width :'100%',
    backgroundColor:'blue',
    justifyContent:'center',
    alignItems:'center'
  },
  sty_text:{
    fontSize: 20,
    color:'white',
    fontWeight:'bold'
  },

  ImageContainer: {
    borderRadius: xs(50),
    width: 50,
    height: 50,
    borderColor: '#FFFFFF',
    borderWidth: 1 / PixelRatio.get(),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    
  },
  // loading centered screenn
  container_loading: {
    flex: 1,
    justifyContent: "center"
  },
  horizontal_loading: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },
});