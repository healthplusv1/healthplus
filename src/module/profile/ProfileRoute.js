import { createStackNavigator} from 'react-navigation-stack'
// import FunPage from './screen/Fun'
import ProfilePage from './screen/Profile'

export const Profilestack = createStackNavigator({
    // Fun: {
    //     screen: FunPage,
    //     navigationOptions:{
    //         headerTitle:'Fun Station',
    //         headerTitleStyle:{
    //             textAlign:  'center',
    //             flex: 1,
    //             fontWeight: 'bold',
    //         }
    //     }
    // },
    Profile: {
        screen: ProfilePage,
        navigationOptions: {
            header: null
        }
    }
});