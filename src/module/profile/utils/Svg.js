import React, { memo } from 'react';
import { ms } from '../utils/Responsive';
import Camera from '../asset/icons/cam.svg'
import Image from '../asset/icons/img.svg'
import Link from '../asset/icons/link.svg'
import Share from '../asset/icons/share.svg'
import Cross from '../asset/icons/cross.svg'


const Svg = ({ icon, size = 30, fill, opacity, ...props }) => {
  const icons = {
    cam: Camera,
    img: Image,
    link: Link,
    share: Share,
    cross: Cross,
  };

  if (icons[icon] === undefined)
    return (
      <></>
    );

  const Icon = icons[icon]
  return (
    <Icon
      width={ms(size)}
      fill={fill || null}
      height={ms(size)}
      fillOpacity={opacity || 1}
      {...props}
    />
  );
}
export default memo(Svg)
