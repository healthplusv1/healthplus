import { createStackNavigator } from 'react-navigation-stack'
import JobsPage from './screen/Jobs'
import OrderPage from './screen/Order'

export const DriverStack = createStackNavigator({
    Jobs: {
        screen: JobsPage,
        navigationOptions: {
            header: null
        }
    },

    Order: {
        screen: OrderPage,
        navigationOptions: {
            header: null
        }
    }
})