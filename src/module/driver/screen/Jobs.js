import React, { Component } from 'react'
import { Text, View, TouchableOpacity, ScrollView, Image, ImageBackground, TextInput, FlatList } from 'react-native'
import { blue, light } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import LinearGradient from 'react-native-linear-gradient'
import FastImage from 'react-native-fast-image'
import Icon from 'react-native-vector-icons/Ionicons'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { API_URL } from 'react-native-dotenv'

export default class Jobs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ImageSource: null,
            nama: 'loading..',
            point: '....',
            loading: false,
            url_avatar: null,
            time: '',
            group: '',
            group1: '',
            id_login: null,
            joined_ongoing: null,
            joined_upcoming: null,
            popular_challenge: null,
            walk_challenge: null,
            run_challenge: null,
            push_up_challenge: null,
            active_tab_style: { width: '100%' },
            notif: '',
            edit: false,
            count: '',
            clicked: 0,
            jobs: [
                {jenis_jobs: 'Baru'},
                {jenis_jobs: 'Sedang dikirim'},
                {jenis_jobs: 'Selesai'}
            ],
            all_jobs: null,
            new_jobs: [
                {id: '1', jarak: '5 Km', waktu: '24:00', harga: '10000', nama_apotek: 'Apotek Sehati', jalan_apotek: 'Ruko Mataram, Jl. MT. Haryono Semarang', penerima: 'Michael Taner', jalan_penerima: 'Ds CoWork, Jl. K.H. Agus Salim Semarang'},
                {id: '2', jarak: '7 Km', waktu: '30:00', harga: '15000', nama_apotek: 'Apotek Sehati', jalan_apotek: 'Ruko Mataram, Jl. MT. Haryono Semarang', penerima: 'Michael Taner', jalan_penerima: 'Ds CoWork, Jl. K.H. Agus Salim Semarang'},
                {id: '3', jarak: '9 Km', waktu: '40:00', harga: '20000', nama_apotek: 'Apotek Sehati', jalan_apotek: 'Ruko Mataram, Jl. MT. Haryono Semarang', penerima: 'Michael Taner', jalan_penerima: 'Ds CoWork, Jl. K.H. Agus Salim Semarang'}
            ],
            sending: [
                {id: '1', jarak: '5 Km', waktu: '01:00', harga: '9000', status_keterlambatan: 'Terlambat', nama_apotek: 'Apotek Sehati', jalan_apotek: 'Ruko Mataram, Jl. MT. Haryono Semarang', penerima: 'Michael Taner', jalan_penerima: 'Ds CoWork, Jl. K.H. Agus Salim Semarang'},
            ],
            finished: [
                {id: '1', jarak: '5 Km', keterlambatan: '0', harga: '10000', nama_apotek: 'Apotek Sehati', jalan_apotek: 'Ruko Mataram, Jl. MT. Haryono Semarang', penerima: 'Michael Taner', jalan_penerima: 'Ds CoWork, Jl. K.H. Agus Salim Semarang', pendapatan: '10000', uang_apotek: '45000'},
            ]

        }
    }

    async componentDidMount() {
        await analytics().logEvent('view_jobs_page', {
            item: 'JOBS_SCREEN'
        })
        await analytics().setUserProperty('driver_sees_jobs_page', 'null')
    
        this._willBlurSubscription = this.props.navigation.addListener('willBlur', payload =>
            BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
        );
        // const name = await AsyncStorage.getItem('@nama')
        const id_login = await AsyncStorage.getItem('@id_login')
            if (id_login !== null) {
            this.setState({
                // name: name,
                id_login: id_login,
            })
        }
      
        axios.get(API_URL+'/main/shopping/cart', {params: {
            id_login: '118205008143923829902'
        }})
        .then(res => {
            this.setState({
                loading: false,
                count: res.data.data.length,
            })
        })
        .catch(e => {
            console.log(e)
        })
    }

    editBtn() {
        if (this.state.edit === false) {
            this.setState({ edit: true })
        } else {
            this.setState({ edit: false })
        }
    }

    async swicth(jenis_jobs, index) {
        this.setState({
            loading: false,
            clicked: index,
            all_jobs: jenis_jobs
        })
        // console.log(jenis_jobs, index)
        // const id_login = await AsyncStorage.getItem('@id_login')
        
		// axios.get(API_URL+'/main/filter_kompetisi', {params: {
            // 	id_login: id_login,
            // 	jenis_challenge: jenis_challenge
            // }})
            // .then(resp => {
			// alert(JSON.stringify(this.state.clicked))
		// })
	}

    format(number) {
        if (number) {
            var rupiah = "";
            var numberrev = number
                .toString()
                .split("")
                .reverse()
                .join("");
            for (var i = 0; i < numberrev.length; i++)
                if (i % 3 == 0) rupiah += numberrev.substr(i, 3) + ".";
            return (
                "Rp " +
                rupiah
                .split("", rupiah.length - 1)
                .reverse()
                .join("")
            );
        } else {
            return (
                "Rp "+number
            );
        }
    }

    render() {
        return (
            <ScrollView style={{ backgroundColor: '#181B22' }} showsVerticalScrollIndicator={false}>
                <LinearGradient
                    start={{x: 0.0, y: 0.25}} end={{x: 0.4, y: 2.0}}
                    locations={[0,0.4,0]}
                    colors={[ '#655353', '#000000', '#000000' ]}
                    style={{ width: '100%' }}
                >
                    <View style={{ width: '100%', alignItems: 'center' }}>
                        <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('4%') }}>
                            <View style={{ width: wp('45%') }}>
                                <Image source={require('../asset/healthPlus.png')} style={{ width: xs(160), height: xs(38) }} />
                            </View>
                            <View style={{ width: wp('45%'), flexDirection: 'row-reverse' }}>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('Drawer')}>
                                    <Image source={require('../asset/menu.png')} style={{ width: wp('7%'), height: wp('7%'), tintColor: light }} />
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('Notifikasi')} style={{ marginRight: wp('2%') }}>
                                    <Icon name="ios-notifications" size={25} color={light} />
                                    {this.state.notif === 'true' ?
                                        <View style={{ width: xs(10), height: xs(10), backgroundColor: 'orangered', borderRadius: xs(5),
                                        position: 'absolute', marginLeft: vs(6), marginTop: vs(1) }} />
                                        :
                                        null
                                    }
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.props.navigation.push('Cart')} style={{ marginRight: wp('2%') }}>
                                    {this.state.count !== 0 ?
                                    <View style={{ width: wp('3.5%'), height: wp('3.5%'), backgroundColor: 'orange', position: 'absolute', borderRadius: wp('3.5%'), zIndex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                        <Text style={[ c.light, f._8 ]}>{this.state.count}</Text>
                                    </View>
                                    :
                                    null
                                    }
                                    <Image source={require('../asset/addCart.png')} style={{ width: wp('7%'), height: wp('7%'), tintColor: light }} />
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.props.navigation.push('FrequentlyOrder')} style={{ marginRight: wp('2%') }}>
                                    <Image source={require('../asset/Frequentlyorder_icon.png')} style={{ width: wp('8%'), height: wp('7%'), tintColor: light }} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                    <View style={[ p.center, b.mt2 ]}>
                        <View style={[ p.row, b.mt2, b.mb3 ]}>
                            <TouchableOpacity onPress={() => this.props.navigation.push("Profile")} style={[ b.mt2, b.ml1 ]}>
                                <FastImage source={this.state.url_avatar === null ? require('../asset/dashboard-default-avatar.png') : { uri: API_URL+'/src/avatar/'+this.state.url_avatar }} style={{ width: xs(70), height: xs(70) }} />
                            </TouchableOpacity>
                            <ImageBackground source={require('../asset/group_1035.png')} style={[ b.ml3, { width: xs(250), height: xs(85) }]} imageStyle={[ b.roundedLow ]}>
                                <View style={ { width: '100%', height: xs(80), alignItems: 'center' }}>
                                    <View style={[ b.mt1, { width: xs(220), flexDirection: 'row', alignItems: 'center' }]}>
                                        <View style={{ width: xs(190) }}>
                                        {this.state.edit === true ?
                                            <TextInput
                                            placeholder='Masukkan Nama Anda'
                                            placeholderTextColor={light}
                                            style={{ width: xs(160), height: xs(35), borderWidth: 1, borderColor: light, borderRadius: xs(5), color: light }}
                                            value={this.state.nama}
                                            onChangeText={(text) => this.setState({ nama: text })}
                                            />
                                            :
                                            <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>{this.state.nama === 'loading..' ? 'Pratama' : this.state.nama}</Text>
                                        }
                                        </View>
                                        <View style={{ width: xs(30), alignItems: 'flex-end' }}>
                                        <TouchableOpacity onPress={() => this.editBtn()}>
                                            <Image source={this.state.edit === false ? require('../asset/edit.png') : require('../asset/checkmark.png')} style={{ width: xs(20), height: xs(20), tintColor: light }} />
                                        </TouchableOpacity>
                                        </View>
                                    </View>
                                    <View style={[ b.mt1, { width: '100%', height: xs(55), backgroundColor: blue, flexDirection: 'row' }]}>
                                        <View style={{ width: xs(95), height: '100%', justifyContent: 'center' }}>
                                            <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: wp('2%') }}>
                                                <FastImage source={require('../asset/point_icon.png')} style={{ width: xs(20), height: xs(20) }} />
                                                <Text style={[ c.light, b.ml1, { fontFamily: 'lineto-circular-pro-bold' }]} numberOfLines={1}>100</Text>
                                            </View>
                                            <Text style={[ f._12, c.light, { marginTop: vs(2), marginLeft: wp('2%') }]}>Total Poin Saya</Text>
                                        </View>
                                        <View style={{ width: xs(40), borderBottomWidth: 1, borderBottomColor: light, transform: [{ rotate: '90deg' }], marginLeft: wp('3%') }} />
                                        <View style={{ width: xs(105), height: '100%', justifyContent: 'center', marginLeft: wp('-11%') }}>
                                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                <FastImage source={require('../asset/dompetdriver_icon.png')} style={{ width: xs(20), height: xs(20) }} />
                                                <Text style={[ c.light, b.ml1, { fontFamily: 'lineto-circular-pro-bold' }]}>{this.format(100000)}</Text>
                                            </View>
                                            <Text style={[ f._12, c.light, { marginTop: vs(2) }]}>Total Saldo Saya</Text>
                                        </View>
                                        <View style={{ width: xs(40), alignItems: 'center', justifyContent: 'center' }}>
                                            <TouchableOpacity>
                                                <Image source={require('../asset/plus1.png')} style={{ width: xs(30), height: xs(30), tintColor: light }} />
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            </ImageBackground>
                        </View>
                    </View>
                </LinearGradient>
                <View style={{ width: '100%', alignItems: 'center', borderBottomWidth: 2, borderBottomColor: blue }}>
                    <View style={{ width: wp('90%'), marginTop: wp('4%'), marginBottom: wp('2%') }}>
                        <Text style={[ c.light, f._18, { fontFamily: 'lineto-circular-pro-bold' }]}>Jobs</Text>
                    </View>
                </View>
                <View style={{ width: '100%', marginTop: wp('4%'), alignItems: 'center' }}>
                    <FlatList
                        horizontal={true}
                        data={this.state.jobs}
                        keyExtractor={item => item.jenis_jobs}
                        renderItem={({item, index}) => (
                            (this.state.clicked === index ?
                                <TouchableOpacity onPress={() => this.swicth(item.jenis_jobs, index)} style={[ b.roundedLow, { backgroundColor: blue, marginRight: wp('4%') }]}>
                                    <Text style={[ b.mt1, b.mb1, c.light, { marginRight: wp('5%'), marginLeft: wp('5%') }]}>{item.jenis_jobs}</Text>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity onPress={() => this.swicth(item.jenis_jobs, index)} style={[ b.roundedLow, { borderWidth: 1, borderColor: blue, marginRight: wp('4%') }]}>
                                    <Text style={[ b.mt1, b.mb1, c.light, { marginRight: wp('5%'), marginLeft: wp('5%') }]}>{item.jenis_jobs}</Text>
                                </TouchableOpacity>
                            )
                        )}
                    />

                    {this.state.clicked === 0 && (
                        <FlatList
                            ListHeaderComponent={<View style={{ marginTop: wp('4%') }} />}
                            data={this.state.new_jobs}
                            keyExtractor={item => item.id}
                            renderItem={({ item }) => (
                                <View style={{ width: wp('90%'), alignItems: 'center', borderWidth: 1.5, borderColor: blue, marginBottom: wp('4%'), borderRadius: wp('2%') }}>
                                    <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center', marginTop: wp('1%'), marginBottom: wp('1%') }}>
                                        <View style={{ width: wp('30%') }}>
                                            <Text style={[ c.light, f._18, { fontFamily: 'lineto-circular-pro-bold', marginLeft: wp('2%') }]}>{item.jarak}</Text>
                                        </View>
                                        <View style={{ width: wp('30%'), alignItems: 'center' }}>
                                            <View style={{ backgroundColor: 'darkorange', flexDirection: 'row', alignItems: 'center', borderRadius: wp('1%') }}>
                                                <Text style={[ c.light, { marginLeft: wp('1%') }]}>Waktu </Text>
                                                <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold', marginRight: wp('1%') }]}>{item.waktu}</Text>
                                            </View>
                                        </View>
                                        <View style={{ width: wp('30%'), alignItems: 'flex-end' }}>
                                            <Text style={{ color: 'darkorange', fontFamily: 'lineto-circular-pro-bold', marginRight: wp('2%') }}>{this.format(item.harga)}</Text>
                                        </View>
                                    </View>
                                    <View style={{ width: '100%', borderBottomWidth: 1.5, borderBottomColor: blue }} />
                                    <View style={{ width: wp('85%'), marginTop: wp('4%'), marginBottom: wp('4%') }}>
                                        <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center' }}>
                                            <View style={{ width: wp('15%') }}>
                                                <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center' }}>
                                                    <Image source={require('../asset/icon_location.png')} style={{ width: xs(20), height: xs(27) }} />
                                                    <View style={{ width: xs(3), height: xs(3), borderRadius: xs(10), backgroundColor: light, marginTop: wp('5%') }} />
                                                    <View style={{ width: xs(3), height: xs(3), borderRadius: xs(10), backgroundColor: light, marginTop: wp('5%'), marginLeft: wp('2%') }} />
                                                    <View style={{ width: xs(3), height: xs(3), borderRadius: xs(10), backgroundColor: light, marginTop: wp('5%'), marginLeft: wp('2%') }} />
                                                </View>
                                            </View>
                                            <View style={{ width: wp('70%') }}>
                                                <Text style={[ c.light, f.bold, f._16 ]}>{item.nama_apotek}</Text>
                                                <Text style={[ c.light, f._12 ]} numberOfLines={1}>{item.jalan_apotek}</Text>
                                            </View>
                                        </View>

                                        <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center', marginTop: wp('2%') }}>
                                            <View style={{ width: wp('15%') }}>
                                                <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center' }}>
                                                    <View style={{ width: xs(3), height: xs(3), borderRadius: xs(10), backgroundColor: light, marginTop: wp('5%') }} />
                                                    <View style={{ width: xs(3), height: xs(3), borderRadius: xs(10), backgroundColor: light, marginTop: wp('5%'), marginLeft: wp('2%') }} />
                                                    <View style={{ width: xs(3), height: xs(3), borderRadius: xs(10), backgroundColor: light, marginTop: wp('5%'), marginLeft: wp('2%') }} />
                                                    <Image source={require('../asset/icon_location.png')} style={{ width: xs(20), height: xs(27) }} />
                                                </View>
                                            </View>
                                            <View style={{ width: wp('70%') }}>
                                                <Text style={[ c.light, f.bold, f._16 ]}>{item.penerima}</Text>
                                                <Text style={[ c.light, f._12 ]} numberOfLines={1}>{item.jalan_penerima}</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ width: '100%', borderBottomWidth: 1.5, borderBottomColor: blue }} />
                                    <View style={{ width: '100%', alignItems: 'flex-end' }}>
                                        <TouchableOpacity onPress={() => this.props.navigation.push('Order')} style={{ width: wp('30%'), height: hp('5%'), flexDirection: 'row', alignItems: 'center', justifyContent: 'center', backgroundColor: blue, borderRadius: wp('2%'), marginRight: wp('2%'), marginTop: wp('3%'), marginBottom: wp('3%') }}>
                                            <Text style={[ c.light ]}>Lihat Pesanan</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            )}
                        />
                    )}

                    {this.state.all_jobs === 'Sedang dikirim' && (
                        <FlatList
                            ListHeaderComponent={<View style={{ marginTop: wp('4%') }} />}
                            data={this.state.sending}
                            keyExtractor={item => item.id}
                            renderItem={({ item }) => (
                                <View style={{ width: wp('90%'), alignItems: 'center', borderWidth: 1.5, borderColor: blue, marginBottom: wp('4%'), borderRadius: wp('2%') }}>
                                    <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center', marginTop: wp('1%'), marginBottom: wp('1%') }}>
                                        <View style={{ width: wp('30%') }}>
                                            <Text style={[ c.light, f._18, { fontFamily: 'lineto-circular-pro-bold', marginLeft: wp('2%') }]}>{item.jarak}</Text>
                                        </View>
                                        <View style={{ width: wp('30%'), alignItems: 'center' }}>
                                            <View style={{ backgroundColor: 'darkorange', flexDirection: 'row', alignItems: 'center', borderRadius: wp('1%') }}>
                                                <Text style={[ c.light, { marginLeft: wp('1%') }]}>Waktu </Text>
                                                <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold', marginRight: wp('1%') }]}>-{item.waktu}</Text>
                                            </View>
                                        </View>
                                        <View style={{ width: wp('30%'), alignItems: 'flex-end' }}>
                                            <Text style={{ color: 'darkorange', fontFamily: 'lineto-circular-pro-bold', marginRight: wp('2%') }}>{this.format(item.harga)}</Text>
                                            <Text style={[ f._10, { color: 'darkorange', fontFamily: 'lineto-circular-pro-bold', marginRight: wp('2%') }]}>{item.status_keterlambatan}</Text>
                                        </View>
                                    </View>
                                    <View style={{ width: '100%', borderBottomWidth: 1.5, borderBottomColor: blue }} />
                                    <View style={{ width: wp('85%'), marginTop: wp('4%'), marginBottom: wp('4%') }}>
                                        <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center' }}>
                                            <View style={{ width: wp('15%') }}>
                                                <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center' }}>
                                                    <Image source={require('../asset/icon_location.png')} style={{ width: xs(20), height: xs(27) }} />
                                                    <View style={{ width: xs(3), height: xs(3), borderRadius: xs(10), backgroundColor: light, marginTop: wp('5%') }} />
                                                    <View style={{ width: xs(3), height: xs(3), borderRadius: xs(10), backgroundColor: light, marginTop: wp('5%'), marginLeft: wp('2%') }} />
                                                    <View style={{ width: xs(3), height: xs(3), borderRadius: xs(10), backgroundColor: light, marginTop: wp('5%'), marginLeft: wp('2%') }} />
                                                </View>
                                            </View>
                                            <View style={{ width: wp('70%') }}>
                                                <Text style={[ c.light, f.bold, f._16 ]}>{item.nama_apotek}</Text>
                                                <Text style={[ c.light, f._12 ]} numberOfLines={1}>{item.jalan_apotek}</Text>
                                            </View>
                                        </View>

                                        <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center', marginTop: wp('2%') }}>
                                            <View style={{ width: wp('15%') }}>
                                                <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center' }}>
                                                    <View style={{ width: xs(3), height: xs(3), borderRadius: xs(10), backgroundColor: light, marginTop: wp('5%') }} />
                                                    <View style={{ width: xs(3), height: xs(3), borderRadius: xs(10), backgroundColor: light, marginTop: wp('5%'), marginLeft: wp('2%') }} />
                                                    <View style={{ width: xs(3), height: xs(3), borderRadius: xs(10), backgroundColor: light, marginTop: wp('5%'), marginLeft: wp('2%') }} />
                                                    <Image source={require('../asset/icon_location.png')} style={{ width: xs(20), height: xs(27) }} />
                                                </View>
                                            </View>
                                            <View style={{ width: wp('70%') }}>
                                                <Text style={[ c.light, f.bold, f._16 ]}>{item.penerima}</Text>
                                                <Text style={[ c.light, f._12 ]} numberOfLines={1}>{item.jalan_penerima}</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ width: '100%', borderBottomWidth: 1.5, borderBottomColor: blue }} />
                                    <View style={{ width: '100%', alignItems: 'flex-end' }}>
                                        <TouchableOpacity onPress={() => this.props.navigation.push('Order')} style={{ width: wp('30%'), height: hp('5%'), flexDirection: 'row', alignItems: 'center', justifyContent: 'center', backgroundColor: blue, borderRadius: wp('2%'), marginRight: wp('2%'), marginTop: wp('3%'), marginBottom: wp('3%') }}>
                                            <Text style={[ c.light ]}>Lihat Pesanan</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            )}
                        />
                    )}

                    {this.state.all_jobs === 'Selesai' && (
                        <FlatList
                            ListHeaderComponent={<View style={{ marginTop: wp('4%') }} />}
                            data={this.state.finished}
                            keyExtractor={item => item.id}
                            renderItem={({ item }) => (
                                <View style={{ width: wp('90%'), alignItems: 'center', borderWidth: 1.5, borderColor: blue, marginBottom: wp('4%'), borderRadius: wp('2%') }}>
                                    <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center', marginTop: wp('1%'), marginBottom: wp('1%') }}>
                                        <View style={{ width: wp('30%') }}>
                                            <Text style={[ c.light, f._18, { fontFamily: 'lineto-circular-pro-bold', marginLeft: wp('2%') }]}>{item.jarak}</Text>
                                        </View>
                                        <View style={{ width: wp('30%'), alignItems: 'center' }}></View>
                                        <View style={{ width: wp('30%'), alignItems: 'flex-end' }}>
                                            <Text style={{ color: light, fontFamily: 'lineto-circular-pro-bold', marginRight: wp('2%') }}>{this.format(item.harga)}</Text>
                                        </View>
                                    </View>
                                    <View style={{ width: '100%', borderBottomWidth: 1.5, borderBottomColor: blue }} />
                                    <View style={{ width: wp('85%'), marginTop: wp('4%'), marginBottom: wp('4%') }}>
                                        <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center' }}>
                                            <View style={{ width: wp('20%') }}></View>
                                            <View style={{ width: wp('65%') }}>
                                                <Text style={[ c.light, f.bold, f._16 ]}>{item.nama_apotek}</Text>
                                                <Text style={[ c.light, f._12 ]} numberOfLines={1}>{item.jalan_apotek}</Text>
                                            </View>
                                        </View>

                                        <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center', marginTop: wp('2%') }}>
                                            <View style={{ width: wp('20%') }}></View>
                                            <View style={{ width: wp('65%') }}>
                                                <Text style={[ c.light, f.bold, f._16 ]}>{item.penerima}</Text>
                                                <Text style={[ c.light, f._12 ]} numberOfLines={1}>{item.jalan_penerima}</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ width: wp('85%'), flexDirection: 'row', alignItems: 'center', marginBottom: wp('4%') }}>
                                        <View style={{ width: wp('10%') }}></View>
                                        <View style={{ width: wp('37.5%') }}>
                                            <Text style={[ c.light ]}>Pendapatan</Text>
                                            <Text style={[ c.light ]}>Uang apotek</Text>
                                            <Text style={[ c.light ]}>Keterlambatan</Text>
                                        </View>
                                        <View style={{ width: wp('37.5%'), alignItems: 'flex-end' }}>
                                            <Text style={[ c.light ]}>{this.format(item.pendapatan)}</Text>
                                            <Text style={[ c.light ]}>{this.format(item.uang_apotek)}</Text>
                                            <Text style={[ c.light ]}>{this.format(item.keterlambatan)}</Text>
                                        </View>
                                    </View>
                                </View>
                            )}
                        />
                    )}
                </View>
            </ScrollView>
        )
    }
}
