import React, { Component } from 'react'
import { Text, View, TouchableOpacity, FlatList, ScrollView } from 'react-native'
import { blue, light } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'

export default class Order extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pesanan: [
                {id: '1', name: 'OBH Combi Flu dan Batuk', img: require('../asset/10000289_1.jpg'), jumlah: '1', harga: '14500'},
                {id: '2', name: 'OBH Combi Flu dan Batuk', img: require('../asset/10000289_1.jpg'), jumlah: '1', harga: '14500'},
                {id: '3', name: 'OBH Combi Flu dan Batuk', img: require('../asset/10000289_1.jpg'), jumlah: '1', harga: '14500'}
            ]
        }
    }

    format(number) {
        if (number) {
            var rupiah = "";
            var numberrev = number
                .toString()
                .split("")
                .reverse()
                .join("");
            for (var i = 0; i < numberrev.length; i++)
                if (i % 3 == 0) rupiah += numberrev.substr(i, 3) + ".";
            return (
                "Rp " +
                rupiah
                .split("", rupiah.length - 1)
                .reverse()
                .join("")
            );
        } else {
            return (
                "Rp "+number
            );
        }
    }

    render() {
        const { pesanan } = this.state;
        let totalQuantity = 0;
        let totalPrice = 0;
        pesanan.forEach((item) => {
            totalQuantity += item.jumlah;
            totalPrice += item.jumlah * item.harga;
        })
        return (
            <ScrollView style={{ backgroundColor: '#121212' }} showsVerticalScrollIndicator={false}>
                <View style={{ width: '100%', alignItems: 'center' }}>
                    <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('4%'), marginBottom: wp('4%') }}>
                        <View style={{ width: wp('20%') }}>
                            <TouchableOpacity onPress={() => this.props.navigation.pop()}>
                                <FastImage source={require('../asset/left.png')} style={{ width: wp('8%'), height: wp('8%') }} />
                            </TouchableOpacity>
                        </View>
                        <View style={{ width: wp('50%'), alignItems: 'center' }}>
                            <Text style={[ c.light, f._18, { fontFamily: 'lineto-circular-pro-bold' }]}>Pesanan</Text>
                        </View>
                        <View style={{ width: wp('20%') }}></View>
                    </View>

                    <View style={{ width: wp('90%'), height: hp('8%'), backgroundColor: 'darkorange', flexDirection: 'row', alignItems: 'center' }}>
                        <FastImage source={require('../asset/dompetdriver_icon.png')} style={{ width: wp('8%'), height: wp('8%'), marginLeft: wp('4%') }} />
                        <View style={{ width: wp('70%'), marginLeft: wp('4%') }}>
                            <Text style={[ c.light, f._18, { fontFamily: 'lineto-circular-pro-bold' }]} numberOfLines={1}>{this.format(10000)}</Text>
                        </View>
                    </View>

                    <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('4%') }}>
                        <View style={{ width: wp('15%') }}>
                            <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center' }}>
                                <FastImage source={require('../asset/icon_location.png')} style={{ width: xs(20), height: xs(27) }} />
                                <View style={{ width: xs(3), height: xs(3), borderRadius: xs(10), backgroundColor: light, marginTop: wp('5%') }} />
                                <View style={{ width: xs(3), height: xs(3), borderRadius: xs(10), backgroundColor: light, marginTop: wp('5%'), marginLeft: wp('2%') }} />
                                <View style={{ width: xs(3), height: xs(3), borderRadius: xs(10), backgroundColor: light, marginTop: wp('5%'), marginLeft: wp('2%') }} />
                            </View>
                        </View>
                        <View style={{ width: wp('75%') }}>
                            <Text style={[ c.light, f.bold, f._16 ]}>Apotek Sehati</Text>
                            <Text style={[ c.light, f._12 ]} numberOfLines={1}>Ruko Mataram, Jl. MT Haryono Semarang</Text>
                        </View>
                    </View>

                    <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('3%') }}>
                        <View style={{ width: wp('15%') }}>
                            <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center' }}>
                                <View style={{ width: xs(3), height: xs(3), borderRadius: xs(10), backgroundColor: light, marginTop: wp('5%') }} />
                                <View style={{ width: xs(3), height: xs(3), borderRadius: xs(10), backgroundColor: light, marginTop: wp('5%'), marginLeft: wp('2%') }} />
                                <View style={{ width: xs(3), height: xs(3), borderRadius: xs(10), backgroundColor: light, marginTop: wp('5%'), marginLeft: wp('2%') }} />
                                <FastImage source={require('../asset/icon_location.png')} style={{ width: xs(20), height: xs(27) }} />
                            </View>
                        </View>
                        <View style={{ width: wp('75%') }}>
                            <Text style={[ c.light, f.bold, f._16 ]}>Michael Taner</Text>
                            <Text style={[ c.light, f._12 ]} numberOfLines={1}>DS CoWork 202, Jl. Agus Salim Semarang</Text>
                        </View>
                    </View>

                    <View style={{ width: wp('90%'), borderBottomWidth: 2, borderBottomColor: blue, marginTop: wp('4%') }} />

                    <View style={{ width: wp('90%'), alignItems: 'flex-end', marginTop: wp('2%') }}>
                        <TouchableOpacity style={{ width: wp('45%'), height: hp('5%'), backgroundColor: blue, borderRadius: wp('5%'), flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                            <FastImage source={require('../asset/map_icon.png')} style={{ width: xs(21), height: xs(18) }} />
                            <Text style={[ c.light, { marginLeft: wp('2%') }]}>Lihat lokasi antar</Text>
                        </TouchableOpacity>
                    </View>

                    <FlatList
                        ListHeaderComponent={<View style={{ marginTop: wp('4%') }} />}
                        data={this.state.pesanan}
                        keyExtractor={item => item.id}
                        renderItem={({ item }) => (
                            <>
                                <View style={{ width: wp('90%'), flexDirection: 'row', marginBottom: wp('2%') }}>
                                    <View style={{ width: wp('20%'), height: wp('20%'), backgroundColor: light, alignItems: 'center', justifyContent: 'center', borderRadius: wp('1%') }}>
                                        <FastImage source={item.img} style={{ width: wp('15%'), height: wp('15%') }} />
                                    </View>
                                    <View style={{ width: wp('50%') }}>
                                        <Text style={[ c.light, { marginLeft: wp('2%'), fontFamily: 'lineto-circular-pro-bold' }]}>{item.name}</Text>
                                    </View>
                                    <View style={{ width: wp('20%'), alignItems: 'flex-end' }}>
                                        <View style={{ width: wp('8%'), height: wp('8%'), borderRadius: wp('8%'), backgroundColor: 'darkorange', alignItems: 'center', justifyContent: 'center' }}>
                                            <Text style={[ c.light ]}>{item.jumlah}</Text>
                                        </View>
                                    </View>
                                </View>
                                <View style={{ width: wp('90%'), borderBottomWidth: 1, borderBottomColor: blue, marginBottom: wp('2%') }} />
                            </>
                        )}
                    />
                    <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('2%') }}>
                        <View style={{ width: wp('50%') }}>
                            <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>Jumlah</Text>
                        </View>
                        <View style={{ width: wp('40%'), alignItems: 'flex-end' }}>
                            <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>{totalQuantity}</Text>
                        </View>
                    </View>
                    <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('2%') }}>
                        <View style={{ width: wp('50%') }}>
                            <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>Total Harga Pesanan</Text>
                        </View>
                        <View style={{ width: wp('40%'), alignItems: 'flex-end' }}>
                            <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>{this.format(totalPrice)}</Text>
                        </View>
                    </View>

                    <TouchableOpacity style={{ width: wp('90%'), height: hp('7%'), borderRadius: wp('2%'), backgroundColor: blue, marginTop: wp('4%'), alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={[ c.light, f._18, { fontFamily: 'lineto-circular-pro-bold' }]}>Top Up</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.props.navigation.pop()} style={{ width: wp('90%'), height: hp('7%'), borderRadius: wp('2%'), backgroundColor: light, marginTop: wp('4%'), marginBottom: wp('4%'), alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={[ c.blue, f._18, { fontFamily: 'lineto-circular-pro-bold' }]}>Kembali</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        )
    }
}
