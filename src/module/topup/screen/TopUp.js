import React from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView, ActivityIndicator, StyleSheet, ToastAndroid, FlatList, RefreshControl } from 'react-native'
import { Tabs, Tab } from 'native-base'
import { xs, vs } from '../utils/Responsive'
import { p, b, c, f } from '../utils/StyleHelper'
import { blue, light } from '../utils/Color'
import { widthPercentageToDP as wp } from 'react-native-responsive-screen'
import Icon from 'react-native-vector-icons/Ionicons'
import { Picker as SelectPicker } from '@react-native-picker/picker'
import Dialog, { DialogContent, DialogTitle } from 'react-native-popup-dialog'
import analytics from '@react-native-firebase/analytics'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { API_URL } from 'react-native-dotenv'

export default class TopUp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            history: undefined,
            list_topup: undefined,
            selectedItem: undefined,
            payment_method: undefined,
            price: 0,
            visible: false,
            paymentURL: '',
            merchantId: undefined,
            poin: ''
        }
    }

    async componentDidMount() {
        setInterval( async () => {
            await analytics().logEvent('HealthPlus', {
                item: 'it worked'
            })
            await analytics().setAnalyticsCollectionEnabled(true)
        }, 600000);

        this.setState({ loading: true })

        const id_login = await AsyncStorage.getItem('@id_login')

        axios.get(API_URL+'/main/get_user_by_id_login', {params: {
            id_login: id_login
        }})
        .then(res => {
            // console.log(res.data.data)
            this.setState({ loading: false })
        })
        .catch(error => {
            console.log('get_user => '+error)
        })

        axios.get(API_URL+'/main/show_list_topup')
        .then(resp => {
            // console.log(resp.data.data)
            this.setState({
                list_topup: resp.data.data,
                loading: false
            })
        })
        .catch(err => {
            console.log('list_topup => '+err)
        })
    
        axios.get(API_URL+'/main/get_history', {params: {
            id_login: id_login
        }})
        .then(response => {
            // console.log(response.data.data[0].merchantOrderId)
            this.setState({
                loading: false,
                history: response.data.data,
                merchantId: response.data.data[0].merchantOrderId
            })
            if (this.state.merchantId === undefined) {
                console.log('Merchant ID tidak ditemukan')
            } else {
                axios.get(API_URL+'/main/check_status', {params: {
                    merchantOrderId: response.data.data[0].merchantOrderId,
                    id_login: id_login,
                }})
                .then(result => {
                    // console.log(result.data[1])
                    this.setState({ loading: false })
                    if (result.data[1]) {
                        console.log('Permintaan tidak valid')
                    } else {
                        console.log(result.data)
                    }
                })
            }
        })
        .catch(e => {
            console.log('get_history => '+e)
        })

    }

    async selectingOperator(itemValue) {
        // console.log(itemValue)
        let value = itemValue.split(' | ')
        await this.setState({ selectedItem: itemValue, price: value[0], poin: value[1] })
        console.log(this.state.selectedItem)
    }

    selectingPayment(payment_method) {
        if(this.state.payment_method === payment_method){
            console.log(payment_method)
            return [ styles.bankItemSelected, b.rounded, b.mb2, p.row ];
        }else{
            return [ styles.bankItem, b.rounded, b.mb2, p.row ];
        }
    }
    
    async request() {
        this.setState({ loading: true })
        const id_login = await AsyncStorage.getItem('@id_login')
        axios.get(API_URL+'/main/request', {params: {
            id_login: id_login,
            poin: this.state.poin,
            price_point: this.state.price,
            payment_method: this.state.payment_method
        }})
        .then(resp => {
            // console.log('Request => '+resp.data.paymentUrl)
            this.setState({ loading: false })
            if (resp.data.Message === 'Minimum Payment 10000') {
                ToastAndroid.show(resp.data.Message, ToastAndroid.SHORT)
            } else {
                this.setState({ visible: true, paymentURL: resp.data.paymentUrl })
            }
        })
        .catch(err => {
            console.log('Request Error => '+err)
            ToastAndroid.show(err, ToastAndroid.SHORT)
        })
    }

    onRefresh() {
        this.setState({ loading: true })
        this.componentDidMount()
    }

    convertToRupiah(number) {
        if (number) {
          var rupiah = "";
          var numberrev = number
            .toString()
            .split("")
            .reverse()
            .join("");
          for (var i = 0; i < numberrev.length; i++)
            if (i % 3 == 0) rupiah += numberrev.substr(i, 3) + ".";
          return (
            "Rp " +
            rupiah
              .split("", rupiah.length - 1)
              .reverse()
              .join("")
          );
        } else {
            return (
                "Rp "+number
            );
        }
    }

    render() {
        return (
            <>
                <Dialog
                    dialogTitle={<DialogTitle title="Notification" />}
                    visible={this.state.visible}
                    onTouchOutside={() => this.setState({visible: false})}
                >
                    <DialogContent style={{ width: xs(300), alignItems: 'center' }}>
                        <View style={[ b.mt2, { width: xs(290) }]}>
                            <Text style={[ p.textCenter ]}>You have successfully requested, please proceed to payment</Text>
                            <Text style={[ p.textCenter ]}>{this.state.paymentURL}</Text>
                        </View>
                        <View style={[ b.mt2, b.mb2, p.row ]}>
                            <TouchableOpacity
                                onPress={() => {
                                    this.props.navigation.push('Pembayaran', {paymentURL: this.state.paymentURL})
                                    this.setState({ visible: false })
                                }}
                                style={[ p.center, b.rounded, { width: xs(100), backgroundColor: blue }]}
                            >
                                <Text style={[ c.light, b.mt2, b.mb2, { fontFamily: 'lineto-circular-pro-bold' }]}>Pay Now</Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                onPress={() => {
                                    this.setState({ visible: false })
                                }}
                                style={[ p.center, b.rounded, b.ml2, { width: xs(100), backgroundColor: blue }]}
                            >
                                <Text style={[ c.light, b.mt2, b.mb2, { fontFamily: 'lineto-circular-pro-bold' }]}>Pay Later</Text>
                            </TouchableOpacity>
                        </View>
                    </DialogContent>
                </Dialog>
                <Tabs tabBarUnderlineStyle={{ borderBottomWidth: 4, borderBottomColor: blue }}>
                    <Tab heading='TopUp' tabStyle={{ backgroundColor: '#000000' }} textStyle={{ color: light }}
                        activeTabStyle={{ backgroundColor: '#181B22' }}
                        activeTextStyle={{ color: blue, fontFamily: 'lineto-circular-pro-bold' }}>
                        <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#000' }} refreshControl={<RefreshControl refreshing={this.state.loading} onRefresh={() => this.onRefresh()} />}>
                            <View style={[ p.center, { width: '100%' }]}>
                                <View style={[ b.mt4, { width: xs(300) }]}>
                                    <Text style={[ c.light, f._18, { fontFamily: 'lineto-circular-pro-bold' }]}>Select Poin</Text>
                                    <View style={[ b.bordered, b.rounded, b.mt2, { width: '100%', height: xs(50), backgroundColor: 'white' }]}>
                                        <SelectPicker
                                            mode={'dropdown'}
                                            selectedValue={this.state.selectedItem}
                                            onValueChange={(itemValue) => this.selectingOperator(itemValue)}
                                        >
                                            <SelectPicker.Item value={-1} label='Choose Poin ...' />
                                            {this.state.list_topup !== undefined ?
                                                this.state.list_topup.map((item) => <SelectPicker.Item label={item.poin+' Poin'} value={item.price_point+' | '+item.poin} /> ) : null
                                            }
                                        </SelectPicker>
                                    </View>

                                    <Text style={[ c.light, f._18, b.mt4, { fontFamily: 'lineto-circular-pro-bold' }]}>Select Payment Method</Text>
                                    <View style={[ b.mt2 ]} />
                                    <TouchableOpacity style={this.selectingPayment('VA')} onPress={() => this.setState({ payment_method: 'VA' })}>
                                        <View style={[ p.center, { width: xs(100) }]}>
                                            <Image source={require('../asset/maybank.png')} style={{ width: xs(80), height: xs(20) }} />
                                        </View>
                                        <View style={[ b.ml2, { width: xs(130), justifyContent: 'center' }]}>
                                            <Text>Maybank</Text>
                                        </View>
                                        {this.state.payment_method === 'VA' ?
                                            <View style={[ b.ml2, p.center, { width: xs(50) }]}>
                                                <Image source={require('../asset/checklist.png')} style={{ width: xs(15), height: xs(15) }} />
                                            </View>
                                            : null
                                        }
                                    </TouchableOpacity>
                                    <TouchableOpacity style={this.selectingPayment('BT')} onPress={() => this.setState({ payment_method: 'BT' })}>
                                        <View style={[ p.center, { width: xs(100) }]}>
                                            <Image source={require('../asset/permata_bank.png')} style={{ width: xs(80), height: xs(25) }} />
                                        </View>
                                        <View style={[ b.ml2, { width: xs(130), justifyContent: 'center' }]}>
                                            <Text>Permata Bank</Text>
                                        </View>
                                        {this.state.payment_method === 'BT' ?
                                            <View style={[ b.ml2, p.center, { width: xs(50) }]}>
                                                <Image source={require('../asset/checklist.png')} style={{ width: xs(15), height: xs(15) }} />
                                            </View>
                                            : null
                                        }
                                    </TouchableOpacity>
                                    <TouchableOpacity style={this.selectingPayment('B1')} onPress={() => this.setState({ payment_method: 'B1' })}>
                                        <View style={[ p.center, { width: xs(100) }]}>
                                            <Image source={require('../asset/cimb3x.png')} style={{ width: xs(80), height: xs(20) }} />
                                        </View>
                                        <View style={[ b.ml2, { width: xs(130), justifyContent: 'center' }]}>
                                            <Text>CIMB Niaga</Text>
                                        </View>
                                        {this.state.payment_method === 'B1' ?
                                            <View style={[ b.ml2, p.center, { width: xs(50) }]}>
                                                <Image source={require('../asset/checklist.png')} style={{ width: xs(15), height: xs(15) }} />
                                            </View>
                                            : null
                                        }
                                    </TouchableOpacity>
                                    <TouchableOpacity style={this.selectingPayment('A1')} onPress={() => this.setState({ payment_method: 'A1' })}>
                                        <View style={[ p.center, { width: xs(100) }]}>
                                            <Image source={require('../asset/atm_bersama.png')} style={{ width: xs(80), height: xs(20) }} />
                                        </View>
                                        <View style={[ b.ml2, { width: xs(130), justifyContent: 'center' }]}>
                                            <Text>ATM Bersama</Text>
                                        </View>
                                        {this.state.payment_method === 'A1' ?
                                            <View style={[ b.ml2, p.center, { width: xs(50) }]}>
                                                <Image source={require('../asset/checklist.png')} style={{ width: xs(15), height: xs(15) }} />
                                            </View>
                                            : null
                                        }
                                    </TouchableOpacity>
                                    <TouchableOpacity style={this.selectingPayment('I1')} onPress={() => this.setState({ payment_method: 'I1' })}>
                                        <View style={[ p.center, { width: xs(100) }]}>
                                            <Image source={require('../asset/bni.png')} style={{ width: xs(80), height: xs(20) }} />
                                        </View>
                                        <View style={[ b.ml2, { width: xs(130), justifyContent: 'center' }]}>
                                            <Text>BNI</Text>
                                        </View>
                                        {this.state.payment_method === 'I1' ?
                                            <View style={[ b.ml2, p.center, { width: xs(50) }]}>
                                                <Image source={require('../asset/checklist.png')} style={{ width: xs(15), height: xs(15) }} />
                                            </View>
                                            : null
                                        }
                                    </TouchableOpacity>
                                    <TouchableOpacity style={this.selectingPayment('M1')} onPress={() => this.setState({ payment_method: 'M1' })}>
                                        <View style={[ p.center, { width: xs(100) }]}>
                                            <Image source={require('../asset/mandiri.png')} style={{ width: xs(80), height: xs(30) }} />
                                        </View>
                                        <View style={[ b.ml2, { width: xs(130), justifyContent: 'center' }]}>
                                            <Text>Mandiri</Text>
                                        </View>
                                        {this.state.payment_method === 'M1' ?
                                            <View style={[ b.ml2, p.center, { width: xs(50) }]}>
                                                <Image source={require('../asset/checklist.png')} style={{ width: xs(15), height: xs(15) }} />
                                            </View>
                                            : null
                                        }
                                    </TouchableOpacity>
                                    <TouchableOpacity style={this.selectingPayment('VC')} onPress={() => this.setState({ payment_method: 'VC' })}>
                                        <View style={[ p.center, { width: xs(100) }]}>
                                            <Image source={require('../asset/VC.png')} style={{ width: xs(80), height: xs(30) }} />
                                        </View>
                                        <View style={[ b.ml2, { width: xs(130), justifyContent: 'center' }]}>
                                            <Text>Credit Card</Text>
                                        </View>
                                        {this.state.payment_method === 'VC' ?
                                            <View style={[ b.ml2, p.center, { width: xs(50) }]}>
                                                <Image source={require('../asset/checklist.png')} style={{ width: xs(15), height: xs(15) }} />
                                            </View>
                                            : null
                                        }
                                    </TouchableOpacity>
                                    <TouchableOpacity style={this.selectingPayment('BK')} onPress={() => this.setState({ payment_method: 'BK' })}>
                                        <View style={[ p.center, { width: xs(100) }]}>
                                            <Image source={require('../asset/bca_klik_pay.png')} style={{ width: xs(80), height: xs(20) }} />
                                        </View>
                                        <View style={[ b.ml2, { width: xs(130), justifyContent: 'center' }]}>
                                            <Text>BCA Klik Pay</Text>
                                        </View>
                                        {this.state.payment_method === 'BK' ?
                                            <View style={[ b.ml2, p.center, { width: xs(50) }]}>
                                                <Image source={require('../asset/checklist.png')} style={{ width: xs(15), height: xs(15) }} />
                                            </View>
                                            : null
                                        }
                                    </TouchableOpacity>
                                    <TouchableOpacity style={this.selectingPayment('OV')} onPress={() => this.setState({ payment_method: 'OV' })}>
                                        <View style={[ p.center, { width: xs(100) }]}>
                                            <Image source={require('../asset/ovo.png')} style={{ width: xs(80), height: xs(30) }} />
                                        </View>
                                        <View style={[ b.ml2, { width: xs(130), justifyContent: 'center' }]}>
                                            <Text>OVO</Text>
                                        </View>
                                        {this.state.payment_method === 'OV' ?
                                            <View style={[ b.ml2, p.center, { width: xs(50) }]}>
                                                <Image source={require('../asset/checklist.png')} style={{ width: xs(15), height: xs(15) }} />
                                            </View>
                                            : null
                                        }
                                    </TouchableOpacity>
                                    <TouchableOpacity style={this.selectingPayment('FT')} onPress={() => this.setState({ payment_method: 'FT' })}>
                                        <View style={[ p.center, { width: xs(100) }]}>
                                            <Image source={require('../asset/retail.png')} style={{ width: xs(80), height: xs(45) }} />
                                        </View>
                                        <View style={[ b.ml2, { width: xs(130), justifyContent: 'center' }]}>
                                            <Text>Retail</Text>
                                        </View>
                                        {this.state.payment_method === 'FT' ?
                                            <View style={[ b.ml2, p.center, { width: xs(50) }]}>
                                                <Image source={require('../asset/checklist.png')} style={{ width: xs(15), height: xs(15) }} />
                                            </View>
                                            : null
                                        }
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </ScrollView>
                        <View style={[ p.row, { width: '100%', height: xs(70), backgroundColor: '#3A3A3A', position: 'relative' }]}>
                            <View style={{ width: xs(150), justifyContent: 'center', marginLeft: vs(30) }}>
                                {this.state.selectedItem !== -1 && this.state.selectedItem !== undefined ?
                                    <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>{this.convertToRupiah(this.state.price)}</Text>
                                    :
                                    <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>Rp 0</Text>
                                }
                            </View>
                            <View style={[ p.alignEnd, { width: xs(150), justifyContent: 'center' }]}>
                                <TouchableOpacity onPress={() => this.request()} style={[ b.rounded, p.center, { width: xs(100), backgroundColor: this.state.selectedItem !== -1 && this.state.selectedItem !== undefined && this.state.payment_method !== undefined ? blue : '#CCC' }]} disabled={this.state.selectedItem === undefined || this.state.payment_method === undefined || this.state.selectedItem === -1}>
                                    <Text style={[ b.mt2, b.mb2, f._16, { fontFamily: 'lineto-circular-pro-bold', color: this.state.selectedItem !== -1 && this.state.selectedItem !== undefined && this.state.payment_method !== undefined ? light : '#000' }]}>Pay</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Tab>

                    <Tab heading='History' tabStyle={{ backgroundColor: '#000000' }} textStyle={{ color: light }}
                        activeTabStyle={{ backgroundColor: '#181B22' }}
                        activeTextStyle={{ color: blue, fontFamily: 'lineto-circular-pro-bold' }}>
                        <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#000' }} refreshControl={<RefreshControl refreshing={this.state.loading} onRefresh={() => this.onRefresh()} />}>
                            {this.state.history === undefined ?
                                <View style={[ p.center, { width: '100%', height: wp('155%') }]}>
                                    <Image source={require('../asset/activity-history.png')} style={{ width: xs(100), height: xs(100), tintColor: '#FFF' }} />
                                    <Text style={[ c.light, b.mt2 ]}>Tidak ada History</Text>
                                </View>
                                :
                                <FlatList
                                    data={this.state.history}
                                    keyExtractor={item => item.id_history}
                                    renderItem={({item}) => {
                                        if (item.status === 'SUCCESS') {
                                            return (
                                                <View style={[ p.row, b.mb2, { width: '100%', height: xs(60), borderWidth: 1, borderColor: 'white', backgroundColor: '#181B22' }]}>
                                                    <View style={[ p.center, { width: xs(60) }]}>
                                                        <View style={[ b.roundedHigh, { width: xs(15), height: xs(15), backgroundColor: 'lightgreen' }]} />
                                                    </View>
                                                    <View style={{ borderWidth: 0.5, borderColor: 'white' }} />
                                                    <View style={[ b.ml2, { width: xs(220), justifyContent: 'center' }]}>
                                                        {item.product_type !== null && (
                                                            <>
                                                                <Text style={[ c.light, f.bold ]}>{item.product_type} Poin</Text>
                                                                <Text style={[ c.light ]}>{this.convertToRupiah(item.payment_amount)}</Text>
                                                            </>
                                                        )}
                                                    </View>
                                                    <View style={{ borderWidth: 0.5, borderColor: 'white', marginLeft: vs(7) }} />
                                                    <TouchableOpacity onPress={() => this.props.navigation.push('CheckPayment', {id_history: item.id_history})} style={[ p.center, { width: xs(60) }]}>
                                                        <Icon name='ios-arrow-forward' color={light} size={35} />
                                                    </TouchableOpacity>
                                                </View>
                                            )
                                        } else if (item.status === 'PROCESS') {
                                            return (
                                                <View style={[ p.row, b.mb2, { width: '100%', height: xs(60), borderWidth: 1, borderColor: 'white', backgroundColor: '#181B22' }]}>
                                                    <View style={[ p.center, { width: xs(60) }]}>
                                                        <View style={[ b.roundedHigh, { width: xs(15), height: xs(15), backgroundColor: '#FFA500' }]} />
                                                    </View>
                                                    <View style={{ borderWidth: 0.5, borderColor: 'white' }} />
                                                    <View style={[ b.ml2, { width: xs(220), justifyContent: 'center' }]}>
                                                        {item.product_type !== null && (
                                                            <>
                                                                <Text style={[ c.light, f.bold ]}>{item.product_type} Poin</Text>
                                                                <Text style={[ c.light ]}>{this.convertToRupiah(item.payment_amount)}</Text>
                                                            </>
                                                        )}
                                                    </View>
                                                    <View style={{ borderWidth: 0.5, borderColor: 'white', marginLeft: vs(7) }} />
                                                    <TouchableOpacity onPress={() => this.props.navigation.push('CheckPayment', {id_history: item.id_history})} style={[ p.center, { width: xs(60) }]}>
                                                        <Icon name='ios-arrow-forward' color={light} size={35} />
                                                    </TouchableOpacity>
                                                </View>
                                            )
                                        } else if (item.status === 'EXPIRED') {
                                            return (
                                                <View style={[ p.row, b.mb2, { width: '100%', height: xs(60), borderWidth: 1, borderColor: 'white', backgroundColor: '#181B22' }]}>
                                                    <View style={[ p.center, { width: xs(60) }]}>
                                                        <View style={[ b.roundedHigh, { width: xs(15), height: xs(15), backgroundColor: 'red' }]} />
                                                    </View>
                                                    <View style={{ borderWidth: 0.5, borderColor: 'white' }} />
                                                    <View style={[ b.ml2, { width: xs(220), justifyContent: 'center' }]}>
                                                        {item.product_type !== null && (
                                                            <>
                                                                <Text style={[ c.light, f.bold ]}>{item.product_type} Poin</Text>
                                                                <Text style={[ c.light ]}>{this.convertToRupiah(item.payment_amount)}</Text>
                                                            </>
                                                        )}
                                                    </View>
                                                    <View style={{ borderWidth: 0.5, borderColor: 'white', marginLeft: vs(7) }} />
                                                    <TouchableOpacity onPress={() => this.props.navigation.push('CheckPayment', {id_history: item.id_history})} style={[ p.center, { width: xs(60) }]}>
                                                        <Icon name='ios-arrow-forward' color={light} size={35} />
                                                    </TouchableOpacity>
                                                </View>
                                            )
                                        }
                                    }}
                                />
                            }
                        </ScrollView>
                    </Tab>
                </Tabs>
            </>
        )
    }
}

const styles = StyleSheet.create({
    bankItem: {
        width: '100%',
        height: xs(50),
        backgroundColor: 'white'
    },

    bankItemSelected: {
        width: '100%',
        height: xs(50),
        backgroundColor: 'lightblue'
    }
})