import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import Icon from 'react-native-vector-icons/Ionicons'
import WebView from 'react-native-webview'
import { xs, vs } from '../utils/Responsive'
import { c, f, b, p } from '../utils/StyleHelper'
import { light, blue } from '../utils/Color'

export default class Pembayaran extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        return (
            <>
                <LinearGradient
                    start={{x: 0.25, y: 0.0}} end={{x: 0.5, y: 3.5}}
					locations={[0,0.4,0]}
					colors={[ '#655353', '#000000', '#000000' ]}
					style={[ p.row, { width: '100%', height: xs(80), backgroundColor: '#000000' }]}
                >
                    <View style={[ p.center, b.ml4 ]}>
                        <TouchableOpacity onPress={() => this.props.navigation.pop()}>
                            <Icon name='ios-arrow-back' color={light} size={38} />
                        </TouchableOpacity>
                    </View>
                    <View style={[ p.center, b.ml4, { width: xs(250) }]}>
                        <Text style={[ c.light, f._18 ]}>Pembayaran</Text>
                    </View>
                </LinearGradient>
                <WebView source={{uri: this.props.navigation.getParam('paymentURL')}} />
            </>
        )
    }
}