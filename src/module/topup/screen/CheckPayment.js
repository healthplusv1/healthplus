import React, { Component } from 'react'
import { Text, View, TouchableOpacity, StyleSheet, ActivityIndicator } from 'react-native'
import { blue, light } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { b, p, f, c } from '../utils/StyleHelper'
import { widthPercentageToDP as wp } from 'react-native-responsive-screen'
import Icon from 'react-native-vector-icons/Ionicons'
import FastImage from 'react-native-fast-image'
import { NavigationEvents } from 'react-navigation'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'
import { API_URL } from 'react-native-dotenv'

export default class CheckPayment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            payment_amount: '',
            merchantId: '',
            product_type: '',
            payment_method: '',
            url_payment: '',
            status: '',
            loading: true
        }
    }

    async componentDidMount() {
        this.setState({ loading: true })
        
        const id_login = await AsyncStorage.getItem('@id_login')
        
        axios.get(API_URL+'/main/get_detail_history', {params: {
            id_history: this.props.navigation.getParam('id_history')
        }})
        .then(res => {
            // console.log(res.data.data)
            this.setState({
                loading: false,
                payment_amount: res.data.data.payment_amount,
                merchantId: res.data.data.merchantOrderId,
                product_type: res.data.data.product_type,
                payment_method: res.data.data.payment_method,
                url_payment: res.data.data.url_payment,
                status: res.data.data.status
            })

            axios.get(API_URL+'/main/check_status', {params: {
                merchantOrderId: res.data.data.merchantOrderId,
                id_login: id_login
            }})
            .then(resp => {
                console.log(resp.data)
            })
        })
    }

    convertToRupiah(number) {
        if (number) {
          var rupiah = "";
          var numberrev = number
            .toString()
            .split("")
            .reverse()
            .join("");
          for (var i = 0; i < numberrev.length; i++)
            if (i % 3 == 0) rupiah += numberrev.substr(i, 3) + ".";
          return (
            "Rp " +
            rupiah
              .split("", rupiah.length - 1)
              .reverse()
              .join("")
          );
        } else {
            return (
                "Rp "+number
            );
        }
      
    }

    render() {
        return (
            <>
                {this.state.loading === true ?
                    <View style={[ styles.container_loading, styles.horizontal_loading ]}>
                        <ActivityIndicator size='large' color={blue} />
                    </View>
                    : null
                }
                <NavigationEvents onDidFocus={() => this.componentDidMount()} />
                <View style={{ width: '100%', height: '100%', backgroundColor: '#181B22' }}>
                    <View style={[ p.row, { width: '100%', height: xs(80), backgroundColor: '#000' }]}>
                        <View style={[ b.ml4, p.center ]}>
                            <TouchableOpacity onPress={() => this.props.navigation.pop()}>
                                <Icon name='ios-arrow-back' color={light} size={40} />
                            </TouchableOpacity>
                        </View>
                        <View style={[ b.ml4, p.center, { width: xs(250) }]}>
                            <Text style={[ c.light, f.bold, f._20 ]}>Detail</Text>
                        </View>
                    </View>

                    <View style={{ width: '100%', height: '100%', alignItems: 'center' }}>
                        <View style={[ b.rounded, { marginTop: wp('40%'), width: xs(300), borderWidth: 1, borderColor: '#FFF', backgroundColor: '#3A3A3A' }]}>
                            {this.state.status === 'SUCCESS' && (
                                <View style={[ b.mt4, b.mb4, b.ml2, b.mr2, { alignItems: 'center' }]}>
                                    {this.state.payment_method === 'VC' && (
                                        <FastImage source={require('../asset/VC.png')} style={{ width: xs(125), height: xs(45) }} />
                                    )}
                                    {this.state.payment_method === 'VA' && (
                                        <FastImage source={require('../asset/maybank.png')} style={{ width: xs(125), height: xs(45) }} />
                                    )}
                                    {this.state.payment_method === 'BT' && (
                                        <FastImage source={require('../asset/permata_bank.png')} style={{ width: xs(125), height: xs(45) }} />
                                    )}
                                    {this.state.payment_method === 'B1' && (
                                        <FastImage source={require('../asset/cimb3x.png')} style={{ width: xs(125), height: xs(45) }} />
                                    )}
                                    {this.state.payment_method === 'A1' && (
                                        <FastImage source={require('../asset/atm_bersama.png')} style={{ width: xs(125), height: xs(45) }} />
                                    )}
                                    {this.state.payment_method === 'I1' && (
                                        <FastImage source={require('../asset/bni.png')} style={{ width: xs(125), height: xs(45) }} />
                                    )}
                                    {this.state.payment_method === 'M1' && (
                                        <FastImage source={require('../asset/mandiri.png')} style={{ width: xs(125), height: xs(45) }} />
                                    )}
                                    {this.state.payment_method === 'BK' && (
                                        <FastImage source={require('../asset/bca_klik_pay.png')} style={{ width: xs(165), height: xs(45) }} />
                                    )}
                                    {this.state.payment_method === 'FT' && (
                                        <FastImage source={require('../asset/retail.png')} style={{ width: xs(150), height: xs(50) }} />
                                    )}
                                    {this.state.payment_method === 'OV' && (
                                        <FastImage source={require('../asset/ovo.png')} style={{ width: xs(125), height: xs(45) }} />
                                    )}
                                    <View style={[ b.mt2, { width: '100%' }]}>
                                        <Text style={[ f.bold, f._16, c.light, b.ml2 ]}>Description</Text>
                                        <View style={[ p.row ]}>
                                            <Text style={[ b.mt1, c.light, b.ml2 ]}>{this.state.product_type} Poin</Text>
                                            <Text style={[ b.mt1, b.ml1, c.light ]}>({this.convertToRupiah(this.state.payment_amount)})</Text>
                                        </View>
                                    </View>
                                    <View style={[ b.mt2, { width: '100%' }]}>
                                        <Text style={[ f.bold, f._16, c.light, b.ml2 ]}>Merchant Order ID</Text>
                                        <Text style={[ b.mt1, c.light, b.ml2 ]}>{this.state.merchantId}</Text>
                                    </View>
                                    <View style={[ b.mt2, { width: '100%' }]}>
                                        <Text style={[ f.bold, f._16, c.light, b.ml2 ]}>Status</Text>
                                        <Text style={[ b.mt1, c.light, b.ml2 ]}>{this.state.status}</Text>
                                    </View>
                                </View>
                            )}

                            {this.state.status === 'PROCESS' && (
                                <View style={[ b.mt4, b.mb4, b.ml2, b.mr2, { alignItems: 'center' }]}>
                                    {this.state.payment_method === 'VC' && (
                                        <FastImage source={require('../asset/VC.png')} style={{ width: xs(125), height: xs(45) }} />
                                    )}
                                    {this.state.payment_method === 'VA' && (
                                        <FastImage source={require('../asset/maybank.png')} style={{ width: xs(125), height: xs(45) }} />
                                    )}
                                    {this.state.payment_method === 'BT' && (
                                        <FastImage source={require('../asset/permata_bank.png')} style={{ width: xs(125), height: xs(45) }} />
                                    )}
                                    {this.state.payment_method === 'B1' && (
                                        <FastImage source={require('../asset/cimb3x.png')} style={{ width: xs(125), height: xs(45) }} />
                                    )}
                                    {this.state.payment_method === 'A1' && (
                                        <FastImage source={require('../asset/atm_bersama.png')} style={{ width: xs(125), height: xs(45) }} />
                                    )}
                                    {this.state.payment_method === 'I1' && (
                                        <FastImage source={require('../asset/bni.png')} style={{ width: xs(125), height: xs(45) }} />
                                    )}
                                    {this.state.payment_method === 'M1' && (
                                        <FastImage source={require('../asset/mandiri.png')} style={{ width: xs(125), height: xs(45) }} />
                                    )}
                                    {this.state.payment_method === 'BK' && (
                                        <FastImage source={require('../asset/bca_klik_pay.png')} style={{ width: xs(165), height: xs(45) }} />
                                    )}
                                    {this.state.payment_method === 'FT' && (
                                        <FastImage source={require('../asset/retail.png')} style={{ width: xs(150), height: xs(50) }} />
                                    )}
                                    {this.state.payment_method === 'OV' && (
                                        <FastImage source={require('../asset/ovo.png')} style={{ width: xs(125), height: xs(45) }} />
                                    )}
                                    <View style={[ b.mt2, { width: '100%' }]}>
                                        <Text style={[ f.bold, f._16, c.light, b.ml2 ]}>Description</Text>
                                        <View style={[ p.row ]}>
                                            <Text style={[ b.mt1, c.light, b.ml2 ]}>{this.state.product_type} Poin</Text>
                                            <Text style={[ b.mt1, b.ml1, c.light ]}>({this.convertToRupiah(this.state.payment_amount)})</Text>
                                        </View>
                                    </View>
                                    <View style={[ b.mt2, { width: '100%' }]}>
                                        <Text style={[ f.bold, f._16, c.light, b.ml2 ]}>Merchant Order ID</Text>
                                        <Text style={[ b.mt1, c.light, b.ml2 ]}>{this.state.merchantId}</Text>
                                    </View>
                                    <View style={[ b.mt2, { width: '100%' }]}>
                                        <Text style={[ f.bold, f._16, c.light, b.ml2 ]}>Status</Text>
                                        <Text style={[ b.mt1, c.light, b.ml2 ]}>{this.state.status}</Text>
                                    </View>
                                    <TouchableOpacity onPress={() => this.props.navigation.push('Pembayaran', {paymentURL: this.state.url_payment})} style={[ b.rounded, p.center, b.mt2, { width: xs(80), height: xs(35), backgroundColor: blue }]}>
                                        <Text style={[ f.bold, c.light ]}>Pay Now</Text>
                                    </TouchableOpacity>
                                </View>
                            )}

                            {this.state.status === 'EXPIRED' && (
                                <View style={[ b.mt4, b.mb4, b.ml2, b.mr2, { alignItems: 'center' }]}>
                                    {this.state.payment_method === 'VC' && (
                                        <FastImage source={require('../asset/VC.png')} style={{ width: xs(125), height: xs(45) }} />
                                    )}
                                    {this.state.payment_method === 'VA' && (
                                        <FastImage source={require('../asset/maybank.png')} style={{ width: xs(125), height: xs(45) }} />
                                    )}
                                    {this.state.payment_method === 'BT' && (
                                        <FastImage source={require('../asset/permata_bank.png')} style={{ width: xs(125), height: xs(45) }} />
                                    )}
                                    {this.state.payment_method === 'B1' && (
                                        <FastImage source={require('../asset/cimb3x.png')} style={{ width: xs(125), height: xs(45) }} />
                                    )}
                                    {this.state.payment_method === 'A1' && (
                                        <FastImage source={require('../asset/atm_bersama.png')} style={{ width: xs(125), height: xs(45) }} />
                                    )}
                                    {this.state.payment_method === 'I1' && (
                                        <FastImage source={require('../asset/bni.png')} style={{ width: xs(125), height: xs(45) }} />
                                    )}
                                    {this.state.payment_method === 'M1' && (
                                        <FastImage source={require('../asset/mandiri.png')} style={{ width: xs(125), height: xs(45) }} />
                                    )}
                                    {this.state.payment_method === 'BK' && (
                                        <FastImage source={require('../asset/bca_klik_pay.png')} style={{ width: xs(165), height: xs(45) }} />
                                    )}
                                    {this.state.payment_method === 'FT' && (
                                        <FastImage source={require('../asset/retail.png')} style={{ width: xs(150), height: xs(50) }} />
                                    )}
                                    {this.state.payment_method === 'OV' && (
                                        <FastImage source={require('../asset/ovo.png')} style={{ width: xs(125), height: xs(45) }} />
                                    )}
                                    <View style={[ b.mt2, { width: '100%' }]}>
                                        <Text style={[ f.bold, f._16, c.light, b.ml2 ]}>Description</Text>
                                        <View style={[ p.row ]}>
                                            <Text style={[ b.mt1, c.light, b.ml2 ]}>{this.state.product_type} Poin</Text>
                                            <Text style={[ b.mt1, b.ml1, c.light ]}>({this.convertToRupiah(this.state.payment_amount)})</Text>
                                        </View>
                                    </View>
                                    <View style={[ b.mt2, { width: '100%' }]}>
                                        <Text style={[ f.bold, f._16, c.light, b.ml2 ]}>Merchant Order ID</Text>
                                        <Text style={[ b.mt1, c.light, b.ml2 ]}>{this.state.merchantId}</Text>
                                    </View>
                                    <View style={[ b.mt2, { width: '100%' }]}>
                                        <Text style={[ f.bold, f._16, c.light, b.ml2 ]}>Status</Text>
                                        <Text style={[ b.mt1, c.light, b.ml2 ]}>{this.state.status}</Text>
                                    </View>
                                </View>
                            )}
                        </View>
                    </View>
                </View>
            </>
        )
    }
}

const styles = StyleSheet.create({
    container_loading: {
        position:'absolute',
        zIndex: 2,
        width: '100%',
        height: '100%',
        justifyContent: "center",
        backgroundColor: 'rgba(0,0,0,0.7)'
    },

    horizontal_loading: {
        flexDirection: "row",
        justifyContent: "space-around",
        padding: 10
    },
})