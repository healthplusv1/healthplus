import React from 'react'
import { Text, View, Image, ImageBackground, ScrollView, TouchableOpacity } from 'react-native'
import { b, p, f, c } from '../utils/StyleHelper';
import { vs, xs } from '../utils/Responsive';
import { blue, light, grey, softBlue } from '../utils/Color';

export default class Advice extends React.Component {
	render() {
		return (
			<>
				<View style={{ width: '100%', height: xs(80), backgroundColor: blue }}>
					<View style={[ p.row, b.ml4, { marginTop: vs(20) }]}>
						<View>
							<TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>
								<Image source={require('../asset/back.png')} style={{ width: xs(25), height: xs(25) }}/>
							</TouchableOpacity>
						</View>
						<View style={[ b.ml4, p.center, { width: xs(240) }]}>
							<Text style={[ c.light, f.bold, f._20 ]}> Nasihat untuk umum </Text>
						</View>
					</View>
				</View>
				<ScrollView showsVerticalScrollIndicator={false}>
					<View style={[ p.center ]}>
						<View style={{ width: xs(300) }}>
							<Text style={[ f.bold, f._18, b.mt4 ]}>
								Langkah-langkah perlindungan dasar terhadap coronavirus baru
							</Text>
							<Text style={[ b.mt4 ]}>
								Tetap mengetahui informasi terbaru tentang wabah COVID-19, tersedia di situs web WHO dan melalui otoritas kesehatan publik nasional dan lokal Anda. Tiang orang yang terinfeksi mengalami liness ringan dan pulih, tetapi bisa lebih parah bagi orang lain. Jaga kesehatan Anda dan lindungi orang lain dengan melakukan hal berikut.
							</Text>
							<Text style={[ f.bold, b.mt3 ]}>
								Cuci tangan Anda sesering mungkin
							</Text>
							<Text style={[ b.mt1 ]}>
								Secara teratur dan bersihkan tangan Anda dengan tangan berbasis alkohol, gosok atau cuci dengan sabun dan air.
							</Text>
							<View style={[ p.row, b.mt1 ]}>
								<Text style={[ f.bold ]}>
									Mengapa?
								</Text>
								<Text style={[ b.ml1 ]}>
									Cuci tangan Anda dengan sabun
								</Text>
							</View>
							<Text>
								dan air atau menggunakan gosok tangan berbasis alkohol membunuh virus yang mungkin ada di tangan Anda.
							</Text>
							<Text style={[ f.bold, b.mt2, ]}>
								Pertahankan jarak sosial
							</Text>
							<Text>
								Pertahankan jarak setidaknya 1 meter (3 kaki) antara diri Anda dan siapa saja yang batuk atau bersin.
							</Text>
							<View style={[ p.row, b.mt1 ]}>
								<Text style={[ f.bold ]}>
									Mengapa?
								</Text>
								<Text style={[ b.ml1 ]}>
									Ketika seseorang batuk atau bersin
								</Text>
							</View>
							<Text>
								Mereka semprotkan tetesan cairan kecil dari hidung atau mulut mereka yang mungkin mengandung virus. Jika Anda terlalu dekat, Anda bisa menghirup tetesan air, termasuk virus COVID-19 jika orang tersebut menderita batuk.
							</Text>
							<Text style={[ f.bold, b.mt2 ]}>
								Hindari menyentuh mata, hidung dan mulut
							</Text>
							<View style={[ p.row, b.mt1 ]}>
								<Text style={[ f.bold ]}>
									Mengapa?
								</Text>
								<Text style={[ b.ml1 ]}>
									Tangan menyentuh banyak permukaan
								</Text>
							</View>
							<Text>
								dan dapat memetik virus. Setelah terkontaminasi, tangan dapat memindahkan virus ke mata, hidung, atau mulut Anda. Dari sana, virus bisa masuk ke tubuh Anda dan bisa membuat Anda sakit.
							</Text>
							<Text style={[ f.bold, b.mt2 ]}>
								Lakukan kebersihan pernafasan
							</Text>
							<Text style={[ b.mt1 ]}>
								Pastikan Anda, dan orang-orang di sekitar Anda, mengikuti kebersihan pernapasan yang baik. Ini berarti menutupi mulut dan hidung Anda dengan siku atau jaringan yang tertekuk saat Anda batuk atau bersin. Kemudian segera buang tisu bekas.
							</Text>
							<View style={[ p.row, b.mt1 ]}>
								<Text style={[ f.bold ]}>
									Mengapa?
								</Text>
								<Text style={[ b.ml1 ]}>
									Tetesan menyebarkan virus. Dengan
								</Text>
							</View>
							<Text>
								mengikuti kebersihan yang baik pernafasan Anda melindungi orang-orang di sekitar Anda dari virus seperti flu, flu dan COVID-19.
							</Text>
							<Text style={[ f.bold, b.mt2 ]}>
								Jika Anda mengalami demam, batuk, dan kesulitan bernapas, cari perawatan medis sejak dini
							</Text>
							<Text style={[ b.mt1 ]}>
								Tetap di rumah jika Anda merasa tidak sehat. Jika Anda mengalami demam, batuk dan kesulitan bernapas, cari bantuan medis dan hubungi terlebih dahulu. Ikuti arahan otoritas kesehatan setempat Anda.
							</Text>
							<View style={[ p.row, b.mt1 ]}>
								<Text style={[ f.bold ]}>
									Mengapa?
								</Text>
								<Text style={[ b.ml1 ]}>
									Otoritas nasional dan lokal akan
								</Text>
							</View>
							<Text>
								memiliki informasi terbaru tentang situasi di daerah Anda.
								Menelepon terlebih dahulu akan memungkinkan penyedia layanan kesehatan Anda dengan cepat
								mengarahkan Anda ke fasilitas kesehatan yang tepat. Ini juga akan melindungi Anda
								dan membantu mencegah penyebaran virus dan infeksi lainnya.
							</Text>
							<Text style={[ f.bold, b.mt2 ]}>
								Tetap terinformasi dan ikuti saran yang diberikan oleh penyedia layanan kesehatan Anda
							</Text>
							<Text style={[ b.mt1 ]}>
								Tetap terinformasi tentang perkembangan terbaru tentang COVID-19.
								Ikuti saran yang diberikan oleh penyedia layanan kesehatan Anda, warga negara Anda
								dan otoritas kesehatan masyarakat setempat atau majikan Anda tentang cara melindungi diri sendiri dan orang lain dari COVID-19.
							</Text>
							<View style={[ p.row, b.mt1 ]}>
								<Text style={[ f.bold ]}>
									Mengapa?
								</Text>
								<Text style={[ b.ml1 ]}>
									Otoritas nasional dan lokal akan
								</Text>
							</View>
							<Text>
								memiliki informasi terbaru tentang apakah COVID-19 menyebar di wilayah Anda.
								Mereka ditempatkan paling baik untuk memberi nasihat tentang apa yang harus dilakukan orang di daerah Anda untuk melindungi diri mereka sendiri.
							</Text>
							<Text style={[ f.bold, b.mt4, f._18 ]}>
								Langkah-langkah perlindungan dasar terhadap coronavirus baru
							</Text>
							<Text style={[ f.bold, b.mt4 ]}>
								Ikuti panduan yang diuraikan di atas.
							</Text>
							<Text style={[ b.mt1 ]}>
								Tetap di rumah jika Anda mulai merasa tidak sehat, bahkan dengan gejala ringan seperti sakit kepala dan sakit kepala,
								hidung berair sedikit, sampai Anda pulih.
							</Text>
							<View style={[ p.row ]}>
								<Text style={[ f.bold ]}>
									Mengapa?
								</Text>
								<Text> 
									Menghindari kontak dengan orang lain
								</Text>
							</View>
							<Text>
								dan kunjungan untuk fasilitas medis akan memungkinkan
								fasilitas ini untuk beroperasi secara lebih efektif dan membantu melindungi Anda dan orang lain
								dari kemungkinan COVID-19 dan virus lainnya.
							</Text>
							<Text style={[ b.mt1 ]}>
								Jika Anda mengalami demam, batuk dan kesulitan bernapas,
								segera dapatkan saran medis karena ini mungkin disebabkan oleh infeksi pernapasan
								atau kondisi serius lainnya. Hubungi terlebih dahulu dan beri tahu penyedia Anda tentang perjalanan atau kontak terbaru dengan Pelancong.
							</Text>
							<View style={[ p.row ]}>
								<Text style={[ f.bold ]}>
									Mengapa?
								</Text>
								<Text style={[ b.ml1 ]}>
									Menelepon terlebih dahulu akan
								</Text>
							</View>
							<Text>
								memungkinkan penyedia layanan kesehatan Anda dengan cepat mengarahkan Anda ke fasilitas kesehatan yang tepat.
								Ini juga akan membantu mencegah kemungkinan penyebaran COVID-19 dan virus lainnya.
							</Text>
						</View>
					</View>
					<View style={{ height: xs(20) }} />
				</ScrollView>
			</>
		)
	}
}