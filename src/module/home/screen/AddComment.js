import React from 'react'
import { Text, View, BackHandler, TouchableOpacity, TextInput, StyleSheet, PixelRatio, ScrollView, Image, Alert, ActivityIndicator } from 'react-native'
import { b, p, f, c } from '../utils/StyleHelper'
import { vs, xs } from '../utils/Responsive'
import { blue, light, grey, softBlue } from '../utils/Color'
import Svg from '../utils/Svg'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import ImagePicker from 'react-native-image-picker'
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage'
import { API_URL } from 'react-native-dotenv'

export default class AddComment extends React.Component {

	constructor(props){
		super(props);
		this.state ={
			deskripsi: '',
			url_gambar: '',
            uri: '',
            fileName: null,
            file_image: null,
            isLoading: false
		}
	}

	launchCamera = () => {
	    let options = {
	      storageOptions: {
	        skipBackup: true,
	        path: 'images',
	      },
	    };
	    ImagePicker.launchCamera(options, (response) => {
	      console.log('Response = ', response);

	      if (response.didCancel) {
	        console.log('User cancelled image picker');
	      } else if (response.error) {
	        console.log('ImagePicker Error: ', response.error);
	      } else if (response.customButton) {
	        console.log('User tapped custom button: ', response.customButton);
	        alert(response.customButton);
	      } else {
	        console.log(response.fileName);
	        this.setState({
	          uri: {uri: response.uri},
	          url_gambar: response.uri,
	          fileName: response.fileName,
	          file_image: response
	        });
	      }
	    });

	}

  launchImageLibrary = () => {
    let options = {
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.launchImageLibrary(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        alert(response.customButton);
      } else {
        console.log(response);
        // alert(JSON.stringify(response));
        this.setState({
          uri: {uri: response.uri},
          url_gambar: response.uri,
          fileName: response.fileName,
          file_image: response
        });
      }
    });

	}

	renderFileUri() {
	    if (this.state.url_gambar) {
	      return <Image
	        source={{ uri: this.state.url_gambar }}
	        style={styles.images}
	      />
	    } else {
	      return <Image
	        style={styles.images}
	      />
	    }
	}

	createFormDataPhoto = (photo, id_login, id_group) => {
	    const data = new FormData();

	    if (this.state.fileName !== null) {
		    data.append('file_image', {
		      name: (photo.fileName == null)? 'myphoto': photo.fileName,
		      type: photo.type,
		      uri: photo.uri
		    });
	    }
	    data.append("id_login", id_login);
	    data.append("id_group", id_group);
	    data.append("deskripsi", this.state.deskripsi);
	    return data;
	  };

	async post_to_group(){
		const id_login = await AsyncStorage.getItem('@id_login')
		const id_group = await AsyncStorage.getItem('@current_id_group')

		if (this.state.deskripsi === '' && this.state.file_image === '') {
			Alert.alert('Masukkan Judul dan Gambar untuk memposting!')
		} else {
			this.setState({
				isLoading: true
			})
		}

	    fetch(API_URL+'/main/post_to_group', {
	      method: 'POST',
	      headers: {
	        'Content-Type': 'multipart/form-data'
	      },
	      body: this.createFormDataPhoto(this.state.file_image, id_login, id_group)
	    })
	      .then(response => response.json())
	      .then(response => {
	        // console.log(response);
	      this.setState({
	      	isLoading: false
	      })
	        Alert.alert(
	        	"",
	        	"Postingan Anda telah terkirim",
	        	[
	        		{ text: 'OK', onPress: () => this.props.navigation.pop() }
	        	]
	        )
	      })
	      .catch(error => {
	        console.log('Upload Error', error);
	        // alert(JSON.stringify(error))
	      });
	}

	render() {
		return (
			<>
				{this.state.isLoading ?
					<View style={[styles.container_loading, styles.horizontal_loading]}>
						<ActivityIndicator size="large" color="#0000ff"/>
					</View>
					:
					null
				}
				<ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#181B22' }}>
					<View style={{ width: '100%', backgroundColor: '#121212', alignItems: 'center' }}>
						<View style={{ width: wp('90%'), marginTop: wp('4%'), marginBottom: wp('4%'), flexDirection: 'row', alignItems: 'center' }}>
							<View style={{ width: wp('20%') }}>
								<TouchableOpacity onPress={() => this.props.navigation.pop()}>
									<Image source={require('../asset/left.png')} style={{ width: wp('8%'), height: wp('8%') }} />
								</TouchableOpacity>
							</View>
							<View style={{ width: wp('50%'), alignItems: 'center' }}>
								<Text style={[ c.light, f.bold, f._18 ]}>Upload Postingan</Text>
							</View>
							<View style={{ width: wp('20%') }}></View>
						</View>
					</View>
					<View style={{ width: '100%', backgroundColor: '#181B22', alignItems: 'center' }}>
						<View style={{ width: wp('90%'), marginTop: wp('4%'), flexDirection: 'row', alignItems: 'center' }}>
							<View style={{ width: wp('20%') }}>
								<FastImage source={require('../asset/10.png')} style={{ width: wp('15%'), height: wp('15%') }} />
							</View>
							<View style={{ width: wp('70%') }}>
								<Text style={[ c.light, f.bold, f._16 ]}>Pratama</Text>
								<TouchableOpacity style={{ width: wp('20%'), height: hp('5%'), backgroundColor: blue, borderRadius: wp('2%'), marginTop: wp('2%'), flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
									<Text style={[ c.light ]}>Publik</Text>
									<Image source={require('../asset/sortDown.png')} style={{ width: wp('2%'), height: wp('2%'), tintColor: light, marginLeft: wp('1%'), marginTop: wp('0.5%') }} />
								</TouchableOpacity>
							</View>
						</View>
						<View style={[ b.rounded, b.shadow, { width: wp('90%'), height: hp('55%'), backgroundColor: light, marginTop: wp('4%') }]}>
							<TextInput
								placeholder='Tulis Postingan...'
								multiline={true}
								style={[ b.ml2, c.light, { width: xs(300) }]}
								onChangeText={text => {
									let deskripsi = this.state.deskripsi
									deskripsi = text
									this.setState({ deskripsi });
								}}
								value={this.state.deskripsi}
							/>
							<View style={[ p.center, { width: '100%' }]}>
								{this.renderFileUri()}
							</View>

							<View style={{ width: '100%', borderBottomWidth: 2, borderBottomColor: blue, marginTop: wp('15%') }} />
							<View style={{ flexDirection: 'row', alignItems: 'center', marginTop: wp('3.5%') }}>
								<TouchableOpacity style={{ marginLeft: wp('4%') }}>
									<Image source={require('../asset/picturepost_icon.png')} style={{ width: xs(30), height: xs(24) }} />
								</TouchableOpacity>

								<TouchableOpacity style={{ marginLeft: wp('6%') }}>
									<Image source={require('../asset/friend_icon.png')} style={{ width: xs(25), height: xs(19), tintColor: blue }} />
								</TouchableOpacity>

								<TouchableOpacity style={{ marginLeft: wp('6%') }}>
									<Image source={require('../asset/attachpost_icon.png')} style={{ width: xs(15), height: xs(23.5) }} />
								</TouchableOpacity>
							</View>
						</View>
					</View>
					<View style={[ b.container, p.center, { marginTop: wp('5%'), marginBottom: wp('5%') }]}>
						<TouchableOpacity onPress={() => this.post_to_group()} style={[ b.roundedLow, p.center, { width: wp('35%'), height: hp('6%'), backgroundColor: blue }]}>
							<Text style={[ c.light, f.bold, f._16 ]}>Post</Text>
						</TouchableOpacity>
					</View>
				</ScrollView>
			</>
		)
	}
}

const styles = StyleSheet.create({
  images: {
    width: xs(315),
    height: xs(195),
    marginHorizontal: 3,
  },

  container_loading: {
	flex: 1,
    position:'absolute',
    zIndex:1,
    width: '100%',
    height: '100%',
    justifyContent: "center",
    backgroundColor: 'rgba(0,0,0,0.7)'
  },
  
  horizontal_loading: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },
});