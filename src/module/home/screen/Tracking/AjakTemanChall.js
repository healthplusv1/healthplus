import React from 'react'
import { View, Text, ScrollView, TouchableOpacity, FlatList, Linking } from 'react-native'
import { Fab, Button } from 'native-base'
import { b, p, f, c } from '../../utils/StyleHelper'
import { vs, xs } from '../../utils/Responsive'
import { blue, light } from '../../utils/Color'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'
import FastImage from 'react-native-fast-image'
import Icon from 'react-native-vector-icons/Ionicons'
import Share from 'react-native-share'
import Svg from '../../utils/Svg'
import { API_URL } from 'react-native-dotenv'

const shareWhatsapp = {
    title: 'Share via',
    message: 'Ayo ikuti tantangan didalam aplikasi ini, yang menang akan mendapatkan poin.',
    url: 'https://play.google.com/store/apps/details?id=com.healthplus',
    social: Share.Social.WHATSAPP,
    whatsAppNumber: ''
};
const shareFacebook = {
    title: 'Share via',
    message: 'Ayo ikuti tantangan didalam aplikasi ini, yang menang akan mendapatkan poin.',
    url: 'https://play.google.com/store/apps/details?id=com.healthplus',
    social: Share.Social.FACEBOOK
};
const shareInstagram = {
    title: 'Share via',
    message: 'Ayo ikuti tantangan didalam aplikasi ini, yang menang akan mendapatkan poin.',
    url: 'https://play.google.com/store/apps/details?id=com.healthplus',
    social: Share.Social.INSTAGRAM
};

export default class AjakTemanChall extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            list_ajak_teman: '',
            active: false,
        }
    }

    async componentDidMount() {
        const id_login = await AsyncStorage.getItem('@id_login')

        axios.get(API_URL+'/main/get_friend_to_invite_kompetisi', {params: {
            id_login_from: id_login,
            id_kompetisi: this.props.navigation.getParam('id_kompetisi')
        }})
        .then(res => {
            this.setState({
                list_ajak_teman: res.data.data
            })
            // alert(JSON.stringify(res))
        })
    }
    
    async invite_to_challenge(id_login_to){
        const id_login_from = await AsyncStorage.getItem('@id_login')

        axios.get(API_URL+'/main/invite_to_competition', {params: {
            id_login_from: id_login_from,
            id_login_to: id_login_to,
            id: this.props.navigation.getParam('id_kompetisi'),
            type: "Competition Invitation"
        }})
        .then(res => {
            this.componentDidMount()
        })
    }

    render() {
        return (
            <>
                <View style={{ width: '100%', height: xs(80), backgroundColor: '#000' }}>
                    <View style={[ p.row, b.ml4, b.mt4 ]}>
                        <View style={{ width: xs(55) }}>
                            <TouchableOpacity onPress={() => this.props.navigation.pop()}>
                                <Icon name="ios-arrow-back" size={40} color={light} />
                            </TouchableOpacity>
                        </View>
                        <View style={[ b.ml2, p.center, { width: xs(200) }]}>
                            <Text style={[ c.light, f._18, { fontFamily: 'lineto-circular-pro-bold' }]}>Ajak Teman</Text>
                        </View>
                    </View>
                </View>
                <View style={{ backgroundColor: '#555555' }}>
                    <View style={[ p.row, b.mt2, b.mb2, p.center, { width: '100%' }]}>
                        <View style={[ p.center, { width: xs(60), height: xs(60) }]}>
                            <TouchableOpacity onPress={() => Share.shareSingle(shareWhatsapp)}>
                                <Svg icon="whatsapp" size={50} />
                            </TouchableOpacity>
                        </View>
                        <View style={[ p.center, b.ml2, { width: xs(60), height: xs(60) }]}>
                            <TouchableOpacity onPress={() => Share.shareSingle(shareFacebook)}>
                                <Svg icon="facebook" size={50} />
                            </TouchableOpacity>
                        </View>
                        <View style={[ p.center, b.ml2, { width: xs(60), height: xs(60) }]}>
                            <TouchableOpacity onPress={() => Share.shareSingle(shareInstagram)}>
                                <Svg icon="instagram" size={50} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#181B22' }}>
                    <View style={{ width: '100%' }}>
                        <FlatList
                            extraData={this.state}
							data={this.state.list_ajak_teman}
							keyExtractor={item => item.id_login}
							renderItem={ ({item}) => (
                                <>
                                    <View style={[ b.mt2, { borderWidth: 1, borderColor: '#E0E3E8' }]} />
                                    <View style={[ p.row, p.center, b.mt2, { width: '100%', height: xs(60) }]}>
                                        <View style={[ p.row, { width: xs(180) }]}>
                                            <FastImage source={item.url_gambar === null ? { uri: API_URL+'/src/avatar/default.png' } : { uri: API_URL+'/src/avatar/'+item.url_gambar }} style={{ width: xs(50), height: xs(50) }} />
                                            <TouchableOpacity onPress={() => this.props.navigation.push('ProfTracking', {id_login: item.id_login})}>
                                                <Text style={[ b.mt3, b.ml2, c.light ]}>{item.nama}</Text>
                                            </TouchableOpacity>
                                        </View>
                                        <View style={[ p.alignEnd, b.ml2, { width: xs(150) }]}>
                                            {item.joined === 'true' ?
                                                <View style={[ p.center, b.roundedLow, { borderWidth: 2, borderColor: '#555555', backgroundColor: '#000' }]}>
                                                    <Text style={[ c.light, b.ml2, b.mr2, b.mt1, b.mb1, { fontFamily: 'lineto-circular-pro-bold' }]}>Sudah Bergabung</Text>
                                                </View>
                                                :
                                                (item.invited === 'true' ?
                                                    <View onPress={() => this.request_friend(item.id_login)} style={[ p.center, b.roundedLow, { width: xs(80), height: xs(30), borderWidth: 2, borderColor: '#555555', backgroundColor: '#000' }]}>
                                                        <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>Terkirim</Text>
                                                    </View>
                                                    :
                                                    <TouchableOpacity onPress={() => this.invite_to_challenge(item.id_login)} style={[ p.center, b.roundedLow, { width: xs(80), height: xs(30), borderWidth: 2, borderColor: '#555555', backgroundColor: '#000' }]}>
                                                        <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>Ajak</Text>
                                                    </TouchableOpacity>
                                                )
                                            }
                                        </View>
                                    </View>
                                    <View style={[ b.mt2, { borderWidth: 1, borderColor: '#E0E3E8' }]} />
                                </>
                            )}
                        />
                    </View>
                </ScrollView>
            </>
        )
    }
}