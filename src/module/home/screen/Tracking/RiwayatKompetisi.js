import React from 'react'
import { View, Text, ScrollView, FlatList, TouchableOpacity, ActivityIndicator, StyleSheet } from 'react-native'
import { b, p, f, c } from '../../utils/StyleHelper'
import { vs, xs } from '../../utils/Responsive'
import { blue, light } from '../../utils/Color'
import Svg from '../../utils/Svg'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import FastImage from 'react-native-fast-image'
import Icon from 'react-native-vector-icons/Ionicons';
import { API_URL } from 'react-native-dotenv'

export default class RiwayatKompetisi extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            history_kompetisi: '',
        }
    }

    async componentDidMount() {
        const id_login = await AsyncStorage.getItem('@id_login')
		axios.get(API_URL+'/main/history_kompetisi', {params:{
            id_group: this.props.navigation.getParam('id_group'),
            id_login: id_login
		}})
		.then(result => {
			// alert(JSON.stringify(result))
			this.setState({
                loading: false,
                history_kompetisi: result.data.data,
            })
			// alert(JSON.stringify(result.data.data))
		})
		.catch(e => {
			console.log("history_kompetisi => "+e)
		})
    }

    render() {
        return (
            <>
                {this.state.loading ?
					<View style={[styles.container_loading, styles.horizontal_loading]}>
						<ActivityIndicator size="large" color="#0000ff"/>
					</View>
					:
					null
				}
                <View style={{ width: '100%', height: '100%', backgroundColor: '#000000' }}>
                    <View style={[ p.row, { width: '100%', height: xs(80) }]}>
                        <View style={[ p.center, { width: xs(50) }]}>
                            <TouchableOpacity onPress={() => this.props.navigation.pop()}>
                                <Icon name="ios-arrow-back" size={35} color={light} />
                            </TouchableOpacity>
                        </View>
                        <View style={[ p.center, b.ml4, { width: xs(220) }]}>
                            <Text style={[ c.light, f._18, { fontFamily: 'lineto-circular-pro-bold' }]}>Riwayat Kompetisi</Text>
                        </View>
                    </View>
                    {this.state.history_kompetisi === undefined ?
                        <View style={[ p.center, { width: '100%', height: xs(250), backgroundColor: '#181B22' }]}>
                            <Svg icon="bendera" size={60} />
                            <Text style={[ c.light, b.mt4 ]}>League ini belum mempunyai aktivitas kompetisi</Text>
                        </View>
                        :
                        <ScrollView showsVerticalScrollIndicator={false} style={{ width: '100%' }}>
                            <View style={{ width: '100%', backgroundColor: '#181B22' }}>
                                <FlatList
                                    extraData={this.state}
                                    data={this.state.history_kompetisi}
                                    keyExtractor={item => item.id_kompetisi}
                                    renderItem={({item}) => (
                                        <TouchableOpacity onPress={() => this.props.navigation.push('Kompetisi', {id_kompetisi: item.id_kompetisi, id_group: item.id_group})} style={[ b.mt2, b.mb3, { width: '100%' }]}>
                                            <View style={[ p.row ]}>
                                                <View style={[ b.ml2, b.mt1 ]}>
                                                    <View style={{ width: xs(60), height: xs(60) }}>
                                                        <FastImage source={{ uri: API_URL+'/src/icon_group/'+item.icon_kompetisi }} style={{ width: '100%', height: '100%' }} />
                                                        <View style={{ 
                                                            width: xs(0), height: xs(0), borderLeftWidth: xs(0), borderRightWidth: xs(15), borderBottomWidth: xs(15), borderStyle: 'solid', borderLeftColor: 'transparent', borderRightColor: 'transparent', borderBottomColor: '#181B22',
                                                            position: "absolute", top: vs(0), left: vs(0), transform: [{rotate: '90deg'}]
                                                        }} />
                                                        <View style={{ 
                                                            width: xs(0), height: xs(0), borderLeftWidth: xs(15), borderRightWidth: xs(0), borderBottomWidth: xs(15), borderStyle: 'solid', borderLeftColor: 'transparent', borderRightColor: 'transparent', borderBottomColor: '#181B22',
                                                            position: "absolute", top: vs(45), left: vs(0), transform: [{rotate: '90deg'}]
                                                        }} />
                                                        <View style={{ 
                                                            width: xs(0), height: xs(0), borderLeftWidth: xs(15), borderRightWidth: xs(0), borderBottomWidth: xs(15), borderStyle: 'solid', borderLeftColor: 'transparent', borderRightColor: 'transparent', borderBottomColor: '#181B22',
                                                            position: "absolute", top: vs(0), left: vs(45), transform: [{rotate: '270deg'}]
                                                        }} />
                                                        <View style={{ 
                                                            width: xs(0), height: xs(0), borderLeftWidth: xs(15), borderRightWidth: xs(0), borderBottomWidth: xs(15), borderStyle: 'solid', borderLeftColor: 'transparent', borderRightColor: 'transparent', borderBottomColor: '#181B22',
                                                            position: "absolute", top: vs(45), left: vs(45)
                                                        }} />
                                                    </View>
                                                </View>
                                                <View style={[ b.ml3, b.mt1, b.mb1, { width: xs(250) }]}>
                                                    <TouchableOpacity onPress={() => this.props.navigation.push('Kompetisi', {id_kompetisi: item.id_kompetisi, id_group: item.id_group})}>
                                                        <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>{item.nama_kompetisi}</Text>
                                                    </TouchableOpacity>
                                                    <View style={[ p.row, b.mt1 ]}>
                                                        <View>
                                                            <Svg icon="tanggal" size={20} />
                                                        </View>
                                                        <View>
                                                            <Text style={[ c.light, b.ml2 ]}>{item.tgl_berakhir.split('-').reverse().join('/')}</Text>
                                                        </View>
                                                    </View>
                                                    <View style={[ p.row, b.mt1 ]}>
                                                        <View>
                                                            <Svg icon="mengikuti" size={20} />
                                                        </View>
                                                        <View>
                                                            <Text style={[ c.light, b.ml2 ]}>{item.peserta} Peserta</Text>
                                                        </View>
                                                    </View>
                                                    <View style={[ b.mt2, { borderWidth: 1, borderColor: light }]} />
                                                    <View style={[ b.mt2 ]}>
                                                        <View>
                                                            <Text style={[ c.light ]}>Pemenang: {item.pemenang}</Text>
                                                        </View>
                                                    </View>
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                    )}
                                />
                            </View>
                        </ScrollView>
                    }
                </View>
            </>
        )
    }
}

const styles = StyleSheet.create({
	// loading centered screenn
	container_loading: {
	  flex: 1,
	  position:'absolute',
	  zIndex:1,
	  width: '100%',
	  height: '100%',
	  justifyContent: "center",
	  backgroundColor: 'rgba(0,0,0,0.7)'
	},
	horizontal_loading: {
	  flexDirection: "row",
	  justifyContent: "space-around",
	  padding: 10
	},
})