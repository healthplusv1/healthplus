import React from 'react'
import { View, Text, ScrollView, TouchableOpacity, FlatList } from 'react-native'
import { b, p, f, c } from '../../utils/StyleHelper'
import { vs, xs } from '../../utils/Responsive'
import { blue, light, grey, softBlue } from '../../utils/Color'
import {NavigationEvents} from 'react-navigation'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'
import FastImage from 'react-native-fast-image'
import Icon from 'react-native-vector-icons/Ionicons'
import { API_URL } from 'react-native-dotenv'

export default class Mengikuti extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            list_teman: '',
            id_login: '',
        }
    }

    async componentDidMount() {
        const id_login = await AsyncStorage.getItem('@id_login')
        this.setState({ id_login: id_login })
        axios.get(API_URL+'/main/get_all_friend', {params: {
            id_login_from: this.props.navigation.getParam('id_login')
        }})
        .then(res => {
            this.setState({
                list_teman: res.data.data
            })
            // alert(JSON.stringify(res))
        })
    }

    async unfriend(id_login_to){
        const id_login_from = await AsyncStorage.getItem('@id_login')
        axios.get(API_URL+'/main/unfriend', {params: {
            id_login_from: id_login_from,
            id_login_to: id_login_to
        }})
        .then(res => {
            this.componentDidMount()
        })
    }

    async cancel_request_friend(id_login_to){
        const id_login_from = await AsyncStorage.getItem('@id_login')
        axios.get(API_URL+'/main/cancel_request_friend', {params: {
            id_login_from: id_login_from,
            id_login_to: id_login_to
        }})
        .then(res => {
            this.componentDidMount()
        })
    }

    render() {
        return (
            <>
                <NavigationEvents onDidFocus={() => this.componentDidMount()} />
                <View style={{ width: '100%', height: xs(80), backgroundColor: '#000' }}>
                    <View style={[ p.row, b.ml4, b.mt4 ]}>
                        <View style={{ width: xs(55) }}>
                            <TouchableOpacity onPress={() => this.props.navigation.pop()}>
                                <Icon name="ios-arrow-back" size={40} color={light} />
                            </TouchableOpacity>
                        </View>
                        <View style={[ b.ml2, p.center, { width: xs(200) }]}>
                            <Text style={[ c.light, f.bold, f._18 ]}>Teman</Text>
                        </View>
                    </View>
                </View>
                <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#181B22' }}>
                    <View style={{ width: '100%' }}>
                        <FlatList
                            extraData={this.state}
							data={this.state.list_teman}
							keyExtractor={item => item.id_login}
							renderItem={ ({item}) => (
                                <>
                                    <View style={[ p.row, p.center, b.mt2, { width: '100%', height: xs(60) }]}>
                                        <View style={[ p.row, { width: xs(180) }]}>
                                            <FastImage source={item.url_gambar === null ? { uri: API_URL+'/src/avatar/default.png' } : { uri: API_URL+'/src/avatar/'+item.url_gambar }} style={{ width: xs(50), height: xs(50) }} />
                                            <TouchableOpacity onPress={() => this.props.navigation.push('ProfTracking', { id_login: item.id_login })}>
                                                <Text style={[ b.mt3, b.ml2, c.light ]}>{item.nama}</Text>
                                            </TouchableOpacity>
                                        </View>
                                        <View style={[ p.alignEnd, b.ml2, { width: xs(150) }]}>
                                        { this.state.id_login != this.props.navigation.getParam('id_login') ?
                                            null
                                            :
                                            (item.status === 'request' ?
                                                <TouchableOpacity onPress={() => this.cancel_request_friend(item.id_login)} style={[ p.center, b.roundedLow, { height: xs(30), borderWidth: 2, borderColor: '#555555', backgroundColor: '#000' }]}>
                                                    <Text style={[ c.light, f.bold, b.ml1, b.mr1 ]}>Batal Meminta</Text>
                                                </TouchableOpacity>
                                                    :
                                                <TouchableOpacity onPress={() => this.unfriend(item.id_login)} style={[ p.center, b.roundedLow, { height: xs(30), borderWidth: 2, borderColor: '#555555', backgroundColor: '#000' }]}>
                                                    <Text style={[ c.light, f.bold, b.ml1, b.mr1 ]}>Berhenti Ikuti</Text>
                                                </TouchableOpacity>
                                            )
                                        }
                                        </View>
                                    </View>
                                    <View style={[ b.mt2, { borderWidth: 1, borderColor: '#E0E3E8' }]} />
                                </>
                            )}
                        />
                    </View>
                </ScrollView>
            </>
        )
    }
}