import React from 'react'
import { Text, View, TouchableOpacity, Image, Alert, StyleSheet, ActivityIndicator } from 'react-native'
import { b, p, f, c } from '../../utils/StyleHelper';
import { vs, xs } from '../../utils/Responsive';
import { blue, light, grey, softBlue } from '../../utils/Color';
import Share from 'react-native-share';
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios';
import {NavigationEvents} from 'react-navigation';
import FastImage from 'react-native-fast-image'
import { API_URL } from 'react-native-dotenv'

export default class Postingan extends React.Component {

	constructor(props){
		super(props);
		this.state = {
            suka: '....',
			liked: false,
			id_login: '',
			id_post_group: '',
			like_post: null
		}
	}

	async componentDidMount(){
		const id_login = await AsyncStorage.getItem('@id_login')
		this.setState({ 
			id_login: id_login,
			isLoading: true
		})
		// alert(this.props.post_gambar)
	    axios.get(API_URL+'/main/get_funstation_latest_suka', {params:{
  			id_post_group: this.props.id
  		}})
  		.then(result => {
	    //   alert(JSON.stringify(result))
	      this.setState({
			suka: result.data.data.suka,
			isLoading: false
	      })
		})
	}

	async _increaseValue(){
		const id_login = await AsyncStorage.getItem('@id_login');

		axios.get(API_URL+'/main/insert_like_post', {params:{
			id_login: id_login,
			id_post_group: this.props.id
		}})
		.then(resp => {
		if (resp.data.status === 'true') {
			// Alert.alert("Berhasil")
			if (this.props.like_id_login === null) {
				this.setState({
					suka: '...',
					id_login: null
				})	
			}else{
				this.setState({
					suka: '...',
					id_login: id_login
				})	
			}
			axios.get(API_URL+'/main/get_funstation_latest_suka', {params:{
				id_post_group: this.props.id
			}})
			.then(result => {
				// alert(JSON.stringify(result))
				this.setState({
				suka: result.data.data.suka
				})
			})
			.catch(e => {
				if (e.message === 'Network Error') {
				Alert.alert('Tidak Ada Koneksi Internet','Mohon Periksa Koneksi Internet Anda Lalu Restart Aplikasi')
				// Alert.alert('Network Error','Please Check Your Internet Connection then Restart The App')
				}else{
				// Alert.alert('', JSON.stringify(e.message))
				}
			})
		} else {
			Alert.alert("Gagal")
		}
		})
		.catch(err => {
			// Alert.alert("Error")
		})
	}
	  
	async _decreaseValue() {
		const id_login = await AsyncStorage.getItem('@id_login')

		axios.get(API_URL+'/main/delete_like_post', {params:{
			id_login: id_login,
			id_post_group: this.props.id
		}})
		.then(resp => {
			if (resp.data.status === 'true') {
				// Alert.alert("Berhasil")
				if (this.props.like_id_login === null) {
					this.setState({
						suka: '...',
						id_login: id_login
					})	
				}else{
					this.setState({
						suka: '...',
						id_login: null
					})	
				}
				axios.get(API_URL+'/main/get_funstation_latest_suka', {params:{
					id_post_group: this.props.id
				}})
				.then(result => {
				// alert(JSON.stringify(result))
				this.setState({
				  suka: result.data.data.suka
				})
			  })
			  .catch(e => {
				if (e.message === 'Network Error') {
				  Alert.alert('Tidak Ada Koneksi Internet','Mohon Periksa Koneksi Internet Anda Lalu Restart Aplikasi')
				  // Alert.alert('Network Error','Please Check Your Internet Connection then Restart The App')
				}else{
				  // Alert.alert('', JSON.stringify(e.message))
				}
			  })
			} else {
				Alert.alert("Gagal")
			}
		})
		.catch(err => {
			// Alert.alert("Error")
		})
	}

	render() {
		if (this.state.isLoading) {
		return (
            <View style={[styles.container_loading, styles.horizontal_loading]}>
              <ActivityIndicator size="large" color="#0000ff" />
            </View>
            )
		}else{
			return (
				<>
					<NavigationEvents onDidFocus={() => this.componentDidMount()} />
					<View style={[ p.row, b.mt3 ]}>
						<View style={{ width: xs(250) }}>
							<View style={[ p.row ]}>
								<Image source={this.props.url_avatar === null ? { uri: API_URL+'/src/avatar/default.png' } : { uri: API_URL+'/src/avatar/'+this.props.url_avatar }} style={[ b.ml4, { width: xs(50), height: xs(50) }]} />
								<View style={[ b.ml3, b.mt2 ]}>
									<TouchableOpacity onPress={eval(this.props.action)}>
										<Text style={[ f.bold, c.light ]}> {this.props.nama} </Text>
									</TouchableOpacity>
									<Text style={[ f._12, { color: '#CCCCCC' }]}> {this.props.tanggal_post} </Text>
								</View>
							</View>
							<Text style={[ b.ml4, b.mt2, f._12, c.light ]}> {this.props.deskripsi} </Text>
							<View style={[ p.row, b.ml4, b.mt2, c.light ]}>
								<View style={[ p.row ]}>
										{this.props.like_id_login === this.state.id_login ?
											<TouchableOpacity activeOpacity={.5} onPress={() => {this._decreaseValue()}}>
												<View style={[ p.row ]}>
													<Image source={require('../../asset/loved.png')} style={{ width: xs(20), height: xs(20) }} />
												</View>
											</TouchableOpacity>
											: 
											<TouchableOpacity activeOpacity={.5} onPress={() => {this._increaseValue()}}>
												<View style={[ p.row ]}>
													<Image source={require('../../asset/love.png')} style={{ width: xs(20), height: xs(20), tintColor: light }} />
												</View>
											</TouchableOpacity>
										}
									<Text style={[ c.light ]}> {this.state.suka} </Text>
								</View>
								<View style={[ p.row, b.ml3 ]}>
									<TouchableOpacity onPress={eval(this.props.comment)}>
										<Image source={require('../../asset/comment.png')} style={{ width: xs(20), height: xs(20), tintColor: light }} />
									</TouchableOpacity>
									<Text style={[ c.light ]}> {this.props.komentar} </Text>
								</View>
								<View style={[ p.row, b.ml3 ]}>
								{this.props.post_gambar === null ? 
									null
									:
									<>
									<TouchableOpacity>
										<Image source={require('../../asset/not_found.png')} style={{ width: xs(20), height: xs(20), tintColor: light }} />
									</TouchableOpacity>
									<Text style={[ c.light ]}> 1 </Text>
									</>
								}
								</View>
							</View>
						</View>
						<View style={[ b.ml1 ]}>
							<FastImage source={this.props.post_gambar === null ? null : { uri: API_URL+'/src/icon_group/'+this.props.post_gambar }} style={{ width: xs(80), height: xs(80) }} />
						</View>
					</View>
					<View style={[ b.mt2, { width: '100%', borderWidth: 1, borderColor: '#EBEBEB' }]} />
				</>
			)
		}
	}
}
const styles = StyleSheet.create({
	// loading centered screenn
	container_loading: {
	  flex: 1,
	  justifyContent: "center"
	},
	horizontal_loading: {
	  flexDirection: "row",
	  justifyContent: "space-around",
	  padding: 10
	},
  
	actionButtonIcon: {
	  fontSize: 20,
	  height: 22,
	  color: 'white',
	},
  });