import React from 'react'
import { Text, View, Image, TouchableOpacity, TextInput, ScrollView, Alert, Button, FlatList, BackHandler, ActivityIndicator, StyleSheet, PermissionsAndroid } from 'react-native'
import { b, p, f, c } from '../../utils/StyleHelper'
import { vs, xs } from '../../utils/Responsive'
import { blue, light, grey, softBlue } from '../../utils/Color'
import DateTimePicker from '@react-native-community/datetimepicker';
import DateTimePicker2 from '@react-native-community/datetimepicker';
import Moment from 'moment';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import Svg from '../../utils/Svg'
import Dialog, { DialogFooter, DialogButton, DialogContent, DialogTitle } from 'react-native-popup-dialog';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage'
import FastImage from 'react-native-fast-image'
import analytics from '@react-native-firebase/analytics'
import { API_URL } from 'react-native-dotenv'

const options = {
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

export default class BuatKompetisi extends React.Component {
	_didFocusSubscription;
	_willBlurSubscription;

	constructor(props){
		super(props);
		this._didFocusSubscription = props.navigation.addListener('didFocus', payload =>
	      BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
	    );
		this.state = {
			nama_kompetisi: '',
			deskripsi: '',
			poin: '',
			juara1: '50',
			juara2: '30',
			juara3: '20',
			startDate: new Date().setDate(new Date().getDate()),
			endDate: new Date().setDate(new Date().getDate()+1),
			visible: false,
			challengeVisible: false,
			selectImageOneVisible: false,
			selectImageTwoVisible: false,
			semi_selected_challenge: '',
			selected_challenge: 'Set',
			penyelenggara: 'Pilih Liga Penyelenggara*',
			id_group: '',
			semi_pilih_penyelenggara: '',
			semi_pilih_id_group: '',
			list_penyelengara: '',
			selected_penyelenggara: '',
			url_gambar: null,
			list_jenis_challenge: [
				{nama_challenge: 'Sit Up', icon: require('../../asset/sit_up.png')},
				{nama_challenge: 'Back Up', icon: require('../../asset/back_up.png')},
				{nama_challenge: 'Pull Up', icon: require('../../asset/pull_up.png')},
				{nama_challenge: 'Push Up', icon: require('../../asset/push_up.png')},
				{nama_challenge: 'Squats', icon: require('../../asset/squats.png')},
				{nama_challenge: 'Squat Jump', icon: require('../../asset/squat_jump.png')},
				{nama_challenge: 'Jumping Jack', icon: require('../../asset/jumping_jaks.png')},
				{nama_challenge: 'Mountain Climbers', icon: require('../../asset/mountain_climber.png')},
				{nama_challenge: 'Plank', icon: require('../../asset/plank.png')},
			],
			month:[
				{bulan: 'Jan'},
				{bulan: 'Feb'},
				{bulan: 'Mar'},
				{bulan: 'Apr'},
				{bulan: 'Mei'},
				{bulan: 'Jun'},
				{bulan: 'Jul'},
				{bulan: 'Agu'},
				{bulan: 'Sep'},
				{bulan: 'Okt'},
				{bulan: 'Nov'},
				{bulan: 'Des'}
			],
			Day: [
				{hari: 'Min'},
				{hari: 'Sen'},
				{hari: 'Sel'},
				{hari: 'Rab'},
				{hari: 'Kam'},
				{hari: 'Jum'},
				{hari: 'Sab'}
			]
		}
	}

	async componentDidMount(){
		await analytics().logEvent('create_challenge', {
			item: 'challenge_id'
		})
		await analytics().setUserProperty('user_create_challenge', 'null')
		
		this._willBlurSubscription = this.props.navigation.addListener('willBlur', payload =>
	      BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
	    );
		const id_login = await AsyncStorage.getItem('@id_login')
		axios.get(API_URL+'/main/get_penyelenggara', {params:{
			id_login: id_login
		}})
		.then(resp => {
			this.setState({
				list_penyelengara: resp.data.data
			})
		})
		.catch(e => console.log(e))
	}

	launch = async () => {
		try {
			const granted = await PermissionsAndroid.requestMultiple([
				PermissionsAndroid.PERMISSIONS.CAMERA,
				PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE
			])
			.then((result) => {
				if (result['android.permission.CAMERA'] && result['android.permission.WRITE_EXTERNAL_STORAGE'] === 'granted') {
					this.setState({ selectImageOneVisible: true })
				} else {
					console.log('Permissions denied')
				}
			})
		} catch (error) {
			console.warn(error)
		}
	}

	launch2 = async () => {
		try {
			const granted = await PermissionsAndroid.requestMultiple([
				PermissionsAndroid.PERMISSIONS.CAMERA,
				PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE
			])
			.then((result) => {
				if (result['android.permission.CAMERA'] && result['android.permission.WRITE_EXTERNAL_STORAGE'] === 'granted') {
					this.setState({ selectImageTwoVisible: true })
				} else {
					console.log('Permissions denied')
				}
			})
		} catch (error) {
			console.warn(error)
		}
	}

	camera = () => {
		launchCamera(options, (response) => {
			console.log('Response = ', response);
  
			if (response.didCancel) {
			  console.log('User cancelled image picker');
			} else if (response.error) {
			  console.log('ImagePicker Error: ', response.error);
			} else if (response.customButton) {
			  console.log('User tapped custom button: ', response.customButton);
			} else {
			  console.log(response.assets[0].fileName);
			  this.setState({
				uri: {uri: response.assets[0].uri},
				icon_kompetisi: response.assets[0].uri,
				fileName: response.assets[0].fileName,
				selectImageOneVisible: false
			  });
			}
		});
	}

	chooseFile = () => {
		launchImageLibrary(options, (response) => {
			console.log('Response = ', response);
  
			if (response.didCancel) {
			  console.log('User cancelled image picker');
			} else if (response.error) {
			  console.log('ImagePicker Error: ', response.error);
			} else if (response.customButton) {
			  console.log('User tapped custom button: ', response.customButton);
			} else {
			  console.log(response.assets[0].fileName);
			  this.setState({
				uri: {uri: response.assets[0].uri},
				icon_kompetisi: response.assets[0].uri,
				fileName: response.assets[0].fileName,
				selectImageOneVisible: false
			  });
			}
		});
	}

	camera2 = () => {
		launchCamera(options, (response) => {
			console.log('Response = ', response);
  
			if (response.didCancel) {
			  console.log('User cancelled image picker');
			} else if (response.error) {
			  console.log('ImagePicker Error: ', response.error);
			} else if (response.customButton) {
			  console.log('User tapped custom button: ', response.customButton);
			} else {
			  console.log(response.assets[0].fileName);
			  this.setState({
				uri: {uri: response.assets[0].uri},
				gambar_cover: response.assets[0].uri,
				fileName: response.assets[0].fileName,
				selectImageTwoVisible: false
			  });
			}
		});
	}

	chooseFile2 = () => {
		launchImageLibrary(options, (response) => {
			console.log('Response = ', response);
  
			if (response.didCancel) {
			  console.log('User cancelled image picker');
			} else if (response.error) {
			  console.log('ImagePicker Error: ', response.error);
			} else if (response.customButton) {
			  console.log('User tapped custom button: ', response.customButton);
			} else {
			  console.log(response.assets[0].fileName);
			  this.setState({
				uri: {uri: response.assets[0].uri},
				gambar_cover: response.assets[0].uri,
				fileName: response.assets[0].fileName,
				selectImageTwoVisible: false
			  });
			}
		});
	}

	renderFileUri() {
	    if (this.state.icon_kompetisi) {
	      return <FastImage
	        source={{ uri: this.state.icon_kompetisi }}
	        style={[ b.bordered, { width: xs(50), height: xs(50) }]}
	      />
	    } else {
	      return <FastImage
	        source={require('../../asset/camera.png')}
	        style={{ width: xs(50), height: xs(50) }}
	      />
	    }
	}

	renderFile() {
		if (this.state.gambar_cover) {
	      return <FastImage
	        source={{ uri: this.state.gambar_cover }}
	        style={[ b.bordered, { width: xs(50), height: xs(50) }]}
	      />
	    } else {
	      return <FastImage
	        source={require('../../asset/camera.png')}
	        style={{ width: xs(50), height: xs(50) }}
	      />
	    }
	}

	handleBackPress = () => {
	    this.setState({visible:false})
	};

	createFormDataPhoto = (icon_kompetisi, gambar_cover, id_login) => {
	    const data = new FormData();
	    data.append('icon_kompetisi', {
	      name: (icon_kompetisi.fileName == null)? 'myphoto': icon_kompetisi.fileName,
	      type: icon_kompetisi.type,
	      uri: icon_kompetisi.uri
	    });
	    data.append('gambar_cover', {
	      name: (gambar_cover.fileName == null)? 'myphoto': gambar_cover.fileName,
	      type: gambar_cover.type,
	      uri: gambar_cover.uri
	    });
	    data.append("nama_kompetisi", this.state.nama_kompetisi);
	    data.append("jenis_challenge", this.state.selected_challenge);
	    data.append("poin", this.state.poin);
	    data.append("tgl_dimulai", this.state.startDate);
	    data.append("tgl_berakhir", this.state.endDate);
	    data.append("id_group", this.state.id_group);
	    data.append("deskripsi", this.state.deskripsi);
		data.append("id_login", id_login);
		data.append("hadiah_juara1", this.state.juara1);
		data.append("hadiah_juara2", this.state.juara2);
		data.append("hadiah_juara3", this.state.juara3);
	    return data;
	};

	async upload_kompetisi(){
		if( (Number(this.state.juara1) + Number(this.state.juara2) + Number(this.state.juara3)) !== 100 ){
			console.log((Number(this.state.juara1) + Number(this.state.juara2) + Number(this.state.juara3)))
			Alert.alert('Total Reward Harus 100%', 'saat ini '+(Number(this.state.juara1) + Number(this.state.juara2) + Number(this.state.juara3))+"%")
			return;
		}
		const id_login = await AsyncStorage.getItem('@id_login')
		// alert(JSON.stringify(this.state.startDate))
		// alert(JSON.stringify(this.state.endDate))
		if (this.state.nama_kompetisi === '' || this.state.poin === '' || this.state.startDate === '' || this.state.endDate === '' || this.state.id_group === '') {
			Alert.alert('Masukkan data yang kurang untuk bisa membuat kompetisimu')
		} else {
			this.setState({
				isLoading: true
			})
		}

	    fetch(API_URL+'/main/insert_kompetisi', {
	      method: 'POST',
	      headers: {
	        'Content-Type': 'multipart/form-data'
	      },
	      body: this.createFormDataPhoto(this.state.icon_kompetisi, this.state.gambar_cover, id_login)
	    })
		.then(response => response.json())
		.then(resp => {
	        // alert(JSON.stringify(resp));
			this.setState({
				isLoading: false
			})
			if (resp.status === 'true') {
				Alert.alert(
					"",
					"Kompetisimu berhasil dibuat",
					[
						{ text: 'OK', onPress: () => this.props.navigation.navigate('Home') }
					]
				)
			} else if (resp.status === 'not_enough_point') {
				Alert.alert("Poin Anda Tidak Cukup")
			} else {
				Alert.alert("Anda Gagal Membuat Kompetisi")
			}
	        
		})
		.catch(error => {
	        console.log('Upload Error', error);
	        // alert(JSON.stringify(error))
		});
	}

	updateInputVal = (val, prop) => {
	    const state = this.state;
	    state[prop] = val;
	    this.setState(state);
	}

	memilih_penyelenggara(id_group, nama_group, gambar){
		this.setState({
			semi_url_gambar: gambar,
			semi_pilih_id_group: id_group,
			semi_pilih_penyelenggara: nama_group,
			selected_penyelenggara: nama_group
		})


		console.log(this.state.selected_penyelenggara)
	}

	memilih_jenis_challenge(nama_challenge){
		this.setState({semi_selected_challenge: nama_challenge})
	}

	render() {
		return (
			<>
				{this.state.isLoading ?
					<View style={[styles.container_loading, styles.horizontal_loading]}>
						<ActivityIndicator size="large" color="#0000ff"/>
					</View>
					:
					null
				}
			  <Dialog
			  	dialogTitle={<DialogTitle style={{ backgroundColor: '#000' }} textStyle={[ c.light ]} title="Pilih Penyelenggara" />}
			    visible={this.state.visible}
			    onTouchOutside={() => this.setState({visible:false})}
				dialogStyle={{ backgroundColor: '#181B22' }}
			    footer={
			      <DialogFooter>
			        <DialogButton
			          text="BATAL"
			          onPress={() => { this.setState({visible:false}) }}
			        />
			        <DialogButton
			          text="PILIH"
			          onPress={() => { 
			          	if (this.state.selected_penyelenggara === '') {
			          		Alert.alert('','Anda Belum Memilih Penyelenggara')
			          	} else {
				          	this.setState({
				          		visible:false,
				          		url_gambar: this.state.semi_url_gambar,
								id_group: this.state.semi_pilih_id_group,
								penyelenggara: this.state.semi_pilih_penyelenggara
							})
			          	}
			          }}
			        />
			      </DialogFooter>
			    }
			  >
			    <DialogContent>
			      <View style={{ width: xs(250), paddingTop: vs(20), maxHeight: xs(330) }}>
	              <ScrollView showsVerticalScrollIndicator={false}>
			      	<TouchableOpacity onPress={() => {
			      		this.setState({visible:false})
			      		this.props.navigation.navigate('BuatGrup')
			      	}}>
			      		<View style={[ p.center, { padding: vs(10), backgroundColor: '#555555', marginBottom: vs(10), borderRadius: vs(5) }]}>
					      	<View style={[ p.row ]}>
						      	<View>
							      	<FastImage source={require('../../asset/plus1.png')} style={{ width: xs(30), height: xs(30), tintColor: light }} />
						      	</View>
						      	<View style={[ p.center, b.ml1 ]}>
						      		<Text style={[ c.light ]}>Buat Liga</Text>
						      	</View>
					      	</View>
			      		</View>
			      	</TouchableOpacity>
			      	<FlatList
			            data={this.state.list_penyelengara}
			            keyExtractor={item => item.id_group}
			            renderItem={({item}) => (
			            <TouchableOpacity onPress={() => {
			            	this.memilih_penyelenggara(item.id_group, item.nama_group, item.url_gambar)
			            }}>
			              <View style={[ p.row, { padding: vs(10), backgroundColor: '#555555', marginBottom: vs(10), borderRadius: vs(5) }]}>
			              	<View>
				              	<FastImage source={item.url_gambar === null ? require('../../asset/icon_group.png') : { uri: API_URL+'/src/icon_group/'+item.url_gambar }} style={{ width: xs(50), height: xs(50) }} />
			              	</View>
			              	<View style={[ b.ml2, p.center, { width: xs(100) }]}>
				              	<Text style={[ c.light ]} numberOfLines={2}>{item.nama_group}</Text>
			              	</View>
			              	<View style={[ b.ml4, p.center, { width: xs(50) }]}>
			              		{this.state.selected_penyelenggara === item.nama_group ?
				              		<FastImage source={require('../../asset/checklist.png')} style={{ width: xs(15), height: xs(15), marginLeft: vs(20) }} />
				              		:
				              		null
			              		}
			              	</View>
			              </View>
			            </TouchableOpacity>
			            )}
			          />
	              </ScrollView>
			      </View>
			    </DialogContent>
			</Dialog>


			<Dialog
			  	dialogTitle={<DialogTitle textStyle={[ c.light ]} style={{ backgroundColor: '#000' }} title="Pilih Jenis Tantangan" />}
			    visible={this.state.challengeVisible}
			    onTouchOutside={() => this.setState({challengeVisible:false})}
				dialogStyle={{ backgroundColor: '#181B22' }}
			    footer={
			      <DialogFooter>
			        <DialogButton
			          text="BATAL"
			          onPress={() => { this.setState({challengeVisible:false}) }}
			        />
			        <DialogButton
			          text="PILIH"
			          onPress={() => { 
			          	if (this.state.semi_selected_challenge === '') {
			          		Alert.alert('','Anda Belum Memilih Jenis Tantangan')
			          	} else {
				          	this.setState({
				          		challengeVisible:false,
								selected_challenge: this.state.semi_selected_challenge
							})
			          	}
			          }}
			        />
			      </DialogFooter>
			    }
			  >
			    <DialogContent>
			      <View style={{ width: xs(250), paddingTop: vs(20), maxHeight: xs(330) }}>
	              <ScrollView showsVerticalScrollIndicator={false}>
			      	<FlatList
			            data={this.state.list_jenis_challenge}
			            keyExtractor={item => item.nama_challenge}
			            renderItem={({item}) => (
			            <TouchableOpacity onPress={() => {
			            	this.memilih_jenis_challenge(item.nama_challenge)
			            }}>
			              <View style={[ p.row, { padding: vs(10), backgroundColor: '#555555', marginBottom: vs(10), borderRadius: vs(5) }]}>
			              	<View>
				              	<Image source={item.icon} style={{ width: xs(80), height: xs(40), tintColor: light }} />
			              	</View>
			              	<View style={[ b.ml2, { width: xs(100) }]}>
				              	<Text style={[ c.light ]}>{item.nama_challenge}</Text>
			              	</View>
			              	<View style={[ p.center, { width: xs(20) }]}>
			              		{this.state.semi_selected_challenge === item.nama_challenge ?
				              		<FastImage source={require('../../asset/checklist.png')} style={{ width: xs(15), height: xs(15), marginLeft: vs(20) }} />
				              		:
				              		null
			              		}
			              	</View>
			              </View>
			            </TouchableOpacity>
			            )}
			          />
	              </ScrollView>
			      </View>
			    </DialogContent>
			</Dialog>

			<Dialog
				visible={this.state.selectImageOneVisible}
				onTouchOutside={() => this.setState({ selectImageOneVisible: false })}
				dialogStyle={{ backgroundColor: light }}
			>
				<DialogContent>
					<View style={{ width: xs(250) }}>
						<Text style={[ f._18, f.bold, b.mt3 ]}>Select Image</Text>
						<TouchableOpacity onPress={() => this.camera()} style={[ b.mt3 ]}>
							<Text>Take Photo...</Text>
						</TouchableOpacity>
						<TouchableOpacity onPress={() => this.chooseFile()} style={[ b.mt3 ]}>
							<Text>Choose from Library...</Text>
						</TouchableOpacity>
						<View style={[ b.mt3, { width: '100%', alignItems: 'flex-end' }]}>
							<TouchableOpacity onPress={() => this.setState({ selectImageOneVisible: false })}>
								<Text style={[ f.bold ]}>CANCEL</Text>
							</TouchableOpacity>
						</View>
					</View>
				</DialogContent>
			</Dialog>

			<Dialog
				visible={this.state.selectImageTwoVisible}
				onTouchOutside={() => this.setState({ selectImageTwoVisible: false })}
				dialogStyle={{ backgroundColor: light }}
			>
				<DialogContent>
					<View style={{ width: xs(250) }}>
						<Text style={[ f._18, f.bold, b.mt3 ]}>Select Image</Text>
						<TouchableOpacity onPress={() => this.camera2()} style={[ b.mt3 ]}>
							<Text>Take Photo...</Text>
						</TouchableOpacity>
						<TouchableOpacity onPress={() => this.chooseFile2()} style={[ b.mt3 ]}>
							<Text>Choose from Library...</Text>
						</TouchableOpacity>
						<View style={[ b.mt3, { width: '100%', alignItems: 'flex-end' }]}>
							<TouchableOpacity onPress={() => this.setState({ selectImageTwoVisible: false })}>
								<Text style={[ f.bold ]}>CANCEL</Text>
							</TouchableOpacity>
						</View>
					</View>
				</DialogContent>
			</Dialog>
			
			<ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#181B22' }}>
				<View style={[ p.center, b.shadowHigh, { width: '100%', height: xs(80), backgroundColor: '#000' }]}>
					<View style={[ p.row, { width: '100%' }]}>
						<TouchableOpacity onPress={() => this.props.navigation.navigate('Home')} style={[ b.ml4 ]}>
							<Image source={require('../../asset/back1.png')} style={{ width: xs(25), height: xs(25), tintColor: light }} />
						</TouchableOpacity>
						<Text style={[ f.bold, f._18, b.ml4, c.light ]}> Buat Tantangan </Text>
					</View>
				</View>
				<View style={{ width: '100%' }}>
					<Text style={[ b.mt4, f._12, b.ml2, { color: light }]}> Atur nama Tantangan, poin, serta tanggal dimulai dan berakhir </Text>
					<View style={[ b.mt2, { width: '100%', backgroundColor: '#555555' }]}>
						<Text style={[ b.ml2, b.mt3, c.light ]}> Nama Tantangan *</Text>
						<TextInput style={[ b.ml2, c.light, { width: xs(300) }]}
							placeholder={"Tulis nama tantanganmu"}
							placeholderTextColor={ c.grey }
							value={this.state.nama_kompetisi}
							onChangeText={(val) => this.updateInputVal(val, 'nama_kompetisi')} />
					</View>
					<Text style={[ b.ml2, b.mt3, { color: light }]}> Tantangan </Text>
					<View style={[ b.mt2, p.row, { width: '100%', height: xs(50), backgroundColor: '#555555' }]}>
						<View style={[ b.ml2, { width: xs(200) }]}>
							<Text style={[ b.mt3, c.light ]}> Pilih jenis tantangan *</Text>
						</View>
						<View style={[ b.ml1, { width: xs(135) }]}>
							<TouchableOpacity onPress={() => { this.setState({challengeVisible:true}) }}>
								<Text style={[ c.blue, b.mt3, p.textRight, f.bold ]}> {this.state.selected_challenge} </Text>
							</TouchableOpacity>
						</View>
					</View>
					<Text style={[ b.ml2, b.mt3, { color: light }]}> Poin </Text>
					<View style={[ b.mt2, p.row, { width: '100%', height: xs(50), backgroundColor: '#555555' }]}>
						<View style={[ p.row, b.ml2, { width: xs(250) }]}>
							<Text style={[ b.mt3, c.light ]}> Poin yang di butuhkan *</Text>
							<FastImage source={require('../../asset/warning.png')} style={[ b.mt3, b.ml1, { width: xs(20), height: xs(20) }]} />
						</View>
						<TextInput style={[ c.blue, f.bold, c.light, { marginLeft: vs(35), width: xs(50) }]}
							placeholder={"Set"}
							textAlign={'right'}
							placeholderTextColor={blue}
							keyboardType={'numeric'}
							value={this.state.poin}
							onChangeText={(val) => this.updateInputVal(val, 'poin')} />
					</View>
					<Text style={[ b.ml2, b.mt3, { color: light }]}> Tanggal dimulai dan berakhir </Text>
					<View style={[ b.mt2, { width: '100%', height: xs(100), backgroundColor: '#555555' }]}>
						<View style={[ p.row ]}>
							<Text style={[ b.ml2, b.mt3, c.light, { width: xs(200) }]}> Tanggal dimulai *</Text>
							<TouchableOpacity onPress={() => {this.setState({startDateActive: true})}}>
								<Text style={[ c.blue, b.mt3, f.bold, b.ml1, p.textRight, { width: xs(135) }]}> {typeof this.state.startDate === 'object' ? 
								this.state.Day[new Date(Date.parse(this.state.startDate)).getDay()].hari+', '+new Date(Date.parse(this.state.startDate)).getDate()+' '+this.state.month[new Date(Date.parse(this.state.startDate)).getMonth()].bulan
								:
								this.state.Day[new Date(this.state.startDate).getDay()].hari+', '+new Date(this.state.startDate).getDate()+' '+this.state.month[new Date(this.state.startDate).getMonth()].bulan
							} </Text>
							</TouchableOpacity>
						</View>
						<View style={[ b.mt3, { borderWidth: 1, width: '100%', borderColor: grey }]} />
						<View style={[ p.row ]}>
							<Text style={[ b.ml2, b.mt3, c.light, { width: xs(200) }]}> Tanggal berakhir *</Text>
							<TouchableOpacity onPress={() => {this.setState({endDateActive: true})}}>
								<Text style={[ c.blue, b.mt3, f.bold, b.ml1, p.textRight, { width: xs(135) }]}> {typeof this.state.endDate === 'object' ? 
								this.state.Day[new Date(Date.parse(this.state.endDate)).getDay()].hari+', '+new Date(Date.parse(this.state.endDate)).getDate()+' '+this.state.month[new Date(Date.parse(this.state.endDate)).getMonth()].bulan
								:
								this.state.Day[new Date(this.state.endDate).getDay()].hari+', '+new Date(this.state.endDate).getDate()+' '+this.state.month[new Date(this.state.endDate).getMonth()].bulan
							} </Text>
							</TouchableOpacity>
						</View>
					</View>
			{this.state.startDateActive === true ? 
				<DateTimePicker mode="date" value={this.state.startDate} minimumDate={new Date()} maximumDate={
					(typeof this.state.endDate === undefined ?
						this.state.endDate.setDate(this.state.endDate.getDate()-1)
						:
						new Date(this.state.endDate).setDate(new Date(this.state.endDate).getDate()-1)
					)
					} onChange={data => {
					if (data.type === 'dismissed') {
						this.setState({
							startDateActive: false
						})
					} else {
						this.setState({
							startDate: data.nativeEvent.timestamp,
							startDateActive: false
						})
					}
				}} />
				:
				null
			}

			{this.state.endDateActive === true ? 
				<DateTimePicker2 mode="date" value={this.state.endDate} minimumDate={
					(typeof this.state.startDate === undefined ?
						this.state.startDate.setDate(this.state.startDate.getDate()+1)
						:
						new Date(this.state.startDate).setDate(new Date(this.state.startDate).getDate()+1)
					)
					} onChange={data => {
					if (data.type === 'dismissed') {
						this.setState({
							endDateActive: false
						})
					} else {
						this.setState({
							endDate: data.nativeEvent.timestamp,
							endDateActive: false
						})
					}
				}} />
				:
				null
			}
					<Text style={[ b.ml2, b.mt3, { color: light }]}> Penyelenggara </Text>
					<TouchableOpacity onPress={() => {
				      this.setState({ visible: true });
				    }}>
						<View style={[ b.mt2, p.row, { width: '100%', height: xs(70), backgroundColor: '#555555' }]}>
							<FastImage source={this.state.url_gambar === null ? require('../../asset/icon_group.png') : { uri: API_URL+'/src/icon_group/'+this.state.url_gambar } } style={[ b.ml2, b.mt2, { width: xs(50), height: xs(50) }]} />
							<Text style={[ b.ml4, { marginTop: vs(25), color: light }]}> {this.state.penyelenggara} </Text>
						</View>
					</TouchableOpacity>
					<Text style={[ b.ml2, b.mt3, { color: light }]}> Ikon dan Cover </Text>
					<View style={[ b.mt2, { width: '100%', backgroundColor: '#555555' }]}>
						<View style={[ p.row ]}>
							<View style={[ b.ml2, b.mt3, p.center, { width: xs(100) }]}>
								<Text style={[ c.light ]}> Icon Tantangan </Text>
							</View>
							<View style={[ b.mt1, { marginLeft: vs(160), width: xs(50), height: xs(50) }]}>
								<TouchableOpacity onPress={() => this.launch()}>
									{this.renderFileUri()}
								</TouchableOpacity>
							</View>
						</View>
						<View style={[ b.mt3, { borderWidth: 1, width: '100%', borderColor: grey }]} />
						<View style={[ p.row ]}>
							<View style={[ b.ml2, b.mt3, p.center, { width: xs(100) }]}>
								<Text style={[ c.light ]}> Gambar Cover </Text>
							</View>
							<View style={[ b.mt1, { marginLeft: vs(160), width: xs(50), height: xs(50) }]}>
								<TouchableOpacity onPress={() => this.launch2()}>
									{this.renderFile()}
								</TouchableOpacity>
							</View>
						</View>
						<View style={{ height: xs(15) }} />
					</View>
					<Text style={[ b.ml2, b.mt3, c.light ]}> Reward </Text>
					<View style={[ b.mt2, { width: '100%', backgroundColor: '#555555' }]}>
						<View style={[ p.row, b.ml2 ]}>
							<View style={{ width: xs(200) }}>
								<Text style={[ c.light, b.mt3 ]}>Juara 1</Text>
							</View>
							<TextInput style={[ c.blue, f.bold, c.light, { marginLeft: vs(70), width: xs(50) }]}
								placeholder={"Set"}
								textAlign={'right'}
								placeholderTextColor={blue}
								keyboardType={'numeric'}
								value={this.state.juara1}
								onChangeText={(val) => this.updateInputVal(val, 'juara1')} />
							<Text style={[ c.light, b.mt3 ]}>%</Text>
						</View>
						<View style={{ borderBottomWidth: 1, borderBottomColor: grey }} />
						<View style={[ p.row, b.ml2 ]}>
							<View style={{ width: xs(200) }}>
								<Text style={[ c.light, b.mt3 ]}>Juara 2</Text>
							</View>
							<TextInput style={[ c.blue, f.bold, c.light, { marginLeft: vs(70), width: xs(50) }]}
								placeholder={"Set"}
								textAlign={'right'}
								placeholderTextColor={blue}
								keyboardType={'numeric'}
								value={this.state.juara2}
								onChangeText={(val) => this.updateInputVal(val, 'juara2')} />
							<Text style={[ c.light, b.mt3 ]}>%</Text>
						</View>
						<View style={{ borderBottomWidth: 1, borderBottomColor: grey }} />
						<View style={[ p.row, b.ml2 ]}>
							<View style={{ width: xs(200) }}>
								<Text style={[ c.light, b.mt3 ]}>Juara 3</Text>
							</View>
							<TextInput style={[ c.blue, f.bold, c.light, { marginLeft: vs(70), width: xs(50) }]}
								placeholder={"Set"}
								textAlign={'right'}
								placeholderTextColor={blue}
								keyboardType={'numeric'}
								value={this.state.juara3}
								onChangeText={(val) => this.updateInputVal(val, 'juara3')} />
							<Text style={[ c.light, b.mt3 ]}>%</Text>
						</View>
					</View>
					<Text style={[ b.ml2, b.mt3, { color: light }]}> Deskripsi </Text>
					<View style={[ b.mt2, { width: '100%', height: xs(100), backgroundColor: '#555555' }]}>
						<Text style={[ b.ml2, b.mt3, c.light ]}> Deskripsi Singkat </Text>
						<TextInput style={[ b.ml2, c.light, { width: xs(300), height: xs(50) }]}
							multiline
							placeholder={"Tulis deskripsi singkat untuk Tantanganmu"}
							placeholderTextColor={[c.grey]}
							value={this.state.deskripsi}
							onChangeText={(val) => this.updateInputVal(val, 'deskripsi')} />
					</View>
					<View style={{ width: '100%', height: xs(30) }} />
				</View>
				<TouchableOpacity onPress={() => this.upload_kompetisi()} style={[ p.center, { width: '100%', height: xs(50), backgroundColor: blue }]}>
					<Text style={[ c.light, f.bold, f._18 ]}> Pratinjau </Text>
				</TouchableOpacity>
			</ScrollView>
		</>)
	}
}

const styles = StyleSheet.create({
	// loading centered screenn
	container_loading: {
	  flex: 1,
	  position:'absolute',
	  zIndex:1,
	  width: '100%',
	  height: '100%',
	  justifyContent: "center",
	  backgroundColor: 'rgba(0,0,0,0.7)'
	},
	horizontal_loading: {
	  flexDirection: "row",
	  justifyContent: "space-around",
	  padding: 10
	},
  });