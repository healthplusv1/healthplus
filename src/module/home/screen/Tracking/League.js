import React from 'react'
import { Text, View, TouchableOpacity, Image, ImageBackground, ScrollView, Alert, FlatList, ActivityIndicator, StyleSheet } from 'react-native'
import { b, p, f, c } from '../../utils/StyleHelper'
import { vs, xs } from '../../utils/Responsive'
import { blue, light, grey, softBlue } from '../../utils/Color'
import Svg from '../../utils/Svg'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'
import {NavigationEvents} from 'react-navigation';
import { Container, Tab, Tabs, StyleProvider } from 'native-base';
import { API_URL } from 'react-native-dotenv'

export default class League extends React.Component {

	constructor(props){
		super(props);
		this.state ={
			group: null,
			group1: null,
			group2: null,
			loading: true
		}
	}

	async componentDidMount() {
		const id_login = await AsyncStorage.getItem('@id_login')
		axios.get(API_URL+'/main/get_admin_group', {params:{
			id_login: id_login
		}})
		.then(result => {
			this.setState({
				loading: false,
				group: result.data.data
			})
			// alert(JSON.stringify(result))
		})
		.catch(e => {
			console.log('get_admin_group => '+e)
		})

		axios.get(API_URL+'/main/get_anggota_group', {params:{
			id_login: id_login
		}})
		.then(result => {
			this.setState({
				loading: false,
				group1: result.data.data
			})
			// alert(JSON.stringify(result.data.data))
		})
		.catch(e => {
			console.log('get_anggota_group => '+e)
		})
	}

	render() {
		if (this.state.loading) {
			return (
            <View style={[styles.container_loading, styles.horizontal_loading]}>
              <ActivityIndicator size="large" color="#0000ff" />
            </View>
            )
		}else{
			return (
			<>
				<NavigationEvents onDidFocus={() => this.componentDidMount()} />
				<Container>
					<Tabs tabBarUnderlineStyle={{borderBottomWidth: 2, borderBottomColor: blue}}>
						<Tab heading='Kompetisi' tabStyle={{backgroundColor: 'white'}} textStyle={{color: '#000000'}} 
					      activeTabStyle={{backgroundColor: '#FFFFFF'}}
					      activeTextStyle={{color: blue}}>
					      <ScrollView showsVerticalScrollIndicator={false}>
						      <View style={{ width: '100%', height: xs(700), backgroundColor: '#F2F2F2' }}>
						      	<View style={{ width: '100%', height: xs(100) }}>
						      		<TouchableOpacity onPress={() => this.props.navigation.navigate('BuatKompetisi')} style={[ b.ml4, b.mt4 ]}>
							      		<Svg icon="kompetisi" size={50} />
						      		</TouchableOpacity>
						      		<Text style={[ b.ml4, b.mt1, p.textCenter, { width: xs(50) }]}>Buat</Text>
						      	</View>
						      	<Text style={[ b.ml4, f.bold, b.mt2 ]}> KOMPETISI DIIKUTI </Text>
						      	<View style={[ b.mt2, p.row, { width: '100%', backgroundColor: light }]}>
						      		<View style={[ b.ml4, b.mt2 ]}>
						      			<Image source={require('../../asset/dashboard.png')} style={{ width: xs(50), height: xs(50) }} />
						      		</View>
						      		<View style={[ b.mt2, b.ml2, { width: xs(260) }]}>
						      			<Text style={[ f.bold ]}> Kompetisi Lari Semarang </Text>
						      			<Text> 2 hari lagi </Text>
						      			<View style={[ b.mt4, { borderBottomWidth: 1, borderBottomColor: '#DADADA' }]} />
						      			<View style={[ p.row ]}>
						      				<View style={{ width: xs(130) }}>
						      					<Text style={[ b.pt2, b.pb2 ]}> Ranking Saya: 3 </Text>
						      				</View>
						      				<View style={{ width: xs(130) }}>
						      					<Text style={[ b.pt2, b.pb2, p.textRight ]}> 54.254 Langkah </Text>
						      				</View>
						      			</View>
						      		</View>
						      	</View>
						      	<Text style={[ f.bold, b.mt2, b.ml4 ]}> UPCOMING </Text>
						      	<View style={[ b.mt2, { width: '100%', backgroundColor: light }]}>
						      		<ScrollView showsVerticalScrollIndicator={false} style={{ height: xs(330) }}>
							      		<View style={[ p.row, b.mt2, { width: '100%' }]}>
								      		<View style={[ b.ml4 ]}>
								      			<Image source={require('../../asset/dashboard.png')} style={{ width: xs(50), height: xs(50) }} />
								      		</View>
								      		<View style={[ b.ml2, { width: xs(260) }]}>
								      			<Text style={[ f.bold ]}> Global Running Competition </Text>
								      			<Text> Dimulai 6 hari lagi </Text>
								      			<TouchableOpacity style={[ p.center, b.mt1, { marginLeft: vs(4), width: '100%', borderWidth: 1, borderColor: blue }]}>
								      				<Text style={[ c.blue, b.pt1, b.pb1 ]}> Ikuti Kompetisi </Text>
								      			</TouchableOpacity>
								      		</View>
							      		</View>
							      		<View style={[ b.mt2, { width: '100%', borderBottomWidth: 1, borderBottomColor: '#DADADA' }]} />
						      		</ScrollView>
						      	</View>
						      	<View style={{ width: '100%', height: xs(10) }} />
						      	<View style={{ width: '100%', height: xs(50), backgroundColor: light }}>
							      	<TouchableOpacity style={[ p.row, { width: '100%' }]}>
							      		<View style={[ b.ml4, { width: xs(50), height: xs(50) }]}>
							      		</View>
							      		<View style={[ b.ml2, { width: xs(200) }]}>
							      			<Text style={{ marginTop: vs(14) }}> Riwayat Kompetisi </Text>
							      		</View>
							      		<View style={[ b.ml4, p.center, { width: xs(50) }]}>
							      			<Image source={require('../../asset/forward.png')} style={{ width: xs(25), height: xs(25) }} />
							      		</View>
							      	</TouchableOpacity>
						      	</View>
						      </View>
					      </ScrollView>
	                    </Tab>

	                    <Tab heading='League' tabStyle={{backgroundColor: 'white'}} textStyle={{color: '#000000'}} 
					      activeTabStyle={{backgroundColor: '#FFFFFF'}}
					      activeTextStyle={{color: blue}}>
							<ScrollView showsVerticalScrollIndicator={false}>
								<View style={{ width: '100%' }}>
									<ImageBackground source={require('../../asset/group_925.png')} style={{ width: '100%', height: xs(180) }}>
										<View style={[ p.row, { width: '100%', height: xs(70), bottom: 0, position: 'absolute' }]}>
											<Image source={require('../../asset/healthplusa_01.png')} style={[ b.ml3, { width: xs(60), height: xs(60) }]} />
											<View style={[ b.ml3, { width: xs(250), height: xs(40), bottom: 0, position: 'absolute', marginLeft: vs(90) }]}>
												<Text style={[ f.bold, f._16 ]}> Gabung League </Text>
												<Text style={[ f._10, { marginLeft: vs(3) }]}> Tunjukkan bahwa dirimu adalah para pemenang! </Text>
											</View>
										</View>
									</ImageBackground>
									<View style={[ p.center, { width: '100%', height: xs(70) }]}>
										<TouchableOpacity onPress={() => this.props.navigation.navigate('BuatGrup')} style={[ p.center, b.rounded, { width: xs(300), height: xs(40), backgroundColor: blue }]}>
											<Text style={[ c.light ]}> Buat League </Text>
										</TouchableOpacity>
									</View>
									<View style={{ width: '100%' }}>
										<View style={{ borderWidth: 2, borderColor: '#F0F0F0' }} />
									</View>
									<View style={{ width: '100%' }}>
										<Text style={[ f.bold, b.ml4, b.mt2 ]}>MY LEAGUE </Text>
										<View style={[ b.mt2, b.pl2, b.pr4 ]}>
											<ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
												<FlatList
								                  extraData={this.state}
								                  horizontal={true}
								                  data={this.state.group1}
								                  keyExtractor={item => item.id_group}
								                  renderItem={({item}) => (
								                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Grup', {id_group: item.id_group})} style={[ b.ml2 ]}>
								                      <View style={[ p.center, { width: xs(80) }]}>
								                        <Image source={{ uri: API_URL+'/src/icon_group/'+item.url_gambar }} style={[ b.roundedLow, { width: xs(80), height: xs(80) }]} />
								                        <Text style={[ p.textCenter, b.mt1, f._12 ]}> {item.nama_group} </Text>
								                      </View>
								                    </TouchableOpacity>
								                  )}
								                />
											</ScrollView>
										</View>

										<Text style={[ f.bold, b.ml4, b.mt4 ]}>LEAGUE TERDEKAT </Text>
										<View style={{ width: '100%', height: xs(300) }}>
											<FlatList
							                  extraData={this.state}
							                  data={this.state.group}
							                  keyExtractor={item => item.id_group}
							                  renderItem={({item}) => (
							                  	<>
								                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Grup', {id_group: item.id_group})} style={[ b.ml4, b.mt2 ]}>
								                      <View style={[ p.row, { width: '100%' }]}>
								                        <Image source={{ uri: API_URL+'/src/icon_group/'+item.url_gambar }} style={[ b.roundedLow, { width: xs(50), height: xs(50) }]} />
								                        <View>
									                        <Text style={[ b.ml4, b.mt1 ]}> {item.nama_group} </Text>
									                        <View style={[ p.row, b.ml4 ]}>
										                        <Text> {item.jml_anggota} Pelari </Text>
										                        <View style={[ b.bordered, b.rounded, { backgroundColor: '#DADADA', width: xs(7), height: xs(7), marginTop: vs(7) }]} />
										                        <Text> {item.lokasi} </Text>
									                        </View>
								                        </View>
								                      </View>
								                    </TouchableOpacity>
								                    <View style={[ b.mt2, { width: '100%', borderBottomWidth: 1, borderBottomColor: '#DADADA' }]} />
							                  	</>
							                  )}
							                />
											<TouchableOpacity onPress={() => this.props.navigation.navigate('ListLeagueTerdekat')}>
												<Text style={[ c.blue ]}> Lihat Selengkapnya </Text>
											</TouchableOpacity>
										</View>
									</View>
								</View>
							</ScrollView>
	                    </Tab>
					</Tabs>
				</Container>
			</>)
		}
	}
}

const styles = StyleSheet.create({
  // loading centered screenn
  container_loading: {
    flex: 1,
    justifyContent: "center"
  },
  horizontal_loading: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },
});