import React from 'react'
import { Text, View, TouchableOpacity, TextInput, StyleSheet, ScrollView, Image, Alert, ActivityIndicator } from 'react-native'
import { b, p, f, c } from '../../utils/StyleHelper'
import { vs, xs } from '../../utils/Responsive'
import { blue, light, grey, softBlue } from '../../utils/Color'
import Svg from '../../utils/Svg'
import ImagePicker from 'react-native-image-picker';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage'
import Icon from 'react-native-vector-icons/Ionicons'
import { API_URL } from 'react-native-dotenv'

export default class UploadImage extends React.Component {

	constructor(props){
		super(props);
		this.state ={
			judul: null,
			url_gambar: '',
            uri: '',
            fileName: null,
            file_image: null,
            isLoading: false
		}
	}

	launchCamera = () => {
	    let options = {
	      storageOptions: {
	        skipBackup: true,
	        path: 'images',
	      },
	    };
	    ImagePicker.launchCamera(options, (response) => {
	      console.log('Response = ', response);

	      if (response.didCancel) {
	        console.log('User cancelled image picker');
	      } else if (response.error) {
	        console.log('ImagePicker Error: ', response.error);
	      } else if (response.customButton) {
	        console.log('User tapped custom button: ', response.customButton);
	        alert(response.customButton);
	      } else {
	        console.log(response.fileName);
	        this.setState({
	          uri: {uri: response.uri},
	          url_gambar: response.uri,
	          fileName: response.fileName,
	          file_image: response
	        });
	      }
	    });

	}

  launchImageLibrary = () => {
    let options = {
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.launchImageLibrary(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        alert(response.customButton);
      } else {
        console.log(response);
        // alert(JSON.stringify(response));
        this.setState({
          uri: {uri: response.uri},
          url_gambar: response.uri,
          fileName: response.fileName,
          file_image: response
        });
      }
    });

	}

	renderFileUri() {
	    if (this.state.url_gambar) {
	      return <Image
	        source={{ uri: this.state.url_gambar }}
	        style={styles.images}
	      />
	    } else {
	      return <Image
	        style={styles.images}
	      />
	    }
	}

	createFormDataPhoto = (photo, id_login) => {
	    const data = new FormData();
	    
		if(this.state.fileName !== null) {
			data.append('file_image', {
				name : (photo.fileName == null)? 'myphoto': photo.fileName,
				type : photo.type,
				uri : photo.uri
			});
		}
	    data.append("deskripsi_post_challenge", this.state.judul);
		data.append("id_login", id_login);
		data.append("id_kompetisi", this.props.navigation.getParam('id_kompetisi'));
	    return data;
	  };

	async upload_post_challenge(){
		const id_login = await AsyncStorage.getItem('@id_login')

		if (this.state.file_image === '') {
			Alert.alert('Masukkan Judul dan Gambar untuk memposting!')
		} else {
			this.setState({
				isLoading: true
			})
		}

	    fetch(API_URL+'/main/insert_post_challenge', {
			method: 'POST',
			headers: {
				'Content-Type': 'multipart/form-data'
			},
			body: this.createFormDataPhoto(this.state.file_image, id_login)
	    })
		.then(response => response.json())
		.then(response => {
	        console.log(response);
			this.setState({
				isLoading: false
			})
	        Alert.alert(
	        	"",
	        	"Berhasil Terkirim",
	        	[
	        		{ text: 'OK', onPress: () => this.props.navigation.pop() }
	        	]
	        )
		})
		.catch(error => {
	        console.log('Upload Error', error);
	        // alert(JSON.stringify(error))
		});
	}

	render() {
		return (
			<>
				{this.state.isLoading ?
					<View style={[styles.container_loading, styles.horizontal_loading]}>
						<ActivityIndicator size="large" color="#0000ff"/>
					</View>
					:
					null
				}
				<ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#181B22' }}>
					<View style={{ height: xs(80), width: '100%', backgroundColor: '#000' }}>
						<View style={[ p.row, b.ml4, b.mt3 ]}>
							<View style={{ width: xs(100) }}>
								<TouchableOpacity onPress={() => this.props.navigation.pop()}>
									<Icon name="ios-close" size={50} color={light} />
								</TouchableOpacity>
							</View>
							<View style={[ p.center, b.ml2, { width: xs(100) }]}>
								<Text style={[ c.light, f.bold, f._20 ]}> Upload </Text>
							</View>
						</View>
					</View>
					<View style={[ p.center, { width: '100%' }]}>
						<View style={[ b.mt4, b.rounded, b.shadow, { width: xs(325), height: xs(250), backgroundColor: '#555555' }]}>
							<TextInput
								placeholder={"Post title..."}
								placeholderTextColor={light}
								multiline={true}
								style={[ b.ml2, c.light, { width: xs(300) }]}
								onChangeText={text => {
									let judul = this.state.judul
									judul = text
									this.setState({ judul });
								}}
								value={this.state.judul}
							/>
							<View style={[ p.center, { width: '100%' }]}>
								{this.renderFileUri()}
							</View>
						</View>

						<View style={[ b.mt4, b.rounded, b.shadow, { width: xs(325), height: xs(150), backgroundColor: '#555555' }]}>
							<TouchableOpacity onPress={this.launchCamera}>
								<View style={[ p.row, { marginLeft: vs(60) }]}>
									<Icon name="ios-camera" color={light} size={35} style={[ b.mt3 ]} />
									<Text style={[ c.light, f.bold, f._18, b.ml4, { marginTop: vs(20) }]}> Gunakan Kamera </Text>
								</View>
							</TouchableOpacity>
							<View style={[ p.center, b.mt4 ]}>
								<View style={{ borderBottomWidth: 2, borderBottomColor: '#C0C0C0', width: xs(300) }} />
							</View>
							<TouchableOpacity onPress={this.launchImageLibrary}>
								<View style={[ p.row, { marginLeft: vs(60) }]}>
									<Icon name="md-link" color={light} size={35} style={[ b.mt4 ]} />
									<Text style={[ c.light, f.bold, f._18, b.ml4, { marginTop: vs(25) }]}> Pilih dari Gallery </Text>
								</View>
							</TouchableOpacity>
						</View>
					</View>
					<View style={[ b.container, p.center, { marginTop: vs(40), paddingBottom: vs(50) }]}>
						<TouchableOpacity onPress={() => this.upload_post_challenge()} style={[ b.roundedHigh, p.center, { backgroundColor: blue, height: xs(40), width: xs(150) }]}>
							<Text style={[ c.light, f.bold, f._18 ]}> Post </Text>
						</TouchableOpacity>
					</View>
				</ScrollView>
			</>
		)
	}
}

const styles = StyleSheet.create({
  images: {
    width: xs(315),
    height: xs(195),
    marginHorizontal: 3,
  },
  container_loading: {
	flex: 1,
	position:'absolute',
	zIndex:1,
	width: '100%',
	height: '100%',
	justifyContent: "center",
	backgroundColor: 'rgba(0,0,0,0.7)'
  },
  horizontal_loading: {
	flexDirection: "row",
	justifyContent: "space-around",
	padding: 10
  },
});