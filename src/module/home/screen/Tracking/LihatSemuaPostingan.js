import React from 'react'
import { Text, View, TouchableOpacity, Image, ImageBackground, ScrollView, StyleSheet, FlatList, ActivityIndicator } from 'react-native'
import { b, p, f, c } from '../../utils/StyleHelper'
import { vs, xs } from '../../utils/Responsive'
import { blue, light, grey, softBlue } from '../../utils/Color'
import Svg from '../../utils/Svg'
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios';
import Postingan from './Postingan.js'
import {NavigationEvents} from 'react-navigation';
import { API_URL } from 'react-native-dotenv'

export default class LihatSemuaPostingan extends React.Component {

	constructor(props){
		super(props);
		this.state ={
			loading: true
		}
	}

	async componentDidMount() {
		const id_login = await AsyncStorage.getItem('@id_login')
		const id_group = await AsyncStorage.getItem('@current_id_group')

		// alert(id_group)
		axios.get(API_URL+'/main/get_all_group_post', {params:{
			id_group: id_group,
			id_login: id_login
		}})
		.then(data => {
			this.setState({
				loading: false,
				list_posting: data.data.data
			})
			// alert(JSON.stringify(data.data.data))
		})
	}

	hitung_hari(from_date) {
		var date1 = new Date(from_date); 
		var date2 = new Date(new Date().getFullYear(),new Date().getMonth() , new Date().getDate()); 
		// console.log(date1);
		// console.log(date2);
		  
		// To calculate the time difference of two dates 
		var Difference_In_Time = date2.getTime() - date1.getTime(); 
		  
		// To calculate the no. of days between two dates 
		var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
		if (typeof Difference_In_Days === 'number') {
			if(Number(Difference_In_Days.toString().replace('-', '')).toFixed() == 0){
				return 'Today';
			}else{
				return Number(Difference_In_Days.toString().replace('-', '')).toFixed()+' Days ago';
			}
		}
	}

	render() {
		return (
			<>
				{this.state.loading ?
					<View style={[styles.container_loading, styles.horizontal_loading]}>
						<ActivityIndicator size="large" color="#0000ff"/>
					</View>
					:
					null
				}
				<NavigationEvents onDidFocus={() => this.componentDidMount()} />
				<ActionButton buttonColor={blue} style={{ zIndex: 1 }} fixNativeFeedbackRadius={true}>
		          <ActionButton.Item buttonColor='#9b59b6' title="Add Comment" onPress={() => this.props.navigation.navigate('AddComment')}>
		            <Icon name="md-create" style={styles.actionButtonIcon} />
		          </ActionButton.Item>
		        </ActionButton>
				<View style={[ p.center, { width: '100%', height: xs(80), backgroundColor: '#000' }]}>
					<View style={[ p.row, { width: '100%' }]}>
						<TouchableOpacity onPress={() => this.props.navigation.pop()} style={[ b.ml4 ]}>
							<Image source={require('../../asset/back.png')} style={{ width: xs(25), height: xs(25) }} />
						</TouchableOpacity>
						<View style={[ b.ml4, p.center, { width: xs(235) }]}>
							<Text style={[ f.bold, f._18, c.light ]}>Postingan</Text>
						</View>
					</View>
				</View>
				<ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#181B22' }}>
					<View style={{ width: '100%' }}>
						<FlatList
							extraData={this.state}
							data={this.state.list_posting}
							keyExtractor={item => item.id_group}
							renderItem={({item}) => (
								<Postingan
					                url_avatar={item.url_gambar}
									nama={item.nama}
									tanggal_post={this.hitung_hari(item.tanggal_post)}
									deskripsi={item.deskripsi}
					                post_gambar={item.post_gambar}
					                id={item.id_post_group}
					                komentar={item.komentar}
									like_id_login={item.like_id_login}
					                action={() => this.props.navigation.push('ProfTracking', {id_login: item.id_login})}
					                comment={() => this.props.navigation.push('LihatKome', {id_post_group: item.id_post_group})}
								/>
							)}
						/>
					</View>
				</ScrollView>
			</>
		)
	}
}

const styles = StyleSheet.create({
  // loading centered screenn
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
  container_loading: {
	flex: 1,
    position:'absolute',
    zIndex:1,
    width: '100%',
    height: '100%',
    justifyContent: "center",
    backgroundColor: 'rgba(0,0,0,0.7)'
  },
  horizontal_loading: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },
});