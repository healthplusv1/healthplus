import React from 'react'
import { Text, View, TouchableOpacity, Image, ScrollView, Alert, TextInput, FlatList, KeyboardAvoidingView, Keyboard, ActivityIndicator, StyleSheet } from 'react-native'
import { b, p, f, c } from '../../utils/StyleHelper'
import { vs, xs } from '../../utils/Responsive'
import { blue, light, grey, softBlue } from '../../utils/Color'
import Svg from '../../utils/Svg'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios';
import {NavigationEvents} from 'react-navigation';
import FastImage from 'react-native-fast-image'
import { API_URL } from 'react-native-dotenv'

export default class LihatKome extends React.Component {

	constructor(props){
		super(props);
		this.state = {
			loading: false,
			nama: null,
			deskripsi: null,
			url_gambar: '',
			tanggal_post: '',
			komentar: '...',
			comment: '',
			suka: '...',
			loading: true,
			all_komentar: [
				{id_post_group: 1, url_gambar: require('../../asset/10.png'), nama: 'Michael', tgl_komentar: '2021-05-24', comment: 'Ayo...'},
				{id_post_group: 2, url_gambar: require('../../asset/10.png'), nama: 'Candra', tgl_komentar: '2021-05-24', comment: 'Ayo guys'},
				{id_post_group: 3, url_gambar: require('../../asset/10.png'), nama: 'Asri Utari', tgl_komentar: '2021-05-24', comment: 'Siapp...'},
			],
			like_id_login: '',
			gambar: '',
			post_gambar: '',
			id_login_profile: '',
		}
	}

	async componentDidMount() {
		const id_group = await AsyncStorage.getItem('@current_id_group')
		const id_login = await AsyncStorage.getItem('@id_login')
		// alert(id_login)
		this.setState({id_login: id_login})
		// alert(id_group)
		axios.get(API_URL+'/main/get_post_detail', {params:{
			id_post_group: this.props.navigation.getParam('id_post_group'),
			id_login: id_login
		}})
		.then(data => {
			this.setState({
				// list_posting: data.data.data,
				nama: data.data.data.nama,
				deskripsi: data.data.data.deskripsi,
				url_gambar: data.data.data.url_gambar,
				tanggal_post: data.data.data.tanggal_post,
				komentar: data.data.data.komentar,
				suka: data.data.data.suka,
				loading: false,
				like_id_login: data.data.data.like_id_login,
				gambar: data.data.latest_like,
				post_gambar: data.data.data.post_gambar,
				id_login_profile: data.data.data.id_login,
			})
			// alert(JSON.stringify(this.state.post_gambar))
		})
		
		axios.get(API_URL+'/main/get_funstation_latest_komentar', {params:{
			id_post_group: this.props.navigation.getParam('id_post_group')
		}})
		.then(resp => {
			this.setState({
				komentar: resp.data.data.komentar,
				loading: false
			})
			// alert(JSON.stringify(resp))
		})

		axios.get(API_URL+'/main/get_all_komentar', {params:{
			id_post_group: this.props.navigation.getParam('id_post_group')
		}})
		.then(result => {
			// alert(JSON.stringify(result))
			this.setState({
				all_komentar: result.data.data,
				loading: false
			})
		})
	}

	async post() {
		const id_login = await AsyncStorage.getItem('@id_login')

		if (this.state.comment === '') {
			Alert.alert('Harap isi kolom komentar')
		} else {
			this.setState({
				loading: true
			})
		}

		axios.get(API_URL+'/main/add_comment', {params:{
			id_post_group: this.props.navigation.getParam('id_post_group'),
			id_login: id_login,
			comment: this.state.comment
		}})
		.then(res => {
			if (res.data.status === 'true') {
				// this.props.navigation.navigate('LihatKome')
				this.setState({
					comment: ''
				})

				this.componentDidMount();
			} else {
				console.log("Gagal Post Comment")
			}
		})
		.catch(e => {
			console.log("upload_error"+e)
		})
	}

	hitung_hari(from_date) {
		var date1 = new Date(from_date); 
		var date2 = new Date(new Date().getFullYear(),new Date().getMonth() , new Date().getDate()); 
		// console.log(date1);
		// console.log(date2);
		  
		// To calculate the time difference of two dates 
		var Difference_In_Time = date2.getTime() - date1.getTime(); 
		  
		// To calculate the no. of days between two dates 
		var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
		if (typeof Difference_In_Days === 'number') {
			if(Number(Difference_In_Days.toString().replace('-', '')).toFixed() == 0){
				return 'Today';
			}else{
				return Number(Difference_In_Days.toString().replace('-', '')).toFixed()+' hari yang lalu';
			}
		}
	}

	async _increaseValue(){
		const id_login = await AsyncStorage.getItem('@id_login');

		axios.get(API_URL+'/main/insert_like_post', {params:{
			id_login: id_login,
			id_post_group: this.props.navigation.getParam('id_post_group')
		}})
		.then(resp => {
		this.componentDidMount()
		if (resp.data.status === 'true') {
			// Alert.alert("Berhasil")
			if (this.state.like_id_login === null) {
				this.setState({
					suka: '...',
					id_login: null
				})
			}else{
				this.setState({
					suka: '...',
					id_login: id_login
				})	
			}
			axios.get(API_URL+'/main/get_funstation_latest_suka', {params:{
				id_post_group: this.props.navigation.getParam('id_post_group')
			}})
			.then(result => {
				// alert(JSON.stringify(result))
				this.setState({
				suka: result.data.data.suka
				})
			})
			.catch(e => {
				if (e.message === 'Network Error') {
				Alert.alert('Tidak Ada Koneksi Internet','Mohon Periksa Koneksi Internet Anda Lalu Restart Aplikasi')
				// Alert.alert('Network Error','Please Check Your Internet Connection then Restart The App')
				}else{
				// Alert.alert('', JSON.stringify(e.message))
				}
			})
		} else {
			Alert.alert("Gagal")
		}
		})
		.catch(err => {
			// Alert.alert("Error")
		})
	}
	  
	async _decreaseValue() {
		const id_login = await AsyncStorage.getItem('@id_login')

		axios.get(API_URL+'/main/delete_like_post', {params:{
			id_login: id_login,
			id_post_group: this.props.navigation.getParam('id_post_group')
		}})
		.then(resp => {
			this.componentDidMount()
			if (resp.data.status === 'true') {
				// Alert.alert("Berhasil")
				if (this.state.like_id_login === null) {
					this.setState({
						suka: '...',
						id_login: id_login
					})	
				}else{
					this.setState({
						suka: '...',
						id_login: null
					})	
				}
				axios.get(API_URL+'/main/get_funstation_latest_suka', {params:{
					id_post_group: this.props.navigation.getParam('id_post_group')
				}})
				.then(result => {
				// alert(JSON.stringify(result))
				this.setState({
				  suka: result.data.data.suka
				})
			  })
			  .catch(e => {
				if (e.message === 'Network Error') {
				  Alert.alert('Tidak Ada Koneksi Internet','Mohon Periksa Koneksi Internet Anda Lalu Restart Aplikasi')
				  // Alert.alert('Network Error','Please Check Your Internet Connection then Restart The App')
				}else{
				  // Alert.alert('', JSON.stringify(e.message))
				}
			  })
			} else {
				Alert.alert("Gagal")
			}
		})
		.catch(err => {
			// Alert.alert("Error")
		})
	}

	render() {
		return (
			<>
				{/* {this.state.loading ?
					<View style={[styles.container_loading, styles.horizontal_loading]}>
						<ActivityIndicator size="large" color="#0000ff" />
					</View>
					:
					null
				} */}
				{/* <NavigationEvents onDidFocus={() => this.componentDidMount()} /> */}
				<View style={[ p.center, b.shadowHigh, { top: 0, width: '100%', height: xs(80), backgroundColor: '#000' }]}>
					<View style={[ p.row, { width: '100%' }]}>
						<TouchableOpacity onPress={() => this.props.navigation.pop()} style={[ b.ml4 ]}>
							<Image source={require('../../asset/back1.png')} style={{ width: xs(25), height: xs(25), tintColor: light }} />
						</TouchableOpacity>
						<View style={[ b.ml4, p.center, { width: xs(235) }]}>
							<Text style={[ f._18, c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>Postingan</Text>
						</View>
					</View>
				</View>
				<ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#181B22' }}>
					<View style={{ width: '100%' }}>
						<View style={[ p.row, b.mt3 ]}>
							<TouchableOpacity onPress={() => this.props.navigation.push('ProfTracking', {id_login: this.state.id_login_profile})}>
								<Image source={this.state.url_gambar === '' ? require('../../asset/10.png') : { uri: API_URL+'/src/avatar/'+this.state.url_gambar } } style={[ b.ml4, { width: xs(50), height: xs(50) }]} />
							</TouchableOpacity>
							<View style={[ b.ml3, b.mt2 ]}>
								<TouchableOpacity onPress={() => this.props.navigation.push('ProfTracking', {id_login: this.state.id_login_profile})}>
									<Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}> Pratama </Text>
								</TouchableOpacity>
								<Text style={[ f._12, { color: 'darkorange' }]}> Nike Run Club Semarang </Text>
							</View>
						</View>
						<Text style={[ b.ml4, b.mt2, f._12, c.light ]}>Olahraga dulu biar sehat guys, kalian jangan lupa olahraga. Tetap semangat walaupun, masih puasa, Semangat.</Text>
						<View style={[ b.ml4, b.mt2 ]}>
							{this.state.post_gambar === null ?
								null
								:
								<FastImage source={require('../../asset/gambar_league.png')} style={{ width: xs(320), height: xs(320), borderRadius: wp('3%') }} />
							}
						</View>

						<View style={{ width: '100%', alignItems: 'center' }}>
							<View style={{ width: wp('90%'), marginTop: wp('5%'), flexDirection: 'row', alignItems: 'center' }}>
								<View style={{ width: wp('50%'), flexDirection: 'row', alignItems: 'center' }}>
									<View style={{ width: wp('25%'), flexDirection: 'row', alignItems: 'center' }}>
										{this.state.like_id_login === this.state.id_login ?
											<TouchableOpacity activeOpacity={.5} onPress={() => {this._decreaseValue()}} style={[ b.ml2 ]}>
												<View style={[ p.row ]}>
													<Image source={require('../../asset/loved.png')} style={{ width: xs(20), height: xs(20) }} />
												</View>
											</TouchableOpacity>
											: 
											<TouchableOpacity activeOpacity={.5} onPress={() => {this._increaseValue()}} style={[ b.ml2 ]}>
												<View style={[ p.row ]}>
													<Image source={require('../../asset/love.png')} style={{ width: xs(20), height: xs(20), tintColor: light }} />
												</View>
											</TouchableOpacity>
										}
										<Text style={[ c.light ]}> {this.state.suka} </Text>
									</View>
									<View style={{ width: wp('25%'), flexDirection: 'row', alignItems: 'center' }}>
										<Image source={require('../../asset/comment.png')} style={{ width: xs(20), height: xs(20), tintColor: light }} />
										<Text style={[ b.ml1, c.light ]}> {this.state.komentar} </Text>
									</View>
								</View>
								<View style={{ width: wp('40%'), alignItems: 'flex-end' }}>
									<Text style={[ c.light, f._12 ]}>{this.hitung_hari('2021-05-20')}</Text>
								</View>
							</View>
						</View>
					</View>
					<ScrollView showsVerticalScrollIndicator={false}>
						<View style={{ width: '100%' }}>
							<FlatList
								ListHeaderComponent={<View style={{ borderTopWidth: 1, borderTopColor: light, marginTop: wp('5%') }} />}
								data={this.state.all_komentar}
								keyExtractor={item => item.id_post_group}
								renderItem={({item}) => (
									<>
										<View style={[ p.row, b.mt2, b.mb2 ]}>
											<View style={{ width: xs(80) }}>
												<TouchableOpacity onPress={() => this.props.navigation.push('ProfTracking', {id_login: item.id_login})}>
													<Image source={item.url_gambar} style={[ b.ml3, b.mt2, { width: xs(50), height: xs(50) }]} />
												</TouchableOpacity>
											</View>
											<View style={[ b.ml2, { width: xs(250) }]}>
												<View style={[ p.row, b.mt2 ]}>
													<TouchableOpacity onPress={() => this.props.navigation.push('ProfTracking', {id_login: item.id_login})}>
														<Text style={[ f._12, c.light ]}>{item.nama} </Text>
													</TouchableOpacity>
													<View style={[ b.mt1, { width: xs(8), height: xs(8), borderRadius: xs(8), backgroundColor: '#DADADA' }]} />
													<Text style={[ f._12, c.light ]}> {this.hitung_hari(item.tgl_komentar)} </Text>
												</View>
												<Text style={[ b.mt2, c.light ]}>{item.comment}</Text>
											</View>
										</View>

										<View style={[ b.mt1, { borderBottomWidth: 1, borderBottomColor: light }]} />
									</>
								)}
							/>
						</View>
						<View style={{ height: xs(43) }} />
					</ScrollView>
				</ScrollView>
				<View style={{ width: '100%', backgroundColor: light, flexDirection: 'row', alignItems: 'center', position: 'relative', bottom: 0 }}>
					<FastImage source={require('../../asset/10.png')} style={[ b.ml3, { width: xs(40), height: xs(40) }]} />
					<TextInput style={[ b.ml2, c.light, { width: xs(220) }]}
						placeholder={"Tulis Komentar"}
						multiline
						value={this.state.comment}
						onChangeText={text => {
							let comment = this.state.comment
							comment = text
							this.setState({ comment })
						}}
					/>
					<View style={[ p.center, { width: xs(100), marginLeft: xs(-10) }]}>
						<TouchableOpacity onPress={() => this.post()}>
							<Text style={[ c.blue, f.bold ]}>Posting</Text>
						</TouchableOpacity>
					</View>
				</View>
			</>
		)
	}
}

const styles = StyleSheet.create({
	// loading centered screenn
	container_loading: {
	  flex: 1,
	  position:'absolute',
	  zIndex:1,
	  width: '100%',
	  height: '100%',
	  justifyContent: "center",
	  backgroundColor: 'rgba(0,0,0,0.7)'
	},
	horizontal_loading: {
	  flexDirection: "row",
	  justifyContent: "space-around",
	  padding: 10
	}
  });