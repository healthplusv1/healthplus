import React from 'react'
import { View, Text, TouchableOpacity, Image, ActivityIndicator, StyleSheet, Alert } from 'react-native'
import { b, p, f, c } from '../../utils/StyleHelper'
import { vs, xs } from '../../utils/Responsive'
import { blue, light } from '../../utils/Color'
import FastImage from 'react-native-fast-image'
import Icon from 'react-native-vector-icons/Ionicons'
import { NavigationEvents } from 'react-navigation'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import analytics from '@react-native-firebase/analytics'
import { API_URL } from 'react-native-dotenv'

export default class UserPost extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
        }
    }

    async componentDidMount(){
		await analytics().logEvent('posted_social', {
			item: 'post_id'
		})
		await analytics().setUserProperty('user_already_posted_social', 'null')

        const id_login = await AsyncStorage.getItem('@id_login')

        this.setState({
            id_login: id_login,
            loading: true
        })
		// alert(this.props.liked_id_login)
        axios.get(API_URL+'/main/get_user_post_latest_suka', {params:{
  			id_user_post: this.props.id
  		}})
  		.then(result => {
	    //   alert(JSON.stringify(result.data.data))
	      this.setState({
			suka: result.data.data.suka_user_post,
			loading: false
	      })
		})
    }

    async _increaseValue(){
		const id_login = await AsyncStorage.getItem('@id_login');

		axios.get(API_URL+'/main/insert_like_user_post', {params:{
			id_login: id_login,
			id_user_post: this.props.id
		}})
		.then(resp => {
		if (resp.data.status === 'true') {
			// Alert.alert("Berhasil")
			if (this.props.liked_id_login === null) {
				this.setState({
					suka: '...',
					id_login: null
				})	
			}else{
				this.setState({
					suka: '...',
					id_login: id_login
				})	
			}
			axios.get(API_URL+'/main/get_user_post_latest_suka', {params:{
				id_user_post: this.props.id
			}})
			.then(result => {
				// alert(JSON.stringify(result))
				this.setState({
				suka: result.data.data.suka_user_post
				})
			})
			.catch(e => {
				if (e.message === 'Network Error') {
				Alert.alert('Tidak Ada Koneksi Internet','Mohon Periksa Koneksi Internet Anda Lalu Restart Aplikasi')
				// Alert.alert('Network Error','Please Check Your Internet Connection then Restart The App')
				}else{
				// Alert.alert('', JSON.stringify(e.message))
				}
			})
		} else {
			Alert.alert("Gagal")
		}
		})
		.catch(err => {
			// Alert.alert("Error")
		})
    }
    
    async _decreaseValue() {
		const id_login = await AsyncStorage.getItem('@id_login')

		axios.get(API_URL+'/main/delete_like_user_post', {params:{
			id_login: id_login,
			id_user_post: this.props.id
		}})
		.then(resp => {
			if (resp.data.status === 'true') {
				// Alert.alert("Berhasil")
				if (this.props.liked_id_login === null) {
					this.setState({
						suka: '...',
						id_login: id_login
					})	
				}else{
					this.setState({
						suka: '...',
						id_login: null
					})	
				}
				axios.get(API_URL+'/main/get_user_post_latest_suka', {params:{
					id_user_post: this.props.id
				}})
				.then(result => {
				// alert(JSON.stringify(result))
				this.setState({
				  suka: result.data.data.suka_user_post
				})
			  })
			  .catch(e => {
				if (e.message === 'Network Error') {
				  Alert.alert('Tidak Ada Koneksi Internet','Mohon Periksa Koneksi Internet Anda Lalu Restart Aplikasi')
				  // Alert.alert('Network Error','Please Check Your Internet Connection then Restart The App')
				}else{
				  // Alert.alert('', JSON.stringify(e.message))
				}
			  })
			} else {
				Alert.alert("Gagal")
			}
		})
		.catch(err => {
			// Alert.alert("Error")
		})
	}

    render() {
        return (
            <View style={[ b.mt2, { backgroundColor: '#181B22' }]}>
                <NavigationEvents onDidFocus={() => this.componentDidMount()} />
                <View style={[ p.row, b.mt3 ]}>
                    <View style={{ width: xs(250) }}>
                        <View style={[ p.row ]}>
                            <View>
                                <FastImage source={this.props.url_avatar === null ? { uri: API_URL+'/src/avatar/default.png' } : { uri: API_URL+'/src/avatar/'+this.props.url_avatar }} style={[ b.ml4, { width: xs(50), height: xs(50) }]} />
                            </View>
                            <View style={[ b.ml3, b.mt2 ]}>
                                <View>
                                    <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>{this.props.nama}</Text>
                                </View>
                                <Text style={[ f._12, { color: '#CCCCCC' }]}>{this.props.tanggal_post}</Text>
                            </View>
                        </View>
                        <Text style={[ b.ml4, b.mt2, f._12, c.light ]}>{this.props.deskripsi}</Text>
                    </View>
                </View>
                <View style={[ b.mt2, p.center, { width: '100%' }]}>
                    {this.props.post_gambar === null ?
                        null
                        :
                        <FastImage source={{ uri: API_URL+'/src/post_group/'+this.props.post_gambar }} style={{ width: xs(300), height: xs(300) }} />
                    }
                </View>
                <View style={[ b.mt2, { width: '100%', borderWidth: 1, borderColor: '#2F3240' }]} />
                <View style={[ p.row, b.mt3, b.mb3, p.center ]}>
                    <View style={[ p.row, p.center, { width: xs(150) }]}>
						{this.props.liked_id_login === this.state.id_login ?
							<TouchableOpacity activeOpacity={.5} onPress={() => {this._decreaseValue()}}>
								<View style={[ p.row ]}>
									<Image source={require('../../asset/loved.png')} style={{ width: xs(20), height: xs(20) }} />
								</View>
							</TouchableOpacity>
							: 
							<TouchableOpacity activeOpacity={.5} onPress={() => {this._increaseValue()}}>
								<View style={[ p.row ]}>
								<Image source={require('../../asset/love.png')} style={{ width: xs(20), height: xs(20), tintColor: light }} />
								</View>
							</TouchableOpacity>
						}
                        <Text style={[ c.light ]}> {this.state.suka} </Text>
                    </View>
                    <View style={[ p.row, b.ml3, p.center, { width: xs(150) }]}>
                        <TouchableOpacity onPress={eval(this.props.comment)}>
                            <Image source={require('../../asset/comment.png')} style={{ width: xs(20), height: xs(20), tintColor: light }} />
                        </TouchableOpacity>
                        <Text style={[ c.light ]}> {this.props.komentar} </Text>
                    </View>
                </View>
            </View>
        )
    }
}