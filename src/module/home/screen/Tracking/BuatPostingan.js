import React from 'react'
import { Text, View, TouchableOpacity, Image, ScrollView, Alert, TextInput, ActivityIndicator, StyleSheet } from 'react-native'
import { b, p, f, c } from '../../utils/StyleHelper'
import { vs, xs } from '../../utils/Responsive'
import { blue, light } from '../../utils/Color'
import ImagePicker from 'react-native-image-picker';
import AsyncStorage from '@react-native-community/async-storage'
import Icon from 'react-native-vector-icons/Ionicons'
import analytics from '@react-native-firebase/analytics'
import { API_URL } from 'react-native-dotenv'

const options = {
  title: 'Select Avatar',
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

export default class BuatPostingan extends React.Component {

	constructor(props){
		super(props);
		this.state ={
			deskripsi: '',
			gambar_user_post: '',
            uri: '',
            fileName: null,
            file_image: null,
            isLoading: false
		}
	}

	async componentDidMount() {
		await analytics().logEvent('post_social', {
			item: 'user_id'
		})
		await analytics().setUserProperty('user_post_social', 'null')
	}

	launchCamera = () => {
	    let options = {
	      storageOptions: {
	        skipBackup: true,
	        path: 'images',
	      },
	    };
		ImagePicker.launchCamera(options, (response) => {
		  console.log('Response = ', response);

		  if (response.didCancel) {
		    console.log('User cancelled image picker');
		  } else if (response.error) {
		    console.log('ImagePicker Error: ', response.error);
		  } else if (response.customButton) {
		    console.log('User tapped custom button: ', response.customButton);
		  } else {
		    console.log(response.fileName);
	        this.setState({
	          uri: {uri: response.uri},
	          gambar_user_post: response.uri,
	          fileName: response.fileName,
	          file_image: response
	        });
		  }
		});
    }
    
    launchGallery = () => {
	    let options = {
	      storageOptions: {
	        skipBackup: true,
	        path: 'images',
	      },
	    };
		ImagePicker.launchImageLibrary(options, (response) => {
		  console.log('Response = ', response);

		  if (response.didCancel) {
		    console.log('User cancelled image picker');
		  } else if (response.error) {
		    console.log('ImagePicker Error: ', response.error);
		  } else if (response.customButton) {
		    console.log('User tapped custom button: ', response.customButton);
		  } else {
		    console.log(response.fileName);
	        this.setState({
	          uri: {uri: response.uri},
	          gambar_user_post: response.uri,
	          fileName: response.fileName,
	          file_image: response
	        });
		  }
		});
	}

	createFormDataPhoto = (photo, id_login) => {
		const data = new FormData();

		if(this.state.fileName !== null) {
			data.append('file_image', {
				name : (photo.fileName == null)? 'myphoto': photo.fileName,
				type : photo.type,
				uri : photo.uri
			});
		}
		data.append("deskripsi_user_post", this.state.deskripsi);
		data.append("id_login", id_login);
		return data;
	}

	async insert_user_post() {
		const id_login = await AsyncStorage.getItem('@id_login')

		if (this.state.deskripsi === '') {
			Alert.alert('Harap isi semua data!')
		} else {
			this.setState({
				isLoading: true
			})
			fetch(API_URL+'/main/insert_user_post', {
				method: 'POST',
				headers: {
					'Content-Type': 'multipart/form-data'
				},
				body: this.createFormDataPhoto(this.state.file_image, id_login)
			})
			.then(response => response.json())
			.then(response => {
				// alert(JSON.stringify(response))
				this.setState({
					isLoading: false
				})
				Alert.alert(
					"",
					"Anda berhasil post",
					[
						{ text: 'OK', onPress: () => this.props.navigation.pop() }
					]
				)
			})
			.catch(error => {
				console.log('Upload Error', error);
			})
		}

	}

	renderFileUri() {
	    if (this.state.gambar_user_post) {
	      return <Image
	        source={{ uri: this.state.gambar_user_post }}
	        style={[ p.center, b.ml4, { width: xs(200), height: xs(125) }]}
	      />
	    } else {
	      return <View style={[ p.center, b.ml4, { width: xs(200), height: xs(125) }]} />
	    }
	}

	render() {
		return (
			<>
				{this.state.isLoading ?
					<View style={[styles.container_loading, styles.horizontal_loading]}>
						<ActivityIndicator size="large" color="#0000ff"/>
					</View>
					:
					null
				}
				<ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#181B22' }}>
					<View style={[ p.center, { width: '100%', height: xs(80), backgroundColor: '#000' }]}>
						<View style={[ p.row, { width: '100%' }]}>
							<TouchableOpacity onPress={() => this.props.navigation.pop()} style={[ b.ml4 ]}>
								<Image source={require('../../asset/back1.png')} style={{ width: xs(25), height: xs(25), tintColor: light }} />
							</TouchableOpacity>
                            <View style={[ b.ml4, p.center, { width: xs(240) }]}>
                                <Text style={[ f.bold, f._18, c.light ]}> Buat Postingan </Text>
                            </View>
						</View>
					</View>
					<View style={[ p.center, { width: '100%' }]}>
						<View style={[ b.mt4, { width: '100%' }]}>
							<View style={[ b.mt2, { width: '100%', height: xs(200), backgroundColor: '#555555' }]}>
								<TextInput style={[ b.ml4, c.light, { width: xs(335) }]}
                                    multiline={true}
                                    placeholder={"Deskripsi"}
                                    placeholderTextColor={light}
                                    value={this.state.deskripsi}
                                    onChangeText={text => {
                                        let deskripsi = this.state.deskripsi
                                        deskripsi = text
                                        this.setState({ deskripsi });
                                    }}
								/>
                                {this.renderFileUri()}
							</View>
						</View>

                        <View style={[ b.mt4, b.rounded, b.shadow, { width: xs(325), height: xs(150), borderWidth: 2, borderColor: '#555555', backgroundColor: '#555555' }]}>
                            <TouchableOpacity onPress={this.launchCamera}>
                                <View style={[ p.row, { marginLeft: vs(60) }]}>
                                    <Icon name="ios-camera" size={40} color={light} style={[ b.mt3 ]} />
                                    <Text style={[ c.light, f.bold, f._18, b.ml4, { marginTop: vs(25) }]}> Gunakan Kamera </Text>
                                </View>
                            </TouchableOpacity>
                            <View style={[ p.center, b.mt4 ]}>
                                <View style={{ borderBottomWidth: 2, borderBottomColor: light, width: xs(300) }} />
                            </View>
                            <TouchableOpacity onPress={this.launchGallery}>
                                <View style={[ p.row, { marginLeft: vs(60) }]}>
                                <Icon name="ios-image" size={40} color={light} style={[ b.mt3 ]} />
                                    <Text style={[ c.light, f.bold, f._18, b.ml4, { marginTop: vs(25) }]}> Pilih dari Gallery </Text>
                                </View>
                            </TouchableOpacity>
                        </View>
					</View>
                    <View style={[ p.center, b.mt4, { width: '100%' }]}>
                        <TouchableOpacity onPress={() => this.insert_user_post()} style={[ b.roundedHigh, { backgroundColor: blue, width: xs(100) }]}>
                            <Text style={[ c.light, p.textCenter, b.mt1, b.mb1, f.bold, f._18 ]}>Post</Text>
                        </TouchableOpacity>
                    </View>
				</ScrollView>
			</>
		)
	}
}

const styles = StyleSheet.create({
	// loading centered screenn
	container_loading: {
	  flex: 1,
	  position:'absolute',
	  zIndex:1,
	  width: '100%',
	  height: '100%',
	  justifyContent: "center",
	  backgroundColor: 'rgba(0,0,0,0.7)'
	},
	horizontal_loading: {
	  flexDirection: "row",
	  justifyContent: "space-around",
	  padding: 10
	},
  });