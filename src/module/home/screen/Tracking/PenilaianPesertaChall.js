import React from 'react'
import { View, Text, TextInput, TouchableOpacity, StyleSheet, FlatList, StatusBar } from 'react-native'
import { b, p, f, c } from '../../utils/StyleHelper'
import { vs, xs } from '../../utils/Responsive'
import { blue, light, grey, softBlue } from '../../utils/Color'
import Video from 'react-native-video'
import MediaControls, { PLAYER_STATES } from 'react-native-media-controls'
import Orientation from 'react-native-orientation-locker'
import KeepAwake from 'react-native-keep-awake';
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'
import Icon from 'react-native-vector-icons/Ionicons'
import FastImage from 'react-native-fast-image'
import Dialog, { DialogFooter, DialogButton, DialogContent, DialogTitle } from 'react-native-popup-dialog';
import { API_URL } from 'react-native-dotenv'

export default class PenilaianPesertaChall extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentTime: 0,
            duration: 0,
            isFullScreen: false,
            isLoading: true,
            paused: false,
            isBuffer: false,
            playerState: PLAYER_STATES.PLAYING,
            screenType:'content',
            jumlah: '',
            list_peserta: '',
            id_playing: '',
            jenis_challenge: '',
            gambar: '',
            nama: '',
            kota: '',
            provinsi: '',
            visible: false,
            timerTime: 0,
        }
    }

    async componentDidMount(){
        const id_login = await AsyncStorage.getItem('@id_login')
        this.onBuffer()
        KeepAwake.activate();

        axios.get(API_URL+'/main/check_admin_challenge', {params:{
            id_kompetisi: this.props.navigation.getParam('id_kompetisi'),
            id_login: id_login
        }})
        .then(resp => {
            // alert(JSON.stringify(resp.data.status))
            this.setState({
                isLoading: false,
                status: resp.data.status
            })
        })

        axios.get(API_URL+'/main/penilaian', {params: {
            id_progress_kompetisi: this.props.navigation.getParam('id_progress_kompetisi'),
            tanggal: this.props.navigation.getParam('tanggal')
        }})
        .then(res => {
            this.setState({
                jumlah: res.data.sedang_dinilai.jumlah,
                timerTime: res.data.sedang_dinilai.durasi,
                id_playing: res.data.sedang_dinilai.id_progress_kompetisi,
                list_peserta: res.data.list_peserta,
                jenis_challenge: res.data.sedang_dinilai.jenis_challenge,
                videos_push_up: res.data.sedang_dinilai.videos_push_up,
                gambar: res.data.sedang_dinilai.url_gambar,
                nama: res.data.sedang_dinilai.nama,
                kota: res.data.sedang_dinilai.kota,
                provinsi: res.data.sedang_dinilai.provinsi,
                id_progress_kompetisi: res.data.sedang_dinilai.id_progress_kompetisi,
            })
            // alert(JSON.stringify(res.data.sedang_dinilai.durasi))
        })
    }

    refresh(id_progress_kompetisi, menilai){
        axios.get(API_URL+'/main/penilaian', {params: {
            id_progress_kompetisi: id_progress_kompetisi,
            tanggal: this.props.navigation.getParam('tanggal')
        }})
        .then(res => {
            if (menilai){
                this.setState({
                    jumlah: res.data.sedang_dinilai.jumlah,
                    list_peserta: res.data.list_peserta,
                    timerTime: res.data.sedang_dinilai.durasi
                })
            } else {
                this.setState({
                    jumlah: res.data.sedang_dinilai.jumlah,
                    timerTime: res.data.sedang_dinilai.durasi,
                    id_playing: res.data.sedang_dinilai.id_progress_kompetisi,
                    list_peserta: res.data.list_peserta,
                    jenis_challenge: res.data.sedang_dinilai.jenis_challenge,
                    videos_push_up: res.data.sedang_dinilai.videos_push_up,
                    gambar: res.data.sedang_dinilai.url_gambar,
                    nama: res.data.sedang_dinilai.nama,
                    kota: res.data.sedang_dinilai.kota,
                    provinsi: res.data.sedang_dinilai.provinsi,
                    id_progress_kompetisi: res.data.sedang_dinilai.id_progress_kompetisi,
                })
                this.videoPlayer.seek(0);
                this.setState({ playerState: PLAYER_STATES.PLAYING });
            }
            // alert(JSON.stringify(res.data.sedang_dinilai.videos_push_up))
        })
    }

    onSeek = seek => {
        this.videoPlayer.seek(seek);
    };
    onPaused = playerState => {
        this.setState({
        paused: !this.state.paused,
        playerState,
        });
    };
    onReplay = () => {
        this.videoPlayer.seek(0);
        this.setState({ playerState: PLAYER_STATES.PLAYING });
    };
    onBuffer = buff => {
        console.log(this.state.videos_push_up)
        console.log(buff)
        if (buff === undefined) {
        }else if (buff.isBuffering === true){
            this.setState({ isLoading: true })
        }else if (buff.isBuffering === false){
            this.setState({ isLoading: false })
        }
    };
    onProgress = data => {
        const { isLoading, playerState } = this.state;
        // Video Player will continue progress even if the video already ended
        if (!isLoading && playerState !== PLAYER_STATES.ENDED) {
            this.setState({ currentTime: data.currentTime });
        }
    };
    onLoad = data => this.setState({ duration: data.duration, isLoading: false });
    onLoadStart = data => this.setState({ isLoading: true });
    onEnd = () => this.setState({ playerState: PLAYER_STATES.ENDED });
    onError = () => alert('Oh! ', error);
    exitFullScreen = () => {
        alert("Exit full screen");
    };
    enterFullScreen = () => {};
    onFullScreen = () => {
        if(this.state.isFullScreen == false){
          Orientation.lockToLandscape();
          StatusBar.setHidden(true)
          this.setState({isFullScreen: true})
        }
        else if (this.state.isFullScreen == true){
          Orientation.lockToPortrait();
          StatusBar.setHidden(false)
          this.setState({isFullScreen: false})
        }
    };
    renderToolbar = () => (
        <View style={[ p.row ]}>
            <View style={[ b.ml2, p.center ]}>
                <FastImage source={this.state.gambar === null ? { uri: API_URL+'/src/avatar/default.png' } : { uri: API_URL+'/src/avatar/'+this.state.gambar }} style={{ width: xs(50), height: xs(50) }} />
            </View>
            <View style={[ b.ml2, { width: xs(150) }]}>
                <Text style={[ f.bold, c.light, f._12, { marginTop: vs(8) }]}>{this.state.nama}</Text>
                
                <Text style={[ c.light, f._10 ]}>{this.state.kota}, {this.state.provinsi}</Text>
            </View>
        </View>
    );
    onSeeking = currentTime => this.setState({ currentTime });

    update_nilai() {
        axios.get(API_URL+'/main/update_nilai_peserta_kompetisi', {params: {
            id_progress_kompetisi: this.state.id_progress_kompetisi,
            jumlah: this.state.jumlah_nilai
        }})
        .then(resp => {
            this.refresh(this.state.id_progress_kompetisi, true)
            // alert(JSON.stringify(resp))
            this.componentDidMount()
        })
    }

    updateInputVal = (val, prop) => {
	    const state = this.state;
	    state[prop] = val;
	    this.setState(state);
	}

    render() {
        const { timerTime } = this.state;
		let centiseconds = ("0" + (Math.floor(timerTime / 10) % 100)).slice(-2);
		let seconds = ("0" + (Math.floor(timerTime / 1000) % 60)).slice(-2);
		let minutes = ("0" + (Math.floor(timerTime / 60000) % 60)).slice(-2);
		let hours = ("0" + Math.floor(timerTime / 3600000)).slice(-2);
        if (this.state.isFullScreen){
            return (
                <>
                    <View style={{ width: '100%', height: '100%' }}>
                        <Video
                            bufferConfig={{
                                minBufferMs: 15000,
                                maxBufferMs: 50000,
                                bufferForPlaybackMs: 2500,
                                bufferForPlaybackAfterRebufferMs: 5000
                            }}
                            onEnd={this.onEnd}
                            onLoad={this.onLoad}
                            onLoadStart={this.onLoadStart}
                            onProgress={this.onProgress}
                            paused={this.state.paused}
                            onBuffer={this.onBuffer}
                            ref={videoPlayer => (this.videoPlayer = videoPlayer)}
                            source={{ uri: API_URL+'/src/icon_group/'+this.state.videos_push_up}}
                            style={styles.mediaPlayer}
                            volume={10}
                            playInBackground={false}
                        />
                        <MediaControls
                            duration={this.state.duration}
                            isLoading={this.state.isLoading}
                            isBuffering={this.state.isBuffering}
                            mainColor="#333"
                            onFullScreen={this.onFullScreen}
                            onPaused={this.onPaused}
                            onReplay={this.onReplay}
                            onSeek={this.onSeek}
                            onSeeking={this.onSeeking}
                            playerState={this.state.playerState}
                            progress={this.state.currentTime}
                            toolbar={this.renderToolbar()}
                        />
                    </View>
                </>
            )
        } else {
            return (
                <View style={{ width: '100%', height: '100%', backgroundColor: '#181B22' }}>
                    <Dialog
                        dialogTitle={<DialogTitle textStyle={[ c.light ]} style={{ backgroundColor: '#181B22' }} title="Jumlah Push Up" />}
                        visible={this.state.visible}
                        onTouchOutside={() => this.setState({visible:false, jumlah_nilai: null})}
                        dialogStyle={{ backgroundColor: '#181B22' }}
                        footer={
                            <DialogFooter>
                                <DialogButton
                                    textStyle={[ c.light ]}
                                    text="BATAL"
                                    onPress={() => { 
                                        this.setState({
                                            visible:false,
                                            jumlah_nilai: null
                                        }) 
                                    }}
                                />
                                <DialogButton
                                    textStyle={[ c.light ]}
                                    text="SIMPAN"
                                    onPress={() => {
                                        this.setState({
                                            visible:false
                                        })
                                        
                                        this.update_nilai()
                                    }}
                                />
                            </DialogFooter>
                        }
                    >
                        <DialogContent>
                            <View style={{ width: xs(250), paddingTop: vs(20) }}>
                                <TextInput
                                    style={[ c.light, f._20, { backgroundColor: '#555555', borderRadius: xs(10), textAlign: 'center' }]}
                                    keyboardType={'numeric'}
                                    // underlineColorAndroid = '#b8b8b8'
                                    onChangeText={(val) => this.updateInputVal(val, 'jumlah_nilai')}
                                />
                            </View>
                        </DialogContent>
                    </Dialog>
                    <View style={[ p.center, { width: '100%', height: xs(80), backgroundColor: '#000' }]}>
                        <View style={[ p.row, { width: '100%' }]}>
                            <View style={[ b.ml4, { width: xs(50) }]}>
                                <TouchableOpacity onPress={() => this.props.navigation.pop()}>
                                    <Icon name="ios-arrow-back" size={25} color={light} />
                                </TouchableOpacity>
                            </View>
                            <View style={[ b.ml4, p.center, { width: xs(180) }]}>
                                <Text style={[ c.light, f._20, f.bold ]}>Penilaian</Text>
                            </View>
                        </View>
                    </View>
                    <View style={{ width: '100%', height: xs(250) }}>
                        <Video
                            bufferConfig={{
                                minBufferMs: 15000,
                                maxBufferMs: 50000,
                                bufferForPlaybackMs: 2500,
                                bufferForPlaybackAfterRebufferMs: 5000
                            }}
                            onEnd={this.onEnd}
                            onLoad={this.onLoad}
                            onLoadStart={this.onLoadStart}
                            onProgress={this.onProgress}
                            paused={this.state.paused}
                            onBuffer={this.onBuffer}
                            ref={videoPlayer => (this.videoPlayer = videoPlayer)}
                            source={{ uri: API_URL+'/src/icon_group/'+this.state.videos_push_up}}
                            style={styles.mediaPlayer}
                            volume={10}
                            playInBackground={false}
                        />
                        <MediaControls
                            duration={this.state.duration}
                            isLoading={this.state.isLoading}
                            isBuffering={this.state.isBuffering}
                            mainColor="#333"
                            onFullScreen={this.onFullScreen}
                            onPaused={this.onPaused}
                            onReplay={this.onReplay}
                            onSeek={this.onSeek}
                            onSeeking={this.onSeeking}
                            playerState={this.state.playerState}
                            progress={this.state.currentTime}
                            toolbar={this.renderToolbar()}
                        />
                    </View>
                    <View style={[ p.row, b.mt4, p.center, { width: '100%' }]}>
                        <View style={[ p.center, { width: xs(150) }]}>
                        { this.state.jenis_challenge === 'Plank' ?
                            <Text style={[ b.mt1, b.mb1, c.light, f.bold ]}>Durasi {this.state.jenis_challenge}</Text>
                            :
                            <Text style={[ b.mt1, b.mb1, c.light, f.bold ]}>Jumlah {this.state.jenis_challenge}</Text>
                        }
                        </View>
                        <View style={[ b.ml2, b.bordered, p.center, b.roundedLow ]}>
                            {this.state.jenis_challenge === 'Plank' ?
                                <Text style={[ b.mt1, b.mb1, b.ml2, b.mr2, c.light, f.bold ]}>{hours} : {minutes} : {seconds} : {centiseconds}</Text>
                                :
                                (this.state.jumlah === null ?
                                    <Text style={[ b.mt1, b.mb1, b.ml2, b.mr2, c.light, f.bold ]}>- -</Text>
                                    :
                                    <Text style={[ b.mt1, b.mb1, b.ml2, b.mr2, c.light, f.bold ]}>{this.state.jumlah}</Text>
                                )
                            }
                        </View>
                        {this.state.status === 'admin' && this.state.jenis_challenge === 'Sit Up' && this.state.jenis_challenge === 'Back Up' && this.state.jenis_challenge === 'Push Up' && this.state.jenis_challenge === 'Pull Up' && this.state.jenis_challenge === 'Squats' && this.state.jenis_challenge === 'Squat Jump' && this.state.jenis_challenge === 'Jumping Jack' && this.state.jenis_challenge === 'Mountain Climbers' && (
                            <TouchableOpacity onPress={() => this.setState({ visible: true })} style={[ b.bordered, p.center, b.ml2, b.roundedLow, { width: xs(60), backgroundColor: '#000' }]}>
                                <Text style={[ b.mt1, b.mb1, c.light, f.bold ]}>Edit</Text>
                            </TouchableOpacity>
                        )}
                        {this.state.status === 'admin' && this.state.jenis_challenge === 'Plank' && (
                            null
                        )}
                        {this.state.status === 'user' && (
                            null
                        )}
                    </View>
                    <FlatList
                        style={[ b.mt4 ]}
                        extraData={this.state}
                        data={this.state.list_peserta}
                        keyExtractor={item => item.id_progress_login}
                        renderItem={({item, index}) => (
                            <>
                                <View style={{ borderBottomWidth: 1, borderBottomColor: '#555555' }} />
                                <View style={[ p.row, b.mt2, b.mb2 ]}>
                                    <View style={[ p.center, { width: xs(40) }]}>
                                        <Text style={[ b.ml3, c.light ]}>{index+1}</Text>
                                    </View>
                                    <View style={[ b.ml2, p.center ]}>
                                        <FastImage source={item.url_gambar === null ? { uri: API_URL+'/src/avatar/default.png' } : { uri: API_URL+'/src/avatar/'+item.url_gambar }} style={{ width: xs(50), height: xs(50) }} />
                                    </View>
                                    <View style={[ b.ml2, { width: xs(80) }]}>
                                        {item.id_login === this.state.id_login ?
                                            <Text style={[ c.blue, f.bold, f._12, { marginTop: vs(5) }]}>Anda</Text>
                                            :
                                            <Text style={[ f.bold, c.light, f._12, { marginTop: vs(5) }]}>{item.nama}</Text>
                                        }
                                        <Text style={[ c.light, f._10 ]}>{item.kota}, {item.provinsi}</Text>
                                    </View>
                                    <View style={[ b.ml2, p.center, { width: xs(100) }]}>
                                        {this.state.jenis_challenge === 'Plank' ?
                                            <Text style={[ c.light, f.bold ]}>{hours} : {minutes} : {seconds} : {centiseconds}</Text>
                                            :
                                            (item.jumlah === null ?
                                                <Text style={[ c.light, f.bold ]}>- -</Text>
                                                :
                                                <Text style={[ c.light, f.bold ]}>{item.jumlah}</Text>
                                            )
                                        }
                                    </View>
                                    {item.id_progress_kompetisi === this.state.id_playing ?
                                        <View style={[ b.ml1, p.center, { width: xs(50) }]}>
                                            <Icon name="ios-play" color={"darkorange"} size={25} />
                                            <Text style={[ f.bold, { color: 'darkorange' }]}>Playing</Text>
                                        </View>
                                    :
                                        <TouchableOpacity onPress={() => this.refresh(item.id_progress_kompetisi, false)} style={[ b.ml1, p.center, { width: xs(50) }]}>
                                            <Icon name="ios-play" color={blue} size={25} />
                                            <Text style={[f.bold, c.blue ]}>Play</Text>
                                        </TouchableOpacity>
                                    }
                                </View>
                                <View style={{ borderBottomWidth: 1, borderBottomColor: '#555555' }} />
                            </>
                        )}
                    />
                </View>
            )
        }
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    toolbar: {
      marginTop: 30,
      backgroundColor: 'white',
      padding: 10,
      borderRadius: 5,
    },
    mediaPlayer: {
      position: 'absolute',
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
      backgroundColor: 'black',
    },
  });