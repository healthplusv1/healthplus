import React from 'react'
import { View, Text, TouchableOpacity, Alert, ActivityIndicator, StatusBar } from 'react-native'
import { b, p, f, c } from '../../utils/StyleHelper'
import { vs, xs } from '../../utils/Responsive'
import { blue, light, grey, softBlue } from '../../utils/Color'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { RNCamera } from 'react-native-camera'
import Icon from 'react-native-vector-icons/Ionicons'
import { API_URL } from 'react-native-dotenv'

export default class UploadPhoto extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            isLoading: false,
            type: RNCamera.Constants.Type.back,
            flash: RNCamera.Constants.FlashMode.off,
            photos: false,
            fileName: '',
            file_image: '',
            id_group: null,
            data: null,
        }
    }

    async componentDidMount() {
        this.state.id_group = await this.props.navigation.getParam('id_group');
        StatusBar.setHidden(true);
    }

    flashSwitch = async () => {
        if (this.state.flash === RNCamera.Constants.FlashMode.off) {
            this.setState({flash: RNCamera.Constants.FlashMode.torch})
        } else {
            this.setState({flash: RNCamera.Constants.FlashMode.off})
        }
    }

    cameraSwitch = async () => {
        if (this.state.type === RNCamera.Constants.Type.back) {
            this.setState({type: RNCamera.Constants.Type.front})
        } else {
            this.setState({type: RNCamera.Constants.Type.back})
        }
    }

    createFormDataPhoto = (photo, id_group) => {
        let data = new FormData();
        data.append('file_image', {
            name: new Date().getTime()+'_'+this.state.id_group+'.jpg',
            type: 'image/jpg',
            uri : photo.uri
        });
        data.append("id_group", id_group);
        return data;
    }

    takePicture = async () => {
        this.state.id_group = await this.props.navigation.getParam('id_group')

        if (this.camera) {
            const options = { quality: 0.5, base64: true };
            this.state.file_image = await this.camera.takePictureAsync(options);
            console.log(this.state.file_image.fileName);

            Alert.alert(
                "",
                "Kirim Photo ini?",
                [
                    {
                        text: "Tidak",
                        onPress: () => this.props.navigation.pop(),
                        style: 'cancel'
                    },
                    {
                        text: "Ya",
                        onPress: () => {
                            this.setState({
                                isLoading: true
                            })

                            fetch(API_URL+'/main/update_profile_group', {
                                method: 'POST',
                                headers: {
                                    'Content-Type': 'multipart/form-data'
                                },
                                body: this.createFormDataPhoto(this.state.file_image, this.state.id_group)
                            })
                            .then(response => response.json())
                            .then(response => {
                                // alert(JSON.stringify(response))
                                console.log(response)
                                this.props.navigation.pop()
                            })
                            .catch(error => {
                                console.log('Upload Error', error);
                            })
                        }
                    }
                ]
            )

        }
    };

    render() {
        if(this.state.isLoading){
            return(
                <View style={{ left: 0, right: 0, top: 0, bottom: 0, position: 'absolute', alignItems: 'center', justifyContent: 'center', backgroundColor: 'transparent' }}>
                     <ActivityIndicator size="large" color="#0000ff"/>
                    <Text>Mengirim Photo...</Text>
                </View>
	      )
        }
        return (
            <View style={{ flex: 1, flexDirection: 'column', backgroundColor: 'black' }}>
                <RNCamera
                    ref={ref => {
                        this.camera = ref;
                    }}
                    style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'center' }}
                    type={this.state.type}
                    flashMode={this.state.flash}
                    androidCameraPermissionOptions={{
                        title: 'Permission to use camera',
                        message: 'We need your permission to use your camera',
                        buttonPositive: 'Ok',
                        buttonNegative: 'Cancel',
                    }}
                />
                <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center' }}>
                    {this.state.type === RNCamera.Constants.Type.front ?
                        null
                        :
                        (this.state.flash === RNCamera.Constants.FlashMode.torch ?
                            <TouchableOpacity onPress={() => { this.flashSwitch() }} style={[ p.center, { flex: 0, backgroundColor: '#fff', borderRadius: xs(40), width: xs(75), height: xs(75), margin: vs(15) }]}>
                                <Icon name="ios-flash" size={30} />
                                <Text style={{fontSize: 10}} >ON</Text>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity onPress={() => { this.flashSwitch() }} style={[ p.center, { flex: 0, backgroundColor: '#fff', borderRadius: xs(40), width: xs(75), height: xs(75), margin: vs(15) }]}>
                                <Icon name="ios-flash" size={30} />
                                <Text style={{fontSize: 10}} >OFF</Text>
                            </TouchableOpacity>
                        )
                    }
                    <TouchableOpacity onPress={this.takePicture.bind(this)} style={[ p.center, { flex: 0, backgroundColor: '#fff', borderRadius: xs(40), width: xs(75), height: xs(75), margin: vs(15) }]}>
                        <Icon name="ios-camera" size={30} />
                    </TouchableOpacity>
                    { this.state.type === RNCamera.Constants.Type.back ?
                        <TouchableOpacity onPress={() => this.cameraSwitch()} style={[ p.center, { flex: 0, backgroundColor: '#fff', borderRadius: xs(40), width: xs(75), height: xs(75), margin: vs(15) }]}>
                            <Icon name="ios-reverse-camera" size={30} />
                            <Text style={{fontSize: 10}} >REAR</Text>
                        </TouchableOpacity>
                        :
                        <TouchableOpacity onPress={() => this.cameraSwitch()} style={[ p.center, { flex: 0, backgroundColor: '#fff', borderRadius: xs(40), width: xs(75), height: xs(75), margin: vs(15) }]}>
                            <Icon name="ios-reverse-camera" size={30} />
                            <Text style={{fontSize: 10}} >FRONT</Text>
                        </TouchableOpacity>
                    }
                </View>
                <View style={{ width: '100%', height: xs(15) }} />
            </View>
        )
    }
}