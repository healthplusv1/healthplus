import React from 'react'
import { View, Text, Image, TouchableOpacity, FlatList, ScrollView, ActivityIndicator, StyleSheet, Alert, ToastAndroid } from 'react-native'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { b, p, f, c } from '../../utils/StyleHelper';
import { vs, xs } from '../../utils/Responsive';
import { blue, light, grey, softBlue } from '../../utils/Color';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import Icon from 'react-native-vector-icons/Ionicons'
import {NavigationEvents} from 'react-navigation'
import FastImage from 'react-native-fast-image'
import LinearGradient from 'react-native-linear-gradient'
import { API_URL } from 'react-native-dotenv'

export default class Notifikasi extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            list_notifikasi: [
                {id: 1, logo: require('../../asset/denied.png'), keterangan: 'Verifikasi akun anda gagal. Silahkan untuk melanjutkan verifikasi ulang.', type: 'Verifikasi Ulang'}
            ]
        }
    }

    async componentDidMount() {
        const id_login = await AsyncStorage.getItem('@id_login')
        
        axios.get(API_URL+'/main/get_all_notification', {params: {
            id_login_to: id_login
        }})
        .then(res => {
            this.setState({
                list_notifikasi: res.data.data,
                loading: false
            })
            // alert(JSON.stringify(res))
        })
    }

    async terima_kompetisi(id) {
        const id_login = await AsyncStorage.getItem('@id_login')
        const showToast = () => {
            ToastAndroid.show("Ajakan Diterima", ToastAndroid.SHORT);
        }

        axios.get(API_URL+'/main/insert_peserta_kompetisi', {params: {
            id_login: id_login,
            id_kompetisi: id
        }})
        .then(resp => {
            if (resp.data.status === 'true') {
                // alert(JSON.stringify(resp))
                showToast()
				this.componentDidMount()
			} else if (resp.data.status === 'not_enough_point') {
				Alert.alert("Poin Anda Tidak Cukup")
			} else {
				Alert.alert("Anda Gagal Mengikuti Kompetisi")
			}
        })
    }

    async tolak_kompetisi(id, id_login_from) {
        const id_login = await AsyncStorage.getItem('@id_login')
        const showToast = () => {
            ToastAndroid.show("Ajakan Ditolak", ToastAndroid.SHORT);
        }

        axios.get(API_URL+'/main/tolak_invite_kompetisi', {params: {
            id_login_to: id_login,
            id_login_from: id_login_from,
            id_kompetisi: id
        }})
        .then(() => {
            this.componentDidMount()
            showToast()
        })
    }

    async terima_league(id) {
        const id_login = await AsyncStorage.getItem('@id_login')
        const showToast = () => {
            ToastAndroid.show("Ajakan Diterima", ToastAndroid.SHORT);
        }

        axios.get(API_URL+'/main/insert_anggota_group', {params: {
            id_login: id_login,
            id_group: id
        }})
        .then(respon => {
            this.componentDidMount()
            showToast()
            // alert(JSON.stringify(respon))
        })
    }

    async tolak_league(id, id_login_from) {
        const id_login = await AsyncStorage.getItem('@id_login')
        const showToast = () => {
            ToastAndroid.show("Ajakan Ditolak", ToastAndroid.SHORT);
        }

        axios.get(API_URL+'/main/tolak_invite_league', {params: {
            id_login_to: id_login,
            id_login_from: id_login_from,
            id_group: id
        }})
        .then(() => {
            this.componentDidMount()
            showToast()
        })
    }

    async terima_join_league(id_login_from, id) {
        const showToast = () => {
            ToastAndroid.show("Perizinan Diterima", ToastAndroid.SHORT);
        }

        axios.get(API_URL+'/main/insert_join_anggota_group', {params: {
            id_login: id_login_from,
            id_group: id
        }})
        .then(respon => {
            this.componentDidMount()
            showToast()
            // alert(JSON.stringify(respon))
        })
    }

    async tolak_join_league(id, id_login_from) {
        const showToast = () => {
            ToastAndroid.show("Perizinan Ditolak", ToastAndroid.SHORT);
        }

        axios.get(API_URL+'/main/tolak_join_league', {params: {
            id_login_from: id_login_from,
            id_group: id
        }})
        .then(resp => {
            this.componentDidMount()
            showToast()
            // alert(JSON.stringify(resp))
        })
    }

    async terima_join_kompetisi(id_login_from, id) {
        const showToast = () => {
            ToastAndroid.show("Perizinan Diterima", ToastAndroid.SHORT);
        }

        axios.get(API_URL+'/main/insert_join_peserta_kompetisi', {params: {
            id_login: id_login_from,
            id_kompetisi: id
        }})
        .then(respon => {
            this.componentDidMount()
            showToast()
            // alert(JSON.stringify(respon))
        })
    }

    async tolak_join_kompetisi(id, id_login_from) {
        const showToast = () => {
            ToastAndroid.show("Perizinan Ditolak", ToastAndroid.SHORT);
        }

        axios.get(API_URL+'/main/tolak_join_kompetisi', {params: {
            id_login_from: id_login_from,
            id_kompetisi: id
        }})
        .then(resp => {
            this.componentDidMount()
            showToast()
            // alert(JSON.stringify(resp))
        })
        .catch(err => {
            // alert(JSON.stringify(err))
        })
    }

    render() {
        return (
            <>
                {this.state.loading ?
					<View style={[styles.container_loading, styles.horizontal_loading]}>
						<ActivityIndicator size="large" color="#0000ff" />
					</View>
					:
					null
				}
                <NavigationEvents onDidFocus={() => this.componentDidMount()} />
                <View style={{ width: '100%', alignItems: 'center', backgroundColor: '#121212' }}>
                    <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('4%'), marginBottom: wp('4%') }}>
                        <View style={{ width: wp('15%') }}>
                            <TouchableOpacity onPress={() => this.props.navigation.pop()}>
                                <FastImage source={require('../../asset/left.png')} style={{ width: vs(30), height: vs(30) }} />
                            </TouchableOpacity>
                        </View>
                        <View style={{ width: wp('60%'), alignItems: 'center' }}>
                            <Text style={[ c.light, f._18, { fontFamily: 'lineto-circular-pro-bold' }]}>Pemberitahuan</Text>
                        </View>
                        <View style={{ width: wp('15%') }}></View>
                    </View>
                </View>
                <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#181B22' }}>
                    <View style={{ width: '100%' }}>
                        <FlatList
                            extraData={this.state}
                            data={this.state.list_notifikasi}
                            keyExtractor={item => item.id_notifikasi}
                            renderItem={({item}) => (
                                (item.type === 'Verifikasi Ulang' ?
                                    <View style={{ width: '100%', alignItems: 'center' }}>
                                        <TouchableOpacity onPress={() => this.props.navigation.push('KTPUlang')} style={{ width: wp('90%'), backgroundColor: blue, marginTop: wp('2%'), alignItems: 'center', borderRadius: wp('1%') }}>
                                            <View style={{ width: wp('80%'), marginTop: wp('4%'), marginBottom: wp('4%'), flexDirection: 'row', alignItems: 'center' }}>
                                                <View style={{ width: wp('15%') }}>
                                                    <Image source={item.logo} style={{ width: wp('10%'), height: wp('10%'), tintColor: light, transform: [{ rotateY: '180deg' }] }} />
                                                </View>
                                                <View style={{ width: wp('65%') }}>
                                                    <Text style={[ c.light ]}>{item.keterangan}</Text>
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                    :
                                    <>
                                        <View style={[ p.row, b.mt2 ]}>
                                            <View style={[ b.ml4 ]}>
                                                <FastImage source={item.image === null ? require('../../asset/icon_group.png') : { uri: API_URL+'/src/icon_group/'+item.image }} style={{ width: xs(50), height: xs(50) }} />
                                            </View>
                                            <View style={[ b.ml2, { width: xs(250) }]}>
                                                <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>{item.title}</Text>
                                                <Text style={[ c.light, f._12 ]}>{item.message}</Text>
                                            </View>
                                        </View>
                                        <View style={{ width: '100%' }}>
                                            {item.type === 'League Invitation' ?
                                                <View style={[ p.row, { marginLeft: vs(80), marginTop: vs(5) }]}>
                                                    <TouchableOpacity onPress={() => this.tolak_league(item.id, item.id_login_from)} style={[ p.center, { borderWidth: 1, borderColor: '#555555', backgroundColor: '#000', width: xs(80), height: xs(30) }]}>
                                                        <Text style={[ c.light ]}>Tolak</Text>
                                                    </TouchableOpacity>
                                                    <TouchableOpacity onPress={() => this.terima_league(item.id)} style={[ b.ml2, p.center, { borderWidth: 1, borderColor: '#555555', backgroundColor: '#000', width: xs(80), height: xs(30) }]}>
                                                        <Text style={[ c.light ]}>Terima</Text>
                                                    </TouchableOpacity>
                                                </View>
                                                :
                                                null
                                            }
                                            {item.type === 'Competition Invitation' ?
                                                <View style={[ p.row, { marginLeft: vs(80), marginTop: vs(5) }]}>
                                                    <TouchableOpacity onPress={() => this.tolak_kompetisi(item.id, item.id_login_from)} style={[ p.center, { borderWidth: 1, borderColor: '#555555', backgroundColor: '#000', width: xs(80), height: xs(30) }]}>
                                                        <Text style={[ c.light ]}>Tolak</Text>
                                                    </TouchableOpacity>
                                                    <TouchableOpacity onPress={() => this.terima_kompetisi(item.id)} style={[ b.ml2, p.center, { borderWidth: 1, borderColor: '#555555', backgroundColor: '#000', height: xs(30) }]}>
                                                        <Text style={[ c.light, b.ml2, b.mr2 ]}>Terima <Text style={{ color: 'skyblue' }}>({item.poin} poin)</Text></Text>
                                                    </TouchableOpacity>
                                                </View>
                                                :
                                                null
                                            }
                                            {item.type === 'Join League Request' ?
                                                <View style={[ p.row, { marginLeft: vs(80), marginTop: vs(5) }]}>
                                                    <TouchableOpacity onPress={() => this.tolak_join_league(item.id, item.id_login_from)} style={[ p.center, { borderWidth: 1, borderColor: '#555555', backgroundColor: '#000', width: xs(80), height: xs(30) }]}>
                                                        <Text style={[ c.light ]}>Tolak</Text>
                                                    </TouchableOpacity>
                                                    <TouchableOpacity onPress={() => this.terima_join_league(item.id_login_from, item.id)} style={[ b.ml2, p.center, { borderWidth: 1, borderColor: '#555555', backgroundColor: '#000', width: xs(80), height: xs(30) }]}>
                                                        <Text style={[ c.light ]}>Terima</Text>
                                                    </TouchableOpacity>
                                                </View>
                                                :
                                                null
                                            }
                                            {item.type === 'Join Challenge Request' ?
                                                <View style={[ p.row, { marginLeft: vs(80), marginTop: vs(5) }]}>
                                                    <TouchableOpacity onPress={() => this.tolak_join_kompetisi(item.id, item.id_login_from)} style={[ p.center, { borderWidth: 1, borderColor: '#555555', backgroundColor: '#000', width: xs(80), height: xs(30) }]}>
                                                        <Text style={[ c.light ]}>Tolak</Text>
                                                    </TouchableOpacity>
                                                    <TouchableOpacity onPress={() => this.terima_join_kompetisi(item.id_login_from, item.id)} style={[ b.ml2, p.center, { borderWidth: 1, borderColor: '#555555', backgroundColor: '#000', width: xs(80), height: xs(30) }]}>
                                                        <Text style={[ c.light ]}>Terima</Text>
                                                    </TouchableOpacity>
                                                </View>
                                                :
                                                null
                                            }
                                        </View>
                                        <View style={[ b.mt2, { borderBottomWidth: 1, borderBottomColor: '#555555' }]} />
                                    </>
                                )
                            )}
                        />
                    </View>
                </ScrollView>
            </>
        )
    }
}

const styles = StyleSheet.create({
    // loading centered screenn
    container_loading: {
      flex: 1,
      position:'absolute',
      zIndex:2,
      width: '100%',
      height: '100%',
      justifyContent: "center",
      backgroundColor: 'rgba(0,0,0,0.7)'
    },
    horizontal_loading: {
      flexDirection: "row",
      justifyContent: "space-around",
      padding: 10
    },
})  