import React from 'react'
import { Text, View, TouchableOpacity, Image, ScrollView, Alert, TextInput, FlatList, KeyboardAvoidingView, Keyboard, ActivityIndicator, StyleSheet } from 'react-native'
import { b, p, f, c } from '../../utils/StyleHelper'
import { vs, xs } from '../../utils/Responsive'
import { blue, light } from '../../utils/Color'
import Svg from '../../utils/Svg'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios';
import {NavigationEvents} from 'react-navigation';
import FastImage from 'react-native-fast-image'
import { API_URL } from 'react-native-dotenv'

export default class UserPostKomentar extends React.Component {

	constructor(props){
		super(props);
		this.state = {
			loading: true,
			nama: null,
			deskripsi: null,
			url_gambar: '',
			tanggal_post: '',
			komentar: '...',
			comment: '',
			suka: '...',
			loading: true,
			all_komentar: '',
			like_id_login: '',
			gambar: '',
			post_gambar: '',
			id_login_profile: '',
		}
	}

	async componentDidMount() {
		const id_group = await AsyncStorage.getItem('@current_id_group')
		const id_login = await AsyncStorage.getItem('@id_login')
		// alert(id_login)
		this.setState({id_login: id_login})
		// alert(id_group)
		axios.get(API_URL+'/main/get_user_post_detail', {params:{
			id_user_post: this.props.navigation.getParam('id_user_post'),
			id_login: id_login
		}})
		.then(data => {
			this.setState({
				// list_posting: data.data.data,
				nama: data.data.data.nama,
				deskripsi: data.data.data.deskripsi_user_post,
				url_gambar: data.data.data.url_gambar,
				tanggal_post: data.data.data.tanggal_user_post,
				komentar: data.data.data.komentar_user_post,
				suka: data.data.data.suka_user_post,
				loading: false,
				like_id_login: data.data.data.liked_id_login,
				gambar: data.data.latest_like,
				post_gambar: data.data.data.gambar_user_post,
				id_login_profile: data.data.data.id_login,
			})
			// alert(data.data.data.komentar_user_post)
		})
		
		axios.get(API_URL+'/main/get_latest_komentar_user_post', {params:{
			id_user_post: this.props.navigation.getParam('id_user_post')
		}})
		.then(resp => {
			// alert(JSON.stringify(resp))
			this.setState({
				komentar: resp.data.data.komentar_user_post,
				loading: false
			})
		})

		axios.get(API_URL+'/main/get_all_komentar_user_post', {params:{
			id_user_post: this.props.navigation.getParam('id_user_post')
		}})
		.then(result => {
			// alert(JSON.stringify(result))
			this.setState({
				all_komentar: result.data.data,
				loading: false
			})
		})
	}

	async post() {
		const id_login = await AsyncStorage.getItem('@id_login')

		if (this.state.comment === '') {
			Alert.alert('Harap isi kolom komentar')
		} else {
			this.setState({
				loading: true
			})
		}

		axios.get(API_URL+'/main/tambah_komentar_user_post', {params:{
			id_user_post: this.props.navigation.getParam('id_user_post'),
			id_login: id_login,
			comment_user: this.state.comment
		}})
		.then(res => {
			if (res.data.status === 'true') {
				// this.props.navigation.navigate('LihatKome')
				this.setState({
					comment: ''
				})

				this.componentDidMount();
			} else {
				console.log("Gagal Post Comment")
			}
		})
		.catch(e => {
			console.log("upload_error"+e)
		})
	}

	hitung_hari(from_date) {
		var date1 = new Date(from_date); 
		var date2 = new Date(new Date().getFullYear(),new Date().getMonth() , new Date().getDate()); 
		// console.log(date1);
		// console.log(date2);
		  
		// To calculate the time difference of two dates 
		var Difference_In_Time = date2.getTime() - date1.getTime(); 
		  
		// To calculate the no. of days between two dates 
		var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
		if (typeof Difference_In_Days === 'number') {
			if(Number(Difference_In_Days.toString().replace('-', '')).toFixed() == 0){
				return 'Today';
			}else{
				return Number(Difference_In_Days.toString().replace('-', '')).toFixed()+' Days ago';
			}
		}
	}

	async _increaseValue(){
		const id_login = await AsyncStorage.getItem('@id_login');

		axios.get(API_URL+'/main/insert_like_user_post', {params:{
			id_login: id_login,
			id_user_post: this.props.navigation.getParam('id_user_post')
		}})
		.then(resp => {
		if (resp.data.status === 'true') {
			// Alert.alert("Berhasil")
			if (this.state.like_id_login === null) {
				this.setState({
					suka: '...',
					id_login: null
				})	
			}else{
				this.setState({
					suka: '...',
					id_login: id_login
				})	
			}
			axios.get(API_URL+'/main/get_user_post_latest_suka', {params:{
				id_user_post: this.props.navigation.getParam('id_user_post')
			}})
			.then(result => {
				// alert(JSON.stringify(result))
				this.setState({
				suka: result.data.data.suka_user_post
				})
			})
			.catch(e => {
				if (e.message === 'Network Error') {
				Alert.alert('Tidak Ada Koneksi Internet','Mohon Periksa Koneksi Internet Anda Lalu Restart Aplikasi')
				// Alert.alert('Network Error','Please Check Your Internet Connection then Restart The App')
				}else{
				// Alert.alert('', JSON.stringify(e.message))
				}
			})
		} else {
			Alert.alert("Gagal")
		}
		})
		.catch(err => {
			// Alert.alert("Error")
		})
    }
    
    async _decreaseValue() {
		const id_login = await AsyncStorage.getItem('@id_login')

		axios.get(API_URL+'/main/delete_like_user_post', {params:{
			id_login: id_login,
			id_user_post: this.props.navigation.getParam('id_user_post')
		}})
		.then(resp => {
			if (resp.data.status === 'true') {
				// Alert.alert("Berhasil")
				if (this.state.like_id_login === null) {
					this.setState({
						suka: '...',
						id_login: id_login
					})	
				}else{
					this.setState({
						suka: '...',
						id_login: null
					})	
				}
				axios.get(API_URL+'/main/get_user_post_latest_suka', {params:{
					id_user_post: this.props.navigation.getParam('id_user_post')
				}})
				.then(result => {
				// alert(JSON.stringify(result))
				this.setState({
				  suka: result.data.data.suka_user_post
				})
			  })
			  .catch(e => {
				if (e.message === 'Network Error') {
				  Alert.alert('Tidak Ada Koneksi Internet','Mohon Periksa Koneksi Internet Anda Lalu Restart Aplikasi')
				  // Alert.alert('Network Error','Please Check Your Internet Connection then Restart The App')
				}else{
				  // Alert.alert('', JSON.stringify(e.message))
				}
			  })
			} else {
				Alert.alert("Gagal")
			}
		})
		.catch(err => {
			// Alert.alert("Error")
		})
	}

	render() {
		return (
			<>
				{this.state.loading ?
					<View style={[styles.container_loading, styles.horizontal_loading]}>
						<ActivityIndicator size="large" color="#0000ff" />
					</View>
					:
					null
				}
				<NavigationEvents onDidFocus={() => this.componentDidMount()} />
				<View style={[ p.center, b.shadowHigh, { top: 0, width: '100%', height: xs(80), backgroundColor: '#000' }]}>
					<View style={[ p.row, { width: '100%' }]}>
						<TouchableOpacity onPress={() => this.props.navigation.pop()} style={[ b.ml4 ]}>
							<Image source={require('../../asset/back1.png')} style={{ width: xs(25), height: xs(25), tintColor: light }} />
						</TouchableOpacity>
						<View style={[ b.ml4, p.center, { width: xs(235) }]}>
							<Text style={[ f._18, c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>Postingan</Text>
						</View>
					</View>
				</View>
				<ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#181B22' }}>
					<View style={{ width: '100%' }}>
						<View style={[ p.row, b.mt3 ]}>
							<Image source={this.state.url_gambar === null ? { uri: API_URL+'/src/avatar/default.png' } : { uri: API_URL+'/src/avatar/'+this.state.url_gambar } } style={[ b.ml4, { width: xs(50), height: xs(50) }]} />
							<View style={[ b.ml3, b.mt2 ]}>
								<TouchableOpacity onPress={() => this.props.navigation.push('ProfTracking', {id_login: this.state.id_login_profile})}>
									<Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}> {this.state.nama} </Text>
								</TouchableOpacity>
								<Text style={[ f._12, { color: '#CCCCCC' }]}> {this.hitung_hari(this.state.tanggal_post)} </Text>
							</View>
						</View>
						<Text style={[ b.ml4, b.mt2, f._12, c.light ]}>{this.state.deskripsi}</Text>
						<View style={[ b.ml4, b.mt2 ]}>
							{this.state.post_gambar === null ?
								null
								:
								<FastImage source={{ uri: API_URL+'/src/post_group/'+this.state.post_gambar }} style={{ width: xs(320), height: xs(320) }} />
							}
						</View>

						<View style={[ p.row, b.mt4 ]}>
							<View style={[ b.bordered, p.row, p.center, { width: xs(180), height: xs(50) }]}>
								{this.state.like_id_login === this.state.id_login ?
									<TouchableOpacity activeOpacity={.5} onPress={() => {this._decreaseValue()}} style={[ b.ml2 ]}>
										<View style={[ p.row ]}>
											<Image source={require('../../asset/loved.png')} style={{ width: xs(30), height: xs(30) }} />
										</View>
									</TouchableOpacity>
									: 
									<TouchableOpacity activeOpacity={.5} onPress={() => {this._increaseValue()}} style={[ b.ml2 ]}>
										<View style={[ p.row ]}>
											<Image source={require('../../asset/love.png')} style={{ width: xs(30), height: xs(30), tintColor: light }} />
										</View>
									</TouchableOpacity>
								}
								<Text style={[ c.light ]}> {this.state.suka} </Text>
								<FlatList
									extraData={this.state}
									horizontal={true}
									data={this.state.gambar}
									keyExtractor={item => item.id_login}
									renderItem={({item}) => (
										<Image source={item.url_gambar === null ? { uri: API_URL+'/src/avatar/default.png' } : { uri: API_URL+'/src/avatar/'+item.url_gambar }} style={[ b.ml1, { width: xs(25), height: xs(25) }]} />
									)}
								/>
							</View>
							<View style={[ b.bordered, p.row, p.center, { width: xs(180), height: xs(50) }]}>
								<Image source={require('../../asset/comment.png')} style={{ width: xs(30), height: xs(30), tintColor: light }} />
								<Text style={[ b.ml1, c.light ]}> {this.state.komentar} </Text>
							</View>
						</View>
					</View>
					<ScrollView showsVerticalScrollIndicator={false}>
						<View style={{ width: '100%' }}>
							<FlatList
								extraData={this.state}
								data={this.state.all_komentar}
								keyExtractor={item => item.id_post_group}
								renderItem={({item}) => (
									<>
										<View style={[ p.row, b.mt2, b.mb2 ]}>
											<View style={{ width: xs(80) }}>
												<Image source={item.url_gambar === null ? { uri: API_URL+'/src/avatar/default.png' } : { uri: API_URL+'/src/avatar/'+item.url_gambar }} style={[ b.ml3, b.mt2, { width: xs(50), height: xs(50) }]} />
											</View>
											<View style={[ b.ml2, { width: xs(250) }]}>
												<View style={[ p.row, b.mt2 ]}>
													<TouchableOpacity onPress={() => this.props.navigation.push('ProfTracking', {id_login: item.id_login})}>
														<Text style={[ f._12, c.light ]}>{item.nama} </Text>
													</TouchableOpacity>
													<View style={[ b.mt1, { width: xs(8), height: xs(8), borderRadius: xs(8), backgroundColor: '#DADADA' }]} />
													<Text style={[ f._12, c.light ]}> {this.hitung_hari(item.tanggal_komentar)} </Text>
												</View>
												<Text style={[ b.mt2, c.light ]}>{item.comment_user}</Text>
											</View>
										</View>

										<View style={[ b.mt1, { borderBottomWidth: 2, borderBottomColor: '#DADADA' }]} />
									</>
								)}
							/>
						</View>
						<View style={{ height: xs(43) }} />
					</ScrollView>
				</ScrollView>
				<KeyboardAvoidingView style={{ position: 'relative', bottom: 0, zIndex: 1, backgroundColor: '#181B22' }}>
					<TouchableOpacity onPress={Keyboard.dismiss}>
						<View style={[ p.row, { width: '100%', backgroundColor: '#181B22' }]}>
							<TextInput style={[ b.ml4, c.light, { width: xs(220) }]}
								placeholder={"Tulis Komentar"}
								placeholderTextColor={light}
								multiline
								value={this.state.comment}
								onChangeText={text => {
									let comment = this.state.comment
									comment = text
									this.setState({ comment })
								}}
							/>
							<View style={[ b.ml4, p.center, { width: xs(100) }]}>
								<TouchableOpacity onPress={() => this.post()}>
									<Text style={[ c.blue, f.bold ]}>Kirim</Text>
								</TouchableOpacity>
							</View>
						</View>
					</TouchableOpacity>
				</KeyboardAvoidingView>
			</>
		)
	}
}

const styles = StyleSheet.create({
	// loading centered screenn
	container_loading: {
	  flex: 1,
	  position:'absolute',
	  zIndex:1,
	  width: '100%',
	  height: '100%',
	  justifyContent: "center",
	  backgroundColor: 'rgba(0,0,0,0.7)'
	},
	horizontal_loading: {
	  flexDirection: "row",
	  justifyContent: "space-around",
	  padding: 10
	}
  });