import React from 'react'
import { View, Text, ScrollView, TouchableOpacity, FlatList } from 'react-native'
import { b, p, f, c } from '../../utils/StyleHelper'
import { vs, xs } from '../../utils/Responsive'
import { blue, light, grey, softBlue } from '../../utils/Color'
import {NavigationEvents} from 'react-navigation'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'
import FastImage from 'react-native-fast-image'
import Icon from 'react-native-vector-icons/Ionicons'
import analytics from '@react-native-firebase/analytics'
import { API_URL } from 'react-native-dotenv'

export default class Pengikut extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            list_cari_teman: '',
        }
    }

    async componentDidMount() {
        await analytics().logEvent('view_friend', {
            item: 'FOLLOWER_SCREEN'
        })
        await analytics().setUserProperty('user_sees_friend_request', 'null')

        const id_login = await AsyncStorage.getItem('@id_login')
        axios.get(API_URL+'/main/get_all_requesting_friend', {params: {
            id_login_to: id_login
        }})
        .then(res => {
            this.setState({
                list_cari_teman: res.data.data
            })
            // alert(JSON.stringify(res))
        })
    }

    async reject(id_login_from) {
        await analytics().logEvent('declined_friend', {
            item: 'friend_id'
        })
        await analytics().setUserProperty('user_reject_request', 'null')

        const id_login = await AsyncStorage.getItem('@id_login')
        axios.get(API_URL+'/main/reject_request_friend', {params: {
            id_login_to: id_login,
            id_login_from: id_login_from
        }})
        .then(res => {
            this.componentDidMount()
        })
    }

    async accept(id_login_from) {
        await analytics().logEvent('added_friend', {
            item: 'friend_id'
        })
        await analytics().setUserProperty('user_accept_request', 'null')

        const id_login = await AsyncStorage.getItem('@id_login')
        axios.get(API_URL+'/main/accept_request_friend', {params: {
            id_login_to: id_login,
            id_login_from: id_login_from
        }})
        .then(res => {
            this.componentDidMount()
        })
    }

    render() {
        return (
            <>
                <NavigationEvents onDidFocus={() => this.componentDidMount()} />
                <View style={{ width: '100%', height: xs(80), backgroundColor: '#000' }}>
                    <View style={[ p.row, b.ml4, b.mt4 ]}>
                        <View style={{ width: xs(55) }}>
                            <TouchableOpacity onPress={() => this.props.navigation.pop()}>
                                <Icon name="ios-arrow-back" size={40} color={light} />
                            </TouchableOpacity>
                        </View>
                        <View style={[ b.ml2, p.center, { width: xs(210) }]}>
                            <Text style={[ c.light, f.bold, f._18 ]}>Permintaan Pertemanan</Text>
                        </View>
                    </View>
                </View>
                <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#181B22' }}>
                    <View style={{ width: '100%' }}>
                        <FlatList
                            extraData={this.state}
							data={this.state.list_cari_teman}
							keyExtractor={item => item.id_login}
							renderItem={ ({item}) => (
                                <>
                                    <View style={[ p.center, b.mt2, { width: '100%' }]}>
                                        <View style={[ p.row, { width: '100%' }]}>
                                            <FastImage source={item.url_gambar === null ? { uri: API_URL+'/src/avatar/default.png' } : { uri: API_URL+'/src/avatar/'+item.url_gambar }} style={[ b.ml2, { width: xs(50), height: xs(50) }]} />
                                            <TouchableOpacity onPress={() => this.props.navigation.push('ProfTracking', {id_login: item.id_login})}>
                                                <Text style={[ b.mt3, b.ml2, c.light ]}>{item.nama}</Text>
                                            </TouchableOpacity>
                                        </View>
                                        <View style={[ p.alignEnd, p.row, { width: xs(200), marginLeft: vs(-20), marginTop: vs(-5) }]}>
                                            <TouchableOpacity onPress={() => this.reject(item.id_login)} style={[ p.center, b.roundedLow, { marginRight: vs(5), width: xs(80), height: xs(30), borderWidth: 2, borderColor: '#555555', backgroundColor: '#000' }]}>
                                                <Text style={[ c.light, f.bold ]}>Tolak</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={() => this.accept(item.id_login)} style={[ p.center, b.roundedLow, { width: xs(80), height: xs(30), borderWidth: 2, borderColor: '#555555', backgroundColor: '#000' }]}>
                                                <Text style={[ c.light, f.bold ]}>Terima</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                    <View style={[ b.mt2, { borderWidth: 1, borderColor: '#E0E3E8' }]} />
                                </>
                            )}
                        />
                    </View>
                </ScrollView>
            </>
        )
    }
}