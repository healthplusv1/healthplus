import React from 'react'
import { View, Text, ScrollView, TouchableOpacity, FlatList } from 'react-native'
import { b, p, f, c } from '../../utils/StyleHelper'
import { vs, xs } from '../../utils/Responsive'
import { blue, light, grey, softBlue } from '../../utils/Color'
import {NavigationEvents} from 'react-navigation'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'
import FastImage from 'react-native-fast-image'
import Icon from 'react-native-vector-icons/Ionicons'
import analytics from '@react-native-firebase/analytics'
import { API_URL } from 'react-native-dotenv'

export default class CariTeman extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            list_cari_teman: '',
        }
    }

    async componentDidMount() {
        await analytics().logEvent('friends_request', {
            item: 'PROFILE_SCREEN'
        })
        await analytics().setUserProperty('user_invite_friend', 'null')
        
        const id_login = await AsyncStorage.getItem('@id_login')

        axios.get(API_URL+'/main/get_all_not_friend', {params: {
            id_login_from: id_login
        }})
        .then(res => {
            this.setState({
                list_cari_teman: res.data.data
            })
            // alert(JSON.stringify(res))
        })
    }
    
    async request_friend(id_login_to){
        const id_login_from = await AsyncStorage.getItem('@id_login')

        axios.get(API_URL+'/main/request_friend', {params: {
            id_login_from: id_login_from,
            id_login_to: id_login_to,
        }})
        .then(res => {
            this.componentDidMount()
        })
    }

    render() {
        return (
            <>
                <View style={{ width: '100%', height: xs(80), backgroundColor: '#000' }}>
                    <View style={[ p.row, b.ml4, b.mt4 ]}>
                        <View style={{ width: xs(55) }}>
                            <TouchableOpacity onPress={() => this.props.navigation.pop()}>
                                <Icon name="ios-arrow-back" size={40} color={light} />
                            </TouchableOpacity>
                        </View>
                        <View style={[ b.ml2, p.center, { width: xs(210) }]}>
                            <Text style={[ c.light, f.bold, f._18 ]}>Cari Teman</Text>
                        </View>
                    </View>
                </View>
                <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#181B22' }}>
                    <View style={{ width: '100%' }}>
                        <FlatList
                            extraData={this.state}
							data={this.state.list_cari_teman}
							keyExtractor={item => item.id_login}
							renderItem={ ({item}) => (
                                <>
                                    <View style={[ p.row, p.center, b.mt2, { width: '100%', height: xs(60) }]}>
                                        <View style={[ p.row, { width: xs(180) }]}>
                                            <FastImage source={item.url_gambar === null ? { uri: API_URL+'/src/avatar/default.png' } : { uri: API_URL+'/src/avatar/'+item.url_gambar }} style={{ width: xs(50), height: xs(50) }} />
                                            <TouchableOpacity onPress={() => this.props.navigation.push('ProfTracking', {id_login: item.id_login})} style={{ width: xs(140) }}>
                                                <Text style={[ b.mt3, b.ml2, c.light ]} numberOfLines={1}>{item.nama}</Text>
                                            </TouchableOpacity>
                                        </View>
                                        <View style={[ p.alignEnd, b.ml2, { width: xs(150) }]}>
                                            <TouchableOpacity onPress={() => this.request_friend(item.id_login)} style={[ p.center, b.roundedLow, { borderWidth: 2, borderColor: '#555555', backgroundColor: '#000' }]}>
                                                <Text style={[ c.light, f.bold, b.ml4, b.mr4, b.mt1, b.mb1 ]}>Tambah Teman</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                    <View style={[ b.mt2, { borderWidth: 1, borderColor: '#E0E3E8' }]} />
                                </>
                            )}
                        />
                    </View>
                </ScrollView>
            </>
        )
    }
}