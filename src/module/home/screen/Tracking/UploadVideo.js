import React from 'react'
import { Text, View, BackHandler, TouchableOpacity, TextInput, StyleSheet, PixelRatio, ScrollView, Image, Alert, ActivityIndicator, StatusBar } from 'react-native'
import { b, p, f, c } from '../../utils/StyleHelper'
import { vs, xs } from '../../utils/Responsive'
import { blue, light, grey, softBlue } from '../../utils/Color'
import ImagePicker from 'react-native-image-picker';
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { RNCamera } from 'react-native-camera'
import Icon from 'react-native-vector-icons/Ionicons'
import Dialog, { DialogFooter, DialogButton, DialogContent, DialogTitle } from 'react-native-popup-dialog';
import FastImage from 'react-native-fast-image'
import { API_URL } from 'react-native-dotenv'

export default class UploadVideo extends React.Component {

	constructor(props){
		super(props);
		this.state ={
            type: RNCamera.Constants.Type.back,
            data: null,
            recording: false,
            flash: RNCamera.Constants.FlashMode.off,
            timerOn: false,
			timerStart: 0,
            timerTime: 0,
            id_login: null,
            isLoading: false,
            visible: true,
            pull_up_visible: true,
            sit_up_visible: true,
            back_up_visible: true,
            plank_visible: true,
            mountain_climber_visible: true,
            jumping_jacks_visible: true,
            squats_visible: true,
            squat_jump_visible: true,
            kirim_visible: false,
            jumlah_nilai: '',
		}
    }

    async componentDidMount() {
        this.state.id_login = await AsyncStorage.getItem('@id_login');
        StatusBar.setHidden(true);
        // alert(JSON.stringify(this.props.navigation.getParam('id_kompetisi')))
    }
    
    startRecording = async () => {
        if (this.camera) {
          this.setState({
              recording: true,
              timerOn: true,
              timerTime: this.state.timerTime,
              timerStart: Date.now() - this.state.timerTime,
            });
            this.timer = setInterval(() => {
                this.setState({
                    timerTime: Date.now() - this.state.timerStart
                });
            }, 10);

            const options = { videoBitrate: 2*1000*1000 };
            this.state.data = await this.camera.recordAsync(options);

            if(this.props.navigation.getParam('jenis_kompetisi') === 'Plank'){
                Alert.alert(
                    "",
                    "Kirim video ini?",
                    [
                        {
                          text: 'BUANG',
                          onPress: () => this.props.navigation.pop(),
                          style: "cancel"
                        },
                        { text: 'KIRIM', onPress: async () => {
                            this.setState({
                                isLoading: true
                            })
                            let formData = new FormData();
                            formData.append("file_video", {
                                name: new Date().getTime()+'_'+this.state.id_login+'.mp4',
                                uri: this.state.data.uri,
                                type: 'video/mp4'
                            });
                            formData.append("id_login", this.state.id_login);
                            formData.append("jumlah", this.state.jumlah_nilai);
                            formData.append("durasi", this.state.timerTime);
                            formData.append("id_kompetisi", this.props.navigation.getParam('id_kompetisi'));
    
                            try {
                                let response = await fetch(API_URL+'/main/upload_video', {
                                    method: 'post',
                                    headers: {
                                        'Content-Type': 'multipart/form-data',
                                    },
                                    body: formData
                                });
                                console.log( await response.json() )
                                Alert.alert('', 'Berhasil Terkirim')
                                this.props.navigation.pop()
                            }
                            catch (error) {
                                console.log('error : ' + error);
                            }
                        } }
                    ],  
                    { cancelable: false }
                )
            }

            
        }
    };
    
    stopRecording = async () => {
        this.setState({
            recording: false,
            timerOn: false,
            kirim_visible: true,
        });
        let data = await this.camera.stopRecording()
        clearInterval(this.timer);
    };
        
    flashSwitch = async () => {
        if (this.state.flash === RNCamera.Constants.FlashMode.off) {
            this.setState({flash: RNCamera.Constants.FlashMode.torch})
        } else {
            this.setState({flash: RNCamera.Constants.FlashMode.off})
        }
    }

    cameraSwitch = async () => {
        if (this.state.type === RNCamera.Constants.Type.back) {
            this.setState({type: RNCamera.Constants.Type.front})
        } else {
            this.setState({type: RNCamera.Constants.Type.back})
        }
    }

    updateInputVal = (val, prop) => {
	    const state = this.state;
	    state[prop] = val;
	    this.setState(state);
	}
        
	render() {
        if(this.state.isLoading){
            return(
                <View style={{ left: 0, right: 0, top: 0, bottom: 0, position: 'absolute', alignItems: 'center', justifyContent: 'center', backgroundColor: 'transparent' }}>
                     <ActivityIndicator size="large" color="#0000ff"/>
                    <Text>Mengirim Video...</Text>
                </View>
	      )
        }
        const { timerTime } = this.state;
        let centiseconds = ("0" + (Math.floor(timerTime / 10) % 100)).slice(-2);
        let seconds = ("0" + (Math.floor(timerTime / 1000) % 60)).slice(-2);
        let minutes = ("0" + (Math.floor(timerTime / 60000) % 60)).slice(-2);
        let hours = ("0" + Math.floor(timerTime / 3600000)).slice(-2);
		return (
            <>
                {this.props.navigation.getParam('jenis_kompetisi') === 'Plank' ?
                    null
                    :
                    <Dialog
                        dialogTitle={<DialogTitle title="Kirim Video" />}
                        visible={this.state.kirim_visible}
                        onTouchOutside={() => {
                            this.props.navigation.pop()
                            this.setState({kirim_visible: false})
                        }}
                        footer={
                            <DialogFooter>
                                <DialogButton
                                    text="BUANG"
                                    onPress={() => {
                                        this.props.navigation.pop()
                                        this.setState({kirim_visible: false})
                                }}
                                />
                                <DialogButton
                                    text="KIRIM"
                                    onPress={async () => {
                                        this.setState({ kirim_visible: false })
                                        
                                        if (this.state.jumlah_nilai === '') {
                                            Alert.alert(
                                                '',
                                                'Masukkan terlebih dahulu jumlahnya',
                                                [
                                                    {
                                                        text: 'OK', onPress: () => { this.setState({ kirim_visible: true }) }
                                                    }
                                                ]
                                            )
                                        } else {
                                            this.setState({
                                                isLoading: true
                                            })
                                            let formData = new FormData();
                                            formData.append("file_video", {
                                                name: new Date().getTime()+'_'+this.state.id_login+'.mp4',
                                                uri: this.state.data.uri,
                                                type: 'video/mp4'
                                            });
                                            formData.append("id_login", this.state.id_login);
                                            formData.append("jumlah", this.state.jumlah_nilai);
                                            formData.append("durasi", this.state.timerTime);
                                            formData.append("id_kompetisi", this.props.navigation.getParam('id_kompetisi'));

                                            setTimeout(() => {
                                                this.setState({ isLoading: true })
                                            }, 1000)

                                            try {
                                                let response = await fetch(API_URL+'/main/upload_video', {
                                                    method: 'post',
                                                    headers: {
                                                        'Content-Type': 'multipart/form-data',
                                                    },
                                                    body: formData
                                                });
                                                console.log( await response.json() )
                                                Alert.alert('', 'Berhasil Terkirim')
                                                this.props.navigation.pop()
                                            }
                                            catch (error) {
                                                console.log('error : ' + error);
                                            }
                                        }
                                    }}
                                />
                            </DialogFooter>
                        }
                    >
                        <DialogContent>
                            <View>
                                <View style={[ b.ml3, b.mr3, b.mt4, p.center ]}>
                                    <Text>Jumlah {this.props.navigation.getParam('jenis_kompetisi')}</Text>
                                    <TextInput
                                        style={[ f._20, { backgroundColor: '#EEEEEE', borderRadius: xs(10), textAlign: 'center', width: xs(50) }]}
                                        keyboardType={'numeric'}
                                        value={this.state.jumlah_nilai}
                                        onChangeText={(val) => this.updateInputVal(val, 'jumlah_nilai')}
                                    />
                                </View>
                            </View>
                        </DialogContent>
                    </Dialog>
                }
                {this.props.navigation.getParam('jenis_kompetisi') === 'Push Up'?
                    <Dialog
                        dialogTitle={<DialogTitle title="Instruksi" />}
                        visible={this.state.visible}
                        onTouchOutside={() => this.setState({ visible: false })}
                        footer={
                            <DialogFooter>
                                <DialogButton
                                    text="Ok, Mengerti!"
                                    onPress={() => {this.setState({ visible: false })}}
                                />
                            </DialogFooter>
                        }
                    >
                        <DialogContent>
                            <View>
                                <View style={[ b.ml3, b.mr3, b.mt4 ]}>
                                    <View style={[ p.row ]}>
                                        <View style={{ width: xs(20) }}>
                                            <View style={[ p.center, { width: xs(20), height: xs(20), borderWidth: 1, borderColor: blue, borderRadius: xs(10) }]}>
                                                <Text style={[ c.blue ]}>1</Text>
                                            </View>
                                        </View>
                                        <View style={[ b.ml2, { width: xs(230) }]}>
                                            <Text style={[ f._12 ]}>Turunkan tubuh Anda ke lantai, hingga siku membentuk sudut 90 derajat.</Text>
                                        </View>
                                    </View>
                                    <View style={[ p.center, b.mt2 ]}>
                                        <FastImage source={require('../../asset/group_1003.png')} style={{ width: xs(150), height: xs(82) }} />
                                    </View>
                                    <View style={[ p.row, b.mt2 ]}>
                                        <View style={{ width: xs(20) }}>
                                            <View style={[ p.center, { width: xs(20), height: xs(20), borderWidth: 1, borderColor: blue, borderRadius: xs(10) }]}>
                                                <Text style={[ c.blue ]}>2</Text>
                                            </View>
                                        </View>
                                        <View style={[ b.ml2, { width: xs(230) }]}>
                                            <Text style={[ f._12 ]}>Angkat tubuh Anda dengan gerakan mendorong lantai agar menjauh dari Anda.</Text>
                                        </View>
                                    </View>
                                    <View style={[ p.center, b.mt2 ]}>
                                        <FastImage source={require('../../asset/group_1005.png')} style={{ width: xs(150), height: xs(52) }} />
                                    </View>
                                    <View style={[ p.row, b.mt2 ]}>
                                        <View style={{ width: xs(20) }}>
                                            <View style={[ p.center, { width: xs(20), height: xs(20), borderWidth: 1, borderColor: blue, borderRadius: xs(10) }]}>
                                                <Text style={[ c.blue ]}>3</Text>
                                            </View>
                                        </View>
                                        <View style={[ b.ml2, { width: xs(230) }]}>
                                            <Text style={[ f._12 ]}>Ulangi menurunkan dan mengangkat tubuh Anda dengan kecepatan yang tetap.</Text>
                                        </View>
                                    </View>
                                    <View style={[ p.center, b.mt2 ]}>
                                        <FastImage source={require('../../asset/group_1006.png')} style={{ width: xs(150), height: xs(87) }} />
                                    </View>
                                </View>
                            </View>
                        </DialogContent>
                    </Dialog>
                    :
                    null
                }
                {/* Pull Up */}
                {this.props.navigation.getParam('jenis_kompetisi') === 'Pull Up'?
                    <Dialog
                        dialogTitle={<DialogTitle title="Instruksi" />}
                        visible={this.state.pull_up_visible}
                        onTouchOutside={() => this.setState({ pull_up_visible: false })}
                        footer={
                            <DialogFooter>
                                <DialogButton
                                    text="Ok, Mengerti!"
                                    onPress={() => {this.setState({ pull_up_visible: false })}}
                                />
                            </DialogFooter>
                        }
                    >
                        <DialogContent>
                            <View>
                                <View style={[ b.ml3, b.mr3, b.mt4 ]}>
                                    <View style={[ p.row ]}>
                                        <View style={{ width: xs(20) }}>
                                            <View style={[ p.center, { width: xs(20), height: xs(20), borderWidth: 1, borderColor: blue, borderRadius: xs(10) }]}>
                                                <Text style={[ c.blue ]}>1</Text>
                                            </View>
                                        </View>
                                        <View style={[ b.ml2, { width: xs(230) }]}>
                                            <Text style={[ f._12 ]}>Pegang bar pull up dengan telapak tangan menghadap kedepan. Tarik berat badan kamu keatas sampai dagu kamu sedikit diatas bar.</Text>
                                        </View>
                                    </View>
                                    <View style={[ p.center, b.mt2 ]}>
                                        <FastImage source={require('../../asset/Instruction/pull_up_1.png')} style={{ width: xs(75), height: xs(88) }} />
                                    </View>
                                    <View style={[ p.row, b.mt2 ]}>
                                        <View style={{ width: xs(20) }}>
                                            <View style={[ p.center, { width: xs(20), height: xs(20), borderWidth: 1, borderColor: blue, borderRadius: xs(10) }]}>
                                                <Text style={[ c.blue ]}>2</Text>
                                            </View>
                                        </View>
                                        <View style={[ b.ml2, { width: xs(230) }]}>
                                            <Text style={[ f._12 ]}>Turunkan badan kamu sampai tangan kamu hampir terentang secara penuh.</Text>
                                        </View>
                                    </View>
                                    <View style={[ p.center, b.mt2 ]}>
                                        <FastImage source={require('../../asset/Instruction/pull_up_2.png')} style={{ width: xs(75), height: xs(88) }} />
                                    </View>
                                    <View style={[ p.row, b.mt2 ]}>
                                        <View style={{ width: xs(20) }}>
                                            <View style={[ p.center, { width: xs(20), height: xs(20), borderWidth: 1, borderColor: blue, borderRadius: xs(10) }]}>
                                                <Text style={[ c.blue ]}>3</Text>
                                            </View>
                                        </View>
                                        <View style={[ b.ml2, { width: xs(230) }]}>
                                            <Text style={[ f._12 ]}>Ulangi menurunkan dan mengangkat tubuh Anda dengan kecepatan yang tetap.</Text>
                                        </View>
                                    </View>
                                    <View style={[ p.center, b.mt2 ]}>
                                        <FastImage source={require('../../asset/Instruction/pull_up_3.png')} style={{ width: xs(75), height: xs(88) }} />
                                    </View>
                                </View>
                            </View>
                        </DialogContent>
                    </Dialog>
                    :
                    null
                }
                {/* Sit Up */}
                {this.props.navigation.getParam('jenis_kompetisi') === 'Sit Up'?
                    <Dialog
                        dialogTitle={<DialogTitle title="Instruksi" />}
                        visible={this.state.sit_up_visible}
                        onTouchOutside={() => this.setState({ sit_up_visible: false })}
                        footer={
                            <DialogFooter>
                                <DialogButton
                                    text="Ok, Mengerti!"
                                    onPress={() => {this.setState({ sit_up_visible: false })}}
                                />
                            </DialogFooter>
                        }
                    >
                        <DialogContent>
                            <View>
                                <View style={[ b.ml3, b.mr3, b.mt4 ]}>
                                    <View style={[ p.row ]}>
                                        <View style={{ width: xs(20) }}>
                                            <View style={[ p.center, { width: xs(20), height: xs(20), borderWidth: 1, borderColor: blue, borderRadius: xs(10) }]}>
                                                <Text style={[ c.blue ]}>1</Text>
                                            </View>
                                        </View>
                                        <View style={[ b.ml2, { width: xs(230) }]}>
                                            <Text style={[ f._12 ]}>Berbaringlah terlentang sambil menekuk kedua lutut, dan sentuhkan ujung jari tangan di belakang telinga.</Text>
                                        </View>
                                    </View>
                                    <View style={[ p.center, b.mt2 ]}>
                                        <FastImage source={require('../../asset/Instruction/sit_up_1.png')} style={{ width: xs(180), height: xs(70) }} />
                                    </View>
                                    <View style={[ p.row, b.mt2 ]}>
                                        <View style={{ width: xs(20) }}>
                                            <View style={[ p.center, { width: xs(20), height: xs(20), borderWidth: 1, borderColor: blue, borderRadius: xs(10) }]}>
                                                <Text style={[ c.blue ]}>2</Text>
                                            </View>
                                        </View>
                                        <View style={[ b.ml2, { width: xs(230) }]}>
                                            <Text style={[ f._12 ]}>Bangunkan tubuh dari lantai lalu dekatkan dada ke paha.</Text>
                                        </View>
                                    </View>
                                    <View style={[ p.center, b.mt2 ]}>
                                        <FastImage source={require('../../asset/Instruction/sit_up_2.png')} style={{ width: xs(170), height: xs(80) }} />
                                    </View>
                                    <View style={[ p.row, b.mt2 ]}>
                                        <View style={{ width: xs(20) }}>
                                            <View style={[ p.center, { width: xs(20), height: xs(20), borderWidth: 1, borderColor: blue, borderRadius: xs(10) }]}>
                                                <Text style={[ c.blue ]}>3</Text>
                                            </View>
                                        </View>
                                        <View style={[ b.ml2, { width: xs(230) }]}>
                                            <Text style={[ f._12 ]}>Turunkan lagi tubuh ke lantai ke posisi semula.</Text>
                                        </View>
                                    </View>
                                    <View style={[ p.center, b.mt2 ]}>
                                        <FastImage source={require('../../asset/Instruction/sit_up_3.png')} style={{ width: xs(190), height: xs(80) }} />
                                    </View>
                                </View>
                            </View>
                        </DialogContent>
                    </Dialog>
                    :
                    null
                }
                {/* Back Up */}
                {this.props.navigation.getParam('jenis_kompetisi') === 'Back Up'?
                    <Dialog
                        dialogTitle={<DialogTitle title="Instruksi" />}
                        visible={this.state.back_up_visible}
                        onTouchOutside={() => this.setState({ back_up_visible: false })}
                        footer={
                            <DialogFooter>
                                <DialogButton
                                    text="Ok, Mengerti!"
                                    onPress={() => {this.setState({ back_up_visible: false })}}
                                />
                            </DialogFooter>
                        }
                    >
                        <DialogContent>
                            <View>
                                <View style={[ b.ml3, b.mr3, b.mt4 ]}>
                                    <View style={[ p.row ]}>
                                        <View style={{ width: xs(20) }}>
                                            <View style={[ p.center, { width: xs(20), height: xs(20), borderWidth: 1, borderColor: blue, borderRadius: xs(10) }]}>
                                                <Text style={[ c.blue ]}>1</Text>
                                            </View>
                                        </View>
                                        <View style={[ b.ml2, { width: xs(230) }]}>
                                            <Text style={[ f._12 ]}>Menghadap tengkurap, dan sentuhkan ujung jari tangan di belakang telinga.</Text>
                                        </View>
                                    </View>
                                    <View style={[ p.center, b.mt2 ]}>
                                        <FastImage source={require('../../asset/Instruction/back_up_1.png')} style={{ width: xs(170), height: xs(50) }} />
                                    </View>
                                    <View style={[ p.row, b.mt2 ]}>
                                        <View style={{ width: xs(20) }}>
                                            <View style={[ p.center, { width: xs(20), height: xs(20), borderWidth: 1, borderColor: blue, borderRadius: xs(10) }]}>
                                                <Text style={[ c.blue ]}>2</Text>
                                            </View>
                                        </View>
                                        <View style={[ b.ml2, { width: xs(230) }]}>
                                            <Text style={[ f._12 ]}>Bangunkan mulai dari dada sampai kepala ke atas.</Text>
                                        </View>
                                    </View>
                                    <View style={[ p.center, b.mt2 ]}>
                                        <FastImage source={require('../../asset/Instruction/back_up_2.png')} style={{ width: xs(160), height: xs(52) }} />
                                    </View>
                                    <View style={[ p.row, b.mt2 ]}>
                                        <View style={{ width: xs(20) }}>
                                            <View style={[ p.center, { width: xs(20), height: xs(20), borderWidth: 1, borderColor: blue, borderRadius: xs(10) }]}>
                                                <Text style={[ c.blue ]}>3</Text>
                                            </View>
                                        </View>
                                        <View style={[ b.ml2, { width: xs(230) }]}>
                                            <Text style={[ f._12 ]}>Turunkan lagi tubuh ke lantai ke posisi semula.</Text>
                                        </View>
                                    </View>
                                    <View style={[ p.center, b.mt2 ]}>
                                        <FastImage source={require('../../asset/Instruction/back_up_3.png')} style={{ width: xs(160), height: xs(50) }} />
                                    </View>
                                </View>
                            </View>
                        </DialogContent>
                    </Dialog>
                    :
                    null
                }
                {/* Plank */}
                {this.props.navigation.getParam('jenis_kompetisi') === 'Plank'?
                    <Dialog
                        dialogTitle={<DialogTitle title="Instruksi" />}
                        visible={this.state.plank_visible}
                        onTouchOutside={() => this.setState({ plank_visible: false })}
                        footer={
                            <DialogFooter>
                                <DialogButton
                                    text="Ok, Mengerti!"
                                    onPress={() => {this.setState({ plank_visible: false })}}
                                />
                            </DialogFooter>
                        }
                    >
                        <DialogContent>
                            <View>
                                <View style={[ b.ml3, b.mr3, b.mt4 ]}>
                                    <View style={[ p.row ]}>
                                        <View style={{ width: xs(20) }}>
                                            <View style={[ p.center, { width: xs(20), height: xs(20), borderWidth: 1, borderColor: blue, borderRadius: xs(10) }]}>
                                                <Text style={[ c.blue ]}>1</Text>
                                            </View>
                                        </View>
                                        <View style={[ b.ml2, { width: xs(230) }]}>
                                            <Text style={[ f._12 ]}>Posisi perut menghadap ke lantai dan kaki lurus di belakang tubuh.</Text>
                                        </View>
                                    </View>
                                    <View style={[ p.center, b.mt2 ]}>
                                        <FastImage source={require('../../asset/Instruction/plank_1.png')} style={{ width: xs(150), height: xs(40) }} />
                                    </View>
                                    <View style={[ p.row, b.mt2 ]}>
                                        <View style={{ width: xs(20) }}>
                                            <View style={[ p.center, { width: xs(20), height: xs(20), borderWidth: 1, borderColor: blue, borderRadius: xs(10) }]}>
                                                <Text style={[ c.blue ]}>2</Text>
                                            </View>
                                        </View>
                                        <View style={[ b.ml2, { width: xs(230) }]}>
                                            <Text style={[ f._12 ]}>Kemudian, turunkan lengan sehingga kita bertumpu pada siku, dengan posisi lengan pada sudut 90 derajat dan ujung siku berada dibawah dan sejajar bahu.</Text>
                                        </View>
                                    </View>
                                    <View style={[ p.center, b.mt2 ]}>
                                        <FastImage source={require('../../asset/Instruction/plank_2.png')} style={{ width: xs(150), height: xs(40) }} />
                                    </View>
                                </View>
                            </View>
                        </DialogContent>
                    </Dialog>
                    :
                    null
                }
                {/* Jumping Jacks */}
                {this.props.navigation.getParam('jenis_kompetisi') === 'Jumping Jack'?
                    <Dialog
                        dialogTitle={<DialogTitle title="Instruksi" />}
                        visible={this.state.jumping_jacks_visible}
                        onTouchOutside={() => this.setState({ jumping_jacks_visible: false })}
                        footer={
                            <DialogFooter>
                                <DialogButton
                                    text="Ok, Mengerti!"
                                    onPress={() => {this.setState({ jumping_jacks_visible: false })}}
                                />
                            </DialogFooter>
                        }
                    >
                        <DialogContent>
                            <View>
                                <View style={[ b.ml3, b.mr3, b.mt4 ]}>
                                    <View style={[ p.row ]}>
                                        <View style={{ width: xs(20) }}>
                                            <View style={[ p.center, { width: xs(20), height: xs(20), borderWidth: 1, borderColor: blue, borderRadius: xs(10) }]}>
                                                <Text style={[ c.blue ]}>1</Text>
                                            </View>
                                        </View>
                                        <View style={[ b.ml2, { width: xs(230) }]}>
                                            <Text style={[ f._12 ]}>Berdirilah tegak, biarkan kedua tangan tergantung rileks di sisi tubuh dan renggangkan kedua telapak kaki selebar bahu.</Text>
                                        </View>
                                    </View>
                                    <View style={[ p.center, b.mt2 ]}>
                                        <FastImage source={require('../../asset/Instruction/jumping_jack_1.png')} style={{ width: xs(25), height: xs(70) }} />
                                    </View>
                                    <View style={[ p.row, b.mt2 ]}>
                                        <View style={{ width: xs(20) }}>
                                            <View style={[ p.center, { width: xs(20), height: xs(20), borderWidth: 1, borderColor: blue, borderRadius: xs(10) }]}>
                                                <Text style={[ c.blue ]}>2</Text>
                                            </View>
                                        </View>
                                        <View style={[ b.ml2, { width: xs(230) }]}>
                                            <Text style={[ f._12 ]}>Meloncatlah sambil meluruskan kedua lengan ke atas.</Text>
                                        </View>
                                    </View>
                                    <View style={[ p.center, b.mt2 ]}>
                                        <FastImage source={require('../../asset/Instruction/jumping_jack_2.png')} style={{ width: xs(30), height: xs(70) }} />
                                    </View>
                                    <View style={[ p.row, b.mt2 ]}>
                                        <View style={{ width: xs(20) }}>
                                            <View style={[ p.center, { width: xs(20), height: xs(20), borderWidth: 1, borderColor: blue, borderRadius: xs(10) }]}>
                                                <Text style={[ c.blue ]}>3</Text>
                                            </View>
                                        </View>
                                        <View style={[ b.ml2, { width: xs(230) }]}>
                                            <Text style={[ f._12 ]}>Kembalilah ke posisi awal, dan luruskan kedua kaki, serta ulangi gerakan ini sesuai kebutuhan.</Text>
                                        </View>
                                    </View>
                                    <View style={[ p.center, b.mt2 ]}>
                                        <FastImage source={require('../../asset/Instruction/jumping_jack_3.png')} style={{ width: xs(40), height: xs(85) }} />
                                    </View>
                                </View>
                            </View>
                        </DialogContent>
                    </Dialog>
                    :
                    null
                }
                {/* Mountain Climber */}
                {this.props.navigation.getParam('jenis_kompetisi') === 'Mountain Climbers'?
                    <Dialog
                        dialogTitle={<DialogTitle title="Instruksi" />}
                        visible={this.state.mountain_climber_visible}
                        onTouchOutside={() => this.setState({ mountain_climber_visible: false })}
                        footer={
                            <DialogFooter>
                                <DialogButton
                                    text="Ok, Mengerti!"
                                    onPress={() => {this.setState({ mountain_climber_visible: false })}}
                                />
                            </DialogFooter>
                        }
                    >
                        <DialogContent>
                            <View>
                                <View style={[ b.ml3, b.mr3, b.mt4 ]}>
                                    <View style={[ p.row ]}>
                                        <View style={{ width: xs(20) }}>
                                            <View style={[ p.center, { width: xs(20), height: xs(20), borderWidth: 1, borderColor: blue, borderRadius: xs(10) }]}>
                                                <Text style={[ c.blue ]}>1</Text>
                                            </View>
                                        </View>
                                        <View style={[ b.ml2, { width: xs(230) }]}>
                                            <Text style={[ f._12 ]}>Lakukan posisi Push Up.</Text>
                                        </View>
                                    </View>
                                    <View style={[ p.center, b.mt2 ]}>
                                        <FastImage source={require('../../asset/Instruction/mountain_climber_1.png')} style={{ width: xs(150), height: xs(65) }} />
                                    </View>
                                    <View style={[ p.row, b.mt2 ]}>
                                        <View style={{ width: xs(20) }}>
                                            <View style={[ p.center, { width: xs(20), height: xs(20), borderWidth: 1, borderColor: blue, borderRadius: xs(10) }]}>
                                                <Text style={[ c.blue ]}>2</Text>
                                            </View>
                                        </View>
                                        <View style={[ b.ml2, { width: xs(230) }]}>
                                            <Text style={[ f._12 ]}>Tarik satu lutut ke atas dan ke arah tengah tubuh Anda.</Text>
                                        </View>
                                    </View>
                                    <View style={[ p.center, b.mt2 ]}>
                                        <FastImage source={require('../../asset/Instruction/mountain_climber_2.png')} style={{ width: xs(150), height: xs(65) }} />
                                    </View>
                                    <View style={[ p.row, b.mt2 ]}>
                                        <View style={{ width: xs(20) }}>
                                            <View style={[ p.center, { width: xs(20), height: xs(20), borderWidth: 1, borderColor: blue, borderRadius: xs(10) }]}>
                                                <Text style={[ c.blue ]}>3</Text>
                                            </View>
                                        </View>
                                        <View style={[ b.ml2, { width: xs(230) }]}>
                                            <Text style={[ f._12 ]}>Ulangi tindakan ini dengan lutut Anda yang lain, dan lanjutkan gerakan bergantian dengan kedua lutut.</Text>
                                        </View>
                                    </View>
                                    <View style={[ p.center, b.mt2 ]}>
                                        <FastImage source={require('../../asset/Instruction/mountain_climber_3.png')} style={{ width: xs(150), height: xs(65) }} />
                                    </View>
                                </View>
                            </View>
                        </DialogContent>
                    </Dialog>
                    :
                    null
                }
                {/* Squats */}
                {this.props.navigation.getParam('jenis_kompetisi') === 'Squats'?
                    <Dialog
                        dialogTitle={<DialogTitle title="Instruksi" />}
                        visible={this.state.squats_visible}
                        onTouchOutside={() => this.setState({ squats_visible: false })}
                        footer={
                            <DialogFooter>
                                <DialogButton
                                    text="Ok, Mengerti!"
                                    onPress={() => {this.setState({ squats_visible: false })}}
                                />
                            </DialogFooter>
                        }
                    >
                        <DialogContent>
                            <View>
                                <View style={[ b.ml3, b.mr3, b.mt4 ]}>
                                    <View style={[ p.row ]}>
                                        <View style={{ width: xs(20) }}>
                                            <View style={[ p.center, { width: xs(20), height: xs(20), borderWidth: 1, borderColor: blue, borderRadius: xs(10) }]}>
                                                <Text style={[ c.blue ]}>1</Text>
                                            </View>
                                        </View>
                                        <View style={[ b.ml2, { width: xs(230) }]}>
                                            <Text style={[ f._12 ]}>Berdiri tegak dengan kaki direntangkan kurang lebih selebar bahu.</Text>
                                        </View>
                                    </View>
                                    <View style={[ p.center, b.mt2 ]}>
                                        <FastImage source={require('../../asset/Instruction/squat_1.png')} style={{ width: xs(40), height: xs(90) }} />
                                    </View>
                                    <View style={[ p.row, b.mt2 ]}>
                                        <View style={{ width: xs(20) }}>
                                            <View style={[ p.center, { width: xs(20), height: xs(20), borderWidth: 1, borderColor: blue, borderRadius: xs(10) }]}>
                                                <Text style={[ c.blue ]}>2</Text>
                                            </View>
                                        </View>
                                        <View style={[ b.ml2, { width: xs(230) }]}>
                                            <Text style={[ f._12 ]}>Doronglah pinggul ke belakang dan tekuk lutut dengan perlahan hingga membentuk sudut 90 derajat.</Text>
                                        </View>
                                    </View>
                                    <View style={[ p.center, b.mt2 ]}>
                                        <FastImage source={require('../../asset/Instruction/squat_2.png')} style={{ width: xs(40), height: xs(50) }} />
                                    </View>
                                    <View style={[ p.row, b.mt2 ]}>
                                        <View style={{ width: xs(20) }}>
                                            <View style={[ p.center, { width: xs(20), height: xs(20), borderWidth: 1, borderColor: blue, borderRadius: xs(10) }]}>
                                                <Text style={[ c.blue ]}>3</Text>
                                            </View>
                                        </View>
                                        <View style={[ b.ml2, { width: xs(230) }]}>
                                            <Text style={[ f._12 ]}>Angkatlah tubuh secara perlahan hingga Anda kembali pada posisi awal.</Text>
                                        </View>
                                    </View>
                                    <View style={[ p.center, b.mt2 ]}>
                                        <FastImage source={require('../../asset/Instruction/squat_3.png')} style={{ width: xs(70), height: xs(100) }} />
                                    </View>
                                </View>
                            </View>
                        </DialogContent>
                    </Dialog>
                    :
                    null
                }
                {/* Squat Jump */}
                {this.props.navigation.getParam('jenis_kompetisi') === 'Squat Jump'?
                    <Dialog
                        dialogTitle={<DialogTitle title="Instruksi" />}
                        visible={this.state.squat_jump_visible}
                        onTouchOutside={() => this.setState({ squat_jump_visible: false })}
                        footer={
                            <DialogFooter>
                                <DialogButton
                                    text="Ok, Mengerti!"
                                    onPress={() => {this.setState({ squat_jump_visible: false })}}
                                />
                            </DialogFooter>
                        }
                    >
                        <DialogContent>
                            <View>
                                <View style={[ b.ml3, b.mr3, b.mt4 ]}>
                                    <View style={[ p.row ]}>
                                        <View style={{ width: xs(20) }}>
                                            <View style={[ p.center, { width: xs(20), height: xs(20), borderWidth: 1, borderColor: blue, borderRadius: xs(10) }]}>
                                                <Text style={[ c.blue ]}>1</Text>
                                            </View>
                                        </View>
                                        <View style={[ b.ml2, { width: xs(230) }]}>
                                            <Text style={[ f._12 ]}>Berdiri dengan kaki dibuka lebar, kedua tangan diletakkan di belakang kepala dengan siku menghadap luar dan tekuk lutut hingga paha sejajar dengan lantai.</Text>
                                        </View>
                                    </View>
                                    <View style={[ p.center, b.mt2 ]}>
                                        <FastImage source={require('../../asset/Instruction/squat_jump_1.png')} style={{ width: xs(50), height: xs(80) }} />
                                    </View>
                                    <View style={[ p.row, b.mt2 ]}>
                                        <View style={{ width: xs(20) }}>
                                            <View style={[ p.center, { width: xs(20), height: xs(20), borderWidth: 1, borderColor: blue, borderRadius: xs(10) }]}>
                                                <Text style={[ c.blue ]}>2</Text>
                                            </View>
                                        </View>
                                        <View style={[ b.ml2, { width: xs(230) }]}>
                                            <Text style={[ f._12 ]}>Angkat tubuh dan locat setinggi yang Anda bisa dengan mendorong ujung kaki ke lantai.</Text>
                                        </View>
                                    </View>
                                    <View style={[ p.center, b.mt2 ]}>
                                        <FastImage source={require('../../asset/Instruction/squat_jump_2.png')} style={{ width: xs(40), height: xs(70) }} />
                                    </View>
                                    <View style={[ p.row, b.mt2 ]}>
                                        <View style={{ width: xs(20) }}>
                                            <View style={[ p.center, { width: xs(20), height: xs(20), borderWidth: 1, borderColor: blue, borderRadius: xs(10) }]}>
                                                <Text style={[ c.blue ]}>3</Text>
                                            </View>
                                        </View>
                                        <View style={[ b.ml2, { width: xs(230) }]}>
                                            <Text style={[ f._12 ]}>Medaratlah secara perlahan dengan posisi jongkok, lakukan berulang secara bertahap sesuai dengan kemampuan Anda.</Text>
                                        </View>
                                    </View>
                                    <View style={[ p.center, b.mt2 ]}>
                                        <FastImage source={require('../../asset/Instruction/squat_jump_3.png')} style={{ width: xs(60), height: xs(70) }} />
                                    </View>
                                </View>
                            </View>
                        </DialogContent>
                    </Dialog>
                    :
                    null
                }
                <View style={{ flex: 1, flexDirection: 'column', backgroundColor: 'black' }}>
                    <RNCamera
                        ref={ref => {
                            this.camera = ref;
                        }}
                        style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'center' }}
                        type={this.state.type}
                        flashMode={this.state.flash}
                        defaultVideoQuality={RNCamera.Constants.VideoQuality["1080p"]}
                        androidCameraPermissionOptions={{
                            title: 'Permission to use camera',
                            message: 'We need your permission to use your camera',
                            buttonPositive: 'Ok',
                            buttonNegative: 'Cancel',
                        }}
                        androidRecordAudioPermissionOptions={{
                            title: 'Permission to use audio recording',
                            message: 'We need your permission to use your audio',
                            buttonPositive: 'Ok',
                            buttonNegative: 'Cancel',
                        }}
                    />
                    <Text style={[ p.textCenter, { color: 'red'}]} >
                        {hours} : {minutes} : {seconds} : {centiseconds}
                    </Text>
                    <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center' }}>
                        {this.state.type === RNCamera.Constants.Type.front ?
                            null
                            :
                            (this.state.flash === RNCamera.Constants.FlashMode.torch ?
                                <TouchableOpacity onPress={() => { this.flashSwitch() }} style={[ p.center, { flex: 0, backgroundColor: '#fff', borderRadius: xs(40), width: xs(75), height: xs(75), margin: vs(15) }]}>
                                    <Icon name="ios-flash" size={30} />
                                    <Text style={{fontSize: 10}} >ON</Text>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity onPress={() => { this.flashSwitch() }} style={[ p.center, { flex: 0, backgroundColor: '#fff', borderRadius: xs(40), width: xs(75), height: xs(75), margin: vs(15) }]}>
                                    <Icon name="ios-flash" size={30} />
                                    <Text style={{fontSize: 10}} >OFF</Text>
                                </TouchableOpacity>
                            )
                        }
                        { this.state.recording ?
                            (this.state.timerOn === true &&
                                <TouchableOpacity onPress={this.stopRecording.bind(this)} style={[ p.center, { flex: 0, backgroundColor: '#fff', borderRadius: xs(40), width: xs(75), height: xs(75), margin: vs(15) }]}>
                                    <Icon name="ios-square" style={{color: 'red'}} size={30} />
                                    <Text style={{fontSize: 10, color: 'red'}} >FINISH</Text>
                                </TouchableOpacity>
                            )
                            :
                            (this.state.timerOn === false && this.state.timerTime === 0 &&
                                <TouchableOpacity onPress={this.startRecording.bind(this)} style={[ p.center, { flex: 0, backgroundColor: '#fff', borderRadius: xs(40), width: xs(75), height: xs(75), margin: vs(15) }]}>
                                    <Icon name="ios-videocam" size={30} />
                                    <Text style={{fontSize: 10}} >START</Text>
                                </TouchableOpacity>
                            )
                        }
                        { this.state.recording ?
                            null
                            :
                            (this.state.type === RNCamera.Constants.Type.back ?
                                <TouchableOpacity onPress={() => this.cameraSwitch()} style={[ p.center, { flex: 0, backgroundColor: '#fff', borderRadius: xs(40), width: xs(75), height: xs(75), margin: vs(15) }]}>
                                    <Image source={require('../../../challenge/asset/switch_camera.png')} style={{ width: xs(30), height: xs(30) }} />
                                    <Text style={{fontSize: 10}} >REAR</Text>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity onPress={() => this.cameraSwitch()} style={[ p.center, { flex: 0, backgroundColor: '#fff', borderRadius: xs(40), width: xs(75), height: xs(75), margin: vs(15) }]}>
                                    <Image source={require('../../../challenge/asset/switch_camera.png')} style={{ width: xs(30), height: xs(30) }} />
                                    <Text style={{fontSize: 10}} >FRONT</Text>
                                </TouchableOpacity>
                            )
                        }
                    </View>
                    <View style={{ width: '100%', height: xs(15) }} />
                </View>
            </>
		)
	}
}

const styles = StyleSheet.create({
  images: {
    width: xs(315),
    height: xs(195),
    marginHorizontal: 3,
  },
});