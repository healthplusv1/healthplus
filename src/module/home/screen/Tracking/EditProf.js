import React from 'react'
import { Text, View, TouchableOpacity, Image, TextInput, Alert, ActivityIndicator, StyleSheet } from 'react-native'
import { b, p, f, c } from '../../utils/StyleHelper'
import { vs, xs } from '../../utils/Responsive'
import { blue, light, grey, softBlue } from '../../utils/Color'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { API_URL } from 'react-native-dotenv'

export default class EditProf extends React.Component {

	constructor(props){
		super(props);
		this.state ={
			bio: '',
			kota: '',
			provinsi: '',
			url_gambar: '',
			loading: true
		}
	}

	async componentDidMount() {
		const id_login = await AsyncStorage.getItem('@id_login')

		axios.get(API_URL+'/main/get_user_login', {params:{
			id_login_from: id_login,
			id_login_to: this.props.navigation.getParam('id_login')
		}})
		.then(response => {
			// alert(JSON.stringify(response))
			this.setState({	
				loading: false,
				bio: response.data.data.bio,
				kota: response.data.data.kota,
				provinsi: response.data.data.provinsi,
				url_gambar: response.data.data.url_gambar
			})
		})
	}

	async updateProfile() {
		this.setState({ loading: true })
		const id_login = await AsyncStorage.getItem('@id_login')

		axios.get(API_URL+'/main/update_user_login', {params:{
			id_login: id_login,
			bio: this.state.bio,
			kota: this.state.kota,
			provinsi: this.state.provinsi
		}})
		.then(resp => {
			// alert(JSON.stringify(resp))
			this.setState({
				loading: false
			})
			if (resp.data.status == 'true') {
				this.props.navigation.navigate('ProfTracking')
			} else {
				Alert.alert("Harap Lengkapi Data Tersebut")
			}
		})
	}

	// updateInputVal = (val, prop) => {
	//     const state = this.state;
	//     state[prop] = val;
	//     this.setState(state);
	// }

	render() {
		return (
			<>
				{this.state.loading ?
					<View style={[styles.container_loading, styles.horizontal_loading]}>
						<ActivityIndicator size="large" color="#9E9E9E"/>
					</View>
					:
					null
				}
				<View style={[ p.center, { width: '100%', height: xs(80), backgroundColor: '#000' }]}>
					<View style={[ p.row, { width: '100%' }]}>
						<TouchableOpacity onPress={() => this.props.navigation.pop()} style={[ b.ml4 ]}>
							<Image source={require('../../asset/back.png')} style={{ width: xs(25), height: xs(25) }} />
						</TouchableOpacity>
						<Text style={[ f.bold, f._18, b.ml4, c.light ]}> Edit Profil </Text>
						<TouchableOpacity onPress={() => this.updateProfile()} style={{ marginLeft: vs(120) }}>
							<Text style={[ c.light, f._18 ]}> Selesai </Text>
						</TouchableOpacity>
					</View>
				</View>
				<View style={{ width: '100%', height: vs(550), backgroundColor: '#181B22' }}>
					<View style={[ p.center, { width: '100%', height: xs(150) }]}>
						<Image source={this.state.url_gambar === null ? { uri: API_URL+'/src/avatar/default.png' } : { uri: API_URL+'/src/avatar/'+this.state.url_gambar } } style={{ width: xs(100), height: xs(100) }} />
					</View>
					<Text style={[ c.light ]}> Bio </Text>
					<TextInput
						style={[ c.light ]}
						multiline
						underlineColorAndroid = '#b8b8b8'
						value={this.state.bio}
						onChangeText={(text) => this.setState({ bio: text })} />
					<Text style={[ c.light ]}> Kota </Text>
					<TextInput
						style={[ c.light ]}
						multiline
						underlineColorAndroid = '#b8b8b8'
						value={this.state.kota}
						onChangeText={(text) => this.setState({ kota: text })} />
					<Text style={[ c.light ]}> Provinsi </Text>
					<TextInput
						style={[ c.light ]}
						multiline
						underlineColorAndroid = '#b8b8b8'
						value={this.state.provinsi}
						onChangeText={(text) => this.setState({ provinsi: text })} />
				</View>
			</>
		)
	}
}

const styles = StyleSheet.create({
	// loading centered screenn
	container_loading: {
	  flex: 1,
	  position:'absolute',
	  zIndex:1,
	  width: '100%',
	  height: '100%',
	  justifyContent: "center",
	  backgroundColor: 'rgba(0,0,0,0.7)'
	},
	horizontal_loading: {
	  flexDirection: "row",
	  justifyContent: "space-around",
	  padding: 10
	},
  });