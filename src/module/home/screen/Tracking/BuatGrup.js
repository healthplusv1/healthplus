import React from 'react'
import { Text, View, TouchableOpacity, Image, ImageBackground, ScrollView, Alert, BackHandler, TextInput, ActivityIndicator, StyleSheet, PermissionsAndroid } from 'react-native'
import { b, p, f, c } from '../../utils/StyleHelper'
import { vs, xs } from '../../utils/Responsive'
import { blue, light, grey, softBlue } from '../../utils/Color'
import Svg from '../../utils/Svg'
import { launchCamera, launchImageLibrary } from 'react-native-image-picker'
import Dialog, { DialogContent } from 'react-native-popup-dialog'
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage'
import analytics from '@react-native-firebase/analytics'
import { API_URL, API_LOCAL } from 'react-native-dotenv'

const options = {
	title: 'Select Avatar',
	storageOptions: {
		skipBackup: true,
		path: 'images',
	}
};

export default class BuatGrup extends React.Component {

	constructor(props){
		super(props);
		this.state ={
			nama_group: '',
			lokasi: '',
			deskripsi: '',
			url_gambar: '',
            uri: '',
            fileName: null,
            file_image: null,
            isLoading: false,
			visible: false
		}
	}

	async componentDidMount() {
		await analytics().logEvent('create_league', {
			item: 'team_id'
		})
		await analytics().setUserProperty('user_create_league', 'null')
	}

	launch = async () => {
		try {
			await PermissionsAndroid.requestMultiple([
				PermissionsAndroid.PERMISSIONS.CAMERA,
				PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE
			])
			.then((result) => {
				if (result['android.permission.CAMERA'] && result['android.permission.WRITE_EXTERNAL_STORAGE'] === 'granted') {
					this.setState({ visible: true })
				} else {
					console.log('Permissions denied')
				}
			})
		} catch (error) {
			console.warn(error)
		}
	}

	camera = () => {
		launchCamera(options, (response) => {
			// console.log('Response = ', response);

			if (response.didCancel) {
				console.log('User cancelled image picker');
			} else if (response.error) {
				console.log('ImagePicker Error: ', response.error);
			} else if (response.customButton) {
				console.log('User tapped custom button: ', response.customButton);
				alert(response.customButton);
			} else {
				console.log('fileName: '+response.assets[0].fileName);
				this.setState({
					uri: {uri: response.assets[0].uri},
					url_gambar: response.assets[0].uri,
					fileName: response.assets[0].fileName,
					file_image: response,
					visible: false
				});
			}
		});
    }

	chooseFile = () => {
		launchImageLibrary(options, (response) => {
			// console.log('Response = ', response);

			if (response.didCancel) {
				console.log('User cancelled image picker');
			} else if (response.error) {
				console.log('ImagePicker Error: ', response.error);
			} else if (response.customButton) {
				console.log('User tapped custom button: ', response.customButton);
				alert(response.customButton);
			} else {
				console.log('fileName: '+response.assets[0].fileName);
				this.setState({
					uri: {uri: response.assets[0].uri},
					url_gambar: response.assets[0].uri,
					fileName: response.assets[0].fileName,
					file_image: response,
					visible: false
				});
			}
		});
	}

	createFormDataPhoto = (photo, id_login) => {
		const data = new FormData();

		if(this.state.fileName !== null) {
			data.append('file_image', {
				name : (photo.fileName == null)? 'myphoto': photo.fileName,
				type : photo.type,
				uri : photo.uri
			});
		}
		data.append("nama_group", this.state.nama_group);
		data.append("lokasi", this.state.lokasi);
		data.append("deskripsi", this.state.deskripsi);
		data.append("id_login", id_login);
		return data;
	}

	async insert_league() {
		const id_login = await AsyncStorage.getItem('@id_login')

		if (this.state.nama_group === '' || this.state.lokasi === '' || this.state.deskripsi === '') {
			Alert.alert('Harap isi semua data!')
		} else {
			this.setState({
				isLoading: true
			})
			fetch(API_URL+'/main/insert_group', {
				method: 'POST',
				headers: {
					'Content-Type': 'multipart/form-data'
				},
				body: this.createFormDataPhoto(this.state.file_image, id_login)
			})
			.then(response => response.json())
			.then(response => {
				// alert(JSON.stringify(response))
				this.setState({
					isLoading: false
				})
				Alert.alert(
					"",
					"League berhasil dibuat",
					[
						{ text: 'OK', onPress: () => this.props.navigation.navigate('Home') }
					]
				)
			})
			.catch(error => {
				console.log('Upload Error', error);
			})
		}

	}

	renderFileUri() {
	    if (this.state.url_gambar) {
	      return <Image
	        source={{ uri: this.state.url_gambar }}
	        style={[ p.center, { marginTop: vs(-40), width: xs(80), height: xs(80), borderRadius: xs(50) }]}
	      />
	    } else {
	      return <View style={[ p.center, { marginTop: vs(-40), width: xs(80), height: xs(80), backgroundColor: '#CFCFCF', borderRadius: xs(50) }]}>
					<Svg icon="camera" size={30} />
				</View>
	    }
	}

	clearText() {
		this.setState({
			lokasi: ''
		})
	}

	render() {
		return (
			<>
				{this.state.isLoading ?
					<View style={[styles.container_loading, styles.horizontal_loading]}>
						<ActivityIndicator size="large" color="#0000ff"/>
					</View>
					:
					null
				}
				<ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#181B22' }}>
					<Dialog
						visible={this.state.visible}
						onTouchOutside={() => this.setState({ visible: false })}
						dialogStyle={{ backgroundColor: light }}
					>
						<DialogContent>
							<View style={{ width: xs(250) }}>
								<Text style={[ f._18, f.bold, b.mt3 ]}>Select Image</Text>
								<TouchableOpacity onPress={() => this.camera()} style={[ b.mt3 ]}>
									<Text>Take Photo...</Text>
								</TouchableOpacity>
								<TouchableOpacity onPress={() => this.chooseFile()} style={[ b.mt3 ]}>
									<Text>Choose from Library...</Text>
								</TouchableOpacity>
								<View style={[ b.mt3, { width: '100%', alignItems: 'flex-end' }]}>
									<TouchableOpacity onPress={() => this.setState({ visible: false })}>
										<Text style={[ f.bold ]}>CANCEL</Text>
									</TouchableOpacity>
								</View>
							</View>
						</DialogContent>
					</Dialog>
					<View style={[ p.center, { width: '100%', height: xs(80), backgroundColor: '#000' }]}>
						<View style={[ p.row, { width: '100%' }]}>
							<View style={{ width: xs(50) }}>
								<TouchableOpacity onPress={() => this.props.navigation.navigate('Home')} style={[ b.ml4 ]}>
									<Image source={require('../../asset/back1.png')} style={{ width: xs(25), height: xs(25), tintColor: light }} />
								</TouchableOpacity>
							</View>
							<View style={[ b.ml4, { width: xs(150) }]}>
								<Text style={[ f.bold, f._18, c.light ]}> Buat Liga </Text>
							</View>
							<View style={[ b.ml4, p.alignEnd, { width: xs(100) }]}>
								<TouchableOpacity onPress={() => this.insert_league()}>
									<Text style={[ c.blue, f._18, f.bold ]}> Buat </Text>
								</TouchableOpacity>
							</View>
						</View>
					</View>
					<View style={{ width: '100%', height: xs(600), height: xs(650) }}>
						<Image source={require('../../asset/runner_01.png')} style={{ width: '100%', height: xs(180) }} />
						<View style={[ p.center, { width: '100%' }]}>
							<TouchableOpacity onPress={() => this.launch()}>
								{this.renderFileUri()}
							</TouchableOpacity>
						</View>
						<View style={[ b.mt4, { width: '100%' }]}>
							<Text style={[ b.ml4, { color: light }]}> NAMA Liga </Text>
							<View style={[ b.mt2, { width: '100%', backgroundColor: '#555555' }]}>
								<TextInput style={[ b.ml4, c.light, { width: xs(335) }]}
									value={this.state.nama_group}
									onChangeText={text => {
										let nama_group = this.state.nama_group
										nama_group = text
										this.setState({ nama_group });
									}}
								/>
							</View>

							<Text style={[ b.ml4, b.mt2, { color: light }]}> Lokasi </Text>
							<View style={[ b.mt2, p.row, { width: '100%', backgroundColor: '#555555' }]}>
								<TextInput style={[ b.ml4, c.light, { width: xs(280) }]}
								value={this.state.lokasi}
								onChangeText={text => {
									let lokasi = this.state.lokasi
									lokasi = text
									this.setState({ lokasi });
								}}
								/>
								<TouchableOpacity onPress={() => this.clearText()} style={{ marginTop: vs(10), marginLeft: vs(7) }}>
									<Text style={[ c.blue, f.bold ]}> Hapus </Text>
								</TouchableOpacity>
							</View>

							<Text style={[ b.ml4, b.mt2, { color: light }]}> Deskripsi </Text>
							<View style={[ b.mt2, { width: '100%', height: xs(100), backgroundColor: '#555555' }]}>
								<TextInput style={[ b.ml4, c.light, { width: xs(335) }]}
								multiline
								value={this.state.deskripsi}
								onChangeText={text => {
									let deskripsi = this.state.deskripsi
									deskripsi = text
									this.setState({ deskripsi });
								}}
								/>
							</View>
						</View>
					</View>
				</ScrollView>
			</>
		)
	}
}

const styles = StyleSheet.create({
	// loading centered screenn
	container_loading: {
	  flex: 1,
	  position:'absolute',
	  zIndex:1,
	  width: '100%',
	  height: '100%',
	  justifyContent: "center",
	  backgroundColor: 'rgba(0,0,0,0.7)'
	},
	horizontal_loading: {
	  flexDirection: "row",
	  justifyContent: "space-around",
	  padding: 10
	},
  });