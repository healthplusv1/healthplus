import React from 'react'
import { Text, View, TouchableOpacity, Image, ScrollView, Alert, FlatList, ActivityIndicator, StyleSheet, TouchableHighlight } from 'react-native'
import { b, p, f, c } from '../../utils/StyleHelper'
import { vs, xs } from '../../utils/Responsive'
import { blue, light, grey, softBlue } from '../../utils/Color'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import {NavigationEvents} from 'react-navigation'
import Icon from 'react-native-vector-icons/Ionicons'
import ActionButton from 'react-native-action-button';
import UserPost from './UserPost.js'
import { Item } from 'native-base'
import analytics from '@react-native-firebase/analytics'
import { API_URL } from 'react-native-dotenv'

export default class ProfTracking extends React.Component {

	constructor(props){
		super(props);
		this.state = {
			nama: 'Pratama',
			url_gambar: null,
			bio: 'Semangat',
			kota: 'Semarang',
			provinsi: 'Jawa Tengah',
			mengikuti: '...',
			pengikut: '...',
			loading: false,
			berteman: false,
			diri_sendiri: false,
			id_login: '',
			requester: '',
			total_request_friend: '',
			social: [
                {key: '0', url_gambar: require('../../asset/10.png'), nama: 'Michael', tanggal_post: '2021-04-10', deskripsi: 'Salam kenal semuanya', post_gambar: null, id_post_group: 1, komentar: '2', like_id_login: '', nama_group: 'USM Sehat', id_group: 1},
                {key: '1', url_gambar: require('../../asset/10.png'), nama: 'Pratama', tanggal_post: '2021-04-16', deskripsi: 'Push Up dulu guys', post_gambar: require('../../asset/pushUpp.jpg'), id_post_group: 2, komentar: '1', like_id_login: '', nama_group: 'GunungPati Sehat', id_group: 2},
            ],
            group2: [
                {id_group: '1', image: require('../../asset/icon_group.png'), nama_group: 'USM Sehat'},
                {id_group: '2', image: require('../../asset/icon_group.png'), nama_group: 'Lari Semarang'},
                {id_group: '3', image: require('../../asset/icon_group.png'), nama_group: 'Nike Run Club Semarang'},
            ],
            group1: [
                {id_group: '1', image: require('../../asset/icon_group.png'), nama_group: 'USM Sehat', lokasi: 'USM', anggota: '45'},
                {id_group: '2', image: require('../../asset/icon_group.png'), nama_group: 'UNNES Run Club', lokasi: 'UNNES', anggota: '36'},
                {id_group: '3', image: require('../../asset/icon_group.png'), nama_group: 'Semarang Sehat', lokasi: 'Simpang Lima', anggota: '40'},
            ],
            count: '',
            list_friends: [
                {id: 1, image: require('../../asset/10.png'), nama: 'Michael Taner', status: 'Teman'},
                {id: 2, image: require('../../asset/10.png'), nama: 'Michael Taner', status: 'Teman'},
                {id: 3, image: require('../../asset/10.png'), nama: 'Michael Taner', status: 'Teman'},
                {id: 4, image: require('../../asset/10.png'), nama: 'Michael Taner', status: 'Teman'},
                {id: 5, image: require('../../asset/10.png'), nama: 'Michael Taner', status: 'Teman'},
            ],
            add_friends: [
                {id: 1, image: require('../../asset/10.png'), nama: 'Michael Taner', status: 'Tambah'},
                {id: 2, image: require('../../asset/10.png'), nama: 'Michael Taner', status: 'Tambah'},
                {id: 3, image: require('../../asset/10.png'), nama: 'Michael Taner', status: 'Tambah'},
                {id: 4, image: require('../../asset/10.png'), nama: 'Michael Taner', status: 'Tambah'},
                {id: 5, image: require('../../asset/10.png'), nama: 'Michael Taner', status: 'Tambah'},
            ],
            request_friends: [
                {id: 1, image: require('../../asset/10.png'), nama: 'Michael Taner', status: 'Request'},
                {id: 2, image: require('../../asset/10.png'), nama: 'Michael Taner', status: 'Request'},
                {id: 3, image: require('../../asset/10.png'), nama: 'Michael Taner', status: 'Request'},
                {id: 4, image: require('../../asset/10.png'), nama: 'Michael Taner', status: 'Request'},
                {id: 5, image: require('../../asset/10.png'), nama: 'Michael Taner', status: 'Request'},
            ],
            status_friends: [
                {status: 'Teman'},
                {status: 'Tambahkan Teman'},
                {status: 'Permintaan Pertemanan'},
            ],
            detail_status_friends: null,
            clicked: 0,
            tabs_clicked: 0,
            tabs_title: [
                {image: require('../../asset/post_icon.png')},
                {image: require('../../asset/league_icon.png')},
                {image: require('../../asset/friend_icon.png')}
            ]
		}
	}

	async componentDidMount() {
		// const id_login = await AsyncStorage.getItem('@id_login')
		// let mySelf = 'true';
		// this.setState({id_login: id_login})

		// await analytics().logEvent('view_profile', {
		// 	item: 'PROFILE_SCREEN'
		// })
		// await analytics().setUserProperty('user_sees_profile', 'null')

		// if (id_login === this.props.navigation.getParam('id_login')) {
		// 	mySelf = 'true';
		// } else {
		// 	mySelf = 'false';
		// }
		// axios.get(API_URL+'/main/get_user_login', {params:{
		// 	id_login_from: id_login,
		// 	id_login_to: this.props.navigation.getParam('id_login'),
		// 	mySelf: mySelf
		// }})
		// .then(resp => {
		// 	// alert(JSON.stringify(resp.data.total_request_friend))
		// 	if (this.props.navigation.getParam('id_login') === id_login) {
		// 		// cari teman
		// 		this.setState({
		// 			diri_sendiri: true
		// 		})
		// 	}
		// 	this.setState({
		// 		loading: false,
		// 		nama: resp.data.data.nama,
		// 		url_gambar: resp.data.data.url_gambar,
		// 		bio: resp.data.data.bio,
		// 		kota: resp.data.data.kota,
		// 		provinsi: resp.data.data.provinsi,
		// 		mengikuti: resp.data.total_friend,
		// 		pengikut: resp.data.data.pengikut,
		// 		requester: resp.data.data.requester,
		// 		berteman: resp.data.data.berteman,
		// 		total_request_friend: resp.data.total_request_friend,
		// 	})
		// 	console.log('mengikuti: '+resp.data.total_friend)
		// })

		// axios.get(API_URL+'/main/get_all_user_post', {params:{
		// 	id_login_to : this.props.navigation.getParam('id_login'),
		// 	id_login: id_login
		// }})
		// .then(res => {
		// 	// alert(JSON.stringify(res.data.data))
		// 	this.setState({
		// 		list_user_post: res.data.data
		// 	})
		// })
	}

	async unfriend(id_login_to){
        const id_login_from = await AsyncStorage.getItem('@id_login')
        axios.get(API_URL+'/main/unfriend', {params: {
            id_login_from: id_login_from,
            id_login_to: id_login_to
        }})
        .then(res => {
            this.componentDidMount()
        })
    }

    async cancel_request_friend(id_login_to){
        const id_login_from = await AsyncStorage.getItem('@id_login')
        axios.get(API_URL+'/main/cancel_request_friend', {params: {
            id_login_from: id_login_from,
            id_login_to: id_login_to
        }})
        .then(res => {
            this.componentDidMount()
        })
	}
	
	async request_friend(id_login_to){
        const id_login_from = await AsyncStorage.getItem('@id_login')

        axios.get(API_URL+'/main/request_friend', {params: {
            id_login_from: id_login_from,
            id_login_to: id_login_to,
        }})
        .then(res => {
            this.componentDidMount()
        })
	}
	
	async reject(id_login_from) {

        const id_login = await AsyncStorage.getItem('@id_login')
        axios.get(API_URL+'/main/reject_request_friend', {params: {
            id_login_to: id_login,
            id_login_from: id_login_from
        }})
        .then(res => {
            this.componentDidMount()
        })
    }

    async accept(id_login_from) {

        const id_login = await AsyncStorage.getItem('@id_login')
        axios.get(API_URL+'/main/accept_request_friend', {params: {
            id_login_to: id_login,
            id_login_from: id_login_from
        }})
        .then(res => {
            this.componentDidMount()
        })
	}
	
	hitung_hari(from_date) {
		var date1 = new Date(from_date); 
		var date2 = new Date(new Date().getFullYear(),new Date().getMonth() , new Date().getDate()); 
		// console.log(date1);
		// console.log(date2);
		  
		// To calculate the time difference of two dates 
		var Difference_In_Time = date2.getTime() - date1.getTime(); 
		  
		// To calculate the no. of days between two dates 
		var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
		if (typeof Difference_In_Days === 'number') {
			if(Number(Difference_In_Days.toString().replace('-', '')).toFixed() == 0){
				return 'Today';
			}else{
				return Number(Difference_In_Days.toString().replace('-', '')).toFixed()+' Days ago';
			}
		}
	}

	async switchTab(index) {
        await this.setState({
            tabs_clicked: index
        })
    }

	render() {
		return (
			<>
				{this.state.loading ?
					<View style={[styles.container_loading, styles.horizontal_loading]}>
						<ActivityIndicator size="large" color="#0000ff" />
					</View>
					:
					null
				}
				<NavigationEvents onDidFocus={() => this.componentDidMount()} />
				{this.state.diri_sendiri === true ?
					<ActionButton buttonColor={blue} style={{ zIndex: 1 }} fixNativeFeedbackRadius={true}>
						<ActionButton.Item buttonColor='#9b59b6' title="Buat Postingan" onPress={() => this.props.navigation.navigate('BuatPostingan')}>
							<Icon name="md-create" size={20} color={light} />
						</ActionButton.Item>
					</ActionButton>
					:
					null
				}
				<View style={{ width: '100%', alignItems: 'center', backgroundColor: '#121212' }}>
					<View style={{ width: wp('90%'), marginTop: wp('4%'), marginBottom: wp('4%'), flexDirection: 'row', alignItems: 'center' }}>
						<View style={{ width: wp('15%') }}>
							<TouchableOpacity onPress={() => this.props.navigation.pop()}>
								<Image source={require('../../asset/left.png')} style={{ width: xs(25), height: xs(25) }} />
							</TouchableOpacity>
						</View>
						<View style={{ width: wp('60%'), alignItems: 'center' }}>
							<Text style={[ f._18, c.light, p.textRight, { fontFamily: 'lineto-circular-pro-bold' }]}> Profile </Text>
						</View>
						<View style={{ width: wp('15%') }}></View>
					</View>
				</View>
				<View style={{ width: '100%', backgroundColor: '#181B22' }}>
					<View style={[ p.row, b.mb2 ]}>
						{this.state.diri_sendiri === true ?
							<TouchableOpacity onPress={() => this.props.navigation.navigate('Avatar')}>
								<Image progressiveRenderingEnabled={true} source={this.state.url_gambar === null ? require('../../asset/10.png') : { uri: API_URL+'/src/avatar/'+this.state.url_gambar } } style={[ b.ml2, b.mt2, { width: xs(80), height: xs(80) }]} />
							</TouchableOpacity>
							:
							<Image progressiveRenderingEnabled={true} source={this.state.url_gambar === null ? require('../../asset/10.png') : { uri: API_URL+'/src/avatar/'+this.state.url_gambar } } style={[ b.ml2, b.mt2, { width: xs(80), height: xs(80) }]} />
						}
						<View style={[ b.ml2, { width: xs(225) }]}>
							<Text style={[ f._16, b.mt2, b.mb2, c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>{this.state.nama}</Text>
							{ this.state.diri_sendiri === true ?
								<>
									<View style={[ p.center, { width: xs(100) }]}>
										<TouchableOpacity onPress={() => this.props.navigation.push('Pengikut')} style={[ p.center ]}>
											<View>
												{this.state.total_request_friend !== '0' ?
													<View style={{ position: 'absolute', width: xs(8), height: xs(8), borderRadius: xs(10), backgroundColor: 'orangered', zIndex: 1 }} />
													:
													null
												}
												<Icon name="ios-notifications" size={20} color={light} />
											</View>
											<Text style={[ c.light, p.textCenter ]}>Permintaan</Text>
										</TouchableOpacity>
									</View>
									<TouchableOpacity onPress={() => this.props.navigation.push('CariTeman')} style={[ p.center, b.ml4, { width: xs(100), height: xs(40), borderWidth: 2, borderColor: blue }]}>
										<Text style={[ c.blue, c.light ]}> Cari Teman </Text>
									</TouchableOpacity>
								</>
								:
								( this.state.berteman === 'false' ?
									(this.state.requester === 'none' ?
										<TouchableOpacity onPress={() => this.request_friend(this.props.navigation.getParam('id_login'))} style={[ p.center, { width: xs(100), height: xs(40), borderWidth: 2, borderColor: blue }]}>
											<Text style={[ c.blue, c.light ]}> Ikuti </Text>
										</TouchableOpacity>
										:
										(this.state.requester === 'you' ?
											<TouchableOpacity onPress={() => this.cancel_request_friend(this.props.navigation.getParam('id_login'))} style={[ p.center, { width: xs(100), height: xs(40), borderWidth: 2, borderColor: blue }]}>
												<Text style={[ c.blue, c.light ]}> Batal Meminta </Text>
											</TouchableOpacity>
											:
											<>
												<TouchableOpacity onPress={() => this.reject(this.props.navigation.getParam('id_login'))} style={[ p.center, { width: xs(100), height: xs(40), borderWidth: 2, borderColor: blue }]}>
													<Text style={[ c.blue, c.light ]}> Tolak </Text>
												</TouchableOpacity>
												<TouchableOpacity onPress={() => this.accept(this.props.navigation.getParam('id_login'))} style={[ p.center, b.ml2, { width: xs(100), height: xs(40), borderWidth: 2, borderColor: blue }]}>
													<Text style={[ c.blue, c.light ]}> Terima </Text>
												</TouchableOpacity>
											</>
										)
									)
									:
									<TouchableOpacity onPress={() => this.unfriend(this.props.navigation.getParam('id_login'))} style={[ p.center, { width: xs(100), height: xs(40), borderWidth: 2, borderColor: blue }]}>
										<Text style={[ c.blue, c.light ]}> Berhenti Ikuti </Text>
									</TouchableOpacity>
								)
							}
						</View>
					</View>
				</View>

				<View style={{ width: '100%', height: wp('2%'), backgroundColor: '#121212' }} />

				<FlatList
                    horizontal={true}
                    data={this.state.tabs_title}
                    keyExtractor={item => item.image}
                    renderItem={({ item, index }) => (
                        (this.state.tabs_clicked === index ?
                            <TouchableHighlight onPress={() => this.switchTab(index)} style={{ width: wp('33.3%'), height: hp('10%'), alignItems: 'center', justifyContent: 'center', backgroundColor: blue }}>
                                <Image source={item.image} style={{ width: xs(25), height: xs(22), marginTop: wp('-6%') }} />
                            </TouchableHighlight>
                            :
                            <TouchableHighlight onPress={() => this.switchTab(index)} style={{ width: wp('33.3%'), height: hp('10%'), alignItems: 'center', justifyContent: 'center', backgroundColor: '#181B22' }}>
                                <Image source={item.image} style={{ width: xs(25), height: xs(22), marginTop: wp('-6%') }} />
                            </TouchableHighlight>
                        )
                    )}
                />

                {this.state.tabs_clicked === 0 && (
                    <>
                        <ActionButton buttonColor={blue} style={{ zIndex: 1 }} fixNativeFeedbackRadius={true}>
                            <ActionButton.Item buttonColor='#9b59b6' title="Tambah Postingan" onPress={() => this.props.navigation.navigate('AddComment')}>
                                <Icon name="md-create" size={20} color={light} />
                            </ActionButton.Item>
                        </ActionButton>
                        <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#121212' }}>
                            <View style={{ width: '100%', alignItems: 'center' }}>
                                <FlatList
                                    ListHeaderComponent={<View style={{ marginTop: wp('4%') }} />}
                                    data={this.state.social}
                                    keyExtractor={item => item.key}
                                    renderItem={({item}) => (
                                        <UserPost
											url_avatar={item.url_gambar}
											nama={item.nama}
											tanggal_post={this.hitung_hari(item.tanggal_user_post)}
											deskripsi={item.deskripsi_user_post}
											post_gambar={item.gambar_user_post}
											id={item.id_user_post}
											komentar={item.komentar_user_post}
											liked_id_login={item.liked_id_login}
											comment={() => this.props.navigation.push('UserPostKomentar', {id_user_post: item.id_user_post, id_login: item.id_login})}
										/>
                                    )}
                                />
                            </View>
                        </ScrollView>
                    </>
                )}

                {this.state.tabs_clicked === 1 && (
                    <>
                        <ActionButton buttonColor={blue} style={{ zIndex: 1 }} fixNativeFeedbackRadius={true}>
                            <ActionButton.Item buttonColor='#9b59b6' title="Buat Liga" onPress={() => this.props.navigation.navigate('BuatGrup')}>
                                <Icon name="md-create" size={20} color={light} />
                            </ActionButton.Item>
                        </ActionButton>
                        <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#121212' }}>
                            <View style={{ width: '100%' }}>
                                <View style={[ b.mt2, { width: '100%', backgroundColor: '#181B22' }]}>
                                    <Text style={[ c.light, b.ml4, b.mt3, { marginBottom: wp('1%') }]}>Liga Saya</Text>
                                    <View style={{ width: '100%', borderBottomWidth: 3, borderBottomColor: blue }} />
                                    <View style={[ b.pl2, b.pr2 ]}>
                                        {this.state.group2 === undefined ?
                                            <View style={[ p.center, b.mt1, b.mb2, { width: '100%' }]}>
                                                <View style={[ b.rounded, p.center, { width: xs(320), backgroundColor: '#000' }]}>
                                                    <Text style={[ b.mt1, b.mb1, c.light ]}>Anda belum Mengikuti Liga apapun</Text>
                                                </View>
                                            </View>
                                            :
                                            <ScrollView showsHorizontalScrollIndicator={false} horizontal={true} style={[ b.mt3, b.mb3 ]}>
                                                <FlatList
                                                    horizontal={true}
                                                    data={this.state.group2}
                                                    keyExtractor={item => item.id_group}
                                                    renderItem={({item}) => (
                                                        <TouchableOpacity onPress={() => this.props.navigation.push('Grup_Chall', {id_group: item.id_group})} style={[ b.ml2 ]}>
                                                            <View style={[ p.center, { width: xs(100) }]}>
                                                                <FastImage source={item.image} style={[ b.rounded, { width: xs(80), height: xs(80) }]} />
                                                                <Text style={[ p.textCenter, b.mt1, f._12, c.light ]} numberOfLines={2}>{item.nama_group}</Text>
                                                            </View>
                                                        </TouchableOpacity>
                                                    )}
                                                />
                                            </ScrollView>
                                        }
                                    </View>
                                </View>

                                <View style={[ b.mt2, { width: '100%', backgroundColor: '#181B22' }]}>
                                    <Text style={[ c.light, b.ml4, b.mt4, f._18, f.bold, { marginBottom: wp('1%') }]}>Semua Liga</Text>
                                    <View style={{ borderBottomWidth: 3, borderBottomColor: blue }} />
                                    {this.state.group1 === undefined ?
                                        <View style={[ p.center, b.mt1, b.mb2, { width: '100%' }]}>
                                            <View style={[ b.rounded, p.center, { width: xs(320), backgroundColor: '#555555' }]}>
                                                <Text style={[ b.mt1, b.mb1, c.light ]}>Tidak ada Liga yang Terserdia</Text>
                                            </View>
                                        </View>
                                        :
                                        <FlatList
                                            ListHeaderComponent={<View style={[ b.mt4 ]} />}
                                            data={this.state.group1}
                                            keyExtractor={item => item.id_group}
                                            renderItem={({item}) => (
                                                <>
                                                    <TouchableOpacity onPress={() => this.props.navigation.push('Grup_Chall', {id_group: item.id_group})} style={[ b.ml4 ]}>
                                                        <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center' }}>
                                                            <FastImage source={item.image} style={[ b.rounded, { width: wp('25%'), height: wp('25%') }]} />
                                                            <View style={[ b.ml4, { width: wp('55%') }]}>
                                                                <View style={{ width: xs(225) }}>
                                                                    <Text style={[ c.light ]} numberOfLines={1}>{item.nama_group}</Text>
                                                                </View>
                                                                <View style={[ c.light, p.row ]}>
                                                                    <Text style={[ c.light ]}>{item.anggota} Anggota</Text>
                                                                    <View style={[ b.bordered, b.rounded, b.ml1, b.mr1, { backgroundColor: '#DADADA', width: xs(7), height: xs(7), marginTop: vs(5) }]} />
                                                                    <View style={{ width: xs(160) }}>
                                                                        <Text style={[ c.light ]} numberOfLines={1}>{item.lokasi}</Text>
                                                                    </View>
                                                                </View>
                                                                <TouchableOpacity onPress={() => this.gabungGroup(item.id_group)} style={[ p.center, b.rounded, b.mt1, { borderWidth: 1, borderColor: '#2C84FF', width: xs(100) }]}>
                                                                    <Text style={[ b.mt1, b.mb1, { color: '#2C84FF' }]}>Gabung</Text>
                                                                </TouchableOpacity>
                                                            </View>
                                                        </View>
                                                    </TouchableOpacity>
                                                    <View style={[ b.mt2, b.mb2, { width: '100%', borderBottomWidth: 1, borderBottomColor: '#555555' }]} />
                                                </>
                                            )}
                                        />
                                    }
                                </View>
                            </View>
                        </ScrollView>
                    </>
                )}

                {this.state.tabs_clicked === 2 && (
                    <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#121212' }}>
						<FlatList
							ListHeaderComponent={<View style={{ marginTop: wp('4%') }} />}
							data={this.state.list_friends}
							keyExtractor={item => item.id}
							renderItem={({item}) => (
								<View style={{ width: '100%', alignItems: 'center' }}>
									<View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
										<TouchableOpacity onPress={() => this.props.navigation.push('ProfTracking')} style={{ width: wp('20%') }}>
											<FastImage source={item.image} style={{ width: wp('15%'), height: wp('15%') }} />
										</TouchableOpacity>
										<TouchableOpacity onPress={() => this.props.navigation.push('ProfTracking')} style={{ width: wp('50%') }}>
											<Text style={[ c.light, f.bold ]} numberOfLines={1}>{item.nama}</Text>
										</TouchableOpacity>
										<View style={{ width: wp('20%'), alignItems: 'flex-end' }}>
											<View style={{ width: wp('18%'), height: hp('5%'), backgroundColor: blue, borderRadius: wp('1%'), alignItems: 'center', justifyContent: 'center' }}>
												<Text style={[ c.light ]}>Tambah</Text>
											</View>
										</View>
									</View>
									<View style={{ width: '100%', borderBottomWidth: 1, borderBottomColor: blue, marginTop: wp('4%'), marginBottom: wp('4%') }} />
								</View>
							)}
						/>
                    </ScrollView>
                )}

				{/* <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#181B22' }}>
					<View style={[ b.mt1, { width: '100%', backgroundColor: '#000' }]}>
						<FlatList
							extraData={this.state}
							data={this.state.list_user_post}
							keyExtractor={item => item.id_login}
							renderItem={({item}) => (
								<UserPost
									url_avatar={item.url_gambar}
									nama={item.nama}
									tanggal_post={this.hitung_hari(item.tanggal_user_post)}
									deskripsi={item.deskripsi_user_post}
									post_gambar={item.gambar_user_post}
									id={item.id_user_post}
									komentar={item.komentar_user_post}
									liked_id_login={item.liked_id_login}
									comment={() => this.props.navigation.push('UserPostKomentar', {id_user_post: item.id_user_post, id_login: item.id_login})}
								/>
							)}
						/>
					</View>
				</ScrollView> */}
			</>
		)
	}
}

const styles = StyleSheet.create({
  // loading centered screenn
  container_loading: {
    flex: 1,
    position:'absolute',
    zIndex:1,
    width: '100%',
    height: '100%',
    justifyContent: "center",
    backgroundColor: 'rgba(0,0,0,0.7)'
  },
  horizontal_loading: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },
});