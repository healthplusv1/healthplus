import React from 'react'
import { Text, View, TouchableOpacity, Image, ImageBackground, ScrollView, Alert, FlatList, ActivityIndicator, StyleSheet } from 'react-native'
import { b, p, f, c } from '../../utils/StyleHelper'
import { vs, xs } from '../../utils/Responsive'
import { blue, light, grey, softBlue } from '../../utils/Color'
import Svg from '../../utils/Svg'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'
import {NavigationEvents} from 'react-navigation';
import FastImage from 'react-native-fast-image'
import { API_URL } from 'react-native-dotenv'

export default class ListLeagueTerdekat extends React.Component {

	constructor(props){
		super(props);
		this.state ={
			group: null,
			group1: null,
			group2: null,
			loading: true
		}
	}

	async componentDidMount() {
		const id_login = await AsyncStorage.getItem('@id_login')
		axios.get(API_URL+'/main/get_admin_group', {params:{
			id_login: id_login
		}})
		.then(result => {
			this.setState({
				loading: false,
				group: result.data.data
			})
			// alert(JSON.stringify(result))
		})
		.catch(e => {
			console.log('get_admin_group => '+e)
		})

		axios.get(API_URL+'/main/get_all_anggota_group', {params:{
			id_login: id_login
		}})
		.then(result => {
			this.setState({
				loading: false,
				group1: result.data.data
			})
			// alert(JSON.stringify(result.data.data))
		})
		.catch(e => {
			console.log('get_anggota_group => '+e)
		})
	}

	render() {
		if (this.state.loading) {
			return (
            <View style={[styles.container_loading, styles.horizontal_loading]}>
              <ActivityIndicator size="large" color="#0000ff" />
            </View>
            )
		}else{
			return (
			<>
				<NavigationEvents onDidFocus={() => this.componentDidMount()} />
				<View style={[ p.center, { width: '100%', height: xs(80), backgroundColor: blue }]}>
					<View style={[ p.row, { width: '100%' }]}>
						<View style={[ b.ml4, { width: xs(50) }]}>
							<TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>
								<Image source={require('../../asset/back.png')} style={{ width: xs(25), height: xs(25) }} />
							</TouchableOpacity>
						</View>
						<View style={{ marginLeft: vs(30), width: xs(200) }}>
							<Text style={[ f.bold, c.light, f._20 ]}>List Semua League</Text>
						</View>
					</View>
				</View>
				<ScrollView showsVerticalScrollIndicator={false}>
					<View style={{ width: '100%' }}>
						<View style={{ width: '100%', height: '100%' }}>
							<FlatList
			                  extraData={this.state}
			                  data={this.state.group1}
			                  keyExtractor={item => item.id_group}
			                  renderItem={({item}) => (
			                  	<>
				                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Grup', {id_group: item.id_group})} style={[ b.ml4, b.mt2 ]}>
				                      <View style={[ p.row, { width: '100%' }]}>
				                        <FastImage source={{ uri: API_URL+'/src/icon_group/'+item.url_gambar }} style={[ b.roundedLow, { width: xs(50), height: xs(50) }]} />
				                        <View>
					                        <Text style={[ b.ml4, b.mt1 ]}> {item.nama_group} </Text>
					                        <View style={[ p.row, b.ml4 ]}>
						                        <Text> {item.jml_anggota} Pelari </Text>
						                        <View style={[ b.bordered, b.rounded, { backgroundColor: '#DADADA', width: xs(7), height: xs(7), marginTop: vs(7) }]} />
						                        <Text> {item.lokasi} </Text>
					                        </View>
				                        </View>
				                      </View>
				                    </TouchableOpacity>
				                    <View style={[ b.mt2, { width: '100%', borderBottomWidth: 1, borderBottomColor: '#DADADA' }]} />
			                  	</>
			                  )}
			                />
						</View>
					</View>
				</ScrollView>
			</>)
		}
	}
}

const styles = StyleSheet.create({
  // loading centered screenn
  container_loading: {
    flex: 1,
    justifyContent: "center"
  },
  horizontal_loading: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },
});