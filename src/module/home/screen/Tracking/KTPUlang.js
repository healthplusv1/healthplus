import React, { Component } from 'react'
import { Text, View, TouchableOpacity, PermissionsAndroid, Image, Alert } from 'react-native'
import { blue, light } from '../../utils/Color'
import { xs, vs } from '../../utils/Responsive'
import { f, c, b, p } from '../../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import { launchCamera } from 'react-native-image-picker'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { API_URL } from 'react-native-dotenv'

export default class KTPUlang extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    launchCamera = async () => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.CAMERA
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log('You can use the camera')
                let options = {
                    storageOptions: {
                        skipBackup: true,
                        path: 'images',
                    },
                };
                launchCamera(options, (response) => {
                    // console.log('Response = ', response);
        
                    if (response.didCancel) {
                        console.log('User cancelled image picker');
                    } else if (response.error) {
                        console.log('ImagePicker Error: ', response.error);
                    } else if (response.customButton) {
                        console.log('User tapped custom button: ', response.customButton);
                        alert(response.customButton);
                    } else {
                        console.log('fileName: '+response.assets[0].fileName);
                        this.setState({
							uri: {uri: response.assets[0].uri},
                            url_gambar: response.assets[0].uri,
                            fileName: response.assets[0].fileName,
                            file_image: response
                        });
						this.props.navigation.push('ProsesVerifyKTPUlang')
                    }
                });
            } else {
                console.log('Camera permission denied')
            }
        } catch (error) {
            console.warn(error)
        }
    }

    // renderFileUri() {
	//     if (this.state.url_gambar) {
	//       return <Image
	//         source={{ uri: this.state.url_gambar }}
	//         style={styles.images}
	//       />
	//     } else {
	//       return <Image
	//         style={styles.images}
	//       />
	//     }
	// }

    createFormDataPhoto = (photo, id) => {
	    const data = new FormData();
	    data.append('file_image', {
	      name: (photo.fileName == null)? 'myphoto': photo.fileName,
	      type: photo.type,
	      uri: photo.uri
	    });

	    data.append("id_user", id);
	    data.append("judul", this.state.judul);
	    data.append("status", 'deactivated');
	    return data;
	  };

	async upload_funstation(){
		const id = await AsyncStorage.getItem('@id')

		// axios.get(API_URL+'/main/insert_funstation', {params:{
		// 	id_user: id,
		// 	judul: this.state.judul,
		// 	url_gambar: this.state.url_gambar,
		// 	status: 'deactivated',
		// }}).then(data => {
		// 	alert(JSON.stringify(data))
		// 	// if (data.data.status == 'true') {
		// 	// 	alert('Success')
		// 	// 	// this.props.navigation.navigate('Fun')
		// 	// } else {
		// 	// 	alert('Failed')
		// 	// }
		// }).catch(erro => {
		// 	// alert(JSON.stringify(erro))
		// 	alert('Error')
		// })

		if (this.state.judul === '' && this.state.file_image === '') {
			Alert.alert('Masukkan Judul dan Gambar untuk memposting!')
		} else {
			this.setState({
				isLoading: true
			})
		}

	    fetch(API_URL+'/main/insert_funstation', {
	      method: 'POST',
	      headers: {
	        'Content-Type': 'multipart/form-data'
	      },
	      body: this.createFormDataPhoto(this.state.file_image, id)
	    })
	      .then(response => response.json())
	      .then(response => {
	        // console.log(response);
	      this.setState({
	      	isLoading: false
	      })
	        // Alert.alert(
	        // 	"Pemberitahuan",
	        // 	"Postingan Anda telah dikirimkan, kami akan memeriksanya.",
	        // 	[
	        // 		{ text: 'OK', onPress: () => this.props.navigation.navigate('Fun') }
	        // 	]
	        // )
	      })
	      .catch(error => {
	        console.log('Upload Error', error);
	        // alert(JSON.stringify(error))
	      });
	}

    render() {
        return (
            <FastImage source={require('../../asset/back_sign.png')} style={{ width: '100%', height: '100%', alignItems: 'center' }}>
                <TouchableOpacity style={{ position: 'absolute', left: wp('4%'), top: wp('4%'), zIndex: 1 }}>
                    <View style={{ width: wp('10%'), height: wp('10%'), borderRadius: wp('10%'), backgroundColor: 'rgba(0,0,0,0.4)', alignItems: 'center', justifyContent: 'center' }}>
                        <FastImage source={require('../../asset/left.png')} style={{ width: vs(25), height: vs(25) }} />
                    </View>
                </TouchableOpacity>
                <View style={{ width: wp('80%'), marginTop: wp('45%') }}>
                    <Text style={[ f.bold, f._20, { textAlign: 'center' }]}>Lampirkan E-KTP</Text>
                    <Text style={{ marginTop: wp('4%'), textAlign: 'center' }}>Data kamu untuk proses verifikasi.</Text>
                    <Text style={{ textAlign: 'center' }}>Data akan tersimpan dan terlindungi.</Text>
                </View>

                <FastImage source={require('../../asset/ktp.png')} style={{ width: xs(200), height: xs(254), marginTop: wp('8%') }}></FastImage>

                <TouchableOpacity onPress={() => this.launchCamera()} style={{ width: wp('85%'), height: hp('7.5%'), backgroundColor: blue, borderRadius: wp('2%'), position: 'absolute', bottom: wp('6%'), alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={[ c.light, f.bold, f._18 ]}>Buka Kamera</Text>
                </TouchableOpacity>
            </FastImage>
        )
    }
}
