import React from 'react'
import { Text, View, BackHandler, TouchableOpacity, I18nManager } from 'react-native'
import { b, p, f, c } from '../../utils/StyleHelper'
import { vs, xs } from '../../utils/Responsive'
import { blue, light, grey, softBlue } from '../../utils/Color'
import WebView from 'react-native-webview'

export default class DailyHoroskope extends React.Component {

	constructor(props){
		super(props);
		this.state ={
			initiateAd: false
		}
	}

	render() {
		return (
		<>
			
			<WebView
		        source={{uri: this.props.navigation.getParam('url')}}
		        onMessage={event => {
					const { data } = event.nativeEvent;
					if (data == "showAd") {
						this.setState({ initiateAd: true })
					}
				}}
		    />
		</>)
	}
}