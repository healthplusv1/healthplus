import React from 'react'
import { Text, View, Image, ImageBackground, ScrollView, TouchableOpacity } from 'react-native'
import { b, p, f, c } from '../utils/StyleHelper';
import { vs, xs } from '../utils/Responsive';
import { blue, light, grey, softBlue } from '../utils/Color';

export default class Spreads extends React.Component {
	render() {
		return (
			<>
				<View style={{ width: '100%', height: xs(80), backgroundColor: blue }}>
					<View style={[ p.row, b.ml4, { marginTop: vs(22) }]}>
						<View>
							<TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>
								<Image source={require('../asset/back.png')} style={{ width: xs(25), height: xs(25) }}/>
							</TouchableOpacity>
						</View>
						<View style={[ b.ml4, { width: xs(260) }]}>
							<Text style={[ c.light, f.bold, f._20, b.ml1 ]}> Bagaimana ini menyebar? </Text>
						</View>
					</View>
				</View>
				<ScrollView showsVerticalScrollIndicator={false}>
					<View>
						<Image source={require('../asset/1678.png')} style={{ width: '100%', height: xs(200) }} />
					</View>
					<View style={[ p.center, b.mt4 ]}>
						<Text style={[ f.bold, f._20 ]}> Coronavirus: penyakitnya </Text>
						<Text style={[ f.bold, f._20 ]}> Covid-19 menjelaskan </Text>
					</View>
					<View style={[ p.center, b.mt2 ]}>
						<View style={{ width: xs(300) }}>
							<View style={[ p.row ]}>
								<Text style={[ f.bold ]}>
									Covid-19
								</Text>
								<Text style={[ b.ml1 ]}>
									wabah menyebabkan Cina untuk
								</Text>
							</View>
							<Text>
								menempatkan banyak negara terkunci tetapi virus telah menyebar dengan cepat
								mengelilingi dunia. Banyak negara mengkarantina
								pelancong dari luar negeri. Cari tahu bagaimana semuanya dimulai.
							</Text>
						</View>
					</View>
					<View style={[ p.center, b.mt2 ]}>
						<View style={{ width: xs(300) }}>
							<View style={[ p.row ]}>
								<Text>
									Penyakit saat itu-misteri,
								</Text>
								<Text style={[ f.bold, b.ml1 ]}>
									Covid-19 adalah yang
								</Text>
							</View>
							<View style={[ p.row ]}>
								<Text style={[ f.bold ]}>
									pertama dilaporkan di Wuhan Desember lalu,
								</Text>
							</View>
							<Text>
								tidak lama sebelum menjelang Tahun Baru bulan ketika Cina melakukan dunia
								migrasi terakhir terbesar. Wuhan adalah ibu kota Hubei
								provinsi dan merupakan pusat perjalanan utama yang menghubungkan beberapa
								kota terbesar di negara ini. Dengan populasi 11 juta,
								Kota Wuhan lebih besar dari London atau New York.
							</Text>
						</View>
					</View>
					<View style={[ p.center, b.mt2 ]}>
						<View style={{ width: xs(300) }}>
							<View style={[ p.row ]}>
								<Text>
									Virus ini biasanya ditularkan melalui
								</Text>
								<Text style={[ f.bold, b.ml1 ]}>
									droplet
								</Text>
							</View>
							<Text>
								saat bersin atau batuk. Sebagian besar tetesan adalah
								kurang dari 100 mikron (0,1mm). Masker bedah adalah
								dirancang untuk mencegah tetesan besar lewat dari satu
								mounth seseorang ke orang lain dari permukaan. N95
								respirator melindungi pemakainya dari menghirup partikel
								lebih besar dari 0,3 mikron dengan diameter. Saat dipasang dengan benar
								respirator menyaring 95 persen partikel di udara.
							</Text>
						</View>
					</View>
					<View style={[ p.center, b.mt2 ]}>
						<View style={{ width: xs(300) }}>
							<Text>
								WHO juga hanya menyarankan untuk menggunakan masker jika ada
								gejala pernapasan seperti batuk atau bersin, ringan
								gejala mirip coronavirus atau sedang merawat seseorang
								diduga memiliki infeksi virus corona.
							</Text>
						</View>
					</View>
					<View style={{ height: xs(30) }} />
				</ScrollView>
			</>
		)
	}
}