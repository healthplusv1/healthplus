import React from 'react'
import { Text, View, BackHandler, TouchableOpacity, I18nManager } from 'react-native'
import { b, p, f, c } from '../utils/StyleHelper'
import { vs, xs } from '../utils/Responsive'
import { blue, light, grey, softBlue } from '../utils/Color'
import WebView from 'react-native-webview'


export default class News extends React.Component {

	constructor(props) {
		super(props);
		this.state = {

		}
	}

	render() {
		return (
			<>
				<View style={[ p.center, { width: '100%', height: xs(50), backgroundColor: '#000' }]}>
					<View style={[ p.alignEnd, { width: '100%' }]}>
						<TouchableOpacity onPress={() => this.props.navigation.navigate('Home')} style={[ b.mr4 ]}>
							<Text style={[ f.bold, f._20, c.light ]}>Batal</Text>
						</TouchableOpacity>
					</View>
				</View>
				<WebView
			        source={{uri: this.props.navigation.getParam('url_link')}}
			    />
			</>
		)
	}
}