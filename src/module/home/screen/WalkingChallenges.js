import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity, ScrollView, StyleSheet, ActivityIndicator, FlatList, Dimensions } from 'react-native'
import { blue, light } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import Icon from 'react-native-vector-icons/Ionicons'
import DashedLine from 'react-native-dashed-line'
import { BarChart } from 'react-native-chart-kit'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'
import { API_URL } from 'react-native-dotenv'

export default class WalkingChallenges extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            count: '',
            walking_challenges: [
                {jenis: 'Jalan'},
                {jenis: 'Naik Tangga'},
            ],
            daily_challenges: [
                {date: 'Hari'},
                {date: 'Minggu'},
                {date: 'Bulan'},
            ],
            clicked: 0,
            detail_walking: null,
            clicked_one: 0,
            daily_detail_walking: null
        }
    }

    async componentDidMount() {
        this.setState({ loading: true })

        axios.get(API_URL+'/main/shopping/cart', {params: {
            id_login: '118205008143923829902'
        }})
        .then(res => {
            this.setState({
                loading: false,
                count: res.data.data.length,
            })
        })
        .catch(e => {
            this.setState({ loading: false })
            console.log(e)
        })
    }

    async swicth(jenis, index) {
        await this.setState({
            loading: false,
            clicked: index,
            detail_walking: jenis
        })
        // alert(JSON.stringify(this.state.clicked))
		// const id_login = await AsyncStorage.getItem('@id_login')

		// axios.get(API_URL+'/main/filter_kompetisi', {params: {
		// 	id_login: id_login,
		// 	jenis_challenge: jenis_challenge
		// }})
		// .then(resp => {
		// })
	}

    async switchDay(week, index) {
        await this.setState({
            loading: false,
            clicked_one: index,
            daily_detail_walking: week
        })
    }

    render() {
        return (
            <>
                {this.state.loading ?
                    <View style={[ styles.horizontal_loading, styles.container_loading ]}>
                        <ActivityIndicator size='large' color={blue} />
                    </View>
                    :
                    null
                }
                <View style={{ width: '100%', height: '100%', backgroundColor: '#121212' }}>
                    <View style={{ width: '100%', alignItems: 'center' }}>
                        <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('4%'), marginBottom: wp('4%') }}>
                            <View style={{ width: wp('45%') }}>
                                <Image source={require('../asset/healthPlus.png')} style={{ width: xs(160), height: xs(38) }} />
                            </View>
                            <View style={{ width: wp('45%'), flexDirection: 'row-reverse' }}>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('Drawer')}>
                                    <Icon name="ios-menu" size={25} color={light} />
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('Notifikasi')} style={{ marginRight: wp('2%') }}>
                                    <Icon name="ios-notifications" size={25} color={light} />
                                    {this.state.notif === 'true' ?
                                        <View style={{ width: xs(10), height: xs(10), backgroundColor: 'orangered', borderRadius: xs(5),
                                        position: 'absolute', marginLeft: vs(6), marginTop: vs(1) }} />
                                        :
                                        null
                                    }
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.props.navigation.push('Cart')} style={{ marginRight: wp('2%') }}>
                                    {this.state.count !== 0 ?
                                        <View style={{ width: wp('3.5%'), height: wp('3.5%'), backgroundColor: 'orange', position: 'absolute', borderRadius: wp('3.5%'), zIndex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                            <Text style={[ c.light, f._8 ]}>{this.state.count}</Text>
                                        </View>
                                        :
                                        null
                                    }
                                    <Image source={require('../asset/addCart.png')} style={{ width: wp('7%'), height: wp('7%'), tintColor: light }} />
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.props.navigation.push('FrequentlyOrder')} style={{ marginRight: wp('2%') }}>
                                    <Image source={require('../asset/Frequentlyorder_icon.png')} style={{ width: wp('8%'), height: wp('7%'), tintColor: light }} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>

                    <View style={{ width: '100%', alignItems: 'center' }}>
                        <View style={{ marginLeft: wp('5%'), height: hp('5%') }}>
                            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                                <FlatList
                                    horizontal={true}
                                    data={this.state.walking_challenges}
                                    keyExtractor={item => item.jenis}
                                    renderItem={({ item, index }) => (
                                        (this.state.clicked === index ?
                                            <TouchableOpacity onPress={() => this.swicth(item.jenis, index)} style={{ width: wp('25%'), backgroundColor: blue, borderRadius: wp('2%'), marginRight: wp('5%'), alignItems: 'center', justifyContent: 'center' }}>
                                                <Text style={[ c.light ]}>{item.jenis}</Text>
                                            </TouchableOpacity>
                                            :
                                            <TouchableOpacity onPress={() => this.swicth(item.jenis, index)} style={{ width: wp('25%'), borderWidth: 1, borderColor: blue, borderRadius: wp('2%'), marginRight: wp('5%'), alignItems: 'center', justifyContent: 'center' }}>
                                                <Text style={[ c.light ]}>{item.jenis}</Text>
                                            </TouchableOpacity>
                                        )
                                    )}
                                />
                            </ScrollView>
                        </View>
                    </View>

                    <ScrollView style={{ backgroundColor: '#121212' }}>
                        <>
                            {this.state.clicked === 0 && (
                                <>
                                    <View style={{ width: '100%', alignItems: 'center', marginTop: wp('4%'), backgroundColor: '#181B22' }}>
                                        <View style={{ marginTop: wp('4%'), height: hp('6%') }}>
                                            <FlatList
                                                horizontal={true}
                                                data={this.state.daily_challenges}
                                                keyExtractor={item => item.date}
                                                renderItem={({ item, index }) => (
                                                    (this.state.clicked_one === index ?
                                                        <TouchableOpacity onPress={() => this.switchDay(item.date, index)} style={{ width: wp('20%'), height: '100%', backgroundColor: blue, borderRadius: wp('2%'), alignItems: 'center', justifyContent: 'center', marginLeft: wp('2%') }}>
                                                            <Text style={[ c.light ]}>{item.date}</Text>
                                                        </TouchableOpacity>
                                                        :
                                                        <TouchableOpacity onPress={() => this.switchDay(item.date, index)} style={{ width: wp('20%'), height: '100%', borderWidth: 1, borderColor: blue, borderRadius: wp('2%'), alignItems: 'center', justifyContent: 'center', marginLeft: wp('2%') }}>
                                                            <Text style={[ c.light ]}>{item.date}</Text>
                                                        </TouchableOpacity>
                                                    )
                                                )}
                                            />
                                        </View>
                                        <View style={{ marginTop: wp('6%'), width: wp('30%'), backgroundColor: light, borderRadius: wp('3%'), alignItems: 'center', justifyContent: 'center' }}>
                                            <Text style={{ marginTop: wp('1%') }}>Total Langkah</Text>
                                            <Text style={{ color: '#00CCFF', marginBottom: wp('1%') }}>2000</Text>
                                        </View>
                                        <View style={{ width: wp('90%'), height: hp('40%'), marginTop: wp('4%') }}>
                                            <View style={{ width: wp('80%'), marginTop: wp('6%') }}>
                                                <BarChart
                                                    data={{
                                                        labels: ['10', '11', '12', '13'],
                                                        datasets: [
                                                            {
                                                                data: [
                                                                    Math.random(30) * 100,
                                                                    Math.random(40) * 100,
                                                                    Math.random(50) * 100,
                                                                    Math.random(60) * 100
                                                                ]
                                                            }
                                                        ]
                                                    }}
                                                    width={Dimensions.get('window').width - 35}
                                                    height={220}
                                                    yAxisLabel={"Rp"}
                                                    chartConfig={{
                                                        backgroundColor: '#181B22',
                                                        backgroundGradientFrom: '#181B22',
                                                        backgroundGradientTo: '#181B22',
                                                        decimalPlaces: 2,
                                                        color: (opacity = 1) => `white`,
                                                        labelColor: (opacity = 1) => `white`,
                                                        barPercentage: 1
                                                    }}
                                                    style={{
                                                        marginVertical: 8,
                                                        borderRadius: wp('2%')
                                                    }}
                                                />
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ width: '100%', alignItems: 'center', marginTop: wp('4%'), marginBottom: wp('6%'), backgroundColor: '#121212' }}>
                                        <View style={{ width: wp('50%'), height: wp('50%'), borderRadius: wp('50%'), borderWidth: 15, borderColor: '#00CCFF', marginTop: wp('4%'), alignItems: 'center', justifyContent: 'center' }}>
                                            <Image source={require('../asset/walking.png')} style={{ width: wp('15%'), height: wp('15%'), tintColor: light }} />
                                            <Text style={[ c.light, f.bold, f._18, { marginTop: wp('1%') }]}>2000</Text>
                                            <Text style={[ c.light ]}>Langkah</Text>
                                        </View>
                                        <View style={{ width: wp('50%'), marginTop: wp('4%'), flexDirection: 'row', alignItems: 'center' }}>
                                            <View style={{ width: wp('25%') }}>
                                                <View style={{ width: wp('20%'), alignItems: 'center' }}>
                                                    <View style={{ width: '100%', height: wp('20%'), borderRadius: wp('20%'), borderWidth: 8, borderColor: 'darkorange', alignItems: 'center', justifyContent: 'center' }}>
                                                        <Image source={require('../asset/marker.png')} style={{ width: wp('10%'), height: wp('10%'), tintColor: 'darkorange' }} />
                                                    </View>
                                                    <Text style={{ color: 'darkorange', marginTop: wp('1%') }}>2 km</Text>
                                                </View>
                                            </View>
                                            <View style={{ width: wp('25%'), alignItems: 'flex-end' }}>
                                                <View style={{ width: wp('20%'), alignItems: 'center' }}>
                                                    <View style={{ width: '100%', height: wp('20%'), borderRadius: wp('20%'), borderWidth: 8, borderColor: '#00CCFF', alignItems: 'center', justifyContent: 'center' }}>
                                                        <Image source={require('../asset/point1_icon.png')} style={{ width: wp('10%'), height: wp('10%'), tintColor: '#00CCFF' }} />
                                                    </View>
                                                    <Text style={{ color: '#00CCFF', marginTop: wp('1%') }}>2000 poin</Text>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                </>
                            )}

                            {this.state.clicked === 1 && (
                                <>
                                    <View style={{ width: '100%', alignItems: 'center', marginTop: wp('4%'), backgroundColor: '#181B22' }}>
                                        <View style={{ marginTop: wp('4%'), height: hp('6%') }}>
                                            <FlatList
                                                horizontal={true}
                                                data={this.state.daily_challenges}
                                                keyExtractor={item => item.date}
                                                renderItem={({ item, index }) => (
                                                    (this.state.clicked_one === index ?
                                                        <TouchableOpacity onPress={() => this.switchDay(item.date, index)} style={{ width: wp('20%'), height: '100%', backgroundColor: blue, borderRadius: wp('2%'), alignItems: 'center', justifyContent: 'center', marginLeft: wp('2%') }}>
                                                            <Text style={[ c.light ]}>{item.date}</Text>
                                                        </TouchableOpacity>
                                                        :
                                                        <TouchableOpacity onPress={() => this.switchDay(item.date, index)} style={{ width: wp('20%'), height: '100%', borderWidth: 1, borderColor: blue, borderRadius: wp('2%'), alignItems: 'center', justifyContent: 'center', marginLeft: wp('2%') }}>
                                                            <Text style={[ c.light ]}>{item.date}</Text>
                                                        </TouchableOpacity>
                                                    )
                                                )}
                                            />
                                        </View>
                                        <View style={{ marginTop: wp('6%'), width: wp('30%'), backgroundColor: light, borderRadius: wp('3%'), alignItems: 'center', justifyContent: 'center' }}>
                                            <Text style={{ marginTop: wp('1%') }}>Total Langkah</Text>
                                            <Text style={{ color: '#00CCFF', marginBottom: wp('1%') }}>500</Text>
                                        </View>
                                        <View style={{ width: wp('90%'), height: hp('40%'), marginTop: wp('4%') }}>
                                            <View style={{ width: wp('80%'), marginTop: wp('6%') }}>
                                                <BarChart
                                                    data={{
                                                        labels: ['10', '11', '12', '13'],
                                                        datasets: [
                                                            {
                                                                data: [
                                                                    Math.random(30) * 100,
                                                                    Math.random(40) * 100,
                                                                    Math.random(50) * 100,
                                                                    Math.random(60) * 100
                                                                ]
                                                            }
                                                        ]
                                                    }}
                                                    width={Dimensions.get('window').width - 35}
                                                    height={220}
                                                    yAxisLabel={"Rp"}
                                                    chartConfig={{
                                                        backgroundColor: '#181B22',
                                                        backgroundGradientFrom: '#181B22',
                                                        backgroundGradientTo: '#181B22',
                                                        decimalPlaces: 2,
                                                        color: (opacity = 1) => `white`,
                                                        labelColor: (opacity = 1) => `white`,
                                                        barPercentage: 1
                                                    }}
                                                    style={{
                                                        marginVertical: 8,
                                                        borderRadius: wp('2%')
                                                    }}
                                                />
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ width: '100%', alignItems: 'center', marginTop: wp('4%'), marginBottom: wp('6%'), backgroundColor: '#181B22' }}>
                                        <View style={{ width: wp('50%'), height: wp('50%'), borderRadius: wp('50%'), borderWidth: 15, borderColor: '#00CCFF', marginTop: wp('4%'), alignItems: 'center', justifyContent: 'center' }}>
                                            <Image source={require('../asset/staircase.png')} style={{ width: wp('15%'), height: wp('15%'), tintColor: light }} />
                                            <Text style={[ c.light, f.bold, f._18, { marginTop: wp('1%') }]}>500</Text>
                                            <Text style={[ c.light ]}>Langkah</Text>
                                        </View>
                                        <View style={{ width: wp('50%'), marginTop: wp('4%'), flexDirection: 'row', alignItems: 'center' }}>
                                            <View style={{ width: wp('25%') }}>
                                                <View style={{ width: wp('20%'), alignItems: 'center' }}>
                                                    <View style={{ width: '100%', height: wp('20%'), borderRadius: wp('20%'), borderWidth: 8, borderColor: 'darkorange', alignItems: 'center', justifyContent: 'center' }}>
                                                        <Image source={require('../asset/marker.png')} style={{ width: wp('10%'), height: wp('10%'), tintColor: 'darkorange' }} />
                                                    </View>
                                                    <Text style={{ color: 'darkorange', marginTop: wp('1%') }}>2 km</Text>
                                                </View>
                                            </View>
                                            <View style={{ width: wp('25%'), alignItems: 'flex-end' }}>
                                                <View style={{ width: wp('20%'), alignItems: 'center' }}>
                                                    <View style={{ width: '100%', height: wp('20%'), borderRadius: wp('20%'), borderWidth: 8, borderColor: '#00CCFF', alignItems: 'center', justifyContent: 'center' }}>
                                                        <Image source={require('../asset/point1_icon.png')} style={{ width: wp('10%'), height: wp('10%'), tintColor: '#00CCFF' }} />
                                                    </View>
                                                    <Text style={{ color: '#00CCFF', marginTop: wp('1%') }}>500 poin</Text>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                </>
                            )}
                        </>
                    </ScrollView>
                </View>
            </>
        )
    }
}

const styles = StyleSheet.create({
    container_loading: {
        flex: 1,
        position:'absolute',
        zIndex:2,
        width: '100%',
        height: '100%',
        justifyContent: "center",
        backgroundColor: 'rgba(0,0,0,0.7)'
    },

    horizontal_loading: {
        flexDirection: "row",
        justifyContent: "space-around",
        padding: 10
    }
})