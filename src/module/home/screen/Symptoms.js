import React from 'react'
import { Text, View, Image, ImageBackground, ScrollView, TouchableOpacity } from 'react-native'
import { b, p, f, c } from '../utils/StyleHelper';
import { vs, xs } from '../utils/Responsive';
import { blue, light, grey, softBlue } from '../utils/Color';

export default class Symptoms extends React.Component {
	render() {
		return (
			<>
				<View style={{ width: '100%', height: xs(80), backgroundColor: blue }}>
					<View style={[ p.row, b.ml4, { marginTop: vs(20) }]}>
						<View>
							<TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>
								<Image source={require('../asset/back.png')} style={{ width: xs(25), height: xs(25) }}/>
							</TouchableOpacity>
						</View>
						<View style={[ b.ml4, p.center, { width: xs(230) }]}>
							<Text style={[ c.light, f.bold, f._20 ]}> Gejala </Text>
						</View>
					</View>
				</View>
				<ScrollView showsVerticalScrollIndicator={false}>
					<View style={[ p.center, b.mt4 ]}>
						<View style={[ p.row, { width: xs(300) }]}>
							<Text style={[ f.bold ]}>
								Virus COVID-19
							</Text>
							<Text style={[ b.ml1 ]}>
								mempengaruhi orang yang
							</Text>
						</View>
					</View>
					<View style={[ p.center ]}>
						<View style={{ width: xs(300) }}>
							<Text>
								berbeda dengan berbagai cara. COVID-19 adalah penyakit pernapasan pada sebagian besar yang terinfeksi
								orang akan mengalami gejala ringan hingga sedang dan
								sembuh tanpa memerlukan perawatan khusus. Orang yang
								memiliki kondisi medis yang mendasarinya dan mereka yang berusia di atas 60 tahun
								tahun memiliki risiko lebih tinggi terkena penyakit parah dan kematian.
							</Text>
						</View>
					</View>
					<View style={[ p.center, b.mt2 ]}>
						<View style={{ width: xs(300) }}>
							<Text style={[ f.bold ]}>
								Gejala umum meliputi:
							</Text>
							<Image source={require('../asset/group_484.png')} style={[ b.mt1, { width:'100%', height: xs(400) }]} />
						</View>
					</View>
					<View style={[ p.center, b.mt1 ]}>
						<View style={{ width: xs(300) }}>
							<Text>
								Orang dengan gejala ringan yang dinyatakan sehat harus mengisolasi diri
								dan menghubungi penyedia medis mereka atau saluran informasi COVID-19
								untuk nasihat tentang pengujian dan rujukan.
							</Text>
						</View>
					</View>
					<View style={[ p.center, b.mt1 ]}>
						<View style={{ width: xs(300) }}>
							<Text>
								Penderita demam, batuk sulit bernapas harus menghubungi dokter dan mencari pertolongan medis.
							</Text>
						</View>
					</View>
					<View style={{ height: xs(20) }} />
				</ScrollView>
			</>
		)
	}
}