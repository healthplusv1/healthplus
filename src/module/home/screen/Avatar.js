import React from 'react'
import { Text, View, Image, TouchableOpacity, ScrollView, FlatList, ActivityIndicator, StyleSheet } from 'react-native'
import { b, p, f, c } from '../utils/StyleHelper';
import { vs, xs } from '../utils/Responsive';
import { blue, light, grey, softBlue } from '../utils/Color';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage'
import FastImage from 'react-native-fast-image'
import { API_URL } from 'react-native-dotenv'

export default class Avatar extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			selected: 0,
			data: null,
			loading: true,
			url_gambar: null
		}
	}

	select(on) {
		this.setState({
			selected: on
		})
	}

	componentDidMount(){
		axios.get(API_URL+'/main/get_all_avatar')
		.then(resp => {
			this.setState({
				data: resp.data.data
			})
			for (var i = 0; i < resp.data.data.length; i++) {
				let update = {};
				update[resp.data.data[i].url_gambar.toString()] = false;
				this.setState(update)
			}
			setTimeout(() => {this.setState({loading: false})}, 2000)
		})
		.catch(erro => {
			this.setState({
				loading: false
			})
			console.log(erro)
		})
	}

	selectAvatar(avatar_gambar, url_gambarr){
		this.setState({
			url_gambar: url_gambarr
		})

		axios.get(API_URL+'/main/get_all_avatar')
		.then(resp => {
			this.setState({
				data: resp.data.data
			})
			for (let i = 0; i < resp.data.data.length; i++) {
				let update = {};
				update[resp.data.data[i].url_gambar.toString()] = false;
				this.setState(update)
			}
		})
		.then(() => {

			const update = {};
			update[avatar_gambar.toString()] = true;
			this.setState(update)
		})
		.catch(erro => {
			this.setState({
				loading: false
			})
			console.log(erro)
		})


		// alert(JSON.stringify(this.state))
	}

	async postAvatar(){
		const id_login = await AsyncStorage.getItem('@id_login')
		if (this.state.url_gambar === null) {
			// code...
		}else{
			axios.get(API_URL+'/main/update_avatar_by_id_login', {params:{
				id_login: id_login,
				url_gambar: this.state.url_gambar
			}})
			.then(resp => {
				this.props.navigation.navigate('ProfTracking')
			})
			.catch(erro => {
				console.log(erro)
			})
		}
	}

	render() {
		return (
			<>
				{this.state.loading ?
					<View style={[styles.container_loading, styles.horizontal_loading]}>
						<ActivityIndicator size="large" color="#0000ff" />
					</View>
					:
					null
				}
				<View style={[ p.center, { width: '100%', height: '100%', backgroundColor: '#000' }]}>
					<View style={[ b.mt2, p.center, { width: xs(300), height: xs(400), borderRadius: 50, backgroundColor: '#181B22' }]}>
						<Text style={[ b.mt2, f.bold, f._20, c.light ]}> Pilih Avatar </Text>
						<ScrollView>
							<FlatList
							extraData={this.state}
							data={this.state.data}
							keyExtractor={item => item.id_avatar}
							numColumns={3}
							renderItem={ ({item, index}) => {
								const abc = item.url_gambar.toString()
								return (
								<View style={[ b.mt2 ]}>
									<TouchableOpacity onPress={() => this.selectAvatar(item.url_gambar, item.url_gambar)} style={[ p.center ]}>
										<FastImage source={{ uri: API_URL+'/src/avatar/'+item.url_gambar }} style={{ width: xs(80), height: xs(80) }} />
										{this.state[abc] === true ?
										<FastImage source={require('../asset/checklist.png')} style={[ b.mt1, { width: xs(15), height: xs(15) }]} />
										: null }
									</TouchableOpacity>
								</View>
								)}}
							/>
						</ScrollView>
						<TouchableOpacity onPress={() => this.postAvatar()} style={[ b.rounded, b.mt4, p.center, { backgroundColor: blue, width: xs(80), height: xs(30) }]}>
							<Text style={[ c.light, f.bold, f._18 ]}> Pilih </Text>
						</TouchableOpacity>
						<View style={{ height: xs(20) }} />
					</View>
				</View>
			</>
		)
	}
}
const styles = StyleSheet.create({
  // loading centered screenn
  container_loading: {
	flex: 1,
    position:'absolute',
    zIndex:1,
    width: '100%',
    height: '100%',
    justifyContent: "center",
    backgroundColor: 'rgba(0,0,0,0.7)'
  },
  horizontal_loading: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },
});