import React from 'react'
import { Text, View, TouchableOpacity, Image, ImageBackground, ScrollView, Clipboard, Alert } from 'react-native'
import { b, p, f, c } from '../utils/StyleHelper'
import { vs, xs } from '../utils/Responsive'
import { blue, light, grey, softBlue } from '../utils/Color'
import Share from 'react-native-share';

const url = 'https://play.google.com/store/apps/details?id=com.healthplus';
const title = '';
const message = 'Please check this out.';
const icon = 'data:<data_type>/<file_extension>;base64,<base64_data>';
const options = Platform.select({
  ios: {
    activityItemSources: [
      { // For sharing url with custom title.
        placeholderItem: { type: 'url', content: url },
        item: {
          default: { type: 'url', content: url },
        },
        subject: {
          default: title,
        },
        linkMetadata: { originalUrl: url, url, title },
      },
      { // For sharing text.
        placeholderItem: { type: 'text', content: message },
        item: {
          default: { type: 'text', content: message },
          message: null, // Specify no text to share via Messages app.
        },
        linkMetadata: { // For showing app icon on share preview.
           title: message
        },
      },
      { // For using custom icon instead of default text icon at share preview when sharing with message.
        placeholderItem: {
          type: 'url',
          content: icon
        },
        item: {
          default: {
            type: 'text',
            content: `${message} ${url}`
          },
        },
        linkMetadata: {
           title: message,
           icon: icon
        }
      },
    ],
  },
  default: {
    title,
    subject: title,
    message: `${message} ${url}`,
  },
});

export default class Web extends React.Component {

	constructor(props){
		super(props);
		this.state ={
			kode: 'KSF152F'
		}
	}

	render() {
		return (
			<ScrollView>
				<View style={{ width: '100%', height: xs(100), backgroundColor: blue }}>
					<View style={[ p.row ]}>
						<TouchableOpacity onPress={() => this.props.navigation.navigate('Home')} style={[b.ml4, { marginTop: vs(30), width: xs(50), height: xs(50) }]}>
							<Image source={require('../asset/back.png')} style={{ width: xs(25), height: xs(25) }}/>
						</TouchableOpacity>
						<Text style={[ c.light, f.bold, f._20, { marginTop: vs(30), marginLeft: vs(40) }]}> Undang Teman </Text>
					</View>
				</View>
				<View style={[ p.center, { backgroundColor: 'whitesmoke' }]}>
					<View style={[ b.mt4, p.center, { width: '100%' }]}>
						<Text style={[ f.bold, { fontSize: 30, color: '#893F00' }]}> Dapatkan </Text>
						<Text style={[ f.bold, { fontSize: 30, color: '#893F00' }]}> 5 Poin HealthPlus+ </Text>
					</View>
					<View style={[ b.mt4, p.center, { width: '100%' }]}>
						<Text> Kamu dan temanmu akan mendapatkan </Text>
						<Text> poin HealthPlus+ setelah mereka </Text>
						<Text> menggunakan kode referralmu! </Text>
					</View>
					<Image source={require('../asset/coin.png')} style={{ width: xs(300), height: xs(300) }} />
					<View style={[ b.mt4, b.shadowHigh, p.center, { backgroundColor: '#FFFFFF', width: xs(300), height: xs(200) }]}>
						<ImageBackground source={require('../asset/subtraction_6.png')} style={[ p.center, { width: xs(250), height: xs(100) }]}>
							<View style={[ p.row, { width: xs(200), height: xs(60) }]}>
								<Text style={[ c.light, f.bold, f._20, b.mt3, { marginLeft: vs(40) }]}> {this.state.kode} </Text>
								<TouchableOpacity
									onPress={() => { 
										Clipboard.setString(this.state.kode)
										Alert.alert('', 'Tersalin!') 
									}}
									style={[ b.ml3, b.mt2 ]}
								>
									<Image source={require('../asset/group_505.png')} style={{ width: xs(40), height: xs(40) }} />
								</TouchableOpacity>
							</View>
						</ImageBackground>
						<TouchableOpacity onPress={() => Share.open(options)} style={[ b.mt4 ]}>
							<Text style={[ f._18, f.bold, { color: '#893F00' }]}> Share Sekarang </Text>
						</TouchableOpacity>
					</View>
					<View style={[ b.mt4 ]} />
				</View>
			</ScrollView>
		)
	}
}