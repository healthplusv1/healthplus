import React from 'react'
import { Text, View, Image, ImageBackground, ScrollView, TouchableOpacity, I18nManager } from 'react-native'
import { b, p, f, c } from '../utils/StyleHelper';
import { vs, xs } from '../utils/Responsive';
import { blueLight, blue, light, grey, softBlue } from '../utils/Color';

export default class Mask extends React.Component {

	constructor(props) {
		super(props);
		
	}

	render() {
		return (
			<>
				<View style={{ width: '100%', height: xs(80), backgroundColor: blue }}>
					<View style={[ p.row, b.ml4 ]}>
						<View style={{ marginTop: vs(20) }}>
							<TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>
								<Image source={require('../asset/back.png')} style={{ width: xs(25), height: xs(25) }}/>
							</TouchableOpacity>
						</View>
						<View style={[ b.mt2, b.ml2 ]}>
							<Text style={[ c.light, f.bold, f._20 ]}>Bagaimana cara menggunakan Masker?</Text>
						</View>
					</View>
				</View>
				<ScrollView showsVerticalScrollIndicator={false}>
					<View style={[ p.row, { backgroundColor: blueLight }]}>
						<ImageBackground source={require('../asset/path_3138.png')} style={[ p.center, { width: xs(75), height: xs(150) }]}>
							<Text style={[ f.bold, { fontSize: 50 }]}> 1 </Text>
						</ImageBackground>
						<Image source={require('../asset/Group440.png')} style={[ b.mt4, { width: xs(100), height: xs(100) }]} />
						<View style={{ width: xs(180) }}>
							<Text style={{ marginTop: vs(30), marginLeft: vs(20) }}>
								Sebelum mengenakan masker, cuci tangan dengan sabun atau air berbasis alkohol.
							</Text>
						</View>
					</View>
					<View>
			          <View style={{ borderBottomWidth: 5, borderBottomColor: '#C0C0C0', width: '100%' }} />
			        </View>

			        <View style={[ p.row ]}>
						<ImageBackground source={require('../asset/path_3137.png')} style={[ p.center, { width: xs(75), height: xs(210) }]}>
							<Text style={[ f.bold, { fontSize: 50 }]}> 2 </Text>
						</ImageBackground>
						<Image source={require('../asset/Group460.png')} style={[ b.ml4, { marginTop: vs(50), width: xs(65), height: xs(100) }]} />
						<View style={{ width: xs(180) }}>
							<Text style={{ marginLeft: vs(30) }}>
								Tutupi mulut dan hidung dengan masker dan pastikan tidak ada celah antara wajah dengan masker Anda.
							</Text>
							<Text style={[ b.mt1, { marginLeft: vs(30) }]}>
								Hindari menyentuh masker saat menggunakannya; jika Anda melakukannya, bersihkan tangan Anda dengan alkohol atau sabun dan air.
							</Text>
						</View>
					</View>
					<View>
			          <View style={{ borderBottomWidth: 5, borderBottomColor: '#C0C0C0', width: '100%' }} />
			        </View>

			        <View style={[ p.row, { backgroundColor: blueLight }]}>
						<ImageBackground source={require('../asset/path_3138.png')} style={[ p.center, { width: xs(75), height: xs(150) }]}>
							<Text style={[ f.bold, { fontSize: 50 }]}> 3 </Text>
						</ImageBackground>
						<Image source={require('../asset/Group462.png')} style={[ b.mt4, b.ml4, { width: xs(65), height: xs(100) }]} />
						<View style={{ width: xs(180) }}>
							<Text style={{ marginTop: vs(30), marginLeft: vs(30) }}>
								Ganti masker dengan yang baru, segera setelah lembab dan jangan gunakan kembali masker sekali pakai.
							</Text>
						</View>
					</View>
					<View>
			          <View style={{ borderBottomWidth: 5, borderBottomColor: '#C0C0C0', width: '100%' }} />
			        </View>

			        <View style={[ p.row ]}>
						<ImageBackground source={require('../asset/path_3137.png')} style={[ p.center, { width: xs(75), height: xs(150) }]}>
							<Text style={[ f.bold, { fontSize: 50 }]}> 4 </Text>
						</ImageBackground>
						<Image source={require('../asset/Group487.png')} style={[ b.mt4, { width: xs(100), height: xs(100) }]} />
						<View style={{ width: xs(180) }}>
							<Text style={[ b.mt4, b.ml3 ]}>
								Untuk membuang masker: lepaskan dari belakang (jangan menyentuh bagian depan masker); segera buang di tempat sampah; Cuci tangan dengan alkohol atau sabun dan air.
							</Text>
						</View>
					</View>
					<View>
			          <View style={{ borderBottomWidth: 5, borderBottomColor: '#C0C0C0', width: '100%' }} />
			        </View>
				</ScrollView>
			</>
		)
	}
}