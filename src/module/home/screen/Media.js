import React from 'react'
import { Text, View, Image, ImageBackground, ScrollView, TouchableOpacity } from 'react-native'
import { b, p, f, c } from '../utils/StyleHelper';
import { vs, xs } from '../utils/Responsive';
import { blue, light, grey, softBlue } from '../utils/Color';

export default class Media extends React.Component {
	render() {
		return (
			<View>
				<View style={{ width: '100%', height: xs(80), backgroundColor: blue }}>
					<View style={[ p.row, b.ml4, { marginTop: vs(20) }]}>
						<View>
							<TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>
								<Image source={require('../asset/back.png')} style={{ width: xs(25), height: xs(25) }}/>
							</TouchableOpacity>
						</View>
						<View style={[ b.ml4, p.center, { width: xs(230) }]}>
							<Text style={[ c.light, f.bold, f._20 ]}> Sumber daya media </Text>
						</View>
					</View>
				</View>
				<ScrollView showsVerticalScrollIndicator={false}>
					<View style={[ p.center, b.mt2 ]}>
			            <View style={{ borderBottomWidth: 2, borderBottomColor: '#C0C0C0', width: xs(325) }} />
			        </View>
		        	<View style={[ b.container, p.center, b.mt4 ]}>
		            	<TouchableOpacity onPress={() => this.props.navigation.navigate('FirstUN')} style={[ b.rounded, { borderWidth: 1, width: xs(330) }]}>
		            		<Text style={[ f.bold, b.ml3 ]}>
		            			Penerbangan solidaritas pertama PBB berangkat dari Addis Ababa dengan membawa vital COVID-19
								pasokan medis ke semua negara Afrika
		            		</Text>
		            		<Text style={[ c.grey, b.ml3, b.mt1 ]}>
		            			14 April 2020 | Joint News release
		            		</Text>
		            	</TouchableOpacity>
		        	</View>
		        	<View style={[ b.container, p.center, b.mt4 ]}>
		            	<TouchableOpacity onPress={() => this.props.navigation.navigate('More')} style={[ b.rounded, { borderWidth: 1, width: xs(330) }]}>
		            		<Text style={[ f.bold, b.ml3 ]}>
		            			Lebih dari 117 juta anak berisiko kehilangan campak
								vaksin, ketika COVID-19 melonjak
		            		</Text>
		            		<Text style={[ c.grey, b.ml3, b.mt1 ]}>
		            			14 April 2020 | Statement
		            		</Text>
		            	</TouchableOpacity>
		        	</View>
		        	<View style={[ b.container, p.center, b.mt4 ]}>
		            	<TouchableOpacity onPress={() => this.props.navigation.navigate('ReleaseCOVID19')} style={[ b.rounded, { borderWidth: 1, width: xs(330) }]}>
		            		<Text style={[ f.bold, b.ml3 ]}>
		            			Siaran Pers-COVID-19 kargo pasokan medis untuk Afghanistan
								tiba di Bandara Internasional Hamid Karzai
		            		</Text>
		            		<Text style={[ c.grey, b.ml3, b.mt1 ]}>
		            			14 April 2020 | WHO Afghanistan News Release
		            		</Text>
		            	</TouchableOpacity>
		        	</View>
		        	<View style={[ b.container, p.center, b.mt4 ]}>
		            	<TouchableOpacity onPress={() => this.props.navigation.navigate('PublicStatement')} style={[ b.rounded, { borderWidth: 1, width: xs(330) }]}>
		            		<Text style={[ f.bold, b.ml3 ]}>
		            			Pernyataan publik untuk kolaborasi pada vaksin COVID-19
								pengembangan
		            		</Text>
		            		<Text style={[ c.grey, b.ml3, b.mt1 ]}>
		            			13 April 2020 | Statement
		            		</Text>
		            	</TouchableOpacity>
		        	</View>
		        	<View style={[ b.container, p.center, b.mt4 ]}>
		            	<TouchableOpacity onPress={() => this.props.navigation.navigate('DirectorGeneral')} style={[ b.rounded, { borderWidth: 1, width: xs(330) }]}>
		            		<Text style={[ f.bold, b.ml3 ]}>
		            			Pidato pembukaan Direktur Jenderal WHO pada briefing media tentang
								COVID-19 - 13 April 2020
		            		</Text>
		            		<Text style={[ c.grey, b.ml3, b.mt1 ]}>
		            			13 April 2020
		            		</Text>
		            	</TouchableOpacity>
		        	</View>
		        	<View style={[ p.center, b.mt4 ]}>
			            <View style={{ borderBottomWidth: 2, borderBottomColor: '#C0C0C0', width: xs(325) }} />
			        </View>
				</ScrollView>
			</View>
		)
	}
}