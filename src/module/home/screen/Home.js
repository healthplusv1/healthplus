import React, { Component } from 'react';
import { View, FlatList, Text, ScrollView, Alert,  BackHandler, Image, ImageBackground, TouchableOpacity, StyleSheet, ActivityIndicator, Dimensions, Platform, PixelRatio, TextInput } from 'react-native';
import { b, p, f, c } from '../utils/StyleHelper';
import { vs, xs } from '../utils/Responsive';
import { blue, light, grey, softBlue } from '../utils/Color';
import Svg from '../utils/Svg'
import {NavigationEvents} from 'react-navigation';
import { Tab, Tabs, Container } from 'native-base';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import Icon from 'react-native-vector-icons/Ionicons';
import ActionButton from 'react-native-action-button';
import CountDown from 'react-native-countdown-component';
import FastImage from 'react-native-fast-image'
import LinearGradient from 'react-native-linear-gradient'
import ImageSlider from 'react-native-image-slider'
import analytics from '@react-native-firebase/analytics'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'
import { API_URL, API_LOCAL } from 'react-native-dotenv'

const url = 'https://play.google.com/store/apps/details?id=com.healthplus';
const title = '';
const message = 'Please check this out.';
const icon = 'data:<data_type>/<file_extension>;base64,<base64_data>';
const options = Platform.select({
  ios: {
    activityItemSources: [
      { // For sharing url with custom title.
        placeholderItem: { type: 'url', content: url },
        item: {
          default: { type: 'url', content: url },
        },
        subject: {
          default: title,
        },
        linkMetadata: { originalUrl: url, url, title },
      },
      { // For sharing text.
        placeholderItem: { type: 'text', content: message },
        item: {
          default: { type: 'text', content: message },
          message: null, // Specify no text to share via Messages app.
        },
        linkMetadata: { // For showing app icon on share preview.
           title: message
        },
      },
      { // For using custom icon instead of default text icon at share preview when sharing with message.
        placeholderItem: {
          type: 'url',
          content: icon
        },
        item: {
          default: {
            type: 'text',
            content: `${message} ${url}`
          },
        },
        linkMetadata: {
           title: message,
           icon: icon
        }
      },
    ],
  },
  default: {
    title,
    subject: title,
    message: `${message} ${url}`,
  },
});

export default class Home extends Component {

  _didFocusSubscription;
  _willBlurSubscription;

  constructor(props) {
    super(props);
    this._didFocusSubscription = props.navigation.addListener('didFocus', payload =>
      BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
    );
    this.state = {
      ImageSource: null,
      nama: 'loading..',
      point: '....',
      loading: false,
      confirmed: 'loading..',
      recovered: 'loading..',
      deaths: 'loading..',
      newslater: null,
      url_avatar: null,
      time: '',
      group: '',
      group1: '',
      id_login: null,
      joined_ongoing: null,
      joined_upcoming: null,
      popular_challenge: null,
      walk_challenge: null,
      run_challenge: null,
      push_up_challenge: null,
      active_tab_style: { width: '100%' },
      notif: '',
      edit: false,
      count: '',
      clicked: 0,
			exercise: [
				{jenis_challenge: 'Push Up'},
				{jenis_challenge: 'Sit Up'},
				{jenis_challenge: 'Back Up'},
				{jenis_challenge: 'Pull Up'},
				{jenis_challenge: 'Jumping Jacks'},
				{jenis_challenge: 'Squats'},
				{jenis_challenge: 'Squat Jump'},
				{jenis_challenge: 'Mountain Climber'},
				{jenis_challenge: 'Plank'},
			],
      all_challenges: null,
      tantangan_saya: [
        {id: 1, image: require('../asset/icon_group.png'), name: 'Lari Marathon Semarang', lokasi: 'Lapangan Simpang 5', tgl_dimulai: '2021-05-24', tgl_berakhir: '2021-05-31', status: 'dimulai'},
        {id: 2, image: require('../asset/icon_group.png'), name: 'Senam Club Semarang', lokasi: 'Gor Jatidiri', tgl_dimulai: '2021-05-31', tgl_berakhir: '2021-06-01', status: 'akan_dimulai'}
      ],
      clicked_orders: 0,
      orders: [
        {status: 'Semua'},
        {status: 'Menuju Apotek'},
        {status: 'Menuju Lokasi'}
      ],
      all_orders: null,
      list_orders: [
        {id: 1, image: require('../asset/obh.jpg'), name: 'OBH Combi Batuk dan Flu', status: 'Driver menuju lokasi', estimasi: 'Estimasi kedatangan 10 min'},
        {id: 2, image: require('../asset/obh.jpg'), name: 'OBH Combi Batuk dan Flu', status: 'Driver menuju apotek', estimasi: 'Estimasi kedatangan 15 min'},
        {id: 3, image: require('../asset/obh.jpg'), name: 'OBH Combi Batuk dan Flu', status: 'Driver menuju apotek', estimasi: 'Estimasi kedatangan 20 min'}
      ],
      list_orders_apotek: [
        {id: 1, image: require('../asset/obh.jpg'), name: 'OBH Combi Batuk dan Flu', status: 'Driver menuju apotek', estimasi: 'Estimasi kedatangan 15 min'},
        {id: 2, image: require('../asset/obh.jpg'), name: 'OBH Combi Batuk dan Flu', status: 'Driver menuju apotek', estimasi: 'Estimasi kedatangan 20 min'}
      ],
      list_orders_lokasi: [
        {id: 1, image: require('../asset/obh.jpg'), name: 'OBH Combi Batuk dan Flu', status: 'Driver menuju lokasi', estimasi: 'Estimasi kedatangan 10 min'}
      ]
    };
  }

  async componentDidMount(){
    this.setState({ loading: true })
    
    await analytics().logEvent('view_home_page', {
      item: 'HOME_SCREEN'
    })
    await analytics().setUserProperty('user_sees_home_page', 'null')

    this._willBlurSubscription = this.props.navigation.addListener('willBlur', payload =>
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
    );
    // const name = await AsyncStorage.getItem('@nama')
    const id_login = await AsyncStorage.getItem('@id_login')
    if (id_login !== null) {
      this.setState({
        // name: name,
        id_login: id_login,
      })
    }

    axios.get(API_LOCAL+'/main/shopping/cart', {params: {
        id_login: id_login
    }})
    .then(res => {
        this.setState({
            loading: false,
            count: res.data.data.length,
        })
    })
    .catch(e => {
        console.log(e)
    })

    // axios.get(API_URL+'/main/get_user_latest_point', {params:{
    //   id_login: id_login
    // }})
    // .then(result => {
    //   this.setState({
    //     point: result.data.data.point
    //   })
    // })

    // axios.get(API_URL+'/main/get_all_news')
    // .then(result => {
    //   this.setState({
    //     newslater: result.data.data,
    //     loading: false
    //   })
    //   // alert(JSON.stringify(result))
    // })

    axios.get(API_LOCAL+'/main/get_user_by_id_login', {params:{
      id_login: id_login

    }})
    .then(result => {
      // console.log(result.data.data[0].nama)
      this.setState({
        url_avatar: result.data.data[0].url_gambar,
        nama: result.data.data[0].nama,
      })
    })
    .catch(e => {
      if (e.message === 'Network Error') {
        Alert.alert('Tidak Ada Koneksi Internet','Mohon Periksa Koneksi Internet Anda Lalu Restart Aplikasi')
        // Alert.alert('Network Error','Please Check Your Internet Connection then Restart The App')
      }else{
        // Alert.alert('', JSON.stringify(e.message))
      }
    })

    // axios.get(API_URL+'/main/get_admin_group', {params:{
    //   id_login: id_login
    // }})
    // .then(result => {
    //   this.setState({
    //     loading: false,
    //     group: result.data.data
    //   })
    //   // alert(JSON.stringify(result))
    // })
    // .catch(e => {
    //   console.log('get_admin_group => '+e)
    // })

    // axios.get(API_URL+'/main/get_anggota_group', {params:{
    //   id_login: id_login
    // }})
    // .then(result => {
    //   this.setState({
    //     loading: false,
    //     group1: result.data.data
    //   })
    //   // alert(JSON.stringify(result))
    // })
    // .catch(e => {
    //   console.log('get_anggota_group => '+e)
    // })


    // axios.get(API_URL+'/main/get_joined_ongoing', {params:{
    //   id_login: id_login
    // }})
    // .then(res => {
    //   this.setState({
    //     joined_ongoing: res.data.data
    //   })
    //   // alert(JSON.stringify(res.data.data.length))
    // })
    // .catch(e => {
    //   // alert(JSON.stringify(e))
    // })

    // axios.get(API_URL+'/main/get_joined_upcoming', {params:{
    //   id_login: id_login
    // }})
    // .then(res => {
    //   this.setState({
    //     joined_upcoming: res.data.data
    //   })
    //   // alert(JSON.stringify(res.data.data.length))
    // })
    // .catch(e => {
    //   // alert(JSON.stringify(e))
    // })

    // axios.get(API_URL+'/main/get_popular_challenge', {params:{
    //   id_login: id_login
    // }})
    // .then(res => {
    //   this.setState({
    //     popular_challenge: res.data.data
    //   })
    //   // alert(JSON.stringify(res.data.data.length))
    // })
    // .catch(e => {
    //   // alert(JSON.stringify(e))
    // })

    // axios.get(API_URL+'/main/get_walk_challenge', {params:{
    //   id_login: id_login
    // }})
    // .then(res => {
    //   this.setState({
    //     walk_challenge: res.data.data
    //   })
    //   // alert(JSON.stringify(res.data.data.length))
    // })
    // .catch(e => {
    //   // alert(JSON.stringify(e))
    // })

    // axios.get(API_URL+'/main/get_run_challenge', {params:{
    //   id_login: id_login
    // }})
    // .then(res => {
    //   this.setState({
    //     run_challenge: res.data.data
    //   })
    //   // alert(JSON.stringify(res.data.data.length))
    // })
    // .catch(e => {
    //   // alert(JSON.stringify(e))
    // })

    // axios.get(API_URL+'/main/get_push_up_challenge', {params:{
    //   id_login: id_login
    // }})
    // .then(res => {
    //   this.setState({
    //     push_up_challenge: res.data.data
    //   })
    //   // alert(JSON.stringify(res))
    // })
    // .catch(e => {
    //   // alert(JSON.stringify(e))
    // })

    // axios.get(API_URL+'/main/check_notification', {params:{
    //   id_login: id_login
    // }})
    // .then(res => {
    //   this.setState({
    //     notif: res.data.notif
    //   })
    //   // alert(JSON.stringify(res))
    // })
    // .catch(e => {
    //   // alert(JSON.stringify(e))
    // })

    // !!!! end componentDidMount() <===>
  }

  formatNumber(num) {
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  }

  async getLatestPoint(){
    const id_login = await AsyncStorage.getItem('@id_login')
    this.setState({
        point: '....'
      })
    axios.get(API_URL+'/main/get_user_latest_point', {params:{
      id_login: id_login
    }})
    .then(result => {
      // alert(JSON.stringify(result))
      this.setState({
        point: result.data.data.point
      })
    })
    .catch(e => {
      if (e.message === 'Network Error') {
        Alert.alert('Tidak Ada Koneksi Internet','Mohon Periksa Koneksi Internet Anda Lalu Restart Aplikasi')
        // Alert.alert('Network Error','Please Check Your Internet Connection then Restart The App')
      }else{
        // Alert.alert('', JSON.stringify(e.message))
      }
    })
  }

  date(tanggal_mulai) {
    let tanggal_dari = new Date(tanggal_mulai)
    let tanggal_sekarang = new Date()

    // To calculate the time difference of two dates 
    var Difference_In_Time = tanggal_sekarang.getTime() - tanggal_dari.getTime(); 
      
    // To calculate the no. of days between two dates 
    var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);

    return Difference_In_Days.toFixed().toString().replace(/-/gi, '')+" hari Lagi";
  }

  timer(date1, date2) {
    var t1 = new Date(Date.parse(date1+'T00:00:01'));
    var t2 = new Date(Date.parse(date2+'T23:59:59'));
    // console.log(date1, date2);
    var d = new Date();
    // var now = d.getTime();
    // console.log(t1, t2, d);
    if (t1.getTime() < d.getTime()) {
      t1 = d;
    }

    var dif = t2.getTime() - t1.getTime();
    // console.log(dif);

    var seconds_left = dif / 1000;
    // console.log(seconds_left, seconds_left/60, (seconds_left/60)/60, ((seconds_left/60)/60)/24);

    return seconds_left;
  }

  async swicth(jenis_challenge, index) {
		// const id_login = await AsyncStorage.getItem('@id_login')

		// axios.get(API_URL+'/main/filter_kompetisi', {params: {
		// 	id_login: id_login,
		// 	jenis_challenge: jenis_challenge
		// }})
		// .then(resp => {
			this.setState({
				loading: false,
				clicked: index,
				all_challenges: jenis_challenge
			})
			// alert(JSON.stringify(this.state.clicked))
		// })
	}

  async swicthOrders(status, index) {
		// const id_login = await AsyncStorage.getItem('@id_login')

		// axios.get(API_URL+'/main/filter_kompetisi', {params: {
		// 	id_login: id_login,
		// 	jenis_challenge: jenis_challenge
		// }})
		// .then(resp => {
			this.setState({
				loading: false,
				clicked_orders: index,
				all_orders: status
			})
			// alert(JSON.stringify(this.state.clicked))
		// })
	}

  editBtn() {
    if (this.state.edit === false) {
      this.setState({ edit: true })
    } else {
      this.setState({ edit: false })
    }
  }

  handleBackPress = () => {
    if (this.props.navigation.isFocused()) {
        Alert.alert(
        "Keluar Aplikasi",
        "Anda yakin menutup aplikasi?",
        [
          {
            text: 'Tidak',
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
          },
          { text: 'Ya', onPress: () => BackHandler.exitApp() }
        ],  
        { cancelable: false }
        );
    }
    return true;
  };

  render() {
    const banners = [
      require('../asset/neww_banner_01.jpg'),
      require('../asset/neww_banner_03.jpg'),
      require('../asset/neww_banner_05.jpg'),
      require('../asset/neww_banner_06.jpg'),
    ]
      return (
      <>
        {this.state.loading ?
					<View style={[styles.container_loading, styles.horizontal_loading]}>
						<ActivityIndicator size="large" color="#0000ff" />
					</View>
					:
					null
				}
        {/* <NavigationEvents onDidFocus={() => this.componentDidMount()} /> */}
        <ActionButton buttonColor={blue} style={{ zIndex: 1 }} fixNativeFeedbackRadius={true}>
          <ActionButton.Item buttonColor='#9b59b6' title="Buat Liga" onPress={() => this.props.navigation.navigate('BuatGrup')}>
            <Icon name="md-create" size={20} color={light} />
          </ActionButton.Item>
          <ActionButton.Item buttonColor='#3498db' title="Buat Tantangan" onPress={() => this.props.navigation.navigate('BuatKompetisi')}>
            <Icon name="md-create" size={20} color={light} />
          </ActionButton.Item>
        </ActionButton>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={{ backgroundColor: '#000000' }}>
            <LinearGradient
              start={{x: 0.0, y: 0.25}} end={{x: 0.4, y: 2.0}}
              locations={[0,0.4,0]}
              colors={[ '#655353', '#000000', '#000000' ]}
              style={{ width: '100%' }}
            >
              <View style={{ width: '100%', alignItems: 'center' }}>
                <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('4%') }}>
                  <View style={{ width: wp('45%') }}>
                    <Image source={require('../asset/healthPlus.png')} style={{ width: xs(160), height: xs(38) }} />
                  </View>
                  <View style={{ width: wp('45%'), flexDirection: 'row-reverse' }}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Drawer')}>
                      <Image source={require('../asset/menu.png')} style={{ width: wp('7%'), height: wp('7%'), tintColor: light }} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Notifikasi')} style={{ marginRight: wp('2%') }}>
                      <Icon name="ios-notifications" size={25} color={light} />
                      {this.state.notif === 'true' ?
                        <View style={{ width: xs(10), height: xs(10), backgroundColor: 'orangered', borderRadius: xs(5),
                          position: 'absolute', marginLeft: vs(6), marginTop: vs(1) }} />
                        :
                        null
                      }
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.props.navigation.push('Cart')} style={{ marginRight: wp('2%') }}>
                        {this.state.count !== 0 ?
                          <View style={{ width: wp('3.5%'), height: wp('3.5%'), backgroundColor: 'orange', position: 'absolute', borderRadius: wp('3.5%'), zIndex: 1, alignItems: 'center', justifyContent: 'center' }}>
                              <Text style={[ c.light, f._8 ]}>{this.state.count}</Text>
                          </View>
                          :
                          null
                        }
                        <Image source={require('../asset/addCart.png')} style={{ width: wp('7%'), height: wp('7%'), tintColor: light }} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.props.navigation.push('FrequentlyOrder')} style={{ marginRight: wp('2%') }}>
                        <Image source={require('../asset/Frequentlyorder_icon.png')} style={{ width: wp('8%'), height: wp('7%'), tintColor: light }} />
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
              <View style={[ p.center, b.mt2 ]}>
                <View style={[ p.row, b.mt2, b.mb3 ]}>
                  <TouchableOpacity onPress={() => this.props.navigation.push("Profile")} style={[ b.mt2, b.ml1 ]}>
                    <FastImage source={this.state.url_avatar !== null ? require('../asset/dashboard-default-avatar.png') : { uri: API_URL+'/src/images/'+this.state.url_avatar }} style={{ width: xs(70), height: xs(70) }} />
                  </TouchableOpacity>
                  <ImageBackground source={require('../asset/group_1035.png')} style={[ b.ml3, { width: xs(250), height: xs(85) }]} imageStyle={[ b.roundedLow ]}>
                    <View style={ { width: '100%', height: xs(80), alignItems: 'center' }}>
                      <View style={[ b.mt1, { width: xs(220), flexDirection: 'row', alignItems: 'center' }]}>
                        <View style={{ width: xs(190) }}>
                          {this.state.edit === true ?
                            <TextInput
                              placeholder='Masukkan Nama Anda'
                              placeholderTextColor={light}
                              style={{ width: xs(160), height: xs(35), borderWidth: 1, borderColor: light, borderRadius: xs(5), color: light }}
                              value={this.state.nama}
                              onChangeText={(text) => this.setState({ nama: text })}
                            />
                            :
                            <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>{this.state.nama}</Text>
                          }
                        </View>
                        <View style={{ width: xs(30), alignItems: 'flex-end' }}>
                          <TouchableOpacity onPress={() => this.editBtn()}>
                            <Image source={this.state.edit === false ? require('../asset/edit.png') : require('../asset/checkmark.png')} style={{ width: xs(20), height: xs(20), tintColor: light }} />
                          </TouchableOpacity>
                        </View>
                      </View>
                      <View style={[ b.mt1, { width: '100%', height: xs(55), backgroundColor: blue }]}>
                        <View style={{ width: xs(95), height: '100%', justifyContent: 'center' }}>
                          <View style={[ b.ml2, { flexDirection: 'row', alignItems: 'center' }]}>
                            <FastImage source={require('../asset/point_icon.png')} style={{ width: xs(20), height: xs(20) }} />
                            <Text style={[ c.light, b.ml1, { fontFamily: 'lineto-circular-pro-bold' }]}>100</Text>
                          </View>
                          <Text style={[ f._12, c.light, b.ml2, { marginTop: vs(2) }]}>Total Poin Saya</Text>
                        </View>
                      </View>
                    </View>
                  </ImageBackground>
                </View>
              </View>
            </LinearGradient>
            
            <View style={[ b.mt1, p.center, { width: '100%', height: xs(150) }]}>
              <ImageSlider
                loop={false}
                autoPlayWithInterval={5000}
                images={banners}
                onPress={({ index }) => {
                  if (index === 0) {
                    this.props.navigation.push('Challenges')
                  } else if (index == 1) {
                    this.props.navigation.push('BuatGrup')
                  } else if (index == 2) {
                    this.props.navigation.push('BuatKompetisi')
                  } else if (index == 3) {
                    this.props.navigation.push('Social')
                  }
                }}
              />
            </View>

            <View style={[ b.mt4, { width: '100%', alignItems: 'center' }]}>
              <View style={{ width: wp('95%'), alignItems: 'center', borderWidth: 2, borderColor: blue, borderRadius: wp('2%') }}>
                <View style={[ b.mt2, b.mb2, { width: wp('90%') }]}>
                  <Text style={[ c.light ]}>Langkah</Text>
                  <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center', marginTop: wp('2%') }}>
                    <View style={{ width: wp('45%'), flexDirection: 'row' }}>
                      <TouchableOpacity onPress={() => this.props.navigation.push('WalkingChallenges')} style={[ b.mr1, { width: wp('20%'), height: hp('5%'),  backgroundColor: blue, borderRadius: wp('2%'), flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }]}>
                        <Image source={require('../asset/walking.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: light }} />
                        <Text style={[ c.light, { marginLeft: wp('1%') }]}>2000</Text>
                      </TouchableOpacity>
                      <TouchableOpacity onPress={() => this.props.navigation.push('WalkingChallenges')} style={{ width: wp('20%'), height: hp('5%'),  backgroundColor: blue, borderRadius: wp('2%'), flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                        <Image source={require('../asset/staircase.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: light }} />
                        <Text style={[ c.light, { marginLeft: wp('1%') }]}>500</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </View>
            </View>

            <View style={[ b.mt4, { width: '100%', backgroundColor: '#181B22' }]}>
              <View style={{ width: '100%', alignItems: 'center', borderBottomWidth: 3, borderBottomColor: blue }}>
                <View style={[ b.mt2, b.mb1, { width: wp('90%') }]}>
                  <Text style={[ c.light, f._16, { fontFamily: 'lineto-circular-pro-bold' }]}>Tantangan Saya</Text>
                </View>
              </View>
              
              <View style={[ b.mt3, { width: '100%' }]}>
                <View style={[ b.ml3, b.mb2, b.mr2 ]}>
                  <ScrollView showsHorizontalScrollIndicator={false} horizontal={true}>
                    <FlatList
                      horizontal={true}
                      data={this.state.exercise}
                      keyExtractor={item => item.jenis_challenge}
                      renderItem={({item, index}) => (
                        (this.state.clicked === index ?
                          <TouchableOpacity onPress={() => this.swicth(item.jenis_challenge, index)} style={[ b.roundedLow, b.ml1, { backgroundColor: blue }]}>
                            <Text style={[ b.ml2, b.mr2, b.mt1, b.mb1, c.light ]}>{item.jenis_challenge}</Text>
                          </TouchableOpacity>
                          :
                          <TouchableOpacity onPress={() => this.swicth(item.jenis_challenge, index)} style={[ b.roundedLow, b.ml1, { borderWidth: 1, borderColor: blue }]}>
                            <Text style={[ b.ml2, b.mr2, b.mt1, b.mb1, c.light ]}>{item.jenis_challenge}</Text>
                          </TouchableOpacity>
                        )
                      )}
                    />
                  </ScrollView>
                </View>
              </View>

              <View style={[ b.mt3, { width: '100%', alignItems: 'center' }]}>
                <FlatList
                  data={this.state.tantangan_saya}
                  keyExtractor={item => item.id}
                  renderItem={({ item }) => (
                    <View style={{ width: wp('90%'), marginBottom: wp('4%'), flexDirection: 'row' }}>
                      <FastImage source={item.image} style={{ width: wp('25%'), height: wp('25%'), borderRadius: wp('2%') }} />
                      <View style={{ width: wp('40%'), marginLeft: wp('3%') }}>
                        <Text style={[ c.light, f.bold ]} numberOfLines={2}>{item.name}</Text>
                        <Text style={[ c.light ]} numberOfLines={1}>{item.lokasi}</Text>
                        <View style={{ width: wp('40%'), borderWidth: 1, borderColor: 'darkorange', marginTop: wp('2%'), flexDirection: 'row', alignItems: 'center', justifyContent: 'center', borderRadius: wp('1%') }}>
                          {item.status === 'dimulai' ?
                            <>
                              <View style={{ width: wp('26%'), flexDirection: 'row', alignItems: 'center' }}>
                                <CountDown
                                  size={8}
                                  until={this.timer(item.tgl_dimulai, item.tgl_berakhir)}
                                  digitStyle={{backgroundColor: '#121212'}}
                                  digitTxtStyle={{color: 'darkorange', fontSize: 13}}
                                  timeLabelStyle={{color: 'red'}}
                                  timeToShow={['H']}
                                  timeLabels={{h: null}}
                                  showSeparator
                                />
                                <Text style={{ color:'darkorange', marginLeft: vs(-3) }}>h</Text>
                                <CountDown
                                  size={8}
                                  until={this.timer(item.tgl_dimulai, item.tgl_berakhir)}
                                  digitStyle={{backgroundColor: '#121212'}}
                                  digitTxtStyle={{color: 'darkorange', fontSize: 13}}
                                  timeLabelStyle={{color: 'red'}}
                                  timeToShow={['D']}
                                  timeLabels={{d: null}}
                                  showSeparator
                                />
                                <Text style={{ color:'darkorange', marginLeft: vs(-3) }}>j</Text>
                                <CountDown
                                  size={8}
                                  until={this.timer(item.tgl_dimulai, item.tgl_berakhir)}
                                  digitStyle={{backgroundColor: '#121212'}}
                                  digitTxtStyle={{color: 'darkorange', fontSize: 13}}
                                  timeLabelStyle={{color: 'red'}}
                                  timeToShow={['M']}
                                  timeLabels={{m: null}}
                                  showSeparator
                                />
                                <Text style={{ color:'darkorange', marginLeft: vs(-3) }}>m</Text>
                              </View>
                            </>
                            :
                            <>
                              <Text style={{ color: 'darkorange' }}>Dimulai </Text>
                              <Text style={{ color: 'darkorange' }}>{this.date(item.tgl_dimulai)}</Text>
                            </>
                          }
                        </View>
                        <TouchableOpacity onPress={() => this.props.navigation.push('Kompetisi_Chall', { id_kompetisi: item.id })} style={{ width: wp('35%'), height: hp('5%'), backgroundColor: blue, borderRadius: wp('2%'), marginTop: wp('2%'), alignItems: 'center', justifyContent: 'center' }}>
                          <Text style={[ c.light ]}>Lihat Tantangan</Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  )}
                />
              </View>
            </View>

            <View style={[ b.mt3, { width: '100%', backgroundColor: '#181B22', alignItems: 'center' }]}>
              <View style={{ width: wp('90%'), marginTop: wp('4%'), marginBottom: wp('2%') }}>
                <Text style={[ c.light, f._16, { fontFamily: 'lineto-circular-pro-bold' }]}>Order akan datang</Text>
              </View>
              <View style={{ width: '100%', borderBottomWidth: 3, borderBottomColor: blue }} />
              <View style={[ b.mt3, { width: '100%' }]}>
                <View style={[ b.ml3, b.mb2, b.mr2 ]}>
                  <ScrollView showsHorizontalScrollIndicator={false} horizontal={true}>
                    <FlatList
                      horizontal={true}
                      data={this.state.orders}
                      keyExtractor={item => item.status}
                      renderItem={({item, index}) => (
                        (this.state.clicked_orders === index ?
                          <TouchableOpacity onPress={() => this.swicthOrders(item.status, index)} style={[ b.roundedLow, b.ml1, { backgroundColor: blue }]}>
                            <Text style={[ b.ml2, b.mr2, b.mt1, b.mb1, c.light ]}>{item.status}</Text>
                          </TouchableOpacity>
                          :
                          <TouchableOpacity onPress={() => this.swicthOrders(item.status, index)} style={[ b.roundedLow, b.ml1, { borderWidth: 1, borderColor: blue }]}>
                            <Text style={[ b.ml2, b.mr2, b.mt1, b.mb1, c.light ]}>{item.status}</Text>
                          </TouchableOpacity>
                        )
                      )}
                    />
                  </ScrollView>
                </View>
              </View>
              {this.state.clicked_orders === 0 && (
                <FlatList
                  ListHeaderComponent={<View style={{ marginTop: wp('2%') }} />}
                  data={this.state.list_orders}
                  keyExtractor={item => item.id}
                  renderItem={({ item }) => (
                    <View style={{ width: wp('90%'), flexDirection: 'row', marginBottom: wp('4%') }}>
                      <View style={{ width: wp('30%') }}>
                        <View style={{ width: '100%', height: hp('20%'), backgroundColor: light, borderRadius: wp('2%'), alignItems: 'center', justifyContent: 'center' }}>
                          <FastImage source={item.image} style={{ width: wp('25%'), height: wp('25%') }} />
                        </View>
                      </View>
                      <View style={{ width: wp('63%'), marginLeft: wp('2%') }}>
                        <Text style={[ c.light, f._16, f.bold ]} numberOfLines={2}>{item.name}</Text>
                        <Text style={[ c.light ]}>{item.status}</Text>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                          <FastImage source={require('../asset/estimated_icon.png')} style={{ width: wp('5%'), height: wp('5%'), marginRight: wp('2%') }} />
                          <Text style={{ color: '#FE8829' }} numberOfLines={2}>{item.estimasi}</Text>
                        </View>
                        <TouchableOpacity onPress={() => this.props.navigation.push('DetailPayment')} style={{ width: wp('30%'), height: hp('5%'), backgroundColor: blue, alignItems: 'center', justifyContent: 'center', borderRadius: wp('2%'), marginTop: wp('4%') }}>
                          <Text style={[ c.light ]}>Lihat Detail</Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  )}
                />
              )}
              {this.state.clicked_orders === 1 && (
                <FlatList
                  ListHeaderComponent={<View style={{ marginTop: wp('2%') }} />}
                  data={this.state.list_orders_apotek}
                  keyExtractor={item => item.id}
                  renderItem={({ item }) => (
                    <View style={{ width: wp('90%'), flexDirection: 'row', marginBottom: wp('4%') }}>
                      <View style={{ width: wp('30%') }}>
                        <View style={{ width: '100%', height: hp('20%'), backgroundColor: light, borderRadius: wp('2%'), alignItems: 'center', justifyContent: 'center' }}>
                          <FastImage source={item.image} style={{ width: wp('25%'), height: wp('25%') }} />
                        </View>
                      </View>
                      <View style={{ width: wp('63%'), marginLeft: wp('2%') }}>
                        <Text style={[ c.light, f._16, f.bold ]} numberOfLines={2}>{item.name}</Text>
                        <Text style={[ c.light ]}>{item.status}</Text>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                          <FastImage source={require('../asset/estimated_icon.png')} style={{ width: wp('5%'), height: wp('5%'), marginRight: wp('2%') }} />
                          <Text style={{ color: '#FE8829' }} numberOfLines={2}>{item.estimasi}</Text>
                        </View>
                        <TouchableOpacity onPress={() => this.props.navigation.push('DetailPayment')} style={{ width: wp('30%'), height: hp('5%'), backgroundColor: blue, alignItems: 'center', justifyContent: 'center', borderRadius: wp('2%'), marginTop: wp('4%') }}>
                          <Text style={[ c.light ]}>Lihat Detail</Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  )}
                />
              )}
              {this.state.clicked_orders === 2 && (
                <FlatList
                  ListHeaderComponent={<View style={{ marginTop: wp('2%') }} />}
                  data={this.state.list_orders_lokasi}
                  keyExtractor={item => item.id}
                  renderItem={({ item }) => (
                    <View style={{ width: wp('90%'), flexDirection: 'row', marginBottom: wp('4%') }}>
                      <View style={{ width: wp('30%') }}>
                        <View style={{ width: '100%', height: hp('20%'), backgroundColor: light, borderRadius: wp('2%'), alignItems: 'center', justifyContent: 'center' }}>
                          <FastImage source={item.image} style={{ width: wp('25%'), height: wp('25%') }} />
                        </View>
                      </View>
                      <View style={{ width: wp('63%'), marginLeft: wp('2%') }}>
                        <Text style={[ c.light, f._16, f.bold ]} numberOfLines={2}>{item.name}</Text>
                        <Text style={[ c.light ]}>{item.status}</Text>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                          <FastImage source={require('../asset/estimated_icon.png')} style={{ width: wp('5%'), height: wp('5%'), marginRight: wp('2%') }} />
                          <Text style={{ color: '#FE8829' }} numberOfLines={2}>{item.estimasi}</Text>
                        </View>
                        <TouchableOpacity onPress={() => this.props.navigation.push('DetailPayment')} style={{ width: wp('30%'), height: hp('5%'), backgroundColor: blue, alignItems: 'center', justifyContent: 'center', borderRadius: wp('2%'), marginTop: wp('4%') }}>
                          <Text style={[ c.light ]}>Lihat Detail</Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  )}
                />
              )}
            </View>

            {/* <View style={{ width: '100%' }}>
              <Text style={[ b.ml4, b.mt4, b.mb2, f._18, c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>News & Media</Text>
            </View>
            <View style={{ width: '100%' }}>
              <View style={[ b.ml2, b.mr2, b.mt2, b.mb2 ]}>
                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{ backgroundColor: '#000' }}>
                  <FlatList
                    extraData={this.state}
                    horizontal={true}
                    data={this.state.newslater}
                    keyExtractor={item => item.id_newslater}
                    renderItem={({item}) => (
                      <TouchableOpacity onPress={() => this.props.navigation.navigate('News', {url_link: item.content})} style={[ b.rounded, b.ml2, b.shadowHigh, { width: xs(150), height: xs(210), backgroundColor: '#181B22' }]}>
                        <View style={[ p.center, { width: xs(150) }]}>
                          <FastImage progressiveRenderingEnabled={true} source={{ uri: API_URL+'/src/images/'+item.picture }} style={[ b.rounded, { width: xs(150), height: xs(100) }]} />
                        </View>
                        <Text style={[ b.pl2, b.pr2, b.pt2, c.light ]}>{item.title}</Text>
                      </TouchableOpacity>
                    )}
                  />
                </ScrollView>
              </View>
            </View> */}
            
          </View>
        </ScrollView>
      </>
      )}
}
const styles = StyleSheet.create({
  // loading centered screenn
  container_loading: {
    flex: 1,
    position:'absolute',
    zIndex:2,
    width: '100%',
    height: '100%',
    justifyContent: "center",
    backgroundColor: 'rgba(0,0,0,0.7)'
  },
  horizontal_loading: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },

  container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#FFF8E1'
  },

  ImageContainer: {
    borderRadius: xs(50),
    borderColor: '#9B9B9B',
    borderWidth: 0 / PixelRatio.get(),
    justifyContent: 'center',
    alignItems: 'center',
    
  },

  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },

  TriangleShapeCSS: {
    width: 100,
    height: 0,
    borderTopWidth: 50,
    borderLeftWidth: 70,
    borderRightWidth: 30,
    borderBottomWidth: 50,
    borderStyle: 'solid',
    backgroundColor: 'transparent',
    borderTopColor: '#00BCD4',
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
    borderBottomColor: '#00BCD4'
  }
});