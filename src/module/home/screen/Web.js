import React from 'react'
import { Text, View, BackHandler, TouchableOpacity, I18nManager } from 'react-native'
import { b, p, f, c } from '../utils/StyleHelper'
import { vs, xs } from '../utils/Responsive'
import { blue, light, grey, softBlue } from '../utils/Color'
import WebView from 'react-native-webview'
import analytics from '@react-native-firebase/analytics'

export default class Web extends React.Component {

	constructor(props){
		super(props);
		this.state ={
		}
	}

	async componentDidMount() {
		await analytics().logEvent('read_news', {
			item: 'NEWS_SCREEN'
		})
		await analytics().setUserProperty('user_read_news', true)
	}

	render() {
		return (
			<>
				<View>
					<View style={{ height: xs(50) }}>
						<TouchableOpacity onPress={() => this.props.navigation.navigate('Home')} style={[ p.textRight, b.mt2, { marginLeft: vs(260) }]}>
							<Text style={[ f.bold, f._20 ]}> Batal </Text>
						</TouchableOpacity>
					</View>
				</View>
				<WebView
			        source={{uri: 'http://covid19.go.id/'}}
			    />
			</>
		)
	}
}