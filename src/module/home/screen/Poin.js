import React from 'react'
import { Text, View, ScrollView, Image, TouchableOpacity, ImageBackground } from 'react-native'
import { b, p, f, c } from '../utils/StyleHelper';
import { vs, xs } from '../utils/Responsive';

export default class How extends React.Component {
	render() {
		return (
			<ScrollView showsVerticalScrollIndicator={false}>
				<View style={[ p.center, { width: '100%' }]}>
					<ImageBackground source={require('../asset/poin.jpg')} style={{ width: xs(360), height: xs(1150) }}>
						<TouchableOpacity onPress={() => this.props.navigation.navigate("Home")}>
							<Image source={require('../asset/back.png')} style={[ b.mt4, b.ml4, { marginTop: vs(30), width: xs(30), height: xs(30) }]} />
						</TouchableOpacity>
					</ImageBackground>
				</View>
			</ScrollView>
		)
	}
}