import React, { memo } from 'react';
import { ms } from '../utils/Responsive';
import Search from '../asset/icons/search.svg'
import Camera from '../asset/icons/camera.svg'
import Kompetisi from '../asset/icons/kompetisi.svg'
import KompetisiDua from '../asset/icons/kompetisidua.svg'
import Mengikuti from '../asset/icons/mengikuti.svg'
import Riwayat from '../asset/icons/riwayat.svg'
import Tanggal from '../asset/icons/tanggal.svg'
import Tanggal2 from '../asset/icons/tanggal2.svg'
import Poin from '../asset/icons/poin.svg'
import Poin2 from '../asset/icons/poin2.svg'
import Piala from '../asset/icons/piala.svg'
import Bendera from '../asset/icons/bendera.svg'
import TanggalSelesai from '../asset/icons/tanggal_selesai.svg'
import DuaBendera from '../asset/icons/dua_bendera.svg'
import Postingan from '../asset/icons/postingan.svg'
import Cam from '../asset/icons/cam.svg'
import Img from '../asset/icons/img.svg'
import Guideline from '../asset/icons/guideline.svg'
import Whatsapp from '../asset/icons/whatsapp.svg'
import Facebook from '../asset/icons/facebook.svg'
import Instagram from '../asset/icons/instagram.svg'
import Friends from '../asset/icons/friend_icon.svg'
import Post from '../asset/icons/post_icon.svg'
import League from '../asset/icons/league_icon.svg'

const Svg = ({ icon, size = 30, fill, opacity, ...props }) => {
  const icons = {
    search: Search,
    camera: Camera,
    kompetisi: Kompetisi,
    kompetisidua: KompetisiDua,
    mengikuti: Mengikuti,
    riwayat: Riwayat,
    tanggal: Tanggal,
    tanggal2: Tanggal2,
    poin: Poin,
    poin2: Poin2,
    piala: Piala,
    bendera: Bendera,
    tanggal_selesai: TanggalSelesai,
    dua_bendera: DuaBendera,
    postingan: Postingan,
    cam: Cam,
    img: Img,
    guideline: Guideline,
    whatsapp: Whatsapp,
    facebook: Facebook,
    instagram: Instagram,
    friends: Friends,
    post: Post,
    league: League
  };

  if (icons[icon] === undefined)
    return (
      <></>
    );

  const Icon = icons[icon]
  return (
    <Icon
      width={ms(size)}
      fill={fill || null}
      height={ms(size)}
      fillOpacity={opacity || 1}
      {...props}
    />
  );
}
export default memo(Svg)
