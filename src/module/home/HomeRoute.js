import { createStackNavigator} from 'react-navigation-stack'
import HomePage from './screen/Home'
import GrupPage from './screen/Tracking/Grup'
import KompetisiPage from './screen/Tracking/Kompetisi'
import RiwayatKompetisiPage from './screen/Tracking/RiwayatKompetisi'
import NotifikasiPage from './screen/Tracking/Notifikasi'
import DrawerPage from '../drawer/screen/Drawer'
import FAQPage from '../drawer/screen/FAQ';
import TermsPage from '../drawer/screen/Terms';
import PrivacyPage from '../drawer/screen/Privacy';
import { vs, xs } from '../home/utils/Responsive';

export const Homestack = createStackNavigator({
    Home: {
        screen: HomePage,
        navigationOptions:{
            header: null,
        }
    },
    
    Grup: {
        screen: GrupPage,
        navigationOptions:{
            header: null
        }
    },

    Kompetisi: {
        screen: KompetisiPage,
        navigationOptions:{
            header: null,
        }
    },

    RiwayatKompetisi: {
        screen: RiwayatKompetisiPage,
        navigationOptions:{
            header: null,
        }
    },

    Notifikasi: {
        screen: NotifikasiPage,
        navigationOptions:{
            header: null,
        }
    },

    Drawer: {
        screen: DrawerPage,
        navigationOptions:{
            header: null,
        }
    },

    FAQ: {
        screen: FAQPage,
        navigationOptions:{
            header: null,
        }
    },

    Terms: {
        screen: TermsPage,
        navigationOptions:{
            header: null,
        }
    },
    
    Privacy: {
        screen: PrivacyPage,
        navigationOptions:{
            header: null,
        }
    },
});