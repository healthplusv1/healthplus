import { createStackNavigator} from 'react-navigation-stack'
import MyRewardsPage from './screen/MyRewards'

export const MyRewardsstack = createStackNavigator({
    MyRewards: {
        screen: MyRewardsPage,
        navigationOptions:{
            headerTitle:'Hadiah Saya',
            headerTitleStyle:{
                textAlign:  'center',
                flex: 1,
                fontWeight: 'bold',
            }
        }
    },
});