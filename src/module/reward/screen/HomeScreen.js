import React from 'react'
import { Text, View, BackHandler, TouchableOpacity, I18nManager } from 'react-native'
import { b, p, f, c } from '../utils/StyleHelper'
import { vs, xs } from '../utils/Responsive'
import { blue, light, grey, softBlue } from '../utils/Color'
import WebView from 'react-native-webview'


export default class HomeScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state ={

    }
  }

  render() {
    return (
      <>
        <View>
          <View style={{ height: xs(50) }}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('MyRewards')} style={[ p.textRight, b.mt2, { marginLeft: vs(260) }]}>
              <Text style={[ f.bold, f._20 ]}> Batal </Text>
            </TouchableOpacity>
          </View>
        </View>
        <WebView
              source={{uri: this.props.navigation.getParam('url_link')}}
          />
      </>
    )
  }
}