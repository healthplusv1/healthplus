import React from 'react'
import { Text, ActivityIndicator, View, StyleSheet, TouchableOpacity, Image, I18nManager, Platform, ScrollView, ImageBackground, FlatList, Alert } from 'react-native'
import Tabs from './HomeScreen';
import { b, p, f, c } from '../utils/StyleHelper';
import { vs, xs } from '../utils/Responsive';
import { blue, light, grey, softBlue } from '../utils/Color';
import DropDownItem from 'react-native-drop-down-item';
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import {NavigationEvents} from 'react-navigation';
import FastImage from 'react-native-fast-image'
import Icon from 'react-native-vector-icons/Ionicons'
import { API_URL } from 'react-native-dotenv'

const IC_ARR_UP = require('../asset/arrow2.png');
const IC_ARR_DOWN = require('../asset/arrow1.png');

// type Props = {};

export default class MyRewards extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
        loadinga: true,
        loadingb: true,
        loadingc: true,
        data_voucher: null,
        data_pulsa: null,
        data_token: null,
        id_voucher: null,
        voucher: [
          {
            title: 'Voucher',
          }
        ],
        pulsa: [
          {
            title: 'Pulsa',
          }
        ],
        token: [
          {
            title: 'Token Game',
          }
        ]
      };
    };

    async componentDidMount(){
      const id_login = await AsyncStorage.getItem('@id_login')

      this.setState({
        loadinga: true,
        loadingb: true,
        loadingc: true
      })

      axios.get(API_URL+'/main/get_all_active_pulsa_voucher', {params:{
        id_login: id_login
      }})
      .then(res => {
        if (res.data.status === 'true') {
          this.setState({
            data_pulsa: res.data.data,
            loadingb: false
          })
          // alert(JSON.stringify(this.state.data_pulsa))
        }else{
          this.setState({
            loadingb: false
          })
        }
      })
      .catch(err => {
        if (err.message === 'Network Error') {
            Alert.alert('Tidak Ada Koneksi Internet','Mohon Periksa Koneksi Internet Anda Lalu Restart Aplikasi')
            // Alert.alert('Nework Error','Please Check Your Internet Connection then Restart The App')
          }
        // Alert.alert('',JSON.stringify(err))
      })

      axios.get(API_URL+'/main/get_all_active_voucher_voucher', {params:{
        id_login: id_login
      }})
      .then(resp => {
        if (resp.data.status === 'true') {
          this.setState({
            data_voucher: resp.data.data,
            id_voucher: resp.data.data.id_voucher,
            loadingc: false
          })
          // alert(JSON.stringify(res))
        }else{
          this.setState({
            loadingc: false
          })
        }
      })
      .catch(err => {
        if (err.message === 'Network Error') {
            Alert.alert('Tidak Ada Koneksi Internet','Mohon Periksa Koneksi Internet Anda Lalu Restart Aplikasi')
            // Alert.alert('Nework Error','Please Check Your Internet Connection then Restart The App')
          }
        // Alert.alert('',JSON.stringify(err))
      })

      axios.get(API_URL+'/main/get_all_active_game_token', {params:{
        id_login: id_login
      }})
      .then(response => {
        if (response.data.status === 'true') {
          this.setState({
            data_token: response.data.data,
            loadinga: false
          })
          // alert(JSON.stringify(this.state.data_token))
        }else{
          this.setState({
            loadinga: false
          })
        }
      })
      .catch(err => {
          if (err.message === 'Network Error') {
            Alert.alert('Tidak Ada Koneksi Internet','Mohon Periksa Koneksi Internet Anda Lalu Restart Aplikasi')
            // Alert.alert('Nework Error','Please Check Your Internet Connection then Restart The App')
          }
        // Alert.alert('',JSON.stringify(err))
      })

      // setTimeout(() => {this.setState({loading: false})}, 2500)
    }

    render() {
      return (
        <>
          {this.state.loadinga || this.state.loadingb || this.state.loadingc ?
            <View style={[styles.container_loading, styles.horizontal_loading]}>
              <ActivityIndicator size="large" color="#0000ff" />
            </View>
            :
            <>
              <NavigationEvents onDidFocus={() => this.componentDidMount()} />
              <View style={[ p.row, { width: '100%', height: xs(80), backgroundColor: '#000', alignItems: 'center' }]}>
                <View style={[ b.ml4, { width: xs(30) }]}>
                  <TouchableOpacity onPress={() => this.props.navigation.pop()}>
                    <Icon name="ios-arrow-back" size={40} color="white" />
                  </TouchableOpacity>
                </View>
                <View style={[ b.ml4, p.center, { width: xs(230) }]}>
                  <Text style={[ c.light, f.bold, f._20 ]}>Hadiah Saya</Text>
                </View>
              </View>
              <View style={[ styles.container ]}>
                <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#181B22', alignSelf: 'stretch' }}>
                  <View>
                    <Image source={require('../asset/myrewards.png')} style={{ width: '100%', height: xs(150) }} />
                  </View>
                  <View style={[ p.center, b.mt4 ]}>
                    {this.state.voucher ?
                      this.state.voucher.map((param, i) => {
                        return (
                          <DropDownItem
                            key={i}
                            style={[ b.mt2 ]}
                            contentVisible={false}
                            invisibleImage={IC_ARR_DOWN}
                            visibleImage={IC_ARR_UP}
                            header={
                              <View style={[ styles.header, b.roundedLow, { backgroundColor: '#2DCC70', width: xs(300), height: xs(50) }]}>
                                <Text style={[ f.bold, b.mt1, { fontSize: 18, color: light }]}>{param.title}</Text>
                              </View>
                            }
                          >
                            {this.state.data_voucher === null ? 
                              <View style={[ p.center, { width: xs(280) }]}>
                                <Text style={[ c.light ]}> Tidak ada hadiah... </Text>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('Voucher')} style={[ b.roundedLow, b.mt2, , p.center, { backgroundColor: '#2DCC70', height: xs(50), width: xs(200) }]}>
                                  <Text style={[ c.light, f._18 ]}> Klaim Hadiah </Text>
                                </TouchableOpacity>
                              </View>
                              :
                              <FlatList
                                extraData={this.state}
                                data={this.state.data_voucher}
                                keyExtractor={item => item.id_user_voucher}
                                renderItem={({item}) => (
                                  <View style={[b.mt2]}>
                                    <TouchableOpacity onPress={() => {
                                      if (item.id_voucher === '11') {
                                        this.props.navigation.navigate('IndomartVoucher', {id_user_voucher: item.id_user_voucher})
                                      } else if (item.id_voucher === '12') {
                                        this.props.navigation.navigate('IndomartVoucher50', {id_user_voucher: item.id_user_voucher})
                                      } else if (item.id_voucher === '13') {
                                        this.props.navigation.navigate('IndomartVoucher100', {id_user_voucher: item.id_user_voucher})
                                      } else {
                                        this.props.navigation.navigate('StarbucksVoucher', {id_user_voucher: item.id_user_voucher})
                                      }
                                    }
                                    }>
                                      <ImageBackground source={require('../asset/Group-480.png')} style={{ width: xs(280), height: xs(90) }}>
                                        <View style={[ p.row ]}>
                                          <View style={[ b.ml3, p.center ]}>
                                            <FastImage source={{ uri: API_URL+'/src/images/'+item.url_gambar }} style={{ width: xs(50), height: xs(50) }} />
                                          </View>
                                          <View style={[ p.center, { width: xs(130), height: xs(75) }]}>
                                            <View>
                                              <Text style={{ marginLeft: vs(7) }}>{item.nama_voucher}</Text>
                                              <Text style={{ marginLeft: vs(7) }}>{item.diskon}</Text>
                                            </View>
                                          </View>
                                          <View style={[ p.center, { width: xs(75) }]}>
                                            <FastImage source={{ uri: API_URL+'/src/images/'+item.url_ikon }} style={[ b.ml1, { width: xs(50), height: xs(50) }]} />
                                          </View>
                                        </View>
                                      </ImageBackground>
                                    </TouchableOpacity>
                                  </View>
                                )}
                              />
                            }
                          </DropDownItem>
                        );
                      })
                      :
                      null
                    }

                    {this.state.pulsa ?
                      this.state.pulsa.map((param, i) => {
                        return (
                          <DropDownItem
                            key={i}
                            style={[ b.mt2 ]}
                            contentVisible={false}
                            invisibleImage={IC_ARR_DOWN}
                            visibleImage={IC_ARR_UP}
                            header={
                              <View style={[ styles.header, b.roundedLow, { backgroundColor: '#A19ECB', width: xs(300), height: xs(50) }]}>
                                <Text style={[ f.bold, b.mt1, { fontSize: 18, color: light }]}>{param.title}</Text>
                              </View>
                            }
                          >
                            {this.state.data_pulsa === null ? 
                              <View style={[ p.center, { width: xs(280) }]}>
                                <Text style={[ c.light ]}> Tidak ada hadiah... </Text>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('Pulsa')} style={[ b.roundedLow, b.mt2, , p.center, { backgroundColor: '#A19ECB', height: xs(50), width: xs(200) }]}>
                                  <Text style={[ c.light, f._18 ]}> Klaim Hadiah </Text>
                                </TouchableOpacity>
                              </View>
                              :
                              <FlatList
                                extraData={this.state}
                                data={this.state.data_pulsa}
                                keyExtractor={item => item.id_user_voucher}
                                renderItem={({item}) => (
                                  <View style={[b.mt2]}>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('StarbucksVoucher', {id_user_voucher: item.id_user_voucher})}>
                                      <ImageBackground source={require('../asset/Group-480.png')} style={{ width: xs(280), height: xs(90) }}>
                                        <View style={[ p.row, b.mt3, b.ml3 ]}>
                                          <Image source={{ uri: API_URL+'/src/images/'+item.url_gambar }} style={{ width: xs(50), height: xs(50) }} />
                                          <View style={{ width: xs(130) }}>
                                            <Text style={[ b.ml2, b.mt1 ]}> {item.nama_voucher} </Text>
                                            <Text style={[ b.ml2 ]}> {item.diskon} </Text>
                                          </View>
                                          <View>
                                            <Image source={{ uri: API_URL+'/src/images/'+item.url_ikon }} style={[ b.ml3, { width: xs(50), height: xs(50) }]} />
                                          </View>
                                        </View>
                                      </ImageBackground>
                                    </TouchableOpacity>
                                  </View>
                                )}
                              />
                            }
                          </DropDownItem>
                        );
                      })
                      :
                      null
                    }

                    {this.state.token ?
                      this.state.token.map((param, i) => {
                        return (
                          <DropDownItem
                            key={i}
                            style={[ b.mt2 ]}
                            contentVisible={false}
                            invisibleImage={IC_ARR_DOWN}
                            visibleImage={IC_ARR_UP}
                            header={
                              <View style={[ styles.header, b.roundedLow, { backgroundColor: '#78D1C2', width: xs(300), height: xs(50) }]}>
                                <Text style={[ f.bold, b.mt1, { fontSize: 18, color: light }]}>{param.title}</Text>
                              </View>
                            }
                          >
                            {this.state.data_token === null ? 
                              <View style={[ p.center, { width: xs(280) }]}>
                                <Text style={[ c.light ]}> Tidak ada hadiah... </Text>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('TokenGame')} style={[ b.roundedLow, b.mt2, , p.center, { backgroundColor: '#78D1C2', height: xs(50), width: xs(200) }]}>
                                  <Text style={[ c.light, f._18 ]}> Klaim Hadiah </Text>
                                </TouchableOpacity>
                              </View>
                              :
                              <FlatList
                                extraData={this.state}
                                data={this.state.data_token}
                                keyExtractor={item => item.id_user_voucher}
                                renderItem={({item}) => (
                                  <View style={[b.mt2]}>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('TokenVoucher', {id_user_voucher: item.id_user_voucher})}>
                                      <ImageBackground source={require('../asset/Group-480.png')} style={{ width: xs(280), height: xs(90) }}>
                                        <View style={[ p.row, b.mt3, b.ml3 ]}>
                                          <Image source={{ uri: API_URL+'/src/images/'+item.url_gambar }} style={{ width: xs(50), height: xs(50) }} />
                                          <View style={{ width: xs(130) }}>
                                            <Text style={[ b.ml2 ]}>
                                              {item.nama_voucher}
                                            </Text>
                                            <Text style={[ b.ml2 ]}>
                                              {item.diskon}
                                            </Text>
                                          </View>
                                          <View>
                                            <Image source={{ uri: API_URL+'/src/images/'+item.url_ikon }} style={[ b.ml3, { width: xs(50), height: xs(50) }]} />
                                          </View>
                                        </View>
                                      </ImageBackground>
                                    </TouchableOpacity>
                                  </View>
                                )}
                              />
                            }
                          </DropDownItem>
                        );
                      })
                      :
                      null
                    }
                  </View>
                  <View style={{ height: xs(30) }} />
                </ScrollView>
              </View>
            </>
          }
        </>
      )
    }
}

const styles = StyleSheet.create({
  // App container
  container: {
    flex: 1,                            // Take up all screen
    // backgroundColor: 'blue',         // Background color
  },
  // Tab content container
  content: {
    flex: 1,                            // Take up all available space
    justifyContent: 'center',           // Center vertically
    alignItems: 'center',               // Center horizontally
    backgroundColor: '#FFFFFF',         // Darker background for content area
  },
  // Content header
  header: {
    margin: 10,                         // Add margin
    color: '#FFFFFF',                   // White color
    fontFamily: 'Avenir',               // Change font family
    fontSize: 26,                       // Bigger font size
  },
  // Content text
  text: {
    marginHorizontal: 20,               // Add horizontal margin
    color: 'rgba(255, 255, 255, 0.75)', // Semi-transparent text
    textAlign: 'center',                // Center
    fontFamily: 'Avenir',
    fontSize: 18,
  },

  header: {
    width: '100%',
    paddingVertical: 8,
    paddingHorizontal: 12,
    flexWrap: 'wrap',
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerTxt: {
    fontSize: 12,
    color: 'rgb(74,74,74)',
    marginRight: 60,
    flexWrap: 'wrap',
  },
  txt: {
    fontSize: 14,
  },
  // loading centered screenn
  container_loading: {
    flex: 1,
    position:'absolute',
    zIndex:2,
    width: '100%',
    height: '100%',
    justifyContent: "center",
    backgroundColor: 'rgba(0,0,0,0.7)'
  },
  horizontal_loading: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  }
});