import React from 'react'
import { Text, View, TouchableOpacity, ScrollView, Image, ImageBackground } from 'react-native'
import { b, p, f, c } from '../utils/StyleHelper'
import { vs, xs } from '../utils/Responsive'
import { blue, light, grey, softBlue } from '../utils/Color'
import axios from 'axios'
import { API_URL } from 'react-native-dotenv'

export default class IndomartVoucher extends React.Component {

	constructor(props){
		super(props);
		this.state ={
			kode: 'loading..',
			loading: true
		}
	}

	componentDidMount(){
		axios.get(API_URL+'/main/get_user_voucher_detail', {params:{
			id_user_voucher: this.props.navigation.getParam('id_user_voucher')
		}})
		.then(result => {
			// alert(JSON.stringify(result))
			if (result.data.status === "voucher_not_found") {
				Alert.alert(
				'Voucher Tidak Ditemukan',
				'Voucher Baru Saja Dihapus',
				[
					{text: 'Kembali', onPress: () => {
						this.props.navigation.navigate('MyRewards')
					}}
				]);
			}else{
				this.setState({
					kode: result.data.data.code,
					loading: false
				})
			}
			// alert(JSON.stringify(this.state))
		})
		.catch(e => {
			// alert(e)
		})
	}

	render() {
		return (
			<ScrollView>
				<View style={{ width: '100%', height: xs(80), backgroundColor: blue }}>
					<View style={[ p.row ]}>
						<TouchableOpacity onPress={() => this.props.navigation.navigate('MyRewards')} style={[b.ml4, { marginTop: vs(25), width: xs(50), height: xs(50) }]}>
							<Image source={require('../asset/back.png')} style={{ width: xs(25), height: xs(25) }}/>
						</TouchableOpacity>
						<Text style={[ c.light, f.bold, f._20, { marginTop: vs(25), marginLeft: vs(40) }]}> Your E-Voucher </Text>
					</View>
				</View>
				<ImageBackground source={require('../asset/VBE.jpg')} style={[ p.center, { width: xs(360), height: xs(650) }]}>
					<View style={{ width: xs(295), height: xs(30) }}>
						<Text style={[ p.textCenter, f._20 ]}> {this.state.kode} </Text>
					</View>
				</ImageBackground>
			</ScrollView>
		)
	}
}