import React from 'react'
import { Alert, Text, View, ScrollView, Image, ImageBackground, TouchableOpacity, BackHandler, Clipboard, Linking, I18nManager } from 'react-native'
import { b, p, f, c } from '../utils/StyleHelper';
import { vs, xs } from '../utils/Responsive';
import { blue, light, grey, softBlue } from '../utils/Color';
import axios from 'axios'
import { API_URL } from 'react-native-dotenv'

export default class TokenVoucher extends React.Component {

	constructor(props) {
	    super(props);
	    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
	    this.state = {
			loading: true,
			kode: 'loading..',
			nama_voucher: 'loading..',
			harga: 'loading..',
			diskon: 'loading..',
			tgl_klaim: 'loading..',
			tgl_exp: 'loading..',
			url: null,
		}
	}

	componentDidMount(){
		axios.get(API_URL+'/main/get_user_game_token_detail', {params:{
			id_user_voucher: this.props.navigation.getParam('id_user_voucher')
		}})
		.then(result => {
			// alert(JSON.stringify(result))
			if (result.data.status === "voucher_not_found") {
				Alert.alert(
				'Voucher Tidak Ditemukan',
				'Voucher Baru Saja Dihapus',
				[
					{text: 'Kembali', onPress: () => {
						this.props.navigation.navigate('MyRewards')
					}}
				]);
			}else{
				this.setState({
					nama_voucher: result.data.data.nama_voucher,
					diskon: result.data.data.diskon,
					deskripsi: result.data.data.deskripsi,
					gambar: result.data.data.url_gambar,
					harga: result.data.data.harga,
					kode: result.data.data.code,
					tgl_klaim: result.data.data.tanggal_klaim,
					tgl_exp: result.data.data.tanggal_exp,
					url: result.data.data.url,
					loading: false
				})
			}
			// alert(JSON.stringify(this.state))
		})
		.catch(e => {
			// alert(e)
		})
	}

	componentWillMount() {
	    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
	}

	componentWillUnmount() {
	    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
	}

	handleBackButtonClick() {
	    this.props.navigation.goBack('MyReward');
	    return true;
	}

	render() {
		return (
			<ScrollView>
				<View style={[ b.container, { width: '100%', height: xs(100), backgroundColor: blue }]}>
					<View style={[ p.row ]}>
						<TouchableOpacity onPress={() => this.props.navigation.navigate('MyRewards')} style={[b.ml4, { marginTop: vs(30), width: xs(50), height: xs(50) }]}>
							<Image source={require('../asset/back.png')} style={{ width: xs(25), height: xs(25) }}/>
						</TouchableOpacity>
						<Text style={[ c.light, f.bold, f._20, { marginTop: vs(30), marginLeft: vs(40) }]}> Your E-Voucher </Text>
					</View>
				</View>
				<View style={[ b.container, p.center ]}>
					<View style={[ p.center, { marginTop: vs(80), width: xs(300) }]}>
						<TouchableOpacity onPress={() => { 
							Clipboard.setString(this.state.kode)
							Alert.alert('', 'Tersalin!') 
						}}>
						<Text style={[ c.blue, f._24, f.bold ]}> {this.state.kode} </Text>
						</TouchableOpacity>
						<Text style={[ f.bold, c.grey, { marginTop: vs(30) }]}> E-Voucher Detail </Text>
						<Text style={[ f._18, p.textCenter ]}> {this.state.nama_voucher} </Text>
						<View style={[ b.mt1, { borderBottomWidth: 2, borderBottomColor: '#C0C0C0', width: '100%' }]} />
						<Text style={[ f.bold, c.grey ]}> Points Claimed </Text>
						<Text style={[ f._18 ]}> {this.state.harga} Pts. </Text>
						<View style={[ b.mt1, { borderBottomWidth: 2, borderBottomColor: '#C0C0C0', width: '100%' }]} />
						<Text style={[ f.bold, c.grey ]}> E-Voucher Amount </Text>
						<Text style={[ f.bold, c.blue, f._18 ]}> {this.state.diskon} </Text>
					</View>
				</View>
				<View style={[ b.container, p.center, { marginTop: vs(50) }]}>
					<ImageBackground source={require('../asset/Path1684.png')} style={{ width: '100%', height: xs(300) }}>
						<View style={[ p.row, b.mt4, { width: xs(350) }]}>
							<View style={[ p.center, b.ml4, { width: xs(100) }]}>
								<Text> Claimed On </Text>
								<Text style={[ c.blue, f.bold, f._18 ]}> {this.state.tgl_klaim.replace(/-/g, '.')} </Text>
							</View>
							<View style={{ marginLeft: vs(100) }}>
								<Text> Contact Details </Text>
							</View>
						</View>
						<View style={[ p.row, b.mt4, { width: xs(350) }]}>
							<View style={[ p.center, b.ml4, { width: xs(100) }]}>
								<Text> Valid Till </Text>
								<Text style={[ c.blue, f.bold, f._18 ]}> {this.state.tgl_exp.replace(/-/g, '.')} </Text>
							</View>
						</View>
						{this.state.url === null ?
						(
							<View></View>
						)
						:
						(
							<View style={[ b.container, p.center, { marginTop: vs(40) }]}>
								<TouchableOpacity onPress={() => this.props.navigation.navigate("HomeScreen", {url_link: this.state.url})} style={[ b.roundedHigh, p.center, { backgroundColor: blue, width: xs(200), height: xs(50) }]}>
									<Text style={[ c.light, f.bold, f._20 ]}> Gunakan Sekarang </Text>
								</TouchableOpacity>
							</View>
						) 
						}
					</ImageBackground>
				</View>
			</ScrollView>
		)
	}
}