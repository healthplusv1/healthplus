import { createStackNavigator} from 'react-navigation-stack'
import DrawerPage from './screen/Drawer'

export const Drawerstack = createStackNavigator({
	Drawer: {
		screen: DrawerPage,
		navigationOptions: {
			header: null,
		}
	}
})