import React from 'react'
import { Text, View, TouchableOpacity, Linking, ScrollView, Image } from 'react-native'
import { b, p, f, c } from '../utils/StyleHelper';
import { vs, xs } from '../utils/Responsive';
import { blue, light, grey, softBlue } from '../utils/Color';
import Svg from '../utils/Svg'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import Icon from 'react-native-vector-icons/Ionicons'
import Share from 'react-native-share'

const url = 'https://play.google.com/store/apps/details?id=com.healthplus';
const title = '';
const message = 'Silahkan download HealthPlus+';
const icon = 'data:<data_type>/<file_extension>;base64,<base64_data>';
const options = Platform.select({
	ios: {
		activityItemSources: [
		{ // For sharing url with custom title.
			placeholderItem: { type: 'url', content: url },
			item: {
			default: { type: 'url', content: url },
			},
			subject: {
			default: title,
			},
			linkMetadata: { originalUrl: url, url, title },
		},
		{ // For sharing text.
			placeholderItem: { type: 'text', content: message },
			item: {
			default: { type: 'text', content: message },
			message: null, // Specify no text to share via Messages app.
			},
			linkMetadata: { // For showing app icon on share preview.
			title: message
			},
		},
		{ // For using custom icon instead of default text icon at share preview when sharing with message.
			placeholderItem: {
			type: 'url',
			content: icon
			},
			item: {
			default: {
				type: 'text',
				content: `${message} ${url}`
			},
			},
			linkMetadata: {
			title: message,
			icon: icon
			}
		},
		],
	},
	default: {
		title,
		subject: title,
		message: `${message} ${url}`,
	},
});

export default class Drawer extends React.Component {

	constructor(props){
		super(props);
	}

	render() {
		return (
			<ScrollView style={{ backgroundColor: '#181B22' }}>
				<View style={[ p.center, { width: '100%', height: xs(80), backgroundColor: '#000' }]}>
					<View style={[ p.row, { width: '100%' }]}>
						<View style={[ b.ml4, { width: xs(50) }]}>
							<TouchableOpacity onPress={() => this.props.navigation.pop()}>
								<Icon name="ios-arrow-back" size={30} color={light} />
							</TouchableOpacity>
						</View>
						<View style={[ p.center, { width: xs(220) }]}>
							<Text style={[ c.light, f._20, { fontFamily: 'lineto-circular-pro-bold' }]}>Pengaturan</Text>
						</View>
					</View>
				</View>
				<View style={[ b.mt3, { width: '100%', alignItems: 'center' }]}>
					<TouchableOpacity onPress={() => this.props.navigation.push('GabungDriver')} style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
						<View style={{ width: wp('10%') }}>
							<Image source={require('../asset/driver_icon.png')} style={{ width: wp('7%'), height: wp('7%'), tintColor: blue }} />
						</View>
						<View style={{ width: wp('70%') }}>
							<Text style={[ f._16, b.ml3, b.mt1, c.light ]}>Gabung jadi driver</Text>
						</View>
						<View style={{ width: wp('10%'), alignItems: 'flex-end' }}>
							<Icon name="ios-arrow-forward" size={25} color={light} />
						</View>
					</TouchableOpacity>
				</View>
				<View style={[ b.mt3, { borderBottomWidth: 1, borderBottomColor: '#999999' }]} />
				<View style={[ b.mt3, { width: '100%', alignItems: 'center' }]}>
					<TouchableOpacity onPress={() => this.props.navigation.push('GabungPenjual')} style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
						<View style={{ width: wp('10%') }}>
							<Image source={require('../asset/apotek_icon.png')} style={{ width: wp('6.5%'), height: wp('6.5%'), tintColor: blue }} />
						</View>
						<View style={{ width: wp('70%') }}>
							<Text style={[ f._16, b.ml3, b.mt1, c.light ]}>Gabung jadi penjual apotek</Text>
						</View>
						<View style={{ width: wp('10%'), alignItems: 'flex-end' }}>
							<Icon name="ios-arrow-forward" size={25} color={light} />
						</View>
					</TouchableOpacity>
				</View>
				<View style={[ b.mt3, { borderBottomWidth: 1, borderBottomColor: '#999999' }]} />
				<View style={[ b.mt3, { width: '100%', alignItems: 'center' }]}>
					<TouchableOpacity onPress={() => this.props.navigation.push('Terms')} style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
						<View style={{ width: wp('10%') }}>
							<Svg icon="terms" size={30} />
						</View>
						<View style={{ width: wp('70%') }}>
							<Text style={[ f._16, b.ml3, b.mt1, c.light ]}>Syarat & Ketentuan</Text>
						</View>
						<View style={{ width: wp('10%'), alignItems: 'flex-end' }}>
							<Icon name="ios-arrow-forward" size={25} color={light} />
						</View>
					</TouchableOpacity>
				</View>
				<View style={[ b.mt3, { borderBottomWidth: 1, borderBottomColor: '#999999' }]} />
				<View style={[ b.mt3, { width: '100%', alignItems: 'center' }]}>
					<TouchableOpacity onPress={() => this.props.navigation.push('FAQ')} style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
						<View style={{ width: wp('10%') }}>
							<Svg icon="faq" size={30} />
						</View>
						<View style={{ width: wp('70%') }}>
							<Text style={[ f._16, b.ml3, b.mt1, c.light ]}>FAQ</Text>
						</View>
						<View style={{ width: wp('10%'), alignItems: 'flex-end' }}>
							<Icon name="ios-arrow-forward" size={25} color={light} />
						</View>
					</TouchableOpacity>
				</View>
				<View style={[ b.mt3, { borderBottomWidth: 1, borderBottomColor: '#999999' }]} />
				<View style={[ b.mt3, { width: '100%', alignItems: 'center' }]}>
					<TouchableOpacity onPress={() => this.props.navigation.push('Privacy')} style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
						<View style={{ width: wp('10%') }}>
							<Svg icon="privacy" size={30} />
						</View>
						<View style={{ width: wp('70%') }}>
							<Text style={[ f._16, b.ml3, b.mt1, c.light ]}>Kebijakan Privasi</Text>
						</View>
						<View style={{ width: wp('10%'), alignItems: 'flex-end' }}>
							<Icon name="ios-arrow-forward" size={25} color={light} />
						</View>
					</TouchableOpacity>
				</View>
				<View style={[ b.mt3, { borderBottomWidth: 1, borderBottomColor: '#999999' }]} />
				<View style={[ b.mt3, { width: '100%', alignItems: 'center' }]}>
					<TouchableOpacity onPress={() => this.props.navigation.push('About')} style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
						<View style={{ width: wp('10%') }}>
							<Svg icon="aboutus" size={30} />
						</View>
						<View style={{ width: wp('70%') }}>
							<Text style={[ f._16, b.ml3, b.mt1, c.light ]}>Tentang Kami</Text>
						</View>
						<View style={{ width: wp('10%'), alignItems: 'flex-end' }}>
							<Icon name="ios-arrow-forward" size={25} color={light} />
						</View>
					</TouchableOpacity>
				</View>
				<View style={[ b.mt3, { borderBottomWidth: 1, borderBottomColor: '#999999' }]} />
				<View style={[ b.mt3, { width: '100%', alignItems: 'center' }]}>
					<TouchableOpacity onPress={ ()=>{ Linking.openURL('https://api.whatsapp.com/send?phone=6281297674927')}} style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
						<View style={{ width: wp('10%') }}>
							<Svg icon="contactus" size={30} />
						</View>
						<View style={{ width: wp('70%') }}>
							<Text style={[ f._16, b.ml3, b.mt1, c.light ]}>Hubungi Kami</Text>
						</View>
						<View style={{ width: wp('10%'), alignItems: 'flex-end' }}>
							<Icon name="ios-arrow-forward" size={25} color={light} />
						</View>
					</TouchableOpacity>
				</View>
				<View style={[ b.mt3, { borderBottomWidth: 1, borderBottomColor: '#999999' }]} />
				<View style={[ b.mt3, { width: '100%', alignItems: 'center' }]}>
					<TouchableOpacity onPress={ () => Share.open(options)} style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
						<View style={{ width: wp('10%') }}>
							<Icon name="md-share" size={30} color={blue} />
						</View>
						<View style={{ width: wp('70%') }}>
							<Text style={[ f._16, b.ml3, b.mt1, c.light ]}>Share</Text>
						</View>
						<View style={{ width: wp('10%'), alignItems: 'flex-end' }}>
							<Icon name="ios-arrow-forward" size={25} color={light} />
						</View>
					</TouchableOpacity>
				</View>
				<View style={[ b.mt3, { borderBottomWidth: 1, borderBottomColor: '#999999' }]} />
			</ScrollView>
		)
	}
}