import React from 'react'
import { Text, View, I18nManager, ScrollView, TouchableOpacity, Image, ImageBackground } from 'react-native'
import { b, p, f, c } from '../utils/StyleHelper';
import { vs, xs } from '../utils/Responsive';
import { blue, light, grey, softBlue } from '../utils/Color';
import Svg from '../utils/Svg'


export default class Terms extends React.Component {

	constructor(props){
		super(props);
	}

	render() {
		return (
			<>
			<View style={{ width: '100%', height: xs(100), backgroundColor: '#000' }}>
				<View style={[ p.row, b.ml4, b.mt4 ]}>
					<TouchableOpacity onPress={() => this.props.navigation.navigate("Drawer")}>
						<Image source={require('../asset/back.png')} style={[ b.mt2, { width: xs(30), height: xs(30) }]} />
					</TouchableOpacity>
					<View style={[ b.ml1, { width: xs(250) }]}>
						<Text style={[ c.light, f._16, b.mt1, p.textCenter, { fontFamily: 'lineto-circular-pro-bold' }]}> SYARAT DAN KETENTUAN PENGGUNAAN </Text>
					</View>
				</View>
			</View>
			<ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#181B22' }}>
				<View style={[ p.center, b.mt4 ]}>
					<View style={{ width: xs(340) }}>
						<View style={[ p.row ]}>
							<Text style={[ c.light ]}>
								Situs
							</Text>
							<TouchableOpacity>
								<Text style={[ c.blue, b.ml1 ]}>
									(www.healthplus.co)
								</Text>
							</TouchableOpacity>
							<Text style={[ b.ml1, c.light ]}>
								dan Aplikasi
							</Text>
						</View>
						<Text style={[ c.light ]}>
							Seluler (HealthPLus) dimiliki dan dioperasikan oleh
						</Text>
						<Text style={[ c.light ]}>
							Health Connect Pte Ltd. Ketentuan berikut ini mengatur penggunaan dan / atau akses Situs dan Aplikasi Seluler Anda.
						</Text>

						<Text style={[ b.mt2, c.light ]}>
							Dengan menggunakan atau mengakses Situs atau Aplikasi Seluler, Anda dianggap telah menerima dan menyetujui untuk terikat oleh Ketentuan ini. Adalah tanggung jawab Anda untuk memastikan bahwa Anda telah membaca dan memahami Ketentuan ini dan segala risiko, kewajiban, dan tanggung jawab yang menyertainya.
						</Text>

						<Text style={[ b.mt2, c.light ]}> 
							Harap baca ketentuan penggunaan ini dengan cermat sebelum Anda mulai menggunakan dan / atau mengakses Situs atau Aplikasi Seluler. Jika Anda tidak menyetujui bagian apa pun dari Ketentuan ini, harap jangan menggunakan dan / atau mengakses Situs atau Aplikasi Seluler.
						</Text>

						<Text style={[ b.mt2, c.light ]}>
							1. DEFINISI DAN INTERPRETASI
						</Text>
						<Text style={[ b.mt2, c.light ]}>
							1.1 Definisi
						</Text>

						<Text style={[ b.mt2, c.light ]}> 
							Persetujuan untuk penggunaan data
						</Text>
						<Text style={[ b.mt2, c.light ]}> 
							'Ketentuan': Syarat dan ketentuan yang terkandung di sini dan setiap perubahan dari waktu ke waktu, sebagaimana dipublikasikan di Situs.
						</Text>
						<Text style={[ b.mt2, c.light ]}> 
							'Data Pribadi': Data, apakah benar atau tidak, yang dapat digunakan untuk mengidentifikasi, menghubungi atau menemukan Anda. Data Pribadi dapat menyertakan nama, alamat email, alamat penagihan, alamat pengiriman, nomor telepon, dan informasi kartu kredit Anda.
						</Text>
						<Text style={[ b.mt2, c.light ]}> 
							'HealthConnect', 'HealthPLus', 'we', 'our' dan 'us': HealthConnect Pte Ltd
						</Text>
						<Text style={[ b.mt2, c.light ]}> 
							'Situs': Baik versi seluler dan web dari situs web yang berlokasi di www.healthplus.co
						</Text>
						<Text style={[ b.mt2, c.light ]}>
							'Aplikasi Seluler': Aplikasi Seluler HealthPlus tersedia di Google Playstore dan Apple App Store
						</Text>

						<Text style={[ b.mt2, c.light ]}> 
							'Anda' atau 'milik Anda': Orang yang berusia 18 tahun atau lebih, atau di bawah pengawasan orang tua atau wali yang sah.
						</Text>
						<Text style={[ b.mt2, c.light ]}> 
							Interpretasi
						</Text>
						<Text style={[ b.mt2, c.light ]}> 
							(a) Judul dalam Ketentuan ini dimasukkan hanya untuk memudahkan dan tidak akan memengaruhi penafsiran Ketentuan ini.
						</Text>
						<Text style={[ b.mt2, c.light ]}> 
							(B) Kecuali jika konteksnya menentukan, kata-kata yang mengimpor singular harus mencakup jamak dan sebaliknya dan kata-kata yang mengimpor jenis kelamin tertentu harus mencakup jenis kelamin lainnya (pria, wanita atau netral).
						</Text>
						<Text style={[ b.mt2, c.light ]}> 
							(C) Setiap referensi untuk melakukan termasuk tanpa batasan, kelalaian, pernyataan atau melakukan, baik secara tertulis atau tidak.
						</Text>
						<Text style={[ b.mt2, c.light ]}> 
							AKSES DAN PENGGUNAAN SITUS
						</Text>
						<Text style={[ b.mt2, c.light ]}> 
							Ketentuan Penggunaan. Dengan menggunakan dan / atau mengakses Situs dan / atau Aplikasi Seluler, Anda dengan ini setuju bahwa:
						</Text>
						<Text style={[ b.mt2, c.light ]}> 
							(a) jika Anda berusia di bawah 18 tahun: Anda harus mendapatkan persetujuan dari orang tua Anda atau wali yang sah, penerimaan mereka terhadap Ketentuan ini dan persetujuan mereka untuk bertanggung jawab atas: (i) tindakan Anda; (ii) segala biaya yang terkait dengan penggunaan Anda atas layanan, informasi, dan fungsi apa pun yang tersedia di Situs atau pembelian produk apa pun, barang atau barang dagangan (termasuk bagiannya) yang tersedia untuk dijual di Situs atau Aplikasi Seluler; dan (iii) penerimaan dan kepatuhan Anda terhadap Ketentuan ini. Jika Anda tidak memiliki izin dari orang tua atau wali Anda, Anda harus berhenti menggunakan / mengakses Situs dan / atau Aplikasi Seluler;
						</Text>
						<Text style={[ b.mt2, c.light ]}> 
							(b) Anda tidak akan menyalin atau mendistribusikan bagian apa pun dari Situs dan / atau Aplikasi Seluler dalam media apa pun tanpa izin tertulis sebelumnya dari kami; dan
						</Text>
						<Text style={[ b.mt2, c.light ]}> 
							(c) Anda tidak akan menggunakan Situs dan / atau Aplikasi Seluler untuk tujuan apa pun yang melanggar hukum atau dilarang oleh Ketentuan ini, atau untuk meminta kinerja dari setiap aktivitas ilegal atau aktivitas lain yang melanggar hak kami atau hak orang lain. Tanpa mengabaikan hak atau batasan lain dalam Ketentuan ini, Anda tidak boleh menggunakan Situs dan / atau Aplikasi Seluler untuk: (i) mentransmisikan melalui atau melalui Situs dan / atau Aplikasi Seluler informasi, data, teks, gambar, file, tautan, atau perangkat lunak kecuali sehubungan dengan penggunaan yang sah atas Situs ini dan / atau Aplikasi Seluler, (ii) memperkenalkan ke Situs dan / atau Aplikasi Seluler setiap virus, worm, trojan horse dan / atau kode berbahaya, (iii) mendapatkan akses tidak sah ke segala komputer atau sistem ponsel, (iv) berkedok sebagai orang lain atau secara tidak sah menyatakan atau salah menggambarkan afiliasi Anda dengan orang atau entitas mana pun, (v) melanggar privasi atau melanggar hak pribadi atau hak milik (termasuk hak kekayaan intelektual) dari setiap orang atau entitas, (vi) salah menggambarkan identitas pengguna atau menggunakan alamat email palsu, (vii) mengutak-atik atau mendapatkan akses ke Situs dan / atau Aplikasi Seluler, (viii) melakukan kegiatan penipuan, atau (ix) mengumpulkan atau informasi panen rega mencari pengguna lain dari Situs dan / atau Aplikasi Seluler untuk alasan apa pun, termasuk, tanpa batasan, untuk mengirim pengguna semacam itu e-mail komersial yang tidak diminta.
						</Text>
						<Text style={[ b.mt2, c.light ]}> 
							2.2 Tautan ke Situs Pihak Ketiga. Situs dan / atau Aplikasi Seluler dapat berisi tautan ke situs web pihak ketiga yang tidak berafiliasi dengan atau dimiliki, dioperasikan, atau dikendalikan oleh kami, termasuk penyedia sistem pembayaran pihak ketiga. Anda mengakui dan setuju bahwa kami tidak bertanggung jawab atas konten, kebijakan privasi, atau praktik situs web pihak ketiga tersebut atau perusahaan yang memilikinya. Dengan menggunakan Situs dan / atau Aplikasi Seluler, Anda secara tegas membebaskan kami dari setiap dan semua tanggung jawab yang timbul dari akses Anda dan penggunaan situs web pihak ketiga mana pun.
						</Text>
						<Text style={[ b.mt2, c.light ]}> 
							PERUBAHAN SITUS
						</Text>
						<Text style={[ b.mt2, c.light ]}> 
							Kami dapat mengubah, menangguhkan, atau menghentikan Situs ini dan / atau Aplikasi Seluler secara keseluruhan atau sebagian, kapan saja dan dengan alasan apa pun, tanpa pemberitahuan atau biaya. Kami dapat, atas kebijakan kami sendiri, menghentikan atau menangguhkan penggunaan atau akses Anda ke semua atau sebagian Situs dan / atau Aplikasi Seluler untuk alasan apa pun, termasuk namun tidak terbatas pada, pelanggaran terhadap Ketentuan ini. Materi apa pun di Situs dan / atau Aplikasi Seluler mungkin kedaluwarsa pada waktu tertentu, dan kami tidak berkewajiban memperbarui materi tersebut.
						</Text>
						<Text style={[ b.mt2, c.light ]}> 
							TRANSAKSI TERMASUK MELALUI SITUS
						</Text>
						<Text style={[ b.mt2, c.light ]}> 
							Kontrak untuk penyediaan barang, layanan atau produk (termasuk tiket, pass, kredensial atau dokumen lain atau otorisasi umum atau khusus yang diberikan oleh Health Connect Pte Ltd) yang dibentuk melalui Situs dan / atau Aplikasi Seluler atau sebagai hasil dari kunjungan yang dilakukan oleh Anda ke Situs dan / atau Aplikasi Seluler diatur oleh syarat dan ketentuan pasokan tertentu untuk barang, layanan, atau produk tersebut, sebagaimana disoroti kepada Anda sebelum kontrak apa pun dibuat.
						</Text>
						<Text style={[ b.mt2, c.light ]}> 
							BATASAN TANGGUNG JAWAB
						</Text>
						<Text style={[ b.mt2, c.light ]}> 
							Tidak Ada Representasi atau Jaminan. Situs dan / atau Aplikasi Seluler, kontennya, layanannya dan semua teks, gambar, barang dagangan dan informasi lain yang disediakan di sini disediakan atas dasar 'sebagaimana adanya' dan 'sebagaimana tersedia' tanpa representasi atau jaminan dalam bentuk apa pun, baik tersurat, tersirat maupun tersirat. atau menurut undang-undang, termasuk tetapi tidak terbatas pada jaminan hak milik atau jaminan tersirat dapat diperjualbelikan, kualitas memuaskan, kesesuaian untuk tujuan tertentu atau non-pelanggaran. Tanpa batasan pada sifat umum dari yang disebutkan di atas, kami dengan tegas menyangkal segala jaminan, ketentuan, jaminan, syarat atau representasi (a) sehubungan dengan keandalan, keakuratan, kelengkapan, dan validitas konten atau materi apa pun di Situs dan / atau Aplikasi Seluler, (B) bahwa fungsi yang terkandung di Situs dan / atau Aplikasi Mobile akan aman, tidak terganggu atau bebas dari kesalahan, (c) bahwa setiap cacat akan diperbaiki, atau (d) bahwa Situs dan / atau Aplikasi Mobile atau server (s) yang membuatnya tersedia bebas dari virus atau komponen berbahaya lainnya. Setiap dan semua jaminan, ketentuan, ketentuan, dan representasi tersebut dikecualikan secara khusus. Kami tidak bertanggung jawab atas kesalahan atau kelalaian dalam materi di Situs dan / atau Aplikasi Seluler, termasuk ketidakakuratan faktual atau lainnya atau kesalahan ketik. Anda secara tegas setuju bahwa penggunaan dan / atau akses Anda terhadap Situs dan / atau Aplikasi Seluler adalah risiko Anda sendiri.
						</Text>
						<Text style={[ b.mt2, c.light ]}> 
							Tidak Ada Pertanggungjawaban atas Kerugian Tidak Langsung atau Konsekuensi. Sejauh diizinkan oleh hukum yang berlaku, kami tidak akan bertanggung jawab kepada pengguna apa pun dari Situs dan / atau Aplikasi Seluler atau orang lain apa pun atas kehilangan atau kerusakan langsung, tidak langsung, khusus atau konsekuensial (termasuk, namun tidak terbatas pada, kerusakan untuk kehilangan keuntungan, kehilangan data atau kehilangan penggunaan) yang timbul dari atau terkait dengan penggunaan, ketidakmampuan untuk menggunakan, kinerja atau kegagalan Situs ini dan / atau Aplikasi Seluler atau materi apa pun yang diposting di dalamnya, atau informasi apa pun yang terkandung di dalamnya atau disimpan atau dikelola oleh kami, terlepas dari apakah kerusakan tersebut dapat diprediksi atau timbul dalam kontrak, gugatan, pemerataan, restitusi, berdasarkan undang-undang, berdasarkan common law atau sebaliknya.
						</Text>
						<Text style={[ b.mt2, c.light ]}> 
							Penyedia Layanan Pihak Ketiga. Anda mengakui dan menyetujui bahwa akses Anda dan penggunaan Situs dan / atau Aplikasi Mobile tergantung pada penyedia layanan pihak ketiga seperti internet, jaringan, konektivitas atau penyedia tautan lainnya. Pembayaran Anda untuk setiap transaksi atau kontrak yang Anda selesaikan untuk penyediaan barang atau jasa yang dibentuk melalui Situs dan / atau Aplikasi Seluler diproses oleh penyedia sistem pembayaran pihak ketiga dan kami tidak menyimpan atau memproses informasi pembayaran tersebut. Kami tidak dapat menjamin keamanan sistem pembayaran pihak ketiga tersebut atau data pembayaran apa pun di Situs. Kami tidak bertanggung jawab atas tindakan atau kelalaian pihak ketiga mana pun dan melepaskan segala dan semua tanggung jawab sehubungan dengan tindakan, kelalaian, atau wanprestasi pihak ketiga tersebut. Tanpa mengurangi sifat umum dari hal tersebut di atas, kami tidak bertanggung jawab atas hasil dari penyelidikan kredit, ketersediaan atau kinerja Internet, koneksi Anda ke Internet atau tindakan atau tidak adanya orang atau entitas lain, termasuk layanan internet apa pun pemberi. Dengan menggunakan dan / atau mengakses Situs dan / atau Aplikasi Seluler, Anda secara tegas membebaskan kami dari setiap dan semua tanggung jawab sehubungan dengan tindakan, kelalaian, atau wanprestasi pihak ketiga tersebut.
						</Text>
						<Text style={[ b.mt2, c.light ]}> 
							Kewajiban yang Diimplikasikan oleh Hukum. Untuk menghindari keraguan, tidak ada dalam Ketentuan 5 ini mengecualikan, membatasi atau memodifikasi kondisi, garansi, hak atau kewajiban yang tersirat dalam Ketentuan ini di mana untuk melakukannya adalah ilegal atau akan membuat ketentuan apa pun dari Perjanjian ini batal.
						</Text>
						<Text style={[ b.mt2, c.light ]}> 
							Koreksi Kesalahan. Mungkin ada informasi di Situs dan / atau Aplikasi Seluler yang mengandung kesalahan ketik, ketidakakuratan, atau kelalaian yang mungkin terkait dengan deskripsi produk, harga, promosi, penawaran, dan ketersediaan. Kami berhak untuk memperbaiki kesalahan, ketidakakuratan atau kelalaian dan untuk mengubah atau memperbarui informasi jika ada informasi di Situs dan / atau Aplikasi Mobile yang tidak akurat setiap saat tanpa pemberitahuan sebelumnya.
						</Text>
						<Text style={[ b.mt2, c.light ]}> 
							Ilegalitas dan Keterpisahan. Masing-masing ketentuan dalam Ketentuan ini dapat dipisahkan dari yang lain. Jika ada ketentuan atau bagian daripadanya yang menjadi atau tidak valid, tidak dapat diberlakukan atau ilegal dalam hal apa pun, ketentuan atau bagian daripadanya akan, sejauh istilah tersebut tidak sah, dianggap tidak membentuk bagian dari Ketentuan ini tetapi validitas, keberlakuan atau legalitas dari ketentuan yang tersisa di bawah ini tidak akan dengan cara apa pun dipengaruhi atau dirusak karenanya.
						</Text>
						<Text style={[ b.mt2, c.light ]}> 
							Hukum yang Mengatur. Ketentuan ini akan diatur oleh, dan ditafsirkan sesuai dengan, hukum Singapura, dan Anda dengan ini tunduk pada yurisdiksi eksklusif pengadilan Singapura. 8.5 Data Pribadi. Dengan menggunakan dan / atau mengakses Situs dan / atau Aplikasi Seluler, Anda mengakui bahwa Anda telah membaca dan menyetujui Kebijakan Privasi kami di http://www.healthplus.co/privacy-policy, yang merupakan bagian dari Ketentuan ini, dan Anda menyetujui pengumpulan, penggunaan, dan / atau pengungkapan atau penanganan Data Pribadi Anda untuk tujuan yang ditetapkan dalam Kebijakan Privasi.
						</Text>
						<View style={{ height: xs(30) }} />
					</View>
				</View>
			</ScrollView>
			</>
		)
	}
}