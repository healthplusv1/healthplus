import React from 'react'
import { Text, View, Image, ImageBackground, ScrollView, TouchableOpacity } from 'react-native'
import { b, p, f, c } from '../utils/StyleHelper';
import { vs, xs } from '../utils/Responsive';
import { blue, light, grey, softBlue } from '../utils/Color';
import Svg from '../utils/Svg'


export default class Privacy extends React.Component {

	constructor(props){
		super(props);
		this.state = {

		}
	}

	render() {
		return (
			<>
			<View style={{ width: '100%', height: xs(100), backgroundColor: '#000' }}>
				<View style={[ p.row, b.ml4, b.mt4 ]}>
					<TouchableOpacity onPress={() => this.props.navigation.navigate("Drawer")}>
						<Image source={require('../asset/back.png')} style={[ b.mt2, { width: xs(30), height: xs(30) }]} />
					</TouchableOpacity>
					<Text style={[ c.light, f._18, b.mt3, { marginLeft: vs(60), fontFamily: 'lineto-circular-pro-bold' }]}> Kebijakan Privasi </Text>
				</View>
			</View>
			<ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#181B22' }}>
				<View style={[ p.center, b.mt4 ]}>
					<View style={{ width: xs(340) }}>
						<Text style={[ c.light ]}>
							Dokumen ini ('DPP') dikeluarkan dengan tunduk dan sesuai dengan Undang-Undang Perlindungan Data Pribadi Singapura (No. 26 tahun 2012) ('Undang-undang') dan menjelaskan bagaimana Health Connect Pte Ltd mengumpulkan, menggunakan, mengungkapkan, dan melindungi informasi ( yang termasuk 'Data Pribadi', yaitu Data apakah benar atau tidak, tentang setiap individu yang dapat diidentifikasi dari data itu atau dari data itu dan informasi lain yang akan kami miliki atau mungkin memiliki akses ke) yang kami pelajari tentang Anda sebagai hasil dari interaksi Anda dengan kami baik melalui situs web (tempat DPP ini diposting) atau dalam hubungannya dengan transaksi offline atau lainnya. Mohon luangkan waktu sejenak untuk membaca DPP ini sehingga Anda mengetahui dan memahami tujuan yang kami kumpulkan, gunakan, dan ungkapkan Data Pribadi Anda.
						</Text>
						<Text style={[ b.mt2, c.light ]}>
							Health Connect Pte Ltd dari waktu ke waktu dapat memperbarui DPP ini untuk memastikan bahwa DPP ini konsisten dengan perkembangan kami di masa depan, tren industri dan / atau setiap perubahan dalam persyaratan hukum atau peraturan. Tunduk pada hak-hak Anda di bidang hukum, Anda setuju untuk terikat oleh ketentuan DPP yang berlaku sebagaimana diperbarui dari waktu ke waktu di situs web kami (http://www.healthplus.co). Silakan periksa kembali secara teratur untuk informasi terbaru tentang penanganan Data Pribadi Anda.
						</Text>

						<Text style={[ b.mt2, c.light ]}>
							1. Data Pribadi
						</Text>
						<Text style={[ b.mt2, c.light ]}>
							1.1 Dalam DPP ini, 'Data Pribadi' mengacu pada data apa pun, apakah benar atau tidak, tentang individu yang dapat diidentifikasi (a) dari data tersebut; atau (b) dari data itu dan informasi lain yang kami miliki atau mungkin memiliki akses, termasuk data dalam catatan kami yang dapat diperbarui dari waktu ke waktu.
						</Text>
						<Text style={[ b.mt2, c.light ]}>
							1.2 Contoh Data Pribadi yang dapat Anda berikan kepada kami termasuk (tergantung pada sifat interaksi Anda dengan kami) nama Anda, nomor NRIC, paspor atau nomor identifikasi lainnya, nomor telepon, alamat surat, alamat email dan informasi lainnya berkaitan dengan individu yang telah Anda berikan kepada kami, dalam bentuk apa pun yang Anda kirimkan kepada kami, atau melalui bentuk interaksi lainnya dengan Anda.
						</Text>

						<Text style={[ b.mt2, c.light ]}>
							2. Penugmpulan Data Pribadi
						</Text>
						<Text style={[ b.mt2, c.light ]}>
							2.1 Secara umum, kami mengumpulkan Data Pribadi ketika Anda menggunakan Situs atau Aplikasi Seluler kami atau ketika Anda menghubungi kami.
						</Text>
						<Text style={[ b.mt2, c.light ]}>
							2.2 Jika Anda memberikan kepada kami dengan Data Pribadi apa pun yang berkaitan dengan pihak ketiga (misalnya, informasi tentang tanggungan Anda, pasangan, anak-anak dan / atau orang tua Anda), dengan mengirimkan informasi tersebut kepada kami, Anda menyatakan kepada kami bahwa Anda telah memperoleh izin dari pihak ketiga tersebut. pihak Anda memberi kami Data Pribadi mereka untuk tujuan masing-masing.
						</Text>
						<Text style={[ b.mt2, c.light ]}>
							2.3 Anda harus memastikan bahwa semua Data Pribadi yang dikirimkan kepada kami lengkap, akurat, benar, dan benar. Kegagalan Anda untuk melakukannya dapat mengakibatkan ketidakmampuan kami untuk memenuhi permintaan dan / atau aplikasi Anda.
						</Text>

						<Text style={[ b.mt2, c.light ]}>
							3. Informasi Apa Yang Kami Kumpulkan?
						</Text>
						<Text style={[ b.mt2, c.light ]}>
							3.1 Kami akan mengambil langkah-langkah yang wajar untuk memastikan bahwa pengumpulan jenis informasi lainnya hanya akan dilakukan jika Anda secara sukarela memberikan ini kepada kami, dan jika pengumpulan tersebut masuk akal dan tidak berlebihan untuk keperluan pengumpulan tersebut.
						</Text>
						<Text style={[ b.mt2, c.light ]}>
							3.2 Kami dapat, misalnya, mengumpulkan dan menyimpan catatan nama Anda, alamat surat, alamat email, nomor telepon, jenis kelamin dan preferensi, perincian kartu kredit (dan informasi pembayaran terkait lainnya), dan informasi lain yang mungkin terkait dengan Pribadi Anda Data.
						</Text>
						<Text style={[ b.mt2, c.light ]}>
							3.3 Kami tidak secara sadar mengumpulkan, menggunakan, atau mengungkapkan informasi yang dapat diidentifikasi secara pribadi dari orang yang berusia di bawah 14 tahun. Tidak ada Data Pribadi yang boleh dikirimkan kepada kami, atau diposting / diedarkan di / melalui Situs atau Aplikasi Mobile kami yang dikelola oleh kami. Jika anak di bawah usia 14 tahun telah memberi kami Data Pribadi tanpa izin orang tua atau wali, orang tua atau wali dapat meminta deskripsi Data Pribadi yang telah dikumpulkan dari atau tentang anak itu dan membatasi atau membatasi pemeliharaan lebih lanjut atau penggunaan informasi seperti itu dari anak itu dan / atau mengarahkan kami untuk menghapus atau berhenti berlangganan mereka.
						</Text>
						<Text style={[ b.mt2, c.light ]}>
							3.4 Jika Anda berusia di bawah 18 tahun, Anda hanya dapat menggunakan situs web dan layanan kami dengan izin dari orang tua dan wali Anda. Kami lebih lanjut mendorong semua orang tua dan wali hukum untuk berpartisipasi dalam atau mengawasi penggunaan Internet oleh anak atau anak-anak mereka dan keterlibatan dalam situs web, dan dalam interaksi mereka dengan kami.
						</Text>
						<Text style={[ b.mt2, c.light ]}>
							3.5 Alamat IP adalah nomor yang secara otomatis ditetapkan untuk komputer Anda ketika Anda mendaftar dengan Penyedia Layanan Internet. Ketika Anda mengunjungi Situs kami, alamat IP Anda secara otomatis login di server kami. Kami menggunakan alamat IP Anda untuk membantu mendiagnosis masalah dengan server kami, dan untuk mengelola situs web kami. Dari alamat IP Anda, kami dapat mengidentifikasi area geografis umum dari mana Anda mengakses situs web kami, namun, kami tidak akan dapat menentukan lokasi geografis yang tepat dari mana Anda mengakses situs web kami. Secara umum kami tidak menautkan alamat IP Anda dengan apa pun yang dapat memungkinkan kami untuk mengidentifikasi Anda kecuali diminta oleh hukum dan peraturan yang berlaku.
						</Text>
						<Text style={[ b.mt2, c.light ]}>
							3.6 Cookie adalah elemen data yang dapat dikirim situs web ke browser Anda, yang kemudian dapat menyimpannya di sistem Anda. Kami menggunakan cookie di beberapa halaman kami untuk menyimpan preferensi pengunjung dan mencatat informasi sesi. Informasi yang kami kumpulkan kemudian digunakan untuk memastikan tingkat layanan yang lebih personal bagi pengguna kami. Anda dapat menyesuaikan pengaturan pada browser Anda sehingga Anda akan diberitahu ketika Anda menerima cookie. Jika Anda ingin menonaktifkan cookie yang terkait dengan teknologi ini, Anda dapat melakukannya dengan mengubah pengaturan pada browser Anda. Silakan lihat dokumentasi browser Anda untuk memeriksa apakah cookie telah diaktifkan di komputer Anda atau untuk meminta agar tidak menerima cookie.
						</Text>
						<Text style={[ b.mt2, c.light ]}>
							3.7 Jika Anda telah memberikan nomor telepon Anda dan telah mengindikasikan bahwa Anda setuju untuk menerima informasi pemasaran atau promosi melalui nomor telepon Anda, maka dari waktu ke waktu, kami dapat menghubungi Anda menggunakan nomor telepon tersebut (termasuk panggilan suara, teks, faks, atau cara lain) dengan informasi tentang produk dan layanan kami (termasuk diskon dan penawaran khusus).
						</Text>

						<Text style={[ b.mt2, c.light ]}>
							4. Persetujuan untuk penggunaan data
						</Text>
						<Text style={[ b.mt2, c.light ]}>
							4.1 Dengan menggunakan Situs dan Aplikasi Seluler, Anda telah memberi kami izin untuk mengumpulkan, menyimpan, dan menggunakan data Anda.
						</Text>
						<View style={{ height: xs(20) }} />
					</View>
				</View>
			</ScrollView>
			</>
		)
	}
}