import React from 'react'
import { Text, View, ScrollView, StyleSheet, I18nManager, Image, ImageBackground, TouchableOpacity } from 'react-native'
import { b, p, f, c } from '../utils/StyleHelper';
import { vs, xs } from '../utils/Responsive';
import { blue, light, grey, softBlue } from '../utils/Color';
import DropDownItem from "react-native-drop-down-item";


const IC_ARR_DOWN = require('../asset/chevron_down.png');
const IC_ARR_UP = require('../asset/chevron_up.png');

export default class FAQ extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			contents: [
			    {
			        
			    },
			]
		};
	}

	render() {
		return (
			<View style={{ backgroundColor: '#181B22', width: '100%', height: '100%' }}>
				<View style={[ p.row, { width: '100%', height: xs(100), backgroundColor: '#000' }]}>
					<TouchableOpacity onPress={() => this.props.navigation.navigate("Drawer")}>
						<Image source={require('../asset/back.png')} style={[ b.ml4, { marginTop: vs(30), width: xs(30), height: xs(30) }]} />
					</TouchableOpacity>
					<Text style={[ c.light, f._20, { marginTop: vs(30), marginLeft: vs(100), fontFamily: 'lineto-circular-pro-bold' }]}> FAQ </Text>
				</View>
			  <ScrollView showsVerticalScrollIndicator={false} style={{ alignSelf: 'stretch', height: xs(550), backgroundColor: '#181B22' }}>
			    {
			      this.state.contents
			        ? this.state.contents.map((param, i) => {
			          return (
			            <DropDownItem
			              key={i}
			              style={[styles.dropDownItem, b.mt4]}
			              contentVisible={false}
			              invisibleImage={IC_ARR_DOWN}
			              visibleImage={IC_ARR_UP}
			              header={
			                <View style={{ width: xs(320) }}>
			                  <Text style={[ c.light, {
			                    fontSize: 20,
			                    marginLeft: vs(10),
			                  }]}>
			                  	Apa itu HealthPlus?
			                  </Text>
			                </View>
			              }
			            >
			              <Text style={[ c.light,
			                styles.txt,
			                {
			                  fontSize: 20,
			                  marginLeft: xs(5)
			                }
			              ]}>
			                HealthPlus adalah aplikasi yang menyerukan pentingnya gaya hidup sehat terintegrasi yang dapat menghasilkan poin yang dapat ditukar dengan berbagai hadiah menarik.
							Poin HealthPlus didapat dengan tetap tinggal di rumah, saat mendaftar pengguna akan menentukan lokasi rumahnya melalui GPS dan poin yang terkumpul dapat ditukar dengan voucher diskon, materi e-learning, game, dan menonton film & drama, produk, dan hadiah menarik lainnya.
			              </Text>
			            </DropDownItem>
			          );
			        })
			        : null
			    }
			    <View style={{ height: xs(10) }} />

			    {
			      this.state.contents
			        ? this.state.contents.map((param, i) => {
			          return (
			            <DropDownItem
			              key={i}
			              style={styles.dropDownItem}
			              contentVisible={false}
			              invisibleImage={IC_ARR_DOWN}
			              visibleImage={IC_ARR_UP}
			              header={
			                <View style={{ width: xs(320) }}>
			                  <Text style={[ c.light, {
			                    fontSize: 20,
			                    marginLeft: vs(10),
			                    
			                  }]}>
			                  	Bagaimana cara Kerja HealthPlus
			                  </Text>
			                </View>
			              }
			            >
			              <Text style={[ c.light,
			                styles.txt,
			                {
			                  fontSize: 20,
			                  marginLeft: xs(5),
			                }
			              ]}>
			                1. Ketika pengguna mengunduh HealthPlus, aplikasi akan meminta persetujuan lokasi.
			              </Text>
			              <Text style={[ c.light,
			                styles.txt,
			                {
			                  fontSize: 20,
			                  marginLeft: xs(5),
			                  marginTop: vs(10)
			                }
			              ]}>
			                2. Dengan ijin lokasi aktif, aplikasi akan secara berkala mengidentifikasi lokasi pengguna.
			              </Text>
			              <Text style={[ c.light,
			                styles.txt,
			                {
			                  fontSize: 20,
			                  marginLeft: xs(5),
			                  marginTop: vs(10)
			                }
			              ]}>
			                3. Untuk mendapatkan poin, Anda akan diminta untuk tinggal di lokasi rumah yang telah ditentukan pada saat pendaftaran selama mungkin.
			              </Text>
			            </DropDownItem>
			          );
			        })
			        : null
			    }
			    <View style={{ height: xs(10) }} />

			    {
			      this.state.contents
			        ? this.state.contents.map((param, i) => {
			          return (
			            <DropDownItem
			              key={i}
			              style={[ styles.dropDownItem, b.pb4 ]}
			              contentVisible={false}
			              invisibleImage={IC_ARR_DOWN}
			              visibleImage={IC_ARR_UP}
			              header={
			                <View  style={{ width: xs(320) }}>
			                  <Text style={[ c.light, {
			                    fontSize: 20,
			                    marginLeft: vs(10),
			                    
			                  }]}>
			                  	Bagaimana cara mengklaim hadiah?
			                  </Text>
			                </View>
			              }
			            >
			              <Text style={[ c.light,
			                styles.txt,
			                {
			                  fontSize: 20,
			                  marginLeft: xs(5),
			                }
			              ]}>
			                1. Scroll layar ke bawah dari klik ikon "KLAIM";
			              </Text>
			              <Text style={[ c.light,
			                styles.txt,
			                {
			                  fontSize: 20,
			                  marginLeft: xs(5),
			                  marginTop: vs(10)
			                }
			              ]}>
			                2. Pilih kategori hadiah yang Anda inginkan;
			              </Text>
			              <Text style={[ c.light,
			                styles.txt,
			                {
			                  fontSize: 20,
			                  marginLeft: xs(5),
			                  marginTop: vs(10)
			                }
			              ]}>
			                3. Pilih item yang Anda inginkan dan pastikan Anda memiliki cukup poin untuk item yang ingin ditukar;
			              </Text>
			              <Text style={[ c.light,
			                styles.txt,
			                {
			                  fontSize: 20,
			                  marginLeft: xs(5),
			                  marginTop: vs(10)
			                }
			              ]}>
			                4. Baca dan pahami setiap deskripsi dalam setiap item. setelah yakin, klik tombol KLAIM. Jika Anda memiliki cukup poin, proses klaim akan berhasil;
			              </Text>
			              <Text style={[ c.light,
			                styles.txt,
			                {
			                  fontSize: 20,
			                  marginLeft: xs(5),
			                  marginTop: vs(10)
			                }
			              ]}>
			                5. Seluruh item yang telah berhasil diklaim akan terkumpul di menu "HADIAH SAYA";
			              </Text>
			              <Text style={[ c.light,
			                styles.txt,
			                {
			                  fontSize: 20,
			                  marginLeft: xs(5),
			                  marginTop: vs(10)
			                }
			              ]}>
			                6. Klik item yang dipilih untuk  menampilkan E-Voucher, dan tunjukkan ke kasir merchant kami untuk dapat diproses atau Anda juga dapat menyalin kode voucher untuk digunakan pada merchant kami yang berupa marketplace atau website.
			              </Text>
			            </DropDownItem>
			          );
			        })
			        : null
			    }
			    <View style={{ height: xs(10) }} />
			  </ScrollView>
			</View>
		)
	}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    paddingTop: 60,
  },
  header: {
    width: '100%',
    paddingVertical: 8,
    paddingHorizontal: 12,
    flexWrap: 'wrap',
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerTxt: {
    fontSize: 12,
    color: 'rgb(74,74,74)',
    marginRight: 60,
    flexWrap: 'wrap',
  },
  txt: {
    fontSize: 14,
  },
});