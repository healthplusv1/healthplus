import React from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import { b, p, f, c } from '../utils/StyleHelper'
import { vs, xs } from '../utils/Responsive'
import { blue, light, grey, softBlue } from '../utils/Color'
import WebView from 'react-native-webview'

export default class About extends React.Component {
	render() {
		return (
			<>
				<View>
					<View style={{ height: xs(50) }}>
						<TouchableOpacity onPress={() => this.props.navigation.pop()} style={[ p.textRight, b.mt2, { marginLeft: vs(260) }]}>
							<Text style={[ f.bold, f._20 ]}> Batal </Text>
						</TouchableOpacity>
					</View>
				</View>
				<WebView
			        source={{uri: 'https://healthplus.co/'}}
			    />
			</>
		)
	}
}