import React, { Component } from 'react'
import { Text, View, TextInput, TouchableOpacity } from 'react-native'
import { blue, light } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import AsyncStorage from '@react-native-community/async-storage'

export default class GabungDriver extends Component {
    constructor(props) {
        super(props);
        this.state = {
            no_kendaraan: '',
            nama_kendaraan: ''
        }
    }

    render() {
        return (
            <FastImage source={require('../asset/back_sign.png')} style={{ width: '100%', height: '100%', alignItems: 'center' }}>
                <View style={{ width: wp('80%'), marginTop: wp('45%') }}>
                    <Text style={[ f.bold, f._20 ]}>Informasi Kendaraan</Text>
                </View>
                <View style={[ b.shadowHigh, { width: wp('80%'), height: hp('22%'), marginTop: wp('4%'), borderRadius: wp('5%'), backgroundColor: light, alignItems: 'center', justifyContent: 'center' }]}>
                    <TextInput
                        style={{ width: wp('78%'), height: hp('5.5%'), borderBottomWidth: 1, borderBottomColor: '#CCC', marginTop: wp('-4%') }}
                        placeholder='Nomor Kendaraan'
                        placeholderTextColor='#CCC'
                        onChangeText={(text) => this.setState({ no_kendaraan: text })}
                        value={this.state.no_kendaraan}
                    />
                    <TextInput
                        style={{ width: wp('78%'), height: hp('5.5%'), borderBottomWidth: 1, borderBottomColor: '#CCC', marginTop: wp('4%') }}
                        placeholder='Nama Kendaraan'
                        placeholderTextColor='#CCC'
                        onChangeText={(text) => this.setState({ nama_kendaraan: text })}
                        value={this.state.nama_kendaraan}
                    />
                </View>

                <TouchableOpacity onPress={() => this.props.navigation.push('SIM')} style={{ width: wp('85%'), height: hp('7.5%'), backgroundColor: blue, borderRadius: wp('2%'), position: 'absolute', bottom: wp('6%'), alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={[ c.light, f.bold, f._18 ]}>Konfirmasi</Text>
                </TouchableOpacity>
            </FastImage>
        )
    }
}
