export const primary = "#4c7dfc"
export const btnPrimary = {
    backgroundColor: primary,
    color: "#fff"
}

export const secondary = "#898989"
export const btnSecondary = {
    backgroundColor: secondary,
    color: "#fff"
}

export const success = "#0cad71"
export const btnSuccess = {
    backgroundColor: success,
    color: "#fff"
}

export const warning = "#ec971f"
export const btnWarning = {
    backgroundColor: warning,
    color: "#fff"
}

export const danger = "rgb(253, 87, 118)"
export const btnDanger = {
    backgroundColor: danger,
    color: "#fff"
}
export const blue = "#4E7ED6" // use
export const blueLight = "#e0e8eb"
export const cloudyBlue = "#707faa"
export const coral = "#272727"
export const dark = "#192340"
export const darker = "#404040"
export const disabled = "#a8a8a8"
export const darkBlue = '#192341'
export const darkerBlue = "#141516"
export const softBlue = "#C8DDF0" //use
export const orange = "#fd743e"
export const softOrange = "#ff7e00"
export const cloudyOrange = "#ffebdd"
export const magenta = "#4176fa"
export const grey = "#666666" //use
export const softGrey = "#999999"
export const paleGrey = "#f4f9ff"
export const windowsBlue = "#346cf9"
export const light = "#fff" //use
export const red = "#e52f53"