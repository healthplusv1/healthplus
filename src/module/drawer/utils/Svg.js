import React, { memo } from 'react';
import { ms } from '../utils/Responsive';
import Terms from '../asset/icons/terms.svg'
import FAQ from '../asset/icons/faq.svg'
import Privacy from '../asset/icons/privacy.svg'
import AboutUs from '../asset/icons/aboutus.svg'
import ContactUs from '../asset/icons/contactus.svg'
import Chevron from '../asset/icons/chevron.svg'
import Whatsapp from '../asset/icons/whatsapp.svg'
import Facebook from '../asset/icons/facebook.svg'
import Instagram from '../asset/icons/instagram.svg'

const Svg = ({ icon, size = 30, fill, opacity, ...props }) => {
  const icons = {
    terms: Terms,
    faq: FAQ,
    privacy: Privacy,
    aboutus: AboutUs,
    contactus: ContactUs,
    chevron: Chevron,
    whatsapp: Whatsapp,
    facebook: Facebook,
    instagram: Instagram,
  };

  if (icons[icon] === undefined)
    return (
      <></>
    );

  const Icon = icons[icon]
  return (
    <Icon
      width={ms(size)}
      fill={fill || null}
      height={ms(size)}
      fillOpacity={opacity || 1}
      {...props}
    />
  );
}
export default memo(Svg)
