import React, { Component } from 'react'
import { Text, View, FlatList, TouchableOpacity, ScrollView, RefreshControl, Image } from 'react-native'
import { blue, light } from '../utils/Color'
import { c, f, b, p } from '../utils/StyleHelper'
import { xs, vs } from '../utils/Responsive'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { API_URL } from 'react-native-dotenv'

export default class HistoryPayment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            status: [
				{jenis_status: 'Belum Bayar', image: require('../asset/belumbayar_icon.png')},
				{jenis_status: 'Menuju Apotek', image: require('../asset/menujuapotek_icon.png')},
				{jenis_status: 'Dikirim', image: require('../asset/dikirim_icon.png')},
				{jenis_status: 'Beri Penilaian', image: require('../asset/beripenilaian_icon.png')},
			],
            all_challenges: null,
            clicked: 0,
            pesanan: [
                {'key': '0', 'image': require('../asset/10000289_1_small.png'), 'name': 'OBH Combi Batu dan Flu', 'price': '14500', 'lots': '1', 'status': 'selesai'},
                {'key': '1', 'image': require('../asset/10000289_1_small.png'), 'name': 'OBH Combi Batu dan Flu', 'price': '14500', 'lots': '2', 'status': 'dinilai'},
            ],
            count: '',
            pending: undefined,
            to_the_apotek: undefined,
            delivering: undefined,
            finished: undefined
        }
    }

    componentDidMount() {
        this.setState({ loading: true })

        axios.get(API_URL+'/main/shopping/order/pending_payment_order', {params: {
            id_login: '118205008143923829902'
        }})
        .then(res => {
            // console.log(res.data.data)
            this.setState({
                loading: false,
                pending: res.data.data
            })
        })
        .catch(e => {
            console.log('Pending History => '+e)
        })

        axios.get(API_URL+'/main/shopping/order/to_the_apotek_order', {params: {
            id_login: '118205008143923829902'
        }})
        .then(res => {
            // console.log(res.data.data.length)
            this.setState({
                loading: false,
                to_the_apotek: res.data.data
            })
        })
        .catch(e => {
            console.log('Pending History => '+e)
        })

        axios.get(API_URL+'/main/shopping/order/delivering_order', {params: {
            id_login: '118205008143923829902'
        }})
        .then(res => {
            // console.log(res.data.data)
            this.setState({
                loading: false,
                delivering: res.data.data
            })
        })
        .catch(e => {
            console.log('Pending History => '+e)
        })

        axios.get(API_URL+'/main/shopping/order/finished_order', {params: {
            id_login: '118205008143923829902'
        }})
        .then(res => {
            // console.log(res.data.data)
            this.setState({
                loading: false,
                finished: res.data.data
            })
        })
        .catch(e => {
            console.log('Pending History => '+e)
        })

        axios.get(API_URL+'/main/shopping/cart', {params: {
            id_login: '118205008143923829902'
        }})
        .then(resp => {
            // console.log(resp.data.data.length)
            this.setState({
                loading: false,
                count: resp.data.data.length
            })
        })
        .then(error => {
            console.log('Count Cart => '+error)
        })
    }

    async swicth(jenis_status, index) {
		await this.setState({
            clicked: index,
            loading: false,
            all_challenges: jenis_status
        })
        // console.log(this.state.clicked)
	}

    nextScreen(item) {
        // console.log(item.id)
        this.props.navigation.push('Payment', { id_produk: item.id })
    }

    format(number) {
        if (number) {
          var rupiah = "";
          var numberrev = number
            .toString()
            .split("")
            .reverse()
            .join("");
          for (var i = 0; i < numberrev.length; i++)
            if (i % 3 == 0) rupiah += numberrev.substr(i, 3) + ".";
          return (
            "Rp " +
            rupiah
              .split("", rupiah.length - 1)
              .reverse()
              .join("")
          );
        } else {
            return (
                "Rp "+number
            );
        }
    }

    render() {
        return (
            <View style={{ width: '100%', height: '100%', backgroundColor: '#121212' }}>
                <View style={{ width: '100%', alignItems: 'center', backgroundColor: '#121212' }}>
                    <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('4%'), marginBottom: wp('4%') }}>
                        <View style={{ width: wp('45%') }}>
                            <FastImage source={require('../asset/healthPlus.png')} style={{ width: xs(160), height: xs(38) }} />
                        </View>
                        <View style={{ width: wp('45%'), flexDirection: 'row-reverse' }}>
                            {/* <TouchableOpacity onPress={() => this.props.navigation.push('PharmacyService')}>
                                <ImageBackground source={require('../asset/headset.png')} style={{ width: wp('7%'), height: wp('7%'), alignItems: 'center', justifyContent: 'center' }}>
                                    <Image source={require('../asset/chatBubbleWhite.png')} style={{ width: wp('4%'), height: wp('4%'), transform: [{ rotateY: '180deg' }] }} />
                                </ImageBackground>
                            </TouchableOpacity> */}
                            <TouchableOpacity onPress={() => this.props.navigation.push('Cart')}>
                                {this.state.count !== '' ?
                                    <View style={{ width: wp('3.5%'), height: wp('3.5%'), backgroundColor: 'orange', position: 'absolute', borderRadius: wp('3.5%'), zIndex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                        <Text style={[ c.light, f._8 ]}>{this.state.count}</Text>
                                    </View>
                                    :
                                    null
                                }
                                <Image source={require('../asset/addCart.png')} style={{ width: wp('7%'), height: wp('7%'), tintColor: light }} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.props.navigation.push('HistoryPayment')} style={{ marginRight: wp('2%') }}>
                                <Image source={require('../asset/order_icon.png')} style={{ width: wp('7%'), height: wp('7%'), tintColor: light }} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.props.navigation.push('FrequentlyOrder')} style={{ marginRight: wp('2%') }}>
                                <Image source={require('../asset/Frequentlyorder_icon.png')} style={{ width: wp('8%'), height: wp('7%'), tintColor: light }} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View style={{ width: '100%', alignItems: 'center' }}>
                    <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ width: wp('15%') }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('eCommerce')}>
                                <FastImage source={require('../asset/left.png')} style={{ width: wp('8%'), height: wp('8%') }} />
                            </TouchableOpacity>
                        </View>
                        <View style={[ p.center, { width: wp('60%') }]}>
                            <Text style={[ c.light, f._18, { fontFamily: 'lineto-circular-pro-bold' }]}>Pesanan</Text>
                        </View>
                        <View style={{ width: wp('15%') }}></View>
                    </View>
                    <View style={{ width: '100%', height: wp('22%'), marginTop: wp('4%'), marginBottom: wp('4%'), alignItems: 'center', backgroundColor: '#181B22' }}>
                        <FlatList
                            data={this.state.status}
                            horizontal={true}
                            keyExtractor={item => item.jenis_status}
                            renderItem={({item, index}) => (
                                (this.state.clicked === index ?
                                    <View style={{ width: wp('22.5%'), alignItems: 'center', justifyContent: 'center', backgroundColor: blue }}>
                                        <TouchableOpacity onPress={() => this.swicth(item.jenis_status, index)} style={{ alignItems: 'center' }}>
                                            <Image source={item.image} style={{ width: wp('7%'), height: wp('7%'), tintColor: light }} />
                                            <Text style={[ b.ml2, b.mr2, b.mt1, b.mb1, c.light, f._12, p.textCenter ]}>{item.jenis_status}</Text>
                                        </TouchableOpacity>
                                    </View>
                                    :
                                    <View style={{ width: wp('22.5%'), alignItems: 'center', justifyContent: 'center' }}>
                                        <TouchableOpacity onPress={() => this.swicth(item.jenis_status, index)} style={{ alignItems: 'center' }}>
                                            <Image source={item.image} style={{ width: wp('7%'), height: wp('7%'), tintColor: light }} />
                                            <Text style={[ b.ml2, b.mr2, b.mt1, b.mb1, c.light, f._12, p.textCenter ]}>{item.jenis_status}</Text>
                                        </TouchableOpacity>
                                    </View>
                                )
                            )}
                        />
                    </View>
                </View>
                <ScrollView style={{ backgroundColor: '#121212' }} refreshControl={<RefreshControl onRefresh={() => this.componentDidMount()} refreshing={this.state.loading} />}>
                    {this.state.clicked === 0 && (
                        (this.state.pending !== undefined ?
                            <FlatList
                                data={this.state.pending}
                                keyExtractor={item => item.id}
                                renderItem={({ item }) => (
                                    <View style={{ width: '100%', alignItems: 'center' }}>
                                        <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
                                            <View style={{ width: wp('45%'), flexDirection: 'row', alignItems: 'center' }}>
                                                <View style={[ p.center, { width: wp('10%'), height: wp('10%'), borderRadius: wp('5%'), backgroundColor: light }]}>
                                                    <Image source={require('../asset/logo_apotek.png')} style={{ width: wp('6%'), height: wp('6%') }} />
                                                </View>
                                                <Text style={[ c.light, f._16, { marginLeft: wp('2%') }]}>Apotek Sehati</Text>
                                            </View>
                                            <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                                                <Text style={[ c.light, f._16 ]}>Belum dibayar</Text>
                                            </View>
                                        </View>
                                        <FlatList
                                            data={item.items}
                                            renderItem={({ item }) => (
                                                <>
                                                    <View style={{ width: wp('90%'), flexDirection: 'row', marginTop: wp('4%') }}>
                                                        <View style={{ width: wp('25%') }}>
                                                            <View style={{ width: '100%', height: wp('25%'), backgroundColor: light, borderRadius: wp('2%'), alignItems: 'center', justifyContent: 'center' }}>
                                                                <Image source={require('../asset/logo_combi_small.png')} style={{ width: wp('5%'), height: wp('5%'), position: 'absolute', left: wp('1%'), top: wp('1%'), zIndex: 1 }} />
                                                                <Image source={{ uri: API_URL+'/src/images/'+item.gambar }} style={{ width: wp('16%'), height: wp('16%') }} />
                                                            </View>
                                                        </View>
                                                        <View style={{ width: wp('45%') }}>
                                                            <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold', marginLeft: wp('2%') }]}>{item.nama}</Text>
                                                        </View>
                                                        <View style={{ width: wp('20%'), alignItems: 'flex-end' }}>
                                                            <Text style={[ c.light, { marginTop: wp('15%') }]}>x{item.jumlah}</Text>
                                                            <Text style={[ c.light ]}>{this.format(item.harga * item.jumlah)}</Text>
                                                        </View>
                                                    </View>
                                                    <View style={{ width: '100%', borderBottomWidth: 1, borderBottomColor: light, marginTop: wp('3%') }} />
                                                </>
                                            )}
                                        />

                                        <View style={{ width: wp('90%'), marginTop: wp('2%'), marginBottom: wp('2%'), flexDirection: 'row', alignItems: 'center' }}>
                                            <View style={{ width: wp('40%') }}>
                                                <Text style={[ c.light ]}>{item.items.length} produk</Text>
                                            </View>
                                            <View style={{ width: wp('50%'), alignItems: 'flex-end' }}>
                                                <Text style={[ c.light ]}>Total Pesanan: {this.format(item.total)}</Text>
                                            </View>
                                        </View>
                                        <View style={{ width: wp('90%'), borderBottomWidth: 1, borderBottomColor: light }} />

                                        <View style={{ width: wp('90%'), alignItems: 'flex-end', marginTop: wp('2%') }}>
                                            <TouchableOpacity onPress={() => this.nextScreen(item)} style={{ width: wp('30%'), height: wp('10%'), backgroundColor: blue, alignItems: 'center', justifyContent: 'center', borderRadius: wp('1%') }}>
                                                <Text style={[ c.light ]}>Bayar Sekarang</Text>
                                            </TouchableOpacity>
                                        </View>

                                        <View style={{ width: '100%', borderBottomWidth: 1, borderBottomColor: light, marginTop: wp('2%'), marginBottom: wp('2%') }} />
                                    </View>
                                )}
                            />
                            :
                            <View style={{ width: '100%', alignItems: 'center' }}>
                                <View style={{ width: wp('90%'), marginTop: wp('4%'), marginBottom: wp('4%') }}>
                                    <Text style={[ c.light, { textAlign: 'center' }]}>Anda belum memiliki history pesanan</Text>
                                </View>
                            </View>
                        )
                    )}

                    {this.state.all_challenges === 'Menuju Apotek' && (
                        (this.state.to_the_apotek.length !== 0 ?
                            <FlatList
                                data={this.state.to_the_apotek}
                                keyExtractor={item => item.id}
                                renderItem={({ item }) => (
                                    <View style={{ width: '100%', alignItems: 'center' }}>
                                        <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
                                            <View style={{ width: wp('45%'), flexDirection: 'row', alignItems: 'center' }}>
                                                <View style={[ p.center, { width: wp('10%'), height: wp('10%'), borderRadius: wp('5%'), backgroundColor: light }]}>
                                                    <Image source={require('../asset/logo_apotek.png')} style={{ width: wp('6%'), height: wp('6%') }} />
                                                </View>
                                                <Text style={[ c.light, f._16, { marginLeft: wp('2%') }]}>Apotek Sehati</Text>
                                            </View>
                                            <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                                                <Text style={[ c.light, f._16 ]}>Menuju Apotek</Text>
                                            </View>
                                        </View>
                                        <TouchableOpacity onPress={() => this.props.navigation.push('DetailPayment')}>
                                            <FlatList
                                                data={item.items}
                                                renderItem={({ item }) => (
                                                    <>
                                                        <View style={{ width: wp('90%'), flexDirection: 'row', marginTop: wp('4%') }}>
                                                            <View style={{ width: wp('25%') }}>
                                                                <View style={{ width: '100%', height: wp('25%'), backgroundColor: light, borderRadius: wp('2%'), alignItems: 'center', justifyContent: 'center' }}>
                                                                    <Image source={require('../asset/logo_combi_small.png')} style={{ width: wp('5%'), height: wp('5%'), position: 'absolute', left: wp('1%'), top: wp('1%'), zIndex: 1 }} />
                                                                    <Image source={{ uri: API_URL+'/src/images/'+item.gambar }} style={{ width: wp('16%'), height: wp('16%') }} />
                                                                </View>
                                                            </View>
                                                            <View style={{ width: wp('45%') }}>
                                                                <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold', marginLeft: wp('2%') }]}>{item.nama}</Text>
                                                            </View>
                                                            <View style={{ width: wp('20%'), alignItems: 'flex-end' }}>
                                                                <Text style={[ c.light, { marginTop: wp('15%') }]}>x{item.jumlah}</Text>
                                                                <Text style={[ c.light ]}>{this.format(item.harga * item.jumlah)}</Text>
                                                            </View>
                                                        </View>
                                                        <View style={{ width: '100%', borderBottomWidth: 1, borderBottomColor: light, marginTop: wp('3%') }} />
                                                    </>
                                                )}
                                            />
                                        </TouchableOpacity>

                                        <View style={{ width: wp('90%'), marginTop: wp('2%'), marginBottom: wp('2%'), flexDirection: 'row', alignItems: 'center' }}>
                                            <View style={{ width: wp('40%') }}>
                                                <Text style={[ c.light ]}>{item.items.length} produk</Text>
                                            </View>
                                            <View style={{ width: wp('50%'), alignItems: 'flex-end' }}>
                                                <Text style={[ c.light ]}>Total Pesanan: {this.format(item.total)}</Text>
                                            </View>
                                        </View>
                                        <View style={{ width: wp('90%'), borderBottomWidth: 1, borderBottomColor: light }} />
                                    </View>
                                )}
                            />
                            :
                            <View style={{ width: '100%', alignItems: 'center' }}>
                                <View style={{ width: wp('90%') }}>
                                    <Text style={[ c.light, { textAlign: 'center' }]}>Anda belum memiliki pesanan</Text>
                                </View>
                            </View>
                        )
                    )}

                    {this.state.all_challenges === 'Dikirim' && (
                        (this.state.delivering.length !== 0 ?
                            <FlatList
                                data={this.state.delivering}
                                keyExtractor={item => item.id}
                                renderItem={({ item }) => (
                                    <View style={{ width: '100%', alignItems: 'center' }}>
                                        <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
                                            <View style={{ width: wp('45%'), flexDirection: 'row', alignItems: 'center' }}>
                                                <View style={[ p.center, { width: wp('10%'), height: wp('10%'), borderRadius: wp('5%'), backgroundColor: light }]}>
                                                    <Image source={require('../asset/logo_apotek.png')} style={{ width: wp('6%'), height: wp('6%') }} />
                                                </View>
                                                <Text style={[ c.light, f._16, { marginLeft: wp('2%') }]}>Apotek Sehati</Text>
                                            </View>
                                            <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                                                <Text style={[ c.light, f._16 ]}>Sedang dikirim</Text>
                                            </View>
                                        </View>
                                        <TouchableOpacity onPress={() => this.props.navigation.push('DetailPayment')}>
                                            <FlatList
                                                data={item.items}
                                                renderItem={({ item }) => (
                                                    <>
                                                        <View style={{ width: wp('90%'), flexDirection: 'row', marginTop: wp('4%') }}>
                                                            <View style={{ width: wp('25%') }}>
                                                                <View style={{ width: '100%', height: wp('25%'), backgroundColor: light, borderRadius: wp('2%'), alignItems: 'center', justifyContent: 'center' }}>
                                                                    <Image source={require('../asset/logo_combi_small.png')} style={{ width: wp('5%'), height: wp('5%'), position: 'absolute', left: wp('1%'), top: wp('1%'), zIndex: 1 }} />
                                                                    <Image source={{ uri: API_URL+'/src/images/'+item.gambar }} style={{ width: wp('16%'), height: wp('16%') }} />
                                                                </View>
                                                            </View>
                                                            <View style={{ width: wp('45%') }}>
                                                                <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold', marginLeft: wp('2%') }]}>{item.nama}</Text>
                                                            </View>
                                                            <View style={{ width: wp('20%'), alignItems: 'flex-end' }}>
                                                                <Text style={[ c.light, { marginTop: wp('15%') }]}>x{item.jumlah}</Text>
                                                                <Text style={[ c.light ]}>{this.format(item.harga * item.jumlah)}</Text>
                                                            </View>
                                                        </View>
                                                        <View style={{ width: '100%', borderBottomWidth: 1, borderBottomColor: light, marginTop: wp('3%') }} />
                                                    </>
                                                )}
                                            />
                                        </TouchableOpacity>

                                        <View style={{ width: wp('90%'), marginTop: wp('2%'), marginBottom: wp('2%'), flexDirection: 'row', alignItems: 'center' }}>
                                            <View style={{ width: wp('40%') }}>
                                                <Text style={[ c.light ]}>{item.items.length} produk</Text>
                                            </View>
                                            <View style={{ width: wp('50%'), alignItems: 'flex-end' }}>
                                                <Text style={[ c.light ]}>Total Pesanan: {this.format(item.total)}</Text>
                                            </View>
                                        </View>
                                        <View style={{ width: wp('90%'), borderBottomWidth: 1, borderBottomColor: light }} />
                                    </View>
                                )}
                            />
                            :
                            <View style={{ width: '100%', alignItems: 'center' }}>
                                <View style={{ width: wp('90%') }}>
                                    <Text style={[ c.light, { textAlign: 'center' }]}>Anda belum memiliki pesanan</Text>
                                </View>
                            </View>
                        )
                    )}

                    {this.state.all_challenges === 'Beri Penilaian' && (
                        (this.state.finished.length !== 0 ?
                            <FlatList
                                data={this.state.pesanan}
                                keyExtractor={item => item.key}
                                renderItem={({item}) => (
                                    <View style={{ width: '100%', alignItems: 'center', marginTop: wp('1%') }}>
                                        <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
                                            <View style={{ width: wp('45%'), flexDirection: 'row', alignItems: 'center' }}>
                                                <View style={[ p.center, { width: wp('10%'), height: wp('10%'), borderRadius: wp('5%'), backgroundColor: light }]}>
                                                    <Image source={require('../asset/logo_apotek.png')} style={{ width: wp('6%'), height: wp('6%') }} />
                                                </View>
                                                <Text style={[ c.light, f._16, { marginLeft: wp('2%') }]}>Apotek Sehati</Text>
                                            </View>
                                            <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                                                {item.status === 'selesai' && (
                                                    <Text style={[ c.light, f._16 ]}>Selesai</Text>
                                                )}
                                                {item.status === 'dinilai' && (
                                                    <Text style={[ c.light, f._16 ]}>Telah dinilai</Text>
                                                )}
                                            </View>
                                        </View>
                                        <TouchableOpacity onPress={() => this.props.navigation.push('DetailPayment')} style={{ width: wp('90%'), flexDirection: 'row', marginTop: wp('4%') }}>
                                            <View style={{ width: wp('25%') }}>
                                                <View style={{ width: '100%', height: wp('25%'), backgroundColor: light, borderRadius: wp('2%'), alignItems: 'center', justifyContent: 'center' }}>
                                                    <Image source={require('../asset/logo_combi_small.png')} style={{ width: wp('5%'), height: wp('5%'), position: 'absolute', left: wp('1%'), top: wp('1%'), zIndex: 1 }} />
                                                    <Image source={item.image} style={{ width: wp('16%'), height: wp('16%') }} />
                                                </View>
                                            </View>
                                            <View style={{ width: wp('45%') }}>
                                                <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold', marginLeft: wp('2%') }]}>{item.name}</Text>
                                            </View>
                                            <View style={{ width: wp('20%'), alignItems: 'flex-end' }}>
                                                <Text style={[ c.light, { marginTop: wp('15%') }]}>x{item.lots}</Text>
                                                <Text style={[ c.light ]}>{this.format(item.price)}</Text>
                                            </View>
                                        </TouchableOpacity>
                                        <View style={{ width: '100%', borderBottomWidth: 1, borderBottomColor: light, marginTop: wp('2%') }} />

                                        <View style={{ width: wp('90%'), marginTop: wp('2%'), marginBottom: wp('2%'), flexDirection: 'row', alignItems: 'center' }}>
                                            <View style={{ width: wp('40%') }}>
                                                <Text style={[ c.light ]}>1 produk</Text>
                                            </View>
                                            <View style={{ width: wp('50%'), alignItems: 'flex-end' }}>
                                                <Text style={[ c.light ]}>Total Pesanan: {this.format(item.price)}</Text>
                                            </View>
                                        </View>
                                        <View style={{ width: wp('90%'), borderBottomWidth: 1, borderBottomColor: light }} />

                                        <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('2%'), marginBottom: wp('2%') }}>
                                            <View style={{ width: wp('45%') }}>
                                                {item.status === 'selesai' && (
                                                    <TouchableOpacity onPress={() => this.props.navigation.push('AddReview')}>
                                                        <Text style={[ c.blue ]}>Berikan penilaian</Text>
                                                    </TouchableOpacity>
                                                )}
                                                {item.status === 'dinilai' && (
                                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                        <Image source={require('../asset/star.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: blue }} />
                                                        <Image source={require('../asset/star.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: blue, marginLeft: wp('1%') }} />
                                                        <Image source={require('../asset/star.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: blue, marginLeft: wp('1%') }} />
                                                        <Image source={require('../asset/star.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: blue, marginLeft: wp('1%') }} />
                                                        <Image source={require('../asset/star.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: blue, marginLeft: wp('1%') }} />
                                                    </View>
                                                )}
                                            </View>
                                            <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                                                <TouchableOpacity onPress={() => this.props.navigation.push('Cart')} style={{ width: wp('30%'), height: wp('10%'), backgroundColor: blue, alignItems: 'center', justifyContent: 'center', borderRadius: wp('1%') }}>
                                                    <Text style={[ c.light ]}>Beli Lagi</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>

                                        <View style={{ width: '100%', height: wp('3%'), backgroundColor: '#181B22' }}></View>
                                    </View>
                                )}
                            />
                            :
                            <View style={{ width: '100%', alignItems: 'center' }}>
                                <View style={{ width: wp('90%') }}>
                                    <Text style={[ c.light, { textAlign: 'center' }]}>Anda belum memiliki pesanan</Text>
                                </View>
                            </View>
                        )
                    )}
                </ScrollView>
            </View>
        )
    }
}
