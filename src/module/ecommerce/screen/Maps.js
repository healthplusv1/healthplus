import React, { Component } from 'react'
import { Text, View } from 'react-native'
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps'

export default class Maps extends Component {
    render() {
        return (
            <View style={{ width: '100%', height: '100%' }}>
                <MapView
                    style={{ flex: 1 }}
                    provider={PROVIDER_GOOGLE}
                    initialRegion={{
                        latitude: -6.971251640977067,
						longitude: 110.4267586756306,
						latitudeDelta: 0.000,
						longitudeDelta: 0.000
                    }}
                >
                    <Marker coordinate={{ latitude: -6.971251640977067, longitude: 110.4267586756306 }} />
                </MapView>
            </View>
        )
    }
}
