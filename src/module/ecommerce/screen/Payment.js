import React, { Component } from 'react'
import { Text, View, TouchableOpacity, TextInput, Image, ScrollView, Dimensions, FlatList, InteractionManager, StyleSheet, ActivityIndicator, BackHandler, Alert } from 'react-native'
import { blue, light, softGrey } from '../utils/Color'
import { c, f, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import SlidingUpPanel from 'rn-sliding-up-panel'
import Icon from 'react-native-vector-icons/FontAwesome'
import Dialog, { DialogButton, DialogFooter, DialogTitle, DialogContent } from 'react-native-popup-dialog'
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete'
import SwitchToggle from 'react-native-switch-toggle'
import Geolocation from '@react-native-community/geolocation'
import Geocoder from 'react-native-geocoding'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { API_URL } from 'react-native-dotenv'

const { width, height } = Dimensions.get('window')

export default class Payment extends Component {
    constructor(props) {
        super(props);
        this.inputRef = React.createRef();
        this.state = {
            change: 'Ganti',
            gantiApotek: 'Ganti',
            btn_bayar: 'Bayar',
            status_pengiriman: 'Mengirim ke',
            alamat: 'Mencari lokasi...',
            pesanan: [],
            visible: false,
            visibleFailed: false,
            mapRegion: null,
            latitude: null,
            longitude: null,
            catatan: '',
            switchOn: false,
            loading: false,
            total: '',
            subtotal: '',
            biaya_kirim: ''
        };
        this.backHandler = null;
    }

    async componentDidMount() {
        this.setState({ loading: true })

        axios.get(API_URL+'/main/shopping/cart', {params: {
            id_login: '118205008143923829902'
        }})
        .then(res => {
            this.setState({
                loading: false,
                pesanan: res.data.data
            })
        })

        axios.get(API_URL+'/main/get_delivery_fee')
        .then(res => {
            // console.log(res.data)
            this.setState({
                loading: false,
                biaya_kirim: res.data.amount
            })
        })

        Geolocation.getCurrentPosition(info => {
            Geocoder.init('AIzaSyAI4dIdi0wXv5sgx3DXh6M_Ad_To6iC8n4')
            Geocoder.from(info.coords.latitude, info.coords.longitude)
            .then(json => {
                // console.log(json.results[0])
                this.setState({
                    alamat: json.results[0].formatted_address
                })
            })
            .catch(error => console.warn(error))
        })

        
        // axios.get(API_URL+'/main/shopping/order/checkout', {params: {
        //     id_login: '118205008143923829902'
        // }})
        // .then(res => {
        //     console.log(res.data.data)
        //     this.setState({
        //         loading: false,
        //         pesanan: res.data.data.items,
        //         total: res.data.data.total,
        //         subtotal: res.data.data.subtotal
        //     })
        // })
        // .catch(e => {
        //     console.log(e)
        //     this.setState({
        //         loading: false
        //     })
        // })
    }

    checkout() {
        // axios.get(API_URL+'/main/shopping/order/checkout', {params: {
        //     id_login: '118205008143923829902'
        // }})
        // .then(resp => {
        //     console.log(resp.data)
        // })
        this.props.navigation.push('TopUpSaldo')
    }

    changeAddress() {
        if (this.state.change !== 'Ganti') {
            this.setState({
                change: 'Ganti',
                btn_bayar: 'Bayar',
                status_pengiriman: 'Mengirim ke'
            })
        } else {
            this.setState({
                change: require('../asset/search.png'),
                btn_bayar: 'Oke',
                status_pengiriman: 'Atur alamat'
            })
        }
    }

    changeApotek() {
        if (this.state.gantiApotek !== 'Ganti') {
            this.setState({
                gantiApotek: 'Ganti'
            })
        } else {
            this.setState({
                gantiApotek: require('../asset/search.png')
            })
        }
    }

    hidePanel() {
        this._panel.hide()
        this.setState({
            change: 'Ganti',
            btn_bayar: 'Bayar',
            status_pengiriman: 'Mengirim ke'
        })
    }

    showKeyboardPress() {
        this._panel.show();
        this.textInputField.focus();
    }

    format(number) {
        if (number) {
          var rupiah = "";
          var numberrev = number
            .toString()
            .split("")
            .reverse()
            .join("");
          for (var i = 0; i < numberrev.length; i++)
            if (i % 3 == 0) rupiah += numberrev.substr(i, 3) + ".";
          return (
            "Rp " +
            rupiah
              .split("", rupiah.length - 1)
              .reverse()
              .join("")
          );
        } else {
            return (
                "Rp "+number
            );
        }
    }

    nextScreen() {
        this.props.navigation.push('DetailPayment')
        this.setState({
            visible: false
        })
    }

    render() {
        const homePlace = { description: 'Home', geometry: { location: { lat: 48.8152937, lng: 2.4597668 } }};
        const workPlace = { description: 'Work', geometry: { location: { lat: 48.8496818, lng: 2.2940881 } }};

        const { pesanan } = this.state;
        let totalQuantity = 0;
        let totalPrice = 0;
        pesanan.forEach((item) => {
            totalQuantity += item.jumlah;
            totalPrice += item.jumlah * item.produk.harga;
        })
        return (
            <>
                {this.state.loading === true ?
                    <View style={[ styles.container_loading, styles.horizontal_loading ]}>
                        <ActivityIndicator size='large' color={blue} />
                    </View>
                    :
                    null
                }
                <Dialog
                    visible={this.state.visible}
                    onTouchOutside={() => this.setState({ visible: false })}
                >
                    <DialogContent>
                        <View style={{ width: wp('50%'), alignItems: 'center' }}>
                            <View style={[ p.center, { width: wp('20%'), height: wp('20%'), backgroundColor: blue, borderRadius: wp('10%'), marginTop: wp('10%') }]}>
                                <Image source={require('../asset/check.png')} style={{ width: wp('10%'), height: wp('10%'), tintColor: light }} />
                            </View>
                            <Text style={[ f._18, f.bold, { marginTop: wp('10%') }]}>Pembayaran Sukses</Text>
                            <Text style={{ marginTop: wp('2%') }}>Senang bisa melayani anda.</Text>
                            <TouchableOpacity onPress={() => this.nextScreen()} style={[ p.center, { width: wp('35%'), height: hp('6%'), backgroundColor: blue, borderRadius: wp('5%'), marginTop: wp('20%'), marginBottom: wp('2%') }]}>
                                <Text style={[ c.light, f._16 ]}>Lanjutkan</Text>
                            </TouchableOpacity>
                        </View>
                    </DialogContent>
                </Dialog>
                <Dialog
                    visible={this.state.visibleFailed}
                    onTouchOutside={() => this.setState({ visibleFailed: false })}
                >
                    <DialogContent>
                        <View style={{ width: wp('50%'), alignItems: 'center' }}>
                            <View style={[ p.center, { width: wp('20%'), height: wp('20%'), backgroundColor: '#FE8829', borderRadius: wp('10%'), marginTop: wp('10%') }]}>
                                <Image source={require('../asset/exclamationMark.png')} style={{ width: wp('10%'), height: wp('10%') }} />
                            </View>
                            <Text style={[ f._18, f.bold, { marginTop: wp('10%') }]}>Pembayaran Gagal</Text>
                            <Text style={{ marginTop: wp('2%'), textAlign: 'center' }}>Kita tidak bisa memproses pembayaran anda. Coba lagi.</Text>
                            <TouchableOpacity onPress={() => this.setState({ visibleFailed: false })} style={[ p.center, { width: wp('35%'), height: hp('6%'), backgroundColor: '#FE8829', borderRadius: wp('5%'), marginTop: wp('20%'), marginBottom: wp('2%') }]}>
                                <Text style={[ c.light, f._16 ]}>Coba Lagi</Text>
                            </TouchableOpacity>
                        </View>
                    </DialogContent>
                </Dialog>
                <View style={{ width: '100%', height: '100%', backgroundColor: '#121212' }}>
                    <View style={{ width: '100%', marginTop: wp('4%'), flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ width: wp('20%') }}>
                            <TouchableOpacity onPress={() => this.props.navigation.pop()} style={{ marginLeft: wp('4%') }}>
                                <FastImage source={require('../asset/left.png')} style={{ width: wp('8%'), height: wp('8%') }} />
                            </TouchableOpacity>
                        </View>
                        <View style={[ p.center, { width: wp('60%') }]}>
                            <Text style={[ c.light, f._18, { fontFamily: 'lineto-circular-pro-bold' }]}>Pembayaran</Text>
                        </View>
                        <View style={{ width: wp('20%') }}></View>
                    </View>
                    {/* <View style={{ width: wp('90%'), height: hp('10%'), backgroundColor: blue, borderRadius: wp('2%'), marginTop: wp('5%'), marginLeft: wp('5%'), flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ width: wp('45%') }}>
                            <Text style={[ c.light, f._12, { marginLeft: wp('4%') }]}>Saldo anda</Text>
                            <Text style={[ c.light, f._18, { fontFamily: 'lineto-circular-pro-bold', marginLeft: wp('4%') }]}>RP 100.000</Text>
                        </View>
                        <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                            <TouchableOpacity onPress={() => this.props.navigation.push('TopUpSaldo')} style={[ p.center, { width: wp('25%'), height: hp('5%'), backgroundColor: light, borderRadius: wp('3%'), marginRight: wp('4%'), flexDirection: 'row', alignItems: 'center' }]}>
                                <Image source={require('../asset/plus2.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: blue }} />
                                <Text style={[ c.blue, { marginLeft: wp('1%') }]}>Top Up</Text>
                            </TouchableOpacity>
                        </View>
                    </View> */}
                    <View style={{ marginTop: wp('4%') }} />
                    <ScrollView>
                        <View style={{ width: '100%' }}>
                            <View style={{ width: '100%', alignItems: 'center', marginBottom: wp('4%') }}>
                                <FlatList
                                    data={this.state.pesanan}
                                    keyExtractor={item => item.id}
                                    renderItem={({ item }) => (
                                        <>
                                            <View style={{ width: wp('90%'), flexDirection: 'row' }}>
                                                <View style={{ width: wp('50%') }}>
                                                    <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>{item.produk.nama}</Text>
                                                    <Text style={[ c.light ]}>{item.jumlah} x {this.format(item.produk.harga)}</Text>
                                                </View>
                                                <View style={{ width: wp('40%'), alignItems: 'flex-end' }}>
                                                    <Text></Text>
                                                    <Text style={[ c.light ]}>{this.format(item.produk.harga * item.jumlah)}</Text>
                                                </View>
                                            </View>
                                        </>
                                    )}
                                />
                                <View style={{ width: wp('90%'), flexDirection: 'row' }}>
                                    <View style={{ width: wp('50%') }}>
                                        <Text style={[ c.light, b.mt1 ]}>Total belanja</Text>
                                        <Text style={[ c.light, b.mt1 ]}>Biaya kirim</Text>
                                        <Text style={[ c.light, b.mt1 ]}>Voucher promo</Text>
                                    </View>
                                    <View style={{ width: wp('40%'), alignItems: 'flex-end' }}>
                                        <Text style={[ c.light, b.mt1 ]}>{this.format(totalPrice)}</Text>
                                        <Text style={[ c.light, b.mt1 ]}>{this.format(this.state.biaya_kirim)}</Text>
                                        <Text style={[ c.light, b.mt1 ]}>-{this.format('10000')}</Text>
                                    </View>
                                </View>
                                <TouchableOpacity onPress={() => this.props.navigation.push('PromoCode')} style={{ width: wp('90%'), height: hp('6%'), flexDirection: 'row', alignItems: 'center', borderWidth: 1, borderColor: blue, marginTop: wp('1%'), borderRadius: wp('1%') }}>
                                    <View style={{ width: wp('45%'), flexDirection: 'row', alignItems: 'center' }}>
                                        <FastImage source={require('../asset/promocodekecil.png')} style={{ width: wp('8%'), height: wp('4%'), marginLeft: wp('2%') }} />
                                        <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold', marginLeft: wp('2%') }]}>Kode Promo</Text>
                                    </View>
                                    <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                                        <FastImage source={require('../asset/promocodebesar.png')} style={{ width: wp('23%'), height: wp('8%'), marginRight: wp('2%'), alignItems: 'flex-end' }}>
                                            <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold', marginRight: wp('2%'), marginTop: wp('1.2%') }]}>-RP10RB</Text>
                                        </FastImage>
                                    </View>
                                </TouchableOpacity>
                                <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('2%') }}>
                                    <View style={{ width: wp('45%'), flexDirection: 'row' }}>
                                        <Image source={require('../asset/point_icon.png')} style={{ width: wp('6%'), height: wp('6%'), marginLeft: wp('2%') }} />
                                        <View style={{ marginLeft: wp('2%') }}>
                                            <Text style={[ c.light, f._16, { fontFamily: 'lineto-circular-pro-bold' }]}>1000</Text>
                                            <Text style={[ c.light ]}>Gunakan poin</Text>
                                        </View>
                                    </View>
                                    <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                                        <SwitchToggle
                                            switchOn={this.state.switchOn}
                                            onPress={() => this.setState({ switchOn: !this.state.switchOn })}
                                            circleColorOn={light}
                                            backgroundColorOn={blue}
                                            containerStyle={{
                                                width: 50,
                                                height: 28,
                                                borderRadius: 25,
                                                backgroundColor: {softGrey},
                                                padding: 5
                                            }}
                                            circleStyle={{
                                                width: 20,
                                                height: 20,
                                                borderRadius: 19,
                                                backgroundColor: {light}
                                            }}
                                        />
                                    </View>
                                </View>
                                <View style={{ width: wp('90%'), flexDirection: 'row' }}>
                                    <View style={{ width: wp('50%') }}>
                                        <Text style={[ c.light, b.mt1 ]}>Gunakan poin</Text>
                                        <Text style={[ c.light, b.mt1, { fontFamily: 'lineto-circular-pro-bold' }]}>Total Bayar</Text>
                                    </View>
                                    <View style={{ width: wp('40%'), alignItems: 'flex-end' }}>
                                        {!this.state.switchOn ?
                                            <Text style={[ c.light, b.mt1 ]}>{this.format('0')}</Text>
                                            :
                                            <Text style={[ c.light, b.mt1 ]}>-{this.format('1000')}</Text>
                                        }
                                        <Text style={[ c.light, b.mt1, { fontFamily: 'lineto-circular-pro-bold' }]}>{!this.state.switchOn ? this.format(totalPrice + this.state.biaya_kirim - 10000) : this.format(totalPrice + this.state.biaya_kirim - 10000 - 1000)}</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                    <View style={[ width, { maxHeight: hp('60%'), position: 'relative', bottom: 0, backgroundColor: '#181B22', alignItems: 'center' }]}>
                        <View style={{ width: wp('90%'), marginTop: wp('4%'), flexDirection: 'row', alignItems: 'center' }}>
                            <View style={{ width: wp('45%') }}>
                                <Text style={[ c.light, f._16, { fontFamily: 'lineto-circular-pro-bold' }]} numberOfLines={2}>Toko Apotek</Text>
                            </View>
                            <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                                {this.state.gantiApotek !== 'Ganti' ?
                                    <TouchableOpacity onPress={() => this.changeApotek()}>
                                        <Image source={this.state.gantiApotek} style={{ width: wp('6%'), height: wp('6%'), tintColor: light }} />
                                    </TouchableOpacity>
                                    :
                                    <TouchableOpacity onPress={() => this.changeApotek()}>
                                        <Text style={[ c.blue, f._16 ]}>{this.state.gantiApotek}</Text>
                                    </TouchableOpacity>
                                }
                            </View>
                        </View>
                        <View style={{ width: wp('90%'), marginTop: wp('2%') }}>
                            <Text style={[ c.light ]}>Apotek Sehati</Text>
                            <Text style={[ c.light, f._12 ]}>Ruko Mataram, Semarang</Text>
                        </View>
                        <View style={{ width: wp('90%'), marginTop: wp('4%'), flexDirection: 'row', alignItems: 'center' }}>
                            <View style={{ width: wp('45%') }}>
                                <Text style={[ c.light, f._16, { fontFamily: 'lineto-circular-pro-bold' }]} numberOfLines={2}>{this.state.status_pengiriman}</Text>
                            </View>
                            <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                                {this.state.change !== 'Ganti' ?
                                    <TouchableOpacity onPress={() => this._panel.show()}>
                                        <Image source={this.state.change} style={{ width: wp('6%'), height: wp('6%'), tintColor: light }} />
                                    </TouchableOpacity>
                                    :
                                    <TouchableOpacity onPress={() => this.changeAddress()}>
                                        <Text style={[ c.blue, f._16 ]}>{this.state.change}</Text>
                                    </TouchableOpacity>
                                }
                            </View>
                        </View>
                        <View style={{ width: wp('90%'), marginTop: wp('2%') }}>
                            <Text style={[ c.light, f._12 ]}>{this.state.alamat}</Text>

                            <Text style={[ c.light, f._16, { marginTop: wp('4%') }]}>Catatan</Text>
                            <View style={{ width: '100%', height: hp('7%'), backgroundColor: light, marginTop: wp('1%'), borderRadius: wp('1%') }}>
                                <TextInput
                                    style={{ width: '100%' }}
                                    multiline={true}
                                    onChangeText={(text) => this.setState({ catatan: text })}
                                    value={this.state.catatan}
                                />
                            </View>
                        </View>
                        {this.state.btn_bayar !== 'Bayar' ?
                            <TouchableOpacity onPress={() => this.changeAddress()} style={[ p.center, { width: wp('30%'), height: hp('5.5%'), backgroundColor: blue, borderRadius: wp('3%'), marginTop: wp('5%'), marginBottom: wp('4%') }]}>
                                <Text style={[ c.light, f._18, { fontFamily: 'lineto-circular-pro-bold' }]}>{this.state.btn_bayar}</Text>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity onPress={() => this.checkout()} style={[ p.center, { width: wp('30%'), height: hp('5.5%'), backgroundColor: blue, borderRadius: wp('3%'), marginTop: wp('5%'), marginBottom: wp('4%') }]}>
                                <Text style={[ c.light, f._18, { fontFamily: 'lineto-circular-pro-bold' }]}>{this.state.btn_bayar}</Text>
                            </TouchableOpacity>
                        }
                    </View>
                </View>
                <SlidingUpPanel ref={c => this._panel = c}>
                    <View style={{ width: '100%', height: '100%', backgroundColor: '#181B22', alignItems: 'center' }}>
                        <GooglePlacesAutocomplete
                            placeholder='Search'
                            minLength={2}
                            fetchDetails={true}
                            listViewDisplayed='auto'
                            styles={{
                                textInputContainer: { width: wp('90%'), marginTop: wp('2%') },
                                listView: { width: wp('90%'), backgroundColor: light, borderRadius: wp('2%') }
                            }}
                            query={{
                                key: 'AIzaSyAI4dIdi0wXv5sgx3DXh6M_Ad_To6iC8n4',
                                language: 'id'
                            }}
                            onPress={(data, detail = null) => {
                                console.log(data.description)
                                this.setState({ alamat: data.description })
                            }}
                            enablePoweredByContainer={false}
                        />

                        <TouchableOpacity onPress={() => this.hidePanel()} style={[ p.center, { width: wp('30%'), height: hp('5.5%'), borderRadius: wp('3%'), backgroundColor: blue, position: 'relative', bottom: 0, marginBottom: wp('4%') }]}>
                            <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>Oke</Text>
                        </TouchableOpacity>
                    </View>
                </SlidingUpPanel>
            </>
        )
    }
}

const styles = StyleSheet.create({
    container_loading: {
        flex: 1,
        position:'absolute',
        zIndex: 2,
        width: '100%',
        height: '100%',
        justifyContent: "center",
        backgroundColor: 'rgba(0,0,0,0.7)'
    },
    horizontal_loading: {
        flexDirection: "row",
        justifyContent: "space-around",
        padding: 10
    },
})