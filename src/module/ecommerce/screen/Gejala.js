import React, { Component } from 'react'
import { Text, View, TouchableOpacity, ScrollView, FlatList, TextInput, Image, TouchableHighlight, Animated, ToastAndroid, RefreshControl, Alert } from 'react-native'
import { blue, light, grey, softGrey } from '../utils/Color'
import { f, c, b, p } from '../utils/StyleHelper'
import { xs, vs } from '../utils/Responsive'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import { Tabs, Tab } from 'native-base'
import Icon from 'react-native-vector-icons/FontAwesome'
import { NavigationEvents } from 'react-navigation'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { API_URL } from 'react-native-dotenv'

var isHidden = true;

export default class Gejala extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            list_product: undefined,
            artikel:[
                {'image': require('../asset/pusing.png'), 'value': 'Demam dan Batuk? Jangan Panik! Kenali Perbedaan Gejala-19', 'uploaded': 'HealthPlus | 23 April 2020', 'key': '0'},
                {'image': require('../asset/pusing.png'), 'value': 'Demam dan Batuk? Jangan Panik! Kenali Perbedaan Gejala-19', 'uploaded': 'HealthPlus | 23 April 2020', 'key': '1'},
                {'image': require('../asset/pusing.png'), 'value': 'Demam dan Batuk? Jangan Panik! Kenali Perbedaan Gejala-19', 'uploaded': 'HealthPlus | 23 April 2020', 'key': '2'},
                {'image': require('../asset/pusing.png'), 'value': 'Demam dan Batuk? Jangan Panik! Kenali Perbedaan Gejala-19', 'uploaded': 'HealthPlus | 23 April 2020', 'key': '3'},
            ],
            status: [
				{jenis_status: 'Pengertian'},
				{jenis_status: 'Penyebab'},
				{jenis_status: 'Penanganan'},
			],
            btn_buy: 'BELI',
            count: '',
            search: '',
            image: require('../asset/triangleArrow.png'),
            bounceValue: new Animated.Value(52),
            clicked: 0,
            status_symptoms: null,
            nama_gejala: 'loading...',
            pengertian: 'loading...',
            penyebab: 'loading...',
            penanganan: 'loading...',
            hidden: true
        }
    }

    componentDidMount() {
        this.setState({ loading: true })

        axios.get(API_URL+'/main/shopping/search', {params: {
            q: this.props.navigation.getParam('qSearch'),
            id_login: '118205008143923829902'
        }})
        .then(resp => {
            console.log('qSearch '+JSON.stringify(resp.data.drugs[0].ratings))
            resp.data.drugs.forEach((value, key) => {

                resp.data.cart.forEach((v,k) => {
                    if (String(v.produk_id) === String(value.id)) {
                        resp.data.drugs[key].jumlah = resp.data.cart[k].jumlah
                        resp.data.drugs[key].cart_id = v.id
                    }
                })
            })

            resp.data.drugs.forEach((value, key) => {
                if (value.jumlah === undefined) {
                    resp.data.drugs[key].jumlah='0'
                }
            })
            this.setState({
                loading: false,
                search: resp.data.symptom.nama,
                nama_gejala: resp.data.symptom.nama,
                pengertian: resp.data.symptom.pengertian,
                penyebab: resp.data.symptom.penyebab,
                penanganan: resp.data.symptom.penanganan,
                count: resp.data.cart.length,
                list_product: resp.data.drugs
            })
        })
    }

    async swicth(jenis_status, index) {
		await this.setState({
            clicked: index,
            status_symptoms: jenis_status
        })
        console.log(this.state.status_symptoms)
	}

    filterList(list) {
        return list.filter(listItem => listItem.toLowerCase().includes(this.state.search.toLowerCase()))
    }

    changePosition() {
        this.setState({
            image: !isHidden ? require('../asset/triangleArrow.png') : require('../asset/triangleArrowUp.png')
        });
        
        var toValue = 52;

        if (isHidden) {
            toValue = 0;
        }

        Animated.spring(
            this.state.bounceValue,
            {
                toValue: toValue,
                velocity: 3,
                tension: 2,
                friction: 8
            }
        ).start();

        isHidden = !isHidden;
    }

    incrementCount = (item) => {
        axios.post(API_URL+'/main/shopping/cart/'+item.id+'/add', {id_login: '118205008143923829902', jumlah: 1})
        .then(resp => {
            // console.log(resp.data)
            this.componentDidMount()
        })
        .catch(err => {
            console.log('add_to_cart: '+err)
        })
        ToastAndroid.show('Added '+item.nama+' Again', ToastAndroid.SHORT)
    }

    decrementCount = (item) => {
        if (item.jumlah === '1') {
            Alert.alert(
                "",
                "Apakah Anda yakin ingin menghapus?",
                [
                  {
                    text: 'Tidak', onPress: () => console.log('Cancel Pressed'), style: "cancel"
                  },
                  { text: 'Ya', onPress: () => {
                    axios.get(API_URL+'/main/shopping/cart/'+item.cart_id+'/remove')
                    .then(res => {
                        // console.log(res.data.status)
                        this.componentDidMount()
                        if (res.data.status === 'true') {
                            ToastAndroid.show('Your order has been deleted', ToastAndroid.SHORT)
                        }
                    })
                  } }
                ],  
                { cancelable: false }
            );
            return true;
        } else {
            axios.post(API_URL+'/main/shopping/cart/'+item.id+'/add', {id_login: '118205008143923829902', jumlah: -1})
            .then(resp => {
                // console.log(resp.data)
                this.componentDidMount()
            })
            .catch(err => {
                console.log('add_to_cart: '+err)
            })
            ToastAndroid.show('Reduce Your '+item.nama+' Product', ToastAndroid.SHORT)
        }
    }

    addToCart(item) {
        this.setState({ loading: true })

        axios.post(API_URL+'/main/shopping/cart/'+item+'/add', {id_login: '118205008143923829902', jumlah: 1})
        .then(resp => {
            // console.log(resp.data)
            this.componentDidMount()
        })
        .catch(err => {
            console.log('add_to_cart: '+err)
        })
    }

    convertToRupiah(number) {
        if (number) {
          var rupiah = "";
          var numberrev = number
            .toString()
            .split("")
            .reverse()
            .join("");
          for (var i = 0; i < numberrev.length; i++)
            if (i % 3 == 0) rupiah += numberrev.substr(i, 3) + ".";
          return (
            "Rp " +
            rupiah
              .split("", rupiah.length - 1)
              .reverse()
              .join("")
          );
        } else {
            return (
                "Rp "+number
            );
        }
    }

    hideShow() {
        this.setState({
            hidden: !this.state.hidden ? true : false
        })
    }

    nextScreen() {
        axios.get(API_URL+'/main/shopping/search', {params: {
            q: this.state.search
        }})
        .then(resp => {
            // console.log('search: '+res.data.symptom.nama)
            this.props.navigation.setParams({ qSearch: this.state.search })
            this.componentDidMount()
        })
    }

    render() {
        const list = ['Headache', 'Cough', 'Fever', 'Sore throat', 'Flu'];
        return (
            <>
                <NavigationEvents onDidFocus={() => this.componentDidMount()} />
                <TouchableOpacity onPress={() => this.props.navigation.push('PharmacyService')} style={{ position: 'absolute', right: wp('5%'), bottom: wp('5%'), zIndex: 1 }}>
                    <FastImage source={require('../asset/floatingbuttonapotekcs.png')} style={{ width: wp('12%'), height: wp('12%') }} />
                </TouchableOpacity>
                <View style={{ width: '100%', alignItems: 'center', backgroundColor: '#121212' }}>
                    <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('4%'), marginBottom: wp('4%') }}>
                        <View style={{ width: wp('45%') }}>
                            <FastImage source={require('../asset/healthPlus.png')} style={{ width: xs(160), height: xs(38) }} />
                        </View>
                        <View style={{ width: wp('45%'), flexDirection: 'row-reverse' }}>
                            <TouchableOpacity onPress={() => this.props.navigation.push('Cart')}>
                                {this.state.count !== '' ?
                                    <View style={{ width: wp('3.5%'), height: wp('3.5%'), backgroundColor: 'orange', position: 'absolute', borderRadius: wp('3.5%'), zIndex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                        <Text style={[ c.light, f._8 ]}>{this.state.count}</Text>
                                    </View>
                                    :
                                    null
                                }
                                <Image source={require('../asset/addCart.png')} style={{ width: wp('7%'), height: wp('7%'), tintColor: light }} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.props.navigation.push('HistoryPayment')}  style={{ marginRight: wp('3%') }}>
                                <Image source={require('../asset/order_icon.png')} style={{ width: wp('7%'), height: wp('7%'), tintColor: light }} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.props.navigation.push('FrequentlyOrder')} style={{ marginRight: wp('3%') }}>
                                <Image source={require('../asset/Frequentlyorder_icon.png')} style={{ width: wp('8%'), height: wp('7%'), tintColor: light }} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{ width: wp('90%'), height: hp('6.5%'), borderRadius: wp('5%'), backgroundColor: softGrey, marginTop: wp('2%'), flexDirection: 'row', alignItems: 'center' }}>
                        <TextInput
                            style={[ c.light, { width: wp('62%'), height: '100%', marginLeft: wp('4%') }]}
                            ref={ref => this.textInputRef = ref}
                            placeholder='Apa yang kamu cari?'
                            placeholderTextColor={light}
                            returnKeyType='search'
                            onChangeText={(text) => this.setState({ search: text })}
                            onSubmitEditing={() => this.nextScreen()}
                            value={this.state.search}
                        />
                        <View style={{ width: wp('20%'), flexDirection: 'row-reverse' }}>
                            {this.state.search === '' ?
                                <TouchableOpacity onPress={() => this.textInputRef.focus()}>
                                    <Image source={require('../asset/search.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: light, marginLeft: wp('4%') }} />
                                </TouchableOpacity>
                                :
                                <TouchableOpacity onPress={() => this.setState({ search: '' })} style={{ marginLeft: wp('4%') }}>
                                    <Image source={require('../asset/cross.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: light }} />
                                </TouchableOpacity>
                            }
                            {/* <TouchableOpacity>
                                <Image source={require('../asset/camera.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: light }} />
                            </TouchableOpacity> */}
                        </View>
                    </View>
                    {this.state.search !== '' && (
                        <View style={{ width: wp('80%'), maxHeight: hp('30%'), backgroundColor: softGrey, marginTop: wp('1%'), borderRadius: wp('1%') }}>
                            <ScrollView showsVerticalScrollIndicator={false}>
                                {this.filterList(list).map((listItem, index) => (
                                    <View>
                                        {listItem === 'Headache' && (
                                            <TouchableOpacity onPress={() => this.props.navigation.push('Gejala')} style={{ width: '100%', height: hp('6%'), justifyContent: 'center' }}>
                                                <Text key={index} style={[ c.light, { marginLeft: wp('2%') }]}>{listItem}</Text>
                                            </TouchableOpacity>
                                        )}
                                        {listItem === 'Cough' && (
                                            <TouchableOpacity onPress={() => this.props.navigation.push('Gejala')} style={{ width: '100%', height: hp('6%'), justifyContent: 'center' }}>
                                                <Text key={index} style={[ c.light, { marginLeft: wp('2%') }]}>{listItem}</Text>
                                            </TouchableOpacity>
                                        )}
                                        {listItem === 'Fever' && (
                                            <TouchableOpacity onPress={() => this.props.navigation.push('Gejala')} style={{ width: '100%', height: hp('6%'), justifyContent: 'center' }}>
                                                <Text key={index} style={[ c.light, { marginLeft: wp('2%') }]}>{listItem}</Text>
                                            </TouchableOpacity>
                                        )}
                                        {listItem === 'Sore throat' && (
                                            <TouchableOpacity onPress={() => this.props.navigation.push('Gejala')} style={{ width: '100%', height: hp('6%'), justifyContent: 'center' }}>
                                                <Text key={index} style={[ c.light, { marginLeft: wp('2%') }]}>{listItem}</Text>
                                            </TouchableOpacity>
                                        )}
                                        {listItem === 'Flu' && (
                                            <TouchableOpacity onPress={() => this.props.navigation.push('Gejala')} style={{ width: '100%', height: hp('6%'), justifyContent: 'center' }}>
                                                <Text key={index} style={[ c.light, { marginLeft: wp('2%') }]}>{listItem}</Text>
                                            </TouchableOpacity>
                                        )}
                                        <View style={{ borderBottomWidth: 1, borderBottomColor: '#181B22' }} />
                                    </View>
                                ))}
                            </ScrollView>
                        </View>
                    )}
                    <View style={{ marginTop: wp('4%') }} />
                </View>
                <Tabs tabBarUnderlineStyle={{ borderBottomWidth: 4, borderBottomColor: blue }}>
                    <Tab heading='Gejala' tabStyle={{ backgroundColor: '#181B22' }} textStyle={{ color: light }} activeTabStyle={{ backgroundColor: blue }} activeTextStyle={{ color: light, fontFamily: 'lineto-circular-pro-bold' }}>
                        <ScrollView style={{ backgroundColor: '#121212' }} refreshControl={<RefreshControl onRefresh={() => this.componentDidMount()} refreshing={this.state.loading} />}>
                            <View style={{ width: '100%', alignItems: 'center' }}>
                                <View style={{ width: wp('90%'), marginTop: wp('4%') }}>
                                    <Text style={[ c.light, f._18, { fontFamily: 'lineto-circular-pro-bold', textTransform: 'capitalize' }]}>{this.state.nama_gejala}</Text>
                                </View>

                                <View style={{ width: wp('90%'), marginTop: wp('4%') }}>
                                    <FlatList
                                        horizontal={true}
                                        data={this.state.status}
                                        keyExtractor={item => item.jenis_status}
                                        renderItem={({ item, index }) => (
                                            (this.state.clicked === index ?
                                                <TouchableOpacity onPress={() => this.swicth(item.jenis_status, index)} style={{ alignItems: 'center', backgroundColor: blue, borderRadius: wp('5%'), marginRight: wp('2%') }}>
                                                    <Text style={[ b.ml3, b.mr3, b.mt1, b.mb1, c.light, f._12, p.textCenter ]}>{item.jenis_status}</Text>
                                                </TouchableOpacity>
                                                :
                                                <TouchableOpacity onPress={() => this.swicth(item.jenis_status, index)} style={{ alignItems: 'center', backgroundColor: '#181B22', borderRadius: wp('5%'), marginRight: wp('2%') }}>
                                                    <Text style={[ b.ml3, b.mr3, b.mt1, b.mb1, c.light, f._12, p.textCenter ]}>{item.jenis_status}</Text>
                                                </TouchableOpacity>
                                            )
                                        )}
                                    />
                                </View>

                                {this.state.status_symptoms === 'Pengetian' || this.state.clicked === 0 && (
                                    <>
                                        <Text style={[ c.light, f._16, { marginTop: wp('4%'), fontFamily: 'lineto-circular-pro-bold' }]}>Apa yang harus anda lakukan  <Image source={require('../asset/info_icon.png')} style={{ width: wp('5%'),  height: wp('5%') }} /></Text>

                                        <View style={{ width: wp('90%'), marginTop: wp('3%'), marginBottom: wp('2%') }}>
                                            <Text style={[ c.light, { marginTop: wp('2%'), marginBottom: wp('2%'), marginLeft: wp('3%'), marginRight: wp('3%') }]}>
                                                {this.state.pengertian}
                                            </Text>
                                        </View>
                                    </>
                                )}

                                {this.state.status_symptoms === 'Penyebab' && (
                                    <>
                                        <Text style={[ c.light, f._16, { marginTop: wp('4%'), fontFamily: 'lineto-circular-pro-bold' }]}>Apa yang harus anda lakukan  <Image source={require('../asset/info_icon.png')} style={{ width: wp('5%'),  height: wp('5%') }} /></Text>

                                        <View style={{ width: wp('90%'), marginTop: wp('3%'), marginBottom: wp('2%') }}>
                                            <Text style={[ c.light, { marginTop: wp('2%'), marginBottom: wp('2%'), marginLeft: wp('3%'), marginRight: wp('3%') }]}>
                                                {this.state.penyebab}
                                            </Text>
                                        </View>
                                    </>
                                )}

                                {this.state.status_symptoms === 'Penanganan' && (
                                    <>
                                        <Text style={[ c.light, f._16, { marginTop: wp('4%'), fontFamily: 'lineto-circular-pro-bold' }]}>Apa yang harus anda lakukan  <Image source={require('../asset/info_icon.png')} style={{ width: wp('5%'),  height: wp('5%') }} /></Text>

                                        <View style={{ width: wp('90%'), marginTop: wp('3%'), marginBottom: wp('2%') }}>
                                            <Text style={[ c.light, { marginTop: wp('2%'), marginBottom: wp('2%'), marginLeft: wp('3%'), marginRight: wp('3%') }]}>
                                                {this.state.penanganan}
                                            </Text>
                                        </View>
                                    </>
                                )}

                                <View style={{ width: '100%', backgroundColor: '#181B22', alignItems: 'center' }}>
                                    <Text style={{ color: 'darkorange', marginTop: wp('4%'), fontFamily: 'lineto-circular-pro-bold' }}>Artikel Terkait</Text>

                                    <View style={{ width: '100%', alignItems: 'center' }}>
                                        <FlatList
                                            ListHeaderComponent={<View style={{ marginTop: wp('1%') }} />}
                                            ListFooterComponent={<View style={{ marginBottom: wp('3%') }} />}
                                            data={this.state.artikel}
                                            keyExtractor={item => item.key}
                                            renderItem={({item}) => (
                                                <TouchableOpacity onPress={() => this.props.navigation.push('ArticleDetail')} style={{ width: wp('90%'), flexDirection: 'row', marginTop: wp('2%') }}>
                                                    <FastImage source={item.image} style={{ width: wp('30%'), height: wp('30%') }} />
                                                    <View style={{ width: wp('55%'), marginLeft: wp('3%') }}>
                                                        <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>{item.value}</Text>
                                                        <Text style={[ c.light, f._12, { marginTop: wp('2%') }]}>{item.uploaded}</Text>
                                                    </View>
                                                </TouchableOpacity>
                                            )}
                                        />
                                    </View>
                                </View>
                            </View>
                        </ScrollView>
                    </Tab>
                    <Tab heading='Obat Rekomendasi' tabStyle={{ backgroundColor: '#181B22' }} textStyle={{ color: light }} activeTabStyle={{ backgroundColor: blue }} activeTextStyle={{ color: light, fontFamily: 'lineto-circular-pro-bold' }}>
                        <ScrollView style={{ backgroundColor: '#121212' }} refreshControl={<RefreshControl onRefresh={() => this.componentDidMount()} refreshing={this.state.loading} />}>
                            <View style={{ width: '100%', alignItems: 'center' }}>
                                <View style={{ width: wp('90%'), marginTop: wp('4%') }}>
                                    <View style={{ width: '100%', alignItems: 'flex-end', marginBottom: wp('4%') }}>
                                        <TouchableOpacity onPress={() => this.hideShow()} style={{ width: wp('20%'), height: hp('4%'), backgroundColor: blue, flexDirection: 'row-reverse', alignItems: 'center',  justifyContent: 'center', borderRadius: wp('2%') }}>
                                            <FastImage source={require('../asset/funnel.png')} style={{ width: wp('5%'), height: wp('5%') }} />
                                            <Text style={[ c.light, { marginRight: wp('1%') }]}>Filter</Text>
                                        </TouchableOpacity>
                                    </View>
                                    {this.state.hidden !== true && (
                                        <FastImage source={require('../asset/filter_background.png')} style={{ width: xs(122), height: xs(130), position: 'absolute', zIndex: 1, right: 0, marginTop: wp('7.5%'), alignItems: 'center' }}>
                                            <TouchableOpacity style={{ width: wp('30%'), marginTop: wp('6.5%'), flexDirection: 'row', alignItems: 'center' }}>
                                                <View style={{ width: wp('20%') }}>
                                                    <Text style={[ c.light ]}>Nama</Text>
                                                </View>
                                                <View style={{ width: wp('10%'), flexDirection: 'row-reverse' }}>
                                                    <Image source={require('../asset/up.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: light }} />
                                                    <Image source={require('../asset/down.png')} style={{ width: wp('5%'), height: wp('5%'), marginRight: xs(-12), tintColor: light }} />
                                                </View>
                                            </TouchableOpacity>

                                            <View style={{ width: wp('30%'), borderBottomWidth: 1, borderBottomColor: light, marginTop: wp('2.5%') }} />

                                            <TouchableOpacity style={{ width: wp('30%'), marginTop: wp('2.5%'), flexDirection: 'row', alignItems: 'center' }}>
                                                <View style={{ width: wp('20%') }}>
                                                    <Text style={[ c.light ]}>Harga</Text>
                                                </View>
                                                <View style={{ width: wp('10%'), flexDirection: 'row-reverse' }}>
                                                    <Image source={require('../asset/up.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: light }} />
                                                    <Image source={require('../asset/down.png')} style={{ width: wp('5%'), height: wp('5%'), marginRight: xs(-12), tintColor: light }} />
                                                </View>
                                            </TouchableOpacity>

                                            <View style={{ width: wp('30%'), borderBottomWidth: 1, borderBottomColor: light, marginTop: wp('2.5%') }} />

                                            <TouchableOpacity style={{ width: wp('30%'), marginTop: wp('2.5%'), flexDirection: 'row', alignItems: 'center' }}>
                                                <View style={{ width: wp('20%') }}>
                                                    <Text style={[ c.light ]}>Populer</Text>
                                                </View>
                                                <View style={{ width: wp('10%'), flexDirection: 'row-reverse' }}>
                                                    <Image source={require('../asset/up.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: light }} />
                                                    <Image source={require('../asset/down.png')} style={{ width: wp('5%'), height: wp('5%'), marginRight: xs(-12), tintColor: light }} />
                                                </View>
                                            </TouchableOpacity>
                                        </FastImage>
                                    )}
                                    <FlatList
                                        ListFooterComponent={<View style={{ marginBottom: wp('9%') }} />}
                                        data={this.state.list_product}
                                        keyExtractor={item => item.id}
                                        numColumns={2}
                                        renderItem={({item}) => (
                                            <View style={{ width: wp('45%'), alignItems: 'center' }}>
                                                <TouchableOpacity onPress={() => this.props.navigation.push('DetailProduct', { id_produk: item.id })} style={{ width: wp('43%'), height: wp('70%'), borderWidth: 1, borderColor: light, borderRadius: wp('1.5%'), alignItems: 'center', marginBottom: wp('3%') }}>
                                                    <View style={{ width: '100%', backgroundColor: light, alignItems: 'center', justifyContent: 'center' }}>
                                                        <FastImage source={require('../asset/logo_combi_small.png')} style={{ width: wp('6%'), height: wp('6%'), position: 'absolute', left: wp('1%'), top: wp('1%') }} />
                                                        <FastImage source={{ uri: API_URL+'/src/images/'+item.gambar}} style={{ width: wp('20%'), height: wp('22%'), marginTop: wp('2%'), marginBottom: wp('2%') }} />
                                                    </View>
                                                    <View style={{ marginTop: wp('2%'), width: wp('30%') }}>
                                                        <Text style={[ c.light, f._16, { fontFamily: 'lineto-circular-pro-bold', textAlign: 'center' }]} numberOfLines={2}>{item.nama}</Text>
                                                    </View>
                                                    <View style={{ marginTop: wp('2%'), width: wp('30%') }}>
                                                        <Text style={[ c.light, { textAlign: 'center', fontSize: wp('3.5%') }]}>{this.convertToRupiah(item.harga)} / {item.satuan}</Text>
                                                    </View>
                                                    <View style={{ flexDirection: 'row', marginTop: wp('1%'), alignItems: 'center' }}>
                                                        <Image source={require('../asset/staredd.png')} style={{ width: wp('4%'), height: wp('4%') }} />
                                                        <Text style={[ c.light, f._12, { marginLeft: wp('1%') }]}>5.0 | 10 Ulasan</Text>
                                                    </View>
                                                    {item.jumlah !== '0' ?
                                                        <View style={{ flexDirection: 'row', alignItems: 'center', borderRadius: wp('1%'), height: wp('7%'), marginTop: wp('4%'), marginBottom: wp('4%') }}>
                                                            <TouchableHighlight onPress={() => this.decrementCount(item)} underlayColor={blue} style={[ p.center, { width: wp('7%'), height: '100%', borderWidth: 1, borderColor: blue, borderRadius: wp('2%') }]}>
                                                                <Icon name='minus' size={10} color={light} />
                                                            </TouchableHighlight>
                                                            <View style={[ p.center, { width: wp('8.3%') }]}>
                                                                <Text style={[ c.light ]}>{item.jumlah}</Text>
                                                            </View>
                                                            <TouchableHighlight onPress={() => this.incrementCount(item)} underlayColor={blue} style={[ p.center, { width: wp('7%'), height: '100%', borderWidth: 1, borderColor: blue, borderRadius: wp('2%') }]}>
                                                                <Icon name='plus' size={10} color={light} />
                                                            </TouchableHighlight>
                                                        </View>
                                                        :
                                                        <TouchableOpacity onPress={() => this.addToCart(item.id)} style={[ p.center, { width: wp('20%'), height: hp('4%'), backgroundColor: blue, borderRadius: wp('2%'), marginTop: wp('4%'), marginBottom: wp('4%') }]}>
                                                            <Text style={{ color: light, fontFamily: 'lineto-circular-pro-bold' }}>{this.state.btn_buy}</Text>
                                                        </TouchableOpacity>
                                                    }
                                                </TouchableOpacity>
                                            </View>
                                        )}
                                    />
                                </View>
                            </View>
                        </ScrollView>
                    </Tab>
                </Tabs>
            </>
        )
    }
}
