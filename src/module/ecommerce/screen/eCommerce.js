import React, { Component } from 'react'
import { Text, View, TouchableOpacity, ToastAndroid, Alert, TextInput, RefreshControl, Image, ImageBackground, ScrollView, FlatList, Animated, TouchableHighlight } from 'react-native'
import { xs, vs } from '../utils/Responsive'
import { blue, light, softGrey } from '../utils/Color'
import { p, c, b, f } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import ImageSlider from 'react-native-image-slider'
import Icon from 'react-native-vector-icons/FontAwesome'
import { NavigationEvents } from 'react-navigation'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { API_URL } from 'react-native-dotenv'

var isHidden = true;

export default class eCommerce extends Component {
    constructor(props) {
        super(props);
        this.state = {
            url_avatar: null,
            nama: '',
            search: '',
            loading: false,
            authUsers: [{
                key: 1,
                item_name: 'AAA',
            },
            {
                key: 2,
                item_name: 'BBB',
            },
            {
                key: 3,
                item_name: 'CCC',
            },
            {
                key: 4,
                item_name: 'DDD',
            },],
            artikel: undefined,
            produk: undefined,
            jenis_obat: undefined,
            gejala: undefined,
            image: require('../asset/triangleArrowUp.png'),
            bounceValue: new Animated.Value(52),
            count: '',
        };
    }

    componentDidMount() {
        this.setState({ loading: true })
        
        axios.get(API_URL+'/main/shopping/produk/popular_product', {params: {
            id_login: '118205008143923829902'
        }})
        .then(res => {
            res.data.data.forEach((value, key) => {

                res.data.cart.forEach((v,k) => {
                    if (String(v.produk_id) === String(value.id)) {
                        res.data.data[key].jumlah = res.data.cart[k].jumlah
                        res.data.data[key].cart_id = v.id
                    }
                })
            })

            res.data.data.forEach((value, key) => {
                if (value.jumlah === undefined) {
                    res.data.data[key].jumlah='0'
                }
            })

            this.setState({
                loading: false,
                count: res.data.cart.length,
                produk: res.data.data
            })
        })
        .catch(e => {
            console.log(e)
        })

        axios.get(API_URL+'/main/shopping/search/list_symptom')
        .then(resp => {
            // console.log(resp.data)
            this.setState({
                loading: false,
                gejala: resp.data
            })
        })

        axios.get(API_URL+'/main/shopping/produk/type')
        .then(response => {
            // console.log(response.data)
            this.setState({
                loading: false,
                jenis_obat: response.data
            })
        })

        axios.get(API_URL+'/main/article')
        .then(result => {
            // console.log(result.data)
            this.setState({
                loading: false,
                artikel: result.data
            })
        })
    }

    filterList(list) {
        return list.filter(listItem => listItem.toLowerCase().includes(this.state.search.toLowerCase()))
    }

    changePosition() {
        this.setState({
            image: !isHidden ? require('../asset/triangleArrowUp.png') : require('../asset/triangleArrow.png')
        });
        
        var toValue = 52;

        if (isHidden) {
            toValue = 0;
        }

        Animated.spring(
            this.state.bounceValue,
            {
                toValue: toValue,
                velocity: 3,
                tension: 2,
                friction: 8
            }
        ).start();

        isHidden = !isHidden;
    }

    incrementCount = (item) => {
        axios.post(API_URL+'/main/shopping/cart/'+item.id+'/add', {id_login: '118205008143923829902', jumlah: 1})
        .then(resp => {
            // console.log(resp.data)
            this.componentDidMount()
            // this.componentDidMount()
        })
        .catch(err => {
            console.log('add_to_cart: '+err)
        })
        ToastAndroid.show('Added Products '+item.nama+'', ToastAndroid.SHORT)
    }

    decrementCount = (item) => {
        if (item.jumlah === '1') {
            Alert.alert(
                "",
                "Apakah Anda yakin ingin menghapus?",
                [
                  {
                    text: 'Tidak', onPress: () => console.log('Cancel Pressed'), style: "cancel"
                  },
                  { text: 'Ya', onPress: () => {
                    axios.get(API_URL+'/main/shopping/cart/'+item.cart_id+'/remove', {params: {
                        id_login: '118205008143923829902'
                    }})
                    .then(res => {
                        this.componentDidMount()
                        if (res.data.status === 'true') {
                            ToastAndroid.show('Your order has been deleted', ToastAndroid.SHORT)
                        }
                    })
                  } }
                ],  
                { cancelable: false }
            );
            return true;
        } else {
            axios.post(API_URL+'/main/shopping/cart/'+item.id+'/add', {id_login: '118205008143923829902', jumlah: -1})
            .then(resp => {
                // console.log(resp.data)
                this.componentDidMount()
            })
            .catch(err => {
                console.log('add_to_cart: '+err)
            })
            ToastAndroid.show('Reduce Your Product', ToastAndroid.SHORT)
        }
    }

    addToCart(item) {
        this.setState({ loading: true })

        axios.post(API_URL+'/main/shopping/cart/'+item.id+'/add', {id_login: '118205008143923829902', jumlah: 1})
        .then(resp => {
            // console.log(resp.data)
            this.componentDidMount()
        })
        .catch(err => {
            console.log('add_to_cart: '+err)
        })
    }

    convertToRupiah(number) {
        if (number) {
          var rupiah = "";
          var numberrev = number
            .toString()
            .split("")
            .reverse()
            .join("");
          for (var i = 0; i < numberrev.length; i++)
            if (i % 3 == 0) rupiah += numberrev.substr(i, 3) + ".";
          return (
            "Rp " +
            rupiah
              .split("", rupiah.length - 1)
              .reverse()
              .join("")
          );
        } else {
            return (
                "Rp "+number
            );
        }
    }

    nextScreen() {
        axios.get(API_URL+'/main/shopping/search', {params: {
            q: this.state.search
        }})
        .then(res => {
            // console.log(this.state.search+' = '+res.data.drugs[0].nama)
            if (this.state.search === res.data.symptom.nama) {
                this.props.navigation.push('Gejala', { qSearch: res.data.symptom.nama })
            } else if (this.state.search === res.data.drugs.nama) {
                this.props.navigation.push('BuyMedicine')
            } else {
                this.props.navigation.push('MedicineList', { qSearch: this.state.search })
            }
        })
    }

    render() {
        const list = ['Headache', 'Cough', 'Fever', 'Sore throat', 'Flu', 'Fatigue', 'Toothache', 'Stomach ache', 'Chlorphenamine', 'Ephidrine', 'Ibuprofen', 'Loratadine', 'Paracetamol', 'Phenylephrine', 'OBH Combi Batuk dan Flu', 'Ultraflu', 'Panadol Cold & Flu', 'Panadol Extra', 'Paramex', 'Konidin'];
        const banners = [
            require('../asset/bannerapotek_1-430.png'),
            require('../asset/bannerapotek_2-430.png'),
            require('../asset/bannerapotek_3-430.png')
        ];
        return (
            <>
                <NavigationEvents onDidFocus={() => this.componentDidMount()} />
                <View style={{ width: '100%', height: '100%', backgroundColor: '#181B22' }}>
                    <TouchableOpacity onPress={() => this.props.navigation.push('PharmacyService')} style={{ position: 'absolute', right: wp('5%'), bottom: wp('5%'), zIndex: 1 }}>
                        <FastImage source={require('../asset/floatingbuttonapotekcs.png')} style={{ width: wp('12%'), height: wp('12%') }} />
                    </TouchableOpacity>
                    <View style={{ width: '100%', alignItems: 'center', backgroundColor: '#121212' }}>
                        <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('5%'), marginBottom: wp('5%') }}>
                            <View style={{ width: wp('45%') }}>
                                <FastImage source={require('../asset/healthPlus.png')} style={{ width: xs(160), height: xs(38) }} />
                            </View>
                            <View style={{ width: wp('45%'), flexDirection: 'row-reverse' }}>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('Drawer')}>
                                    <Image source={require('../asset/menu.png')} style={{ width: wp('7%'), height: wp('7%'), tintColor: light }} />
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('Notifikasi')} style={{ marginRight: wp('2%') }}>
                                    <Image source={require('../asset/notification.png')} style={{ width: wp('7%'), height: wp('7%'), tintColor: light }} />
                                    {this.state.notif === 'true' ?
                                        <View style={{ width: xs(10), height: xs(10), backgroundColor: 'orangered', borderRadius: xs(5),
                                        position: 'absolute', marginLeft: vs(6), marginTop: vs(1) }} />
                                        :
                                        null
                                    }
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.props.navigation.push('Cart')} style={{ marginRight: wp('2%') }}>
                                    {this.state.count !== 0 ?
                                    <View style={{ width: wp('3.5%'), height: wp('3.5%'), backgroundColor: 'orange', position: 'absolute', borderRadius: wp('3.5%'), zIndex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                        <Text style={[ c.light, f._8 ]}>{this.state.count}</Text>
                                    </View>
                                    :
                                    null
                                    }
                                    <Image source={require('../asset/addCart.png')} style={{ width: wp('7%'), height: wp('7%'), tintColor: light }} />
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.props.navigation.push('FrequentlyOrder')} style={{ marginRight: wp('2%') }}>
                                    <Image source={require('../asset/Frequentlyorder_icon.png')} style={{ width: wp('8%'), height: wp('7%'), tintColor: light }} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                    <ScrollView refreshControl={<RefreshControl onRefresh={() => this.componentDidMount()} refreshing={this.state.loading} />}>
                        <View style={{ width: '100%', alignItems: 'center' }}>
                            <View style={{ width: wp('90%'), height: hp('6.5%'), borderRadius: wp('5%'), backgroundColor: softGrey, marginTop: wp('5%'), flexDirection: 'row', alignItems: 'center' }}>
                                <TextInput
                                    style={[ c.light, { width: wp('62%'), height: '100%', marginLeft: wp('4%') }]}
                                    ref={ref => this.textInputRef = ref}
                                    placeholder='Bagaimana keadaanmu hari ini?'
                                    placeholderTextColor={light}
                                    returnKeyType='search'
                                    onChangeText={(text) => this.setState({ search: text })}
                                    onSubmitEditing={() => this.nextScreen()}
                                    value={this.state.search}
                                />
                                <View style={{ width: wp('20%'), flexDirection: 'row-reverse' }}>
                                    {this.state.search === '' ?
                                        <TouchableOpacity onPress={() => this.textInputRef.focus()}>
                                            <Image source={require('../asset/search.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: light, marginLeft: wp('4%') }} />
                                        </TouchableOpacity>
                                        :
                                        <TouchableOpacity onPress={() => this.setState({ search: '' })} style={{ marginLeft: wp('4%') }}>
                                            <Image source={require('../asset/cross.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: light }} />
                                        </TouchableOpacity>
                                    }
                                    {/* <TouchableOpacity>
                                        <Image source={require('../asset/camera.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: light }} />
                                    </TouchableOpacity> */}
                                </View>
                            </View>
                            {/* {this.state.search !== '' && (
                                <View style={{ width: wp('80%'), maxHeight: hp('30%'), backgroundColor: softGrey, marginTop: wp('1%'), borderRadius: wp('1%') }}>
                                    <ScrollView showsVerticalScrollIndicator={false}>
                                        {this.filterList(list).map((listItem, index) => (
                                            <View>
                                                {listItem === 'Headache' && (
                                                    <TouchableOpacity onPress={() => this.props.navigation.push('Gejala')} style={{ width: '100%', height: hp('6%'), justifyContent: 'center' }}>
                                                        <Text key={index} style={[ c.light, { marginLeft: wp('2%') }]}>{listItem}</Text>
                                                    </TouchableOpacity>
                                                )}
                                                {listItem === 'Cough' && (
                                                    <TouchableOpacity onPress={() => this.props.navigation.push('Gejala')} style={{ width: '100%', height: hp('6%'), justifyContent: 'center' }}>
                                                        <Text key={index} style={[ c.light, { marginLeft: wp('2%') }]}>{listItem}</Text>
                                                    </TouchableOpacity>
                                                )}
                                                {listItem === 'Fever' && (
                                                    <TouchableOpacity onPress={() => this.props.navigation.push('Gejala')} style={{ width: '100%', height: hp('6%'), justifyContent: 'center' }}>
                                                        <Text key={index} style={[ c.light, { marginLeft: wp('2%') }]}>{listItem}</Text>
                                                    </TouchableOpacity>
                                                )}
                                                {listItem === 'Sore throat' && (
                                                    <TouchableOpacity onPress={() => this.props.navigation.push('Gejala')} style={{ width: '100%', height: hp('6%'), justifyContent: 'center' }}>
                                                        <Text key={index} style={[ c.light, { marginLeft: wp('2%') }]}>{listItem}</Text>
                                                    </TouchableOpacity>
                                                )}
                                                {listItem === 'Flu' && (
                                                    <TouchableOpacity onPress={() => this.props.navigation.push('Gejala')} style={{ width: '100%', height: hp('6%'), justifyContent: 'center' }}>
                                                        <Text key={index} style={[ c.light, { marginLeft: wp('2%') }]}>{listItem}</Text>
                                                    </TouchableOpacity>
                                                )}

                                                {listItem === 'Chlorphenamine' && (
                                                    <TouchableOpacity onPress={() => this.props.navigation.push('BuyMedicine')} style={{ width: '100%', height: hp('6%'), justifyContent: 'center' }}>
                                                        <Text key={index} style={[ c.light, { marginLeft: wp('2%') }]}>{listItem}</Text>
                                                    </TouchableOpacity>
                                                )}
                                                {listItem === 'Ephidrine' && (
                                                    <TouchableOpacity onPress={() => this.props.navigation.push('BuyMedicine')} style={{ width: '100%', height: hp('6%'), justifyContent: 'center' }}>
                                                        <Text key={index} style={[ c.light, { marginLeft: wp('2%') }]}>{listItem}</Text>
                                                    </TouchableOpacity>
                                                )}
                                                {listItem === 'Ibuprofen' && (
                                                    <TouchableOpacity onPress={() => this.props.navigation.push('BuyMedicine')} style={{ width: '100%', height: hp('6%'), justifyContent: 'center' }}>
                                                        <Text key={index} style={[ c.light, { marginLeft: wp('2%') }]}>{listItem}</Text>
                                                    </TouchableOpacity>
                                                )}
                                                {listItem === 'Loratadine' && (
                                                    <TouchableOpacity onPress={() => this.props.navigation.push('BuyMedicine')} style={{ width: '100%', height: hp('6%'), justifyContent: 'center' }}>
                                                        <Text key={index} style={[ c.light, { marginLeft: wp('2%') }]}>{listItem}</Text>
                                                    </TouchableOpacity>
                                                )}
                                                {listItem === 'Paracetamol' && (
                                                    <TouchableOpacity onPress={() => this.props.navigation.push('BuyMedicine')} style={{ width: '100%', height: hp('6%'), justifyContent: 'center' }}>
                                                        <Text key={index} style={[ c.light, { marginLeft: wp('2%') }]}>{listItem}</Text>
                                                    </TouchableOpacity>
                                                )}
                                                {listItem === 'Phenylephrine' && (
                                                    <TouchableOpacity onPress={() => this.props.navigation.push('BuyMedicine')} style={{ width: '100%', height: hp('6%'), justifyContent: 'center' }}>
                                                        <Text key={index} style={[ c.light, { marginLeft: wp('2%') }]}>{listItem}</Text>
                                                    </TouchableOpacity>
                                                )}

                                                {listItem === 'OBH Combi Batuk dan Flu' && (
                                                    <TouchableOpacity onPress={() => this.props.navigation.push('MedicineList')} style={{ width: '100%', height: hp('6%'), justifyContent: 'center' }}>
                                                        <Text key={index} style={[ c.light, { marginLeft: wp('2%') }]}>{listItem}</Text>
                                                    </TouchableOpacity>
                                                )}
                                                {listItem === 'Ultraflu' && (
                                                    <TouchableOpacity onPress={() => this.props.navigation.push('MedicineList')} style={{ width: '100%', height: hp('6%'), justifyContent: 'center' }}>
                                                        <Text key={index} style={[ c.light, { marginLeft: wp('2%') }]}>{listItem}</Text>
                                                    </TouchableOpacity>
                                                )}
                                                {listItem === 'Panadol Cold & Flu' && (
                                                    <TouchableOpacity onPress={() => this.props.navigation.push('MedicineList')} style={{ width: '100%', height: hp('6%'), justifyContent: 'center' }}>
                                                        <Text key={index} style={[ c.light, { marginLeft: wp('2%') }]}>{listItem}</Text>
                                                    </TouchableOpacity>
                                                )}
                                                {listItem === 'Panadol Extra' && (
                                                    <TouchableOpacity onPress={() => this.props.navigation.push('MedicineList')} style={{ width: '100%', height: hp('6%'), justifyContent: 'center' }}>
                                                        <Text key={index} style={[ c.light, { marginLeft: wp('2%') }]}>{listItem}</Text>
                                                    </TouchableOpacity>
                                                )}
                                                {listItem === 'Paramex' && (
                                                    <TouchableOpacity onPress={() => this.props.navigation.push('MedicineList')} style={{ width: '100%', height: hp('6%'), justifyContent: 'center' }}>
                                                        <Text key={index} style={[ c.light, { marginLeft: wp('2%') }]}>{listItem}</Text>
                                                    </TouchableOpacity>
                                                )}
                                                {listItem === 'Konidin' && (
                                                    <TouchableOpacity onPress={() => this.props.navigation.push('MedicineList')} style={{ width: '100%', height: hp('6%'), justifyContent: 'center' }}>
                                                        <Text key={index} style={[ c.light, { marginLeft: wp('2%') }]}>{listItem}</Text>
                                                    </TouchableOpacity>
                                                )}
                                                <View style={{ borderBottomWidth: 1, borderBottomColor: '#181B22' }} />
                                            </View>
                                        ))}
                                    </ScrollView>
                                </View>
                            )} */}

                            <View style={{ marginTop: wp('4%'), width: '100%', height: xs(150) }}>
                                <ImageSlider
                                    loop={false}
                                    autoPlayWithInterval={5000}
                                    images={banners}
                                    onPress={({ index }) => this.props.navigation.push('BannersPromo', { id_banner: index })}
                                />
                            </View>

                            <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('5%') }}>
                                <View style={{ width: wp('45%') }}>
                                    <Text style={[ c.light, f._16, { fontFamily: 'lineto-circular-pro-bold' }]}>Jenis Gejala</Text>
                                </View>
                                <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                                    <TouchableOpacity onPress={() => this.props.navigation.push('ListSymtoms')}>
                                        <Text style={[ c.blue ]}>Lihat Semua</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={{ marginTop: wp('4%'), marginBottom: wp('3%'), marginLeft: wp('5%'), maxHeight: wp('60%') }}>
                                <FlatList
                                    numColumns={4}
                                    data={this.state.gejala}
                                    keyExtractor={item => item.key}
                                    renderItem={({item}) => (
                                        <TouchableOpacity onPress={() => this.props.navigation.push('Gejala', { qSearch: item.nama })} style={{ width: wp('18.5%'), height: '100%', marginRight: wp('5%'), marginBottom: wp('2%') }}>
                                            {item.nama === 'Batuk' && (<FastImage source={require('../asset/batuk_icon.png')} style={{ width: '100%', height: wp('18.5%') }} />)}
                                            {item.nama === 'demam' && (<FastImage source={require('../asset/demam_icon.png')} style={{ width: '100%', height: wp('18.5%') }} />)}
                                            {item.nama === 'flu' && (<FastImage source={require('../asset/flu_icon.png')} style={{ width: '100%', height: wp('18.5%') }} />)}
                                            {item.nama === 'sakit gigi' && (<FastImage source={require('../asset/sakitgigi_icon.png')} style={{ width: '100%', height: wp('18.5%') }} />)}
                                            {item.nama === 'sakit kepala' && (<FastImage source={require('../asset/sakitkepala_icon.png')} style={{ width: '100%', height: wp('18.5%') }} />)}
                                            {item.nama === 'sakit perut' && (<FastImage source={require('../asset/sakitperut_icon.png')} style={{ width: '100%', height: wp('18.5%') }} />)}
                                            <View style={{ width: '100%' }}>
                                                <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold', textAlign: 'center', marginTop: wp('2%'), textTransform: 'capitalize' }]} numberOfLines={2}>{item.nama}</Text>
                                            </View>
                                        </TouchableOpacity>
                                    )}
                                />
                            </View>

                            <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
                                <View style={{ width: wp('45%') }}>
                                    <Text style={[ c.light, f._16, { fontFamily: 'lineto-circular-pro-bold' }]}>Jenis Obat</Text>
                                </View>
                                <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                                    <TouchableOpacity onPress={() => this.props.navigation.push('ListMedicine')}>
                                        <Text style={[ c.blue ]}>Lihat Semua</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={{ width: wp('90%'), marginTop: wp('4%') }}>
                                <FlatList
                                    numColumns={3}
                                    data={this.state.jenis_obat}
                                    keyExtractor={item => item.id}
                                    renderItem={({item}) => (
                                        <View style={{ width: wp('30%'), alignItems: 'center' }}>
                                            <TouchableOpacity onPress={() => this.props.navigation.push('BuyMedicine')} style={[ p.center, { width: wp('28%'), height: wp('15%'), backgroundColor: blue, borderRadius: wp('2%'), marginTop: wp('2%') }]}>
                                                <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold', fontSize: wp('3.2%'), textTransform: 'capitalize' }]}>{item.nama}</Text>
                                            </TouchableOpacity>
                                        </View>
                                    )}
                                />
                            </View>

                            <Text style={[ c.light, f._16, { fontFamily: 'lineto-circular-pro-bold', marginTop: wp('5%') }]}>Pembelian Terpopuler</Text>
                            <View style={{ marginTop: wp('4%'), marginLeft: wp('5%'), width: wp('95%'), height: wp('50%') }}>
                                <FlatList
                                    horizontal={true}
                                    data={this.state.produk}
                                    keyExtractor={item => item.id}
                                    renderItem={({ item }) => (
                                        <>
                                            <View style={{ width: wp('60%'), height: '100%', backgroundColor: light, borderRadius: wp('1%'), alignItems: 'center' }}>
                                                <Image source={require('../asset/logo_combi.png')} style={{ width: wp('10%'), height: wp('10%'), position: 'absolute', zIndex: 1, left: wp('1%'), top: wp('1%') }} />
                                                <FastImage source={{ uri: API_URL+'/src/images/'+item.gambar }} style={{ width: wp('20%'), height: wp('25%') }} />
                                                <View style={{ width: '100%', height: wp('30%'), backgroundColor: blue, flexDirection: 'row' }}>
                                                    <View style={{ width: wp('35%'), height: '100%' }}>
                                                        <TouchableOpacity onPress={() => this.props.navigation.push('DetailProduct', { id_produk: item.id })}>
                                                            <Text style={[ c.light, f._16, { fontFamily: 'lineto-circular-pro-bold', marginLeft: wp('2%'), marginTop: wp('1%') }]} numberOfLines={2}>{item.nama}</Text>
                                                        </TouchableOpacity>
                                                        <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: wp('2%'), marginTop: wp('1%') }}>
                                                            <Text style={[ c.light, f._12 ]}>{this.convertToRupiah(item.harga)} / </Text>
                                                            <Text style={[ c.light, f._12 ]}>{item.satuan}</Text>
                                                        </View>
                                                        <View style={{ flexDirection: 'row', marginLeft: wp('2%'), marginTop: wp('2%'), alignItems: 'center' }}>
                                                            <Image source={require('../asset/star.png')} style={{ width: wp('4%'), height: wp('4%') }} />
                                                            <Text style={[ c.light, f._12, { marginLeft: wp('1%') }]}>5.0 | 10 Ulasan</Text>
                                                        </View>
                                                    </View>
                                                    <View style={{ width: wp('25%'), height: '100%', alignItems: 'center' }}>
                                                        {item.jumlah !== '0' ?
                                                            <View style={{ flexDirection: 'row', alignItems: 'center', borderRadius: wp('1%'), height: wp('7%'), marginTop: wp('13%') }}>
                                                                <TouchableOpacity onPress={() => this.decrementCount(item)} style={[ p.center, { width: wp('7%'), height: '100%', borderWidth: 1, borderColor: light, borderRadius: wp('2%') }]}>
                                                                    <Icon name='minus' size={10} color={light} />
                                                                </TouchableOpacity>
                                                                <View style={[ p.center, { width: wp('8.3%') }]}>
                                                                    <Text style={[ c.light ]}>{item.jumlah}</Text>
                                                                </View>
                                                                <TouchableOpacity onPress={() => this.incrementCount(item)} style={[ p.center, { width: wp('7%'), height: '100%', borderWidth: 1, borderColor: light, borderRadius: wp('2%') }]}>
                                                                    <Icon name='plus' size={10} color={light} />
                                                                </TouchableOpacity>
                                                            </View>
                                                            :
                                                            <TouchableOpacity onPress={() => this.addToCart(item)} style={[ p.center, { width: wp('20%'), height: wp('8%'), backgroundColor: light, borderRadius: wp('2%'), marginTop: wp('13%') }]}>
                                                                <Text style={[ c.blue, f._16, { fontFamily: 'lineto-circular-pro-bold' }]}>BELI</Text>
                                                            </TouchableOpacity>
                                                        }
                                                    </View>
                                                </View>
                                            </View>
                                            <View style={{ width: wp('5%') }} />
                                        </>
                                    )}
                                />
                            </View>

                            <Text style={[ f._16, { color: 'darkorange', marginTop: wp('4%'), fontFamily: 'lineto-circular-pro-bold' }]}>Artikel</Text>
                            {this.state.artikel !== undefined ?
                                <FlatList
                                    ListHeaderComponent={<View style={{ marginTop: wp('2.5%') }} />}
                                    data={this.state.artikel}
                                    keyExtractor={item => item.id_media}
                                    renderItem={({item}) => (
                                        <TouchableOpacity onPress={() => this.props.navigation.push('ArticleDetail', {id_media: item.id_media})} style={{ width: wp('90%'), flexDirection: 'row', marginBottom: wp('3%') }}>
                                            <FastImage source={require('../asset/batuk.jpg')} style={{ width: wp('25%'), height: wp('25%'), borderRadius: wp('2%') }} />
                                            <View style={{ width: wp('55%'), marginLeft: wp('3%') }}>
                                                <Text style={[ c.light, c.blue, { fontFamily: 'lineto-circular-pro-bold' }]}>{item.nama_media}</Text>
                                                <Text style={[ c.light, f._12, { marginTop: wp('2%') }]}>{item.created_at}</Text>
                                            </View>
                                        </TouchableOpacity>
                                    )}
                                />
                                :
                                <View style={{ width: wp('90%'), marginTop: wp('4%'), marginBottom: wp('4%'), alignItems: 'center', backgroundColor: '#121212', borderRadius: wp('2%') }}>
                                    <Image source={require('../asset/news.png')} style={{ width: wp('8%'), height: wp('8%'), tintColor: '#CCC', marginTop: wp('10%') }} />
                                    <Text style={{ color: '#CCC', marginTop: wp('1%'), marginBottom: wp('10%') }}>Tidak artikel yang tersedia</Text>
                                </View>
                            }
                        </View>
                    </ScrollView>
                </View>
            </>
        )
    }
}
