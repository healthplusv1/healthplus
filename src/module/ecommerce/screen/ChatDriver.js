import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Linking, TextInput, ScrollView, Image } from 'react-native'
import { blue, light, softGrey } from '../utils/Color'
import { f, c, b, p } from '../utils/StyleHelper'
import { xs } from '../utils/Responsive'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'

export default class ChatDriver extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        return (
            <View style={{ width: '100%', height: '100%', backgroundColor: '#181B22' }}>
                <View style={{ width: '100%', alignItems: 'center', backgroundColor: '#121212' }}>
                    <View style={{ width: wp('90%'), marginTop: wp('4%'), marginBottom: wp('4%'), flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ width: wp('15%') }}>
                            <TouchableOpacity onPress={() => this.props.navigation.pop()}>
                                <FastImage source={require('../asset/left.png')} style={{ width: wp('8%'), height: wp('8%') }} />
                            </TouchableOpacity>
                        </View>
                        <View style={{ width: wp('60%'), flexDirection: 'row', alignItems: 'center' }}>
                            <View style={{ width: wp('15%'), height: wp('15%'), backgroundColor: light, borderRadius: wp('10%'), alignItems: 'center', justifyContent: 'center' }}>
                                <Image source={require('../asset/user.png')} style={{ width: wp('10%'), height: wp('10%'), tintColor: '#CCC' }} />
                            </View>
                            <View style={{ marginLeft: wp('4%') }}>
                                <Text style={[ c.light, f._16, { fontFamily: 'lineto-circular-pro-bold' }]}>Eko</Text>
                                <Text style={[ c.light ]}>H 1234 AD</Text>
                                <Text style={[ c.light ]}>Vario</Text>
                            </View>
                        </View>
                        <View style={{ width: wp('15%'), alignItems: 'flex-end' }}>
                            <TouchableOpacity onPress={() => {Linking.openURL('tel:123')}}>
                                <Image source={require('../asset/phone.png')} style={{ width: wp('8%'), height: wp('8%'), tintColor: blue }} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>

                <ScrollView>
                    <View style={{ width: '100%', alignItems: 'center', marginTop: wp('4%') }}>
                        <View style={{ width: wp('90%') }}>
                            <View style={{ maxWidth: wp('65%'), backgroundColor: blue, borderRadius: wp('2%') }}>
                                <Text style={[ c.light, b.mt1, b.ml2, b.mr2, { textAlign: 'right' }]}>Selamat Siang. Silahkan ditunggu ya</Text>
                                <Text style={[ c.light, f._12, b.ml2, b.mr2, b.mb2, { textAlign: 'right' }]}>11:00 AM</Text>
                            </View>
                        </View>

                        <View style={{ width: wp('90%'), marginTop: wp('4%'), alignItems: 'flex-end' }}>
                            <View style={{ maxWidth: wp('65%'), backgroundColor: light, borderRadius: wp('2%') }}>
                                <Text style={[ b.mt1, b.ml2, b.mr2, { textAlign: 'right' }]}>Ya</Text>
                                <Text style={[ f._12, b.ml2, b.mr2, b.mb2, { textAlign: 'right' }]}>11:00 AM</Text>
                            </View>
                        </View>
                    </View>
                </ScrollView>

                <View style={{ width: '100%', backgroundColor: light, flexDirection: 'row', alignItems: 'center' }}>
                    <TextInput
                        style={{ width: wp('70%'), marginLeft: wp('5%') }}
                        placeholder='Tulis Pesan'
                        placeholderTextColor={softGrey}
                    />
                    <View style={{ width: wp('20%'), alignItems: 'flex-end' }}>
                        <TouchableOpacity>
                            <FastImage source={require('../asset/sendmessage_icon.png')} style={{ width: wp('7%'), height: wp('7%') }} />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}
