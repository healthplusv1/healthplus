import React, { Component } from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import { blue, light } from '../utils/Color'
import { f, c, b, p } from '../utils/StyleHelper'
import { xs } from '../utils/Responsive'
import { widthPercentageToDP as wp, heightPercentageToDP as hp, widthPercentageToDP } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'

export default class Disbute extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        return (
            <View style={{ width: '100%', height: '100%', backgroundColor: '#121212', alignItems: 'center' }}>
                <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('4%'), marginBottom: wp('4%') }}>
                    <View style={{ width: wp('15%') }}>
                        <TouchableOpacity onPress={() => this.props.navigation.pop()}>
                            <FastImage source={require('../asset/left.png')} style={{ width: wp('8%'), height: wp('8%') }} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ width: wp('60%'), alignItems: 'center' }}>
                        <Text style={[ c.light, f._18, { fontFamily: 'lineto-circular-pro-bold' }]}>Pusat Bantuan</Text>
                    </View>
                    <View style={{ width: wp('15%') }}></View>
                </View>

                <View style={{ width: '100%', height: wp('10%'), backgroundColor: '#181B22', alignItems: 'center', justifyContent: 'center' }}>
                    <View style={{ width: wp('90%') }}>
                        <Text style={[  c.light]}>Hubungi Kami</Text>
                    </View>
                </View>

                <TouchableOpacity style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('4%') }}>
                    <View style={{ width: wp('10%') }}>
                        <FastImage source={require('../asset/floatingbuttonapotekcs.png')} style={{ width: '100%', height: wp('10%') }} />
                    </View>
                    <View style={{ marginLeft: wp('4%') }}>
                        <Text style={[ c.light ]}>Chat dengan HealthPlus</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('4%') }}>
                    <View style={{ width: wp('10%'), height: wp('10%'), backgroundColor: '#1A69C6', borderRadius: wp('3%'), alignItems: 'center', justifyContent: 'center' }}>
                        <FastImage source={require('../asset/mail_icon.png')} style={{ width: xs(20), height: xs(15) }} />
                    </View>
                    <View style={{ marginLeft: wp('4%') }}>
                        <Text style={[ c.light ]}>Email</Text>
                        <Text style={[ c.light ]}>Tulis Pertanyaanmu sekarang</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}
