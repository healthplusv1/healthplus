import React, { Component } from 'react'
import { Text, View, TouchableOpacity, FlatList, TouchableHighlight, ScrollView, TextInput } from 'react-native'
import { blue, light, softGrey } from '../utils/Color'
import { f, b, c, p } from '../utils/StyleHelper'
import { xs } from '../utils/Responsive'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import axios from 'axios'

export default class PromoCode extends Component {
    constructor(props) {
        super(props);
        this.state = {
            promoCode: [
                {'key': '0', 'jenis_voucher': 'Gratis Ongkir', 'code': 'HPLUS10K', 'desc': 'Gratis Ongkir 10RB', 'for': 'Pengguna Baru', 'exp': 'Hingga 31 Maret 2021'},
                {'key': '1', 'jenis_voucher': 'Gratis Ongkir', 'code': 'HPLUS10K', 'desc': 'Gratis Ongkir 10RB', 'for': 'Pengguna Baru', 'exp': 'Hingga 31 Maret 2021'},
            ],
            checked: 0,
            promoCodeClicked: null
        };
        this.handleClick = this.handleClick.bind(this);
        this.handleDoubleClick = this.handleDoubleClick.bind(this);
    }

    async swicth(jenis_voucher, index) {
		await this.setState({
            checked: index,
            promoCodeClicked: jenis_voucher
        })
	}

    handleClick() {
        console.log('Clicked me once')
    }

    handleDoubleClick() {
        console.log('This is awesome \n Double tap success')
    }

    render() {
        return (
            <View style={{ width: '100%', height: '100%', backgroundColor: '#121212' }}>
                <View style={{ width: '100%', alignItems: 'center' }}>
                    <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('4%'), marginBottom: wp('4%') }}>
                        <View style={{ width: wp('15%') }}>
                            <TouchableOpacity onPress={() => this.props.navigation.pop()}>
                                <FastImage source={require('../asset/left.png')} style={{ width: wp('8%'), height: wp('8%') }} />
                            </TouchableOpacity>
                        </View>
                        <View style={{ width: wp('60%'), alignItems: 'center' }}>
                            <Text style={[ c.light, f._18, { fontFamily: 'lineto-circular-pro-bold' }]}>Pilih Kode Promo</Text>
                        </View>
                        <View style={{ width: wp('15%') }}></View>
                    </View>
                </View>

                <ScrollView>
                    <View style={{ width: '100%', alignItems: 'center' }}>
                        <View style={{ width: wp('90%'), marginTop: wp('2%') }}>
                            <FlatList
                                data={this.state.promoCode}
                                keyExtractor={item => item.key}
                                renderItem={({ item, index }) => (
                                    <TouchableOpacity>
                                        <FastImage source={require('../asset/couponpromo.png')} style={{ width: '100%', height: xs(105), marginBottom: wp('4%'), flexDirection: 'row', alignItems: 'center' }}>
                                            <View style={{ width: wp('27.5%'), height: '100%', alignItems: 'center', justifyContent: 'center' }}>
                                                <View style={{ width: wp('20%') }}>
                                                    <Text style={[ c.light, p.textCenter, f._18, { fontFamily: 'lineto-circular-pro-bold' }]}>{item.jenis_voucher}</Text>
                                                </View>
                                            </View>
                                            <View style={{ width: wp('47%'), height: '100%' }}>
                                                <Text style={[ c.blue, f.bold, f._16, { marginTop: wp('2%'), marginLeft: wp('4%') }]}>{item.code}</Text>
                                                <Text style={{ marginLeft: wp('4%'), marginTop: wp('2%') }}>{item.desc}</Text>
                                                <View style={{ marginLeft: wp('4%'), borderWidth: 1, borderColor: blue, width: wp('32%'), alignItems: 'center', borderRadius: wp('2%') }}>
                                                    <Text style={[ c.blue ]}>{item.for}</Text>
                                                </View>
                                                <Text style={[ f._12, { marginLeft: wp('4%'), color: softGrey, marginTop: wp('1%') }]}>{item.exp}</Text>
                                            </View>
                                            <View style={{ width: wp('15.5%'), height: '100%' }}>
                                                {this.state.checked === index ?
                                                    <View style={{ width: wp('5%'), height: wp('5%'), backgroundColor: blue, borderRadius: wp('5%'), marginLeft: wp('6%'), marginTop: wp('2%') }}></View>
                                                    :
                                                    <View style={{ width: wp('5%'), height: wp('5%'), borderWidth: 1, borderColor: blue, borderRadius: wp('5%'), marginLeft: wp('6%'), marginTop: wp('2%') }}></View>
                                                }
                                                <TouchableOpacity>
                                                    <Text style={[ c.blue, f._12, { marginLeft: wp('6%'), marginTop: wp('15%') }]}>S&K</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </FastImage>
                                    </TouchableOpacity>
                                )}
                            />
                        </View>
                    </View>
                </ScrollView>

                <View style={{ width: '100%', height: wp('30%'), backgroundColor: '#181B22', alignItems: 'center', justifyContent: 'center' }}>
                    <View style={{ width: wp('90%') }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <FastImage source={require('../asset/promocodekecil.png')} style={{ width: xs(32), height: xs(15) }} />
                            <Text style={[ c.light, f.bold, { marginLeft: wp('2%') }]}>Kode Promo</Text>
                        </View>

                        <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center', borderRadius: wp('1%'), backgroundColor: light, marginTop: wp('3%') }}>
                            <TextInput
                                style={{ width: wp('60%'), height: wp('12%'), marginLeft: wp('2%') }}
                                placeholder='Masukkan'
                            />
                            <TouchableHighlight underlayColor='#3399FF' onPress={() => this.props.navigation.pop()} style={{ width: wp('28%'), height: wp('12%'), backgroundColor: 'blue', alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={[ c.light ]}>Gunakan</Text>
                            </TouchableHighlight>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}
