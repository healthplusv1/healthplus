import React, { Component } from 'react'
import { Text, View, TouchableOpacity, ScrollView, RefreshControl, Image, Linking, TouchableHighlight } from 'react-native'
import { blue, light, grey, softGrey } from '../utils/Color'
import { b, f, c, p } from '../utils/StyleHelper'
import { xs } from '../utils/Responsive'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import CardFlip from 'react-native-card-flip'
import Dialog, { DialogButton, DialogFooter, DialogTitle, DialogContent } from 'react-native-popup-dialog'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'

export default class DetailPayment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            visible: false,
        }
    }

    componentDidMount() {
        this.setState({ loading: true })
        this.afterLoading()
    }

    afterLoading() {
        this.setState({ loading: false })
    }

    nextScreen() {
        this.props.navigation.push('AddReview')
        this.setState({
            visible: false
        })
    }

    format(number) {
        if (number) {
          var rupiah = "";
          var numberrev = number
            .toString()
            .split("")
            .reverse()
            .join("");
          for (var i = 0; i < numberrev.length; i++)
            if (i % 3 == 0) rupiah += numberrev.substr(i, 3) + ".";
          return (
            "Rp " +
            rupiah
              .split("", rupiah.length - 1)
              .reverse()
              .join("")
          );
        } else {
            return (
                "Rp "+number
            );
        }
    }

    render() {
        return (
            <RefreshControl refreshing={this.state.loading} onRefresh={() => this.componentDidMount()} style={{ width: '100%', height: '100%' }}>
                <Dialog
                    visible={this.state.visible}
                    onTouchOutside={() => this.setState({ visible: false })}
                >
                    <DialogContent>
                        <View style={{ width: wp('60%'), height: wp('85%') }}>
                            <CardFlip style={{ width: '100%', height: '100%' }} ref={(card) => this.card = card}>
                                <View style={{ width: '100%', alignItems: 'center' }}>
                                    <Image source={require('../asset/konfirmasipesanan.png')} style={{ width: xs(125), height: xs(103), marginTop: wp('8%') }} />
                                    <Text style={[ f._16, f.bold, { marginTop: wp('8%') }]}>Konfirmasi Pesanan</Text>
                                    <View style={{ width: wp('55%') }}>
                                        <Text style={{ marginTop: wp('2%'), textAlign: 'center' }}>Mohon tekan tombol dibawah jika pesanan sudah diterima</Text>
                                    </View>
                                    <TouchableOpacity onPress={() => this.card.flip()} style={[ p.center, { width: wp('35%'), height: hp('6%'), backgroundColor: blue, borderRadius: wp('5%'), marginTop: wp('8%'), marginBottom: wp('2%') }]}>
                                        <Text style={[ c.light, f._16 ]}>Konfirmasi</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{ width: '100%', alignItems: 'center' }}>
                                    <View style={{ width: wp('20%'), height: wp('20%'), marginTop: wp('8%'), borderRadius: wp('20%'), borderWidth: 4, borderColor: blue, alignItems: 'center', justifyContent: 'center' }}>
                                        <Image source={require('../asset/check.png')} style={{ width: wp('10%'), height: wp('10%'), tintColor: blue }} />
                                    </View>
                                    <View style={{ width: wp('58%'), marginTop: wp('8%') }}>
                                        <Text style={[ f._16, f.bold, { textAlign: 'center' }]}>Terima Kasih sudah menggunakan layanan kami</Text>
                                    </View>
                                    <TouchableOpacity onPress={() => this.nextScreen()} style={[ p.center, { width: wp('35%'), height: hp('6%'), backgroundColor: blue, borderRadius: wp('5%'), marginTop: wp('4%') }]}>
                                        <Text style={[ c.light, f._16 ]}>Beri Rating</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => this.setState({ visible: false })} style={[ p.center, { width: wp('35%'), height: hp('6%'), borderWidth: 1, borderColor: blue, borderRadius: wp('5%'), marginTop: wp('4%'), marginBottom: wp('2%') }]}>
                                        <Text style={[ c.blue, f._16 ]}>Nanti saja</Text>
                                    </TouchableOpacity>
                                </View>
                            </CardFlip>
                        </View>
                    </DialogContent>
                </Dialog>
                <ScrollView style={{ backgroundColor: '#121212' }}>
                    <View style={{ width: '100%', alignItems: 'center', marginTop: wp('4%') }}>
                        <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
                            <View style={{ width: wp('15%') }}>
                                <TouchableOpacity onPress={() => this.props.navigation.push('HistoryPayment')}>
                                    <FastImage source={require('../asset/left.png')} style={{ width: wp('8%'), height: wp('8%') }} />
                                </TouchableOpacity>
                            </View>
                            <View style={[ p.center, { width: wp('60%') }]}>
                                <Text style={[ c.light, f._18, { fontFamily: 'lineto-circular-pro-bold' }]}>Detail Pembelian</Text>
                            </View>
                            <View style={{ width: wp('15%'), alignItems: 'flex-end' }}>
                                <TouchableOpacity onPress={() => this.props.navigation.push('Disbute')}>
                                    <Image source={require('../asset/help.png')} style={{ width: wp('8%'), height: wp('8%'), tintColor: blue }} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>

                    <View style={{ width: '100%', backgroundColor: '#181B22', alignItems: 'center', marginTop: wp('4%') }}>
                        <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('4%') }}>
                            <View style={{ width: wp('45%') }}>
                                <Text style={[ c.light ]}>No.Referensi</Text>
                            </View>
                            <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                                <Text style={[ c.light ]}>123456789</Text>
                            </View>
                        </View>
                        <View style={{ borderBottomWidth: 1, borderBottomColor: light, width: wp('90%'), marginTop: wp('4%') }} />
                        <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('4%'), marginBottom: wp('4%') }}>
                            <View style={{ width: wp('45%') }}>
                                <Text style={[ c.light ]}>Waktu</Text>
                            </View>
                            <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                                <Text style={[ c.light ]}>14:00 WIB, 03/17/2021</Text>
                            </View>
                        </View>
                    </View>

                    <View style={{ width: '100%', alignItems: 'center', marginTop: wp('4%') }}>
                        <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
                            <View style={{ width: wp('10%'), height: wp('10%'), backgroundColor: light, borderRadius: wp('5%'), alignItems: 'center', justifyContent: 'center' }}>
                                <FastImage source={require('../asset/logo_apotek.png')} style={{ width: wp('6%'), height: wp('6%') }} />
                            </View>
                            <Text style={[ c.light, { marginLeft: wp('2%') }]}>Apotek Sehati</Text>
                        </View>
                        <View style={{ borderBottomWidth: 1, borderBottomColor: light, width: '100%', marginTop: wp('3%') }} />
                        <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('4%') }}>
                            <View style={{ width: wp('25%') }}>
                                <View style={{ width: '100%', height: wp('25%'), backgroundColor: light, borderRadius: wp('2%'), alignItems: 'center', justifyContent: 'center' }}>
                                    <Image source={require('../asset/logo_combi_small.png')} style={{ width: wp('5%'), height: wp('5%'), position: 'absolute', left: wp('1%'), top: wp('1%'), zIndex: 1 }} />
                                    <Image source={require('../asset/10000289_1_small.png')} style={{ width: wp('16%'), height: wp('16%') }} />
                                </View>
                            </View>
                            <View style={{ width: wp('45%') }}>
                                <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold', marginLeft: wp('2%') }]}>OBH Combi Batuk dan Flu</Text>
                            </View>
                            <View style={{ width: wp('20%'), alignItems: 'flex-end' }}>
                                <Text style={[ c.light, { marginTop: wp('15%') }]}>x1</Text>
                                <Text style={[ c.light ]}>Rp 14.500</Text>
                            </View>
                        </View>

                        <View style={{ width: '100%', borderBottomWidth: 1, borderBottomColor: light, marginTop: wp('4%') }} />

                        <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('4%') }}>
                            <View style={{ width: wp('45%') }}>
                                <Text style={[ c.light ]}>1 produk</Text>
                            </View>
                            <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                                <Text style={[ c.light ]}>Total Pesanan: {this.format('14500')}</Text>
                            </View>
                        </View>

                        <View style={{ width: wp('90%'), borderBottomWidth: 1, borderBottomColor: light, marginTop: wp('4%'), marginBottom: wp('4%') }} />

                        <View style={{ width: '100%', alignItems: 'center', backgroundColor: '#181B22' }}>
                            <View style={{ width: wp('90%'), marginTop: wp('4%'), flexDirection: 'row' }}>
                                <View style={{ width: wp('20%'), height: wp('20%'), backgroundColor: light, borderRadius: wp('15%'), alignItems: 'center', justifyContent: 'center' }}>
                                    <FastImage source={require('../asset/logo_apotek.png')} style={{ width: wp('15%'), height: wp('15%') }} />
                                </View>
                                <View style={{ marginLeft: wp('4%'), marginTop: wp('2%') }}>
                                    <Text style={[ c.light, f._16, { fontFamily: 'lineto-circular-pro-bold' }]}>Apotek Sehati</Text>
                                    <Text style={[ c.light ]}>Ruko Mataram</Text>
                                    <Text style={[ c.light ]}>Semarang</Text>
                                </View>
                            </View>
                            <View style={{ width: '100%', marginTop: wp('4%'), flexDirection: 'row', alignItems: 'center' }}>
                                <TouchableOpacity onPress={() => {Linking.openURL('tel:123')}} style={{ width: wp('50%'), height: hp('8%'), borderWidth: 1, borderColor: blue, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                                    <Image source={require('../asset/phone.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: blue }} />
                                    <Text style={[ c.blue, { marginLeft: wp('2%') }]}>Call</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.props.navigation.push('ChatPharmacy')} style={{ width: wp('50%'), height: hp('8%'), borderWidth: 1, borderColor: blue, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                                    <Image source={require('../asset/chatBubble.png')} style={{ width: wp('6%'), height: wp('6%'), transform: [{ rotateY: '180deg' }] }} />
                                    <Text style={[ c.blue, { marginLeft: wp('2%') }]}>Chat</Text>
                                </TouchableOpacity>
                            </View>
                        </View>

                        <View style={{ width: wp('90%'), flexDirection: 'row', marginTop: wp('4%') }}>
                            <View style={{ width: wp('10%'), alignItems: 'center' }}>
                                <View style={{ width: wp('8%'), height: wp('8%'), backgroundColor: '#FE8829', borderRadius: wp('5%'), alignItems: 'center', justifyContent: 'center' }}>
                                    <Image source={require('../asset/check.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: light }} />
                                </View>
                                <View style={{ width: wp('1%'), height: wp('4%'), backgroundColor: '#FE8829', marginTop: xs(-1) }} />
                                <View style={{ width: wp('8%'), height: wp('8%'), backgroundColor: '#FE8829', borderRadius: wp('5%'), alignItems: 'center', justifyContent: 'center', marginTop: xs(-1) }}>
                                    <Image source={require('../asset/check.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: light }} />
                                </View>
                                <View style={{ width: wp('1%'), height: wp('4%'), backgroundColor: '#FE8829', marginTop: xs(-1) }} />
                                <View style={{ width: wp('8%'), height: wp('8%'), backgroundColor: '#FE8829', borderRadius: wp('5%'), alignItems: 'center', justifyContent: 'center', marginTop: xs(-1) }}>
                                    <Image source={require('../asset/check.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: light }} />
                                </View>
                                <View style={{ width: wp('1%'), height: wp('4%'), backgroundColor: '#FE8829', marginTop: xs(-1) }} />
                                <View style={{ width: wp('8%'), height: wp('8%'), backgroundColor: light, borderRadius: wp('5%'), alignItems: 'center', justifyContent: 'center', marginTop: xs(-1) }}>
                                    {/* <Image source={require('../asset/check.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: light }} /> */}
                                    <View style={{ width: wp('3%'), height: wp('3%'), borderRadius: wp('3%'), backgroundColor: '#FE8829' }}></View>
                                </View>
                                <View style={{ width: wp('1%'), height: wp('4%'), backgroundColor: light, marginTop: xs(-1) }} />
                                <View style={{ width: wp('8%'), height: wp('8%'), backgroundColor: light, borderRadius: wp('5%'), alignItems: 'center', justifyContent: 'center', marginTop: xs(-1) }}>
                                    {/* <Image source={require('../asset/check.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: light }} /> */}
                                </View>
                            </View>
                            <View style={{ width: wp('75%'), marginLeft: wp('5%') }}>
                                <Text style={[ c.light, { marginTop: wp('1.5%') }]}>Driver sedang menuju apotek</Text>
                                <Text style={[ c.light, { marginTop: wp('6%') }]}>Driver sampai di apotek</Text>
                                <Text style={[ c.light, { marginTop: wp('6%') }]}>Driver sudah membawa barang pesananmu</Text>
                                <Text style={[ c.light, { marginTop: wp('6.5%') }]}>Driver sedang menuju lokasi pengiriman</Text>
                                <Text style={[ c.light, { marginTop: wp('6.5%') }]}>Pelanggan telah menerima pesanan</Text>
                            </View>
                        </View>

                        <View style={{ width: wp('90%'), marginTop: wp('4%'), flexDirection: 'row', alignItems: 'center' }}>
                            <View style={{ width: wp('45%'), flexDirection: 'row', alignItems: 'center' }}>
                                <FastImage source={require('../asset/estimated_icon.png')} style={{ width: wp('6%'), height: wp('6%') }} />
                                <View style={{ marginLeft: wp('4%') }}>
                                    <Text style={{ color: '#FC8C24' }}>Estimasi Kedatangan</Text>
                                    <Text style={{ color: '#FC8C24' }}>10 min</Text>
                                </View>
                            </View>
                            <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                                <TouchableHighlight onPress={() => this.props.navigation.push('Maps')} underlayColor='#3399FF' style={{ backgroundColor: blue, borderRadius: wp('5%') }}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: wp('2%'), marginRight: wp('2%'), marginTop: wp('2.5%'), marginBottom: wp('2.5%') }}>
                                        <FastImage source={require('../asset/map_icon.png')} style={{ width: xs(20), height: xs(15) }} />
                                        <Text style={[ c.light, f._12, { marginLeft: wp('2%') }]}>Lihat posisi driver</Text>
                                    </View>
                                </TouchableHighlight>
                            </View>
                        </View>

                        <View style={{ width: '100%', alignItems: 'center', backgroundColor: '#181B22', marginTop: wp('4%') }}>
                            <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('4%'), marginBottom: wp('4%') }}>
                                <View style={{ width: wp('20%'), alignItems: 'center', justifyContent: 'center' }}>
                                    <View style={{ width: '100%', height: wp('20%'), backgroundColor: softGrey, borderRadius: wp('10%'), alignItems: 'center', justifyContent: 'center' }}>
                                        <Image source={require('../asset/user.png')} style={{ width: wp('15%'), height: wp('15%'), tintColor: '#CCC' }} />
                                    </View>
                                </View>
                                <View style={{ width: wp('50%') }}>
                                    <Text style={[ c.light, f._16, { marginLeft: wp('3%') }]}>Driver Identitas</Text>
                                    <Text style={[ c.light, f._16, { marginLeft: wp('3%'), fontFamily: 'lineto-circular-pro-bold' }]}>Eko</Text>
                                    <Text style={[ c.light, f._16, { marginLeft: wp('3%') }]}>H 1234 AD</Text>
                                    <Text style={[ c.light, f._16, { marginLeft: wp('3%') }]}>Vario</Text>
                                </View>
                                <View style={{ width: wp('20%') }}>
                                    <TouchableOpacity onPress={() => this.props.navigation.push('ChatDriver')} style={{ flexDirection: 'row', alignItems: 'center', marginTop: wp('18%') }}>
                                        <Image source={require('../asset/chatBubble.png')} style={{ width: wp('6%'), height: wp('6%'), transform: [{ rotateY: '180deg' }] }} />
                                        <Text style={[ c.blue, { marginLeft: wp('2%') }]}>Chat</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                        
                        <TouchableOpacity onPress={() => this.setState({ visible: true })} style={[ p.center, { width: wp('35%'), height: hp('6%'), borderRadius: wp('1%'), backgroundColor: blue, marginTop: wp('4%') }]}>
                            <Text style={[ c.light, f._16, { fontFamily: 'lineto-circular-pro-bold' }]}>Simpan</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.props.navigation.pop()} style={[ p.center, { width: wp('35%'), height: hp('6%'), borderRadius: wp('1%'), borderWidth: 1, borderColor: light, marginTop: wp('4%'), marginBottom: wp('4%') }]}>
                            <Text style={[ c.light, f._16, { fontFamily: 'lineto-circular-pro-bold' }]}>Kembali</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </RefreshControl>
        )
    }
}
