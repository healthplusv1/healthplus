import React, { Component } from 'react'
import { Text, View, TouchableOpacity, ScrollView, FlatList, RefreshControl, Image, ToastAndroid, Alert } from 'react-native'
import { blue, light } from '../utils/Color'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import Icon from 'react-native-vector-icons/FontAwesome'
import { NavigationEvents } from 'react-navigation'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { API_URL } from 'react-native-dotenv'

export default class Cart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cart: [],
            count: '',
            loading: false,
            check: [],
        }
    }

    componentDidMount() {
        this.setState({ loading: true })

        axios.get(API_URL+'/main/shopping/cart', {params: {
            id_login: '118205008143923829902'
        }})
        .then(res => {
            // console.log(res.data.data.length)
            // res.data.products.forEach((value, key) => {

            //     res.data.data.forEach((v,k) => {
            //         if (String(v.produk_id) === String(value.id)) {
            //             res.data.products[key].jumlah = res.data.data[k].jumlah
            //         }
            //     })
            // })

            // res.data.products.forEach((value, key) => {
            //     if (value.jumlah === undefined) {
            //         res.data.products[key].jumlah='0'
            //     }
            // })

            this.setState({
                loading: false,
                count: res.data.data.length,
                cart: res.data.data
            })
            // console.log(this.state.cart)
        })
    }

    incrementCount = (item) => {
        this.setState({ loading: true })

        axios.post(API_URL+'/main/shopping/cart/'+item.produk_id+'/add', {id_login: '118205008143923829902', jumlah: 1})
        .then(resp => {
            // console.log(resp.data.data)
            this.componentDidMount()
            ToastAndroid.show('Added Products '+item.produk.nama+'', ToastAndroid.SHORT)
        })
        .catch(err => {
            console.log('add_to_cart: '+err)
        })
    }

    decrementCount = (item) => {
        if (item.jumlah === '1') {
            Alert.alert(
                "",
                "Apakah Anda yakin ingin menghapus?",
                [
                  {
                    text: 'Tidak', onPress: () => console.log('Cancel Pressed'), style: "cancel"
                  },
                  { text: 'Ya', onPress: () => {
                    axios.get(API_URL+'/main/shopping/cart/'+item.id+'/remove')
                    .then(res => {
                        // console.log(res.data.status)
                        this.componentDidMount()
                        if (res.data.status === 'true') {
                            ToastAndroid.show('Your order has been deleted', ToastAndroid.SHORT)
                        }
                    })
                  }}
                ],  
                { cancelable: false }
            );
            return true;
        } else {
            axios.post(API_URL+'/main/shopping/cart/'+item.produk_id+'/add', {id_login: '118205008143923829902', jumlah: -1})
            .then(resp => {
                // console.log(resp.data.data)
                // console.log('Jumlah: '+jumlah)
                this.componentDidMount()
                ToastAndroid.show('Reduce Your Product '+item.produk.nama+'', ToastAndroid.SHORT)
            })
            .catch(err => {
                console.log('add_to_cart: '+err)
            })
        }
    }

    removeProduct(item) {
        Alert.alert(
            "",
            "Apakah Anda yakin ingin menghapus?",
            [
                {text: 'Tidak', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                {text: 'Ya', onPress: () => {
                    this.setState({ loading: true })
                    axios.get(API_URL+'/main/shopping/cart/'+item.id+'/remove')
                    .then(res => {
                        // console.log(res.data.status)
                        this.componentDidMount()
                        if (res.data.status === 'true') {
                            ToastAndroid.show('Your order has been deleted', ToastAndroid.SHORT)
                        }
                    })
                }}
            ],
            {cancelable: false}
        );
        return true;
    }

    format(number) {
        if (number) {
          var rupiah = "";
          var numberrev = number
            .toString()
            .split("")
            .reverse()
            .join("");
          for (var i = 0; i < numberrev.length; i++)
            if (i % 3 == 0) rupiah += numberrev.substr(i, 3) + ".";
          return (
            "Rp " +
            rupiah
              .split("", rupiah.length - 1)
              .reverse()
              .join("")
          );
        } else {
            return (
                "Rp "+number
            );
        }
    }

    render() {
        const { cart } = this.state;
        let totalQuantity = 0;
        let totalPrice = 0;
        cart.forEach((item) => {
            totalQuantity += item.jumlah;
            totalPrice += item.jumlah * item.produk.harga;
        })
        return (
            <>
                <NavigationEvents onDidFocus={() => this.componentDidMount()} />
                <View style={{ width: '100%', height: '100%', backgroundColor: '#121212' }}>
                    <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center', marginTop: wp('4%') }}>
                        <View style={{ width: wp('20%') }}>
                            <TouchableOpacity onPress={() => this.props.navigation.pop()} style={{ marginLeft: wp('4%') }}>
                                <FastImage source={require('../asset/left.png')} style={{ width: wp('8%'), height: wp('8%') }} />
                            </TouchableOpacity>
                        </View>
                        <View style={[ p.center, { width: wp('60%') }]}>
                            <Text style={[ c.light, f._18, { fontFamily: 'lineto-circular-pro-bold' }]}>Keranjang</Text>
                        </View>
                        <View style={{ width: wp('20%') }}></View>
                    </View>
                    <ScrollView refreshControl={<RefreshControl onRefresh={() => this.componentDidMount()} refreshing={this.state.loading} />}>
                        <View style={{ width: '100%', alignItems: 'center' }}>
                            <View style={{ width: wp('75%'), marginTop: wp('5%') }}>
                                <FlatList
                                    data={this.state.cart}
                                    keyExtractor={item => item.id}
                                    renderItem={({item}) => (
                                        <>
                                            <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center' }}>
                                                <View style={{ width: wp('25%') }}>
                                                    <FastImage source={{ uri: API_URL+'/src/images/'+item.produk.gambar}} style={{ width: wp('22%'), height: wp('22%') }} />
                                                </View>
                                                <View style={{ width: wp('25%') }}>
                                                    <View>
                                                        <Text style={[ c.light ]}>{item.produk.nama}</Text>
                                                    </View>
                                                    <View style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: blue, borderRadius: wp('1%'), height: wp('7%'), marginTop: wp('4%') }}>
                                                        <TouchableOpacity onPress={() => this.decrementCount(item)} style={[ p.center, { width: wp('8.3%') }]}>
                                                            <Icon name='minus' size={10} color={light} />
                                                        </TouchableOpacity>
                                                        <View style={[ p.center, { width: wp('8.3%') }]}>
                                                            <Text style={[ c.light ]}>{item.jumlah}</Text>
                                                        </View>
                                                        <TouchableOpacity onPress={() => this.incrementCount(item)} style={[ p.center, { width: wp('8.3%') }]}>
                                                            <Icon name='plus' size={10} color={light} />
                                                        </TouchableOpacity>
                                                    </View>
                                                </View>
                                                <View style={{ width: wp('25%'), alignItems: 'flex-end' }}>
                                                    <TouchableOpacity onPress={() => this.removeProduct(item)}>
                                                        <Image source={require('../asset/trash.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: blue }} />
                                                    </TouchableOpacity>
                                                    <Text style={[ c.light, { marginTop: wp('5%') }]}>{this.format(item.jumlah * item.produk.harga)}</Text>
                                                </View>
                                            </View>
                                            <View style={{ borderBottomWidth: 1, borderBottomColor: blue, marginTop: wp('2%'), marginBottom: wp('4%') }} />
                                        </>
                                    )}
                                />
                            </View>
                        </View>
                    </ScrollView>
                    <View style={{ width: '100%', height: hp('20%'), position: 'relative', bottom: 0, backgroundColor: '#181B22', alignItems: 'center' }}>
                        <View style={{ width: wp('75%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('5%') }}>
                            <View style={{ width: wp('37.5') }}>
                                <Text style={[ c.light ]}>Jumlah produk ({this.state.count})</Text>
                            </View>
                            <View style={{ width: wp('37.5'), alignItems: 'flex-end' }}>
                                <Text style={[ c.light, { fontFamily: 'lineto_circular-pro-bold' }]}>{this.format(totalPrice)}</Text>
                            </View>
                        </View>
                        <TouchableOpacity onPress={() => this.props.navigation.push('Payment')} style={[ p.center, { width: wp('75%'), height: hp('9%'), backgroundColor: this.state.count !== 0 ? blue : '#CCC', borderRadius: wp('2%'), marginTop: wp('5%') }]} disabled={this.state.count === 0}>
                            <Text style={[ c.light, f._18, { fontFamily: 'lineto-circular-pro-bold' }]}>Buat Pesanan</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </>
        )
    }
}
