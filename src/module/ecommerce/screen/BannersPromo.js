import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Image, TextInput, ScrollView } from 'react-native'
import { blue, light, softGrey } from '../utils/Color'
import { f, c, b, p } from '../utils/StyleHelper'
import { xs } from '../utils/Responsive'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'

export default class BannersPromo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            search: ''
        }
    }

    render() {
        return (
            <View style={{ width: '100%', height: '100%', backgroundColor: '#121212' }}>
                <View style={{ width: '100%', alignItems: 'center' }}>
                    <View style={{ width: wp('90%'), marginTop: wp('4%'), marginBottom: wp('4%'), flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ width: wp('45%') }}>
                            <FastImage source={require('../asset/healthPlus.png')} style={{ width: xs(160), height: xs(38) }} />
                        </View>
                        <View style={{ width: wp('45%'), flexDirection: 'row-reverse' }}>
                            <TouchableOpacity onPress={() => this.props.navigation.push('Cart')}>
                                <View style={{ width: wp('3.5%'), height: wp('3.5%'), backgroundColor: 'orange', position: 'absolute', borderRadius: wp('3.5%'), zIndex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={[ c.light, f._8 ]}>3</Text>
                                </View>
                                <Image source={require('../asset/addCart.png')} style={{ width: wp('7%'), height: wp('7%'), tintColor: light }} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.props.navigation.push('HistoryPayment')} style={{ marginRight: wp('2%') }}>
                                <Image source={require('../asset/order_icon.png')} style={{ width: wp('7%'), height: wp('7%'), tintColor: light }} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.props.navigation.push('FrequentlyOrder')} style={{ marginRight: wp('2%') }}>
                                <Image source={require('../asset/Frequentlyorder_icon.png')} style={{ width: wp('8%'), height: wp('7%'), tintColor: light }} />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={{ width: wp('90%'), height: hp('6.5%'), borderRadius: wp('5%'), backgroundColor: softGrey, marginTop: wp('4%'), flexDirection: 'row', alignItems: 'center' }}>
                        <TextInput
                            style={[ c.light, { width: wp('62%'), height: '100%', marginLeft: wp('4%') }]}
                            ref={ref => this.textInputRef = ref}
                            placeholder='Bagaimana keadaanmu hari ini?'
                            placeholderTextColor={light}
                            onChangeText={(text) => this.setState({ search: text })}
                            value={this.state.search}
                        />
                        <View style={{ width: wp('20%'), flexDirection: 'row-reverse' }}>
                            {this.state.search === '' ?
                                <TouchableOpacity onPress={() => this.textInputRef.focus()}>
                                    <Image source={require('../asset/search.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: light, marginLeft: wp('4%') }} />
                                </TouchableOpacity>
                                :
                                <TouchableOpacity onPress={() => this.setState({ search: '' })} style={{ marginLeft: wp('4%') }}>
                                    <Image source={require('../asset/cross.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: light }} />
                                </TouchableOpacity>
                            }
                            {/* <TouchableOpacity>
                                <Image source={require('../asset/camera.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: light }} />
                            </TouchableOpacity> */}
                        </View>
                    </View>
                </View>

                <ScrollView style={{ marginTop: wp('4%') }}>
                    <View style={{ width: '100%', alignItems: 'center' }}>
                        <View style={{ width: wp('90%') }}>
                            {this.props.navigation.getParam('id_banner') === 0 && (
                                <FastImage source={require('../asset/bannerapotek_1-430.png')} style={{ width: '100%', height: xs(135) }} />
                            )}
                            {this.props.navigation.getParam('id_banner') === 1 && (
                                <FastImage source={require('../asset/bannerapotek_2-430.png')} style={{ width: '100%', height: xs(135) }} />
                            )}
                            {this.props.navigation.getParam('id_banner') === 2 && (
                                <FastImage source={require('../asset/bannerapotek_3-430.png')} style={{ width: '100%', height: xs(135) }} />
                            )}
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}
