import React, { Component } from 'react'
import { Text, View, TouchableOpacity, ScrollView } from 'react-native'
import { blue, light, softGrey } from '../utils/Color'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { API_URL } from 'react-native-dotenv'

export default class ArticleDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            nama_media: '',
            deskripsi: '',
            created: ''
        }
    }

    componentDidMount() {
        axios.get(API_URL+'/main/article_detail', {params: {
            id_media: this.props.navigation.getParam('id_media')
        }})
        .then(res => {
            // console.log(res.data)
            this.setState({
                loading: false,
                nama_media: res.data.nama_media,
                deskripsi: res.data.deskripsi,
                created: res.data.created_at
            })
        })
    }

    render() {
        return (
            <ScrollView style={{ backgroundColor: '#121212' }}>
                <FastImage source={require('../asset/batukone.jpg')} style={{ width: '100%', height: hp('30%') }}>
                    <TouchableOpacity onPress={() => this.props.navigation.pop()} style={[ p.center, { marginLeft: wp('4%'), marginTop: wp('4%'), width: wp('8%'), height: wp('8%'), backgroundColor: '#121212', borderRadius: wp('5%') }]}>
                        <FastImage source={require('../asset/left.png')} style={{ width: wp('6%'), height: wp('6%') }} />
                    </TouchableOpacity>
                </FastImage>

                <View style={{ width: '100%', alignItems: 'center' }}>
                    <View style={{ width: wp('90%'),  marginTop: wp('2%') }}>
                        <Text style={[ f._12, { color: softGrey }]}>Ilustrasi Foto</Text>
                        <Text style={[ c.light, f._18, { fontFamily: 'lineto-circular-pro-bold', marginTop: wp('4%') }]}>HEALTHPLUS Artikel</Text>
                        <Text style={[ c.light, f._18, { fontFamily: 'lineto-circular-pro-bold', marginTop: wp('2%') }]}>{this.state.nama_media}</Text>
                    </View>
                </View>

                <View style={{ borderBottomWidth: 2, borderBottomColor: blue, width: '100%', marginTop: wp('4%'), marginBottom: wp('2%') }} />

                <View style={{ width: '100%', alignItems: 'center' }}>
                    <View style={{ width: wp('90%') }}>
                        <Text style={[ f._12, { color: softGrey }]}>Admin - HealthPlus Artikel</Text>
                        <Text style={[ f._12, { color: softGrey }]}>{this.state.created}</Text>
                        <Text style={[ c.light, { marginTop: wp('2%') }]}>{this.state.deskripsi}</Text>
                        
                        <Text style={[ c.light, { marginTop: wp('4%'), marginBottom: wp('4%') }]}>Dikutip dari Alodokter.com</Text>
                    </View>
                </View>
            </ScrollView>
        )
    }
}
