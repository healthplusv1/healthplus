import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { blue, light } from '../utils/Color'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete'

export default class SearchAddress extends Component {
    render() {
        return (
            <View style={{ width: '100%', height: '100%', alignItems: 'center', backgroundColor: '#CCC' }}>
                <GooglePlacesAutocomplete
                    placeholder='Search'
                    numberOfLines={2}
                    styles={{
                        textInputContainer: { width: wp('90%'), marginTop: wp('4%') }
                    }}
                    query={{
                        key: 'AIzaSyC_0rTZThOJL3POoTHYh4cXQ087aaKqfCA',
                        language: 'en',
                        types:'(cities)'
                    }}
                />
            </View>
        )
    }
}
