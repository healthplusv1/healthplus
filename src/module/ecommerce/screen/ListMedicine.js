import React, { Component } from 'react'
import { Text, View, ScrollView, Image, TouchableOpacity, TextInput, SectionList, FlatList, Animated } from 'react-native'
import { blue, light, softGrey } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import { AlphabetList } from 'react-native-section-alphabet-list'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'
import { API_URL } from 'react-native-dotenv'

export default class ListMedicine extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            search: '',
            alphabet: [
                {key: '1', huruf: 'A'},
                {key: '2', huruf: 'B'},
                {key: '3', huruf: 'C'},
                {key: '4', huruf: 'D'},
                {key: '5', huruf: 'E'},
                {key: '6', huruf: 'F'},
                {key: '7', huruf: 'G'},
                {key: '8', huruf: 'H'},
                {key: '9', huruf: 'I'},
                {key: '10', huruf: 'J'},
                {key: '11', huruf: 'K'},
                {key: '12', huruf: 'L'},
                {key: '13', huruf: 'M'},
                {key: '14', huruf: 'N'},
                {key: '15', huruf: 'O'},
                {key: '16', huruf: 'P'},
                {key: '17', huruf: 'Q'},
                {key: '18', huruf: 'R'},
                {key: '19', huruf: 'S'},
                {key: '20', huruf: 'T'},
                {key: '21', huruf: 'U'},
                {key: '22', huruf: 'V'},
                {key: '23', huruf: 'W'},
                {key: '24', huruf: 'X'},
                {key: '25', huruf: 'Y'},
                {key: '26', huruf: 'Z'},
            ],
            contacts: [
                {key: '1', value: 'Asam Lambung'},
                {key: '2', value: 'Asam Urat'},
                {key: '3', value: 'Anemia'},
                {key: '4', value: 'Aids'},
                {key: '5', value: 'Ambeien'},
                {key: '7', value: 'Batuk'},
                {key: '8', value: 'Batuk Kering'},
                {key: '9', value: 'Batuk Berdahak'},
                {key: '10', value: 'Batu Ginjal'},
                {key: '11', value: 'Batu Empedu'},
                {key: '12', value: 'Campak'},
                {key: '13', value: 'Cacar Air'},
                {key: '14', value: 'Cacar Monyet'},
                {key: '15', value: 'Cacing Kremi'},
                {key: '16', value: 'Cantengan'},
                {key: '17', value: 'Diabetes'},
                {key: '18', value: 'Demam'},
                {key: '19', value: 'Demam Berdarah'},
                {key: '20', value: 'Diare'},
                {key: '21', value: 'Disentri'},
                {key: '22', value: 'Epilepsi'},
                {key: '23', value: 'E. coli'},
                {key: '24', value: 'Empiema'},
                {key: '25', value: 'Edema Paru'},
                {key: '26', value: 'Entropion'},
                {key: '27', value: 'Flu'},
                {key: '28', value: 'Flu Babi'},
                {key: '29', value: 'Flu Burung'},
                {key: '30', value: 'Fobia'},
                {key: '8231', value: 'Fobia Social'},
                {key: '32', value: 'Gagal Jantung'},
                {key: '33', value: 'Gagal Ginjal Akut'},
                {key: '34', value: 'Gondongan'},
                {key: '35', value: 'Glaukoma'},
                {key: '36', value: 'Gastritis'},
                {key: '37', value: 'HIV dan AIDS'},
                {key: '38', value: 'Hipotensi'},
                {key: '39', value: 'Hipoksida'},
                {key: '40', value: 'Hipertensi'},
                {key: '41', value: 'Herpes'},
                {key: '42', value: 'Insomnia'},
                {key: '43', value: 'Impetigo'},
                {key: '44', value: 'Impotensi'},
                {key: '45', value: 'Infeksi Kulit'},
                {key: '46', value: 'Infeksi Aliran Darah'},
                {key: '47', value: 'Jantung Berdebar'},
                {key: '48', value: 'Jamur Kuku'},
                {key: '49', value: 'Jamur Kulit'},
                {key: '50', value: 'Jerawat'},
                {key: '51', value: 'Jet Lag'},
                {key: '52', value: 'Kanker'},
                {key: '53', value: 'Kantuk'},
                {key: '54', value: 'Kapalan'},
                {key: '55', value: 'Kejang'},
                {key: '56', value: 'Kista'},
                {key: '57', value: 'Laringitis'},
                {key: '58', value: 'Leukimia'},
                {key: '59', value: 'Limfoma'},
                {key: '60', value: 'Lipoma'},
                {key: '61', value: 'Listeria'},
                {key: '62', value: 'Malaria'},
                {key: '63', value: 'Mimisan'},
                {key: '64', value: 'Mata Ikan'},
                {key: '65', value: 'Migrain'},
                {key: '66', value: 'Muntah Darah'},
                {key: '67', value: 'Nyeri Sendi'},
                {key: '68', value: 'Neuropati'},
                {key: '69', value: 'Narkolepsi'},
                {key: '70', value: 'Neuroblastoma'},
                {key: '71', value: 'Nyeri Pergelangan Tangan'},
                {key: '72', value: 'Osteoporosis'},
                {key: '73', value: 'Obesitas'},
                {key: '74', value: 'Osteofit'},
                {key: '75', value: 'Otitis Media'},
                {key: '76', value: 'Orchitis'},
                {key: '77', value: 'Panu'},
                {key: '78', value: 'Patah Tulang'},
                {key: '79', value: 'Psikopat'},
                {key: '80', value: 'Pusing'},
                {key: '81', value: 'Polio'},
                {key: '82', value: 'Q'},
                {key: '83', value: 'Rematik'},
                {key: '84', value: 'Rabies'},
                {key: '85', value: 'Rabun Senja'},
                {key: '86', value: 'Radang Amandel'},
                {key: '87', value: 'Roseola'},
                {key: '88', value: 'Sariawan'},
                {key: '89', value: 'Sakit Gigi'},
                {key: '90', value: 'Sakit Maag'},
                {key: '91', value: 'Sesak Napas'},
                {key: '92', value: 'Sindrom'},
                {key: '93', value: 'Tifus'},
                {key: '94', value: 'Tumor'},
                {key: '95', value: 'Tinnitus'},
                {key: '96', value: 'Takikardia'},
                {key: '97', value: 'Tetanus'},
                {key: '98', value: 'Ulkus Kelamin'},
                {key: '99', value: 'Uretritis'},
                {key: '100', value: 'Ulkus Dekubitus'},
                {key: '101', value: 'Uveitis'},
                {key: '102', value: 'Ulkus Kornea'},
                {key: '103', value: 'Vertigo'},
                {key: '104', value: 'Varises'},
                {key: '105', value: 'Vaginitis'},
                {key: '106', value: 'Vaskulitis'},
                {key: '107', value: 'Vitiligo'},
                {key: '108', value: 'Wasir'},
                {key: '109', value: 'Wilson`s Disease'},
                {key: '200', value: 'Williams Syndrome'},
                {key: '201', value: 'Xerosis'},
                {key: '202', value: 'Xanthelasma'},
                {key: '203', value: 'Xerophthalmia'},
                {key: '204', value: 'Y'},
                {key: '205', value: 'Zanamivir'},
            ],
            clicked: 0,
            showToast: false,
            showText: ''
        };
        this.animatedValue = new Animated.Value(0);
    }

    async swicth(index, item, value) {
        await this.setState({
            clicked: index,
            showToast: true,
            showText: item
        }, () => {
            Animated.timing(this.animatedValue, {
                toValue: 1,
                duration: 500
            }).start(this.closeToast())
        })
        // console.log(index)
		// const id_login = await AsyncStorage.getItem('@id_login')

		// axios.get(API_URL+'/main/filter_kompetisi', {params: {
		// 	id_login: id_login,
		// 	jenis_challenge: jenis_challenge
		// }})
		// .then(resp => {
		// })
	}

    closeToast() {
        setTimeout(() => {
            Animated.timing(this.animatedValue, {
                toValue: 0,
                duration: 500
            }).start(() => {
                this.setState({ showToast: false })
            })
        }, 500);
    }

    // getData = () => {
    //     let contactsArr = [];
    //     let aCode = 'A'.charCodeAt(0);
    //     for (let i = 0; i < 26; i++) {
    //         let currChar = String.fromCharCode(aCode + i);
    //         let obj = {
    //             title: currChar
    //         }
            
    //         let currContacts = this.state.contacts.filter(item => {
    //             return item.name[0].toUpperCase() === currChar;
    //         });
    //         if (currContacts.length > 0) {
    //             currContacts.sort((a, b) => a.name.localeCompare(b.name));
    //             obj.data = currContacts;
    //             contactsArr.push(obj);
    //         }
    //     }
    //     return contactsArr;
    // }

    render() {
        return (
            <View style={{ width: '100%', height: '100%', backgroundColor: '#121212' }}>
                <View style={{ width: '100%', alignItems: 'center' }}>
                    <View style={{ width: wp('90%'), marginTop: wp('4%'), marginBottom: wp('4%'), flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ width: wp('20%') }}>
                            <TouchableOpacity onPress={() => this.props.navigation.pop()}>
                                <Image source={require('../asset/left.png')} style={{ width: wp('8%'), height: wp('8%') }} />
                            </TouchableOpacity>
                        </View>
                        <View style={{ width: wp('50%'), alignItems: 'center' }}>
                            <Text style={[ c.light, f._18, f.bold ]}>Daftar jenis obat</Text>
                        </View>
                        <View style={{ width: wp('20%') }}></View>
                    </View>
                </View>

                <View style={{ width: '100%', alignItems: 'center' }}>
                    <View style={{ width: wp('90%'), marginBottom: wp('4%'), backgroundColor: softGrey, flexDirection: 'row', alignItems: 'center', borderRadius: wp('10%') }}>
                        <TextInput
                            placeholder='Cari jenis obat'
                            placeholderTextColor={light}
                            style={{ width: wp('70%'), marginLeft: wp('4%'), color: light }}
                            ref={ref => this.textInputRef = ref}
                            onChangeText={(text) => this.setState({ search: text })}
                            value={this.state.search}
                        />

                        {this.state.search === '' ?
                            <TouchableOpacity onPress={() => this.textInputRef.focus()}>
                                <Image source={require('../asset/search.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: light, marginLeft: wp('5%') }} />
                            </TouchableOpacity>
                            :
                            <TouchableOpacity onPress={() => this.setState({ search: '' })} style={{ marginLeft: wp('5%') }}>
                                <Image source={require('../asset/cross.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: light }} />
                            </TouchableOpacity>
                        }
                    </View>
                </View>

                <View style={{ width: '100%', height: hp('76%'), alignItems: 'center', backgroundColor: '#181B22' }}>
                    <AlphabetList
                        style={{ width: wp('90%'), marginTop: wp('2%') }}
                        data={this.state.contacts}
                        renderCustomItem={( item ) => (
                            <View style={{ width: wp('74%'), borderBottomWidth: 1, borderBottomColor: blue, marginLeft: wp('5%'), marginBottom: wp('2%') }}>
                                <Text style={[ c.blue, { marginBottom: wp('2%') }]}>{item.value}</Text>
                            </View>
                        )}
                        renderCustomSectionHeader={( section ) => (
                            <View style={{ position: 'absolute' }}>
                                <Text style={[ c.light, f.bold, f._16, { marginTop: wp('-0.3%') }]}>{section.title}</Text>
                            </View>
                        )}
                        renderCustomIndexLetter={( item1 ) => (
                            (this.state.clicked === item1.index.toString() ?
                                <TouchableOpacity onPress={() => this.swicth(item1.index.toString(), item1.item.title, item1.onPress.call())} style={{ width: wp('5%'), height: wp('5%'), borderWidth: 1, borderColor: light, borderRadius: wp('1%'), alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={[ c.light, f._12 ]}>{item1.item.title}</Text>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity onPress={() => this.swicth(item1.index.toString(), item1.item.title, item1.onPress.call())} style={{ width: wp('5%'), height: wp('5%'), alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={[ c.light, f._12 ]}>{item1.item.title}</Text>
                                </TouchableOpacity>
                            )
                        )}
                        sectionHeaderHeight={40}
                        getItemHeight={(sectionIndex, rowIndex) => sectionIndex === 0 ? 25 : 26}
                        indexContainerStyle={{
                            width: wp('8.5%'),
                            backgroundColor: blue,
                            height: hp('74%'),
                            borderRadius: wp('1%')
                        }}
                    />
                    {this.state.showToast && this.state.showText === 'A' && (
                        <Animated.View style={{ position: 'absolute', zIndex: 2, right: wp('13.5%'), marginTop: wp('4.5%') }}>
                            <FastImage source={require('../asset/balon.png')} style={{ width: wp('10%'), height: wp('10%'), alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={[ f._16 ]}>{this.state.showText}</Text>
                            </FastImage>
                        </Animated.View>
                    )}
                    {this.state.showToast && this.state.showText === 'B' && (
                        <Animated.View style={{ position: 'absolute', zIndex: 2, right: wp('13.5%'), marginTop: wp('9.5%') }}>
                            <FastImage source={require('../asset/balon.png')} style={{ width: wp('10%'), height: wp('10%'), alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={[ f._16 ]}>{this.state.showText}</Text>
                            </FastImage>
                        </Animated.View>
                    )}
                    {this.state.showToast && this.state.showText === 'C' && (
                        <Animated.View style={{ position: 'absolute', zIndex: 2, right: wp('13.5%'), marginTop: wp('14.5%') }}>
                            <FastImage source={require('../asset/balon.png')} style={{ width: wp('10%'), height: wp('10%'), alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={[ f._16 ]}>{this.state.showText}</Text>
                            </FastImage>
                        </Animated.View>
                    )}
                    {this.state.showToast && this.state.showText === 'D' && (
                        <Animated.View style={{ position: 'absolute', zIndex: 2, right: wp('13.5%'), marginTop: wp('19.5%') }}>
                            <FastImage source={require('../asset/balon.png')} style={{ width: wp('10%'), height: wp('10%'), alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={[ f._16 ]}>{this.state.showText}</Text>
                            </FastImage>
                        </Animated.View>
                    )}
                    {this.state.showToast && this.state.showText === 'E' && (
                        <Animated.View style={{ position: 'absolute', zIndex: 2, right: wp('13.5%'), marginTop: wp('24.5%') }}>
                            <FastImage source={require('../asset/balon.png')} style={{ width: wp('10%'), height: wp('10%'), alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={[ f._16 ]}>{this.state.showText}</Text>
                            </FastImage>
                        </Animated.View>
                    )}
                    {this.state.showToast && this.state.showText === 'F' && (
                        <Animated.View style={{ position: 'absolute', zIndex: 2, right: wp('13.5%'), marginTop: wp('29.5%') }}>
                            <FastImage source={require('../asset/balon.png')} style={{ width: wp('10%'), height: wp('10%'), alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={[ f._16 ]}>{this.state.showText}</Text>
                            </FastImage>
                        </Animated.View>
                    )}
                    {this.state.showToast && this.state.showText === 'G' && (
                        <Animated.View style={{ position: 'absolute', zIndex: 2, right: wp('13.5%'), marginTop: wp('34.5%') }}>
                            <FastImage source={require('../asset/balon.png')} style={{ width: wp('10%'), height: wp('10%'), alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={[ f._16 ]}>{this.state.showText}</Text>
                            </FastImage>
                        </Animated.View>
                    )}
                    {this.state.showToast && this.state.showText === 'H' && (
                        <Animated.View style={{ position: 'absolute', zIndex: 2, right: wp('13.5%'), marginTop: wp('39.5%') }}>
                            <FastImage source={require('../asset/balon.png')} style={{ width: wp('10%'), height: wp('10%'), alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={[ f._16 ]}>{this.state.showText}</Text>
                            </FastImage>
                        </Animated.View>
                    )}
                    {this.state.showToast && this.state.showText === 'I' && (
                        <Animated.View style={{ position: 'absolute', zIndex: 2, right: wp('13.5%'), marginTop: wp('44.5%') }}>
                            <FastImage source={require('../asset/balon.png')} style={{ width: wp('10%'), height: wp('10%'), alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={[ f._16 ]}>{this.state.showText}</Text>
                            </FastImage>
                        </Animated.View>
                    )}
                    {this.state.showToast && this.state.showText === 'J' && (
                        <Animated.View style={{ position: 'absolute', zIndex: 2, right: wp('13.5%'), marginTop: wp('49.5%') }}>
                            <FastImage source={require('../asset/balon.png')} style={{ width: wp('10%'), height: wp('10%'), alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={[ f._16 ]}>{this.state.showText}</Text>
                            </FastImage>
                        </Animated.View>
                    )}
                    {this.state.showToast && this.state.showText === 'K' && (
                        <Animated.View style={{ position: 'absolute', zIndex: 2, right: wp('13.5%'), marginTop: wp('54.5%') }}>
                            <FastImage source={require('../asset/balon.png')} style={{ width: wp('10%'), height: wp('10%'), alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={[ f._16 ]}>{this.state.showText}</Text>
                            </FastImage>
                        </Animated.View>
                    )}
                    {this.state.showToast && this.state.showText === 'L' && (
                        <Animated.View style={{ position: 'absolute', zIndex: 2, right: wp('13.5%'), marginTop: wp('59.5%') }}>
                            <FastImage source={require('../asset/balon.png')} style={{ width: wp('10%'), height: wp('10%'), alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={[ f._16 ]}>{this.state.showText}</Text>
                            </FastImage>
                        </Animated.View>
                    )}
                    {this.state.showToast && this.state.showText === 'M' && (
                        <Animated.View style={{ position: 'absolute', zIndex: 2, right: wp('13.5%'), marginTop: wp('64.5%') }}>
                            <FastImage source={require('../asset/balon.png')} style={{ width: wp('10%'), height: wp('10%'), alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={[ f._16 ]}>{this.state.showText}</Text>
                            </FastImage>
                        </Animated.View>
                    )}
                    {this.state.showToast && this.state.showText === 'N' && (
                        <Animated.View style={{ position: 'absolute', zIndex: 2, right: wp('13.5%'), marginTop: wp('69.5%') }}>
                            <FastImage source={require('../asset/balon.png')} style={{ width: wp('10%'), height: wp('10%'), alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={[ f._16 ]}>{this.state.showText}</Text>
                            </FastImage>
                        </Animated.View>
                    )}
                    {this.state.showToast && this.state.showText === 'O' && (
                        <Animated.View style={{ position: 'absolute', zIndex: 2, right: wp('13.5%'), marginTop: wp('74.5%') }}>
                            <FastImage source={require('../asset/balon.png')} style={{ width: wp('10%'), height: wp('10%'), alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={[ f._16 ]}>{this.state.showText}</Text>
                            </FastImage>
                        </Animated.View>
                    )}
                    {this.state.showToast && this.state.showText === 'P' && (
                        <Animated.View style={{ position: 'absolute', zIndex: 2, right: wp('13.5%'), marginTop: wp('79.5%') }}>
                            <FastImage source={require('../asset/balon.png')} style={{ width: wp('10%'), height: wp('10%'), alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={[ f._16 ]}>{this.state.showText}</Text>
                            </FastImage>
                        </Animated.View>
                    )}
                    {this.state.showToast && this.state.showText === 'Q' && (
                        <Animated.View style={{ position: 'absolute', zIndex: 2, right: wp('13.5%'), marginTop: wp('84.5%') }}>
                            <FastImage source={require('../asset/balon.png')} style={{ width: wp('10%'), height: wp('10%'), alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={[ f._16 ]}>{this.state.showText}</Text>
                            </FastImage>
                        </Animated.View>
                    )}
                    {this.state.showToast && this.state.showText === 'R' && (
                        <Animated.View style={{ position: 'absolute', zIndex: 2, right: wp('13.5%'), marginTop: wp('89.5%') }}>
                            <FastImage source={require('../asset/balon.png')} style={{ width: wp('10%'), height: wp('10%'), alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={[ f._16 ]}>{this.state.showText}</Text>
                            </FastImage>
                        </Animated.View>
                    )}
                    {this.state.showToast && this.state.showText === 'S' && (
                        <Animated.View style={{ position: 'absolute', zIndex: 2, right: wp('13.5%'), marginTop: wp('94.5%') }}>
                            <FastImage source={require('../asset/balon.png')} style={{ width: wp('10%'), height: wp('10%'), alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={[ f._16 ]}>{this.state.showText}</Text>
                            </FastImage>
                        </Animated.View>
                    )}
                    {this.state.showToast && this.state.showText === 'T' && (
                        <Animated.View style={{ position: 'absolute', zIndex: 2, right: wp('13.5%'), marginTop: wp('99.5%') }}>
                            <FastImage source={require('../asset/balon.png')} style={{ width: wp('10%'), height: wp('10%'), alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={[ f._16 ]}>{this.state.showText}</Text>
                            </FastImage>
                        </Animated.View>
                    )}
                    {this.state.showToast && this.state.showText === 'U' && (
                        <Animated.View style={{ position: 'absolute', zIndex: 2, right: wp('13.5%'), marginTop: wp('104.5%') }}>
                            <FastImage source={require('../asset/balon.png')} style={{ width: wp('10%'), height: wp('10%'), alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={[ f._16 ]}>{this.state.showText}</Text>
                            </FastImage>
                        </Animated.View>
                    )}
                    {this.state.showToast && this.state.showText === 'V' && (
                        <Animated.View style={{ position: 'absolute', zIndex: 2, right: wp('13.5%'), marginTop: wp('109.5%') }}>
                            <FastImage source={require('../asset/balon.png')} style={{ width: wp('10%'), height: wp('10%'), alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={[ f._16 ]}>{this.state.showText}</Text>
                            </FastImage>
                        </Animated.View>
                    )}
                    {this.state.showToast && this.state.showText === 'W' && (
                        <Animated.View style={{ position: 'absolute', zIndex: 2, right: wp('13.5%'), marginTop: wp('114.5%') }}>
                            <FastImage source={require('../asset/balon.png')} style={{ width: wp('10%'), height: wp('10%'), alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={[ f._16 ]}>{this.state.showText}</Text>
                            </FastImage>
                        </Animated.View>
                    )}
                    {this.state.showToast && this.state.showText === 'X' && (
                        <Animated.View style={{ position: 'absolute', zIndex: 2, right: wp('13.5%'), marginTop: wp('119.5%') }}>
                            <FastImage source={require('../asset/balon.png')} style={{ width: wp('10%'), height: wp('10%'), alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={[ f._16 ]}>{this.state.showText}</Text>
                            </FastImage>
                        </Animated.View>
                    )}
                    {this.state.showToast && this.state.showText === 'Y' && (
                        <Animated.View style={{ position: 'absolute', zIndex: 2, right: wp('13.5%'), marginTop: wp('124.5%') }}>
                            <FastImage source={require('../asset/balon.png')} style={{ width: wp('10%'), height: wp('10%'), alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={[ f._16 ]}>{this.state.showText}</Text>
                            </FastImage>
                        </Animated.View>
                    )}
                    {this.state.showToast && this.state.showText === 'Z' && (
                        <Animated.View style={{ position: 'absolute', zIndex: 2, right: wp('13.5%'), marginTop: wp('129.5%') }}>
                            <FastImage source={require('../asset/balon.png')} style={{ width: wp('10%'), height: wp('10%'), alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={[ f._16 ]}>{this.state.showText}</Text>
                            </FastImage>
                        </Animated.View>
                    )}
                    {/* <View style={{ width: wp('80%') }}>
                        <SectionList
                            ListFooterComponent={<View style={{ marginBottom: wp('3%') }} />}
                            sections={this.getData()}
                            keyExtractor={item => item.index}
                            renderItem={({ item }) => (
                                <View style={{ width: wp('75%'), borderBottomWidth: 1, borderBottomColor: blue, marginLeft: wp('5%') }}>
                                    <Text style={[ c.blue, { marginTop: wp('2%'), marginBottom: wp('2%') }]}>{item.name}</Text>
                                </View>
                            )}
                            renderSectionHeader={({ section }) => (
                                <View style={{ marginTop: wp('2%'), position: 'absolute' }}>
                                    <Text style={[ c.light, f.bold ]}>{section.title}</Text>
                                </View>
                            )}
                        />
                    </View>
                    <View style={{ width: wp('8%'), height: hp('75.8%'), backgroundColor: blue, borderRadius: wp('1%'), marginLeft: wp('2%'), alignItems: 'center' }}>
                        <FlatList
                            ListHeaderComponent={<View style={{ marginTop: wp('1%') }} />}
                            ListFooterComponent={<View style={{ marginBottom: wp('1%') }} />}
                            data={this.state.alphabet}
                            keyExtractor={item => item.key}
                            renderItem={({ item, index }) => (
                                (this.state.clicked === index ?
                                    <TouchableOpacity onPress={() => this.swicth(index)} style={{ width: wp('5.1%'), height: wp('5.1%'), alignItems: 'center', justifyContent: 'center', borderWidth: 1, borderColor: light, borderRadius: wp('1%') }}>
                                        <Text style={[ c.light, f._10 ]}>{item.huruf}</Text>
                                    </TouchableOpacity>
                                    :
                                    <TouchableOpacity onPress={() => this.swicth(index)} style={{ width: wp('5.1%'), height: wp('5.1%'), alignItems: 'center', justifyContent: 'center' }}>
                                        <Text style={[ c.light, f._10 ]}>{item.huruf}</Text>
                                    </TouchableOpacity>
                                )
                            )}
                        />
                    </View> */}
                </View>
            </View>
        )
    }
}
