import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity, ToastAndroid, Alert, ActivityIndicator, StyleSheet } from 'react-native'
import { blue, light } from '../utils/Color'
import { c, f, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import Icon from 'react-native-vector-icons/FontAwesome'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'
import { API_URL } from 'react-native-dotenv'

export default class PopularProduct extends Component {
    constructor(props) {
        super(props);
        this.state = {
            btn_buy: 'BELI',
            count: '',
            loading: false
        }
    }

    componentDidMount() {
        this.setState({ loading: true })
        axios.get(API_URL+'/main/shopping/cart', {params: {
            id_login: '118205008143923829902',
            produk: this.props.id
        }})
        .then(res => {
            // console.log(res.data.data.jumlah)
            this.setState({
                loading: false,
                count: res.data.data.length
            })
        })
    }

    incrementCount = () => {
        axios.post(API_URL+'/main/shopping/cart/'+this.props.id+'/add', {id_login: '118205008143923829902', jumlah: 1})
        .then(resp => {
            // console.log(resp.data)
            this.componentDidMount()
        })
        .catch(err => {
            console.log('add_to_cart: '+err)
        })
        ToastAndroid.show('Added Products Again', ToastAndroid.SHORT)
    }

    decrementCount = () => {
        axios.post(API_URL+'/main/shopping/cart/'+this.props.id+'/add', {id_login: '118205008143923829902', jumlah: -1})
        .then(resp => {
            // console.log(resp.data)
            this.componentDidMount()
        })
        .catch(err => {
            console.log('add_to_cart: '+err)
        })
        ToastAndroid.show('Reduce Your Product', ToastAndroid.SHORT)
        if (this.state.count === 1) {
            Alert.alert(
                "",
                "Apakah Anda yakin ingin menghapus?",
                [
                  {
                    text: 'Tidak', onPress: () => this.setState({ count: 1 }), style: "cancel"
                  },
                  { text: 'Ya', onPress: () => {
                    axios.get(API_URL+'/main/shopping/cart/'+this.props.id+'/remove')
                    .then(res => {
                        // console.log(res.data.status)
                        this.componentDidMount()
                        if (res.data.status === 'true') {
                            ToastAndroid.show('Your order has been deleted', ToastAndroid.SHORT)
                        }
                    })
                  } }
                ],  
                { cancelable: false }
            );
            return true;
        }
    }

    addToCart() {
        this.setState({ loading: true })

        axios.post(API_URL+'/main/shopping/cart/'+this.props.id+'/add', {id_login: '118205008143923829902', jumlah: 1})
        .then(resp => {
            // console.log(resp.data)
            this.componentDidMount()
        })
        .catch(err => {
            console.log('add_to_cart: '+err)
        })
    }

    convertToRupiah(number) {
        if (number) {
          var rupiah = "";
          var numberrev = number
            .toString()
            .split("")
            .reverse()
            .join("");
          for (var i = 0; i < numberrev.length; i++)
            if (i % 3 == 0) rupiah += numberrev.substr(i, 3) + ".";
          return (
            "Rp " +
            rupiah
              .split("", rupiah.length - 1)
              .reverse()
              .join("")
          );
        } else {
            return (
                "Rp "+number
            );
        }
    }
    
    render() {
        return (
            <>
                {this.state.loading === true ?
                    <View style={[ styles.container_loading, styles.horizontal_loading ]}>
                        <ActivityIndicator size='large' color={blue} />
                    </View>
                    :
                    null
                }
                <View style={{ width: wp('60%'), height: '100%', backgroundColor: light, borderRadius: wp('1%'), alignItems: 'center' }}>
                    <Image source={require('../asset/logo_combi.png')} style={{ width: wp('10%'), height: wp('10%'), position: 'absolute', zIndex: 1, left: wp('1%'), top: wp('1%') }} />
                    <FastImage source={{ uri: API_URL+'/src/images/'+this.props.gambar }} style={{ width: wp('20%'), height: wp('25%') }} />
                    <View style={{ width: '100%', height: wp('30%'), backgroundColor: blue, flexDirection: 'row' }}>
                        <View style={{ width: wp('35%'), height: '100%' }}>
                            <TouchableOpacity onPress={eval(this.props.action)}>
                                <Text style={[ c.light, f._16, { fontFamily: 'lineto-circular-pro-bold', marginLeft: wp('2%'), marginTop: wp('1%') }]}>{this.props.nama}</Text>
                            </TouchableOpacity>
                            <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: wp('2%'), marginTop: wp('1%') }}>
                                <Text style={[ c.light, f._12 ]}>{this.convertToRupiah(this.props.harga)} / </Text>
                                <Text style={[ c.light, f._12 ]}>{this.props.satuan}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', marginLeft: wp('2%'), marginTop: wp('2%'), alignItems: 'center' }}>
                                <Image source={require('../asset/star.png')} style={{ width: wp('4%'), height: wp('4%') }} />
                                <Text style={[ c.light, f._12, { marginLeft: wp('1%') }]}>5.0 | 10 Ulasan</Text>
                            </View>
                        </View>
                        <View style={{ width: wp('25%'), height: '100%', alignItems: 'center' }}>
                            {this.state.count !== 0 ?
                                <View style={{ flexDirection: 'row', alignItems: 'center', borderRadius: wp('1%'), height: wp('7%'), marginTop: wp('13%') }}>
                                    <TouchableOpacity onPress={this.decrementCount} style={[ p.center, { width: wp('7%'), height: '100%', borderWidth: 1, borderColor: light, borderRadius: wp('2%') }]}>
                                        <Icon name='minus' size={10} color={light} />
                                    </TouchableOpacity>
                                    <View style={[ p.center, { width: wp('8.3%') }]}>
                                        <Text style={[ c.light ]}>{this.state.count}</Text>
                                    </View>
                                    <TouchableOpacity onPress={this.incrementCount} style={[ p.center, { width: wp('7%'), height: '100%', borderWidth: 1, borderColor: light, borderRadius: wp('2%') }]}>
                                        <Icon name='plus' size={10} color={light} />
                                    </TouchableOpacity>
                                </View>
                                :
                                <TouchableOpacity onPress={() => this.addToCart()} style={[ p.center, { width: wp('20%'), height: wp('8%'), backgroundColor: light, borderRadius: wp('2%'), marginTop: wp('13%') }]}>
                                    <Text style={[ c.blue, f._16, { fontFamily: 'lineto-circular-pro-bold' }]}>{this.state.btn_buy}</Text>
                                </TouchableOpacity>
                            }
                        </View>
                    </View>
                </View>
                <View style={{ width: wp('3%') }} />
            </>
        )
    }
}

const styles = StyleSheet.create({
    container_loading: {
        flex: 1,
        position:'absolute',
        zIndex:2,
        width: '100%',
        height: '100%',
        justifyContent: "center",
        backgroundColor: 'rgba(0,0,0,0.7)'
    },
    
    horizontal_loading: {
        flexDirection: "row",
        justifyContent: "space-around",
        padding: 10
    }
})