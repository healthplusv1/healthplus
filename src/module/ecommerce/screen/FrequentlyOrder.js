import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Image, ScrollView, FlatList, TouchableHighlight, Animated, ImageBackground, RefreshControl } from 'react-native'
import { blue, light } from '../utils/Color'
import { c, b, f, p } from '../utils/StyleHelper'
import { xs, vs } from '../utils/Responsive'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import Icon from 'react-native-vector-icons/FontAwesome'
import { NavigationEvents } from 'react-navigation'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'
import { API_URL } from 'react-native-dotenv'

var isHidden = true;

export default class FrequentlyOrder extends Component {
    constructor(props) {
        super(props);
        this.state = {
            list_product: undefined,
            image: require('../asset/triangleArrow.png'),
            bounceValue: new Animated.Value(52),
            count: '',
            loading: false
        }
    }

    componentDidMount() {
        this.setState({ loading: true })

        axios.get(API_URL+'/main/shopping/cart', {params: {
            id_login: '118205008143923829902'
        }})
        .then(res => {
            res.data.products.forEach((value, key) => {

                res.data.data.forEach((v,k) => {
                    if (String(v.produk_id) === String(value.id)) {
                        res.data.products[key].jumlah = res.data.data[k].jumlah
                    }
                })
            })

            res.data.products.forEach((value, key) => {
                if (value.jumlah === undefined) {
                    res.data.products[key].jumlah='0'
                }
            })

            this.setState({
                loading: false,
                count: res.data.data.length,
                list_product: res.data.products
            })
        })
    }

    changePosition() {
        this.setState({
            image: !isHidden ? require('../asset/triangleArrow.png') : require('../asset/triangleArrowUp.png')
        });
        
        var toValue = 52;

        if (isHidden) {
            toValue = 0;
        }

        Animated.spring(
            this.state.bounceValue,
            {
                toValue: toValue,
                velocity: 3,
                tension: 2,
                friction: 8
            }
        ).start();

        isHidden = !isHidden;
    }

    incrementCount = (item) => {
        axios.post(API_URL+'/main/shopping/cart/'+item.id+'/add', {id_login: '118205008143923829902', jumlah: 1})
        .then(resp => {
            // console.log(resp.data)
            this.componentDidMount()
        })
        .catch(err => {
            console.log('add_to_cart: '+err)
        })
        ToastAndroid.show('Added Products '+item.nama+'', ToastAndroid.SHORT)
    }

    decrementCount = (item) => {
        if (item.jumlah === 1) {
            Alert.alert(
                "",
                "Apakah Anda yakin ingin menghapus?",
                [
                  {
                    text: 'Tidak', onPress: () => console.log('Cancel Pressed'), style: "cancel"
                  },
                  { text: 'Ya', onPress: () => {
                    axios.get(API_URL+'/main/shopping/cart/'+item.id+'/remove')
                    .then(res => {
                        // console.log(res.data.status)
                        this.componentDidMount()
                        if (res.data.status === 'true') {
                            ToastAndroid.show('Your order has been deleted', ToastAndroid.SHORT)
                        }
                    })
                  } }
                ],  
                { cancelable: false }
            );
            return true;
        } else {
            axios.post(API_URL+'/main/shopping/cart/'+item.id+'/add', {id_login: '118205008143923829902', jumlah: -1})
            .then(resp => {
                // console.log(resp.data)
                this.componentDidMount()
            })
            .catch(err => {
                console.log('add_to_cart: '+err)
            })
            ToastAndroid.show('Reduce Your '+item.nama+' Product', ToastAndroid.SHORT)
        }
    }

    addToCart(item) {
        this.setState({ loading: true })

        axios.post(API_URL+'/main/shopping/cart/'+item+'/add', {id_login: '118205008143923829902', jumlah: 1})
        .then(resp => {
            // console.log(resp.data)
            this.componentDidMount()
        })
        .catch(err => {
            console.log('add_to_cart: '+err)
        })
    }

    convertToRupiah(number) {
        if (number) {
          var rupiah = "";
          var numberrev = number
            .toString()
            .split("")
            .reverse()
            .join("");
          for (var i = 0; i < numberrev.length; i++)
            if (i % 3 == 0) rupiah += numberrev.substr(i, 3) + ".";
          return (
            "Rp " +
            rupiah
              .split("", rupiah.length - 1)
              .reverse()
              .join("")
          );
        } else {
            return (
                "Rp "+number
            );
        }
    }

    render() {
        return (
            <>
                <NavigationEvents onDidFocus={() => this.componentDidMount()} />
                <View style={{ width: '100%', height: '100%', backgroundColor: '#121212' }}>
                    {/* <TouchableHighlight underlayColor='#3399FF' onPress={() => this.changePosition()} style={{ width: wp('15%'), height: wp('15%'), backgroundColor: blue, borderRadius: wp('3%'), position: 'absolute', bottom: wp('5%'), right: wp('5%'), zIndex: 2, elevation: 13, shadowOpacity: 0.39, shadowColor: '#000', shadowOffset: { width: 6, height: 6 }, shadowRadius: 8.30, alignItems: 'center', justifyContent: 'center' }}>
                        <FastImage source={this.state.image} style={{ width: wp('8%'), height: wp('8%') }} />
                    </TouchableHighlight>
                    <Animated.View style={{ position: 'absolute', zIndex: 1, bottom: wp('22%'), right: wp('7.5%'), backgroundColor: blue, width: wp('10%'), height: wp('10%'), borderRadius: wp('3%'), transform: [{ translateY: this.state.bounceValue }] }}>
                        <TouchableHighlight underlayColor='#3399FF' onPress={() => this.props.navigation.push('Cart')} style={{ width: '100%', height: '100%', backgroundColor: blue, borderRadius: wp('3%'), alignItems: 'center', justifyContent: 'center' }}>
                            <FastImage source={require('../asset/addCart.png')} style={{ width: wp('6%'), height: wp('6%') }} />
                        </TouchableHighlight>
                    </Animated.View> */}
                    <View style={{ width: '100%', alignItems: 'center', backgroundColor: '#121212' }}>
                        <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('4%'), marginBottom: wp('4%') }}>
                            <View style={{ width: wp('45%') }}>
                                <FastImage source={require('../asset/healthPlus.png')} style={{ width: xs(160), height: xs(38) }} />
                            </View>
                            <View style={{ width: wp('45%'), flexDirection: 'row-reverse' }}>
                                {/* <TouchableOpacity onPress={() => this.props.navigation.push('PharmacyService')}>
                                    <ImageBackground source={require('../asset/headset.png')} style={{ width: wp('7%'), height: wp('7%'), alignItems: 'center', justifyContent: 'center' }}>
                                        <Image source={require('../asset/chatBubbleWhite.png')} style={{ width: wp('4%'), height: wp('4%'), transform: [{ rotateY: '180deg' }] }} />
                                    </ImageBackground>
                                </TouchableOpacity> */}
                                <TouchableOpacity onPress={() => this.props.navigation.push('Cart')}>
                                    {this.state.count !== '' ?
                                        <View style={{ width: wp('3.5%'), height: wp('3.5%'), backgroundColor: 'orange', position: 'absolute', borderRadius: wp('3.5%'), zIndex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                            <Text style={[ c.light, f._8 ]}>{this.state.count}</Text>
                                        </View>
                                        :
                                        null
                                    }
                                    <Image source={require('../asset/addCart.png')} style={{ width: wp('7%'), height: wp('7%'), tintColor: light }} />
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.props.navigation.push('HistoryPayment')} style={{ marginRight: wp('2%') }}>
                                    <Image source={require('../asset/order_icon.png')} style={{ width: wp('7%'), height: wp('7%'), tintColor: light }} />
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.props.navigation.push('FrequentlyOrder')} style={{ marginRight: wp('2%') }}>
                                    <Image source={require('../asset/Frequentlyorder_icon.png')} style={{ width: wp('8%'), height: wp('7%'), tintColor: light }} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                    <View style={{ width: '100%', alignItems: 'center', marginBottom: wp('4%') }}>
                        <View style={{ width: wp('90%'), marginTop: wp('4%'), flexDirection: 'row', alignItems: 'center' }}>
                            <View style={{ width: wp('15%') }}>
                                <TouchableOpacity onPress={() => this.props.navigation.pop()}>
                                    <FastImage source={require('../asset/left.png')} style={{ width: wp('8%'), height: wp('8%') }} />
                                </TouchableOpacity>
                            </View>
                            <View style={{ width: wp('60%'), alignItems: 'center' }}>
                                <Text style={[ c.light, f._18, { fontFamily: 'lineto-circular-pro-bold' }]}>Barang yang sering dibeli</Text>
                            </View>
                            <View style={{ width: wp('15%') }}></View>
                        </View>
                    </View>
                    <ScrollView style={{ backgroundColor: '#181B22' }} refreshControl={<RefreshControl onRefresh={() => this.componentDidMount()} refreshing={this.state.loading} />}>
                        <View style={{ width: '100%', alignItems: 'center', marginTop: wp('4%') }}>
                            <View style={{ width: wp('90%') }}>
                                <FlatList
                                    data={this.state.list_product}
                                    keyExtractor={item => item.key}
                                    numColumns={2}
                                    renderItem={({item}) => (
                                        <View style={{ width: wp('45%'), alignItems: 'center' }}>
                                            <TouchableOpacity onPress={() => this.props.navigation.push('DetailProduct', { id_produk: item.id })} style={{ width: wp('43%'), height: wp('65%'), borderWidth: 1, borderColor: light, borderRadius: wp('1.5%'), alignItems: 'center', marginBottom: wp('3%') }}>
                                                <View style={{ width: '100%', backgroundColor: light, alignItems: 'center', justifyContent: 'center' }}>
                                                    <FastImage source={require('../asset/logo_combi_small.png')} style={{ width: wp('6%'), height: wp('6%'), position: 'absolute', left: wp('1%'), top: wp('1%') }} />
                                                    <FastImage source={{ uri: API_URL+'/src/images/'+item.gambar}} style={{ width: wp('20%'), height: wp('22%'), marginTop: wp('2%'), marginBottom: wp('2%') }} />
                                                </View>
                                                <View style={{ marginTop: wp('2%'), width: wp('30%') }}>
                                                    <Text style={[ c.light, f._16, { fontFamily: 'lineto-circular-pro-bold', textAlign: 'center' }]}>{item.nama}</Text>
                                                </View>
                                                <View style={{ marginTop: wp('2%'), width: wp('30%') }}>
                                                    <Text style={[ c.light, { textAlign: 'center', fontSize: wp('3.5%') }]}>{item.harga} / {item.satuan}</Text>
                                                </View>
                                                <View style={{ flexDirection: 'row', marginTop: wp('1%'), alignItems: 'center' }}>
                                                    <Image source={require('../asset/star.png')} style={{ width: wp('4%'), height: wp('4%') }} />
                                                    <Text style={[ c.light, f._12, { marginLeft: wp('1%') }]}>5.0 | 10 Ulasan</Text>
                                                </View>
                                                {item.jumlah !== '0' ?
                                                    <View style={{ flexDirection: 'row', alignItems: 'center', borderRadius: wp('1%'), height: wp('7%'), marginTop: wp('4%'), marginBottom: wp('4%') }}>
                                                        <TouchableOpacity onPress={() => this.decrementCount(item)} style={[ p.center, { width: wp('7%'), height: '100%', borderWidth: 1, borderColor: light, borderRadius: wp('2%') }]}>
                                                            <Icon name='minus' size={10} color={light} />
                                                        </TouchableOpacity>
                                                        <View style={[ p.center, { width: wp('8.3%') }]}>
                                                            <Text style={[ c.light ]}>{item.jumlah}</Text>
                                                        </View>
                                                        <TouchableOpacity onPress={() => this.incrementCount(item)} style={[ p.center, { width: wp('7%'), height: '100%', borderWidth: 1, borderColor: light, borderRadius: wp('2%') }]}>
                                                            <Icon name='plus' size={10} color={light} />
                                                        </TouchableOpacity>
                                                    </View>
                                                    :
                                                    <TouchableOpacity onPress={() => this.addToCart(item.id)} style={[ p.center, { width: wp('20%'), height: hp('4%'), backgroundColor: light, borderRadius: wp('2%'), marginTop: wp('4%'), marginBottom: wp('4%') }]}>
                                                        <Text style={{ fontFamily: 'lineto-circular-pro-bold' }}>BELI</Text>
                                                    </TouchableOpacity>
                                                }
                                            </TouchableOpacity>
                                        </View>
                                    )}
                                />
                            </View>
                        </View>
                    </ScrollView>
                </View>
            </>
        )
    }
}
