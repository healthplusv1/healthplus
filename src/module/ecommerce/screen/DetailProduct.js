import React, { Component } from 'react'
import { Text, View, ScrollView, TouchableOpacity, Alert, Image, FlatList, TouchableHighlight, RefreshControl, ToastAndroid } from 'react-native'
import { blue, light, softGrey } from '../utils/Color'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import Icon from 'react-native-vector-icons/FontAwesome'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { API_URL } from 'react-native-dotenv'

export default class DetailProduct extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            btn_buy: 'BELI',
            count: undefined,
            review: undefined,
            gambar: '',
            nama: 'loading...',
            harga: 'loading...',
            satuan: 'loading...',
            kandungan_aktif: 'loading...',
            fungsi: 'loading...',
            kegunaan: 'loading...',
            peringatan: 'loading...',
            cara_penggunaan: 'loading...',
            informasi_lain: 'loading...',
            kandungan_tidak_aktif: 'loading...',
            id_cart: ''
        }
    }

    componentDidMount() {
        this.setState({ loading: true })

        axios.get(API_URL+'/main/seller/produk/'+this.props.navigation.getParam('id_produk')+'/get')
        .then(res => {
            // console.log(res.data.data)
            this.setState({
                loading: false,
                gambar: res.data.data.gambar,
                nama: res.data.data.nama,
                harga: res.data.data.harga,
                satuan: res.data.data.satuan,
                kandungan_aktif: res.data.data.kandungan_aktif,
                fungsi: res.data.data.fungsi,
                kegunaan: res.data.data.kegunaan,
                peringatan: res.data.data.peringatan,
                cara_penggunaan: res.data.data.cara_penggunaan,
                informasi_lain: res.data.data.informasi_lain,
                kandungan_tidak_aktif: res.data.data.kandungan_tidak_aktif,
            })
        })
        .catch(e => {
            console.log('detailProduct => '+e)
        })

        axios.get(API_URL+'/main/shopping/produk/rating_all', {params: {
            id: this.props.navigation.getParam('id_produk')
        }})
        .then(resp => {
            console.log(resp.data)
            this.setState({
                loading: false,
                review: resp.data
            })
        })

        axios.get(API_URL+'/main/shopping/cart', {params: {
            id_login: '118205008143923829902',
            produk: this.props.navigation.getParam('id_produk')
        }})
        .then(res => {
            [res.data.data].forEach((data, key)=>{
                console.log(typeof data)
                // if () {
                // } else {
                    
                // }
            })
            
            for (let child of this.json2array(res.data.data)) {
                this.setState({
                    loading: false,
                    count: child.jumlah,
                    id_cart: child.id
                })
            }
        })
    }

    json2array(json){
        var result = [];
        var keys = Object.keys(json);
        keys.forEach(function(key){
            result.push(json[key]);
        });
        return result;
    }

    incrementCount = () => {
        axios.post(API_URL+'/main/shopping/cart/'+this.props.navigation.getParam('id_produk')+'/add', {id_login: '118205008143923829902', jumlah: 1})
        .then(resp => {
            // console.log(resp.data)
            this.componentDidMount()
            ToastAndroid.show('Added Products '+this.state.nama+'', ToastAndroid.SHORT)
        })
        .catch(err => {
            console.log('add_to_cart: '+err)
        })
    }

    decrementCount = () => {
        if (this.state.count === '1') {
            Alert.alert(
                "",
                "Apakah Anda yakin ingin menghapus?",
                [
                  {
                    text: 'Tidak', onPress: () => console.log('Cancel Pressed'), style: "cancel"
                  },
                  { text: 'Ya', onPress: () => {
                    axios.get(API_URL+'/main/shopping/cart/'+this.state.id_cart+'/remove')
                    .then(res => {
                        // console.log(res.data.status)
                        this.componentDidMount()
                        if (res.data.status === 'true') {
                            ToastAndroid.show('Your order has been deleted', ToastAndroid.SHORT)
                        }
                    })
                  } }
                ],  
                { cancelable: false }
            );
            return true;
        } else {
            axios.post(API_URL+'/main/shopping/cart/'+this.props.navigation.getParam('id_produk')+'/add', {id_login: '118205008143923829902', jumlah: -1})
            .then(resp => {
                // console.log(resp.data)
                this.componentDidMount()
                ToastAndroid.show('Reduce Your '+this.state.nama+' Product', ToastAndroid.SHORT)
            })
            .catch(err => {
                console.log('add_to_cart: '+err)
            })
        }
    }

    addToCart() {
        axios.post(API_URL+'/main/shopping/cart/'+this.props.navigation.getParam('id_produk')+'/add', {id_login: '118205008143923829902', jumlah: 1})
        .then(resp => {
            console.log(resp.data.buyer)
            this.componentDidMount()
            ToastAndroid.show('Added Products '+this.state.nama+'', ToastAndroid.SHORT)
        })
        .catch(err => {
            console.log('add_to_cart: '+err)
        })
    }

    hitung_hari(from_date) {
		var date1 = new Date(from_date); 
		var date2 = new Date(new Date().getFullYear(),new Date().getMonth() , new Date().getDate()); 
		// console.log(date1);
		// console.log(date2);
		  
		// To calculate the time difference of two dates 
		var Difference_In_Time = date2.getTime() - date1.getTime(); 
		  
		// To calculate the no. of days between two dates 
		var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
		if (typeof Difference_In_Days === 'number') {
			if(Number(Difference_In_Days.toString().replace('-', '')).toFixed() == 0){
				return 'Today';
			}else{
				return Number(Difference_In_Days.toString().replace('-', '')).toFixed()+' Days ago';
			}
		}
	}

    convertToRupiah(number) {
        if (number) {
          var rupiah = "";
          var numberrev = number
            .toString()
            .split("")
            .reverse()
            .join("");
          for (var i = 0; i < numberrev.length; i++)
            if (i % 3 == 0) rupiah += numberrev.substr(i, 3) + ".";
          return (
            "Rp " +
            rupiah
              .split("", rupiah.length - 1)
              .reverse()
              .join("")
          );
        } else {
            return (
                "Rp "+number
            );
        }
    }

    render() {
        return (
            <View style={{ width: '100%', height: '100%', backgroundColor: '#121212' }}>
                <View style={{ width: '100%', alignItems: 'center' }}>
                    <View style={{ width: wp('90%'), marginTop: wp('4%'), flexDirection: 'row' }}>
                        <View style={{ width: wp('20%') }}>
                            <TouchableOpacity onPress={() => this.props.navigation.pop()}>
                                <FastImage source={require('../asset/left.png')} style={{ width: wp('8%'), height: wp('8%') }} />
                            </TouchableOpacity>
                        </View>
                        <View style={{ width: wp('50%'), alignItems: 'center' }}>
                            <Text style={[ c.light, f._18, { fontFamily: 'lineto-circular-pro-bold' }]}>Produk Detail</Text>
                        </View>
                        <View style={{ width: wp('20%') }}></View>
                    </View>
                </View>
                
                <ScrollView refreshControl={<RefreshControl onRefresh={() => this.componentDidMount()} refreshing={this.state.loading} />}>
                    <View style={{ width: '100%', alignItems: 'center' }}>
                        <View style={{ width: wp('45%'), height: wp('45%'), backgroundColor: light, alignItems: 'center', justifyContent: 'center', marginTop: wp('4%'), borderRadius: wp('0.5%') }}>
                            <FastImage source={{ uri: API_URL+'/src/images/'+this.state.gambar }} style={{ width: wp('28%'), height: wp('28%') }} />
                        </View>
                        <View style={{ maxWidth: wp('60%'), marginTop: wp('4%') }}>
                            <Text style={[ c.light, f._16, { fontFamily: 'lineto-circular-pro-bold' }]} numberOfLines={1}>{this.state.nama}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text style={[ c.light ]}>{this.convertToRupiah(this.state.harga)}/</Text>
                            <Text style={[ c.light ]}>{this.state.satuan}</Text>
                        </View>
                        {this.state.count !== undefined && (
                            <View style={{ flexDirection: 'row', alignItems: 'center', borderRadius: wp('1%'), height: wp('7%'), marginTop: wp('4%') }}>
                                <TouchableHighlight underlayColor={blue} onPress={this.decrementCount} style={[ p.center, { width: wp('7%'), height: '100%', borderWidth: 1, borderColor: blue, borderRadius: wp('2%') }]}>
                                    <Icon name='minus' size={10} color={light} />
                                </TouchableHighlight>
                                <View style={[ p.center, { width: wp('8%') }]}>
                                    <Text style={[ c.light ]}>{this.state.count}</Text>
                                </View>
                                <TouchableHighlight underlayColor={blue} onPress={this.incrementCount} style={[ p.center, { width: wp('7%'), height: '100%', borderWidth: 1, borderColor: blue, borderRadius: wp('2%') }]}>
                                    <Icon name='plus' size={10} color={light} />
                                </TouchableHighlight>
                            </View>
                        )}
                        {this.state.count === undefined && (
                            <TouchableOpacity onPress={() => this.addToCart()} style={[ p.center, { width: wp('30%'), height: hp('5%'), backgroundColor: blue, borderRadius: wp('2%'), marginTop: wp('4%') }]}>
                                <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>{this.state.btn_buy}</Text>
                            </TouchableOpacity>
                        )}
                    </View>

                    <View style={{ width: '100%', backgroundColor: '#181B22', alignItems: 'center', marginTop: wp('4%'), marginBottom: wp('4%') }}>
                        <View style={{ width: wp('90%'), marginTop: wp('5%'), flexDirection: 'row' }}>
                            <View style={{ width: wp('45%') }}>
                                <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>Kandungan aktif</Text>
                            </View>
                            <View style={{ width: wp('45%'), alignItems: 'flex-end' }}>
                                <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>Fungsi</Text>
                            </View>
                        </View>
                        <View style={{ width: wp('90%'), flexDirection: 'row' }}>
                            <View style={{ width: wp('55%') }}>
                                <Text style={[ c.light, f._12 ]}>{this.state.kandungan_aktif}</Text>
                            </View>
                            <View style={{ width: wp('35%'), alignItems: 'flex-end' }}>
                                <Text style={[ c.light, f._12, { textAlign: 'right' }]}>{this.state.fungsi}</Text>
                            </View>
                        </View>
                        <View style={{ width: wp('90%'), marginTop: wp('5%') }}>
                            <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>Kegunaan:</Text>
                            <Text style={[ c.light ]}>{this.state.kegunaan}</Text>
                        </View>
                        <View style={{ width: wp('90%'), marginTop: wp('5%') }}>
                            <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>Peringatan:</Text>
                            <Text style={[ c.light ]}>{this.state.peringatan}</Text>
                        </View>
                        <View style={{ width: wp('90%'), marginTop: wp('5%') }}>
                            <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>Cara Penggunaan:</Text>
                            <Text style={[ c.light ]}>{this.state.cara_penggunaan}</Text>
                        </View>
                        <View style={{ width: wp('90%'), marginTop: wp('5%') }}>
                            <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>Informasi lain:</Text>
                            <Text style={[ c.light ]}>{this.state.informasi_lain}</Text>
                        </View>
                        <View style={{ width: wp('90%'), marginTop: wp('5%'), marginBottom: wp('4%') }}>
                            <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>Kandungan tidak aktif:</Text>
                            <Text style={[ c.light ]}>{this.state.kandungan_tidak_aktif}</Text>
                        </View>
                    </View>

                    <View style={{ width: '100%', backgroundColor: '#181B22', alignItems: 'center' }}>
                        <View style={{ width: wp('90%'), flexDirection: 'row', marginTop: wp('4%') }}>
                            <View style={{ width: wp('60%') }}>
                                <Text style={[ c.light, f._16, { fontFamily: 'lineto-circular-pro-bold' }]}>Ulasan Pembeli</Text>
                                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: wp('1%') }}>
                                    <Image source={require('../asset/star.png')} style={{ width: wp('7%'), height: wp('7%'), tintColor: '#FE8829' }} />
                                    <Text style={[ c.light, f._16, { fontFamily: 'lineto-circular-pro-bold', marginLeft: wp('1%') }]}>5.0</Text>
                                    <Text style={[ c.light, { marginLeft: wp('1%') }]}>dari 10 ulasan</Text>
                                </View>
                            </View>
                            <View style={{ width: wp('30%'), alignItems: 'flex-end' }}>
                                <TouchableOpacity onPress={() => this.props.navigation.push('AllReview')}>
                                    <Text style={[ c.blue ]}>Lihat Semua</Text>
                                </TouchableOpacity>
                            </View>
                        </View>

                        <View style={{ width: wp('90%'), marginTop: wp('4%') }}>
                            {/* <View style={{ width: '100%', alignItems: 'center', position: 'absolute', zIndex: 1, bottom: wp('4%') }}>
                                <TouchableHighlight onPress={() => this.props.navigation.push('AddReview')} style={{ width: wp('30%'), height: hp('6%'), alignItems: 'center', justifyContent: 'center', backgroundColor: blue, borderRadius: wp('2%') }} underlayColor='#3399FF'>
                                    <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>Tulis Ulasan</Text>
                                </TouchableHighlight>
                            </View> */}
                            <FlatList
                                data={this.state.review}
                                keyExtractor={item => item.id}
                                renderItem={({item}) => (
                                    <>
                                        <View style={{ width: '100%', height: wp('15%'), flexDirection: 'row' }}>
                                            <View style={{ width: wp('15%'), height: '100%' }}>
                                                <View style={{ width: '100%', height: '100%', backgroundColor: softGrey, borderRadius: wp('10%'), alignItems: 'center', justifyContent: 'center' }}>
                                                    <Image source={require('../asset/user.png')} style={{ width: wp('10%'), height: wp('10%'), tintColor: '#CCC' }} />
                                                </View>
                                            </View>
                                            <View style={{ width: wp('50%'), height: '100%' }}>
                                                <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold', marginLeft: wp('4%'), marginTop: wp('1%') }]}>{item.username}</Text>
                                                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: wp('1%'), marginLeft: wp('4%') }}>
                                                    {item.score === '5' && (
                                                        <>
                                                            <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                            <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                            <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                            <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                            <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                        </>
                                                    )}
                                                    {item.score === '4' && (
                                                        <>
                                                            <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                            <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                            <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                            <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                            <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%') }} />
                                                        </>
                                                    )}
                                                    {item.score === '3' && (
                                                        <>
                                                            <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                            <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                            <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                            <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%') }} />
                                                            <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%') }} />
                                                        </>
                                                    )}
                                                    {item.score === '2' && (
                                                        <>
                                                            <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                            <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                            <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%') }} />
                                                            <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%') }} />
                                                            <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%') }} />
                                                        </>
                                                    )}
                                                    {item.score === '1' && (
                                                        <>
                                                            <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                            <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%') }} />
                                                            <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%') }} />
                                                            <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%') }} />
                                                            <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%') }} />
                                                        </>
                                                    )}
                                                    <Text style={[ c.light, { marginLeft: wp('2%') }]}>{item.score}</Text>
                                                </View>
                                            </View>
                                            <View style={{ width: wp('25%'), height: '100%', alignItems: 'flex-end' }}>
                                                <Text style={[ c.light, f._12, { marginTop: wp('7.3%') }]}>{this.hitung_hari(item.created_at)}</Text>
                                            </View>
                                        </View>

                                        <View style={{ width: '100%', marginTop: wp('2%'), marginBottom: wp('4%') }}>
                                            <Text style={[ c.light, { textAlign: 'justify' }]}>{item.ulasan}</Text>
                                        </View>
                                    </>
                                )}
                            />
                        </View>
                    </View>
                </ScrollView>
                {this.state.count !== undefined && (
                    <View style={{ width: '100%', height: hp('10%'), position: 'relative', bottom: 0, backgroundColor: '#2A2A2A', alignItems: 'center', justifyContent: 'center' }}>
                        <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
                            <View style={{ width: wp('50%'), flexDirection: 'row' }}>
                                <Text style={[ c.light ]}>{this.state.count} Produk</Text>
                                <Text style={[ c.light ]}> | </Text>
                                <Text style={[ c.light ]}>Rp 14.500</Text>
                            </View>
                            <View style={{ width: wp('40%'), alignItems: 'flex-end' }}>
                                <TouchableOpacity onPress={() => this.props.navigation.push('Cart')} style={{ width: wp('30%'), height: hp('5%'), backgroundColor: blue, borderRadius: wp('1%'), alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={[ c.light ]}>Keranjang</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                )}
            </View>
        )
    }
}
