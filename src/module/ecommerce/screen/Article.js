import React, { Component } from 'react'
import { Text, View, TouchableOpacity, FlatList, ScrollView, Image } from 'react-native'
import { blue, light } from '../utils/Color'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'

export default class Article extends Component {
    constructor(props) {
        super(props);
        this.state = {
            artikel:[
                {'image': require('../asset/pusing.png'), 'value': 'Demam dan Batuk? Jangan Panik! Kenali Perbedaan Gejala-19', 'uploaded': 'HealthPlus | 23 April 2020', 'key': '0'},
                {'image': require('../asset/pusing.png'), 'value': 'Demam dan Batuk? Jangan Panik! Kenali Perbedaan Gejala-19', 'uploaded': 'HealthPlus | 23 April 2020', 'key': '1'},
                {'image': require('../asset/pusing.png'), 'value': 'Demam dan Batuk? Jangan Panik! Kenali Perbedaan Gejala-19', 'uploaded': 'HealthPlus | 23 April 2020', 'key': '2'},
                {'image': require('../asset/pusing.png'), 'value': 'Demam dan Batuk? Jangan Panik! Kenali Perbedaan Gejala-19', 'uploaded': 'HealthPlus | 23 April 2020', 'key': '3'},
                {'image': require('../asset/pusing.png'), 'value': 'Demam dan Batuk? Jangan Panik! Kenali Perbedaan Gejala-19', 'uploaded': 'HealthPlus | 23 April 2020', 'key': '4'},
                {'image': require('../asset/pusing.png'), 'value': 'Demam dan Batuk? Jangan Panik! Kenali Perbedaan Gejala-19', 'uploaded': 'HealthPlus | 23 April 2020', 'key': '5'},
            ]
        }
    }

    render() {
        return (
            <>
                <View style={{ width: '100%', height: hp('15%'), backgroundColor: '#121212' }}>
                    <View style={{ width: '100%', position: 'absolute', bottom: wp('4%'), flexDirection: 'row', alignItems: 'center' }}>
                        <TouchableOpacity onPress={() => this.props.navigation.pop()} style={{ marginLeft: wp('4%') }}>
                            <FastImage source={require('../asset/left.png')} style={{ width: wp('8%'), height: wp('8%') }} />
                        </TouchableOpacity>
                        <View style={[ p.center, p.row, { width: wp('76%') }]}>
                            <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>HEALTHPLUS</Text>
                            <Text style={[ c.light, { marginLeft: wp('1.5%') }]}>Artikel</Text>
                        </View>
                        <TouchableOpacity>
                            <Image source={require('../asset/search.png')} style={{ width: wp('8%'), height: wp('8%'), tintColor: light }} />
                        </TouchableOpacity>
                    </View>
                </View>
                <ScrollView style={{ backgroundColor: '#181B22' }}>
                    <FlatList
                        data={this.state.artikel}
                        keyExtractor={item => item.key}
                        renderItem={({item}) => (
                            <TouchableOpacity onPress={() => this.props.navigation.push('ArticleDetail')} style={{ width: wp('90%'), flexDirection: 'row', marginTop: wp('2%') }}>
                                <FastImage source={item.image} style={{ width: wp('30%'), height: wp('30%') }} />
                                <View style={{ width: wp('55%'), marginLeft: wp('3%') }}>
                                    <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>{item.value}</Text>
                                    <Text style={[ c.light, f._12, { marginTop: wp('2%') }]}>{item.uploaded}</Text>
                                </View>
                            </TouchableOpacity>
                        )}
                    />
                    <View style={{ marginTop: wp('2%') }} />
                </ScrollView>
            </>
        )
    }
}
