import React, { Component } from 'react'
import { Text, View, TouchableOpacity, ScrollView, FlatList, RefreshControl, ToastAndroid, Image, TouchableHighlight } from 'react-native'
import { blue, light } from '../utils/Color'
import { c, f, b, p } from '../utils/StyleHelper'
import { xs, vs } from '../utils/Responsive'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import Icon from 'react-native-vector-icons/FontAwesome'
import { NavigationEvents } from 'react-navigation'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { API_URL } from 'react-native-dotenv'

export default class MedicineList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            list_product: undefined,
            count: '',
        }
    }

    componentDidMount() {
        this.setState({ loading: true })
        
        axios.get(API_URL+'/main/shopping/search', {params: {
            q: this.props.navigation.getParam('qSearch'),
            id_login: '118205008143923829902'
        }})
        .then(resp => {
            // console.log('qSearch '+JSON.stringify(resp.data.drugs))
            resp.data.drugs.forEach((value, key) => {

                resp.data.cart.forEach((v,k) => {
                    if (String(v.produk_id) === String(value.id)) {
                        resp.data.drugs[key].jumlah = resp.data.cart[k].jumlah
                        resp.data.drugs[key].cart_id = v.id
                    }
                })
            })

            resp.data.drugs.forEach((value, key) => {
                if (value.jumlah === undefined) {
                    resp.data.drugs[key].jumlah='0'
                }
            })
            this.setState({
                loading: false,count: resp.data.cart.length,
                list_product: resp.data.drugs
            })
        })
    }

    incrementCount = (item) => {
        axios.post(API_URL+'/main/shopping/cart/'+item.id+'/add', {id_login: '118205008143923829902', jumlah: 1})
        .then(resp => {
            // console.log(resp.data)
            this.componentDidMount()
            // this.componentDidMount()
        })
        .catch(err => {
            console.log('add_to_cart: '+err)
        })
        ToastAndroid.show('Added Products '+item.nama+'', ToastAndroid.SHORT)
    }

    decrementCount = (item) => {
        if (item.jumlah === '1') {
            Alert.alert(
                "",
                "Apakah Anda yakin ingin menghapus?",
                [
                  {
                    text: 'Tidak', onPress: () => console.log('Cancel Pressed'), style: "cancel"
                  },
                  { text: 'Ya', onPress: () => {
                    axios.get(API_URL+'/main/shopping/cart/'+item.cart_id+'/remove')
                    .then(res => {
                        // console.log(res.data.status)
                        this.componentDidMount()
                        if (res.data.status === 'true') {
                            ToastAndroid.show('Your order has been deleted', ToastAndroid.SHORT)
                        }
                    })
                  } }
                ],  
                { cancelable: false }
            );
            return true;
        } else {
            axios.post(API_URL+'/main/shopping/cart/'+item.id+'/add', {id_login: '118205008143923829902', jumlah: -1})
            .then(resp => {
                // console.log(resp.data)
                this.componentDidMount()
            })
            .catch(err => {
                console.log('add_to_cart: '+err)
            })
            ToastAndroid.show('Reduce Your Product', ToastAndroid.SHORT)
        }
    }

    addToCart(item) {
        this.setState({ loading: true })

        axios.post(API_URL+'/main/shopping/cart/'+item+'/add', {id_login: '118205008143923829902', jumlah: 1})
        .then(resp => {
            // console.log(resp.data)
            this.componentDidMount()
        })
        .catch(err => {
            console.log('add_to_cart: '+err)
        })
    }

    convertToRupiah(number) {
        if (number) {
          var rupiah = "";
          var numberrev = number
            .toString()
            .split("")
            .reverse()
            .join("");
          for (var i = 0; i < numberrev.length; i++)
            if (i % 3 == 0) rupiah += numberrev.substr(i, 3) + ".";
          return (
            "Rp " +
            rupiah
              .split("", rupiah.length - 1)
              .reverse()
              .join("")
          );
        } else {
            return (
                "Rp "+number
            );
        }
    }

    render() {
        return (
            <>
                <NavigationEvents onDidFocus={() => this.componentDidMount()} />
                <View style={{ width: '100%', height: '100%', backgroundColor: '#121212', alignItems: 'center' }}>
                    <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('4%') }}>
                        <View style={{ width: wp('45%') }}>
                            <FastImage source={require('../asset/healthPlus.png')} style={{ width: xs(160), height: xs(38) }} />
                        </View>
                        <View style={{ width: wp('45%'), flexDirection: 'row-reverse' }}>
                            <TouchableOpacity onPress={() => this.props.navigation.push('Cart')} style={{ marginRight: wp('2%') }}>
                                {this.state.count !== '' ?
                                    <View style={{ width: wp('3.5%'), height: wp('3.5%'), backgroundColor: 'orange', position: 'absolute', borderRadius: wp('3.5%'), zIndex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                        <Text style={[ c.light, f._8 ]}>{this.state.count}</Text>
                                    </View>
                                    :
                                    null
                                }
                                <Image source={require('../asset/addCart.png')} style={{ width: wp('7%'), height: wp('7%'), tintColor: light }} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.props.navigation.push('HistoryPayment')} style={{ marginRight: wp('2%') }}>
                                <Image source={require('../asset/order_icon.png')} style={{ width: wp('7%'), height: wp('7%'), tintColor: light }} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.props.navigation.push('FrequentlyOrder')} style={{ marginRight: wp('2%') }}>
                                <Image source={require('../asset/Frequentlyorder_icon.png')} style={{ width: wp('8%'), height: wp('7%'), tintColor: light }} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    {/* <View style={{ width: wp('90%'), marginTop: wp('5%'), alignItems: 'flex-end' }}>
                        <TouchableOpacity style={[ p.center, p.row, { width: wp('25%'), height: wp('8%'), backgroundColor: blue, borderRadius: wp('1%') }]}>
                            <Text style={[ c.light ]}>filter</Text>
                            <FastImage source={require('../asset/funnel.png')} style={{ width: wp('5%'), height: wp('5%'), marginLeft: wp('2%') }} />
                        </TouchableOpacity>
                    </View> */}
                    <ScrollView style={{ marginTop: wp('5%') }}>
                        <View style={{ width: '100%', alignItems: 'center' }}>
                            <View style={{ width: wp('90%') }}>
                                <FlatList
                                    data={this.state.list_product}
                                    keyExtractor={item => item.id}
                                    numColumns={2}
                                    renderItem={({item}) => (
                                        <View style={{ width: wp('45%'), alignItems: 'center' }}>
                                                <TouchableOpacity onPress={() => this.props.navigation.push('DetailProduct', { id_produk: item.id })} style={{ width: wp('43%'), height: wp('70%'), borderWidth: 1, borderColor: light, borderRadius: wp('1.5%'), alignItems: 'center', marginBottom: wp('3%') }}>
                                                    <View style={{ width: '100%', backgroundColor: light, alignItems: 'center', justifyContent: 'center' }}>
                                                        <FastImage source={require('../asset/logo_combi_small.png')} style={{ width: wp('6%'), height: wp('6%'), position: 'absolute', left: wp('1%'), top: wp('1%') }} />
                                                        <FastImage source={{ uri: API_URL+'/src/images/'+item.gambar}} style={{ width: wp('20%'), height: wp('22%'), marginTop: wp('2%'), marginBottom: wp('2%') }} />
                                                    </View>
                                                    <View style={{ marginTop: wp('2%'), width: wp('30%') }}>
                                                        <Text style={[ c.light, f._16, { fontFamily: 'lineto-circular-pro-bold', textAlign: 'center' }]} numberOfLines={2}>{item.nama}</Text>
                                                    </View>
                                                    <View style={{ marginTop: wp('2%'), width: wp('30%') }}>
                                                        <Text style={[ c.light, { textAlign: 'center', fontSize: wp('3.5%') }]}>{this.convertToRupiah(item.harga)} / {item.satuan}</Text>
                                                    </View>
                                                    <View style={{ flexDirection: 'row', marginTop: wp('1%'), alignItems: 'center' }}>
                                                        <Image source={require('../asset/staredd.png')} style={{ width: wp('4%'), height: wp('4%') }} />
                                                        <Text style={[ c.light, f._12, { marginLeft: wp('1%') }]}>5.0 | 10 Ulasan</Text>
                                                    </View>
                                                    {item.jumlah !== '0' ?
                                                        <View style={{ flexDirection: 'row', alignItems: 'center', borderRadius: wp('1%'), height: wp('7%'), marginTop: wp('4%'), marginBottom: wp('4%') }}>
                                                            <TouchableHighlight onPress={() => this.decrementCount(item)} underlayColor={blue} style={[ p.center, { width: wp('7%'), height: '100%', borderWidth: 1, borderColor: blue, borderRadius: wp('2%') }]}>
                                                                <Icon name='minus' size={10} color={light} />
                                                            </TouchableHighlight>
                                                            <View style={[ p.center, { width: wp('8.3%') }]}>
                                                                <Text style={[ c.light ]}>{item.jumlah}</Text>
                                                            </View>
                                                            <TouchableHighlight onPress={() => this.incrementCount(item)} underlayColor={blue} style={[ p.center, { width: wp('7%'), height: '100%', borderWidth: 1, borderColor: blue, borderRadius: wp('2%') }]}>
                                                                <Icon name='plus' size={10} color={light} />
                                                            </TouchableHighlight>
                                                        </View>
                                                        :
                                                        <TouchableOpacity onPress={() => this.addToCart(item.id)} style={[ p.center, { width: wp('20%'), height: hp('4%'), backgroundColor: blue, borderRadius: wp('2%'), marginTop: wp('4%'), marginBottom: wp('4%') }]}>
                                                            <Text style={{ color: light, fontFamily: 'lineto-circular-pro-bold' }}>BELI</Text>
                                                        </TouchableOpacity>
                                                    }
                                                </TouchableOpacity>
                                            </View>
                                    )}
                                />
                            </View>
                        </View>
                    </ScrollView>
                </View>
            </>
        )
    }
}
