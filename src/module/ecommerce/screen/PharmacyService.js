import React, { Component } from 'react'
import { Text, View, TouchableOpacity, TextInput, Image, ScrollView, FlatList, TouchableHighlight, Linking } from 'react-native'
import { blue, light, softGrey } from '../utils/Color'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import * as Animatable from 'react-native-animatable'
import Accordion from 'react-native-collapsible/Accordion'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'

const listApotek = [
    {name: 'Apotek Sehati', address: 'Ruko Mataram 15', last_contact: '11:30, 31 Maret 2021'},
    {name: 'Apotek Sehati B', address: 'Ruko Mataram 15B', last_contact: '10:00, 25 Maret 2021'},
    {name: 'Apotek Sehati C', address: 'Ruko Mataram 15C', last_contact: '09:30, 20 Februari 2021'},
    {name: 'Apotek Sehati D', address: 'Ruko Mataram 15D', last_contact: ''},
    {name: 'Apotek Sehati E', address: 'Ruko Mataram 15E', last_contact: ''},
    {name: 'Apotek Sehati F', address: 'Ruko Mataram 15F', last_contact: ''},
    {name: 'Apotek Sehati G', address: 'Ruko Mataram 15G', last_contact: ''},
    {name: 'Apotek Sehati H', address: 'Ruko Mataram 15H', last_contact: ''},
]

export default class PharmacyService extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listApotek: [
                {'key': '0', 'name': 'Apotek Sehati', 'address': 'Ruko Mataram 15', 'last_contact': '11:30, 31 Maret 2021'},
                {'key': '1', 'name': 'Apotek Sehati B', 'address': 'Ruko Mataram 15B', 'last_contact': '10:00, 25 Maret 2021'},
                {'key': '2', 'name': 'Apotek Sehati C', 'address': 'Ruko Mataram 15C', 'last_contact': '09:30, 20 Februari 2021'},
                {'key': '3', 'name': 'Apotek Sehati D', 'address': 'Ruko Mataram 15D', 'last_contact': ''},
                {'key': '4', 'name': 'Apotek Sehati E', 'address': 'Ruko Mataram 15E', 'last_contact': ''},
                {'key': '5', 'name': 'Apotek Sehati F', 'address': 'Ruko Mataram 15F', 'last_contact': ''},
                {'key': '6', 'name': 'Apotek Sehati G', 'address': 'Ruko Mataram 15G', 'last_contact': ''},
                {'key': '7', 'name': 'Apotek Sehati H', 'address': 'Ruko Mataram 15H', 'last_contact': ''},
            ],
            activeSections: [],
            collapsed: true,
        }
    }

    setSections = sections => {
        this.setState({
            activeSections: sections.includes(undefined) ? [] : sections,
        });
    }

    renderHeader = (sections) => {
        return (
        <Animatable.View
            duration={400}
            style={{ width: '100%', borderBottomWidth: 1, borderBottomColor: blue, alignItems: 'center' }}
        >
            <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('2%'), marginBottom: wp('2%') }}>
                <View style={{ width: wp('15%') }}>
                    <View style={{ width: '100%', height: wp('15%'), backgroundColor: light, borderRadius: wp('10%'), alignItems: 'center', justifyContent: 'center' }}>
                        <FastImage source={require('../asset/logo_apotek.png')} style={{ width: wp('10%'), height: wp('10%') }} />
                    </View>
                </View>
                <View style={{ width: wp('75%') }}>
                    <Text style={[ c.light, f._16, { marginLeft: wp('4%') }]}>{sections.name}</Text>
                    <Text style={[ c.light, { marginLeft: wp('4%') }]}>{sections.address}</Text>
                    <Text style={[ c.light, f._12, { marginLeft: wp('4%') }]}>{sections.last_contact}</Text>
                </View>
            </View>
        </Animatable.View>
        );
    };

    renderContent() {
        return (
        <Animatable.View
            duration={400}
            style={{ width: '100%', flexDirection: 'row', alignItems: 'center' }}
        >
            <TouchableOpacity onPress={() => {Linking.openURL('tel:123')}} style={{ width: wp('50%'), height: hp('8%'), borderWidth: 1, borderColor: blue, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                <Image source={require('../asset/phone.png')} style={{ width: wp('6%'), height: wp('6%'), tintColor: blue }} />
                <Text style={[ c.blue, { marginLeft: wp('2%') }]}>Call</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{ width: wp('50%'), height: hp('8%'), borderWidth: 1, borderColor: blue, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                <Image source={require('../asset/chatBubble.png')} style={{ width: wp('6%'), height: wp('6%'), transform: [{ rotateY: '180deg' }] }} />
                <Text style={[ c.blue, { marginLeft: wp('2%') }]}>Chat</Text>
            </TouchableOpacity>
        </Animatable.View>
        );
    }

    render() {
        return (
            <View style={{ width: '100%', height: '100%', backgroundColor: '#121212' }}>
                <View style={{ width: '100%', alignItems: 'center', backgroundColor: blue }}>
                    <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('4%'), marginBottom: wp('4%') }}>
                        <View style={{ width: wp('15%') }}>
                            <TouchableOpacity onPress={() => this.props.navigation.pop()}>
                                <FastImage source={require('../asset/left.png')} style={{ width: wp('8%'), height: wp('8%') }} />
                            </TouchableOpacity>
                        </View>
                        <View style={{ width: wp('60%'), alignItems: 'center' }}>
                            <Text style={[ c.light, f._18, { fontFamily: 'lineto-circular-pro-bold' }]}>Layanan Apotek</Text>
                        </View>
                        <View style={{ width: wp('15%') }}></View>
                    </View>
                </View>

                <View style={{ width: '100%', alignItems: 'center' }}>
                    <View style={{ width: wp('90%'), height: hp('6.5%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('5%'), marginBottom: wp('5%'), backgroundColor: softGrey, borderRadius: wp('6%') }}>
                        <TextInput
                            ref={ref => this.textInputRef = ref}
                            style={{ marginLeft: wp('4%'), width: wp('60%'), color: light }}
                            placeholder='Cari Apotek'
                            placeholderTextColor={light}
                        />
                        <View style={{ width: wp('22%'), alignItems: 'flex-end' }}>
                            <TouchableOpacity onPress={() => this.textInputRef.focus()}>
                                <Image source={require('../asset/search.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: light }} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>

                <ScrollView style={{ backgroundColor: '#181B22' }}>
                    <Accordion
                        activeSections={this.state.activeSections}
                        sections={listApotek}
                        touchableComponent={TouchableHighlight}
                        renderHeader={this.renderHeader}
                        renderContent={this.renderContent}
                        duration={400}
                        onChange={this.setSections}
                    />
                </ScrollView>
            </View>
        )
    }
}
