import React, { Component } from 'react'
import { Text, View, TouchableOpacity, TouchableHighlight, TextInput, Image, ScrollView, Easing, Animated, StyleSheet, RefreshControl } from 'react-native'
import { blue, light, softGrey } from '../utils/Color'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import SwipeableRating from 'react-native-swipeable-rating'
import CustomizedStarRating from 'react-native-customized-star-rating'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'
import { API_URL } from 'react-native-dotenv'

const images = {
    starFilled: require('../asset/stared.png'),
    starUnFilled: require('../asset/star.png')
}

export default class AddReview extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            scaleAnimation: new Animated.Value(1),
            filledStar: 0,
            ulasan: ''
        }
    }

    submitReview() {
        this.setState({ loading: true })

        axios.post(API_URL+'/main/shopping/produk/3/rate', {id_login: '118205008143923829902', score: this.state.filledStar, ulasan: this.state.ulasan})
        .then(res => {
            // console.log(res.data)
            if (res.data.status) {
                this.setState({ loading: false })
                this.props.navigation.pop()
            }
        })
        .catch(e => console.log(e))
    }

    async clickStar(j) {
        await this.setState({ filledStar: j })
        // console.log(this.state.filledStar)
    }

    render() {
        return (
            <ScrollView style={{ backgroundColor: '#121212' }} refreshControl={<RefreshControl onRefresh={() => this.submitReview()} refreshing={this.state.loading} />}>
                <View style={{ width: '100%', alignItems: 'center' }}>
                    <View style={{ width: wp('90%'), marginTop: wp('4%'), flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ width: wp('15%') }}>
                            <TouchableOpacity onPress={() => this.props.navigation.pop()}>
                                <FastImage source={require('../asset/left.png')} style={{ width: wp('8%'), height: wp('8%') }} />
                            </TouchableOpacity>
                        </View>
                        <View style={{ width: wp('60%'), alignItems: 'center' }}>
                            <Text style={[ c.light, f._18, { fontFamily: 'lineto-circular-pro-bold' }]}>Ulasan</Text>
                        </View>
                        <View style={{ width: wp('15%') }}></View>
                    </View>

                    <View style={{ width: wp('25%'), height: wp('25%'), backgroundColor: softGrey, alignItems: 'center', justifyContent: 'center', borderRadius: wp('20%'), marginTop: wp('8%') }}>
                        <Image source={require('../asset/user.png')} style={{ width: wp('20%'), height: wp('20%'), tintColor: '#CCC' }} />
                    </View>
                    <View style={{ width: wp('80%'), marginTop: wp('2%') }}>
                        <Text style={[ c.light, f._16, { textAlign: 'center', fontFamily: 'lineto-circular-pro-bold' }]}>Bagaimana pengalaman anda berbelanja obat dengan HealthPlus?</Text>
                    </View>
                    <View style={{ marginTop: wp('4%') }} />
                    <CustomizedStarRating
                        noOfStars={'5'}
                        starRowStyle={styles.starRowStyle}
                        starSizeStyle={styles.starSizeStyle}
                        selectedStar={this.state.filledStar}
                        starAnimationScale={1.15}
                        animationDuration={300}
                        easingType={Easing.easeInCirc}
                        emptyStarImagePath={require('../asset/star.png')}
                        filledStarImagePath={require('../asset/staredd.png')}
                        onClickFunc={(i) => this.clickStar(i)}
                    />
                    {this.state.filledStar === 1 && (
                        <Text style={[ c.light, { marginTop: wp('1%') }]}>OK</Text>
                    )}
                    {this.state.filledStar === 2 && (
                        <Text style={[ c.light, { marginTop: wp('1%') }]}>Hmm...</Text>
                    )}
                    {this.state.filledStar === 3 && (
                        <Text style={[ c.light, { marginTop: wp('1%') }]}>Good</Text>
                    )}
                    {this.state.filledStar === 4 && (
                        <Text style={[ c.light, { marginTop: wp('1%') }]}>Very Good</Text>
                    )}
                    {this.state.filledStar === 5 && (
                        <Text style={[ c.light, { marginTop: wp('1%') }]}>Amazing</Text>
                    )}

                    <View style={{ width: wp('90%'), marginTop: wp('10%'), flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ width: wp('45%') }}>
                            <Text style={[ c.light, f._16, { fontFamily: 'lineto-circular-pro-bold' }]}>Tulis Ulasan</Text>
                        </View>
                        <View style={{ width: wp('45%'), alignItems: 'center' }}></View>
                    </View>
                    <View style={{ width: wp('90%'), maxHeight: hp('20%'), borderWidth: 1, borderColor: blue, borderRadius: wp('2%'), backgroundColor: '#181B22', marginTop: wp('2%'), alignItems: 'center' }}>
                        <TextInput
                            placeholder='Input Text here'
                            placeholderTextColor={light}
                            style={{ width: wp('85%'), color: light }}
                            multiline={true}
                            onChangeText={(text) => this.setState({ ulasan: text })}
                        />
                    </View>

                    <TouchableHighlight onPress={() => this.submitReview()} underlayColor='#3399FF' style={{ width: wp('35%'), height: hp('6%'), backgroundColor: blue, alignItems: 'center', justifyContent: 'center', marginTop: wp('10%'), borderRadius: wp('2%') }}>
                        <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>Kirim Ulasan</Text>
                    </TouchableHighlight>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    starRowStyle: {
        flexDirection: 'row'
    },

    starSizeStyle: {
        width: wp('10%'),
        height: wp('10%')
    }
})