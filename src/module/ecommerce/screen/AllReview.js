import React, { Component } from 'react'
import { Text, View, TouchableHighlight, Image, ScrollView, FlatList, TouchableOpacity } from 'react-native'
import { blue, light, softGrey } from '../utils/Color'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'

export default class AllReview extends Component {
    constructor(props) {
        super(props);
        this.state = {
            review: [
                {key: '0', username: 'Michael Taner', desc: 'Ini hebat, obatnya manjur dan harganya juga terjangkau', time: '1 hari yang lalu', star: '5.0'},
                {key: '1', username: 'Pratama', desc: 'Ini hebat, obatnya manjur dan harganya juga terjangkau. Saya tidak perlu keluar untuk membelinya. Semoga obatnya tambah lengkap dan ada banyak promo ataupun diskon yang bisa didapat.', time: '1 hari yang lalu', star: '5.0'},
                {key: '2', username: 'Rizqi', desc: 'Ini hebat, obatnya manjur dan harganya juga terjangkau. Saya tidak perlu keluar untuk membelinya. Semoga obatnya tambah lengkap dan ada banyak promo ataupun diskon yang bisa didapat.', time: '1 hari yang lalu', star: '5.0'},
            ]
        }
    }

    render() {
        return (
            <View style={{ width: '100%', height: '100%', backgroundColor: '#121212' }}>
                <View style={{ width: '100%', alignItems: 'center', position: 'absolute', bottom: wp('4%'), zIndex: 1 }}>
                    <TouchableHighlight onPress={() => this.props.navigation.push('AddReview')} style={{ width: wp('30%'), height: hp('6%'), alignItems: 'center', justifyContent: 'center', backgroundColor: blue, borderRadius: wp('2%') }} underlayColor='#3399FF'>
                        <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>Tulis Ulasan</Text>
                    </TouchableHighlight>
                </View>
                <ScrollView style={{ backgroundColor: '#121212' }}>
                    <View style={{ width: '100%', alignItems: 'center', marginTop: wp('4%') }}>
                        <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
                            <View style={{ width: wp('15%') }}>
                                <TouchableOpacity onPress={() => this.props.navigation.pop()}>
                                    <FastImage source={require('../asset/left.png')} style={{ width: wp('8%'), height: wp('8%') }} />
                                </TouchableOpacity>
                            </View>
                            <View style={{ width: wp('60%'), alignItems: 'center' }}>
                                <Text style={[ c.light, f._18, { fontFamily: 'lineto-circular-pro-bold' }]}>Ulasan</Text>
                            </View>
                            <View style={{ width: wp('15%') }}></View>
                        </View>

                        <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold', marginTop: wp('4%'), fontSize: wp('10%') }]}>5.0</Text>
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: wp('2%') }}>
                            <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                            <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                            <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                            <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                            <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                        </View>
                        <Text style={[ c.light, { marginTop: wp('2%') }]}>berdasarkan dari 10 ulasan</Text>

                        <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('4%') }}>
                            <View style={{ width: wp('25%'), flexDirection: 'row' }}>
                                <Image source={require('../asset/star.png')} style={{ width: wp('4%'), height: wp('4%'), tintColor: '#FE8829' }} />
                                <Image source={require('../asset/star.png')} style={{ width: wp('4%'), height: wp('4%'), tintColor: '#FE8829' }} />
                                <Image source={require('../asset/star.png')} style={{ width: wp('4%'), height: wp('4%'), tintColor: '#FE8829' }} />
                                <Image source={require('../asset/star.png')} style={{ width: wp('4%'), height: wp('4%'), tintColor: '#FE8829' }} />
                                <Image source={require('../asset/star.png')} style={{ width: wp('4%'), height: wp('4%'), tintColor: '#FE8829' }} />
                            </View>
                            <View style={{ width: wp('55%') }}>
                                <View style={{ width: '100%', height: wp('2%'), backgroundColor: '#FE8829', borderRadius: wp('5%') }} />
                            </View>
                            <View style={{ width: wp('10%'), alignItems: 'flex-end' }}>
                                <Text style={[ c.light, f._12 ]}>10</Text>
                            </View>
                        </View>
                        <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('1%') }}>
                            <View style={{ width: wp('25%'), flexDirection: 'row' }}>
                                <Image source={require('../asset/star.png')} style={{ width: wp('4%'), height: wp('4%'), tintColor: '#FE8829' }} />
                                <Image source={require('../asset/star.png')} style={{ width: wp('4%'), height: wp('4%'), tintColor: '#FE8829' }} />
                                <Image source={require('../asset/star.png')} style={{ width: wp('4%'), height: wp('4%'), tintColor: '#FE8829' }} />
                                <Image source={require('../asset/star.png')} style={{ width: wp('4%'), height: wp('4%'), tintColor: '#FE8829' }} />
                                <Image source={require('../asset/star.png')} style={{ width: wp('4%'), height: wp('4%'), tintColor: light }} />
                            </View>
                            <View style={{ width: wp('55%') }}>
                                <View style={{ width: '100%', height: wp('2%'), backgroundColor: light, borderRadius: wp('5%') }} />
                            </View>
                            <View style={{ width: wp('10%'), alignItems: 'flex-end' }}>
                                <Text style={[ c.light, f._12 ]}>0</Text>
                            </View>
                        </View>
                        <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('1%') }}>
                            <View style={{ width: wp('25%'), flexDirection: 'row' }}>
                                <Image source={require('../asset/star.png')} style={{ width: wp('4%'), height: wp('4%'), tintColor: '#FE8829' }} />
                                <Image source={require('../asset/star.png')} style={{ width: wp('4%'), height: wp('4%'), tintColor: '#FE8829' }} />
                                <Image source={require('../asset/star.png')} style={{ width: wp('4%'), height: wp('4%'), tintColor: '#FE8829' }} />
                                <Image source={require('../asset/star.png')} style={{ width: wp('4%'), height: wp('4%'), tintColor: light }} />
                                <Image source={require('../asset/star.png')} style={{ width: wp('4%'), height: wp('4%'), tintColor: light }} />
                            </View>
                            <View style={{ width: wp('55%') }}>
                                <View style={{ width: '100%', height: wp('2%'), backgroundColor: light, borderRadius: wp('5%') }} />
                            </View>
                            <View style={{ width: wp('10%'), alignItems: 'flex-end' }}>
                                <Text style={[ c.light, f._12 ]}>0</Text>
                            </View>
                        </View>
                        <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('1%') }}>
                            <View style={{ width: wp('25%'), flexDirection: 'row' }}>
                                <Image source={require('../asset/star.png')} style={{ width: wp('4%'), height: wp('4%'), tintColor: '#FE8829' }} />
                                <Image source={require('../asset/star.png')} style={{ width: wp('4%'), height: wp('4%'), tintColor: '#FE8829' }} />
                                <Image source={require('../asset/star.png')} style={{ width: wp('4%'), height: wp('4%'), tintColor: light }} />
                                <Image source={require('../asset/star.png')} style={{ width: wp('4%'), height: wp('4%'), tintColor: light }} />
                                <Image source={require('../asset/star.png')} style={{ width: wp('4%'), height: wp('4%'), tintColor: light }} />
                            </View>
                            <View style={{ width: wp('55%') }}>
                                <View style={{ width: '100%', height: wp('2%'), backgroundColor: light, borderRadius: wp('5%') }} />
                            </View>
                            <View style={{ width: wp('10%'), alignItems: 'flex-end' }}>
                                <Text style={[ c.light, f._12 ]}>0</Text>
                            </View>
                        </View>
                        <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('1%') }}>
                            <View style={{ width: wp('25%'), flexDirection: 'row' }}>
                                <Image source={require('../asset/star.png')} style={{ width: wp('4%'), height: wp('4%'), tintColor: '#FE8829' }} />
                                <Image source={require('../asset/star.png')} style={{ width: wp('4%'), height: wp('4%'), tintColor: light }} />
                                <Image source={require('../asset/star.png')} style={{ width: wp('4%'), height: wp('4%'), tintColor: light }} />
                                <Image source={require('../asset/star.png')} style={{ width: wp('4%'), height: wp('4%'), tintColor: light }} />
                                <Image source={require('../asset/star.png')} style={{ width: wp('4%'), height: wp('4%'), tintColor: light }} />
                            </View>
                            <View style={{ width: wp('55%') }}>
                                <View style={{ width: '100%', height: wp('2%'), backgroundColor: light, borderRadius: wp('5%') }} />
                            </View>
                            <View style={{ width: wp('10%'), alignItems: 'flex-end' }}>
                                <Text style={[ c.light, f._12 ]}>0</Text>
                            </View>
                        </View>
                    </View>

                    <View style={{ width: '100%', alignItems: 'center', backgroundColor: '#181B22', marginTop: wp('8%') }}>
                        <View style={{ width: wp('90%'), marginTop: wp('4%') }}>
                            <FlatList
                                data={this.state.review}
                                keyExtractor={item => item.key}
                                renderItem={({item}) => (
                                    <>
                                        <View style={{ width: '100%', height: wp('15%'), flexDirection: 'row' }}>
                                            <View style={{ width: wp('15%'), height: '100%' }}>
                                                <View style={{ width: '100%', height: '100%', backgroundColor: softGrey, borderRadius: wp('10%'), alignItems: 'center', justifyContent: 'center' }}>
                                                    <Image source={require('../asset/user.png')} style={{ width: wp('10%'), height: wp('10%'), tintColor: '#CCC' }} />
                                                </View>
                                            </View>
                                            <View style={{ width: wp('50%'), height: '100%' }}>
                                                <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold', marginLeft: wp('4%'), marginTop: wp('1%') }]}>{item.username}</Text>
                                                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: wp('1%'), marginLeft: wp('4%') }}>
                                                    {item.star === '5.0' ?
                                                        <>
                                                            <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                            <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                            <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                            <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                            <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                        </>
                                                        :
                                                        <>
                                                            <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                            <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                            <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                            <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: '#FE8829' }} />
                                                            <Image source={require('../asset/star.png')} style={{ width: wp('5%'), height: wp('5%'), tintColor: light }} />
                                                        </>
                                                    }
                                                    <Text style={[ c.light, { marginLeft: wp('2%') }]}>{item.star}</Text>
                                                </View>
                                            </View>
                                            <View style={{ width: wp('25%'), height: '100%', alignItems: 'flex-end' }}>
                                                <Text style={[ c.light, f._12, { marginTop: wp('7.3%') }]}>{item.time}</Text>
                                            </View>
                                        </View>

                                        <View style={{ width: '100%', marginTop: wp('2%'), marginBottom: wp('4%') }}>
                                            <Text style={[ c.light, { textAlign: 'justify' }]}>{item.desc}</Text>
                                        </View>
                                    </>
                                )}
                            />
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}
