import React, { Component } from 'react'
import { Text, View, TouchableOpacity, ScrollView, TextInput, FlatList } from 'react-native'
import { blue, light, grey, softGrey } from '../utils/Color'
import { b, p, f, c } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import SearchBar from 'react-native-search-bar'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'

export default class Recomendation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            search: '',
            gejala:[
                {'value': 'Sakit Kepala', 'key': '0'},
                {'value': 'Batuk', 'key': '1'},
                {'value': 'Demam', 'key': '2'},
                {'value': 'Sakit Gigi', 'key': '3'},
                {'value': 'Flu', 'key': '4'},
                {'value': 'Nyeri Otot', 'key': '5'},
            ],
            artikel:[
                {'image': require('../asset/pusing.png'), 'value': 'Demam dan Batuk? Jangan Panik! Kenali Perbedaan Gejala-19', 'uploaded': 'HealthPlus | 23 April 2020', 'key': '0'},
                {'image': require('../asset/pusing.png'), 'value': 'Demam dan Batuk? Jangan Panik! Kenali Perbedaan Gejala-19', 'uploaded': 'HealthPlus | 23 April 2020', 'key': '1'},
                {'image': require('../asset/pusing.png'), 'value': 'Demam dan Batuk? Jangan Panik! Kenali Perbedaan Gejala-19', 'uploaded': 'HealthPlus | 23 April 2020', 'key': '2'},
            ]
        }
    }

    render() {
        return (
            <ScrollView style={{ backgroundColor: '#181B22' }}>
                <View style={[ p.center, { width: '100%', height: hp('28%'), backgroundColor: '#121212' }]}>
                    <Text style={[ c.light, f._18, { fontFamily: 'lineto-circular-pro-bold' }]}>Rekomendasi</Text>
                    <TextInput
                        style={{ width: wp('80%'), height: hp('6%'), backgroundColor: grey, marginTop: wp('4%'), borderRadius: wp('5%') }}
                        placeholder='Bagaimana keadaanmu hari ini?'
                        placeholderTextColor={softGrey}
                    />
                </View>
                <View style={{ width: '100%', alignItems: 'center', marginBottom: wp('4%') }}>
                    <FlatList
                        data={this.state.gejala}
                        keyExtractor={item => item.key}
                        numColumns={3}
                        renderItem={({item}) => (
                            <>
                                <View style={{ width: wp('33%'), alignItems: 'center' }}>
                                    <TouchableOpacity onPress={() => this.props.navigation.push('Gejala')} style={[ p.center, { width: wp('26%'), height: hp('5%'), borderWidth: 1, borderColor: blue, borderRadius: wp('3%'), marginTop: wp('4%') }]}>
                                        <Text style={[ c.blue, { fontFamily: 'lineto-circular-pro-bold' }]}>{item.value}</Text>
                                    </TouchableOpacity>
                                </View>
                            </>
                        )}
                    />
                    <TouchableOpacity style={[ p.center, { width: wp('26%'), height: hp('5%'), borderWidth: 1, borderColor: softGrey, borderRadius: wp('3%'), marginTop: wp('4%') }]}>
                        <Text style={{ fontFamily: 'lineto-circular-pro-bold', color: softGrey }}>Show more</Text>
                    </TouchableOpacity>

                    <Text style={{ color: 'darkorange', fontFamily: 'lineto-circular-pro-bold', marginTop: wp('5%') }}>Artikel Terakhir</Text>
                    <FlatList
                        data={this.state.artikel}
                        keyExtractor={item => item.key}
                        renderItem={({item}) => (
                            <TouchableOpacity onPress={() => this.props.navigation.push('ArticleDetail')} style={{ width: wp('90%'), flexDirection: 'row', marginTop: wp('2%') }}>
                                <FastImage source={item.image} style={{ width: wp('30%'), height: wp('30%') }} />
                                <View style={{ width: wp('55%'), marginLeft: wp('3%') }}>
                                    <Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>{item.value}</Text>
                                    <Text style={[ c.light, f._12, { marginTop: wp('2%') }]}>{item.uploaded}</Text>
                                </View>
                            </TouchableOpacity>
                        )}
                    />
                    <TouchableOpacity onPress={() => this.props.navigation.push('Article')} style={[ p.center, { width: wp('26%'), height: hp('5%'), borderWidth: 1, borderColor: softGrey, borderRadius: wp('3%'), marginTop: wp('4%') }]}>
                        <Text style={{ fontFamily: 'lineto-circular-pro-bold', color: softGrey }}>View All</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        )
    }
}
