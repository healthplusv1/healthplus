import { createStackNavigator } from 'react-navigation-stack'
import eCommercePage from './screen/eCommerce'
import RecomendationPage from './screen/Recomendation'
import BuyMedicinePage from './screen/BuyMedicine'
import BuySuplementPage from './screen/BuySuplement'
import CosmeticsPage from './screen/Cosmetics'
import SexualsPage from './screen/Sexuals'
import ArticlePage from './screen/Article'
import ArticleDetailPage from './screen/ArticleDetail'
import GejalaPage from './screen/Gejala'
import MedicineListPage from './screen/MedicineList'
import BannersPromoPage from './screen/BannersPromo'

export const eCommercestack = createStackNavigator({
    eCommerce: {
        screen: eCommercePage,
        navigationOptions:{
            header: null
        }
    },

    Recomendation: {
        screen: RecomendationPage,
        navigationOptions:{
            header: null
        }
    },

    BuyMedicine: {
        screen: BuyMedicinePage,
        navigationOptions:{
            header: null
        }
    },

    BuySuplement: {
        screen: BuySuplementPage,
        navigationOptions:{
            header: null
        }
    },

    Cosmetics: {
        screen: CosmeticsPage,
        navigationOptions:{
            header: null
        }
    },

    Sexuals: {
        screen: SexualsPage,
        navigationOptions:{
            header: null
        }
    },

    Article: {
        screen: ArticlePage,
        navigationOptions:{
            header: null
        }
    },

    ArticleDetail: {
        screen: ArticleDetailPage,
        navigationOptions:{
            header: null
        }
    },

    Gejala: {
        screen: GejalaPage,
        navigationOptions:{
            header: null
        }
    },

    MedicineList: {
        screen: MedicineListPage,
        navigationOptions:{
            header: null
        }
    },

    BannersPromo: {
        screen: BannersPromoPage,
        navigationOptions: {
            header: null
        }
    }
})