import React from 'react'
import { View, Text, ActivityIndicator, TouchableOpacity, Image, StyleSheet, Alert } from 'react-native'
import { b, p, f, c } from '../utils/StyleHelper'
import { vs, xs } from '../utils/Responsive'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import { blue, light } from '../utils/Color'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import {NavigationEvents} from 'react-navigation'
import FastImage from 'react-native-fast-image'
import Icon from 'react-native-vector-icons/FontAwesome'
import { API_URL } from 'react-native-dotenv'

export default class ListSocial extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			suka: '2',
			value: 0,
			liked: false,
			id_login: '',
			id_post_group: '',
			like_post: null,
			isLoading: false,
			image: require('../asset/loveLight.png')
		}
	}

	// async componentDidMount(){
	// 	const id_login = await AsyncStorage.getItem('@id_login')
	// 	this.setState({ 
	// 		id_login: id_login,
	// 		isLoading: true
	// 	})

	// 	//  alert(this.props.like_id_login)
	//     axios.get(API_URL+'/main/get_funstation_latest_suka', {params:{
  	// 		id_post_group: this.props.id
  	// 	}})
  	// 	.then(result => {
	//     //   alert(JSON.stringify(result))
	//       this.setState({
	// 		suka: result.data.data.suka,
	// 		isLoading: false
	//       })
	// 	})
	// }

	async _increaseValue() {
		if (this.state.liked === false) {
				this.setState({
					liked: true,
					value: this.state.value +1,
					image: require('../asset/loved.png')
				})
			}
		else {    
			this.setState({
				liked: false,
				value: this.state.value -1,
				image: require('../asset/loveLight.png')
			})
		}
		// const id_login = await AsyncStorage.getItem('@id_login');

		// axios.get(API_URL+'/main/insert_like_post', {params:{
		// 	id_login: id_login,
		// 	id_post_group: this.props.id
		// }})
		// .then(resp => {
		// if (resp.data.status === 'true') {
		// 	// Alert.alert("Berhasil")
		// 	if (this.props.like_id_login === null) {
		// 		this.setState({
		// 			suka: '...',
		// 			id_login: null
		// 		})	
		// 	}else{
		// 		this.setState({
		// 			suka: '...',
		// 			id_login: id_login
		// 		})	
		// 	}
		// 	axios.get(API_URL+'/main/get_funstation_latest_suka', {params:{
		// 		id_post_group: this.props.id
		// 	}})
		// 	.then(result => {
		// 		// alert(JSON.stringify(result))
		// 		this.setState({
		// 		suka: result.data.data.suka
		// 		})
		// 	})
		// 	.catch(e => {
		// 		if (e.message === 'Network Error') {
		// 		Alert.alert('Tidak Ada Koneksi Internet','Mohon Periksa Koneksi Internet Anda Lalu Restart Aplikasi')
		// 		// Alert.alert('Network Error','Please Check Your Internet Connection then Restart The App')
		// 		}else{
		// 		// Alert.alert('', JSON.stringify(e.message))
		// 		}
		// 	})
		// } else {
		// 	Alert.alert("Gagal")
		// }
		// })
		// .catch(err => {
		// 	// Alert.alert("Error")
		// })
	}
	  
	async _decreaseValue() {
		// const id_login = await AsyncStorage.getItem('@id_login')

		// axios.get(API_URL+'/main/delete_like_post', {params:{
		// 	id_login: id_login,
		// 	id_post_group: this.props.id
		// }})
		// .then(resp => {
		// 	if (resp.data.status === 'true') {
		// 		// Alert.alert("Berhasil")
		// 		if (this.props.like_id_login === null) {
		// 			this.setState({
		// 				suka: '...',
		// 				id_login: id_login
		// 			})	
		// 		}else{
		// 			this.setState({
		// 				suka: '...',
		// 				id_login: null
		// 			})	
		// 		}
		// 		axios.get(API_URL+'/main/get_funstation_latest_suka', {params:{
		// 			id_post_group: this.props.id
		// 		}})
		// 		.then(result => {
		// 		// alert(JSON.stringify(result))
		// 		this.setState({
		// 		  suka: result.data.data.suka
		// 		})
		// 	  })
		// 	  .catch(e => {
		// 		if (e.message === 'Network Error') {
		// 		  Alert.alert('Tidak Ada Koneksi Internet','Mohon Periksa Koneksi Internet Anda Lalu Restart Aplikasi')
		// 		  // Alert.alert('Network Error','Please Check Your Internet Connection then Restart The App')
		// 		}else{
		// 		  // Alert.alert('', JSON.stringify(e.message))
		// 		}
		// 	  })
		// 	} else {
		// 		Alert.alert("Gagal")
		// 	}
		// })
		// .catch(err => {
		// 	// Alert.alert("Error")
		// })
	}

	render() {
		if (this.state.isLoading) {
			return (
				<View style={[styles.container_loading, styles.horizontal_loading]}>
				  <ActivityIndicator size="large" color="#0000ff" />
				</View>
			)
		}else{
			return (
				<View style={[ b.mb4, { width: wp('90%'), backgroundColor: '#181B22', borderRadius: wp('5%') }]}>
					{/* <NavigationEvents onDidFocus={() => this.componentDidMount()} /> */}
					<View style={{ marginTop: wp('4%') }}>
						<View style={{ width: wp('85%') }}>
							<View style={[ p.row ]}>
								<TouchableOpacity onPress={eval(this.props.action)}>
									<FastImage source={this.props.url_avatar} style={[ b.ml4, { width: xs(50), height: xs(50) }]} />
								</TouchableOpacity>
								<View style={[ b.ml3, b.mt2 ]}>
									<TouchableOpacity onPress={eval(this.props.action)}>
										<Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}> {this.props.nama} </Text>
									</TouchableOpacity>
									<TouchableOpacity onPress={eval(this.props.actionGroup)}>
										<Text style={[ b.ml1, { fontFamily: 'lineto-circular-pro-bold', color: 'darkorange' }]} numberOfLines={1}>{this.props.nama_group}</Text>
									</TouchableOpacity>
								</View>
							</View>
							<Text style={[ b.ml4, b.mt2, f._12, c.light ]}>{this.props.deskripsi}</Text>
						</View>
					</View>
					<View style={[ b.mt2, p.center, { width: '100%' }]}>
						{this.props.post_gambar === null ?
							null
							:
							<FastImage source={this.props.post_gambar} style={{ width: xs(300), height: xs(300), borderRadius: wp('3%') }} />
						}
					</View>
					<View style={{ width: '100%', marginTop: wp('3%'), marginBottom: wp('3%'), alignItems: 'center' }}>
						<View style={{ width: wp('80%'), flexDirection: 'row' }}>
							<View style={{ width: wp('50%'), flexDirection: 'row', alignItems: 'center' }}>
								<View style={{ width: wp('25%'), flexDirection: 'row', alignItems: 'center' }}>
									{/* {this.props.like_id_login === this.state.id_login ?
										<TouchableOpacity activeOpacity={.5} onPress={() => {this._decreaseValue()}}>
											<View style={[ p.row ]}>
												<Image source={require('../asset/loved.png')} style={{ width: xs(20), height: xs(20) }} />
											</View>
										</TouchableOpacity>
										: 
										<TouchableOpacity activeOpacity={.5} onPress={() => {this._increaseValue()}}>
											<View style={[ p.row ]}>
												<Image source={require('../asset/love.png')} style={{ width: xs(20), height: xs(20), tintColor: light }} />
											</View>
										</TouchableOpacity>
									} */}
									<TouchableOpacity activeOpacity={.5} onPress={() => {this._increaseValue()}}>
										<FastImage source={this.state.image} style={{ width: xs(20), height: xs(20) }} />
									</TouchableOpacity>
									<Text style={[ c.light ]}> {this.state.value}</Text>
								</View>
								<View style={{ width: wp('25%'), flexDirection: 'row', alignItems: 'center' }}>
									<TouchableOpacity onPress={eval(this.props.comment)}>
										<Image source={require('../asset/comment.png')} style={{ width: xs(20), height: xs(20), tintColor: light }} />
									</TouchableOpacity>
									<Text style={[ c.light ]}> {this.props.komentar} </Text>
								</View>
							</View>
							<View style={{ width: wp('30%'), alignItems: 'flex-end' }}>
								<Text style={[ c.light, f._12 ]}>{this.props.tanggal_post}</Text>
							</View>
						</View>
					</View>
				</View>
			)
		}
	}
}

const styles = StyleSheet.create({
	// loading centered screenn
	container_loading: {
	  flex: 1,
	  justifyContent: "center"
	},
	horizontal_loading: {
	  flexDirection: "row",
	  justifyContent: "space-around",
	  padding: 10
	},
  
	actionButtonIcon: {
	  fontSize: 20,
	  height: 22,
	  color: 'white',
	},
});