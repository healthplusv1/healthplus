import React from 'react'
import { View, Text, ActivityIndicator, FlatList, StyleSheet, ScrollView, TouchableOpacity, Image, TouchableHighlight } from 'react-native'
import { b, p, f, c } from '../utils/StyleHelper'
import { vs, xs } from '../utils/Responsive'
import { blue, light } from '../utils/Color'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import LinearGradient from 'react-native-linear-gradient'
import {NavigationEvents} from 'react-navigation'
import ListSocial from './ListSocial.js'
import Icon from 'react-native-vector-icons/Ionicons'
import FastImage from 'react-native-fast-image'
import ActionButton from 'react-native-action-button'
import { Tabs, Tab, Container } from 'native-base'
import analytics from '@react-native-firebase/analytics'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'
import { API_URL } from 'react-native-dotenv'

export default class Social extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            refreshing: false,
            social: [
                {key: '0', url_gambar: require('../asset/10.png'), nama: 'Michael', tanggal_post: '2021-04-10', deskripsi: 'Salam kenal semuanya', post_gambar: null, id_post_group: 1, komentar: '2', like_id_login: '', nama_group: 'USM Sehat', id_group: 1},
                {key: '1', url_gambar: require('../asset/10.png'), nama: 'Pratama', tanggal_post: '2021-04-16', deskripsi: 'Push Up dulu guys', post_gambar: require('../asset/pushUpp.jpg'), id_post_group: 2, komentar: '1', like_id_login: '', nama_group: 'GunungPati Sehat', id_group: 2},
            ],
            group2: [
                {id_group: '1', image: require('../asset/icon_group.png'), nama_group: 'USM Sehat'},
                {id_group: '2', image: require('../asset/icon_group.png'), nama_group: 'Lari Semarang'},
                {id_group: '3', image: require('../asset/icon_group.png'), nama_group: 'Nike Run Club Semarang'},
            ],
            group1: [
                {id_group: '1', image: require('../asset/icon_group.png'), nama_group: 'USM Sehat', lokasi: 'USM', anggota: '45'},
                {id_group: '2', image: require('../asset/icon_group.png'), nama_group: 'UNNES Run Club', lokasi: 'UNNES', anggota: '36'},
                {id_group: '3', image: require('../asset/icon_group.png'), nama_group: 'Semarang Sehat', lokasi: 'Simpang Lima', anggota: '40'},
            ],
            count: '',
            list_friends: [
                {id: 1, image: require('../asset/10.png'), nama: 'Michael Taner', status: 'Teman'},
                {id: 2, image: require('../asset/10.png'), nama: 'Michael Taner', status: 'Teman'},
                {id: 3, image: require('../asset/10.png'), nama: 'Michael Taner', status: 'Teman'},
                {id: 4, image: require('../asset/10.png'), nama: 'Michael Taner', status: 'Teman'},
                {id: 5, image: require('../asset/10.png'), nama: 'Michael Taner', status: 'Teman'},
            ],
            add_friends: [
                {id: 1, image: require('../asset/10.png'), nama: 'Michael Taner', status: 'Tambah'},
                {id: 2, image: require('../asset/10.png'), nama: 'Michael Taner', status: 'Tambah'},
                {id: 3, image: require('../asset/10.png'), nama: 'Michael Taner', status: 'Tambah'},
                {id: 4, image: require('../asset/10.png'), nama: 'Michael Taner', status: 'Tambah'},
                {id: 5, image: require('../asset/10.png'), nama: 'Michael Taner', status: 'Tambah'},
            ],
            request_friends: [
                {id: 1, image: require('../asset/10.png'), nama: 'Michael Taner', status: 'Request'},
                {id: 2, image: require('../asset/10.png'), nama: 'Michael Taner', status: 'Request'},
                {id: 3, image: require('../asset/10.png'), nama: 'Michael Taner', status: 'Request'},
                {id: 4, image: require('../asset/10.png'), nama: 'Michael Taner', status: 'Request'},
                {id: 5, image: require('../asset/10.png'), nama: 'Michael Taner', status: 'Request'},
            ],
            status_friends: [
                {status: 'Teman'},
                {status: 'Tambahkan Teman'},
                {status: 'Permintaan Pertemanan'},
            ],
            detail_status_friends: null,
            clicked: 0,
            tabs_clicked: 0,
            tabs_title: [
                {image: require('../asset/post_icon.png')},
                {image: require('../asset/league_icon.png')},
                {image: require('../asset/friend_icon.png')}
            ]
        }
    }

    async componentDidMount() {
        this.setState({ loading: true })

        await analytics().logEvent('view_profile', {
            item: 'SOCIAL_SCREEN'
        })
        await analytics().setUserProperty('user_sees_post', 'null')
        
        // const id_group = await AsyncStorage.getItem('@current_id_group')
        // const id_login = await AsyncStorage.getItem('@id_login')

        axios.get(API_URL+'/main/shopping/cart', {params: {
			id_login: '118205008143923829902'
		}})
		.then(res => {
			this.setState({
				loading: false,
				count: res.data.data.length,
			})
		})
		.catch(e => {
            this.setState({ loading: false })
			console.log(e)
		})

        // axios.get(API_URL+'/main/get_global_group_post', {params: {
        //     id_login: id_login
        // }})
		// .then(data => {
		// 	this.setState({
        //         list_posting: data.data.data,
        //         loading: false,
        //         refreshing: false,
		// 	})
		// 	// alert(JSON.stringify(data))
		// })

        // axios.get(API_URL+'/main/get_all_anggota_group', {params:{
		// 	id_login: id_login
		// }})
		// .then(result => {
		// 	this.setState({
		// 		loading: false,
		// 		group1: result.data.data
		// 	})
		// 	// alert(JSON.stringify(result.data.data))
		// })
		// .catch(e => {
		// 	console.log('get_anggota_group => '+e)
		// })

		// axios.get(API_URL+'/main/get_admin_group', {params:{
		// 	id_login: id_login
		// }})
		// .then(result => {
		// 	this.setState({
		// 		loading: false,
		// 		group2: result.data.data
		// 	})
		// 	// alert(JSON.stringify(result))
		// })
		// .catch(e => {
		// 	console.log('get_admin_group => '+e)
		// })
    }

    async gabungGroup(id_group) {
		const id_login = await AsyncStorage.getItem('@id_login');

		axios.get(API_URL+'/main/request_join_anggota_league', {params:{
			id_login: id_login,
			id_league: id_group
		}})
		.then(response => {
			// alert(JSON.stringify(response))
			if(response.data.status === 'true') {
				Alert.alert('', 'Permintaan untuk Bergabung ke League Terkirim')
				
				axios.get(API_URL+'/main/plus_point', {params:{
					id_login: id_login,
					point: 2,
				}})
				.then(() => {
					console.info('success added 2 point')
					// Alert.alert('',JSON.stringify(result.data))
				})
			}
			this.setState({
				status_join: response.data.status,
			})
			this.componentDidMount()
		})
	}

    hitung_hari(from_date) {
		var date1 = new Date(from_date); 
		var date2 = new Date(new Date().getFullYear(),new Date().getMonth() , new Date().getDate()); 
		// console.log(date1);
		// console.log(date2);
		  
		// To calculate the time difference of two dates 
		var Difference_In_Time = date2.getTime() - date1.getTime(); 
		  
		// To calculate the no. of days between two dates 
		var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
		if (typeof Difference_In_Days === 'number') {
			if(Number(Difference_In_Days.toString().replace('-', '')).toFixed() == 0){
				return 'Today';
			}else{
				return Number(Difference_In_Days.toString().replace('-', '')).toFixed()+' hari yang lalu';
			}
		}
	}

    async swicth(status, index) {
        await this.setState({
            loading: false,
            clicked: index,
            detail_status_friends: status
        })
        // alert(JSON.stringify(this.state.clicked))
		// const id_login = await AsyncStorage.getItem('@id_login')

		// axios.get(API_URL+'/main/filter_kompetisi', {params: {
		// 	id_login: id_login,
		// 	jenis_challenge: jenis_challenge
		// }})
		// .then(resp => {
		// })
	}

    async switchTab(index) {
        await this.setState({
            tabs_clicked: index
        })
    }

    render() {
        return (
            <View style={{ width: '100%', height: '100%', backgroundColor: '#121212' }}>
                {this.state.loading ?
					<View style={[styles.container_loading, styles.horizontal_loading]}>
						<ActivityIndicator size="large" color="#0000ff" />
					</View>
					:
					null
				}
                {/* <NavigationEvents onDidFocus={() => this.componentDidMount()} /> */}
                <View style={{ width: '100%', alignItems: 'center' }}>
                    <View style={{ width: wp('90%'), marginTop: wp('4%'), marginBottom: wp('4%'), flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ width: wp('45%') }}>
                            <FastImage source={require('../asset/healthPlus.png')} style={{ width: xs(160), height: xs(38) }} />
                        </View>
                        <View style={{ width: wp('45%'), flexDirection: 'row-reverse' }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Drawer')}>
                                <Image source={require('../asset/menu.png')} style={{ width: wp('7%'), height: wp('7%'), tintColor: light }} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Notifikasi')} style={{ marginRight: wp('2%') }}>
                                <Icon name="ios-notifications" size={25} color={light} />
                                {this.state.notif === 'true' ?
                                    <View style={{ width: xs(10), height: xs(10), backgroundColor: 'orangered', borderRadius: xs(5),
                                    position: 'absolute', marginLeft: vs(6), marginTop: vs(1) }} />
                                    :
                                    null
                                }
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.props.navigation.push('Cart')} style={{ marginRight: wp('2%') }}>
                                {this.state.count !== 0 ?
                                <View style={{ width: wp('3.5%'), height: wp('3.5%'), backgroundColor: 'orange', position: 'absolute', borderRadius: wp('3.5%'), zIndex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={[ c.light, f._8 ]}>{this.state.count}</Text>
                                </View>
                                :
                                null
                                }
                                <Image source={require('../asset/addCart.png')} style={{ width: wp('7%'), height: wp('7%'), tintColor: light }} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.props.navigation.push('FrequentlyOrder')} style={{ marginRight: wp('2%') }}>
                                <Image source={require('../asset/Frequentlyorder_icon.png')} style={{ width: wp('8%'), height: wp('7%'), tintColor: light }} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <FlatList
                    horizontal={true}
                    data={this.state.tabs_title}
                    keyExtractor={item => item.image}
                    renderItem={({ item, index }) => (
                        (this.state.tabs_clicked === index ?
                            <TouchableHighlight onPress={() => this.switchTab(index)} style={{ width: wp('33.3%'), height: hp('8%'), alignItems: 'center', justifyContent: 'center', backgroundColor: blue }}>
                                <Image source={item.image} style={{ width: xs(25), height: xs(22), marginTop: wp('-2.5%') }} />
                            </TouchableHighlight>
                            :
                            <TouchableHighlight onPress={() => this.switchTab(index)} style={{ width: wp('33.3%'), height: hp('8%'), alignItems: 'center', justifyContent: 'center', backgroundColor: '#181B22' }}>
                                <Image source={item.image} style={{ width: xs(25), height: xs(22), marginTop: wp('-2.5%') }} />
                            </TouchableHighlight>
                        )
                    )}
                />

                {this.state.tabs_clicked === 0 && (
                    <>
                        <ActionButton buttonColor={blue} style={{ zIndex: 1 }} fixNativeFeedbackRadius={true}>
                            <ActionButton.Item buttonColor='#9b59b6' title="Tambah Postingan" onPress={() => this.props.navigation.navigate('AddComment')}>
                                <Icon name="md-create" size={20} color={light} />
                            </ActionButton.Item>
                        </ActionButton>
                        <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#121212' }}>
                            <View style={{ width: '100%', alignItems: 'center' }}>
                                <FlatList
                                    ListHeaderComponent={<View style={{ marginTop: wp('4%') }} />}
                                    data={this.state.social}
                                    keyExtractor={item => item.key}
                                    renderItem={({item}) => (
                                        <ListSocial
                                            url_avatar={item.url_gambar}
                                            nama={item.nama}
                                            tanggal_post={this.hitung_hari(item.tanggal_post)}
                                            deskripsi={item.deskripsi}
                                            post_gambar={item.post_gambar}
                                            id={item.id_post_group}
                                            komentar={item.komentar}
                                            like_id_login={item.like_id_login}
                                            nama_group={item.nama_group}
                                            action={() => this.props.navigation.push('Profile', {id_login: item.id_login})}
                                            comment={() => this.props.navigation.push('LihatKome', {id_post_group: item.id_post_group, id_login: item.id_login})}
                                            actionGroup={() => this.props.navigation.push('Grup_Chall', {id_group: item.id_group})}
                                        />
                                    )}
                                />
                            </View>
                        </ScrollView>
                    </>
                )}

                {this.state.tabs_clicked === 1 && (
                    <>
                        <ActionButton buttonColor={blue} style={{ zIndex: 1 }} fixNativeFeedbackRadius={true}>
                            <ActionButton.Item buttonColor='#9b59b6' title="Buat Liga" onPress={() => this.props.navigation.navigate('BuatGrup')}>
                                <Icon name="md-create" size={20} color={light} />
                            </ActionButton.Item>
                        </ActionButton>
                        <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#121212' }}>
                            <View style={{ width: '100%' }}>
                                <View style={[ b.mt2, { width: '100%', backgroundColor: '#181B22' }]}>
                                    <Text style={[ c.light, b.ml4, b.mt3, { marginBottom: wp('1%') }]}>Liga Saya</Text>
                                    <View style={{ width: '100%', borderBottomWidth: 3, borderBottomColor: blue }} />
                                    <View style={[ b.pl2, b.pr2 ]}>
                                        {this.state.group2 === undefined ?
                                            <View style={[ p.center, b.mt1, b.mb2, { width: '100%' }]}>
                                                <View style={[ b.rounded, p.center, { width: xs(320), backgroundColor: '#000' }]}>
                                                    <Text style={[ b.mt1, b.mb1, c.light ]}>Anda belum Mengikuti Liga apapun</Text>
                                                </View>
                                            </View>
                                            :
                                            <ScrollView showsHorizontalScrollIndicator={false} horizontal={true} style={[ b.mt3, b.mb3 ]}>
                                                <FlatList
                                                    horizontal={true}
                                                    data={this.state.group2}
                                                    keyExtractor={item => item.id_group}
                                                    renderItem={({item}) => (
                                                        <TouchableOpacity onPress={() => this.props.navigation.push('Grup_Chall', {id_group: item.id_group})} style={[ b.ml2 ]}>
                                                            <View style={[ p.center, { width: xs(100) }]}>
                                                                <FastImage source={item.image} style={[ b.rounded, { width: xs(80), height: xs(80) }]} />
                                                                <Text style={[ p.textCenter, b.mt1, f._12, c.light ]} numberOfLines={2}>{item.nama_group}</Text>
                                                            </View>
                                                        </TouchableOpacity>
                                                    )}
                                                />
                                            </ScrollView>
                                        }
                                    </View>
                                </View>

                                <View style={[ b.mt2, { width: '100%', backgroundColor: '#181B22' }]}>
                                    <Text style={[ c.light, b.ml4, b.mt4, f._18, f.bold, { marginBottom: wp('1%') }]}>Semua Liga</Text>
                                    <View style={{ borderBottomWidth: 3, borderBottomColor: blue }} />
                                    {this.state.group1 === undefined ?
                                        <View style={[ p.center, b.mt1, b.mb2, { width: '100%' }]}>
                                            <View style={[ b.rounded, p.center, { width: xs(320), backgroundColor: '#555555' }]}>
                                                <Text style={[ b.mt1, b.mb1, c.light ]}>Tidak ada Liga yang Terserdia</Text>
                                            </View>
                                        </View>
                                        :
                                        <FlatList
                                            ListHeaderComponent={<View style={[ b.mt4 ]} />}
                                            data={this.state.group1}
                                            keyExtractor={item => item.id_group}
                                            renderItem={({item}) => (
                                                <>
                                                    <TouchableOpacity onPress={() => this.props.navigation.push('Grup_Chall', {id_group: item.id_group})} style={[ b.ml4 ]}>
                                                        <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center' }}>
                                                            <FastImage source={item.image} style={[ b.rounded, { width: wp('25%'), height: wp('25%') }]} />
                                                            <View style={[ b.ml4, { width: wp('55%') }]}>
                                                                <View style={{ width: xs(225) }}>
                                                                    <Text style={[ c.light ]} numberOfLines={1}>{item.nama_group}</Text>
                                                                </View>
                                                                <View style={[ c.light, p.row ]}>
                                                                    <Text style={[ c.light ]}>{item.anggota} Anggota</Text>
                                                                    <View style={[ b.bordered, b.rounded, b.ml1, b.mr1, { backgroundColor: '#DADADA', width: xs(7), height: xs(7), marginTop: vs(5) }]} />
                                                                    <View style={{ width: xs(160) }}>
                                                                        <Text style={[ c.light ]} numberOfLines={1}>{item.lokasi}</Text>
                                                                    </View>
                                                                </View>
                                                                <TouchableOpacity onPress={() => this.gabungGroup(item.id_group)} style={[ p.center, b.rounded, b.mt1, { borderWidth: 1, borderColor: '#2C84FF', width: xs(100) }]}>
                                                                    <Text style={[ b.mt1, b.mb1, { color: '#2C84FF' }]}>Gabung</Text>
                                                                </TouchableOpacity>
                                                            </View>
                                                        </View>
                                                    </TouchableOpacity>
                                                    <View style={[ b.mt2, b.mb2, { width: '100%', borderBottomWidth: 1, borderBottomColor: '#555555' }]} />
                                                </>
                                            )}
                                        />
                                    }
                                </View>
                            </View>
                        </ScrollView>
                    </>
                )}

                {this.state.tabs_clicked === 2 && (
                    <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#121212' }}>
                        <View style={{ marginLeft: wp('5%'), marginTop: wp('4%') }}>
                            <FlatList
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                                data={this.state.status_friends}
                                keyExtractor={item => item.status}
                                renderItem={({ item, index }) => (
                                    (this.state.clicked === index ?
                                        <TouchableOpacity onPress={() => this.swicth(item.status, index)} style={{ backgroundColor: blue, borderRadius: wp('2%'), marginRight: wp('5%'), alignItems: 'center', justifyContent: 'center' }}>
                                            <Text style={[ c.light, { marginTop: wp('2%'), marginBottom: wp('2%'), marginLeft: wp('4%'), marginRight: wp('4%') }]}>{item.status}</Text>
                                        </TouchableOpacity>
                                        :
                                        <TouchableOpacity onPress={() => this.swicth(item.status, index)} style={{ borderWidth: 1, borderColor: blue, borderRadius: wp('2%'), marginRight: wp('5%'), alignItems: 'center', justifyContent: 'center' }}>
                                            <Text style={[ c.light, { marginTop: wp('2%'), marginBottom: wp('2%'), marginLeft: wp('4%'), marginRight: wp('4%') }]}>{item.status}</Text>
                                        </TouchableOpacity>
                                    )
                                )}
                            />
                        </View>
                        {this.state.clicked === 0 && (
                            <FlatList
                                ListHeaderComponent={<View style={{ marginTop: wp('4%') }} />}
                                data={this.state.list_friends}
                                keyExtractor={item => item.id}
                                renderItem={({item}) => (
                                    <View style={{ width: '100%', alignItems: 'center' }}>
                                        <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
                                            <TouchableOpacity onPress={() => this.props.navigation.push('ProfTracking')} style={{ width: wp('20%') }}>
                                                <FastImage source={item.image} style={{ width: wp('15%'), height: wp('15%') }} />
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={() => this.props.navigation.push('ProfTracking')} style={{ width: wp('50%') }}>
                                                <Text style={[ c.light, f.bold ]} numberOfLines={1}>{item.nama}</Text>
                                            </TouchableOpacity>
                                            <View style={{ width: wp('20%'), alignItems: 'flex-end' }}>
                                                <View style={{ width: wp('18%'), height: hp('5%'), backgroundColor: blue, borderRadius: wp('1%'), alignItems: 'center', justifyContent: 'center' }}>
                                                    <Text style={[ c.light ]}>{item.status}</Text>
                                                </View>
                                            </View>
                                        </View>
                                        <View style={{ width: '100%', borderBottomWidth: 1, borderBottomColor: blue, marginTop: wp('4%'), marginBottom: wp('4%') }} />
                                    </View>
                                )}
                            />
                        )}
                        {this.state.detail_status_friends === 'Tambahkan Teman' && (
                            <FlatList
                                ListHeaderComponent={<View style={{ marginTop: wp('4%') }} />}
                                data={this.state.add_friends}
                                keyExtractor={item => item.id}
                                renderItem={({item}) => (
                                    <View style={{ width: '100%', alignItems: 'center' }}>
                                        <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
                                            <TouchableOpacity onPress={() => this.props.navigation.push('ProfTracking')} style={{ width: wp('20%') }}>
                                                <FastImage source={item.image} style={{ width: wp('15%'), height: wp('15%') }} />
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={() => this.props.navigation.push('ProfTracking')} style={{ width: wp('50%') }}>
                                                <Text style={[ c.light, f.bold ]} numberOfLines={1}>{item.nama}</Text>
                                            </TouchableOpacity>
                                            <View style={{ width: wp('20%'), alignItems: 'flex-end' }}>
                                                <TouchableOpacity style={{ width: wp('18%'), height: hp('5%'), backgroundColor: blue, borderRadius: wp('1%'), alignItems: 'center', justifyContent: 'center' }}>
                                                    <Text style={[ c.light ]}>{item.status}</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                        <View style={{ width: '100%', borderBottomWidth: 1, borderBottomColor: blue, marginTop: wp('4%'), marginBottom: wp('4%') }} />
                                    </View>
                                )}
                            />
                        )}
                        {this.state.detail_status_friends === 'Permintaan Pertemanan' && (
                            <FlatList
                                ListHeaderComponent={<View style={{ marginTop: wp('4%') }} />}
                                data={this.state.request_friends}
                                keyExtractor={item => item.id}
                                renderItem={({item}) => (
                                    <View style={{ width: '100%', alignItems: 'center' }}>
                                        <View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center' }}>
                                            <TouchableOpacity onPress={() => this.props.navigation.push('ProfTracking')} style={{ width: wp('20%') }}>
                                                <FastImage source={item.image} style={{ width: wp('15%'), height: wp('15%') }} />
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={() => this.props.navigation.push('ProfTracking')} style={{ width: wp('40%') }}>
                                                <Text style={[ c.light, f.bold ]} numberOfLines={1}>{item.nama}</Text>
                                            </TouchableOpacity>
                                            <View style={{ width: wp('30%'), flexDirection: 'row-reverse' }}>
                                                <TouchableOpacity style={{ width: wp('18%'), height: hp('5%'), backgroundColor: light, borderRadius: wp('1%'), alignItems: 'center', justifyContent: 'center', marginLeft: wp('2%') }}>
                                                    <Text style={[ c.blue ]}>Tolak</Text>
                                                </TouchableOpacity>
                                                <TouchableOpacity style={{ width: wp('18%'), height: hp('5%'), backgroundColor: blue, borderRadius: wp('1%'), alignItems: 'center', justifyContent: 'center' }}>
                                                    <Text style={[ c.light ]}>Tambah</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                        <View style={{ width: '100%', borderBottomWidth: 1, borderBottomColor: blue, marginTop: wp('4%'), marginBottom: wp('4%') }} />
                                    </View>
                                )}
                            />
                        )}
                    </ScrollView>
                )}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    // loading centered screenn
    container_loading: {
      flex: 1,
      position:'absolute',
      zIndex:1,
      width: '100%',
      height: '100%',
      justifyContent: "center",
      backgroundColor: 'rgba(0,0,0,0.7)'
    },
    horizontal_loading: {
      flexDirection: "row",
      justifyContent: "space-around",
      padding: 10
    },
  
    actionButtonIcon: {
      fontSize: 20,
      height: 22,
      color: 'white',
    },
});