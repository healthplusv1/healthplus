import React from 'react'
import { Text, View, TouchableOpacity, Image, ImageBackground, ScrollView, Alert, StyleSheet, TextInput, ActivityIndicator, FlatList, StatusBar, ToastAndroid } from 'react-native'
import { b, p, f, c } from '../utils/StyleHelper'
import { vs, xs } from '../utils/Responsive'
import { blue, light, grey, softBlue } from '../utils/Color'
import Svg from '../../home/utils/Svg'
import ImagePicker from 'react-native-image-picker';
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons';
import {NavigationEvents} from 'react-navigation';
import Group from './Group_Chall.js';
import Dialog, { DialogFooter, DialogButton, DialogContent, DialogTitle } from 'react-native-popup-dialog';
import FastImage from 'react-native-fast-image'
import CountDown from 'react-native-countdown-component';
import { API_URL } from 'react-native-dotenv'

const options = {
  title: 'Select Avatar',
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

export default class Grup_Social extends React.Component {

	constructor(props){
		super(props);
		this.state ={
			url_gambar: '',
            uri: '',
            fileName: '',
            nama_group: null,
            lokasi: null,
            deskripsi: null,
            loading: true,
            jml_anggota: '...',
            aktivitas: '...',
            post: '...',
            joined: null,
			list_posting: [],
			visible: false,
			resourcePath: {},
			file_image: '',
			id_group: null,
			list_kompetisi: '',
		}
	}

	async componentDidMount() {

		StatusBar.setHidden(false);

		const id_login = await AsyncStorage.getItem('@id_login')
		axios.get(API_URL+'/main/get_group_detail', {params:{
			id_group: this.props.navigation.getParam('id_group')
		}})
		.then(result => {
			// alert(JSON.stringify(result))
			if (result.data.status === "group_not_found") {
			this.setState({loading: false}) 
				Alert.alert(
				'Grup Tidak Ditemukan',
				'Grup Baru Saja Dihapus',
				[
					{text: 'Kembali', onPress: () => {
						this.props.navigation.navigate('League')
					}}
				]);
			}else{
				this.setState({
					loading: false,
					nama_group: result.data.data.nama_group,
					lokasi: result.data.data.lokasi,
					deskripsi: result.data.data.deskripsi,
					url_gambar: result.data.data.url_gambar,
					jml_anggota: result.data.data.jml_anggota,
					aktivitas: result.data.data.aktivitas,
					post: result.data.data.post,
					data: result.data.data,
					list_kompetisi: result.data.kompetisi,
					kompetisi: result.data.kompetisi.length,
				})

				AsyncStorage.setItem('@current_id_group', result.data.data.id_group)
			}
			// alert(JSON.stringify(result.data.kompetisi.length))
		})
		.catch(e => {
			console.log("get_group_detail => "+e)
		})

		axios.get(API_URL+'/main/check_join', {params:{
			id_group: this.props.navigation.getParam('id_group'),
			id_login: id_login
		}})
		.then(resp => {
			this.setState({
				joined: resp.data.status
			})
			// alert(JSON.stringify(resp.data.status))
		})
		.catch(e => {
			console.log("check_join => "+e)
		})

		axios.get(API_URL+'/main/get_group_post', {params:{
			id_group: this.props.navigation.getParam('id_group'),
			id_login: id_login
		}})
		.then(data => {
			this.setState({
				list_posting: data.data.data
			})
			// alert(JSON.stringify(data.data.data[0].id_post_group))
		})
	}

	async gabungGroup() {
		const id_login = await AsyncStorage.getItem('@id_login');

		axios.get(API_URL+'/main/request_join_anggota_league', {params:{
			id_login: id_login,
			id_league: this.props.navigation.getParam('id_group')
		}})
		.then(response => {
			// alert(JSON.stringify(this.state.status_join))
			this.setState({
				status_join: response.data.status,
			})
			this.componentDidMount()
		})
	}

	async keluarGroup() {
		const id_login = await AsyncStorage.getItem('@id_login')
		const showToast = () => {
            ToastAndroid.show("Anda Telah Keluar", ToastAndroid.SHORT);
        }

		axios.get(API_URL+'/main/delete_anggota_group', {params:{
			id_group: this.props.navigation.getParam('id_group'),
			id_login: id_login
		}})
		.then(response => {
			// alert(JSON.stringify(response))
			if (response.data.status === 'remaining_admin_one') {
				Alert.alert(
					"Gagal",
					"Admin grup hanya 1, jadi Anda tidak bisa keluar",
					[
						{ text: 'OK', onPress: () => this.props.navigation.navigate('Grup') }
					]
				)
			} else if (response.data.status === 'true') {
				// Alert.alert("Berhasil Keluar Grup")
				showToast()
				this.props.navigation.pop()
				axios.get(API_URL+'/main/get_group_latest_anggota', {params: {
					id_group: this.props.navigation.getParam('id_group')
				}})
				.then(resp => {
					// alert(JSON.stringify(resp))
					this.setState({
						jml_anggota: resp.data.data.jml_anggota
					})
				})
				.catch(errror => {
					// alert(JSON.stringify(errror))
				})
			} else {
				Alert.alert("Gagal Keluar Grup")
			}
		})
		.catch(error => {
			console.log('Keluar group => ' + error)
		})
	}

	hitung_hari(from_date) {
		var date1 = new Date(from_date); 
		var date2 = new Date(new Date().getFullYear(),new Date().getMonth() , new Date().getDate()); 
		// console.log(date1);
		// console.log(date2);
		  
		// To calculate the time difference of two dates 
		var Difference_In_Time = date2.getTime() - date1.getTime(); 
		  
		// To calculate the no. of days between two dates 
		var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
		if (typeof Difference_In_Days === 'number') {
			if(Number(Difference_In_Days.toString().replace('-', '')).toFixed() == 0){
				return 'Today';
			}else{
				return Number(Difference_In_Days.toString().replace('-', '')).toFixed()+' Days ago';
			}
		}
	}

	createFormDataPhoto = (photo, id_group) => {
        let data = new FormData();
        data.append('file_image', {
            name: new Date().getTime()+'_'+this.state.id_group+'.jpg',
            type: 'image/jpg',
            uri : photo.uri
        });
        data.append("id_group", id_group);
        return data;
    }

	launchImageLibrary = async () => {
        this.state.id_group = await this.props.navigation.getParam('id_group')

		let options = {
		  storageOptions: {
			skipBackup: true,
			path: 'images',
		  },
		};
		ImagePicker.launchImageLibrary(options, (response) => {
		  console.log('Response = ', response);
	
		  if (response.didCancel) {
			console.log('User cancelled image picker');
		  } else if (response.error) {
			console.log('ImagePicker Error: ', response.error);
		  } else if (response.customButton) {
			console.log('User tapped custom button: ', response.customButton);
			alert(response.customButton);
		  } else {
			console.log(response.fileName);
	        this.setState({
	          uri: {uri: response.uri},
	          fileName: response.fileName,
	          file_image: response
	        });
			fetch(API_URL+'/main/update_profile_group', {
				method: 'POST',
				headers: {
					'Content-Type': 'multipart/form-data'
				},
				body: this.createFormDataPhoto(this.state.file_image, this.state.id_group)
			})
			.then(response => response.json())
			.then(response => {
				// alert(JSON.stringify(response))
				this.componentDidMount()
			})
			.catch(error => {
				console.log('Upload Error', error);
			})
		  }
		});
	
	}

	timer(date1, date2) {
		var t1 = new Date(Date.parse(date1+'T00:00:01'));
		var t2 = new Date(Date.parse(date2+'T23:59:59'));
		// console.log(date1, date2);
		var d = new Date();
		// var now = d.getTime();
		// console.log(t1, t2, d);
		if (t1.getTime() < d.getTime()) {
		  t1 = d;
		}
	
		var dif = t2.getTime() - t1.getTime();
		// console.log(dif);
	
		var seconds_left = dif / 1000;
		// console.log(seconds_left, seconds_left/60, (seconds_left/60)/60, ((seconds_left/60)/60)/24);
	
		return seconds_left;
	}

	renderFileUri() {
	    if (this.state.url_gambar) {
	      return <FastImage
	        source={{ uri: API_URL+'/src/icon_group/'+this.state.url_gambar }}
	        style={[ p.center, { width: xs(80), height: xs(80), borderRadius: xs(50) }]}
	      />
	    } else {
	      return <View style={[ p.center, { width: xs(80), height: xs(80), backgroundColor: '#CFCFCF', borderRadius: xs(50) }]}>
					<Image source={require('../../home/asset/icon_group.png')} style={{ width: xs(80), height: xs(80), borderRadius: xs(40) }} />
				</View>
	    }
	}

	day_left_to_begin(tanggal_mulai) {
		let tanggal_dari = new Date(tanggal_mulai)
		let tanggal_sekarang = new Date()
	
		// To calculate the time difference of two dates 
		var Difference_In_Time = tanggal_sekarang.getTime() - tanggal_dari.getTime(); 
		  
		// To calculate the no. of days between two dates 
		var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
	
		return Difference_In_Days.toFixed().toString().replace(/-/gi, '');
	}

	render() {
		return (
			<>
				{this.state.loading ?
					<View style={[styles.container_loading, styles.horizontal_loading]}>
						<ActivityIndicator size="large" color="#0000ff" />
					</View>
					:
					null
				}
				<NavigationEvents onDidFocus={() => this.componentDidMount()} />
				<Dialog
					dialogTitle={<DialogTitle title="Select a Photo" />}
					visible={this.state.visible}
					onTouchOutside={() => this.setState({ visible: false })}
					footer={
						<DialogFooter style={{ marginTop: vs(-8) }}>
							<DialogButton
								text="Cancel"
								onPress={() => {this.setState({ visible: false })}}
							/>
						</DialogFooter>
					}
				>
					<DialogContent>
						<View style={{ width: xs(250), paddingTop: vs(20) }}>
							<View style={[ p.row, { height: xs(35) }]}>
								<Icon name="ios-camera" size={30} />
								<TouchableOpacity onPress={() => {
									this.setState({ visible: false })
									this.props.navigation.navigate('UploadPhoto', {id_group: this.props.navigation.getParam('id_group')})
								}}
									style={[ b.ml3 ]}
								>
									<Text style={[ f._18 ]}>Take Photo</Text>
								</TouchableOpacity>
							</View>
							<View style={[ p.row, { height: xs(35) }]}>
								<Icon name="md-link" size={30} />
								<TouchableOpacity
									onPress={() => {
										this.setState({ visible: false })
										this.launchImageLibrary()
									}}
									style={[ b.ml3 ]}
								>
									<Text style={[ f._18 ]}>Choose from Library</Text>
								</TouchableOpacity>
							</View>
						</View>
					</DialogContent>
				</Dialog>
				{this.state.joined === 'true' ?
					<ActionButton buttonColor={blue} style={{ zIndex: 1 }} fixNativeFeedbackRadius={true}>
						<ActionButton.Item buttonColor='#9b59b6' title="Add Comment" onPress={() => this.props.navigation.navigate('AddComment')}>
							<Icon name="md-create" size={22} color={light} />
						</ActionButton.Item>
					</ActionButton>
					: null
				}
				<ScrollView showsVerticalScrollIndicator={false}>
					<View style={{ width: '100%', backgroundColor: '#181B22' }}>
						<ImageBackground source={require('../../home/asset/runner_01.png')} style={{ width: '100%', height: xs(180) }}>
							<View style={[ p.row, { width: '100%' }]}>
								<View style={[ b.ml4, b.mt4, { width: xs(200) }]}>
									<TouchableOpacity onPress={() => this.props.navigation.pop()}>
										<Image source={require('../../home/asset/back.png')} style={{ width: xs(25), height: xs(25) }} />
									</TouchableOpacity>
								</View>
								{this.state.joined === 'true' ?
									<View style={[ b.mt4, b.ml4, p.alignEnd, { width: vs(100) }]}>
										<TouchableOpacity onPress={() => this.props.navigation.navigate('AjakTemanLeague', {id_group: this.props.navigation.getParam('id_group')})} style={[ p.center, { backgroundColor: 'rgba(0,0,0,0.4)', width: xs(35), height: xs(35), borderRadius: xs(20) }]}>
											<Icon name="ios-person-add" size={25} color={light} />
										</TouchableOpacity>
									</View>
									:
									null
								}
							</View>
						</ImageBackground>
						<View style={[ p.center, { marginTop: vs(-40), width: '100%' }]}>
							{this.renderFileUri()}
							<TouchableOpacity onPress={() => {
									this.setState({ visible: true })
								}} 
								style={[ p.center, { marginLeft: vs(60), marginTop: vs(-28), width: xs(30), height: xs(30), backgroundColor: '#EBEBEB', borderRadius: xs(20) }]}
							>
								<Svg icon="camera" size={15} />	
							</TouchableOpacity>

							<View style={[ b.mt4, p.center, { width: xs(250) }]}>
								<Text style={[ f._20, p.textCenter, c.light, { fontFamily: 'lineto-circular-pro-bold' }]}> {this.state.nama_group} </Text>
								<Text style={[ b.mt2, c.light ]}> {this.state.lokasi} </Text>
								<View style={[ b.mt3, c.light, { width: xs(200) }]}>
									<Text style={[ p.textCenter, c.light ]}>{this.state.deskripsi}</Text>
								</View>
							</View>

							<View style={[ b.mt4, p.row, { width: '100%', borderBottomWidth: 3, borderBottomColor: '#2F3237' }]}>
								<View style={[ p.center, b.mb1, { width: xs(118) }]}>
									<Text style={[ c.light ]}>{this.state.jml_anggota}</Text>
									<Text style={[ c.light ]}>Anggota</Text>
								</View>
								<View style={{ width: xs(3), backgroundColor: '#2F3237', borderWidth: 1, borderColor: '#2F3237' }} />
								<View style={[ p.center, b.mb1, { width: xs(118) }]}>
									<Text style={[ c.light ]}>{this.state.post}</Text>
									<Text style={[ c.light ]}>Post</Text>
								</View>
								<View style={{ width: xs(3), backgroundColor: '#2F3237', borderWidth: 1, borderColor: '#2F3237' }} />
								<View style={[ p.center, b.mb1, { width: xs(118) }]}>
									<Text style={[ c.light ]}>{this.state.kompetisi}</Text>
									<Text style={[ c.light ]}>Kompetisi</Text>
								</View>
							</View>
							{this.state.joined === 'request' ?
								<View style={[ p.center, b.mt4, b.mb3, b.roundedLow, b.shadow, { width: xs(300), height: xs(35), backgroundColor: '#555555' }]}>
									<Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}> Diminta </Text>
								</View>
								:
								(this.state.joined === 'true' ?
									null
									:
									(this.state.status_join === 'true' ?
										<View style={[ p.center, b.mt4, b.mb3, b.roundedLow, b.shadow, { width: xs(300), height: xs(35), backgroundColor: '#555555' }]}>
											<Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}> Diminta </Text>
										</View>
										:
										<TouchableOpacity activeOpacity={.5} onPress={() => this.gabungGroup()} style={[ p.center, b.mt4, b.mb3, b.roundedLow, b.shadow, { width: xs(300), height: xs(35), backgroundColor: blue }]}>
											<Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}> Gabung </Text>
										</TouchableOpacity>
									)
								)
								
							}
							<View style={[ b.mt1, p.center, { width: '100%', backgroundColor: '#000000' }]}>
								<View style={[ b.mt4, { width: '100%' }]}>
									<Text style={[ b.ml4, b.pt1, b.pb2, c.light, { fontFamily: 'lineto-circular-pro-bold' }]}> Kompetisi </Text>
								</View>
							</View>
							{this.state.list_kompetisi.length === 0 ?
								<View style={[ p.center, { width: '100%' }]}>
									<View style={[ b.mt4, b.mb1, p.center ]}>
										<Svg icon="dua_bendera" size={70} />
										<Text style={[ c.grey, p.textCenter ]}>League ini tidak mempunyai kompetisi aktif atau kompetisi yang akan datang</Text>
									</View>
								</View>
								:
								<>
									<View style={[ b.mt1, p.center, { width: '100%' }]}>
										<View style={[ b.mt2, { width: '100%' }]}>
											<Text style={[ b.ml4, c.light, { fontFamily: 'lineto-circular-pro-bold' }]}> Kompetisi Saya </Text>
										</View>
									</View>
									<View style={{ width: '100%' }}>
										<View style={[ b.mt2, b.ml4, b.pr2, b.rounded, { backgroundColor: '#000000' }]}>
											<ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
												<FlatList
													extraData={this.state}
													horizontal={true}
													data={this.state.list_kompetisi}
													keyExtractor={item => item.id_kompetisi}
													renderItem={({item}) => (
														<TouchableOpacity onPress={() => this.props.navigation.push('Kompetisi_Social', {id_kompetisi: item.id_kompetisi, id_group: item.id_group})} style={[ b.rounded, b.shadowHigh, b.ml1, b.mt1, b.mb1, { width: xs(200), backgroundColor: '#2F3237' }]}>
															<View style={[ b.ml2, b.mt4 ]}>
																<View style={{ width: xs(60), height: xs(60) }}>
																	<FastImage source={{ uri: API_URL+'/src/icon_group/'+item.icon_kompetisi }} style={{ width: '100%', height: '100%' }} />
																	<View style={{ 
																		width: xs(0), height: xs(0), borderLeftWidth: xs(0), borderRightWidth: xs(15), borderBottomWidth: xs(15), borderStyle: 'solid', borderLeftColor: 'transparent', borderRightColor: 'transparent', borderBottomColor: '#2F3237',
																		position: "absolute", top: vs(0), left: vs(0), transform: [{rotate: '90deg'}]
																	}} />
																	<View style={{ 
																		width: xs(0), height: xs(0), borderLeftWidth: xs(15), borderRightWidth: xs(0), borderBottomWidth: xs(15), borderStyle: 'solid', borderLeftColor: 'transparent', borderRightColor: 'transparent', borderBottomColor: '#2F3237',
																		position: "absolute", top: vs(45), left: vs(0), transform: [{rotate: '90deg'}]
																	}} />
																	<View style={{ 
																		width: xs(0), height: xs(0), borderLeftWidth: xs(15), borderRightWidth: xs(0), borderBottomWidth: xs(15), borderStyle: 'solid', borderLeftColor: 'transparent', borderRightColor: 'transparent', borderBottomColor: '#2F3237',
																		position: "absolute", top: vs(0), left: vs(45), transform: [{rotate: '270deg'}]
																	}} />
																	<View style={{ 
																		width: xs(0), height: xs(0), borderLeftWidth: xs(15), borderRightWidth: xs(0), borderBottomWidth: xs(15), borderStyle: 'solid', borderLeftColor: 'transparent', borderRightColor: 'transparent', borderBottomColor: '#2F3237',
																		position: "absolute", top: vs(45), left: vs(45)
																	}} />
																</View>
															</View>
															<View style={[ b.ml2, b.mt1, b.mb3, b.mr2 ]}>
																<Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>{item.nama_kompetisi}</Text>
																<View style={[ p.row, b.mt1 ]}>
																	<View>
																		<Svg icon="piala" size={20} />
																	</View>
																	<View style={[ b.ml2 ]}>
																		<Text style={[ c.light ]}>{item.jenis_challenge}</Text>
																	</View>
																</View>
																<View style={[ p.row, b.mt2 ]}>
																	<View>
																		<Svg icon="poin" size={20} />
																	</View>
																	<View style={[ b.ml2 ]}>
																		<Text style={[ c.light ]}>{item.hadiah} Poin</Text>
																	</View>
																</View>
																{new Date().getTime() >= new Date(item.tgl_dimulai).getTime() ?
																	<View style={[ p.row, b.mt2, b.mb1, b.roundedLow, { borderWidth: 1, borderColor: blue }]}>
																		<View style={{ width: xs(80) }}>
																			<Text style={[ c.light, b.ml1 ]}>Sisa Waktu</Text>
																		</View>
																		<View style={[ b.roundedLow, b.ml1, p.row, { backgroundColor: blue }]}>
																			<CountDown
																				size={8}
																				until={this.timer(item.tgl_dimulai, item.tgl_berakhir)}
																				digitStyle={{backgroundColor: blue}}
																				digitTxtStyle={{color: light, fontSize: 15}}
																				timeToShow={['D']}
																				timeLabels={{ d: null }}
																			/>
																			<Text style={[ c.light, { marginTop: vs(1) }]}>h</Text>
																			<CountDown
																				size={8}
																				until={this.timer(item.tgl_dimulai, item.tgl_berakhir)}
																				digitStyle={{backgroundColor: blue}}
																				digitTxtStyle={{color: light, fontSize: 15}}
																				timeToShow={['H']}
																				timeLabels={{ h: null }}
																			/>
																			<Text style={[ c.light, { marginTop: vs(1) }]}>j</Text>
																			<CountDown
																				size={8}
																				until={this.timer(item.tgl_dimulai, item.tgl_berakhir)}
																				digitStyle={{backgroundColor: blue}}
																				digitTxtStyle={{color: light, fontSize: 15}}
																				timeToShow={['M']}
																				timeLabels={{ m: null }}
																			/>
																			<Text style={[ c.light, { marginTop: vs(1), marginRight: vs(2.5) }]}>m</Text>
																		</View>
																	</View>
																	:
																	<View style={[ p.row, b.mt2, b.mb1, b.roundedLow, { borderWidth: 1, borderColor: '#555555', width: xs(167) }]}>
																		<View style={{ width: xs(80) }}>
																			<Text style={[ c.light, b.ml1 ]}>Dimulai</Text>
																		</View>
																		<View style={[ b.roundedLow, b.ml1, { backgroundColor: '#555555' }]}>
																			<Text style={[ c.light, b.ml2, b.mr2 ]}>{this.day_left_to_begin(item.tgl_dimulai)} hari lagi</Text>
																		</View>
																	</View>
																}
															</View>
														</TouchableOpacity>
													)}
												/>
											</ScrollView>
										</View>
									</View>
								</>
							}
							<View style={{ width: '100%' }}>
								<View style={[ b.mt2, { width: '100%', borderWidth: 1, borderColor: '#2F3237' }]} />

								<TouchableOpacity onPress={() => this.props.navigation.push('RiwayatKompetisi_Social', {id_group: this.props.navigation.getParam('id_group')})}>
									<View style={[ p.row, b.mt2, b.mb2 ]}>
										<Text style={[ b.ml4, c.light, { width: xs(200) }]}> Riwayat Kompetisi </Text>
										<View style={[ b.ml1, p.alignEnd, { width: xs(115) }]}>
											<Icon name="ios-arrow-forward" size={20} color={light} />
										</View>
									</View>
								</TouchableOpacity>
							</View>
							<View style={[ b.mt1, p.center, { width: '100%', backgroundColor: '#000000' }]}>
								<View style={[ b.mt4, { width: '100%' }]}>
									<Text style={[ b.ml4, b.pt1, b.pb2, c.light, { fontFamily: 'lineto-circular-pro-bold' }]}> Postingan </Text>
								</View>
							</View>
							<View style={{ width: '100%' }}>
								{this.state.list_posting === undefined ?
									<View style={[ p.center, { width: '100%' }]}>
										<View style={[ p.center, b.mt4, b.mb4 ]}>
											<Svg icon="postingan" size={50} />
											<Text style={[ b.mt1, b.mb1, c.light ]}>Postingan League</Text>
											<Text style={[ c.light, p.textCenter, { width: xs(300) }]}>Sepertinya belum ada yang memposting dalam League ini</Text>
										</View>
									</View>
									:
									<>
										<FlatList
											extraData={this.state}
											data={this.state.list_posting}
											keyExtractor={item => item.id_group}
											renderItem={({item}) => (
												<Group
													url_avatar={item.url_gambar}
													nama={item.nama}
													tanggal_post={this.hitung_hari(item.tanggal_post)}
													deskripsi={item.deskripsi}
													post_gambar={item.post_gambar}
													id={item.id_post_group}
													komentar={item.komentar}
													like_id_login={item.like_id_login}
													action={() => this.props.navigation.push('ProfTracking', {id_login: item.id_login})}
													comment={() => this.props.navigation.push('LihatKome', {id_post_group: item.id_post_group, id_login: item.id_login})}
												/>
											)}
										/>

										<TouchableOpacity onPress={() => this.props.navigation.navigate('LihatSemuaPostingan')}>
											<View style={[ p.row, b.mt2, b.mb2 ]}>
												<Text style={[ b.ml4, c.light, { width: xs(200) }]}> Lihat semua Postingan </Text>
												<View style={[ b.ml1, p.alignEnd, { width: xs(115) }]}>
													<Icon name="ios-arrow-forward" size={20} color={light} />
												</View>
											</View>
										</TouchableOpacity>
									</>
								}
							</View>
							{this.state.joined === 'true' ?
								<View style={[ p.center, { width: '100%', backgroundColor: '#000000' }]}>
									<TouchableOpacity activeOpacity={.5} onPress={() => this.keluarGroup()} style={[ p.center, b.mt4, b.roundedLow, { width: xs(300), borderWidth: 1, borderColor: '#2F3237' }]}>
										<Text style={[ c.blue, b.mt1, b.mb1 ]}> Keluar League </Text>
									</TouchableOpacity>
									<View style={{ height: xs(20) }} />
								</View>
								:
								null
							}
						</View>
					</View>
				</ScrollView>
			</>
		)
	}
}

const styles = StyleSheet.create({
  // loading centered screenn
  container_loading: {
    flex: 1,
    position:'absolute',
    zIndex:1,
    width: '100%',
    height: '100%',
    justifyContent: "center",
    backgroundColor: 'rgba(0,0,0,0.7)'
  },
  horizontal_loading: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },

  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
});