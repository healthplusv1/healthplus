import { createStackNavigator} from 'react-navigation-stack'
import SocialPage from './screen/Social'
import Grup_SocialPage from './screen/Grup_Social'
import Kompetisi_SocialPage from './screen/Kompetisi_Social'
import RiwayatKompetisi_SocialPage from './screen/RiwayatKompetisi_Social'
import LeaguePage from '../league/screen/League'
import { vs, xs } from '../social/utils/Responsive';

export const Socialstack = createStackNavigator({
    Social: {
        screen: SocialPage,
        navigationOptions:{
            header: null,
        }
    },

    Grup_Social: {
        screen: Grup_SocialPage,
        navigationOptions:{
            header: null,
        }
    },

    Kompetisi_Social: {
        screen: Kompetisi_SocialPage,
        navigationOptions:{
            header: null,
        }
    },

    RiwayatKompetisi_Social: {
        screen: RiwayatKompetisi_SocialPage,
        navigationOptions:{
            header: null,
        }
    },

    League: {
        screen: LeaguePage,
        navigationOptions:{
            header: null
        }
    }
});