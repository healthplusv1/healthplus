import { createStackNavigator} from 'react-navigation-stack'
import ChallengesPage from './screen/Challenges'
import Kompetisi_ChallPage from './screen/Kompetisi_Chall'
import Grup_ChallePage from './screen/Grup_Challe'
import RiwayatKompetisi_ChallPage from './screen/RiwayatKompetisi_Chall'
import PostChallengePage from './screen/PostChallenge'
import KomentarPostChallengePage from './screen/KomentarPostChallenge'

export const Challengesstack = createStackNavigator({
    Challenges: {
        screen: ChallengesPage,
        navigationOptions:{
            header: null,
        }
    },

    Kompetisi_Chall: {
        screen: Kompetisi_ChallPage,
        navigationOptions:{
            header: null,
        }
    },

    Grup_Challe: {
        screen: Grup_ChallePage,
        navigationOptions:{
            header: null,
        }
    },

    RiwayatKompetisi_Chall: {
        screen: RiwayatKompetisi_ChallPage,
        navigationOptions:{
            header: null,
        }
    },

    PostChallenge: {
        screen: PostChallengePage,
        navigationOptions:{
            header: null,
        }
    },

    KomentarPostChallenge: {
        screen: KomentarPostChallengePage,
        navigationOptions:{
            header: null,
        }
    }
});