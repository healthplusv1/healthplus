import React, { memo } from 'react';
import { ms } from '../utils/Responsive';
import Piala from '../asset/icons/piala.svg'
import Mengikuti from '../asset/icons/mengikuti.svg'
import Poin from '../asset/icons/poin.svg'
import Tanggal from '../asset/icons/tanggal.svg'
import Bendera from '../asset/icons/dua_bendera.svg'
import TanggalBerakhir from '../asset/icons/tanggal_selesai.svg'


const Svg = ({ icon, size = 30, fill, opacity, ...props }) => {
  const icons = {
    piala: Piala,
    mengikuti: Mengikuti,
    poin: Poin,
    tanggal: Tanggal,
    bendera: Bendera,
    tanggal_selesai: TanggalBerakhir,
  };

  if (icons[icon] === undefined)
    return (
      <></>
    );

  const Icon = icons[icon]
  return (
    <Icon
      width={ms(size)}
      fill={fill || null}
      height={ms(size)}
      fillOpacity={opacity || 1}
      {...props}
    />
  );
}
export default memo(Svg)
