import React from 'react'
import { Text, View, TouchableOpacity, Image, ImageBackground, ScrollView, Alert, ActivityIndicator, StatusBar, FlatList, StyleSheet } from 'react-native'
import { b, p, f, c } from '../utils/StyleHelper'
import { vs, xs } from '../utils/Responsive'
import { blue, light, grey, softBlue } from '../utils/Color'
import Svg from '../../home/utils/Svg'
import {NavigationEvents} from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios';
import CountDown from 'react-native-countdown-component';
import Icon from 'react-native-vector-icons/Ionicons'
import Dialog, { DialogFooter, DialogButton, DialogContent, DialogTitle } from 'react-native-popup-dialog';
import ImagePicker from 'react-native-image-picker';
import FastImage from 'react-native-fast-image'
import LinearGradient from 'react-native-linear-gradient'
import analytics from '@react-native-firebase/analytics'
import { API_URL } from 'react-native-dotenv'

export default class Kompetisi_Chall extends React.Component {

	constructor(props){
		super(props);
		this.state = {
			icon_kompetisi: '',
			gambar_cover: '',
			nama_kompetisi: null,
			deskripsi: null,
			jenis_challenge: null,
			tgl_dimulai: 0,
			tgl_berakhir: 0,
			poin: null,
			peserta: '...',
			nama_group: null,
			jml_anggota: '...',
			url_gambar: '',
			loading: false,
			joined: null,
			already_upload: '',
			joined_kompetisi: null,
			visible: false,
			id_group: null,
			id_login: null,
			file_video: null,
			fileName: '',
			seleksi: [],
			list_hari: '',
			clicked: 0,
		}
	}

	async componentDidMount() {
		await analytics().logEvent('created_challenge', {
			item: 'challenge_id'
		})
		await analytics().setUserProperty('user_sees_detail_challenge', 'null')
        StatusBar.setHidden(false);

		const id_group = await AsyncStorage.getItem('@current_id_group')
		const id_login = await AsyncStorage.getItem('@id_login')
		this.setState({
			id_group: id_group,
			id_login: id_login,
			id_kompetisi: this.props.navigation.getParam('id_kompetisi')
		})
		
		axios.get(API_URL+'/main/get_detail_challenge', {params: {
			id_kompetisi: this.props.navigation.getParam('id_kompetisi'),
			id_login: id_login,
		}})
		.then(res => {
			// alert(JSON.stringify(res.data.data.id_group))
			
			res.data.list_hari.map((d, index) => {
				console.log(new Date(d.date).getDate()+' '+new Date(d.date).getMonth()+' '+new Date(d.date).getFullYear()+' ||| '+new Date().getDate()+' '+new Date().getMonth()+' '+new Date().getFullYear())
				if (new Date(d.date).getDate()+' '+new Date(d.date).getMonth()+' '+new Date(d.date).getFullYear() === new Date().getDate()+' '+new Date().getMonth()+' '+new Date().getFullYear()){
					this.setState({
						clicked: index,
						tanggal_diklik: d.date
					})
				}
			})

			this.setState({
				icon_kompetisi: res.data.data.icon_kompetisi,
				gambar_cover: res.data.data.gambar_cover,
				nama_kompetisi: res.data.data.nama_kompetisi,
				deskripsi: res.data.data.deskripsi_kompetisi,
				jenis_challenge: res.data.data.jenis_challenge,
				tgl_dimulai: res.data.data.tgl_dimulai,
				tgl_berakhir: res.data.data.tgl_berakhir,
				poin: res.data.data.poin,
				peserta: res.data.data.jumlah_peserta_kompetisi,
				nama_group: res.data.data.nama_group,
				jml_anggota: res.data.data.jml_anggota,
				url_gambar: res.data.data.url_gambar,
				list_hari: res.data.list_hari,
				status: res.data.sudah_claim,
				id_group_visit: res.data.data.id_group,
				loading: false
			})
		})

		axios.get(API_URL+'/main/check_join', {params:{
			id_group: id_group,
			id_login: id_login
		}})
		.then(resp => {
			this.setState({joined: resp.data.status})
			// alert(JSON.stringify(resp))
		})
		.catch(e => {
			this.setState({ loading: false })
			console.log("check_join => "+e)
		})

		axios.get(API_URL+'/main/check_join_kompetisi', {params:{
			id_kompetisi: this.props.navigation.getParam('id_kompetisi'),
			id_login: id_login
		}})
		.then(resp => {
			this.setState({joined_kompetisi: resp.data.status})
			// alert(JSON.stringify(resp))
		})
		.catch(e => {
			this.setState({ loading: false })
			console.log("check_join_kompetisi => "+e)
		})

		axios.get(API_URL+'/main/seleksi', {params:{
			id_kompetisi: this.props.navigation.getParam('id_kompetisi')
		}})
		.then(resp => {
			// alert(JSON.stringify(resp.data.data[0].durasi))
			this.setState({
				seleksi: resp.data.data,
			})
		})
		.catch(e => {
			this.setState({ loading: false })
			console.log("check_seleksi => "+e)
		})

		axios.get(API_URL+'/main/juara_jumlah', {params:{
			id_kompetisi: this.props.navigation.getParam('id_kompetisi')
		}})
		.then(resp => {
			// alert(JSON.stringify(resp))
			this.setState({
				seleksi_juara: resp.data.data,
			})
		})
		.catch(e => {
			this.setState({ loading: false })
			console.log("check_jumlah_juara => "+e)
		})
		
		axios.get(API_URL+'/main/juara_jumlah_plank', {params:{
			id_kompetisi: this.props.navigation.getParam('id_kompetisi')
		}})
		.then(resp => {
			// alert(JSON.stringify(resp))
			this.setState({
				seleksi_juara_plank: resp.data.data,
			})
		})
		.catch(e => {
			this.setState({ loading: false })
			console.log("check_jumlah_juara => "+e)
		})

		axios.get(API_URL+'/main/check_if_already_upload', {params:{
			id_login: id_login,
			id_kompetisi: this.props.navigation.getParam('id_kompetisi')
		}})
		.then(resp => {
			this.setState({already_upload: resp.data.status})
			// alert(JSON.stringify(resp))
		})
		.catch(e => {
			this.setState({ loading: false })
			console.log("check_if_already_upload => "+e)
		})

		axios.get(API_URL+'/main/check_if_already_ended', {params:{
			id_kompetisi: this.props.navigation.getParam('id_kompetisi')
		}})
		.then(resp => {
			if (resp.data.status === 'true') {
				console.log("Kompetisi ini sudah berakhir")
			} else {
				console.log("Kompetisi ini belum berakhir")
			}
		})
		.catch(err => {
			this.setState({ loading: false })
			console.log("check_if_already_ended => "+err)
		})
	}

	async claim_hadiah() {
		await analytics().logEvent('redeem_point', {
			item: 'redeem_point'
		})
		await analytics().setUserProperty('user_redeemed_point', 'null')
		
		const id_login = await AsyncStorage.getItem('@id_login')

		axios.get(API_URL+'/main/claim_poin_untuk_pemenang_jumlah', {params:{
			id_kompetisi: this.props.navigation.getParam('id_kompetisi'),
			id_login: id_login
		}})
		.then(resp => {
			console.log(resp.data)
			alert(JSON.stringify(resp.data))
			if(resp.data.data === 'bukan_pemenang'){
				Alert.alert("Tetap Semangat!", "Poin Anda telah dikembalikan setengah")
			} else if(resp.data.data === 'berhasil_claim'){
				Alert.alert("", "Selamat! Anda berhak mendapatkan "+ resp.data.poin +" Poin. Karena menduduki posisi ke-"+ resp.data.juara +" di Kompetisi ini")
			}
			this.setState({
				status: resp.data.status
			})
			this.componentDidMount()
		})
		.catch(err => {
			console.log("claim_poin_untuk_pemenang => "+err)
		})
	}

	async claim_hadiah_plank() {
		const id_login = await AsyncStorage.getItem('@id_login')

		axios.get(API_URL+'/main/claim_poin_untuk_pemenang_durasi', {params:{
			id_kompetisi: this.props.navigation.getParam('id_kompetisi'),
			id_login: id_login
		}})
		.then(resp => {
			// alert(JSON.stringify(resp))
			if(resp.data.data === 'bukan_pemenang'){
				Alert.alert("Tetap Semangat!", "Poin Anda telah dikembaikan setengah")
			} else if(resp.data.data === 'berhasil_claim'){
				Alert.alert("", "Selamat! Anda berhak mendapatkan "+ resp.data.poin +" Poin. Karena menduduki posisi ke-"+ resp.data.juara +" di Kompetisi ini")
			}
			this.setState({
				status: resp.data.status
			})
			this.componentDidMount()
		})
		.catch(err => {
			console.log("claim_poin_untuk_pemenang_plank => "+err)
		})
	}

	async ikutiKompetisi() {
		const id_login = await AsyncStorage.getItem('@id_login');

		axios.get(API_URL+'/main/request_join_peserta_challenge', {params:{
			id_kompetisi: this.props.navigation.getParam('id_kompetisi'),
			id_login: id_login
		}})
		.then(resp => {
			// alert(JSON.stringify(resp))
			if (resp.data.status === 'true') {
				this.componentDidMount()
			} else if (resp.data.status === 'not_enough_point') {
				Alert.alert("Poin Anda Tidak Cukup")
			} else {
				Alert.alert("Anda Gagal Mengikuti Kompetisi")
			}
		})
		.catch(err => {
			console.log("join_peserta_kompetisi => "+err)
		})
	}

	async gabungGroup() {
		const id_login = await AsyncStorage.getItem('@id_login');
		const id_group = await AsyncStorage.getItem('@current_id_group');

		axios.get(API_URL+'/main/request_join_anggota_league', {params:{
			id_login: id_login,
			id_group: id_group
		}})
		.then(response => {
			if(response.data.status === 'true') {
				Alert.alert('', 'Permintaan untuk Bergabung ke League Terkirim')
				
				axios.get(API_URL+'/main/plus_point', {params:{
					id_login: id_login,
					point: 2,
				}})
				.then(() => {
					console.info('success added 2 point')
					// Alert.alert('',JSON.stringify(result.data))
				})
			}
			this.setState({
				status_join: response.data.status,
			})
			this.componentDidMount()
		})
		.catch(e => {
			console.log(JSON.stringify(e))
		})
	}

	day_left_to_begin(tanggal_mulai) {
		let tanggal_dari = new Date(tanggal_mulai)
		let tanggal_sekarang = new Date()
	
		// To calculate the time difference of two dates 
		var Difference_In_Time = tanggal_sekarang.getTime() - tanggal_dari.getTime(); 
		  
		// To calculate the no. of days between two dates 
		var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
	
		return Difference_In_Days.toFixed().toString().replace(/-/gi, '');
	}

	day_left_to_finish(tanggal_berakhirr) {
		let tanggal_dari = new Date()
		let tanggal_berakhir = new Date(tanggal_berakhirr)
	
		// To calculate the time difference of two dates 
		var Difference_In_Time = tanggal_berakhir.getTime() - tanggal_dari.getTime(); 
		  
		// To calculate the no. of days between two dates 
		var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
	
		return Difference_In_Days.toFixed().toString().replace(/-/gi, '');
	}

	timer_berakhir(date1, date2) {
		var t1 = new Date(Date.parse(date1+'T00:00:01'));
		var t2 = new Date(Date.parse(date2+'T23:59:59'));
		// console.log(date1, date2);
		var d = new Date();
		// var now = d.getTime();
		// console.log(t1, t2, d);
		if (t1.getTime() < d.getTime()) {
		  t1 = d;
		}
	
		var dif = t2.getTime() - t1.getTime();
		// console.log(dif);
	
		var seconds_left = dif / 1000;
		// console.log(seconds_left, seconds_left/60, (seconds_left/60)/60, ((seconds_left/60)/60)/24);
	
		return seconds_left;
	}

	timer_dimulai(date1, date2) {
		var t1 = new Date(Date.parse(date1+'T00:00:01'));
		var t2 = new Date(Date.parse(date2+'T23:59:59'));
		// console.log(date1, date2);
		var d = new Date();
		// var now = d.getTime();
		// console.log(d);
		if (t2.getTime() > d.getTime()) {
		  t2 > d;
		}
	
		var dif = t2.getTime() - d.getTime();
		// console.log(dif);
	
		var seconds_left = dif / 1000;
		// console.log(seconds_left, seconds_left/60, (seconds_left/60)/60, ((seconds_left/60)/60)/24);
	
		return seconds_left;
	}

	createFormDataPhoto = (video, id_login) => {
		let formData = new FormData();
		formData.append("file_video", {
			name: new Date().getTime()+'_'+id_login+'.mp4',
			uri: video.uri,
			type: 'video/mp4'
		});
		formData.append("id_login", id_login);
		formData.append("id_kompetisi", this.props.navigation.getParam('id_kompetisi'));
        return formData;
	}

	videoLibrary = async () => {
		this.state.id_login = await AsyncStorage.getItem('@id_login');
		
		let options = {
			title: 'Video Picker', 
			mediaType: 'video',
			storageOptions: {
				skipBackup: true,
				path: 'images',
			},
		};
		ImagePicker.launchImageLibrary(options, (response) => {
		  console.log('Response = ', response);
	
		  if (response.didCancel) {
				console.log('User cancelled image picker');
		  } else if (response.error) {
				console.log('ImagePicker Error: ', response.error);
		  } else if (response.customButton) {
				console.log('User tapped custom button: ', response.customButton);
				alert(response.customButton);
		  } else {
				console.log(response.fileName);
				this.setState({
				  uri: {uri: response.uri},
				  fileName: response.fileName,
				  file_video: response
				});

				fetch(API_URL+'/main/upload_video', {
					method: 'post',
					headers: {
						'Content-Type': 'multipart/form-data',
					},
					body: this.createFormDataPhoto(this.state.file_video, this.state.id_login)
				})
				.then(response => response.json())
				.then(response => {
					console.log(response)
					Alert.alert('', 'Berhasil Terkirim')
				})
				.catch(error => {
					console.log('Upload Error', error);
				})
		  }
		});
	
	}

	async click(index, date, status){
		this.setState({tanggal_diklik: date})
		

		if(status === 'behind'){
			axios.get(API_URL+'/main/seleksi', {params:{
			id_kompetisi: this.props.navigation.getParam('id_kompetisi'),
			tanggal: date
			}})
			.then(resp => {
				this.setState({
					already_upload: 'true',
					clicked: index,
					seleksi: resp.data.data,
				})
			})
			.catch(e => {
				console.log("check_seleksi => "+e)
			})
		} else {
			const id_login = await AsyncStorage.getItem('@id_login')

			axios.get(API_URL+'/main/seleksi', {params:{
			id_kompetisi: this.props.navigation.getParam('id_kompetisi'),
			tanggal: date
			}})
			.then(resp => {
				this.setState({
					clicked: index,
					seleksi: resp.data.data,
				})
			})
			.catch(e => {
				console.log("check_seleksi => "+e)
			})

			axios.get(API_URL+'/main/check_if_already_upload', {params:{
				id_login: id_login,
				id_kompetisi: this.props.navigation.getParam('id_kompetisi'),
				tanggal: date
			}})
			.then(resp => {
				this.setState({already_upload: resp.data.status})
				// alert(JSON.stringify(resp))
			})
			.catch(e => {
				console.log("check_if_already_upload => "+e)
			})
		}
	}

	render() {
		return (
			<>
				{this.state.loading ?
					<View style={[styles.container_loading, styles.horizontal_loading]}>
						<ActivityIndicator size="large" color="#0000ff"/>
					</View>
					:
					null
				}
				<NavigationEvents onDidFocus={() => this.componentDidMount()} />
				<Dialog
					dialogTitle={<DialogTitle title="Select a Video" />}
					visible={this.state.visible}
					onTouchOutside={() => this.setState({ visible: false })}
					footer={
						<DialogFooter style={{ marginTop: vs(-8) }}>
							<DialogButton
								text="Cancel"
								onPress={() => {this.setState({ visible: false })}}
							/>
						</DialogFooter>
					}
				>
					<DialogContent>
						<View style={{ width: xs(250), paddingTop: vs(20) }}>
							<View style={[ p.row, { height: xs(35) }]}>
								<Icon name="ios-videocam" size={30} />
								<TouchableOpacity onPress={() => {
									this.setState({ visible: false })
									this.props.navigation.navigate('UploadVideo', {id_kompetisi: this.state.id_kompetisi, jenis_kompetisi: this.state.jenis_challenge})
								}}
									style={[ b.ml3 ]}
								>
									<Text style={[ f._18 ]}>Take Video</Text>
								</TouchableOpacity>
							</View>
							<View style={[ p.row, { height: xs(35) }]}>
								<Icon name="md-link" size={30} />
								<TouchableOpacity
									onPress={() => {
										this.setState({ visible: false })
										this.videoLibrary()
									}}
									style={[ b.ml3 ]}
								>
									<Text style={[ f._18 ]}>Choose from Library</Text>
								</TouchableOpacity>
							</View>
						</View>
					</DialogContent>
				</Dialog>
				<ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#181B22' }}>
					<FastImage source={require('../asset/runner_01.png')} style={{ width: '100%', height: xs(180) }}>
						<View style={[ p.row, { width: '100%' }]}>
							<View style={[ b.ml4, b.mt4, { width: xs(200) }]}>
								<TouchableOpacity onPress={() => this.props.navigation.pop()}>
									<FastImage source={require('../../home/asset/back.png')} style={{ width: xs(25), height: xs(25) }} />
								</TouchableOpacity>
							</View>
							{this.state.joined_kompetisi === 'true' ?
								<View style={[ b.mt4, b.ml4, p.alignEnd, { width: xs(100) }]}>
									<TouchableOpacity onPress={() => this.props.navigation.navigate('AjakTemanChall', {id_kompetisi: this.props.navigation.getParam('id_kompetisi')})} style={[ p.center, { backgroundColor: 'rgba(0,0,0,0.4)', width: xs(35), height: xs(35), borderRadius: xs(20) }]}>
										<Icon name="ios-person-add" size={25} color={light} />
									</TouchableOpacity>
								</View>
								:
								null
							}
						</View>
					</FastImage>
					<View style={[ p.center, { marginTop: vs(-28), width: '100%' }]}>
						<FastImage source={require('../asset/icon_group.png')} style={[ b.rounded, { width: xs(60), height: xs(60) }]} />
					</View>
					<View style={[ p.center, { width: '100%' }]}>
						<View style={[ b.mt2, { width: xs(250) }]}>
							<Text style={[ f._18, p.textCenter, c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>Lari Club Semarang</Text>
						</View>
						<View style={[ b.mt1, { width: xs(250) }]}>
							<Text style={[ p.textCenter, c.light ]}>Ayo kita berlomba untuk bisa memenangkan kompetisi ini</Text>
						</View>
						{this.state.joined_kompetisi === 'true' ?
							(this.state.jenis_challenge === 'Push Up' || this.state.jenis_challenge === 'Sit Up' || this.state.jenis_challenge === 'Back Up' || this.state.jenis_challenge === 'Pull Up' || this.state.jenis_challenge === 'Plank' || this.state.jenis_challenge === 'Mountain Climbers' || this.state.jenis_challenge === 'Jumping Jack' || this.state.jenis_challenge === 'Squats' || this.state.jenis_challenge === 'Squat Jump' ?
							<View style={[ p.row, b.roundedLow, b.mt2, { borderWidth: 1, borderColor: blue }]}>
								{new Date().getTime() >= new Date(this.state.tgl_dimulai).getTime() ?
									<View style={[ p.row ]}>
										<View style={[ p.center, b.mt1, b.mb2, { width: xs(150) }]}>
											<Text style={[ c.light ]}>Sisa waktu challenge :</Text>
											<CountDown
												size={10}
												until={this.timer_berakhir(this.state.tgl_dimulai, this.state.tgl_berakhir)}
												style={{ marginTop: vs(5), marginLeft: vs(3) }}
												digitStyle={{backgroundColor: blue}}
												digitTxtStyle={{color: '#FFFFFF', fontSize: 15}}
												timeLabelStyle={{color: 'red', fontWeight: 'bold'}}
												separatorStyle={{color: 'black'}}
												timeToShow={['D', 'H', 'M', 'S']}
												timeLabels={{m: null, s: null}}
												showSeparator
											/>
										</View>
										{new Date().getTime() <= new Date(this.state.tgl_berakhir).getTime() ?
											<View style={[ p.center, b.ml2, { backgroundColor: blue, width: xs(70) }]}>
												<Text style={[ c.light ]}>Klaim</Text>
											</View>
											:
											(this.state.jenis_challenge === 'Plank' ?
												(this.state.status === 'true' ?
													<View style={[ p.center, b.ml2, { backgroundColor: blue, width: xs(70) }]}>
														<Text style={[ c.light, p.textCenter ]}>Sudah diklaim</Text>
													</View>
													:
													<TouchableOpacity onPress={() => this.claim_hadiah_plank()} style={[ p.center, b.ml2, { backgroundColor: blue, width: xs(70) }]}>
														<Text style={[ c.light ]}>Klaim</Text>
													</TouchableOpacity>
												)
												:
												(this.state.status === 'true' ?
													<View style={[ p.center, b.ml2, { backgroundColor: blue, width: xs(70) }]}>
														<Text style={[ c.light, p.textCenter ]}>Sudah diklaim</Text>
													</View>
													:
													<TouchableOpacity onPress={() => this.claim_hadiah()} style={[ p.center, b.ml2, { backgroundColor: blue, width: xs(70) }]}>
														<Text style={[ c.light ]}>Klaim</Text>
													</TouchableOpacity>
												)
											)
										}
									</View>
									:
									<View style={[ b.roundedLow, p.center, { backgroundColor: 'black', width: xs(300), height: xs(30) }]}>
										<CountDown
											size={10}
											until={this.timer_dimulai(this.state.tgl_berakhir, this.state.tgl_dimulai)}
											digitStyle={{backgroundColor: 'black'}}
											digitTxtStyle={{color: '#FFFFFF', fontSize: 15}}
											timeLabelStyle={{color: 'red', fontWeight: 'bold'}}
											separatorStyle={{color: 'white'}}
											timeToShow={['D', 'H', 'M', 'S']}
											timeLabels={{m: null, s: null}}
											showSeparator
										/>
									</View>
								}
								</View>
								:
								<View style={[ p.row, b.roundedLow, b.mt2, { borderWidth: 1, borderColor: blue }]}>
								{new Date().getTime() >= new Date(this.state.tgl_dimulai).getTime() ?
									<View style={[ p.row ]}>
										<View style={[ p.center, b.mt1, b.mb1, { width: xs(150) }]}>
											<Text>Sisa waktu challenge :</Text>
											<CountDown
											size={10}
											until={this.timer_berakhir(this.state.tgl_dimulai, this.state.tgl_berakhir)}
											style={{ marginTop: vs(5), marginLeft: vs(3) }}
											digitStyle={{backgroundColor: blue}}
											digitTxtStyle={{color: '#FFFFFF', fontSize: 15}}
											timeLabelStyle={{color: 'red', fontWeight: 'bold'}}
											separatorStyle={{color: 'black'}}
											timeToShow={['D', 'H', 'M', 'S']}
											timeLabels={{m: null, s: null}}
											showSeparator
											/>
										</View>
										<View style={[ b.bordered, b.ml2, b.mr2, { height: xs(40), marginTop: vs(7) }]} />
										<View style={[ p.center, { width: xs(80) }]}>
											<Text>Jarak saya :</Text>
											<View style={[ p.row ]}>
												<Text style={[ c.blue, f.bold ]}>1.25</Text>
												<Text> Km</Text>
											</View>
										</View>
										{this.state.jenis_challenge === 'Jalan' ?
											<TouchableOpacity onPress={() => this.props.navigation.push('HomeTracking', {id_kompetisi: this.props.navigation.getParam('id_kompetisi')})} style={[ p.center, b.ml2, { backgroundColor: blue, width: xs(70) }]}>
												<Text style={[ c.light ]}>Jalan</Text>
											</TouchableOpacity>
											:
											<TouchableOpacity onPress={() => this.props.navigation.push('MapsTracking', {id_kompetisi: this.props.navigation.getParam('id_kompetisi')})} style={[ p.center, b.ml2, { backgroundColor: blue, width: xs(70) }]}>
												<Text style={[ c.light ]}>Lari</Text>
											</TouchableOpacity>
										}
									</View>
									:
									<View style={[ b.roundedLow, p.center, { backgroundColor: 'black', width: xs(300), height: xs(30) }]}>
										<CountDown
											size={10}
											until={this.timer_dimulai(this.state.tgl_berakhir, this.state.tgl_dimulai)}
											digitStyle={{backgroundColor: 'black'}}
											digitTxtStyle={{color: '#FFFFFF', fontSize: 15}}
											timeLabelStyle={{color: 'red', fontWeight: 'bold'}}
											separatorStyle={{color: 'white'}}
											timeToShow={['D', 'H', 'M', 'S']}
											timeLabels={{m: null, s: null}}
											showSeparator
										/>
									</View>
								}
								</View>
							)
							:
							(this.state.joined_kompetisi === 'request' ?
								<View style={[ b.roundedLow, b.shadow, p.center, b.mt3, { width: xs(300), height: xs(30), backgroundColor: '#555555' }]}>
									<Text style={[ c.light ]}>Diminta</Text>
								</View>
								:
								(new Date().getTime() >= new Date(this.state.tgl_berakhir).getTime() ?
									<TouchableOpacity onPress={() => this.props.navigation.push('Rating')} style={[ b.roundedLow, b.shadow, b.mt3, { height: xs(30), backgroundColor: blue }]}>
										<Text style={[ c.light, b.ml4, b.mr4, b.mt1, b.mb1 ]}>Lihat Tantangan Akhir</Text>
									</TouchableOpacity>
									:
									<TouchableOpacity activeOpacity={.5} onPress={() => this.ikutiKompetisi()} style={[ b.roundedLow, b.shadow, p.center, b.mt3, { width: xs(300), height: xs(30), backgroundColor: blue }]}>
										<Text style={[ c.light ]}>Ikuti Tantangan</Text>
									</TouchableOpacity>
								)
							)
						}
						{/* Tantangan ini sudah berakhir */}
						{this.state.jenis_challenge === 'Plank' ?
							(this.state.seleksi_juara_plank === undefined ?
								null
								:
								(this.state.seleksi_juara_plank.length === 0 ?
									null
									:
									<View style={[ p.center, p.row, b.mt3, { width: xs(300) }]}>
										{this.state.seleksi_juara_plank.length === 1 ?
											<View style={{ width: xs(100) }} />
											:
											<View style={{ width: xs(100) }}>
												<View style={[ p.center, { width: xs(20), height: xs(20), backgroundColor: 'orange', position: 'absolute', zIndex: 1, borderRadius: xs(10), marginLeft: vs(60) }]}>
													<Text style={[ c.light ]}>2</Text>
												</View>
												<FastImage source={this.state.seleksi_juara_plank[1].url_gambar === null ? { uri: API_URL+'/src/avatar/default.png' } : { uri: API_URL+'/src/avatar/'+this.state.seleksi_juara_plank[1].url_gambar }} style={{ width: xs(50), height: xs(50), marginLeft: vs(25) }} />
												<Text style={[ c.light, p.textCenter, f._12 ]}>{this.state.seleksi_juara_plank[1].nama}</Text>
											</View>
										}
										<View style={{ width: xs(100) }}>
											<FastImage source={require('../../home/asset/pemenang.png')} style={{ position: 'absolute', width: xs(50), height: xs(50), zIndex: 1, marginLeft: vs(55), marginTop: vs(-25) }} />
											<FastImage source={this.state.seleksi_juara_plank[0].url_gambar === null ? { uri: API_URL+'/src/avatar/default.png' } : { uri: API_URL+'/src/avatar/'+this.state.seleksi_juara_plank[0].url_gambar }} style={{ width: xs(80), height: xs(80), marginLeft: vs(10) }} />
											<Text style={[ c.light, p.textCenter, f._12 ]}>{this.state.seleksi_juara_plank[0].nama}</Text>
										</View>
										{this.state.seleksi_juara_plank.length > 2 ?
											<View style={{ width: xs(100) }}>
												<View style={[ p.center, { width: xs(20), height: xs(20), backgroundColor: 'orange', position: 'absolute', zIndex: 1, borderRadius: xs(10), marginLeft: vs(60) }]}>
													<Text style={[ c.light ]}>3</Text>
												</View>
												<FastImage source={this.state.seleksi_juara_plank[2].url_gambar === null ? { uri: API_URL+'/src/avatar/default.png' } : { uri: API_URL+'/src/avatar/'+this.state.seleksi_juara_plank[2].url_gambar }} style={{ width: xs(50), height: xs(50), marginLeft: vs(25) }} />
												<Text style={[ c.light, p.textCenter, f._12 ]}>{this.state.seleksi_juara_plank[2].nama}</Text>
											</View>
											:
											<View style={{ width: xs(100) }} />
										}
									</View>
								)
							)
							:
							(this.state.seleksi_juara === undefined ?
								null
								:
								(this.state.seleksi_juara.length === 0 ?
									null
									:
									<View style={[ p.center, p.row, { width: xs(300), marginTop: vs(40) }]}>
										{this.state.seleksi_juara.length === 1 ?
											<View style={{ width: xs(100) }} />
											:
											<View style={{ width: xs(100) }}>
												<View style={[ p.center, { width: xs(20), height: xs(20), backgroundColor: 'orange', position: 'absolute', zIndex: 1, borderRadius: xs(10), marginLeft: vs(60) }]}>
													<Text style={[ c.light ]}>2</Text>
												</View>
												<FastImage source={this.state.seleksi_juara[1].url_gambar === null ? { uri: API_URL+'/src/avatar/default.png' } : { uri: API_URL+'/src/avatar/'+this.state.seleksi_juara[1].url_gambar }} style={{ width: xs(50), height: xs(50), marginLeft: vs(25) }} />
												<Text style={[ c.light, p.textCenter, f._12 ]}>{this.state.seleksi_juara[1].nama}</Text>
											</View>
										}
										<View style={{ width: xs(100) }}>
											<FastImage source={require('../../home/asset/pemenang.png')} style={{ position: 'absolute', width: xs(50), height: xs(50), zIndex: 1, marginLeft: vs(55), marginTop: vs(-25) }} />
											<FastImage source={this.state.seleksi_juara[0].url_gambar === null ? { uri: API_URL+'/src/avatar/default.png' } : { uri: API_URL+'/src/avatar/'+this.state.seleksi_juara[0].url_gambar }} style={{ width: xs(80), height: xs(80), marginLeft: vs(10) }} />
											<Text style={[ c.light, p.textCenter, f._12 ]}>{this.state.seleksi_juara[0].nama}</Text>
										</View>
										{this.state.seleksi_juara.length > 2 ?
											<View style={{ width: xs(100) }}>
												<View style={[ p.center, { width: xs(20), height: xs(20), backgroundColor: 'orange', position: 'absolute', zIndex: 1, borderRadius: xs(10), marginLeft: vs(60) }]}>
													<Text style={[ c.light ]}>3</Text>
												</View>
												<FastImage source={this.state.seleksi_juara[2].url_gambar === null ? { uri: API_URL+'/src/avatar/default.png' } : { uri: API_URL+'/src/avatar/'+this.state.seleksi_juara[2].url_gambar }} style={{ width: xs(50), height: xs(50), marginLeft: vs(25) }} />
												<Text style={[ c.light, p.textCenter, f._12 ]}>{this.state.seleksi_juara[2].nama}</Text>
											</View>
											:
											<View style={{ width: xs(100) }} />
										}
									</View>
								)
							)
						}
						<View style={[ b.mt3, { width: xs(300) }]}>
							<View style={[ p.row ]}>
								<View style={{ width: xs(50) }}>
									<Svg icon="piala" size={20} />
								</View>
								<View style={{ width: xs(200) }}>
									<Text style={[ c.light ]}>Lari</Text>
								</View>
							</View>
							<View style={[ p.row, b.mt2 ]}>
								<View style={{ width: xs(50) }}>
									<Svg icon="tanggal" size={20} />
								</View>
								<View style={{ width: xs(200) }}>
									<Text style={[ c.light ]}>{new Date('2021-05-24').getDate()+'/'+(new Date('2021-05-24').getMonth()+1)+'/'+new Date('2021-05-24').getFullYear()}</Text>
								</View>
							</View>
							<View style={[ p.row, b.mt2 ]}>
								<View style={{ width: xs(50) }}>
									<Svg icon="tanggal_selesai" size={20} />
								</View>
								<View style={{ width: xs(200) }}>
									<Text style={[ c.light ]}>{new Date('2021-05-31').getDate()+'/'+(new Date('2021-05-31').getMonth()+1)+'/'+new Date('2021-05-31').getFullYear()}</Text>
								</View>
							</View>
							<View style={[ p.row, b.mt2 ]}>
								<View style={{ width: xs(50) }}>
									<Svg icon="poin" size={20} />
								</View>
								<View style={{ width: xs(200) }}>
									<Text style={[ c.light ]}>{+5 * +4} Poin</Text>
								</View>
							</View>
						</View>
						<View style={[ b.mt2, { width: '100%', borderBottomWidth: 1, borderBottomColor: '#555555' }]} />
						<View style={[ p.row, p.center, b.shadowHigh, { width: '100%', height: xs(100), backgroundColor: '#181B22' }]}>
							<View style={[ p.center, { width: xs(150) }]}>
								{
									new Date().getTime() >= new Date(this.state.tgl_dimulai).getTime() ?
									<>
										<Text style={[ f._24, c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>
											{this.day_left_to_finish(this.state.tgl_berakhir)}
										</Text>
										<Text style={[ c.light ]}>Hari Lagi Berakhir</Text>
									</>
									:
									<>
										<Text style={[ f._24, c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>
											{this.day_left_to_begin(this.state.tgl_dimulai)}
										</Text>
										<Text style={[ c.light ]}>Hari Lagi Dimulai</Text>
									</>
								}
							</View>
							<View style={[ b.bordered, b.ml2, b.mr2, { height: xs(40) }]} />
							<View style={[ p.center, { width: xs(150) }]}>
								<Text style={[ f._24, c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>4</Text>
								<Text style={[ c.light ]}>Peserta</Text>
							</View>
						</View>
						<View style={{ width: '100%', backgroundColor: '#000' }}>
							{this.state.joined_kompetisi === 'true' ?
								(this.state.jenis_challenge === 'Push Up' || this.state.jenis_challenge === 'Sit Up' || this.state.jenis_challenge === 'Back Up' || this.state.jenis_challenge === 'Pull Up' || this.state.jenis_challenge === 'Plank' || this.state.jenis_challenge === 'Mountain Climbers' || this.state.jenis_challenge === 'Jumping Jack' || this.state.jenis_challenge === 'Squats' || this.state.jenis_challenge === 'Squat Jump' || this.state.jenis_challenge === 'Side Push Up' ?
									<>
										<View style={[ p.row, p.center, b.mt4, { width: '100%', height: xs(50), backgroundColor: '#181B22' }]}>
											<View style={{ width: xs(100) }}>
												<FastImage source={{ uri: API_URL+'/src/icon_group/'+this.state.icon_kompetisi }} style={[ b.roundedLow, { width: xs(40), height: xs(40) }]} />
											</View>
											<View style={{ width: xs(150) }}>
												<TouchableOpacity onPress={() => this.props.navigation.push('PostChallenge', {id_kompetisi: this.props.navigation.getParam('id_kompetisi')})}>
													<Text style={[ c.light ]}>Lihat semua postingan</Text>
												</TouchableOpacity>
											</View>
											<View style={[ p.alignEnd, { width: xs(50) }]}>
												<TouchableOpacity onPress={() => this.props.navigation.push('UploadImage', {id_kompetisi: this.props.navigation.getParam('id_kompetisi')})}>
													<Icon name="ios-camera" size={40} color={blue} />
												</TouchableOpacity>
											</View>
										</View>
										{this.state.already_upload === 'true' ?
											null
											:
											(new Date().getTime() >= new Date(this.state.tgl_berakhir).getTime() ?
												null
												:
												<View style={[ p.center, b.mt4, { width: '100%', height: xs(100), backgroundColor: '#181B22' }]}>
													<View style={[ p.row, { borderWidth: 1, borderColor: blue }]}>
														<View style={[ p.center, { width: xs(200) }]}>
															<Image source={require('../../home/asset/upload.png')} style={{ width: xs(20), height: xs(20), tintColor: light }} />
															<Text style={[ b.mt1, c.light ]}>Rekam/upload video</Text>
														</View>
														<TouchableOpacity
															onPress={() => this.props.navigation.push('UploadVideo', {id_kompetisi: this.state.id_kompetisi, jenis_kompetisi: this.state.jenis_challenge})}
															style={[ p.center, { backgroundColor: blue, width: xs(100), height: xs(70) }]}
														>
															<Text style={[ c.light, f._18, p.textCenter, { fontFamily: 'lineto-circular-pro-bold' }]}>{this.state.jenis_challenge}</Text>
														</TouchableOpacity>
													</View>
												</View>
											)
										}
									</>
									:
									<View style={[ p.row, p.center, b.mt4, { width: '100%', height: xs(50) }]}>
										<View style={{ width: xs(100) }}>
											<Image source={require('../../home/asset/logo-lari.png')} style={[ b.roundedLow, { width: xs(40), height: xs(40) }]} />
										</View>
										<View style={{ width: xs(150) }}>
											<TouchableOpacity>
												<Text>Lihat semua postingan</Text>
											</TouchableOpacity>
										</View>
										<View style={[ p.alignEnd, { width: xs(50) }]}>
											<TouchableOpacity onPress={() => this.props.navigation.push('UploadImage', {id_kompetisi: this.props.navigation.getParam('id_kompetisi')})}>
												<Icon name="ios-camera" size={40} color={blue} />
											</TouchableOpacity>
										</View>
									</View>
								)
								:
								null
							}
							<View style={[ b.mt4, p.center, { width: '100%', height: xs(100), backgroundColor: '#181B22' }]}>
								<TouchableOpacity onPress={() => this.props.navigation.navigate('Grup_Chall', {id_group: this.state.id_group_visit})}>
									<View style={[ b.mt2, p.row, b.mb2, { width: xs(300) }]}>
										<View>
											<FastImage source={( this.state.url_gambar === '' ? (require('../asset/icon_group.png')) : {uri: API_URL+'/src/icon_group/'+this.state.url_gambar} )} style={[ b.rounded, { width: xs(50), height: xs(50) }]} />
										</View>
										<View style={[ b.ml2, { width: xs(235) }]}>
											<Text style={[ f._10, c.light ]}>LIGA PENYELENGGARA</Text>
											<View style={{ width: xs(235) }}>
												<Text style={[ c.light, { fontFamily: 'lineto-circular-pro-bold' }]}>Nike Run Club Semarang</Text>
											</View>
											<Text style={[ c.light ]}>45 Member</Text>
											{this.state.joined === 'request' ?
												<View style={[ p.center, b.roundedLow, b.shadow, b.mt1, { width: xs(150), backgroundColor: '#555555' }]}>
													<Text style={[ c.light, b.mt1, b.mb1 ]}>Diminta</Text>
												</View>
												:
												(this.state.joined === 'true' ?
													null
													:
													(this.state.status_join === 'true' ?
														<View style={[ p.center, b.roundedLow, b.shadow, b.mt1, { width: xs(150), backgroundColor: '#555555' }]}>
															<Text style={[ c.light, b.mt1, b.mb1 ]}>Diminta</Text>
														</View>
														:
														<TouchableOpacity activeOpacity={.5} onPress={() => this.gabungGroup()} style={[ p.center, b.roundedLow, b.shadow, b.mt1, { width: xs(150), backgroundColor: blue }]}>
															<Text style={[ c.light, b.mt1, b.mb1 ]}>Gabung Liga</Text>
														</TouchableOpacity>
													)
												)
											}
										</View>
									</View>
								</TouchableOpacity>
							</View>
							{new Date().getTime() >= new Date(this.state.tgl_dimulai).getTime() ?
								(this.state.joined_kompetisi === 'true'?
									<>
										<View style={{ width: '100%' }}>
											<View style={[ b.mt2, b.mb2, b.ml2, b.mr2 ]}>
												<FlatList
													extraData={this.state}
													horizontal={true}
													data={this.state.list_hari}
													keyExtractor={item => item.day}
													renderItem={({item, index}) => (
														<View style={[ p.center, b.mt2, b.ml2 ]}>
															{new Date(item.date).getDate()+' '+new Date(item.date).getMonth()+' '+new Date(item.date).getFullYear() === new Date().getDate()+' '+new Date().getMonth()+' '+new Date().getFullYear() ?
																<>
																	<TouchableOpacity onPress={() => this.click(index, item.date, item.status)}>
																		<LinearGradient style={[ b.roundedLow, p.center, { width: xs(35), height: xs(35) }]} colors={['#78A4FA', '#003DB8']}>
																			<View style={[ p.center, { width: xs(25), height: xs(25), borderWidth: 1, borderColor: light, borderRadius: xs(15) }]}>
																				<Text style={[ c.light ]}>{index+1}</Text>
																			</View>
																		</LinearGradient>
																	</TouchableOpacity>
																	{ this.state.clicked === index ?
																		<Text style={[ c.blue, f._10 ]}>Hari ini</Text>
																	:
																		<Text style={[ c.light, f._10 ]}>Hari ini</Text>
																	}
																</>
																:
																(item.status === 'ahead' ?
																	<>
																		<View style={[ b.roundedLow, p.center, { width: xs(35), height: xs(35), backgroundColor: '#BBBBBB' }]}>
																			<View style={[ p.center, { width: xs(30), height: xs(30), backgroundColor: '#A8A8A8', borderRadius: xs(15) }]}>
																				<Text>{index+1}</Text>
																			</View>
																		</View>
																		{ this.state.clicked === index ?
																			<Text style={[ c.blue, f._10 ]}>Hari {index+1}</Text>
																		:
																			<Text style={[ c.light, f._10 ]}>Hari {index+1}</Text>
																		}
																	</>
																	:
																	(item.uploaded === 'false' ?
																		<>
																			<TouchableOpacity onPress={() => this.click(index, item.date, item.status)}>
																				<LinearGradient style={[ b.roundedLow, p.center, { width: xs(35), height: xs(35) }]} colors={['#78A4FA', '#003DB8']}>
																					<View style={[ p.center, { width: xs(25), height: xs(25), borderWidth: 1, borderColor: light, borderRadius: xs(15) }]}>
																						<Icon name="ios-close" size={25} color={light} />
																					</View>
																				</LinearGradient>
																			</TouchableOpacity>
																			{ this.state.clicked === index ?
																				<Text style={[ c.blue, f._10 ]}>Hari {index+1}</Text>
																			:
																				<Text style={[ c.light, f._10 ]}>Hari {index+1}</Text>
																			}
																		</>
																		:
																		<>
																			<TouchableOpacity onPress={() => this.click(index, item.date, item.status)}>
																				<LinearGradient style={[ b.roundedLow, p.center, { width: xs(35), height: xs(35) }]} colors={['#78A4FA', '#003DB8']}>
																					<View style={[ p.center, { width: xs(25), height: xs(25), borderWidth: 1, borderColor: light, borderRadius: xs(15) }]}>
																						<Icon name="ios-checkmark" size={25} color={light} />
																					</View>
																				</LinearGradient>
																			</TouchableOpacity>
																			{ this.state.clicked === index ?
																				<Text style={[ c.blue, f._10 ]}>Hari {index+1}</Text>
																			:
																				<Text style={[ c.light, f._10 ]}>Hari {index+1}</Text>
																			}
																		</>
																	)
																)
															}
														</View>
													)}
												/>
											</View>
										</View>
										<View style={{ width: '100%' }}>
											<View style={[ p.row, b.mt2 ]}>
												<View style={{ width: xs(40) }}>
													<Text style={[ c.light, f._12, p.textRight ]}>Rank</Text>
												</View>
												<View style={[ b.ml2, { width: xs(140) }]}>
													<Text style={[ b.ml4, c.light, f._12 ]}>Peserta</Text>
												</View>
												<View style={[ p.center, b.ml1, { width: xs(100) }]}>
													{this.state.jenis_challenge === 'Plank' ?
														<Text style={[ c.light, f._12 ]}>Durasi {this.state.jenis_challenge}</Text>
														:
														<Text style={[ c.light, f._12 ]}>Jumlah {this.state.jenis_challenge}</Text>
													}
												</View>
												<View style={[ b.ml2, p.center, { width: xs(50) }]}>
													<Text style={[ c.light, f._12 ]}>Video</Text>
												</View>
											</View>
										</View>
										<View style={[ b.mt2, { width: '100%', backgroundColor: '#181B22' }]}>
											<FlatList
												extraData={this.state}
												data={this.state.seleksi}
												keyExtractor={item => item.id_login}
												renderItem={({item, index}) => (
													<>
														<View style={[ p.row, b.mt2, b.mb2 ]}>
															<View style={[ p.center, { width: xs(40) }]}>
																<Text style={[ b.ml3, c.light ]}>{index+1}</Text>
															</View>
															<View style={[ b.ml2, p.center ]}>
																<FastImage source={item.url_gambar === null ? { uri: API_URL+'/src/avatar/default.png' } : { uri: API_URL+'/src/avatar/'+item.url_gambar }} style={{ width: xs(50), height: xs(50) }} />
															</View>
															<View style={[ b.ml2, { width: xs(80) }]}>
																{item.id_login === this.state.id_login ?
																	<Text style={[ c.blue, f.bold, b.mt3 ]}>Anda</Text>
																	:
																	<Text style={[ f.bold, c.light, b.mt3 ]} numberOfLines={1}>{item.nama}</Text>
																}
															</View>
															<View style={[ b.ml2, p.center, { width: xs(100) }]}>
															{item.jenis_challenge === 'Plank' ?
																<Text style={[ c.light ]}>{item.durasi}</Text>
																:
																(item.jumlah === null ?
																	<Text style={{ color: 'red' }}>(Belum Dinilai)</Text>
																	:
																	<Text style={[ c.light ]}>{item.jumlah}</Text>
																)
															}
															</View>
															{item.videos_push_up === null ?
																<View style={[ b.ml1, p.center, { width: xs(50) }]}>
																	<Icon name="ios-play" color={grey} size={25} />
																	<Text style={[ c.grey ]}>Play</Text>
																</View>
															:
																<TouchableOpacity onPress={() => this.props.navigation.navigate('PenilaianPesertaChall', {id_progress_kompetisi: item.id_progress_kompetisi, tanggal: this.state.tanggal_diklik, id_kompetisi: this.state.id_kompetisi})} style={[ b.ml1, p.center, { width: xs(50) }]}>
																	<Icon name="ios-play" color={blue} size={25} />
																	<Text style={[ c.blue ]}>Play</Text>
																</TouchableOpacity>
															}
														</View>
														<View style={{ borderBottomWidth: 1, borderBottomColor: '#555555' }} />
													</>
												)}
											/>
										</View>
									</>
									:
									<View style={{ backgroundColor: '#181B22', height: xs(20) }} />
								)
								:
								<View style={[ p.center, { width: '100%', backgroundColor: '#181B22' }]}>
									<Image source={require('../../home/asset/calendar.png')} style={{ width: xs(105), height: xs(102) }} />
								</View>
							}
						</View>
					</View>
				</ScrollView>
			</>
		)
	}
}

const styles = StyleSheet.create({
	// loading centered screenn
	container_loading: {
	  flex: 1,
	  position:'absolute',
	  zIndex:1,
	  width: '100%',
	  height: '100%',
	  justifyContent: "center",
	  backgroundColor: 'rgba(0,0,0,0.7)'
	},
	horizontal_loading: {
	  flexDirection: "row",
	  justifyContent: "space-around",
	  padding: 10
	},
})