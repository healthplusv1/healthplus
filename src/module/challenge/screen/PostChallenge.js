import React from 'react'
import { View, Text, ActivityIndicator, StyleSheet, TouchableOpacity, ScrollView, FlatList } from 'react-native'
import { b, p, c, f } from '../utils/StyleHelper'
import { vs, xs } from '../utils/Responsive'
import { blue, light } from '../utils/Color'
import FastImage from 'react-native-fast-image'
import Icon from 'react-native-vector-icons/Ionicons'
import { NavigationEvents } from 'react-navigation'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import ListPostChallenge from './ListPostChallenge.js'
import { API_URL } from 'react-native-dotenv'


export default class PostChallenge extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
        }
    }

    async componentDidMount() {
        const id_login = await AsyncStorage.getItem('@id_login')

        axios.get(API_URL+'/main/get_all_post_challenge', {params:{
			id_kompetisi: this.props.navigation.getParam('id_kompetisi'),
			id_login: id_login
		}})
		.then(res => {
			this.setState({
                list_posting: res.data.data,
                loading: false,
			})
			// alert(JSON.stringify(res.data.data))
		})
    }

    hitung_hari(from_date) {
		var date1 = new Date(from_date); 
		var date2 = new Date(new Date().getFullYear(),new Date().getMonth() , new Date().getDate()); 
		// console.log(date1);
		// console.log(date2);
		  
		// To calculate the time difference of two dates 
		var Difference_In_Time = date2.getTime() - date1.getTime(); 
		  
		// To calculate the no. of days between two dates 
		var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
		if (typeof Difference_In_Days === 'number') {
			if(Number(Difference_In_Days.toString().replace('-', '')).toFixed() == 0){
				return 'Today';
			}else{
				return Number(Difference_In_Days.toString().replace('-', '')).toFixed()+' Days ago';
			}
		}
	}

    render() {
        return (
            <>
                {this.state.loading ?
					<View style={[styles.container_loading, styles.horizontal_loading]}>
						<ActivityIndicator size="large" color="#0000ff"/>
					</View>
					:
					null
				}
                <NavigationEvents onDidFocus={() => this.componentDidMount()} />
                <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#000' }}>
                    <View style={[ p.center, { width: '100%', height: xs(80), backgroundColor: '#000' }]}>
                        <View style={[ p.row, { width: '100%' }]}>
                            <View style={[ b.ml4 ]}>
                                <TouchableOpacity onPress={() => this.props.navigation.pop()}>
                                    <Icon name="ios-arrow-back" size={35} color={light} />
                                </TouchableOpacity>
                            </View>
                            <View style={[ p.center, b.ml4, { width: xs(270) }]}>
                                <Text style={[ c.light, f.bold, f._18 ]}>Postingan Challenge</Text>
                            </View>
                        </View>
                    </View>
					<View style={{ width: '100%', borderBottomWidth: 1, borderBottomColor: '#181B22' }} />
                    <View style={{ width: '100%' }}>
                        <FlatList
							extraData={this.state}
							data={this.state.list_posting}
							keyExtractor={item => item.id_login}
							renderItem={({item}) => (
								<ListPostChallenge
									url_avatar={item.url_gambar}
									nama={item.nama}
									tanggal_post={this.hitung_hari(item.tanggal_post_challenge)}
									deskripsi={item.deskripsi_post_challenge}
									post_gambar={item.gambar_post_challenge}
									id={item.id_post_challenge}
									komentar={item.komentar_post_challenge}
									liked_id_login={item.likee_id_login}
									action={() => this.props.navigation.push('ProfTracking', {id_login: item.id_login})}
									comment={() => this.props.navigation.push('KomentarPostChallenge', {id_post_challenge: item.id_post_challenge, id_login: item.id_login})}
								/>
							)}
						/>
                    </View>
                </ScrollView>
            </>
        )
    }
}

const styles = StyleSheet.create({
	// loading centered screenn
	container_loading: {
	  flex: 1,
	  position:'absolute',
	  zIndex:1,
	  width: '100%',
	  height: '100%',
	  justifyContent: "center",
	  backgroundColor: 'rgba(0,0,0,0.7)'
	},
	horizontal_loading: {
	  flexDirection: "row",
	  justifyContent: "space-around",
	  padding: 10
	},
})