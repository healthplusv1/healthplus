import React, { Component } from 'react'
import { Text, View, ScrollView, FlatList } from 'react-native'
import { blue, light } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { b, p, f, c } from '../utils/StyleHelper'
import Svg from '../utils/Svg'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'

export default class Rating extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rank: [
                {id: 4, image: require('../asset/10.png'), nama: 'Michael Taner', poin: '500' },
                {id: 5, image: require('../asset/10.png'), nama: 'Michael Taner', poin: '500' },
                {id: 6, image: require('../asset/10.png'), nama: 'Michael Taner', poin: '500' },
                {id: 7, image: require('../asset/10.png'), nama: 'Michael Taner', poin: '500' },
                {id: 8, image: require('../asset/10.png'), nama: 'Michael Taner', poin: '500' },
            ]
        }
    }

    render() {
        return (
            <View style={{ width: '100%', height: '100%', backgroundColor: '#121212' }}>
                <View style={{ width: '100%', alignItems: 'center' }}>
                    <Text style={[ c.light, f._18, { fontFamily: 'lineto-circular-pro-bold', marginTop: wp('4%'), marginBottom: wp('4%') }]}>Peringkat</Text>
                    <View style={{ width: wp('90%'), flexDirection: 'row' }}>
                        <View style={{ width: wp('28%'), height: hp('20%'), backgroundColor: light, alignItems: 'center', borderRadius: wp('3%'), marginTop: wp('4.5%'), marginRight: wp('3%') }}>
                            <FastImage source={require('../asset/10.png')} style={{ width: wp('13%'), height: wp('13%'), marginTop: wp('2%') }} />
                            <Text style={[ f.bold, f._12, { marginTop: wp('1%') }]} numberOfLines={1}>Michael Taner</Text>
                            <Text style={[ f._12, { marginTop: wp('0.5%') }]}>500 poin</Text>
                            <Text style={[ f.bold, f._12, { marginTop: wp('1%'), color: 'darkorange' }]}>Juara 2</Text>
                        </View>
                        <View style={{ width: wp('28%'), height: hp('22.5%'), backgroundColor: light, alignItems: 'center', borderRadius: wp('3%'), marginRight: wp('3%') }}>
                            <FastImage source={require('../asset/10.png')} style={{ width: wp('13%'), height: wp('13%'), marginTop: wp('2%') }} />
                            <Text style={[ f.bold, f._12, { marginTop: wp('1%') }]} numberOfLines={1}>Michael Taner</Text>
                            <Text style={[ f._12, { marginTop: wp('0.5%') }]}>500 poin</Text>
                            <Text style={[ f.bold, f._12, { marginTop: wp('1%'), color: 'darkorange' }]}>Juara 1</Text>
                        </View>
                        <View style={{ width: wp('28%'), height: hp('20%'), backgroundColor: light, alignItems: 'center', borderRadius: wp('3%'), marginTop: wp('4.5%') }}>
                            <FastImage source={require('../asset/10.png')} style={{ width: wp('13%'), height: wp('13%'), marginTop: wp('2%') }} />
                            <Text style={[ f.bold, f._12, { marginTop: wp('1%') }]} numberOfLines={1}>Michael Taner</Text>
                            <Text style={[ f._12, { marginTop: wp('0.5%') }]}>500 poin</Text>
                            <Text style={[ f.bold, f._12, { marginTop: wp('1%'), color: 'darkorange' }]}>Juara 3</Text>
                        </View>
                    </View>
                    <View style={{ width: wp('90%'), marginTop: wp('6%') }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Svg icon='piala' size={26} />
                            <Text style={[ c.light, { marginLeft: wp('4%') }]}>Senam</Text>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: wp('2%') }}>
                            <Svg icon='tanggal' size={26} />
                            <Text style={[ c.light, { marginLeft: wp('4%') }]}>{new Date('2021-05-24').getDate()+'/'+(new Date('2021-05-24').getMonth()+1)+'/'+new Date('2021-05-24').getFullYear()}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: wp('2%') }}>
                            <Svg icon='tanggal_selesai' size={26} />
                            <Text style={[ c.light, { marginLeft: wp('4%') }]}>{new Date('2021-05-31').getDate()+'/'+(new Date('2021-05-31').getMonth()+1)+'/'+new Date('2021-05-31').getFullYear()}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: wp('2%') }}>
                            <Svg icon='poin' size={26} />
                            <Text style={[ c.light, { marginLeft: wp('4%') }]}>500 poin</Text>
                        </View>
                    </View>
                    <View style={{ width: wp('90%'), height: hp('5%'), backgroundColor: 'darkorange', marginTop: wp('6%'), borderRadius: wp('2%'), flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ width: wp('15%'), alignItems: 'center' }}>
                            <Text style={[ c.light ]}>Rank</Text>
                        </View>
                        <View style={{ width: wp('55%') }}>
                            <Text style={[ c.light ]}>Nama</Text>
                        </View>
                        <View style={{ width: wp('20%') }}>
                            <Text style={[ c.light ]}>Poin</Text>
                        </View>
                    </View>
                </View>
                <ScrollView style={{ backgroundColor: '#121212', marginTop: wp('4%') }}>
                    <View style={{ width: '100%', alignItems: 'center' }}>
                        <FlatList
                            data={this.state.rank}
                            keyExtractor={item => item.id}
                            renderItem={({ item }) => (
                                <View style={{ width: wp('90%'), height: hp('10%'), backgroundColor: '#2A2A2A', marginBottom: wp('4%'), borderRadius: wp('2%'), flexDirection: 'row', alignItems: 'center' }}>
                                    <View style={{ width: wp('15%'), alignItems: 'center' }}>
                                        <Text style={[ c.light ]}>{item.id}</Text>
                                    </View>
                                    <View style={{ width: wp('55%'), flexDirection: 'row', alignItems: 'center' }}>
                                        <FastImage source={item.image} style={{ width: wp('13%'), height: wp('13%') }} />
                                        <View style={{ marginLeft: wp('2%'), width: wp('39') }}>
                                            <Text style={[ c.light ]} numberOfLines={1}>{item.nama}</Text>
                                        </View>
                                    </View>
                                    <View style={{ width: wp('20%') }}>
                                        <Text style={[ c.light ]}>{item.poin} poin</Text>
                                    </View>
                                </View>
                            )}
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}
