import React from 'react'
import { Text, View, TouchableOpacity, Image, ImageBackground, ScrollView, Alert, FlatList, ActivityIndicator, StyleSheet, ToastAndroid } from 'react-native'
import { b, p, f, c } from '../utils/StyleHelper'
import { vs, xs } from '../utils/Responsive'
import { blue, light, grey, softBlue } from '../utils/Color'
import {NavigationEvents} from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons'
import FastImage from 'react-native-fast-image'
import LinearGradient from 'react-native-linear-gradient'
import Svg from '../utils/Svg'
import CountDown from 'react-native-countdown-component'
import ActionButton from 'react-native-action-button';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import { Tabs, Tab } from 'native-base'
import analytics from '@react-native-firebase/analytics'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'
import { API_URL } from 'react-native-dotenv'

export default class Challenges extends React.Component {

	constructor(props){
		super(props);
		this.state ={
			all_challenges: null,
			loading: false,
			joined_ongoing: '',
			clicked: 0,
			exercise: [
				{jenis_challenge: 'Push Up'},
				{jenis_challenge: 'Sit Up'},
				{jenis_challenge: 'Back Up'},
				{jenis_challenge: 'Pull Up'},
				{jenis_challenge: 'Jumping Jacks'},
				{jenis_challenge: 'Squats'},
				{jenis_challenge: 'Squat Jump'},
				{jenis_challenge: 'Mountain Climber'},
				{jenis_challenge: 'Plank'},
			],
			live_stream: null,
			status: '',
			ongoing: [
				{id_kompetisi: '1', image: require('../asset/icon_group.png'), nama_kompetisi: 'Lari Club Semarang', jenis_challenge: 'Lari', pengikut: '5', tgl_dimulai: '2021-05-05', tgl_berakhir: '2021-05-07'},
				{id_kompetisi: '2', image: require('../asset/icon_group.png'), nama_kompetisi: 'Senam Club Semarang', jenis_challenge: 'Senam', pengikut: '6', tgl_dimulai: '2021-05-05', tgl_berakhir: '2021-05-07'},
			],
			upcoming: [
				{id_kompetisi: '3', image: require('../asset/icon_group.png'), nama_kompetisi: 'Lari Club Semarang', jenis_challenge: 'Lari', pengikut: '5', tgl_dimulai: '2021-05-10', tgl_berakhir: '2021-05-12'},
				{id_kompetisi: '4', image: require('../asset/icon_group.png'), nama_kompetisi: 'Senam Club Semarang', jenis_challenge: 'Senam', pengikut: '6', tgl_dimulai: '2021-05-10', tgl_berakhir: '2021-05-12'},
			],
			count: '',
			riwayat_challnges: [
				{id_kompetisi: '1', image: require('../asset/icon_group.png'), nama_kompetisi: 'Lari Marathon Semarang', jenis_challenge: 'Lari', pengikut: '12', tgl_dimulai: '2021-05-05', tgl_berakhir: '2021-05-07', pemenang: 'Erwin Sanjaya'},
				{id_kompetisi: '2', image: require('../asset/icon_group.png'), nama_kompetisi: 'Lari Marathon Ungaran', jenis_challenge: 'Lari', pengikut: '10', tgl_dimulai: '2021-05-05', tgl_berakhir: '2021-05-10', pemenang: 'Erwin Sanjaya'},
			]
		}
	}

	async componentDidMount() {
		await analytics().logEvent('view_challenge', {
			item: 'CHALLENGE_SCREEN'
		})
		await analytics().setUserProperty('user_sees_all_challenge', 'null')

		// const id_login = await AsyncStorage.getItem('@id_login')

		axios.get(API_URL+'/main/shopping/cart', {params: {
			id_login: '118205008143923829902'
		}})
		.then(res => {
			this.setState({
				loading: false,
				count: res.data.data.length,
			})
		})
		.catch(e => {
			console.log(e)
		})

		// axios.get(API_URL+'/main/get_all_challenge', {params:{
		// 	id_login: id_login
		// }})
		// .then(res => {
		// 	this.setState({
		// 		all_challenges: res.data.data,
		// 		loading: false
		// 	})
		// 	// alert(JSON.stringify(res.data.data))
		// })
		// .catch(e => {
		// 	// alert(JSON.stringify(e))
		// })

		// axios.get(API_URL+'/main/get_joined_ongoing', {params:{
		// 	id_login: id_login
		// }})
		// .then(res => {
		// 	this.setState({
		// 		joined_ongoing: res.data.data
		// 	})
		// 	// alert(JSON.stringify(res.data.data.length))
		// })
		// .catch(e => {
		// 	// alert(JSON.stringify(e))
		// })

		// axios.get(API_URL+'/main/filter_kompetisi', {params: {
		// 	id_login: id_login,
		// 	jenis_challenge: 'Push Up'
		// }})
		// .then(resp => {
		// 	this.setState({
		// 		loading: false,
		// 		all_challenges: resp.data.data
		// 	})
		// })

		// axios.get(API_URL+'/main/check_anggota_league', {params: {
		// 	id_login: id_login,
		// }})
		// .then(response => {
		// 	// alert(JSON.stringify(response.data.status))
		// 	this.setState({
		// 		loading: false,
		// 		status: response.data.status
		// 	})
		// })

		// axios.get(API_URL+'/main/get_all_live_stream')
		// .then(result => {
		// 	this.setState({
		// 		loading: false,
		// 		live_stream: result.data.data
		// 	})
		// 	// console.log(result.data.data)
		// })
		// .catch(e => {
		// 	console.log('get_all_live_stream => '+e)
		// })
	}

	timer(date1, date2) {
		var t1 = new Date(Date.parse(date1+'T00:00:01'));
		var t2 = new Date(Date.parse(date2+'T23:59:59'));
		// console.log(date1, date2);
		var d = new Date();
		// var now = d.getTime();
		// console.log(t1, t2, d);
		if (t1.getTime() < d.getTime()) {
		  t1 = d;
		}
	
		var dif = t2.getTime() - t1.getTime();
		// console.log(dif);
	
		var seconds_left = dif / 1000;
		// console.log(seconds_left, seconds_left/60, (seconds_left/60)/60, ((seconds_left/60)/60)/24);
	
		return seconds_left;
	}

	day_left_to_begin(tanggal_mulai) {
		let tanggal_dari = new Date(tanggal_mulai)
		let tanggal_sekarang = new Date()
	
		// To calculate the time difference of two dates 
		var Difference_In_Time = tanggal_sekarang.getTime() - tanggal_dari.getTime(); 
		  
		// To calculate the no. of days between two dates 
		var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
	
		return Difference_In_Days.toFixed().toString().replace(/-/gi, '');
	}

	// async join_live_stream(id_streaming) {
	// 	const id_login = await AsyncStorage.getItem('@id_login')
	// 	axios.get(API_URL+'/main/join_live_stream', {params: {
	// 		id_streaming: id_streaming,
	// 		url_server: 'rtmp://192.168.1.1/live/'+id_streaming+'_'+id_login,
	// 		id_login: id_login,
	// 	}})
	// 	.then(result => {
	// 		// console.log(result.data.status)
	// 		if (result.data.status === 'user_already_exist') {
	// 			Alert.alert(
	// 				'You already join',
	// 				'Do you want join again?',
	// 				[
	// 					{ text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
	// 					{ text: 'Yes', onPress: () => console.log('You already join again') }
	// 				],
	// 				{ cancelable: false }
	// 			)
	// 		} else if (result.data.status === 'jumlah_anggota_sudah_full') {
	// 			ToastAndroid.show('The number of members of this live stream is full', ToastAndroid.SHORT)
	// 		} else if (result.data.status === 'true') {
	// 			const unsubscribe = NetInfo.addEventListener(state => {
	// 				// console.log('Connection Type: '+state.type)
	// 				// console.log('Connected ? '+state.isConnected)
		
	// 				if (state.type === 'cellular') {
	// 					Alert.alert(
	// 						'Currently you are using cellular data',
	// 						'Please use wifi to start this live stream',
	// 						[
	// 							{ text: 'OK', onPress: () => { wifi.setEnabled(true) } }
	// 						]
	// 					)
	// 				} else if (state.type === 'wifi') {
	// 					this.props.navigation.push('LiveStream', {id_streaming: id_streaming})
	// 				}
	// 			})
	// 			unsubscribe()
	// 		}
	// 	})
	// 	.catch(error => {
	// 		console.log('join_live_stream: '+error)
	// 	})
	// }

	render() {
		return (
			<>
				{this.state.loading ?
					<View style={[styles.container_loading, styles.horizontal_loading]}>
						<ActivityIndicator size="large" color="#0000ff" />
					</View>
					:
					null
				}
				<View style={{ width: '100%', height: '100%', backgroundColor: '#121212' }}>
					<View style={{ width: '100%', alignItems: 'center', backgroundColor: '#121212' }}>
						<View style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginTop: wp('4%'), marginBottom: wp('4%') }}>
							<View style={{ width: wp('45%') }}>
								<FastImage source={require('../asset/healthPlus.png')} style={{ width: xs(160), height: xs(38) }} />
							</View>
							<View style={{ width: wp('45%'), flexDirection: 'row-reverse' }}>
								<TouchableOpacity onPress={() => this.props.navigation.navigate('Drawer')}>
									<Image source={require('../asset/menu.png')} style={{ width: wp('7%'), height: wp('7%'), tintColor: light }} />
								</TouchableOpacity>
								<TouchableOpacity onPress={() => this.props.navigation.navigate('Notifikasi')} style={{ marginRight: wp('2%') }}>
									<Icon name="ios-notifications" size={25} color={light} />
									{this.state.notif === 'true' ?
										<View style={{ width: xs(10), height: xs(10), backgroundColor: 'orangered', borderRadius: xs(5),
										position: 'absolute', marginLeft: vs(6), marginTop: vs(1) }} />
										:
										null
									}
								</TouchableOpacity>
								<TouchableOpacity onPress={() => this.props.navigation.push('Cart')} style={{ marginRight: wp('2%') }}>
									{this.state.count !== 0 ?
									<View style={{ width: wp('3.5%'), height: wp('3.5%'), backgroundColor: 'orange', position: 'absolute', borderRadius: wp('3.5%'), zIndex: 1, alignItems: 'center', justifyContent: 'center' }}>
										<Text style={[ c.light, f._8 ]}>{this.state.count}</Text>
									</View>
									:
									null
									}
									<Image source={require('../asset/addCart.png')} style={{ width: wp('7%'), height: wp('7%'), tintColor: light }} />
								</TouchableOpacity>
								<TouchableOpacity onPress={() => this.props.navigation.push('FrequentlyOrder')} style={{ marginRight: wp('2%') }}>
									<Image source={require('../asset/Frequentlyorder_icon.png')} style={{ width: wp('8%'), height: wp('7%'), tintColor: light }} />
								</TouchableOpacity>
							</View>
						</View>
					</View>
					{this.state.status !== '' ?
						<ScrollView style={{ backgroundColor: '#000', width: '100%' }}>
							<View style={[ p.center, { width: '100%', backgroundColor: '#3A3A3A' }]}>
								<View style={[ b.mt4, b.mb4, p.center, { width: xs(300) }]}>
									<Svg icon="bendera" size={80} />
									<Text style={[ c.light, p.textCenter ]}>Silahkan bergabung Liga terlebih dahulu untuk berpartisipasi dalam Tantangan.</Text>
								</View>
								<TouchableOpacity onPress={() => this.props.navigation.push('League')} style={[ b.mt4, b.mb4, b.roundedHigh, { backgroundColor: '#000' }]}>
									<Text style={[ c.light, b.mt2, b.mb2, b.ml4, b.mr4, p.textCenter, f.bold ]}>Cari Liga</Text>
								</TouchableOpacity>
							</View>
						</ScrollView>
						:
						<>
							<Tabs locked={true} tabBarUnderlineStyle={{ borderBottomWidth: 4, borderBottomColor: blue }}>
								<Tab heading='Tantangan' tabStyle={{ backgroundColor: '#181B22' }} textStyle={{ color: light }} activeTabStyle={{ backgroundColor: blue }} activeTextStyle={{ color: light, fontWeight: 'bold' }}>
									<View style={{ width: '100%', height: '100%'}}>
										<ActionButton buttonColor={blue} style={{ zIndex: 1 }} fixNativeFeedbackRadius={true}>
											<ActionButton.Item buttonColor='#3498db' title="Buat Tantangan" onPress={() => this.props.navigation.navigate('BuatKompetisi')}>
												<Icon name="md-create" size={20} color={light} />
											</ActionButton.Item>
											{/* <ActionButton.Item buttonColor='#3498db' title="Buat Live Stream" onPress={() => this.props.navigation.push('LiveStream')}>
												<Icon name="md-create" size={20} color={light} />
											</ActionButton.Item> */}
										</ActionButton>
										<ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#181B22' }}>
											<View style={{ width: '100%', height: wp('2%'), backgroundColor: '#121212' }} />
											<View style={{ width: '100%' }}>
												<Text style={[ c.light, f.bold, f._16, { marginTop: wp('3%'), marginBottom: wp('1%'), marginLeft: wp('5%') }]}>Tantangan Berlangsung</Text>

												<View style={{ width: '100%', borderBottomWidth: 3, borderBottomColor: blue }} />

												<View style={{ marginTop: wp('4%'), marginLeft: wp('5%'), marginBottom: wp('4%') }}>
													<ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
														<FlatList
															data={this.state.ongoing}
															keyExtractor={item => item.id_kompetisi}
															horizontal={true}
															renderItem={({ item }) => (
																<TouchableOpacity onPress={() => this.props.navigation.push('Kompetisi_Chall', { id_kompetisi: item.id_kompetisi })} style={{ width: wp('30%'), marginRight: wp('5%'), backgroundColor: '#121212', borderRadius: wp('2%'), alignItems: 'center' }}>
																	<FastImage source={item.image} style={{ width: wp('20%'), height: wp('20%'), borderRadius: wp('3%'), marginTop: wp('2%') }} />
																	<Text style={[ c.light, { textAlign: 'center' }]} numberOfLines={2}>{item.nama_kompetisi}</Text>
																	<View style={{ width: wp('26%'), marginTop: wp('1%'), flexDirection: 'row', alignItems: 'center' }}>
																		<Svg icon='piala' size={16} />
																		<Text style={[ c.light, f._12, { marginLeft: wp('2%') }]}>{item.jenis_challenge}</Text>
																	</View>
																	<View style={{ width: wp('26%'), flexDirection: 'row', alignItems: 'center' }}>
																		<Svg icon='mengikuti' size={16} />
																		<Text style={[ c.light, f._12, { marginLeft: wp('2%') }]}>{item.pengikut} Mengikuti</Text>
																	</View>
																	<View style={{ width: wp('26%'), flexDirection: 'row', alignItems: 'center', borderWidth: 1, borderRadius: wp('2%'), borderColor: 'darkorange', marginTop: wp('1%'), marginBottom: wp('2%') }}>
																		<View style={{ marginLeft: vs(3) }} />
																		<CountDown
																			size={8}
																			until={this.timer(item.tgl_dimulai, item.tgl_berakhir)}
																			digitStyle={{backgroundColor: '#121212'}}
																			digitTxtStyle={{color: 'darkorange', fontSize: 13}}
																			timeLabelStyle={{color: 'red'}}
																			timeToShow={['H']}
																			timeLabels={{h: null}}
																			showSeparator
																		/>
																		<Text style={{ color:'darkorange', marginLeft: vs(-3) }}>h</Text>
																		<CountDown
																			size={8}
																			until={this.timer(item.tgl_dimulai, item.tgl_berakhir)}
																			digitStyle={{backgroundColor: '#121212'}}
																			digitTxtStyle={{color: 'darkorange', fontSize: 13}}
																			timeLabelStyle={{color: 'red'}}
																			timeToShow={['D']}
																			timeLabels={{d: null}}
																			showSeparator
																		/>
																		<Text style={{ color:'darkorange', marginLeft: vs(-3) }}>j</Text>
																		<CountDown
																			size={8}
																			until={this.timer(item.tgl_dimulai, item.tgl_berakhir)}
																			digitStyle={{backgroundColor: '#121212'}}
																			digitTxtStyle={{color: 'darkorange', fontSize: 13}}
																			timeLabelStyle={{color: 'red'}}
																			timeToShow={['M']}
																			timeLabels={{m: null}}
																			showSeparator
																		/>
																		<Text style={{ color:'darkorange', marginLeft: vs(-3) }}>m</Text>
																	</View>
																</TouchableOpacity>
															)}
														/>
													</ScrollView>
												</View>
											</View>

											<View style={{ width: '100%', height: wp('2%'), backgroundColor: '#121212' }} />

											<View style={{ width: '100%'}}>
												<Text style={[ c.light, f._16, f.bold, { marginTop: wp('3%'), marginBottom: wp('1%'), marginLeft: wp('5%') }]}>Tantangan akan datang</Text>

												<View style={{ width: '100%', borderBottomWidth: 3, borderBottomColor: blue }} />

												<View style={{ width: '100%', alignItems: 'center' }}>
													<FlatList
														ListHeaderComponent={<View style={{ marginTop: wp('4%') }} />}
														data={this.state.upcoming}
														keyExtractor={item => item.id_kompetisi}
														renderItem={({ item }) => (
															<TouchableOpacity onPress={() => this.props.navigation.push('Kompetisi_Chall', { id_kompetisi: item.id_kompetisi })} style={{ width: wp('90%'), marginBottom: wp('4%'), flexDirection: 'row', alignItems: 'center' }}>
																<FastImage source={item.image} style={{ width: wp('25%'), height: wp('25%'), borderRadius: wp('2%') }} />
																<View style={{ width: wp('40%'), marginLeft: wp('3%') }}>
																	<Text style={[ c.light, f.bold ]} numberOfLines={2}>{item.nama_kompetisi}</Text>
																	<View style={{ flexDirection: 'row', alignItems: 'center', marginTop: wp('2%') }}>
																		<Svg icon='piala' size={16} />
																		<Text style={[ c.light, { marginLeft: wp('2%') }]}>{item.jenis_challenge}</Text>
																	</View>
																	<View style={{ flexDirection: 'row', alignItems: 'center', marginTop: wp('1%') }}>
																		<Svg icon='mengikuti' size={16} />
																		<Text style={[ c.light, { marginLeft: wp('2%') }]}>{item.pengikut} Mengikuti</Text>
																	</View>
																	<View style={{ width: wp('38%'), borderWidth: 1, borderColor: 'darkorange', marginTop: wp('2%'), flexDirection: 'row', alignItems: 'center', justifyContent: 'center', borderRadius: wp('1%') }}>
																		<Text style={{ color: 'darkorange' }}>Dimulai </Text>
																		<View style={{ flexDirection: 'row', alignItems: 'center' }}>
																			<Text style={{ color: 'darkorange' }}>{this.day_left_to_begin(item.tgl_dimulai)}</Text>
																			<Text style={{ color: 'darkorange' }}> hari lagi</Text>
																		</View>
																	</View>
																</View>
															</TouchableOpacity>
														)}
													/>
												</View>
											</View>
										</ScrollView>
									</View>
								</Tab>
								<Tab heading='Riwayat Tantangan' tabStyle={{ backgroundColor: '#181B22' }} textStyle={{ color: light }} activeTabStyle={{ backgroundColor: blue }} activeTextStyle={{ color: light, fontWeight: 'bold' }}>
									{this.state.riwayat_challnges === undefined ?
										<View style={{ width: '100%', height: '100%', alignItems: 'center', justifyContent: 'center', backgroundColor: '#121212' }}>
											<Image source={require('../asset/time_machine.png')} style={{ width: wp('20%'), height: wp('20%'), tintColor: '#CCC' }} />
											<Text style={[ c.light, { marginTop: wp('2%'), color: '#CCC' }]}>Belum ada riwayat</Text>
										</View>
										:
										<View style={{ width: '100%', height: '100%', alignItems: 'center', backgroundColor: '#181B22' }}>
											<View style={{ width: '100%', height: wp('2%'), backgroundColor: '#121212' }}></View>
											<FlatList
												ListHeaderComponent={<View style={{ marginTop: wp('4%') }} />}
												data={this.state.riwayat_challnges}
												keyExtractor={item => item.id_kompetisi}
												renderItem={({ item }) => (
													<TouchableOpacity onPress={() => this.props.navigation.push('Rating')} style={{ width: wp('90%'), flexDirection: 'row', alignItems: 'center', marginBottom: wp('4%') }}>
														<FastImage source={item.image} style={{ width: wp('20%'), height: wp('20%'), borderRadius: wp('2%') }} />
														<View style={{ width: wp('66%'), marginLeft: wp('4%') }}>
															<Text style={[ c.light, f._16, { fontFamily: 'lineto-circular-pro-bold' }]} numberOfLines={2}>{item.nama_kompetisi}</Text>
															<View style={{ flexDirection: 'row', alignItems: 'center', marginTop: wp('2%') }}>
																<Svg icon='tanggal_selesai' size={22} />
																<Text style={[ c.light, { marginLeft: wp('2%') }]}>{new Date(item.tgl_berakhir).getDate()+'/'+(new Date(item.tgl_berakhir).getMonth()+1)+'/'+new Date(item.tgl_berakhir).getFullYear()}</Text>
															</View>
															<View style={{ flexDirection: 'row', alignItems: 'center', marginTop: wp('2%') }}>
																<Svg icon='mengikuti' size={22} />
																<Text style={[ c.light, { marginLeft: wp('2%') }]}>{item.pengikut} Mengikuti</Text>
															</View>
															<View style={{ width: '100%', borderBottomWidth: 1, borderBottomColor: light, marginTop: wp('2%') }} />
															<Text style={[ c.light, { marginTop: wp('2%') }]}>Pemenang: {item.pemenang}</Text>
														</View>
													</TouchableOpacity>
												)}
											/>
										</View>
									}
								</Tab>
							</Tabs>
						</>
					}
				</View>
			</>
		)
	}
}

const styles = StyleSheet.create({
  // loading centered screenn
  container_loading: {
    flex: 1,
    position:'absolute',
    zIndex:1,
    width: '100%',
    height: '100%',
    justifyContent: "center",
    backgroundColor: 'rgba(0,0,0,0.7)'
  },
  horizontal_loading: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },
});