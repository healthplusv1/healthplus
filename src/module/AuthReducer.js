const initialState = {
    isLogedIn : false,
    user: null,
    token:null
}

const AuthReducer = (state = initialState, action) =>{
    switch (action.type) {
        case 'SET_AUTH':
            return{
                ...state,
                user: action.user,
                token:action.token,
                isLogedIn: true
            }
        case 'IS_LOGGED_OUT':
            return initialState
    
        default:
            return initialState
    }
}

export default AuthReducer