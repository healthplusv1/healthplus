import React, { memo } from 'react';
import { ms } from '../utils/Responsive';
import Home from '../asset/icons/home_01.svg'
import Rewardaaa_01 from '../asset/icons/rewardaaa_01.svg'
import Notification from '../asset/icons/notification.svg'
import Profile from '../asset/icons/profile.svg'
import Reward from '../asset/icons/reward.svg'
import Funstation2 from '../asset/icons/funstation2.svg'
import Drawer from '../asset/icons/drawer.svg'
import Facebook from '../asset/icons/fb.svg'
import Google from '../asset/icons/google.svg'
import Challenge from '../asset/icons/kmpetisi_01.svg'
import League from '../asset/icons/league_01.svg'
import Guideline from '../asset/icons/guideline_01.svg'
import Rewards from '../asset/icons/rewards.svg'
import TebalGuideline from '../asset/icons/tebalguideline.svg'
import Klaim from '../asset/icons/klaim_01.svg'
import Tebal_Rewards from '../asset/icons/tebal_rewards.svg'
import Feed from '../asset/icons/feed.svg'
import Dasdas from '../asset/icons/dasdas.svg'


const Svg = ({ icon, size = 30, sizes= 25, fill, opacity, ...props }) => {
  const icons = {
    home: Home,
    rewardaaa_01: Rewardaaa_01,
    notification: Notification,
    profile: Profile,
    reward: Reward,
    funstation2: Funstation2,
    drawer: Drawer,
    fb: Facebook,
    google: Google,
    challenge: Challenge,
    league: League,
    guideline: Guideline,
    rewards: Rewards,
    tebalguideline: TebalGuideline,
    klaim: Klaim,
    tebal_rewards: Tebal_Rewards,
    feed: Feed,
    dasdas: Dasdas,
  };

  if (icons[icon] === undefined)
    return (
      <></>
    );

  const Icon = icons[icon]
  return (
    <Icon
      width={ms(sizes)}
      fill={fill || null}
      height={ms(size)}
      fillOpacity={opacity || 1}
      {...props}
    />
  );
}
export default memo(Svg)
