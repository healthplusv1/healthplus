import React, { Component } from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import { blue, light } from '../utils/Color'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import FastImage from 'react-native-fast-image'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'

export default class ProsesVerifyKTP extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        return (
            <FastImage source={require('../asset/back_sign.png')} style={{ width: '100%', height: '100%', alignItems: 'center' }}>
                <View style={{ width: wp('70%'), marginTop: wp('45%') }}>
                    <Text style={[ f.bold, f._20, { textAlign: 'center' }]}>Proses Verifikasi</Text>
                    <Text style={{ textAlign: 'center' }}>Data kamu sedang dalam proses verifikasi, tunggu hingga 1X24jam.</Text>
                </View>

                <FastImage source={require('../asset/Verifikasi.png')} style={{ width: xs(150), height: xs(173), marginTop: wp('18%') }}></FastImage>

                <TouchableOpacity onPress={() => this.props.navigation.push('Tabs')} style={{ width: wp('85%'), height: hp('7.5%'), backgroundColor: blue, borderRadius: wp('2%'), position: 'absolute', bottom: wp('6%'), alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={[ c.light, f.bold, f._18 ]}>Konfirmasi</Text>
                </TouchableOpacity>
            </FastImage>
        )
    }
}
