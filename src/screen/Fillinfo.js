import React, { Component } from 'react';
import { View, Alert, Text, Image, TextInput, TouchableOpacity, StyleSheet, ActivityIndicator, ScrollView } from 'react-native';
import { b, p, f, c } from '../utils/StyleHelper';
import { vs, xs } from '../utils/Responsive';
import { blue, light, disabled } from '../utils/Color';
import Ionicon from 'react-native-vector-icons/Ionicons'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'
import {NavigationEvents} from 'react-navigation';
import CheckBox from '@react-native-community/checkbox'
import Geolocation from '@react-native-community/geolocation';
import Svg from '../utils/Svg'
import FastImage from 'react-native-fast-image'
import { widthPercentageToDP as wp } from 'react-native-responsive-screen'
import app from '@react-native-firebase/app'
import iid from '@react-native-firebase/iid'
import { API_URL } from 'react-native-dotenv'

export default class Fillinfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            location: null,
            latitude: null,
            longitude: null,
            // lat_hp: '',
            // lon_hp: '',
            btn_submit: false,
            checked: false,
            loading: false,
            bahasa: 'Indonesia',
            negara: 'Indonesia',
            kota: '',
            provinsi: '',
            // no_hp: '',
            iid: '',
        };
    }

    async componentDidMount() {

        let iid = await app.iid().getToken()
        this.setState({ iid: iid })

        // alert(this.props.navigation.getParam('id_login'))
        // setInterval(() => {
        //   alert(JSON.stringify(this.state))
        // }, 5000)

        // const bahasa = await AsyncStorage.getItem('@bahasa');
        // const negara = await AsyncStorage.getItem('@negara');
        // const no_hp = await AsyncStorage.getItem('@no_hp');
        const name = await AsyncStorage.getItem('@nama');

        this.setState({
            // bahasa: bahasa,
            // negara: negara,
            // no_hp: no_hp,
            name: name
        })

        // setInterval(() => console.log(this.state.lat_hp+' '+this.state.lon_hp), 5000)
        this.fill_form()
        // alert(JSON.stringify(this.state))
    }

    fill_form() {
        if (this.props.navigation.getParam('location') != null) {
            
            axios.get('https://maps.googleapis.com/maps/api/geocode/json?latlng='+this.props.navigation.getParam('location').latitude+','+this.props.navigation.getParam('location').longitude+'&key=AIzaSyBl8maXfxHVwRHYbBkhKVIMfNQ98bdozrI')
            .then(data => {
                console.log("Kota => ", JSON.stringify(data.data.results[0].address_components[4]))
                console.log("Provinsi => ", JSON.stringify(data.data.results[0].address_components[5]))
                if (data.data.results[0].address_components[4].long_name === 'Indonesia' || /^\d+$/.test(data.data.results[0].address_components[5].long_name.toString().trim()) === true) {
                    this.setState({
                        latitude: this.props.navigation.getParam('location').latitude,
                        longitude: this.props.navigation.getParam('location').longitude,
                        location: this.props.navigation.getParam('location').address,
                        kota: '-',
                        provinsi: '-',
                    })
                } else {
                    this.setState({
                        latitude: this.props.navigation.getParam('location').latitude,
                        longitude: this.props.navigation.getParam('location').longitude,
                        location: this.props.navigation.getParam('location').address,
                        kota: data.data.results[0].address_components[4].long_name,
                        provinsi: data.data.results[0].address_components[5].long_name,
                    })
                }
            })
        }
    }

    agreement() {
        this.setState({
            btn_submit: this.state.btn_submit == false ? true : false
        })
        if (this.state.checked === false) {
            this.setState({
                checked: true
            })
        }else{
            this.setState({
                checked: false
            })
        }
    }

    submitData = async () => {
        const id_login = await AsyncStorage.getItem('@id_login')
        this.setState({ loading: true })
        if (this.state.btn_submit == false) {
            this.setState({ loading: false })
            // alert(translate("alert"))
        } else if (this.state.location === null || this.state.location === '')
        {
            this.setState({ loading: false })
            Alert.alert('', 'Mohon Isi Semua Form')
            // Alert.alert('', 'Please fill all form')
        }else{
            // alert(JSON.stringify(this.state))
            axios.get(API_URL+'/main/update_user', {params:{
                id_login: this.props.navigation.getParam('id_login'),
                nama: this.state.name,
                alamat: this.state.location,
                lat_rumah: this.state.latitude,
                lon_rumah: this.state.longitude,
                // lat_hp: this.state.lat_hp,
                // lon_hp: this.state.lon_hp,
                bahasa: this.state.bahasa,
                negara: this.state.negara,
                kota: this.state.kota,
                provinsi: this.state.provinsi
            }})
            .then(result => {
                console.log(result.data)
                if (result.data.status === 'true') {
                    // this.setState({ loading: false })
                    axios.get(API_URL+'/main/update_id_notification', {params: {
						id_login: this.props.navigation.getParam('id_login'),
						id_notification: this.state.iid
					}})
					.then(resp => {
                        // alert(JSON.stringify(resp))
						// AsyncStorage.setItem('@login', 'true')
                        axios.get(API_URL+'/main/get_user_by_id_login', {params:{
                            id_login: id_login
                        }})
                        .then(result => {
                            // alert(JSON.stringify(result))
                            AsyncStorage.setItem('@nama', this.state.name)
                            // AsyncStorage.setItem('@nama', this.state.name)
                            AsyncStorage.setItem('@login', 'true')
                            AsyncStorage.setItem('@id', result.data.data[0].id_user)
                            // AsyncStorage.setItem('@registered', 'true')
                        })
                        .then(() => {
                            let heart = setInterval(() => this.check_radius(), 300000)
                            this.props.navigation.navigate('Home')
                            this.setState({ loading: false })
                        })
                        .catch(e => {
                            this.setState({ loading: false })
                            // Alert.alert('', JSON.stringify(e))
                        })
					})
                }
            })
            .catch(e => {
                console.log(e)
            })
        }
    }

    render() {
        return (
            <>
            {this.state.loading === true ?
                <View style={[styles.container_loading, styles.horizontal_loading]}>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
                :
                null
            }
            <NavigationEvents onDidFocus={() => this.fill_form()} />
            <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#181B22' }}>
                <View style={[b.container, b.p4, p.column]}>
                    <Text style={[p.textCenter, f._24, c.light, { fontFamily: 'lineto-circular-pro-bold' }]}> Pilih Lokasi Anda </Text>
                    <View style={[p.center, b.my4, { height: vs(250) }]}>
                        <FastImage source={require('../asset/info-icon.png')} style={{ width: wp('68%'), height: '100%' }} resizeMode={'cover'} />
                    </View>
                    <View>
                        <View style={[b.mb2, p.row, p.alignCenter, p.justifyBetween, { borderBottomColor: blue, borderBottomWidth: 2 }]}>
                            <View style={{ width: xs(220) }}>
                                <TextInput
                                    style={[ c.light ]}
                                    placeholder={"Masukkan alamat lokasi"}
                                    placeholderTextColor={"#FFF"}
                                    onChangeText={location => {
                                        let locations = this.state.location
                                        locations = location
                                        this.setState({ location });
                                    }}
                                    value={this.state.location}
                                />
                            </View>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Maplocation', {
                                location: this.state.location,
                            })} style={[p.alignEnd, b.py1, b.roundedHigh, b.px4, p.row, { backgroundColor: blue }]}>
                                <Ionicon name={'md-locate'} color={light} size={xs(15)} />
                                <Text style={[c.light, b.ml1]}> Pilih </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <Text style={[c.blue, f._14,]}>
                        Silahkan isi alamat dengan benar
                    </Text>
                    <View style={[p.row, b.py3, p.alignCenter]}>
                        <CheckBox
                            tintColors={{ false: '#FFF' }}
                            value={this.state.checked}
                            onChange={() => this.agreement()}
                        />
                        <Text style={[c.light, f._14]}> Saya setuju dengan </Text>
                        <Text style={[c.light, f._14, { textDecorationLine: 'underline', fontFamily: 'lineto-circular-pro-bold' }]}> syarat & ketentuan </Text>
                    </View>
                    <TouchableOpacity onPress={() => this.submitData()} style={[p.center, b.py2, b.rounded, b.px4, p.row, { backgroundColor: this.state.checked === false ? disabled : blue }]} disabled={this.state.checked === false}>
                        <Text style={[c.light, b.ml1, f._20, { fontFamily: 'lineto-circular-pro-bold' }]}> Kirimkan </Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
            </>
        );
    }
}
const styles = StyleSheet.create({
  // loading centered screenn
  container_loading: {
    position:'absolute',
    zIndex: 1,
    width: '100%',
    height: '100%',
    justifyContent: "center",
    backgroundColor: 'rgba(0,0,0,0.7)'
  },

  horizontal_loading: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },
});