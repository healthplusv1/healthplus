import React from 'react'
import { View, Text, Image, TextInput, TouchableOpacity, ToastAndroid } from 'react-native'
import FastImage from 'react-native-fast-image'
import { widthPercentageToDP as wp } from 'react-native-responsive-screen'
import { xs, vs } from '../utils/Responsive'
import { f, c, b, p } from '../utils/StyleHelper'
import { blue, light } from '../utils/Color'
import axios from 'axios'
import auth from '@react-native-firebase/auth'
import app from '@react-native-firebase/app'
import iid from '@react-native-firebase/iid'
import AsyncStorage from '@react-native-community/async-storage'
import analytics from '@react-native-firebase/analytics'
import { API_URL } from 'react-native-dotenv'

export default class SignInPhone extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            phone_number: '',
            code: '',
            confirm: null,
            timer: 60
        }
    }

    async componentDidMount() {
        const token = await app.iid().getToken()
        this.setState({ iid: token })

        await analytics().logEvent('sign_in', {
            item: 'PHONE_NUMBER'
        })
        await analytics().setUserProperty('user_get_info_from_phone_number', 'null')
        await analytics().setAnalyticsCollectionEnabled(true)
        
        auth().onAuthStateChanged(async user => {
            if (user) {
                console.log("logged in");
                // console.log("phone => ", await AsyncStorage.getItem('@phone'))
                if (await AsyncStorage.getItem('@id_login') !== null) {
                    const id_login = await AsyncStorage.getItem('@id_login')
                    axios.get(API_URL+'/get_user_by_id_login', {params: {
                    id_login: id_login
                    }})
                    .then(resp => {
                    if (resp.data.status === 'true') {
                        this.props.navigation.navigate('KTP')
                    }
                    })
                } else if (await AsyncStorage.getItem('@phone') !== null) {
                    this.props.navigation.navigate('InputName')
                }
            } else {
                console.log("not logged in");
            }
        });
    }

    signInWithPhone = async (phoneNumber) => {
        this.setState({ timer: 120 })
        axios.get(API_URL+'/check_phone_number', {params: {
            no_hp: this.state.phone_number
        }})
        .then(async (resp) => {
            // console.log(resp.data.status)
            // if (resp.data.status === 'user_no_exist_yet') {
            //     ToastAndroid.show("Nomor telepon ini belum terdaftar", ToastAndroid.SHORT)
            // } else if (resp.data.status === 'user_already_exist') {
            //     axios.get(API_URL+'/get_user_by_no_hp', {params: {
            //         no_hp: this.state.phone_number
            //     }})
            //     .then(async (resp) => {
            //         axios.get(API_URL+'/update_id_notification', {params: {
            //             id_login: this.state.id_email,
            //             id_notification: this.state.iid
            //         }})
            //         .then(result => {
            //             console.log(result)
            //         })
            //         console.log(resp.data.data[0].id_login)
            //         AsyncStorage.setItem('@id_login', resp.data.data[0].id_login)
            //     })
                try {
                    const confirmation = await auth().signInWithPhoneNumber(phoneNumber)
                    console.log("confirm => ", confirmation)
                    this.setState({ confirm: confirmation })
                    let timer_interval = setInterval(() => {
                        if(this.state.timer === 0){
                            clearInterval(timer_interval)
                        } else {
                            this.setState({
                                timer: this.state.timer - 1
                            })
                        }
                    }, 1000)
                } catch (error) {
                    console.log("Invalid => ", error)
                    if (error.code === 'auth/unknown') {
                        ToastAndroid.show("Terlalu banyak permintaan OTP, silahkan coba lagi nanti", ToastAndroid.SHORT)
                    }
                }
            // }
        })
    }

    confirmCode = async () => {
        try {
            await this.state.confirm.confirm(this.state.code)
            ToastAndroid.show("Success", ToastAndroid.SHORT)
            this.props.navigation.navigate('Home')
        } catch (error) {
            if (error.code === 'auth/session-expired') {
                ToastAndroid.show("Kode sms sudah tidak berlaku. Harap kirim ulang kode verifikasi untuk mencoba lagi", ToastAndroid.SHORT)
            } else if (error.code === 'auth/invalid-verification-code') {
                ToastAndroid.show("Kode verifikasi salah", ToastAndroid.SHORT)
            }
            console.log('Invalid Code => '+error)
        }
    }

    handleBackButton = () => {
        this.props.navigation.pop();
        return true;
    }

    render() {
        return (
            <FastImage source={require('../asset/background.jpg')} style={[ p.center, { width: '100%', height: '100%' }]}>
                <View style={[ b.roundedHigh, { width: xs(300), height: xs(500), backgroundColor: light, alignItems: 'center' }]}>
                    <FastImage source={require('../asset/HEALTHPLUS.png')} style={{ width: xs(125), height: xs(125), marginTop: vs(30) }} />
                    <Text style={[ f._24, b.mt4, { fontFamily: 'lineto-circular-pro-bold' }]}>Sign In</Text>
                    {!this.state.confirm ?
                        <>
                            <View style={[ p.row, b.mt4, b.rounded, { width: xs(230), height: xs(50), alignItems: 'center', borderWidth: 1 }]}>
                                <View style={[ p.center, { height: '100%', width: xs(40) }]}>
                                    <Image source={require('../asset/phone-call.png')} style={{ width: xs(20), height: xs(20), tintColor: (this.state.phone_number === '' ? '#D3D3D3' : 'black') }} />
                                </View>
                                <TextInput
                                    style={{ width: xs(190) }}
                                    placeholder="Masukkan Nomor Telepon"
                                    keyboardType={"phone-pad"}
                                    onChangeText={(text) => this.setState({ phone_number: text })}
                                    value={this.state.phone_number}
                                />
                            </View>
                            <View style={[ b.mt1, { width: xs(210) }]}>
                                <Text style={[ f._10 ]}>Contoh: +6285222334444</Text>
                            </View>
                            <TouchableOpacity onPress={() => this.signInWithPhone(this.state.phone_number)} style={{ backgroundColor: blue, borderRadius: wp('2.22223%'), marginTop: wp('3%'), marginBottom: wp('3%'), alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={[ c.light, f._16, b.mt2, b.mb2, b.ml4, b.mr4, { fontFamily: 'lineto-circular-pro-bold' }]}>Kirim verifikasi kode</Text>
                            </TouchableOpacity>
                        </>
                        :
                        <>
                            <View style={[ p.row, b.mt4, b.rounded, { width: xs(230), height: xs(50), alignItems: 'center', borderWidth: 1 }]}>
                                <View style={[ p.center, { width: xs(40), height: '100%' }]}>
                                    <Image source={this.state.code === '' ? require('../asset/key.png') : require('../asset/keyBlack.png')} style={{ width: xs(20), height: xs(20) }} />
                                </View>
                                <TextInput
                                    style={{ width: xs(190) }}
                                    placeholder="Masukkan Kode"
                                    keyboardType={"number-pad"}
                                    onChangeText={(text) => this.setState({ code: text })}
                                    value={this.state.code}
                                />
                            </View>
                            <View style={{ width: wp('60%'), alignItems: 'center', justifyContent: 'center' }}>
                                {this.state.timer === 0 ?
                                    <Text style={{ color: 'white' }} onPress={() => this.signInWithPhone(this.state.phone_number)}>Kirim Ulang Kode</Text>
                                    :
                                    <Text style={{ color: 'white' }}>Kirim Ulang Kode ({this.state.timer})</Text>
                                }
                            </View>
                            <TouchableOpacity onPress={this.confirmCode} style={{ width: wp('60%'), height: wp('11%'), backgroundColor: 'white', borderRadius: wp('2.22223%'), marginTop: wp('3%'), marginBottom: wp('3%'), elevation: 4, alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={{ fontWeight: 'bold', fontSize: wp('4.5%') }}>Konfirmasi verifikasi kode</Text>
                            </TouchableOpacity>
                        </>
                    }
                    <View style={[ p.row, p.center, { marginTop: vs(50), width: '100%' }]}>
                        <Text style={{ color: '#b8b8b8', fontFamily: 'lineto-circular-pro-bold' }}> BELUM PUNYA AKUN? </Text>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('SignUpPhone')}>
                            <Text style={[ c.blue, { fontFamily: 'lineto-circular-pro-bold' }]}> SIGN UP </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </FastImage>
        )
    }
}