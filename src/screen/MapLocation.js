import React, { Component } from 'react';
import { View, Text } from 'react-native';
import LocationView from "react-native-location-view";
import { b } from '../utils/StyleHelper';

export default class MapLocation extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <View style={[b.container]}>
                <LocationView
                    ref="map"
                    apiKey={"AIzaSyAI4dIdi0wXv5sgx3DXh6M_Ad_To6iC8n4"}
                    markerColor="#D4AF37"
                    initialLocation={{
                        latitude: -6.966667,
                        longitude: 110.416664
                    }}
                    onLocationSelect={result => {
                        // alert(JSON.stringify(result))
                        this.props.navigation.navigate('Fillinfo', {
                            location: result
                        })
                        // alert(JSON.stringify(result))
                    }}
                    actionText="Pick This Location"
                    autoCompleteStyle={{
                        width: "80%",
                        padding: 5,
                        marginTop: 5
                    }}
                />
            </View>
        );
    }
}
