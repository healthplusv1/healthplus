import React, { Component } from 'react';
import { Alert, View, Text, ImageBackground, Image, KeyboardAvoidingView, StyleSheet, I18nManager, ActivityIndicator, TouchableWithoutFeedback, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import { b, p, f, c } from '../utils/StyleHelper';
import { vs, xs } from '../utils/Responsive';
import { blue, light, grey, softBlue } from '../utils/Color';
import auth from '@react-native-firebase/auth';
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { API_URL } from 'react-native-dotenv'
// import * as RNLocalize from "react-native-localize";
// import i18n from "i18n-js";
// import memoize from "lodash.memoize";



// const translationGetters = {
//   // lazy requires (metro bundler does not support symlinks)
//   id: () => require("../translations/id.json"),
// };

// const translate = memoize(
//   (key, config) => i18n.t(key, config),
//   (key, config) => (config ? key + JSON.stringify(config) : key)
// );

// const setI18nConfig = () => {
//   // fallback if no available language fits
//   const fallback = { languageTag: "en", isRTL: false };

//   const { languageTag, isRTL } =
//     RNLocalize.findBestAvailableLanguage(Object.keys(translationGetters)) ||
//     fallback;

//   // clear translation cache
//   translate.cache.clear();
//   // update layout direction
//   I18nManager.forceRTL(isRTL);
//   // set i18n-js config
//   i18n.translations = { [languageTag]: translationGetters[languageTag]() };
//   i18n.locale = languageTag;
// };

export default class Phone extends Component {
    constructor(props) {
        // setI18nConfig();
        super(props);
        this.state = {
            phone: '',
            btn_enable: false
        };
    }

    // componentDidMount() {
    //     RNLocalize.addEventListener("change", this.handleLocalizationChange);
    // }

    //   componentWillUnmount() {
    //     RNLocalize.removeEventListener("change", this.handleLocalizationChange);
    //   }

    //   handleLocalizationChange = () => {
    //     setI18nConfig();
    //     this.forceUpdate();
    //   };

    choose_language(lang) {
        this.setState({
            lang_selected: lang,
        })
    }

    async sendCode() {
    AsyncStorage.setItem('@timer', '1800')
    this.setState({loading: true})
    const no_hp = await AsyncStorage.getItem('@no_hp')
      if (this.state.phone === '') {
        this.setState({loading: false})
        Alert.alert('Mohon Masukan Nomor HP Anda')
      }else{
         axios.get(API_URL+'/main/get_user_by_no_hp', {params:{no_hp: '62'+this.state.phone}})
         .then(result => {

            if (result.data.status === 'user_not_found') {
                auth().verifyPhoneNumber('+62'+this.state.phone)
                    .on('state_changed', res => {
                          const verif = res.verificationId;
                          // alert(JSON.stringify(res))
                          if (res.state === 'verified') {
                            console.log(res.state)
                            axios.get(API_URL+'/main/get_user_by_no_hp', {params:{no_hp: '62'+this.state.phone}})
                            .then(result => {
                                        this.setState({loading: false})
                                        AsyncStorage.setItem('@no_hp', '62'+this.state.phone)
                                        if (result.data.status === 'user_not_found') {
                                            AsyncStorage.setItem('@registered', 'false')
                                            this.props.navigation.navigate('Permission')  
                                        }else{
                                            AsyncStorage.setItem('@registered', 'true')
                                            AsyncStorage.setItem('@no_hp', result.data.data[0].no_hp)
                                            AsyncStorage.setItem('@nama', result.data.data[0].nama)
                                            AsyncStorage.setItem('@point', result.data.data[0].point)
                                            AsyncStorage.setItem('@id', result.data.data[0].id_user)
                                            this.props.navigation.navigate('Permission')
                                        }
                            })
                            .catch(error => {
                                alert(error)
                            });
                        }else if (res.state === 'sent') {
                            // setTimeout(() => {
                                this.setState({loading: false}) 
                        }else if (res.state === 'timeout') {
                            // alert("Phone number doesn't exist")
                        }
                    })
                    .catch(error => {
                      this.setState({
                      loading: false
                    });
                      alert(error)
                    });
            }else{
                // alert(JSON.stringify(result))
                if (result.data.data[0].disabled === 'true') {
                    this.setState({
                        loading: false
                    })
                    Alert.alert('Akun Anda Telah Diblokir')
                }else{
                    Alert.alert(
                      '',
                      'Data Anda Sudah Terdaftar',
                      [
                        {text: 'OK'},
                      ]
                    );
                    auth().verifyPhoneNumber('+62'+this.state.phone)
                    .on('state_changed', res => {
                          const verif = res.verificationId;
                          // alert(JSON.stringify(res))
                          if (res.state === 'verified') {
                            console.log(res.state)
                            axios.get(API_URL+'/main/get_user_by_no_hp', {params:{no_hp: '62'+this.state.phone}})
                            .then(result => {
                                        this.setState({loading: false})
                                        AsyncStorage.setItem('@no_hp', '62'+this.state.phone)
                                        if (result.data.status === 'user_not_found') {
                                            this.props.navigation.navigate('Permission')
                                            AsyncStorage.setItem('@registered', 'false')
                                        }else{
                                            AsyncStorage.setItem('@registered', 'true')
                                            AsyncStorage.setItem('@no_hp', result.data.data[0].no_hp)
                                            AsyncStorage.setItem('@nama', result.data.data[0].nama)
                                            AsyncStorage.setItem('@point', result.data.data[0].point)
                                            AsyncStorage.setItem('@id', result.data.data[0].id_user)
                                            AsyncStorage.setItem('@registered', 'true')
                                            AsyncStorage.setItem('@registered', 'true')
                                            this.props.navigation.navigate('Permission')
                                        }
                                    // }
                                    // alert('Phone number already exist')
                                    // simpan data dari api di asyncStorage
                            })
                            .catch(error => {
                                alert(error)
                            });
                        }else if (res.state === 'sent') {
                            // setTimeout(() => {
                                this.setState({loading: false}) 
                            //     alert("If this phone number is inside this phone, Then try again later, The server is busy")
                            //      }, 15000)
                            // console.warn(res.state)
                            // AsyncStorage.setItem('@no_hp', '62'+this.state.phone)
                            // axios.get(API_URL+'/main/get_user_by_no_hp', {params:{no_hp: '62'+this.state.phone}})
                            // .then(result => {
                            //     if (result.data.status === 'user_not_found') {
                            //         this.props.navigation.navigate('Otp', {
                            //             confirm: verif,
                            //             phone: '62'+this.state.phone
                            //         })
                            //         AsyncStorage.setItem('@registered', 'false')
                            //     }else{
                            //         if (result.data.data[0].disabled === 'true') {
                            //             alert('Your account has been blocked')
                            //         }else{
                            //             AsyncStorage.setItem('@registered', 'true')
                            //             AsyncStorage.setItem('@no_hp', result.data.data[0].no_hp)
                            //             AsyncStorage.setItem('@nama', result.data.data[0].nama)
                            //             AsyncStorage.setItem('@point', result.data.data[0].point)
                            //             this.props.navigation.navigate('Otp', {
                            //                 confirm: verif,
                            //                 phone: '62'+this.state.phone
                            //             })
                            //             AsyncStorage.setItem('@registered', 'true')
                            //         }
                            //     }
                            // })
                            // .catch(e => {
                            //     alert(JSON.stringify(e))
                            // })
                        }else if (res.state === 'timeout') {
                            // alert(JSON.stringify(res))
                            this.setState({
                              loading: false
                            });
                        }
                    })
                    .catch(e => {
                      this.setState({
                      loading: false
                    });
                      alert(e)
                      // alert("Unusual activity detected. Try again later")
                    });
                }
            }
         });
        //     this.setState({
        //       btn_enable: true
        //     })
        //     auth().signInWithPhoneNumber('+62'+this.state.phone).then((data) => {
        //     axios.get(API_URL+'/main/check_no_hp', {params:{no_hp: '62'+this.state.phone}})
        //     .then(respon => {
        //         if (respon.data.status === 'tidak_ada') {    
        //             AsyncStorage.setItem('@no_hp', '62'+this.state.phone)
        //             this.props.navigation.navigate('Otp', {
        //                 confirm: data,
        //                 phone: '62'+this.state.phone
        //             })
        //             AsyncStorage.setItem('@registered', 'false')
        //         }else{
        //             axios.get(API_URL+'/main/get_user_by_no_hp', {params:{no_hp: '62'+this.state.phone}})
        //             .then(result => {
        //                 AsyncStorage.setItem('@registered', 'true')
        //                 AsyncStorage.setItem('@no_hp', result.data.data[0].no_hp)
        //                 AsyncStorage.setItem('@nama', result.data.data[0].nama)
        //                 AsyncStorage.setItem('@point', result.data.data[0].point)
        //                 this.props.navigation.navigate('Otp', {
        //                     confirm: data,
        //                     phone: '62'+this.state.phone
        //                 })
        //             })
        //             .catch(e => {
        //                 alert(JSON.stringify(e))
        //             })
        //             // alert('Phone number already exist')
        //             // simpan data dari api di asyncStorage
        //         }
        //     })
        //     .catch(error => {
        //         alert(JSON.stringify(error))
        //     })

        // }).catch((error) => {
        //   alert(JSON.stringify(error))
        //   this.setState({
        //         btn_enable: true
        //     })
        // })
      }
  }
    

    render() {
        if (this.state.loading) {
            return (
            <View style={[styles.container_loading, styles.horizontal_loading]}>
              <ActivityIndicator size="large" color="#0000ff" />
            </View>
            )
        }else{
            
        return (
            <ScrollView>
                <View style={{ flex: 3 }}>
                    <ImageBackground source={require('../asset/login-header.png')} style={[p.alignCenter, b.w100, { height: vs(315) }]}>
                        <View style={{ marginTop: vs(40) }}>
                            <Image source={require('../asset/login-logo-white.png')} style={{ width: xs(150), height: xs(150) }} resizeMode={'stretch'} />
                        </View>
                    </ImageBackground>
                </View>
                <View style={[b.px4, b.pt4, { flex: 3 }]}>
                    <View style={[ b.container ]}>
                        <Text style={[c.blue, f.bold, f._24, p.textCenter, b.mt4]}> Masukkan Nomor Telepon Anda </Text>
                        <View style={[p.row, b.roundedHigh, p.wrap, p.justifyBetween, p.alignCenter, { backgroundColor: '#e6e6e6', marginTop: xs(30), marginBottom: xs(30) }]}>
                            <View style={{ width: xs(60) }}>
                                <Image source={require('../asset/login-phone-icon.png')} style={{ width: xs(50), height: xs(50) }} />
                            </View>
                            <Text style={[f._20]} >+62</Text>
                            <TextInput
                                keyboardType={'numeric'}
                                placeholder="8xxxxx"
                                onChangeText={(phones) => {
                                    let phone = this.state.phone
                                    phone = phones
                                    this.setState({ phone })
                                }}
                                value={this.state.phone}
                                style={{ width: xs(210), fontSize: xs(20) }} />
                        </View>
                        <Text style={[p.textCenter, f._20]}> OTP akan mengirim ke nomor telepon ini </Text>
                    </View>

                </View>
                <View style={[ b.pt4, { flex: 1 }]}>
                    <ImageBackground source={require('../asset/login-footer.png')} style={[p.alignCenter, b.w100, { height: vs(120) }]}>
                        <View style={[b.container, p.center, { marginTop: vs(35) }]}>
                            <TouchableOpacity disabled={this.state.btn_enable} onPress={() => this.sendCode()} style={[b.py2, b.roundedHigh, { backgroundColor: light, paddingHorizontal: xs(30) }]}>
                                <Text style={[f.bold, c.blue, f._20]}> Get OTP </Text>
                            </TouchableOpacity>
                        </View>
                    </ImageBackground>
                </View>
            </ScrollView>
        );
        }
    }
}
const styles = StyleSheet.create({
  // loading centered screenn
  container_loading: {
    flex: 1,
    justifyContent: "center"
  },
  horizontal_loading: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },
});