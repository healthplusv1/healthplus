import React, { Component } from 'react'
import { Text, View, TextInput, TouchableOpacity, StyleSheet, ActivityIndicator } from 'react-native'
import { xs, vs } from '../utils/Responsive'
import { p, f, c, b } from '../utils/StyleHelper'
import { blue, light } from '../utils/Color'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import FastImage from 'react-native-fast-image'
import app from '@react-native-firebase/app'
import iid from '@react-native-firebase/iid'
import analytics from '@react-native-firebase/analytics'
import { API_URL } from 'react-native-dotenv'

export default class InputName extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            nama: '',
            id_user: ''
        }
    }

    async componentDidMount() {
        let iid = await app.iid().getToken()
        this.setState({iid:iid})

        const id_login = await AsyncStorage.getItem('@id_login')
        axios.get(API_URL+'/main/get_user_by_id_login', {params: {
            id_login: id_login
        }})
        .then(res => {
            console.log(res.data.data[0].id_user)
            this.setState({
                id_user: res.data.data[0].id_user
            })
        })
        
        await analytics().logEvent('create_account', {
            item: 'user_id'
        })
        await analytics().setUserProperty('create_account_from_user_id', 'null')
    }

    async submit() {
        this.setState({ loading: true })
        const phone_number = await AsyncStorage.getItem('@phone')
        axios.get(API_URL+'/main/insert_phone_number', {params: {
            nama: this.state.nama,
            no_hp: phone_number,
        }})
        .then(async resp => {
            await analytics().logEvent('created_account', {
                method: 'PHONE_NUMBER',
                user_id: this.state.id_user
            })
            await analytics().setUserProperty('user_created_account_from_phone_number', 'null')
            // console.log(resp.data.id_login)
            if (resp.data.status === 'true') {
                AsyncStorage.setItem('@nama', this.state.nama)
                AsyncStorage.setItem('@id_login', resp.data.id_login)
                axios.get(API_URL+'/main/update_id_notification', {params: {
					id_login: resp.data.id_login,
					id_notification: this.state.iid
				}})
                this.props.navigation.push('KTP')
            }
            this.setState({ loading: false })
        })
        .catch(err => {
            console.log('Insert Error => '+err)
        })
    }

    render() {
        return (
            <>
                {this.state.loading ?
                    <View style={[ styles.container_loading, styles.horizontal_loading ]}>
                        <ActivityIndicator size='large' color={blue} />
                    </View>
                    :
                    null
                }
                <FastImage source={require('../asset/background.jpg')} style={[ p.center, { width: '100%', height: '100%' }]}>
                    <View style={[ b.roundedHigh, { width: xs(300), height: xs(500), backgroundColor: light, alignItems: 'center' }]}>
                        <FastImage source={require('../asset/HEALTHPLUS.png')} style={{ width: xs(125), height: xs(125), marginTop: vs(30) }} />
                        <Text style={[ f._20, b.mt4, { fontFamily: 'lineto-circular-pro-bold' }]}>Masukkan Nama Lengkap</Text>
                        <View style={[ p.row, b.rounded, b.mt4, p.center, { width: xs(230), height: xs(50), borderWidth: 1 }]}>
                            <TextInput
                                style={{ width: xs(200) }}
                                placeholder='Masukkan Nama Anda'
                                onChangeText={(text) => this.setState({ nama: text })}
                                value={this.state.nama}
                            />
                        </View>
                        <TouchableOpacity onPress={() => this.submit()} style={[ p.center, b.mt4, b.roundedHigh, { backgroundColor: blue }]}>
                            <Text style={[ b.mt2, b.mb2, b.ml4, b.mr4, c.light, f._16, { fontFamily: 'lineto-circular-pro-bold' }]}>Submit</Text>
                        </TouchableOpacity>
                    </View>
                </FastImage>
            </>
        )
    }
}

const styles = StyleSheet.create({
    container_loading: {
        flex: 1,
        position:'absolute',
        zIndex: 2,
        width: '100%',
        height: '100%',
        justifyContent: "center",
        backgroundColor: 'rgba(0,0,0,0.7)'
    },
    horizontal_loading: {
        flexDirection: "row",
        justifyContent: "space-around",
        padding: 10
    }
})