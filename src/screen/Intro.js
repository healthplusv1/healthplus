import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ImageBackground, Image, BackHandler } from 'react-native';
import { b, p, f, c } from '../utils/StyleHelper';
import { vs, xs } from '../utils/Responsive';
import { blue, light } from '../utils/Color';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import Swiper from "react-native-web-swiper";
import AsyncStorage from '@react-native-community/async-storage'
import FastImage from 'react-native-fast-image'


export default class Intro extends Component {

    _didFocusSubscription;
    _willBlurSubscription;

    constructor(props) {
        super(props);
        this._didFocusSubscription = props.navigation.addListener('didFocus', payload =>
          BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
        );
        this.state = {
        };
    }

    componentWillUnmount(){
        this._didFocusSubscription && this._didFocusSubscription.remove();
        this._willBlurSubscription && this._willBlurSubscription.remove();
    }

    componentDidMount(){
        this._willBlurSubscription = this.props.navigation.addListener('willBlur', payload =>
          BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
        );
    }

    nextScreen(){
        // AsyncStorage.setItem('@login', 'true')
        this.props.navigation.navigate('Permission')
    }

    handleBackPress = () => {
        BackHandler.exitApp();
        return true;
    };

    render() {
        return (
            <View style={{ width: '100%', height: '100%', backgroundColor: '#000' }}>
                <Swiper showsButtons={false}>
                    <View style={{ width: '100%', height: ('100%') }}>
                        <View style={{ width: '100%', height: hp('15%') }}>
                            <FastImage source={require('../asset/iconstambahan-04.png')} style={{ width: '100%', height: '100%' }} />
                        </View>

                        <View style={[ p.center, { width: '100%', height: hp('42.5%') }]}>
                            <FastImage source={require('../asset/iconsplash-01.png')} style={{ width: xs(310), height: xs(250) }} />
                        </View>

                        <FastImage source={require('../asset/iconstambahan-011.png')} style={{ width: '100%', height: hp('42.5%') }}>
                            <View style={[ p.center, { width: '100%', marginTop: vs(80) }]}>
                                <View style={[ b.mt4, { width: xs(200) }]}>
                                    <Text style={[ p.textCenter, c.light, f._20, { fontFamily: 'lineto-circular-pro-bold' }]}>Buat Liga</Text>
                                    <Text style={[ p.textCenter, c.light ]}>Buat liga dan bergabunglah dengan liga untuk bermain dengan temanmu</Text>
                                </View>
                            </View>
                        </FastImage>
                    </View>

                    <View style={{ width: '100%', height: '100%' }}>
                        <View style={{ width: '100%', height: hp('15%') }}>
                            <FastImage source={require('../asset/iconstambahan-04.png')} style={{ width: '100%', height: '100%' }} />
                        </View>

                        <View style={[ p.center, { width: '100%', height: hp('42.5%') }]}>
                            <FastImage source={require('../asset/iconsplash-02.png')} style={{ width: xs(350), height: xs(250) }} />
                        </View>

                        <FastImage source={require('../asset/iconstambahan-022.png')} style={{ width: '100%', height: hp('42.5%') }}>
                            <View style={[ p.center, { width: '100%', marginTop: vs(80) }]}>
                                <View style={[ b.mt4, { width: xs(200) }]}>
                                    <Text style={[ p.textCenter, c.light, f._20, { fontFamily: 'lineto-circular-pro-bold' }]}>Ajak Temanmu</Text>
                                    <Text style={[ p.textCenter, c.light ]}>Ajak temanmu untuk bergabung di ligamu dan share di social mediamu</Text>
                                </View>
                            </View>
                        </FastImage>
                    </View>

                    <View style={{ width: '100%', height: '100%' }}>
                        <View style={{ width: '100%', height: hp('15%') }}>
                            <FastImage source={require('../asset/iconstambahan-04.png')} style={{ width: '100%', height: '100%' }} />
                        </View>

                        <View style={[ p.center, { width: '100%', height: hp('42.5%') }]}>
                            <FastImage source={require('../asset/iconsplash-03.png')} style={{ width: xs(350), height: xs(250) }} />
                        </View>

                        <FastImage source={require('../asset/iconstambahan-011.png')} style={{ width: '100%', height: hp('42.5%') }}>
                            <View style={[ p.center, { width: '100%', marginTop: vs(80) }]}>
                                <View style={[ b.mt4, { width: xs(250) }]}>
                                    <Text style={[ p.textCenter, c.light, f._20, { fontFamily: 'lineto-circular-pro-bold' }]}>Memenangkan Poin</Text>
                                    <Text style={[ p.textCenter, c.light ]}>Bergabunglah di dalam tantangan dan bermain bersama temanmu, untuk memenangkan poin</Text>
                                </View>
                            </View>
                            <View style={[ p.center, b.mt4 ]}>
                                <TouchableOpacity onPress={() => this.nextScreen()} style={[b.py1, b.roundedHigh, { backgroundColor: light, paddingHorizontal: xs(30) }]}>
                                    <Text style={[ c.blue, f._20, { fontFamily: 'lineto-circular-pro-bold' }]}>Ayo Mulai</Text>
                                </TouchableOpacity>
                            </View>
                        </FastImage>

                    </View>
                </Swiper>
            </View>
        );
    }
}
