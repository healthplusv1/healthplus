import React, { Component } from 'react';
import { AppRegistry, View, Text, Image, TouchableOpacity, ScrollView, ImageBackground, PermissionsAndroid, Alert } from 'react-native';
import { b, p, f, c } from '../utils/StyleHelper';
import { vs, xs } from '../utils/Responsive';
import { blue, light, grey, softBlue } from '../utils/Color';
import ConnectivityManager from 'react-native-connectivity-status';
import AsyncStorage from '@react-native-community/async-storage'
import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';
import FastImage from 'react-native-fast-image'




const onLocationEnablePressed = () => {
  if (Platform.OS === 'android') {
    RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({interval: 10000, fastInterval: 5000})
    .then(data => {
      if (data === 'already-enabled') {
        this.setState({location: 'true'})
        Alert.alert('', 'GPS Sudah Menyala');
      }
    }).catch(err => {
      // The user has not accepted to enable the location services or something went wrong during the process
      // "err" : { "code" : "ERR00|ERR01|ERR02", "message" : "message"}
      // codes : 
      //  - ERR00 : The user has clicked on Cancel button in the popup
      //  - ERR01 : If the Settings change are unavailable
      //  - ERR02 : If the popup has failed to open
      console.log("Off")
    });
  }
}

export async function requestLocationPermission() 
{
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
    )
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log("You can use the location");
    } else {
      console.log("location permission denied");
    }
  } catch (err) {
    console.warn(err)
  }
}

export default class Permission extends Component {
    constructor(props) {
        super(props);
        this.state = {
            lang_selected: 0,
            location: 'false'
        };
        this.push_notification = this.push_notification.bind(this)
    }

    async onLocationEnablePressed() {
      if (Platform.OS === 'android') {
        RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({interval: 10000, fastInterval: 5000})
        .then(data => {
          if (data === 'already-enabled') {
            // Alert.alert('', 'GPS Already Enabled')
            this.setState({location: 'true'})
          }
          if (data === 'enabled') {
            this.setState({location: 'true'})
          }
        }).catch(err => {
            this.setState({location: 'false'})
          // The user has not accepted to enable the location services or something went wrong during the process
          // "err" : { "code" : "ERR00|ERR01|ERR02", "message" : "message"}
          // codes : 
          //  - ERR00 : The user has clicked on Cancel button in the popup
          //  - ERR01 : If the Settings change are unavailable
          //  - ERR02 : If the popup has failed to open
          // console.log("Off")
          // alert(err)
        });
      }
    }

    // async componentDidMount() {
    //     await requestLocationPermission();
    //     await this.onLocationEnablePressed();
    // }

    push_notification(On) {
        this.setState({
            lang_selected: on
        })
    }
    

    render() {
        return (
            <ImageBackground source={require('../asset/permission-blue-bg.png')} style={{ flex:1 }}>
              <ScrollView>
                <View style={[b.container, b.p2, p.column]}>
                    <Text style={[ p.textCenter, f._24, c.light, { fontFamily: 'lineto-circular-pro-bold' }]}> Siapkan Izin </Text>
                    <View style={[p.center, { height: vs(200) }]}>
                        <FastImage source={require('../asset/permission-icon.png')} style={{ width: '90%', height: '70%' }} resizeMode={'stretch'} />
                    </View>

                    <View style={{ width: xs(340) }}>
                      <View style={[p.row]}>
                          <Text style={[ b.ml2, c.light, f._16, { fontFamily: 'lineto-circular-pro-bold' }]}> HealthPlus </Text>
                          <Text style={[c.light, f._16]}> akan memerlukan beberapa </Text>
                      </View>
                      <View style={[p.row]}>
                          <Text style={[b.ml2, c.light, f._16]}> izin untuk memberikan Anda hadiah </Text>
                      </View>
                      <View style={[p.row]}>
                          <Text style={[b.ml2, c.light, f._16]}> di rumah. Silahkan periksa </Text>
                          <Text style={[c.light, f._16, { fontFamily: 'lineto-circular-pro-bold' }]}> "Izinkan" </Text>
                          <Text style={[c.light, f._16]}> atau </Text>
                      </View>
                      <View style={[p.row]}>
                          <Text style={[b.ml2, c.light, f._16, { fontFamily: 'lineto-circular-pro-bold' }]}> "Ya" </Text>
                          <Text style={[c.light, f._16]}> untuk </Text>
                          <Text style={[c.light, f._16]}> akses izin berikut dibawah ini. </Text>
                      </View>
                    </View>

                    <Text style={[b.ml2, c.light, f._20, b.mt4, { fontFamily: 'lineto-circular-pro-bold' }]}> Status Izin Aplikasi saat ini </Text>

                    {/* <TouchableOpacity onPress={() => this.onLocationEnablePressed()} style={[b.py2, b.rounded, b.px4, p.row, b.mt4, { backgroundColor: light }]}>
                        <Text style={[ b.ml4, f._20 ]}> Izin Lokasi </Text>
                        <View style={[p.center, { width: xs(20) }]}>
                            {
                                this.state.location === 'false' ?
                                <FastImage source={require('../asset/cross.jpg')} style={[b.ml3, { width: xs(15), height: xs(15) }]} />
                                :
                                <FastImage source={require('../asset/permission-green-checklist.png')} style={[b.ml3, { width: xs(15), height: xs(15) }]} />
                            }
                        </View>
                    </TouchableOpacity> */}
                    <TouchableOpacity style={[b.py2, b.rounded, b.px4, p.row, b.mt4, { backgroundColor: light }]}>
                        <Text style={[ b.ml4, f._20 ]}> Notifikasi </Text>
                        <View style={[p.center, { width: xs(20) }]}>
                            {
                                this.state.lang_selected == 0 &&
                                <FastImage source={require('../asset/permission-green-checklist.png')} style={[b.ml3, { width: xs(15), height: xs(15) }]} />
                            }
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Akun')} style={[p.center, b.py2, b.rounded, b.mt4, { backgroundColor: blue }]}>
                        <Text style={[ c.light, f._20, { fontFamily: 'lineto-circular-pro-bold' }]}> Bagus, siap </Text>
                    </TouchableOpacity>
                </View>
              </ScrollView>
            </ImageBackground>
        );
    }
}