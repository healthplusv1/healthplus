import React, { Component } from 'react';
import { Text, View, TouchableOpacity, Image, ImageBackground, TextInput, ActivityIndicator, Alert, StyleSheet } from 'react-native'
import { b, p, f, c } from '../utils/StyleHelper'
import { vs, xs } from '../utils/Responsive'
import { blue, light, grey, softBlue } from '../utils/Color'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import app from '@react-native-firebase/app'
import iid from '@react-native-firebase/iid'
import analytics from '@react-native-firebase/analytics'
import { API_URL, API_LOCAL } from 'react-native-dotenv'

export default class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      displayName: '',
      email: '',
      password: '',
      isLoading: false
    };
  }

  async componentDidMount() {
    let iid = await app.iid().getToken()
    this.setState({iid:iid})
    
    await analytics().logEvent('create_account', {
      item: 'Email'
    })
    await analytics().setUserProperty('user_create_account', 'null')
  }

  updateInputVal = (val, prop) => {
    const state = this.state;
    state[prop] = val;
    this.setState(state);
  }

  registerUser = async () => {
    if(this.state.displayName === '' && this.state.email === '' && this.state.password === '') {
      Alert.alert('Enter details to signup!')
    } else {
      this.setState({
        isLoading: true,
      })
      axios.get(API_LOCAL+'/main/insert_register', {params:{
        id_notification: this.state.iid,
        nama: this.state.displayName,
        email: this.state.email,
        password: this.state.password
      }})
      .then(async response => {
        this.setState({
          isLoading: false,
        })
        // console.log(response.data.id_login)
        AsyncStorage.setItem('@nama', this.state.displayName)
        AsyncStorage.setItem('@id_login', response.data.id_login)
        if (response.data.status === 'user_already_exist') {
          Alert.alert("Data sudah terdaftar, silahkan login")
        } else {
          console.log("Data baru ditambahkan")
          await analytics().logEvent('created_account', {
            item: 'Email'
          })
          await analytics().setUserProperty('user_created_account_from_email', 'null')
          this.props.navigation.navigate('KTP')
        }
      })
    }  
  }

  validate = (text) => {
    console.log(text);
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      // console.log("Email is Not Correct");
      this.setState({ email: text, email_valid: 'Email is Not Correct' })
      return true;
    }
    else {
      this.setState({ email: text, email_valid: 'Email is Correct' })
      // console.log("Email is Correct");
    }
  }

  render() {
    return (
      <>
        {this.state.isLoading === true ?
          <View style={[ styles.container_loading, styles.horizontal_loading ]}>
            <ActivityIndicator size="large" color="blue"/>
          </View>
          :
          null
        }
        <ImageBackground source={require('../asset/back_sign.png')} style={[ b.container, p.center, { width: '100%', height: '100%' }]}>
          <View style={[ b.roundedHigh, b.shadowHigh, p.center, { marginTop: vs(100), width: xs(300), height: xs(200) }]}>
            <TextInput style={{ width: xs(300) }}
              underlineColorAndroid = '#b8b8b8'
              placeholder = "Nama"
              placeholderTextColor = { c.grey }
              value={this.state.displayName}
              onChangeText={(val) => this.updateInputVal(val, 'displayName')}/>

            <TextInput style={{ width: xs(300) }}
              underlineColorAndroid = '#b8b8b8'
              placeholder = "Email"
              placeholderTextColor = { c.grey }
              autoCapitalize = "none"
              keyboardType={"email-address"}
              value={this.state.email}
              onChangeText={(text) => this.validate(text)}/>

            {this.state.email_valid === 'Email is Not Correct' ?
              <View style={{ width: xs(300) }}>
                <Text style={{ color: 'red', marginLeft: vs(4) }}>Email salah</Text>
              </View>
              :
              null
            }
            
            <TextInput style={{ width: xs(300) }}
              underlineColorAndroid = '#b8b8b8'
              placeholder = "Password"
              placeholderTextColor = { c.grey }
              autoCapitalize = "none"
              secureTextEntry= {true}
              value={this.state.password}
              onChangeText={(val) => this.updateInputVal(val, 'password')}/>
          </View>
          <TouchableOpacity onPress={this.registerUser} style={[ b.roundedHigh, p.center, { marginTop: vs(25), width: xs(100), height: xs(40), backgroundColor: blue }]}>
              <Text style={[ c.light ]}> SIGN UP </Text>
          </TouchableOpacity>
          <View style={[ p.row, p.center, { marginTop: vs(50), width: '100%' }]}>
            <Text style={{ color: '#b8b8b8', fontFamily: 'lineto-circular-pro-bold' }}> SUDAH PUNYA AKUN? </Text>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('Email')}>
                <Text style={[ c.blue, { fontFamily: 'lineto-circular-pro-bold' }]}> SIGN IN </Text>
            </TouchableOpacity>
          </View>
        </ImageBackground>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container_loading: {
    position:'absolute',
    zIndex: 1,
    width: '100%',
    height: '100%',
    justifyContent: "center",
    backgroundColor: 'rgba(0,0,0,0.7)'
  },

  horizontal_loading: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },
})