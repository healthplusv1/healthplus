import React, { Component, useState } from 'react';
import { StyleSheet, View, Text, ImageBackground, Image, KeyboardAvoidingView, I18nManager, TouchableWithoutFeedback, TouchableOpacity, TextInput, ActivityIndicator } from 'react-native';
import { b, p, f, c } from '../utils/StyleHelper';
import { vs, xs } from '../utils/Responsive';
import { blue, light, grey, softBlue } from '../utils/Color';
import axios from 'axios'
import auth from '@react-native-firebase/auth';
import Confirmationcode from '../component/Confirmationcode';
import ConnectivityManager from 'react-native-connectivity-status';
import AsyncStorage from '@react-native-community/async-storage'



export default class Otp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            timer: 4,
            code: '',
            confirmation: this.props.navigation.getParam('confirm'),
            value: null,
            loading: false
        };
    }

    componentDidMount() {
        this.interval = setInterval(
            () => this.setState((prevState) => ({ timer: prevState.timer - 1 })),
            1000
        );
    }

    verificationCode = async () => {
        this.setState({ loading: true })
        try {
          const value = await AsyncStorage.getItem('@kode')
          if(value !== null) {
            // this.setState({ loading: false })
            // value previously stored
            // this.state.confirmation.confirm(value)
            // alert(value)
            const verif = auth.PhoneAuthProvider.credential(this.props.navigation.getParam('confirm'), value)
                auth().signInWithCredential(verif)
                .then(res => {
                    this.setState({ loading: false })
                    this.props.navigation.navigate('Permission')
                })
                .catch(e => {
                    this.setState({ loading: false })
                    alert(e)
                })
            }
            // alert(JSON.stringify(this.state.confirmation))
        } catch(e) {
            this.setState({ loading: false })
            alert(JSON.stringify(e))
        }
    }

    sendCodeAgain() {
        this.setState({ loading: true })
        if (this.state.timer > 0) {
            alert('Please Wait till the timer end')
        } else {
            this.reload_countDown()
            const phone = this.props.navigation.getParam('phone')
            auth().signInWithPhoneNumber('+'+phone)
                .then((data) => {
                    this.setState({ loading: false })
                    alert('Sended Again')
                    this.setState({ confirmation: data })
                })
                .catch(error => {
                    this.setState({ loading: false })
                    alert(error)
                })
        }
    }

    componentDidUpdate() {
        if (this.state.timer === 0) {
            clearInterval(this.interval);
        }
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    reload_countDown() {
        this.setState({
            timer: 4
        })
        this.interval = setInterval(
            () => this.setState((prevState) => ({ timer: prevState.timer - 1 })),
            1000
        );
    }

    getconfirm = (confirm) => {
        if (this.state.confirmasi == null) {
            this.setState({
                confirmasi: confirm
            })

        }

    }

    render() {
        if (this.state.loading) {
            return (
            <View style={[styles.container_loading, styles.horizontal_loading]}>
              <ActivityIndicator size="large" color="#0000ff" />
            </View>
            )
        }else{
        return (
            <>
                <View style={{ flex: 3 }}>
                    <ImageBackground source={require('../asset/login-header.png')} style={[p.alignCenter, b.w100, { height: vs(300) }]}>
                        <View style={[b.mt4]}>
                            <Image source={require('../asset/login-logo-white.png')} style={{ width: xs(150), height: xs(150) }} resizeMode={'stretch'} />
                        </View>
                    </ImageBackground>
                </View>
                <View style={[b.px4, b.pt4, { flex: 3 }]}>
                    <View style={[b.pt4, b.container, { height: vs(200) }]}>
                        <Text style={[c.blue, f.bold, f._24, p.textCenter, b.mt4, b.mb3]}>Enter OTP</Text>
                        <Text style={[p.textCenter, f._16]}>We have sent an OTP to your phone number</Text>
                        <View style={[p.alignCenter, { marginTop: xs(50), marginBottom: xs(20) }]}>
                            <Confirmationcode confirm={this.getconfirm(this.props.navigation.getParam('confirm'))} />
                        </View>
                        <View style={[p.row, p.center]}>
                            <Text style={[p.textCenter, f._16, c.blue]}>Request new OTP in</Text>
                            <Text style={[p.textCenter, f._16, c.blue]}> {this.state.timer} secs </Text>
                            <TouchableOpacity onPress={() => this.sendCodeAgain()}>
                                <Text style={[p.textCenter, f._16, c.blue, { textDecorationLine: 'underline' }]}>here</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                </View>
                <View style={{ flex: 1 }}>
                    <ImageBackground source={require('../asset/login-footer.png')} style={[p.alignCenter, b.w100, { height: vs(120) }]}>
                        <View style={[b.container, p.center]}>
                            <TouchableOpacity onPress={() => this.verificationCode()} style={[b.py2, b.roundedHigh, { backgroundColor: light, paddingHorizontal: xs(30) }]}>
                                <Text style={[f.bold, c.blue, f._20]}>Verify</Text>
                            </TouchableOpacity>
                        </View>
                    </ImageBackground>
                </View>
            </>
        );}
    }
}
const styles = StyleSheet.create({
  // loading centered screenn
  container_loading: {
    flex: 1,
    justifyContent: "center"
  },
  horizontal_loading: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },
});