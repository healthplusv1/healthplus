import React, { Component } from 'react';
import { View, StyleSheet, Alert, StatusBar, Text } from 'react-native';
import FastImage from 'react-native-fast-image'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import LinearGradient from 'react-native-linear-gradient'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'
import { API_URL, API_LOCAL } from 'react-native-dotenv'

export default class SplashScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lat_hp: '',
      lon_hp: ''
    };
  }
 
  async componentDidMount() {
    // let state = Store.getState();
    // AsyncStorage.clear()
    // auth().signOut()
    // auth().onAuthStateChanged(async user => {
    //   if (user) {
    //     console.log('logged in')
    //     if (await AsyncStorage.getItem('@id_login') !== null) {
    //       this.props.navigation.navigate('Home')
    //     } else if (await AsyncStorage.getItem('@phone') !== null) {
    //       this.props.navigation.push('InputName')
    //     }
    //   } else {
    //     console.log('not logged in')
    //   }
    // })
    // try{
    //   // AsyncStorage.setItem('@no_hp', '6282136506049')
    //   // alert(JSON.stringify(auth().currentUser))
    //   const login = await AsyncStorage.getItem('@login')
    //   if (login !== null) {
    //     let heart = setInterval(() => this.check_radius(), 300000)
    //     setTimeout(() => {
    //       this.props.navigation.navigate('Home')
    //     }, 2000)
    //   } else {
    //     // alert(login )
    //     // let heart = setInterval(() => this.check_radius(), 3000)
    //     setTimeout(() => {
    //       this.props.navigation.navigate('Intro')
    //     }, 2000)
    //   }
    // }catch(e){
    //   console.log(e)
    // }

    const check_login = await AsyncStorage.getItem('@id_login')
    // setTimeout(() => {
    //   this.props.navigation.navigate('Tabs')
    // }, 2000);
    if (check_login !== null){
      axios.get(API_LOCAL+'/main/get_user_by_id_login', {params: {
        id_login: check_login
      }})
      .then(resp => {
        if (resp.data.status === 'true') {
          setTimeout(() => {
            this.props.navigation.navigate('Tabs')
          }, 2000)
        }
      })
      .catch(err => {
        console.log(err)
      })
    } else {
      setTimeout(() => {
        this.props.navigation.navigate('Intro')
      }, 2000)
    }
  }

  render() {
    return (
      <>
        <View style={styles.container}>
          <FastImage source={require('../asset/splashscreen.png')} style={{ width: '100%', height: '100%', alignItems: 'center' }} />
        </View>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
})