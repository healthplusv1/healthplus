import * as firebase from 'firebase'

var firebaseConfig = {
    apiKey: "AIzaSyAVe2BmuzqYsxn4hQUuwMKcx_6iWVyeDEs",
    authDomain: "healthplus-healthplus.firebaseapp.com",
    databaseURL: "https://healthplus-healthplus.firebaseio.com",
    projectId: "healthplus-healthplus",
    storageBucket: "healthplus-healthplus.appspot.com",
    messagingSenderId: "931083708186",
    appId: "1:931083708186:web:ebed7dbae9aab19ae8aea3",
    measurementId: "G-EMFDCQSBXD"
};
firebase.initializeApp(firebaseConfig);

export default firebase;