import React from 'react'
import { Text, View, TouchableOpacity, Image, ImageBackground, Alert } from 'react-native'
import { b, p, f, c } from '../utils/StyleHelper'
import { vs, xs } from '../utils/Responsive'
import { blue, light, grey, softBlue } from '../utils/Color'
import Svg from '../utils/Svg'
import { GoogleSignin, GoogleSigninButton, statusCodes } from '@react-native-community/google-signin';
import { AccessToken, GraphRequest, GraphRequestManager, LoginManager } from 'react-native-fbsdk';
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'
import FastImage from 'react-native-fast-image'
import app from '@react-native-firebase/app'
import iid from '@react-native-firebase/iid'
import analytics from '@react-native-firebase/analytics'
import { API_URL } from 'react-native-dotenv'

export default class Akun extends React.Component {

	constructor(props){
		super(props);
		this.state ={
			name: '',
			email: '',
			no_hp: '',
			id: '',
			id_email: '',
			userInfo: {},
		}
	}

	getInfoFromToken = token => {
	    const PROFILE_REQUEST_PARAMS = {
	      fields: {
	        string: 'id,name,first_name,last_name',
	      },
	    };
	    const profileRequest = new GraphRequest(
	      '/me',
	      {token, parameters: PROFILE_REQUEST_PARAMS},
	      (error, user) => {
	        if (error) {
	          Alert.alert('login info has error: ' + error);
	          Alert.alert('Error fetching data: ' + error.toString());
	        } else {
	          this.setState({userInfo: user});

	          // alert(JSON.stringify(user));
	          // console.log('result:', user);
	          axios.get(API_URL+'/main/insert_fb', {params:{
		      	nama: this.state.userInfo.name,
		      	id_login: this.state.userInfo.id
		      }})
		      .then(async response => {
				await analytics().logEvent('create_account', {
					item: 'FACEBOOK'
				})
				await analytics().setUserProperty('user_get_info_from_facebook', 'null')

		      	// alert(JSON.stringify(response))
		    	AsyncStorage.setItem('@nama', this.state.userInfo.name)
		    	AsyncStorage.setItem('@id_login', this.state.userInfo.id)
		    	if (response.data.status === 'user_already_exist') {
					// Alert.alert("Data sudah ada")
					axios.get(API_URL+'/main/update_id_notification', {params: {
						id_login: this.state.userInfo.id,
						id_notification: this.state.iid
					}})
					.then(() => {
						AsyncStorage.setItem('@login', 'true')
						this.props.navigation.navigate('KTP')
					})
		    	} else {
					// Alert.alert("Data baru ditambahkan")
					axios.get(API_URL+'/main/update_id_notification', {params: {
						id_login: this.state.userInfo.id,
						id_notification: this.state.iid
					}})
					.then(async () => {
						await analytics().logEvent('created_account', {
							item: 'FACEBOOK'
						})
						await analytics().setUserProperty('user_created_account_from_facebook', 'null')
						this.props.navigation.navigate('KTP')
					})
		    	}
		      })
	        }
	      },
	    );
	    new GraphRequestManager().addRequest(profileRequest).start();
	};

	loginWithFacebook = () => {
	    // Attempt a login using the Facebook login dialog asking for default permissions.
	    LoginManager.logInWithPermissions(['public_profile']).then(
	      login => {
	        if (login.isCancelled) {
	          console.log('Login cancelled');
	        } else {
	          AccessToken.getCurrentAccessToken().then(data => {
	            const accessToken = data.accessToken.toString();
				this.getInfoFromToken(accessToken);
	          });
	        }
	      },
	      error => {
	        console.log('Login fail with error: ' + error);
	      },
	    );
	};

	// get_Response_Info = async (error, result) => {
	//     if (error) {
	//       Alert.alert('Error fetching data: ' + error.toString());
	//     } else {
	//       // alert(JSON.stringify(result));
	//       this.setState({
	//       	name: result.name,
	//       	id: result.id
	//       });
	//     }
	// };

	// onLogout = () => {
	// //Clear the state after logout
	// 	this.setState({ name: null, email_fb: null });
	// };

	async componentDidMount() {

		const token = await app.messaging().getToken()
		this.setState({ iid: token })
		console.log(token)

		// GoogleSignin.configure({
		//   // scopes: ['https://www.googleapis.com/auth/'], // what API you want to access on behalf of the user, default is email and profile
		//   webClientId: '931083708186-vapis2bh52aui407o7knluup6fncf5lb.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
		//   // offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
		//   hostedDomain: '', // specifies a hosted domain restriction
		//   loginHint: '', // [iOS] The user's ID, or email address, to be prefilled in the authentication UI if possible. [See docs here](https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd)
		//   forceCodeForRefreshToken: true, // [Android] related to `serverAuthCode`, read the docs link below *.
		//   // accountName: '', // [Android] specifies an account name on the device that should be used
		//   // iosClientId: '<FROM DEVELOPER CONSOLE>', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
		// });
	}

	_signIn = async () => {
	  try {
		GoogleSignin.configure({
			// scopes: ['https://www.googleapis.com/auth/'], // what API you want to access on behalf of the user, default is email and profile
			webClientId: '931083708186-vapis2bh52aui407o7knluup6fncf5lb.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
			// offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
			hostedDomain: '', // specifies a hosted domain restriction
			loginHint: '', // [iOS] The user's ID, or email address, to be prefilled in the authentication UI if possible. [See docs here](https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd)
			forceCodeForRefreshToken: true, // [Android] related to `serverAuthCode`, read the docs link below *.
			// accountName: '', // [Android] specifies an account name on the device that should be used
			// iosClientId: '<FROM DEVELOPER CONSOLE>', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
		});
	    await GoogleSignin.hasPlayServices();
	    const userInfo = await GoogleSignin.signIn();
	    this.setState({
	    	name: userInfo.user.name,
	    	email: userInfo.user.email,
	    	id_email: userInfo.user.id
	    });
	    console.log(userInfo);
	    axios.get(API_URL+'/main/insert_social', {params:{
	      	nama: this.state.name,
	      	email: this.state.email,
	      	id_login: this.state.id_email
	    }})
	    .then(async response => {
			await analytics().logEvent('create_account', {
				item: 'GOOGLE'
			})
			await analytics().setUserProperty('user_get_info_from_google', 'null')

	    	AsyncStorage.setItem('@nama', this.state.name)
	    	AsyncStorage.setItem('@id_login', this.state.id_email)
	    	if (response.data.status === 'user_already_exist') {
				// Alert.alert("Data sudah ada")
				axios.get(API_URL+'/main/update_id_notification', {params: {
					id_login: this.state.id_email,
					id_notification: this.state.iid
				}})
				.then(() => {
					AsyncStorage.setItem('@login', 'true')
					this.props.navigation.navigate('KTP')
					console.log(this.state.iid)
				})
	    	} else {
				// Alert.alert("Data baru ditambahkan")
				axios.get(API_URL+'/main/update_id_notification', {params: {
					id_login: this.state.id_email,
					id_notification: this.state.iid
				}})
				.then(async () => {
					await analytics().logEvent('created_account', {
						item: 'GOOGLE'
					})
					await analytics().setUserProperty('user_created_account_from_google', 'null')
					this.props.navigation.navigate('KTP')
				})
	    	}
	    })
	  } catch (error) {
	    if (error.code === statusCodes.SIGN_IN_CANCELLED) {
	      // user cancelled the login flow
	    } else if (error.code === statusCodes.IN_PROGRESS) {
	      // operation (e.g. sign in) is in progress already
	    } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
	      // play services not available or outdated
	    } else {
	      // some other error happened
	    }
	    console.log(error)
	  }
	};

	render() {
		return (
			<ImageBackground source={require('../asset/background.jpg')} style={[ b.container, p.center, { width: '100%', height: '100%' }]}>
				<View style={[ b.roundedHigh, p.center, { width: xs(300), height: xs(500), backgroundColor: '#FFFFFF' }]}>
					<FastImage source={require('../asset/HEALTHPLUS.png')} style={{ width: xs(125), height: xs(125) }} />
					<Text style={[ f._24, b.mt4, { fontFamily: 'lineto-circular-pro-bold' }]}> Sign In </Text>
					<Text style={[ b.mt3 ]}> Buat akun dengan memilih pilihan di bawah: </Text>
                    <TouchableOpacity onPress={this.loginWithFacebook} style={[ b.roundedLow, b.mt3, { width: xs(250), height: xs(40), backgroundColor: '#415993' }]}>
                        <View style={[ p.row, { height: xs(40) }]}>
                            <Svg icon="fb" size={30} style={[ b.mt1, { marginLeft: xs(35) }]} />
                            <Text style={[ c.light, b.ml2, b.mt2 ]}> Sign In with Facebook </Text>
                        </View>
                    </TouchableOpacity>
                    
                    <TouchableOpacity onPress={this._signIn} style={[ b.roundedLow, b.mt2, { width: xs(250), height: xs(40), backgroundColor: '#D85040' }]}>
                        <View style={[ p.row, { height: xs(40) }]}>
                            <Svg icon="google" size={30} style={[ b.mt1, { marginLeft: xs(35) }]} />
                            <Text style={[ c.light, b.ml2, b.mt2 ]}> Sign In with Google </Text>
                        </View>
                    </TouchableOpacity>
                    <View style={[ b.mt2, p.row, p.center, { width: '100%' }]}>
                    	<View style={{ width: xs(100), borderWidth: 1, borderColor: '#dbdbdb' }} />
	                    <Text style={{ color: '#b8b8b8' }}> Atau </Text>
                    	<View style={{ width: xs(100), borderWidth: 1, borderColor: '#dbdbdb' }} />
                    </View>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Email')} style={[ b.roundedLow, b.mt2, { width: xs(250), height: xs(40), backgroundColor: '#469FF8' }]}>
                        <View style={[ p.center, { height: xs(40) }]}>
                            <Text style={[ c.light ]}> Sign In with Email </Text>
                        </View>
                    </TouchableOpacity>
					<TouchableOpacity onPress={() => this.props.navigation.navigate('SignInPhone')} style={[ b.roundedLow, b.mt2, { width: xs(250), height: xs(40), backgroundColor: '#469FF8' }]}>
                        <View style={[ p.center, { height: xs(40) }]}>
                            <Text style={[ c.light ]}> Sign In with Phone Number </Text>
                        </View>
                    </TouchableOpacity>
				</View>
			</ImageBackground>
		)
	}
}