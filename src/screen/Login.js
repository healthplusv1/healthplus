import React, { Component } from 'react';
import { View, StyleSheet, Text, TouchableNativeFeedback } from 'react-native';
import Input from '../component/Input';
import axios from 'axios'
import { connect } from 'react-redux';
import { setAuth } from '../module/AuthAction';
import { POST } from '../helper/Request';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: ""
    };
    this._doLogin = this._doLogin.bind(this)
  }

  _doLogin() {
    POST('https://eventrific.com/api/v1/login', {
      //   email: this.state.email,
      //   password: this.state.password
      email: 'sangvictim@gmail.com',
      password: '12345678'
    })
      .then(response => {
        this.props.setauth({
          user: response.data.result.user,
          token: response.data.result.token,
          isLogedIn: true
        })
        this.props.navigation.navigate('ModulHome')
      })
      .catch(error => {
        console.debug(JSON.stringify(error))
        alert("Email & password salah !")
      })
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={{ width: '100%' }}>
          <Input
            label={"Email"}
            onChange={email => this.setState({ email })}
            value={this.state.email}
          />
          <Input
            label={"Password"}
            onChange={password => this.setState({ password })}
            value={this.state.password}
          />

          <TouchableNativeFeedback onPress={this._doLogin}>
            <View style={{ alignItems: 'center', paddingVertical: 8, borderRadius: 50, borderColor: '#979797', borderWidth: 1, marginTop: 20 }}>
              <Text style={{ color: '#fff', fontSize: 20 }} allowFontScaling={false}>Login</Text>
            </View>
          </TouchableNativeFeedback>

          <TouchableNativeFeedback onPress={() => this.props.navigation.navigate('Register')}>
            <View style={{ alignItems: 'center', paddingVertical: 8, borderRadius: 50, borderColor: '#979797', borderWidth: 1, marginTop: 20 }}>
              <Text style={{ color: '#fff', fontSize: 20 }} allowFontScaling={false}>Register</Text>
            </View>
          </TouchableNativeFeedback>
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#3a3535',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
  }
})

const mapStateToProps = (state) => {
  return {
    isLogedIn: state.auth.isLogedIn,
    user: state.auth.user,
    token: state.auth.token
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setauth: (payload = {}) => dispatch(setAuth(payload))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)
