import React from 'react'
import { createSwitchNavigator, createAppContainer } from 'react-navigation'
import { createDrawerNavigator } from 'react-navigation-drawer'
import { createStackNavigator } from 'react-navigation-stack'
import { createBottomTabNavigator } from 'react-navigation-tabs'
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';

//================= import stack ================
import { Homestack } from '../module/home/HomeRoute'

import { eCommercestack } from '../module/ecommerce/eCommerceRoute'

import { Challengesstack } from '../module/challenge/ChallengeRoute'

import { Rewardstack } from '../module/claim/navigation'

import { Socialstack } from '../module/social/Route'

import { Profilestack } from '../module/profile/ProfileRoute.js'

import { DriverStack } from '../module/driver/DriverRoute'


//================= impor screen ================
import SplashScreenPage from '../screen/SplashScreen';
// import PhonePage from '../screen/Phone';
// import OtpPage from '../screen/Otp';
import IntroPage from '../screen/Intro';
import PermissionPage from '../screen/Permission';
import AkunPage from '../screen/Akun';
import EmailPage from '../screen/Email';
import RegisterPage from '../screen/Register';
import SignInPhonePage from '../screen/SignInPhone';
import SignUpPhonePage from '../screen/SignUpPhone';
import InputNamePage from '../screen/InputName';
import FillinfoPage from '../screen/Fillinfo';
import MapLocationPage from '../screen/MapLocation';
import KTPPage from '../screen/KTP'
import ProsesVerifyKTPPage from '../screen/ProsesVerifyKTP'

//================= Home ================
import WebPage from '../module/home/screen/Web';
import FirstUNPage from '../module/home/screen/FirstUN';
import MorePage from '../module/home/screen/More';
import ReleaseCOVID19Page from '../module/home/screen/ReleaseCOVID19';
import PublicStatementPage from '../module/home/screen/PublicStatement';
import DirectorGeneralPage from '../module/home/screen/DirectorGeneral';
import NewsPage from '../module/home/screen/News';
import AvatarPage from '../module/home/screen/Avatar';
import DailyHoroskopePage from '../module/home/screen/Games/DailyHoroskope';
import PoinPage from '../module/home/screen/Poin';
import ReferralPage from '../module/home/screen/Referral';
import AddCommentPage from '../module/home/screen/AddComment';
import WalkingChallengesPage from '../module/home/screen/WalkingChallenges'

//================= Tracking ================
// import HomeTrackingPage from '../module/home/screen/Tracking/HomeTracking';
// import MapsTrackingPage from '../module/home/screen/Tracking/MapsTracking';
import ListLeagueTerdekatPage from '../module/home/screen/Tracking/ListLeagueTerdekat'
import BuatGrupPage from '../module/home/screen/Tracking/BuatGrup';
import ProfTrackingPage from '../module/home/screen/Tracking/ProfTracking';
import EditProfPage from '../module/home/screen/Tracking/EditProf';
import BuatKompetisiPage from '../module/home/screen/Tracking/BuatKompetisi';
import UploadVideoPage from '../module/home/screen/Tracking/UploadVideo';
import UploadImagePage from '../module/home/screen/Tracking/UploadImage';
import UploadPhotoPage from '../module/home/screen/Tracking/UploadPhoto';
import CariTemanPage from '../module/home/screen/Tracking/CariTeman';
import MengikutiPage from '../module/home/screen/Tracking/Mengikuti';
import PengikutPage from '../module/home/screen/Tracking/Pengikut';
import AjakTemanLeaguePage from '../module/home/screen/Tracking/AjakTemanLeague';
import AjakTemanChallPage from '../module/home/screen/Tracking/AjakTemanChall';
import PenilaianPesertaChallPage from '../module/home/screen/Tracking/PenilaianPesertaChall';
import BuatPostinganPage from '../module/home/screen/Tracking/BuatPostingan';
import UserPostKomentarPage from '../module/home/screen/Tracking/UserPostKomentar';
import KTPUlangPage from '../module/home/screen/Tracking/KTPUlang'
import ProsesVerifyKTPUlangPage from '../module/home/screen/Tracking/ProsesVerifyKTPUlang'

//================= Claim ================
import AdobePage from '../module/claim/screen/Adobe';
import HowPage from '../module/claim/screen/How';
import HtmlClaimPage from '../module/claim/screen/HtmlClaim';
import GamePage from '../module/claim/screen/Game';
import HtmlPage from '../module/claim/screen/src/Html';
import FilmClaimPage from '../module/claim/screen/FilmClaim';
import FilmDramaPage from '../module/claim/screen/FilmDrama';
import MediaMoviePage from '../module/claim/screen/MediaMovie';
import MediaClaimPage from '../module/claim/screen/MediaClaim';
import DramaClaimPage from '../module/claim/screen/DramaClaim';

//================= Reward ================
import HomeScreenPage from '../module/reward/screen/HomeScreen';
import StarbucksVoucherPage from '../module/reward/screen/StarbucksVoucher';
import TokenVoucherPage from '../module/reward/screen/TokenVoucher';
import IndomartVoucherPage from '../module/reward/screen/IndomartVoucher'
import IndomartVoucher50Page from '../module/reward/screen/IndomartVouchers50'
import IndomartVoucher100Page from '../module/reward/screen/IndomartVouchers100'

//================= League ================
import Grup_ChallPage from '../module/league/screen/Grup_Chall'
import Kompetisi_LeaguePage from '../module/league/screen/Kompetisi_League'
import RiwayatKompetisi_LeaguePage from '../module/league/screen/RiwayatKompetisi_League'
import LihatSemuaPostinganPage from '../module/home/screen/Tracking/LihatSemuaPostingan'
import LihatKomePage from '../module/home/screen/Tracking/LihatKome'

//================= Challenge ================
// import LiveStreamPage from '../module/challenge/screen/LiveStream'
// import LiveStreamTwoPage from '../module/challenge/screen/LiveStreamTwo'
// import StoryViewPage from '../module/challenge/screen/StoryView'
import RatingPage from '../module/challenge/screen/Rating'

//================= Fun ================
import AddFunPage from '../module/profile/screen/AddFun';

//================= Drawer ================
import AboutPage from '../module/drawer/screen/About';
import GabungDriverPage from '../module/drawer/screen/GabungDriver'
import GabungPenjualPage from '../module/drawer/screen/GabungPenjual'
import SIMPage from '../module/drawer/screen/SIM'
import ProsesVerifyPage from '../module/drawer/screen/ProsesVerify'

//================= TopUp ================
import TopUpPage from '../module/topup/screen/TopUp'
import PembayaranPage from '../module/topup/screen/Pembayaran'
import CheckPaymentPage from '../module/topup/screen/CheckPayment'

//================= E-Commerce ================
import CartPage from '../module/ecommerce/screen/Cart'
import PaymentPage from '../module/ecommerce/screen/Payment'
import SearchAddressPage from '../module/ecommerce/screen/SearchAddress'
import HistoryPaymentPage from '../module/ecommerce/screen/HistoryPayment'
import DetailPaymentPage from '../module/ecommerce/screen/DetailPayment'
import TopUpSaldoPage from '../module/ecommerce/screen/TopUpSaldo'
import DetailProductPage from '../module/ecommerce/screen/DetailProduct'
import FrequentlyOrderPage from '../module/ecommerce/screen/FrequentlyOrder'
import DisbutePage from '../module/ecommerce/screen/Disbute'
import PromoCodePage from '../module/ecommerce/screen/PromoCode'
import PharmacyServicePage from '../module/ecommerce/screen/PharmacyService'
import ChatPharmacyPage from '../module/ecommerce/screen/ChatPharmacy'
import AddReviewPage from '../module/ecommerce/screen/AddReview'
import AllReviewPage from '../module/ecommerce/screen/AllReview'
import ChatDriverPage from '../module/ecommerce/screen/ChatDriver'
import MapsPage from '../module/ecommerce/screen/Maps'
import ListSymtomsPage from '../module/ecommerce/screen/ListSymtoms'
import ListMedicinePage from '../module/ecommerce/screen/ListMedicine'

//================= Profile ================
import SmartWatchPage from '../module/profile/screen/SmartWatch'

//================= impor icon ================
import { Image, Text } from 'react-native'
import Ionicons from "react-native-vector-icons/Ionicons";
import Svg from '../utils/Svg';
import { blue, light } from '../utils/Color';
import { xs, vs } from '../module/home/utils/Responsive'

const apotek = require('../asset/icons/apotek_icon.png');
const profile = require('../asset/icons/profilebutton.png');
const driver = require('../asset/icons/driver_icon.png');

const MainTabs = createBottomTabNavigator({
    ModulHome: {
        screen: Homestack,
        navigationOptions: {
            tabBarLabel: "Beranda",
            tabBarIcon: ({ tintColor }) => (
                <Svg icon="home" size={30} fill={tintColor} />
            )
        }
    },
    ModulECommerce: {
        screen: eCommercestack,
        navigationOptions: {
            tabBarLabel: "Apotek",
            tabBarIcon: ({ focused }) => (
                <Image source={apotek} style={{ width: wp('6.5%'), height: wp('6.5%'), tintColor: focused ? blue : light }} />
            )
        }
    },
    ModulChallenges: {
        screen: Challengesstack,
        navigationOptions: {
            tabBarLabel: "Tantangan",
            tabBarIcon: ({ tintColor }) => (
                <Svg icon="challenge" size={30} fill={tintColor} />
            )
        }
    },
    ModulSetting: {
        screen: Socialstack,
        navigationOptions: {
            tabBarLabel: "Social",
            tabBarIcon: ({ tintColor }) => (
                <Svg icon="dasdas" size={30} fill={tintColor} />
            )
        }
    },
    // ModulNotification: {
    //     screen: Rewardstack,
    //     navigationOptions: {
    //         tabBarLabel: "Hadiah",
    //         tabBarIcon: ({ tintColor }) => (
    //             <Svg icon="klaim" size={30} fill={tintColor} />
    //         )
    //     }
    // },
    ModulLeague: {
        screen: Profilestack,
        navigationOptions: {
            tabBarLabel: "Profil",
            tabBarIcon: ({ focused }) => (
                <Image source={profile} style={{ width: wp('6.5%'), height: wp('6.5%'), tintColor: focused ? blue : light }} />
            )
        }
    },
    }, {
    tabBarOptions: {
        activeTintColor: blue,
        inactiveTintColor: '#FFF',
        showLabel: true,
        style:{
            backgroundColor: '#2A2A2A',
            height: xs(60),
            borderTopColor: '#2A2A2A'
        },
        labelStyle:{
            marginBottom: vs(8)
        }
    }
})

const DriverTabs = createBottomTabNavigator({
    ModulHome: {
        screen: Homestack,
        navigationOptions: {
            tabBarLabel: "Beranda",
            tabBarIcon: ({ tintColor }) => (
                <Svg icon="home" size={30} fill={tintColor} />
            )
        }
    },
    ModulNotification: {
        screen: DriverStack,
        navigationOptions: {
            tabBarLabel: "Jobs",
            tabBarIcon: ({ focused }) => (
                <Image source={driver} style={{ width: wp('6.5%'), height: wp('6.5%'), tintColor: focused ? blue : light }} />
            )
        }
    },
    ModulECommerce: {
        screen: eCommercestack,
        navigationOptions: {
            tabBarLabel: "Apotek",
            tabBarIcon: ({ focused }) => (
                <Image source={apotek} style={{ width: wp('6.5%'), height: wp('6.5%'), tintColor: focused ? blue : light }} />
            )
        }
    },
    ModulChallenges: {
        screen: Challengesstack,
        navigationOptions: {
            tabBarLabel: "Tantangan",
            tabBarIcon: ({ tintColor }) => (
                <Svg icon="challenge" size={30} fill={tintColor} />
            )
        }
    },
    ModulSetting: {
        screen: Socialstack,
        navigationOptions: {
            tabBarLabel: "Social",
            tabBarIcon: ({ tintColor }) => (
                <Svg icon="dasdas" size={30} fill={tintColor} />
            )
        }
    },
    ModulLeague: {
        screen: Profilestack,
        navigationOptions: {
            tabBarLabel: "Profil",
            tabBarIcon: ({ focused }) => (
                <Image source={profile} style={{ width: wp('6.5%'), height: wp('6.5%'), tintColor: focused ? blue : light }} />
            )
        }
    },
    }, {
    tabBarOptions: {
        activeTintColor: blue,
        inactiveTintColor: '#FFF',
        showLabel: true,
        style:{
            backgroundColor: '#2A2A2A',
            height: xs(60),
            borderTopColor: '#2A2A2A'
        },
        labelStyle:{
            marginBottom: vs(8)
        }
    }
})

const App = createStackNavigator({
    SplashScreen: {
        screen: SplashScreenPage,
        navigationOptions:{
            header: null,
        }
    },
    // Phone: {
    //     screen: PhonePage,
    //     navigationOptions:{
    //         header: null,
    //     }
    // },
    // Otp: {
    //     screen: OtpPage,
    //     navigationOptions:{
    //         header: null,
    //     }
    // },
    Intro: {
        screen: IntroPage,
        navigationOptions:{
            header: null,
        }
    },
    Permission: {
        screen: PermissionPage,
        navigationOptions:{
            header: null,
        }
    },
    Akun: {
        screen: AkunPage,
        navigationOptions:{
            header: null,
        }
    },
    Email: {
        screen: EmailPage,
        navigationOptions:{
            header: null,
        }
    },
    Register: {
        screen: RegisterPage,
        navigationOptions:{
            header: null,
        }
    },
    SignInPhone: {
        screen: SignInPhonePage,
        navigationOptions:{
            header: null,
        }
    },
    SignUpPhone: {
        screen: SignUpPhonePage,
        navigationOptions:{
            header: null,
        }
    },
    InputName: {
        screen: InputNamePage,
        navigationOptions:{
            header: null,
        }
    },
    Fillinfo: {
        screen: FillinfoPage,
        navigationOptions:{
            header: null,
        }
    },
    Maplocation: {
        screen: MapLocationPage,
        navigationOptions:{
            header: null,
        }
    },
    KTP: {
        screen: KTPPage,
        navigationOptions:{
            header: null
        }
    },
    ProsesVerifyKTP: {
        screen: ProsesVerifyKTPPage,
        navigationOptions: {
            header: null
        }
    },
    Tabs: {
        screen: MainTabs,
        navigationOptions:{
            header: null,
        }
    },

    //Home    
    Web: {
        screen: WebPage,
        navigationOptions:{
            header: null,
        }
    },
    FirstUN: {
        screen: FirstUNPage,
        navigationOptions:{
            header: null,
        }
    },
    More: {
        screen: MorePage,
        navigationOptions:{
            header: null,
        }
    },
    ReleaseCOVID19: {
        screen: ReleaseCOVID19Page,
        navigationOptions:{
            header: null,
        }
    },
    PublicStatement: {
        screen: PublicStatementPage,
        navigationOptions:{
            header: null,
        }
    },
    DirectorGeneral: {
        screen: DirectorGeneralPage,
        navigationOptions:{
            header: null,
        }
    },
    News: {
        screen: NewsPage,
        navigationOptions:{
            header: null,
        }
    },
    Avatar: {
        screen: AvatarPage,
        navigationOptions:{
            header: null,
        }
    },
    DailyHoroskope: {
        screen: DailyHoroskopePage,
        navigationOptions:{
            header: null,
        }
    },
    Poin: {
        screen: PoinPage,
        navigationOptions:{
            header: null,
        }
    },
    Referral: {
        screen: ReferralPage,
        navigationOptions:{
            header: null,
        }
    },
    AddComment:{
        screen: AddCommentPage,
        navigationOptions:{
            header: null,
        }
    },
    WalkingChallenges: {
        screen: WalkingChallengesPage,
        navigationOptions: {
            header: null
        }
    },

    //Tracking
    // HomeTracking: {
    //     screen: HomeTrackingPage,
    //     navigationOptions:{
    //         header: null,
    //     }
    // },

    // MapsTracking: {
    //     screen: MapsTrackingPage,
    //     navigationOptions:{
    //         header: null,
    //     }
    // },

    ListLeagueTerdekat: {
        screen: ListLeagueTerdekatPage,
        navigationOptions:{
            header: null,
        }
    },

    BuatGrup: {
        screen: BuatGrupPage,
        navigationOptions:{
            header: null,
        }
    },

    ProfTracking: {
        screen: ProfTrackingPage,
        navigationOptions:{
            header: null,
        }
    },

    EditProf: {
        screen: EditProfPage,
        navigationOptions:{
            header: null,
        }
    },

    BuatKompetisi: {
        screen: BuatKompetisiPage,
        navigationOptions:{
            header: null,
        }
    },

    UploadVideo: {
        screen: UploadVideoPage,
        navigationOptions:{
            header: null,
        }
    },

    UploadPhoto: {
        screen: UploadPhotoPage,
        navigationOptions:{
            header: null,
        }
    },

    UploadImage: {
        screen: UploadImagePage,
        navigationOptions:{
            header: null,
        }
    },

    CariTeman: {
        screen: CariTemanPage,
        navigationOptions:{
            header: null,
        }
    },

    Mengikuti: {
        screen: MengikutiPage,
        navigationOptions:{
            header: null,
        }
    },

    Pengikut: {
        screen: PengikutPage,
        navigationOptions:{
            header: null,
        }
    },

    AjakTemanLeague: {
        screen: AjakTemanLeaguePage,
        navigationOptions:{
            header: null,
        }
    },

    AjakTemanChall: {
        screen: AjakTemanChallPage,
        navigationOptions:{
            header: null,
        }
    },

    PenilaianPesertaChall: {
        screen: PenilaianPesertaChallPage,
        navigationOptions:{
            header: null,
        }
    },

    BuatPostingan: {
        screen: BuatPostinganPage,
        navigationOptions:{
            header: null,
        }
    },

    UserPostKomentar: {
        screen: UserPostKomentarPage,
        navigationOptions:{
            header: null,
        }
    },

    KTPUlang: {
        screen: KTPUlangPage,
        navigationOptions: {
            header: null
        }
    },

    ProsesVerifyKTPUlang: {
        screen: ProsesVerifyKTPUlangPage,
        navigationOptions: {
            header: null
        }
    },

    //Claim
    Html: {
        screen: HtmlPage,
        navigationOptions:{
            header: null,
        }
    },
    Adobe: {
        screen: AdobePage,
        navigationOptions:{
            header: null,
        }
    },
    How: {
        screen: HowPage,
        navigationOptions:{
            header: null,
        }
    },
    HtmlClaim: {
        screen: HtmlClaimPage,
        navigationOptions:{
            header: null,
        }
    },
    Game: {
        screen: GamePage,
        navigationOptions:{
            header: null,
        }
    },
    FilmClaim: {
        screen: FilmClaimPage,
        navigationOptions:{
            header: null,
        }
    },
    FilmDrama: {
        screen: FilmDramaPage,
        navigationOptions:{
            header: null,
        }
    },
    MediaMovie: {
        screen: MediaMoviePage,
        navigationOptions:{
            header: null,
        }
    },
    MediaClaim: {
        screen: MediaClaimPage,
        navigationOptions:{
            header: null,
        }
    },
    DramaClaim: {
        screen: DramaClaimPage,
        navigationOptions:{
            header: null,
        }
    },

    //Reward
    HomeScreen: {
        screen: HomeScreenPage,
        navigationOptions:{
            header: null,
        }
    },
    StarbucksVoucher: {
        screen: StarbucksVoucherPage,
        navigationOptions:{
            header: null,
        }
    },
    TokenVoucher: {
        screen: TokenVoucherPage,
        navigationOptions:{
            header: null,
        }
    },
    IndomartVoucher: {
        screen: IndomartVoucherPage,
        navigationOptions:{
            header: null,
        }
    },
    IndomartVoucher50: {
        screen: IndomartVoucher50Page,
        navigationOptions:{
            header: null,
        }
    },
    IndomartVoucher100: {
        screen: IndomartVoucher100Page,
        navigationOptions:{
            header: null,
        }
    },

    //League
    Grup_Chall: {
        screen: Grup_ChallPage,
        navigationOptions:{
            header: null,
        }
    },

    Kompetisi_League: {
        screen: Kompetisi_LeaguePage,
        navigationOptions:{
            header: null,
        }
    },

    RiwayatKompetisi_League: {
        screen: RiwayatKompetisi_LeaguePage,
        navigationOptions:{
            header: null,
        }
    },

    LihatSemuaPostingan: {
        screen: LihatSemuaPostinganPage,
        navigationOptions:{
            header: null,
        }
    },

    LihatKome: {
        screen: LihatKomePage,
        navigationOptions:{
            header: null,
        }
    },

    //Challenge
    // LiveStream: {
    //     screen: LiveStreamPage,
    //     navigationOptions:{
    //         header: null
    //     }
    // },
    // LiveStreamTwo: {
    //     screen: LiveStreamTwoPage,
    //     navigationOptions: {
    //         header: null
    //     }
    // },
    // StoryView: {
    //     screen: StoryViewPage,
    //     navigationOptions: {
    //         header: null
    //     }
    // },
    Rating: {
        screen: RatingPage,
        navigationOptions: {
            header: null
        }
    },

    //Fun
    AddFun: {
        screen: AddFunPage,
        navigationOptions:{
            header: null,
        }
    },
    
    //Drawer
    About: {
        screen: AboutPage,
        navigationOptions:{
            header: null,
        }
    },

    GabungDriver: {
        screen: GabungDriverPage,
        navigationOptions: {
            header: null
        }
    },

    GabungPenjual: {
        screen: GabungPenjualPage,
        navigationOptions: {
            header: null
        }
    },

    SIM: {
        screen: SIMPage,
        navigationOptions: {
            header: null
        }
    },

    ProsesVerify: {
        screen: ProsesVerifyPage,
        navigationOptions: {
            header: null
        }
    },

    //TopUp
    TopUp: {
        screen: TopUpPage,
        navigationOptions:{
            header: null,
        }
    },
    Pembayaran: {
        screen: PembayaranPage,
        navigationOptions:{
            header: null
        }
    },
    CheckPayment: {
        screen: CheckPaymentPage,
        navigationOptions: {
            header: null
        }
    },

    //E_Commerce
    Cart: {
        screen: CartPage,
        navigationOptions: {
            header: null
        }
    },

    Payment: {
        screen: PaymentPage,
        navigationOptions: {
            header: null
        }
    },

    SearchAddress: {
        screen: SearchAddressPage,
        navigationOptions: {
            header: null
        }
    },

    HistoryPayment: {
        screen: HistoryPaymentPage,
        navigationOptions: {
            header: null
        }
    },

    DetailPayment: {
        screen: DetailPaymentPage,
        navigationOptions: {
            header: null
        }
    },

    TopUpSaldo: {
        screen: TopUpSaldoPage,
        navigationOptions: {
            header: null
        }
    },

    DetailProduct: {
        screen: DetailProductPage,
        navigationOptions: {
            header: null
        }
    },

    FrequentlyOrder: {
        screen: FrequentlyOrderPage,
        navigationOptions: {
            header: null
        }
    },

    Disbute: {
        screen: DisbutePage,
        navigationOptions: {
            header: null
        }
    },

    PromoCode: {
        screen: PromoCodePage,
        navigationOptions: {
            header: null
        }
    },

    PharmacyService: {
        screen: PharmacyServicePage,
        navigationOptions: {
            header: null
        }
    },

    ChatPharmacy: {
        screen: ChatPharmacyPage,
        navigationOptions: {
            header: null
        }
    },

    AddReview: {
        screen: AddReviewPage,
        navigationOptions: {
            header: null
        }
    },

    AllReview: {
        screen: AllReviewPage,
        navigationOptions: {
            header: null
        }
    },

    ChatDriver: {
        screen: ChatDriverPage,
        navigationOptions: {
            header: null
        }
    },

    Maps: {
        screen: MapsPage,
        navigationOptions: {
            header: null
        }
    },

    ListSymtoms: {
        screen: ListSymtomsPage,
        navigationOptions: {
            header: null
        }
    },

    ListMedicine: {
        screen: ListMedicinePage,
        navigationOptions: {
            header: null
        }
    },

    //profile
    SmartWatch: {
        screen: SmartWatchPage,
        navigationOptions: {
            header: null
        }
    },

    Driver: {
        screen: DriverTabs,
        navigationOptions: {
            header: null
        }
    },
    
    initialRouteName: "SplashScreen"
});

export default createAppContainer(App);